//
// $Id: fd_money.h,v 1.3 2001/10/18 07:41:54 mavetju Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef INCLUDED_FD_MONEY_H
#define INCLUDED_FD_MONEY_H

#define	COINS_SILVER_IN_GOLD	100

//
// Bank and money routines
//
char *		money_string_colour	(int gold,int silver,char *colour);
char *		money_string		(int gold,int silver);
CHAR_DATA *	find_banker		(CHAR_DATA *ch);
bool		can_afford		(CHAR_DATA *ch,CHAR_DATA *keeper,
					 int price);

void		do_balance		(CHAR_DATA *ch,char *argument);
void		do_deposit		(CHAR_DATA *ch,char *argument);
void		do_withdraw		(CHAR_DATA *ch,char *argument);

#endif	// INCLUDED_FD_MONEY_H
