$Id: SYNTAX,v 1.1 2001/08/14 00:05:51 edwin Exp $

CODING SYNTAX - MavEtJu's

These is the coding-syntax-rules I try to follow. I'm not saying
that they are good or bad, they're the one which give me the most
readable code.

INDENT
------
    Indent goes with four spaces. Eight spaces should be a tab.
    In vi(1): set autoindent, set shiftwidth=4

IF
--
    single if-statement
	if (...) {
	} else {
	}

    mulitple if-statements seperated by elses
	if (...) {
	} else if (...) {
	}

    single if-statement with multiple booleans
	if (...
	&&  ...) {
	}


    multiple if-statements which are related
	if (...)	...;
	if (.....)	.....;

    remarks
	- 'if' is not a function! so 'if(...)' is bad.
	- If it doesn't fit on one line, feel free to make something
	  artistic of it.

FOR
---
    normal for-statement
	for (...;...;...) {
	}

    long normal for-statement
	for (...;
	     ...;
	     ...) {
	}

    remarks
	- 'for' is not a function! so 'for(...)' is bad.
	- If it doesn't fit on one line, feel free to make something
	  artistic of it.

COMPARISON WITH 0
-----------------
    integer:	i==0		i!=0
    pointer:	p==NULL		p!=NULL
    boolean	!b		b

    remarks:
	- comparison with 0 for integers and pointers always written
	  out completly.

SWITCH
------
    switch-statement with long case-statements
	switch (...) {
	case ...:	// identifier
	    ...;
	    break;

	case ...:
	    ...
	    // FALLTHROUGH!

	case ...:
	    break;
	}

    switch-statement with short similar case-statements
	switch (...) {
	case ...:	..........;		break;
	case ...:	..............;		break;
	case ...:	............;		break;
	}

    remarks:
	- Fall through situations should always be clearly identicated.
	- For long switch-statements and the use of which_keyword(),
	  specify what the number of the case-statement is for.

FUNCTIONS
---------
    function header
	void a(...,...) {
	}

    function prototypes
	void	a		(...);
	void	b		(...);
	void	cdefghijklmn	(...);

    remarks:
	- In definitions, always try to make it look readable.

VARIABLES
---------
    in function header
	type	...;
	type *	...;
	type	...=...;

    remarks:
	- Outline if more than one variable.
	- Keep the pointers clearly visible.

