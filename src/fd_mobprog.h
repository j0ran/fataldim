//
// $Id: fd_mobprog.h,v 1.31 2006/03/12 17:18:42 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef INCLUDED_FD_MOBPROG_H
#define INCLUDED_FD_MOBPROG_H

int	mp_error		(CHAR_DATA *ch);

void	mp_act_trigger		(char *argument, CHAR_DATA *mob,
				 CHAR_DATA *ch, const void *arg1,
				 const void *arg2, int type);


void	mp_load_trigger		(CHAR_DATA *mob,CHAR_DATA *loader);
void	mp_entry_trigger	(CHAR_DATA *mob);
void	mp_delay_trigger	(CHAR_DATA *mob);
void	mp_random_trigger	(CHAR_DATA *mob);
void	mp_hour_trigger		(CHAR_DATA *mob,int hour);
void	mp_fight_trigger	(CHAR_DATA *mob,CHAR_DATA *victim);
bool	mp_preattack_trigger	(CHAR_DATA *mob,CHAR_DATA *attacker,char *means);
void	mp_kill_trigger		(CHAR_DATA *mob,CHAR_DATA *attacker);
void	mp_death_trigger	(CHAR_DATA *mob,CHAR_DATA *killer);
void	mp_killed_trigger	(CHAR_DATA *mob,CHAR_DATA *victim);
bool	mp_surrender_trigger	(CHAR_DATA *mob,CHAR_DATA *ch);
void	mp_bribe_trigger	(CHAR_DATA *mob,CHAR_DATA *ch,int amount);
void	mp_flee_trigger		(CHAR_DATA *ch);
bool	mp_exit_trigger		(CHAR_DATA *ch,int dir);
void	mp_leave_trigger	(CHAR_DATA *ch,int dir);
void	mp_give_trigger		(CHAR_DATA *mob,CHAR_DATA *ch,OBJ_DATA *obj);
void 	mp_greet_trigger	(CHAR_DATA *ch,int dir);
void	mp_hpcnt_trigger	(CHAR_DATA *mob,CHAR_DATA *ch);
void	mp_timer_trigger	(CHAR_DATA *mob);
bool	mp_prebuy_trigger	(CHAR_DATA *ch,CHAR_DATA *shopkeeper);
int	mp_buy_trigger		(CHAR_DATA *ch,CHAR_DATA *shopkeeper,OBJ_DATA *obj);
void	mp_postbuy_trigger	(CHAR_DATA *ch, CHAR_DATA *shopkeeper);
bool	mp_prelookat_trigger	(CHAR_DATA *mob,CHAR_DATA *ch);
void	mp_lookat_trigger	(CHAR_DATA *mob,CHAR_DATA *ch);
bool	mp_interpret_unknown	(CHAR_DATA *ch,char *command);
bool	mp_interpret_preknown	(CHAR_DATA *ch,char *command);
void	mp_interpret_postknown	(CHAR_DATA *ch,char *command);
bool    mp_prerecall_trigger	(CHAR_DATA *player);
void    mp_recall_trigger	(CHAR_DATA *player);
void    mp_recallto_trigger	(CHAR_DATA *player);
void    mp_speech_trigger	(CHAR_DATA *player,char *text);
void    mp_tell_trigger		(CHAR_DATA *mob,CHAR_DATA *player,char *text);
void    mp_walked_trigger	(CHAR_DATA *mob);
void    mp_didnotfind_trigger	(CHAR_DATA *mob,int resultcode);
bool    mp_presocial_trigger	(CHAR_DATA *mob,char *string,CHAR_DATA *ch);
void    mp_social_trigger	(CHAR_DATA *mob,char *string,CHAR_DATA *ch);

bool	mp_prespec_trigger	(CHAR_DATA *mob,CHAR_DATA *victim,OBJ_DATA *obj);
void	mp_spec_trigger		(CHAR_DATA *mob,CHAR_DATA *victim,OBJ_DATA *obj);

void	mp_sunset_trigger	(void);
void	mp_sunrise_trigger	(void);
void	mp_dayend_trigger	(void);
void	mp_daystart_trigger	(void);

void    mp_weather_trigger      (int old_state, int new_state);

//
// return values for mp_buy_trigger()
//
#define MP_BUY_NO_TRIGGER	0
#define MP_BUY_ALLOWED		1
#define MP_BUY_NOT_ALLOWED	2
#define MP_BUY_I_GAVE_IT	3

#endif	// INCLUDED_FD_MOBPROG_H
