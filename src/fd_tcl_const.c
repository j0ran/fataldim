//
// $Id: fd_tcl_const.c,v 1.27 2008/05/01 18:47:04 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"

const TABLE_TYPE roomtrigger_table[] = {
    {	"pre_look",		RTRIG_PRELOOK				},
    {	"look",			RTRIG_LOOK				},
    {	"pre_speech",		RTRIG_PRESPEECH				},
    {	"speech",		RTRIG_SPEECH				},
    {	"pre_look_ed",		RTRIG_PRELOOK_ED			},
    {	"look_ed",		RTRIG_LOOK_ED				},
    {	"enter",		RTRIG_ENTER				},
    {	"leave",		RTRIG_LEAVE				},
    {	"pre_recall",		RTRIG_PRERECALL				},
    {	"recall",		RTRIG_RECALL				},
    {	"recall_to",		RTRIG_RECALL_TO				},
    {	"pre_reset",		RTRIG_PRERESET				},
    {	"reset",		RTRIG_RESET				},
    {	"pre_trap",		RTRIG_PRETRAP				},
    {	"trap",			RTRIG_TRAP				},

    {	"canlock",		RTRIG_CANLOCK				},
    {	"pre_lock",		RTRIG_PRELOCK				},
    {	"lock",			RTRIG_LOCK				},
    {	"canunlock",		RTRIG_CANUNLOCK				},
    {	"pre_unlock",		RTRIG_PREUNLOCK				},
    {	"unlock",		RTRIG_UNLOCK				},

    {	"sunset",		RTRIG_SUNSET				},
    {	"sunrise",		RTRIG_SUNRISE				},
    {	"start_day",		RTRIG_DAYSTART				},
    {	"end_day",		RTRIG_DAYEND				},

    {	"weather",		RTRIG_WEATHER				},

    {	"timer",		RTRIG_TIMER				},
    {	"hour",			RTRIG_HOUR				},
    {	"random",		RTRIG_RANDOM				},
    {	"delay",		RTRIG_DELAY				},

    {	"pre_interpret",	RTRIG_INTERPRET_PREKNOWN		},
    {	"post_interpret",	RTRIG_INTERPRET_POSTKNOWN		},
    {	"interpret_unknown",	RTRIG_INTERPRET_UNKNOWN			},
    {	"social",		RTRIG_SOCIAL				},
    {	"pre_social",		RTRIG_PRESOCIAL				},

    {	"pre_attack",		RTRIG_PREATTACK				},
    {	"kill",			RTRIG_KILL				},
    {	"death",		RTRIG_DEATH				},
    {	NULL,			0					}
};

const TABLE_TYPE areatrigger_table[] = {
    {	"pre_reset",		ATRIG_PRERESET				},
    {	"reset",		ATRIG_RESET				},

    {	"timer",		ATRIG_TIMER				},
    {	"hour",			ATRIG_HOUR				},
    {	"random",		ATRIG_RANDOM				},
    {	"delay",		ATRIG_DELAY				},

    {	"enter",		ATRIG_ENTER				},
    {	"leave",		ATRIG_LEAVE				},

    {	"sunset",		ATRIG_SUNSET				},
    {	"sunrise",		ATRIG_SUNRISE				},
    {	"start_day",		ATRIG_DAYSTART				},
    {	"end_day",		ATRIG_DAYEND				},

    {	"weather",		ATRIG_WEATHER				},

    {	NULL,			0					}
};

const TABLE_TYPE objtrigger_table[] = {
    {	"drop",			OTRIG_DROP				},
    {	"get",			OTRIG_GET				},
    {	"put",			OTRIG_PUT				},
    {   "give",			OTRIG_GIVE				},
    {	"open",			OTRIG_OPEN				},
    {	"close",		OTRIG_CLOSE				},
    {	"canlock",		OTRIG_CANLOCK				},
    {	"lock",			OTRIG_LOCK				},
    {	"canunlock",		OTRIG_CANUNLOCK				},
    {	"unlock",		OTRIG_UNLOCK				},
    {	"eat",			OTRIG_EAT				},
    {	"drink",		OTRIG_DRINK				},
    {	"quaff",		OTRIG_QUAFF				},
    {	"recite",		OTRIG_RECITE				},
    {	"lookin",		OTRIG_LOOKIN				},
    {	"lookat",		OTRIG_LOOKAT				},
    {	"lookat_ed",		OTRIG_LOOKAT_ED				},
    {	"zap",			OTRIG_ZAP				},
    {	"wear",			OTRIG_WEAR				},
    {	"remove",		OTRIG_REMOVE				},
    {	"hit",			OTRIG_HIT				},
    {	"enter",		OTRIG_ENTER				},
    {	"key",			OTRIG_KEY				},
    {	"brandish",		OTRIG_BRANDISH				},
    {	"sit",			OTRIG_SIT				},
    {	"stand",		OTRIG_STAND				},
    {	"rest",			OTRIG_REST				},
    {	"sleep",		OTRIG_SLEEP				},
    {	"speech",		OTRIG_SPEECH				},
    {	"read",			OTRIG_READ				},
    {	"sell",			OTRIG_SELL				},
    {	"use",			OTRIG_USE				},
    {	"putin",		OTRIG_PUTIN				},
    {	"getout",		OTRIG_GETOUT				},
    {	"nohit",		OTRIG_NOHIT				},
    {	"play",			OTRIG_PLAY				},
    {	"playfilter",		OTRIG_PLAYFILTER			},
    {   "buy",			OTRIG_BUY				},
    {	"trap",			OTRIG_TRAP				},

    {	"pre_drop",		OTRIG_PREDROP				},
    {	"pre_get",		OTRIG_PREGET				},
    {	"pre_put",		OTRIG_PREPUT				},
    {   "pre_give",		OTRIG_PREGIVE				},
    {	"pre_open",		OTRIG_PREOPEN				},
    {	"pre_close",		OTRIG_PRECLOSE				},
    {	"pre_lock",		OTRIG_PRELOCK				},
    {	"pre_unlock",		OTRIG_PREUNLOCK				},
    {	"pre_eat",		OTRIG_PREEAT				},
    {	"pre_drink",		OTRIG_PREDRINK				},
    {	"pre_quaff",		OTRIG_PREQUAFF				},
    {	"pre_recite",		OTRIG_PRERECITE				},
    {	"pre_lookin",		OTRIG_PRELOOKIN				},
    {	"pre_lookat",		OTRIG_PRELOOKAT				},
    {	"pre_lookat_ed",	OTRIG_PRELOOKAT_ED			},
    {	"pre_zap",		OTRIG_PREZAP				},
    {	"pre_wear",		OTRIG_PREWEAR				},
    {	"pre_remove",		OTRIG_PREREMOVE				},
    {	"pre_hit",		OTRIG_PREHIT				},
    {	"pre_enter",		OTRIG_PREENTER				},
    {	"pre_key",		OTRIG_PREKEY				},
    {	"pre_brandish",		OTRIG_PREBRANDISH			},
    {	"pre_sit",		OTRIG_PRESIT				},
    {	"pre_stand",		OTRIG_PRESTAND				},
    {	"pre_rest",		OTRIG_PREREST				},
    {	"pre_sleep",		OTRIG_PRESLEEP				},
    {	"pre_speech",		OTRIG_PRESPEECH				},
    {	"pre_attack",		OTRIG_PREATTACK				},
    {	"pre_read",		OTRIG_PREREAD				},
    {	"pre_sell",		OTRIG_PRESELL				},
    {	"pre_use",		OTRIG_PREUSE				},
    {	"pre_putin",		OTRIG_PREPUTIN				},
    {	"pre_getout",		OTRIG_PREGETOUT				},
    {	"pre_play",		OTRIG_PREPLAY				},
    {	"pre_trap",		OTRIG_PRETRAP				},

    {	"sunset",		OTRIG_SUNSET				},
    {	"sunrise",		OTRIG_SUNRISE				},
    {	"start_day",		OTRIG_DAYSTART				},
    {	"end_day",		OTRIG_DAYEND				},

    {	"weather",		OTRIG_WEATHER				},

    {	"timer",		OTRIG_TIMER				},
    {	"hour",			OTRIG_HOUR				},
    {	"random",		OTRIG_RANDOM				},
    {	"delay",		OTRIG_DELAY				},

    {	"load",			OTRIG_LOAD				},

    {	"interpret_unknown",	OTRIG_INTERPRET_UNKNOWN			},
    {	"pre_interpret",	OTRIG_INTERPRET_PREKNOWN		},
    {	"post_interpret",	OTRIG_INTERPRET_POSTKNOWN		},

    {	NULL,			0					}
};

const TABLE_TYPE mobtrigger_table[] = {
    {	"act",			MTRIG_ACT				},
    {	"bribe",		MTRIG_BRIBE				},
    {	"death",		MTRIG_DEATH				},
    {	"entry",		MTRIG_ENTRY				},
    {	"fight",		MTRIG_FIGHT				},
    {	"flee",			MTRIG_FLEE				},
    {	"give",			MTRIG_GIVE				},
    {	"greet",		MTRIG_GREET				},
    {	"grall",		MTRIG_GRALL				},
    {	"pre_attack",		MTRIG_PREATTACK				},
    {	"kill",			MTRIG_KILL				},
    {	"hpcnt",		MTRIG_HITPOINTCOUNT			},
    {	"random",		MTRIG_RANDOM				},
    {	"speech",		MTRIG_SPEECH				},
    {	"exit",			MTRIG_EXIT				},
    {	"exall",		MTRIG_EXALL				},
    {	"delay",		MTRIG_DELAY				},
    {	"surrender",		MTRIG_SURRENDER				},
    {	"leave",		MTRIG_LEAVE				},
    {	"leall",		MTRIG_LEALL				},
    {   "timer",		MTRIG_TIMER				},
    {	"hour",			MTRIG_HOUR				},
    {	"special",		MTRIG_SPECIAL				},
    {	"prebuy",		MTRIG_PREBUY				},
    {	"buy",			MTRIG_BUY				},
    {	"postbuy",		MTRIG_POSTBUY				},
    {	"lookat",		MTRIG_LOOKAT				},
    {	"pre_lookat",		MTRIG_PRELOOKAT				},
    {	"killed",		MTRIG_KILLED				},
    {	"pre_recall",		MTRIG_PRERECALL				},
    {	"recall",		MTRIG_RECALL				},
    {	"recall_to",		MTRIG_RECALL_TO				},
    {	"walked",		MTRIG_WALKED				},
    {	"notfound",		MTRIG_DIDNOTFIND			},
    {	"load",			MTRIG_LOAD				},
    {	"pre_spec",		MTRIG_PRESPEC				},
    {	"spec",			MTRIG_SPEC				},

    {	"interpret_unknown",	MTRIG_INTERPRET_UNKNOWN			},
    {	"pre_interpret",	MTRIG_INTERPRET_PREKNOWN		},
    {	"post_interpret",	MTRIG_INTERPRET_POSTKNOWN		},
    {	"social",		MTRIG_SOCIAL				},
    {	"pre_social",		MTRIG_PRESOCIAL				},

    {	"sunset",		MTRIG_SUNSET				},
    {	"sunrise",		MTRIG_SUNRISE				},
    {	"start_day",		MTRIG_DAYSTART				},
    {	"end_day",		MTRIG_DAYEND				},

    {	"weather",		MTRIG_WEATHER				},

    {	NULL,			0					}
};

const TABLE_TYPE mobmem_table[] =
{
    {	"customer",		MEM_CUSTOMER				},
    {	"seller",		MEM_SELLER				},
    {	"hostile",		MEM_HOSTILE				},
    {	"afraid",		MEM_AFRAID				},
    {	"custom1",		MEM_CUSTOM1,				},
    {	"custom2",		MEM_CUSTOM2,				},
    {	"custom3",		MEM_CUSTOM3,				},
    {	"custom4",		MEM_CUSTOM4,				},

    {	NULL,			0					}
};

