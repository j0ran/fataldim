/* $Id: comm.c,v 1.194 2008/03/26 14:37:25 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Thanks to abaddon for proof-reading our comm.c and pointing out bugs.  *
 *  Any remaining bugs are, of course, our work, not his.  :)              *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

/*
 * This file contains all of the OS-dependent stuff:
 *   startup, BSD sockets for tcp/ip, i/o, timing.
 *
 * The data flow for input is:
 *    Game_loop ---> Read_from_descriptor ---> Read
 *    Game_loop ---> Read_from_buffer
 *
 * The data flow for output is:
 *    Game_loop ---> Process_Output ---> Write_to_descriptor -> Write
 *
 * The OS-dependent functions are Read_from_descriptor and Write_to_descriptor.
 * -- Furey  26 Jan 1993
 */

/* 19971223 EG	Modified nanny()/CON_GEN_GROUPS to make sure at
        least all the skills from the base group are
        included.
 * 19980106 EG	And removed the code because the bug was somewhere else
 */

#include <signal.h>
#include "merc.h"

#include "interp.h"
#include "color.h"
#include "fd_screen.h"
#include "fd_mxp.h"
#ifdef I3
#include "intermud3/fd_i3.h"
#endif
#ifdef THREADED_DNS
#include "rdns_cache.h"
#endif

/*
 * OS-dependent declarations.
 */

#if defined(__CYGWIN32__)
int	close		( int fd );
int	read		( int fd, char *buf, int nbyte );
int	write		( int fd, char *buf, int nbyte );
#endif




/*
 * Global variables.
 */
DESCRIPTOR_DATA *   descriptor_list;	/* All open descriptors		*/
DESCRIPTOR_DATA *   d_next;		/* Next descriptor in loop	*/
FILE *		    fpReserve;		/* Reserved file handle		*/
bool                MOBtrigger = TRUE;  /* act() switch                 */
time_t		    current_time;	/* time of this pulse		*/




/* Signal definition, linux/unix only */
#ifdef UNIX
typedef void (*sigfunc)(int);
#define RETSIGTYPE void
#endif

/*
 * Other local functions (OS-independent).
 */
bool	check_parse_name	( char *name );
bool	check_reconnect		( DESCRIPTOR_DATA *d, char *name,
                              bool fConn );
bool	check_playing		( DESCRIPTOR_DATA *d, char *name );
void	check_hack		( DESCRIPTOR_DATA *d, char *name );
void	nanny			( DESCRIPTOR_DATA *d, char *argument );

// OS wants us to do a copyover
void sig_hup(int signal) {
    logf("SIGNAL! HUP --> copyover");
    mud_data.reboot_type=REBOOT_COPYOVER;
    mud_data.reboot_pulse=PULSE_REBOOT;
    mud_data.reboot_timer=5;
    strcpy(mud_data.reboot_by,"the operating system");

    send_to_all_char("}rCOPYOVER instructions received from operating system. GO SOMEWHERE SAFE NOW!{x\n\r");
}

// OS wants us to do a shutdown
void sig_term(int signal) {
    logf("SIGNAL! TERM --> shutdown");
    mud_data.reboot_type=REBOOT_SHUTDOWN;
    mud_data.reboot_pulse=1;
    mud_data.reboot_timer=1;
    strcpy(mud_data.reboot_by,"the operating system");

    send_to_all_char("}rSHUTDOWN instructions received from operating system. BYE BYE!{x\n\r");
}

// OS wants us to do a shutdown
void sig_int(int signal) {
    logf("SIGNAL! INT --> shutdown");
    mud_data.reboot_type=REBOOT_SHUTDOWN;
    mud_data.reboot_pulse=PULSE_REBOOT;
    mud_data.reboot_timer=5;
    strcpy(mud_data.reboot_by,"the operating system");

    send_to_all_char("}rSHUTDOWN instructions received from operating system.{x\n\r");
}

int main( int argc, char **argv )
{
    struct timeval now_time;
    int i;
    char args[MSL];

    /*
     * Memory debugging if needed.
     */
#if defined(MALLOC_DEBUG)
    malloc_debug( 2 );
#endif

    args[0]=0;
    for (i=0;i<argc;i++) {
        strcat(args,argv[i]);
        strcat(args," ");
    }
    logf("[0] INIT: Fatal dimensions started as %s",args);

    /*
     * Init MUD_status
     */
    InitMudStatus("init",argc,argv);	// done in fd_conf.c
    gettimeofday( &now_time, NULL );
    current_time 	= (time_t) now_time.tv_sec;
    mud_data.boottime=now_time.tv_sec;
    mud_data.boot_time=str_dup(ctime(&current_time));

#ifdef THREADED_DNS
    rdns_cache_set_ttl(12*60*60);	// twelve hours is moooore than enough
#endif

    /*
     * MobProg & ObjProg Initialisation
     */
    tcl_init();

    /*
     * Reserve one channel for our use.
     */
    if ( ( fpReserve = fopen( NULL_FILE, "r" ) ) == NULL )
    {
        logf("[0] %s: %s",NULL_FILE,ERROR);
        exit( 1 );
    }

    if (mud_data.copyover)
        copyover_recover_mud();

    /*
     * Run the game.
     */
#ifdef UNIX
#ifndef NO_INET4
    if (mud_data.control4_mud==0)
        mud_data.control4_mud  = init_socket( mud_data.mud_port,4 );
    {
        int tos;
        tos=0x10; // LOW DELAY
        setsockopt(mud_data.control4_mud,IPPROTO_IP,IP_TOS, &tos, sizeof(tos));
    }
#ifdef HAS_HTTP
    if (mud_data.control4_http==0)
        mud_data.control4_http = init_socket( mud_data.http_port,4 );
#endif
#ifdef HAS_POP3
    if (mud_data.control4_pop3==0)
        mud_data.control4_pop3 = init_socket( mud_data.pop3_port,4 );
#endif
#endif
#ifdef INET6
    if (mud_data.control6_mud==0)
        mud_data.control6_mud  = init_socket( mud_data.mud_port,6 );
#ifdef HAS_HTTP
    if (mud_data.control6_http==0)
        mud_data.control6_http = init_socket( mud_data.http_port,6 );
#endif
#ifdef HAS_POP3
    if (mud_data.control6_pop3==0)
        mud_data.control6_pop3 = init_socket( mud_data.pop3_port,6 );
#endif
#endif

    boot_db( );

#ifdef I3
    I3_main(FALSE);
#endif

    logf("[0] INIT: Fatal Dimensions mud server is rocking on port %d.",
         mud_data.mud_port );
#ifdef HAS_HTTP
    logf("[0] INIT: Fatal Dimensions HTTP server is rocking on port %d.",
         mud_data.http_port);
#endif
#ifdef HAS_POP3
    logf("[0] INIT: Fatal Dimensions POP3 server is rocking on port %d.",
         mud_data.pop3_port);
#endif

    if (mud_data.copyover)
        copyover_recover_players();

    signal(SIGHUP,sig_hup);
    signal(SIGTERM,sig_term);
    signal(SIGINT,sig_int);

    if (mud_data.nogame==FALSE)
        game_loop_unix( );

#ifdef I3
    if (mud_data.reboot_type==REBOOT_REBOOT)
        I3_shutdown(275);
    if (mud_data.reboot_type==REBOOT_SHUTDOWN)
        I3_shutdown(0);
#endif
#ifndef NO_INET4
    close (mud_data.control4_mud);
#ifdef HAS_HTTP
    close (mud_data.control4_http);
#endif
#ifdef HAS_POP3
    close (mud_data.control4_pop3);
#endif
#endif
#ifdef INET6
    close (mud_data.control6_mud);
#ifdef HAS_HTTP
    close (mud_data.control6_http);
#endif
#ifdef HAS_POP3
    close (mud_data.control6_pop3);
#endif
#endif	// I3

#endif	// UNIX

    /*
     * That's all, folks.
     */
    logf( "[0] Normal termination of game." );
    exit( 0 );
    return 0;
}





void nanny_set_race(CHAR_DATA *ch,int race)
{
    int i;

    ch->race=race;
    for (i = 0; i < MAX_STATS; i++)
        ch->perm_stat[i] = pc_race_table[race].stats[i];
    STR_OR_STR(ch->strbit_affected_by,	race_table[race].strbit_eff,MAX_FLAGS);
    STR_OR_STR(ch->strbit_imm_flags,	race_table[race].strbit_imm,MAX_FLAGS);
    STR_OR_STR(ch->strbit_res_flags,	race_table[race].strbit_res,MAX_FLAGS);
    STR_OR_STR(ch->strbit_vuln_flags,	race_table[race].strbit_vuln,MAX_FLAGS);
    ch->form	= race_table[race].form;
    ch->parts	= race_table[race].parts;
    ch->size	= pc_race_table[race].size;
}

void nanny_calc_points(CHAR_DATA *ch)
{
    ch->pcdata->points=
            class_table[ch->class].points+
            pc_race_table[ch->race].points;
}

void nanny_reset_skills(CHAR_DATA *ch)
{
    int i;

    //for(i=0;i<MAX_SKILL;i++) ch->pcdata->learned[i]=0;
    bzero(ch->pcdata->skill,sizeof(ch->pcdata->skill));
    //for(i=0;i<MAX_GROUP;i++) ch->pcdata->group_known[i]=FALSE;
    bzero(ch->pcdata->group_known,sizeof(ch->pcdata->group_known));

    for (i = 0; i < MAX_BONUS_SKILLS; i++)
    {
        if (pc_race_table[ch->race].skills[i] == NULL) break;
        group_add(ch,pc_race_table[ch->race].skills[i],FALSE);
    }

    group_add(ch,"rom basics",FALSE);
    group_add(ch,class_table[ch->class].base_group,TRUE);
    ch->pcdata->skill[gsn_recall].learned=50;
    group_add(ch,weapon_table[ch->gen_data->weapon].name,FALSE);
    ch->pcdata->skill[*weapon_table[ch->gen_data->weapon].gsn].learned=40;

    gendata_clear(ch);
    gendata_group_add(ch,class_table[ch->class].default_group,TRUE);
}

static void nanny_to_email(CHAR_DATA *ch)
{
    int restart=0;
    /* Check if customization is okay */
    if (ch->gen_data->points_chosen<class_table[ch->class].needed_points) {
        sprintf_to_char(ch,"\n\r"
                           "You need to spend at least %d creation points.\n\r"
                           "Press enter to return to creation.\n\r",
                        ch->pcdata->points+class_table[ch->class].needed_points);
        return;
    }

    gendata_to_pcdata(ch);
    free_gen_data(ch->gen_data);
    ch->gen_data=NULL;

    GetCharProperty(ch,PROPERTY_INT,"restart",&restart);
    if (restart!=0) {
        show_help(ch,"motd");
        ch->desc->connected = CON_READ_MOTD;
    } else {
        write_to_buffer(ch->desc,"\n\rPlease enter your email address, prepend a @ to keep it hidden from mortals.\n\r",0);
        write_to_buffer(ch->desc,"This has to be valid if you ever want us to reset your password.\n\r",0);
        write_to_buffer(ch->desc,"(ENTER for none): ",0);
        ch->desc->connected = CON_GET_EMAIL;
    }
}

#ifdef HAS_ALTS
void nanny_showalts(CHAR_DATA *ch) {
    char *p,*q;
    int number=2;
    char names[MSL];

    send_to_char("\n\rList of your alts:\n\r",ch);
    sprintf_to_char(ch,"%2d. %-15s",1,ch->name);

    strcpy(names,ch->pcdata->alts);
    p=names;
    q=strchr(p,' ');
    while (q) {
        p[0]=UPPER(p[0]);
        q[0]=0;
        sprintf_to_char(ch,"%2d. %-15s",number,p);
        q[0]=' ';
        p=q+1;
        q=strchr(p,' ');
        number++;
        if (number%4==0)
            send_to_char("\n\r",ch);
    }
    p[0]=UPPER(p[0]);
    sprintf_to_char(ch,"%2d. %-15s",number++,p);
    send_to_char("\n\r",ch);

    sprintf_to_char(ch,"\n\rChoose your character (1..%d): ",number-1);
}
#endif

void init_char_descriptor(DESCRIPTOR_DATA *d,CHAR_DATA *ch) {
    if (d->pueblo)
        STR_SET_BIT(ch->strbit_act,PLR_PUEBLO);
    else
        STR_REMOVE_BIT(ch->strbit_act,PLR_PUEBLO);
    if (d->mxp)
        STR_SET_BIT(ch->strbit_act,PLR_MXP);
    else
        STR_REMOVE_BIT(ch->strbit_act,PLR_MXP);
#ifdef HAS_MCCP
    if (d->mccp)
        STR_SET_BIT(ch->strbit_act,PLR_MCCP);
    else
        STR_REMOVE_BIT(ch->strbit_act,PLR_MCCP);
    if (d->mccp2)
        STR_SET_BIT(ch->strbit_act,PLR_MCCP2);
    else
        STR_REMOVE_BIT(ch->strbit_act,PLR_MCCP2);
#endif
}

/*
 * Deal with sockets that haven't logged in yet.
 */
void nanny( DESCRIPTOR_DATA *d, char *argument )
{
    DESCRIPTOR_DATA *d_old, *d_next;
    char buf[MAX_STRING_LENGTH];
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *ch;
    char *pwdnew;
    char *p;
    int iClass,race,i;
    bool fOld;
    char s[MAX_STRING_LENGTH];

    while ( isspace(*argument) )
        argument++;

    ch = d->character;

    switch ( d->connected )
    {

    default:
        bugf( "Nanny: bad d->connected %d.", d->connected );
        close_socket( d );
        return;

    case CON_GET_NAME:
        if ( argument[0] == '\0' )
        {
            logf( "[%d] Denying access to %s (no name specified)",
                  d->descriptor,dns_gethostname(d));
            wiznet(WIZ_LOGINS,0,NULL,NULL,
                   "Denying access to %s (no name specified)",
                   dns_gethostname(d));
            close_socket( d );
            return;
        }

        if (strncmp(argument,"GET ",4)==0) {
            logf( "[%d] Denying access to %s (webbrowser)",
                  d->descriptor,dns_gethostname(d));
            wiznet(WIZ_LOGINS,0,NULL,NULL,
                   "Denying access to %s (webbrowser)",
                   dns_gethostname(d));
            close_socket( d );
            return;
        }

        if (strncmp(argument,"PUEBLOCLIENT",12)==0) {
            d->pueblo=TRUE;
            logf( "[%d] %s is using Pueblo",d->descriptor,dns_gethostname(d));
            wiznet(WIZ_TELNET,0,NULL,NULL,
                   "%s is using Pueblo",
                   dns_gethostname(d));
            return;
        }

        argument[0] = UPPER(argument[0]);
        if ( !check_parse_name( argument ) ) {
            logf("[%d] Denying access to %s@%s (invalid name).",
                 d->descriptor, argument, dns_gethostname(d));
            wiznet(WIZ_LOGINS,0,NULL,NULL,
                   "Denying access to %s@%s (invalid name).",
                   argument,dns_gethostname(d));
            write_to_buffer( d, "Illegal name, try another.\n\rName: ", 0 );
            return;
        }

        fOld = load_char_obj( d, argument );
        ch   = d->character;
        init_char_descriptor(d,ch);

        if (STR_IS_SET(ch->strbit_act, PLR_DENY)) {
            logf( "[%d] Denying access to %s@%s (player deny).",
                  d->descriptor,argument, dns_gethostname(d));
            wiznet(WIZ_LOGINS,0,NULL,NULL,
                   "Denying access to %s@%s (player deny).",
                   argument,dns_gethostname(d));
            write_to_buffer( d, "You are denied access.\n\r", 0 );
            close_socket( d );
            mud_data.banplayer_violations++;
            return;
        }

        if (check_lockout(d->ipv6,d->Host,TRUE) ||
                checkban_siteban(dns_gethostname(d))) {
            logf( "[%d] Denying access to %s@%s (ban site).",
                  d->descriptor,ch->name,dns_gethostname(d));
            wiznet(WIZ_LOGINS,0,NULL,NULL,
                   "Denying access to %s@%s (ban site).",
                   ch->name,dns_gethostname(d));
            write_to_buffer(d,"Your site has been banned from this mud.\n\r",0);
            close_socket(d);
            mud_data.bansite_violations++;
            return;
        }

        if ( check_reconnect( d, argument, FALSE ) )
        {
            fOld = TRUE;
        }
        else
        {
            if ( mud_data.wizlock && !IS_IMMORTAL(ch))
            {
                logf( "[%d] Denying access to %s@%s (wizlock).",
                      d->descriptor, ch->name,dns_gethostname(d));
                wiznet(WIZ_LOGINS,0,NULL,NULL,
                       "Denying access to %s@%s (wizlock).",
                       ch->name,dns_gethostname(d));
                write_to_buffer( d, "The game is wizlocked.\n\r", 0 );
                close_socket( d );
                mud_data.wizlock_violations++;
                return;
            }

            if ( mud_data.reboot_type!=REBOOT_NONE )
            {
                logf( "[%d] Denying access to %s@%s (%s).",
                      d->descriptor,ch->name,dns_gethostname(d),
                      reboot_state());
                wiznet(WIZ_LOGINS,0,NULL,NULL,
                       "Denying access to %s@%s (%s).",
                       ch->name,dns_gethostname(d),
                       reboot_state());

                sprintf(buf,"The mud is about to %s in %d minute%s.\n\r",
                        reboot_state(),
                        mud_data.reboot_timer/2,
                        mud_data.reboot_timer!=2?"s":"");
                write_to_buffer( d, buf, 0);
                if(!IS_IMMORTAL(ch))
                {
                    close_socket( d );
                    return;
                }
            }
        }

        if ( fOld )
        {
            /* Old player */
            check_hack(d,ch->name);
            write_to_buffer( d, "Password: ", 0 );
            write_to_buffer( d, echo_will, 0 );
            d->connected = CON_GET_OLD_PASSWORD;
            return;
        } else {
            /* New player */
            if (mud_data.newlock)
            {
                logf( "[%d] Denying access to %s@%s (newlock).",
                      d->descriptor, ch->name,dns_gethostname(d));
                wiznet(WIZ_LOGINS,0,NULL,NULL,
                       "Denying access to %s@%s (newlock).",
                       ch->name,dns_gethostname(d));
                write_to_buffer( d, "The game is newlocked.\n\r", 0 );
                close_socket( d );
                mud_data.newlock_violations++;
                return;
            }

            sprintf( buf, "Did I get that right, %s [Yes/No]? ", argument );
            write_to_buffer( d, buf, 0 );
            d->connected = CON_CONFIRM_NEW_NAME;
            return;
        }
        break;

    case CON_GET_OLD_PASSWORD:
#ifdef UNIX
        write_to_buffer( d, "\n\r", 2 );
#endif

        if (strcmp(crypt(argument,ch->pcdata->pwd),ch->pcdata->pwd)) {
            logf("[%d] Denying access to %s@%s (bad password).",
                 d->descriptor,ch->name,dns_gethostname(d));
            wiznet(WIZ_LOGINS,0,NULL,NULL,
                   "Denying access to %s@%s (bad password).",
                   ch->name,dns_gethostname(d));
            write_to_buffer( d, "Wrong password.\n\r", 0 );

            if (d->character->pet) {
                CHAR_DATA *pet=d->character->pet;

                char_to_room(pet,get_room_index( ROOM_VNUM_LIMBO));
                stop_follower(pet);
                extract_char(pet,TRUE);
            }
            check_lockout(d->ipv6,d->Host,FALSE);
            close_socket( d );
            return;
        }

        if (checkban_immortalonly(dns_gethostname(d)) &&
                !IS_IMMORTAL(d->character)) {
            logf("[%d] Denying access to %s@%s (non-immortalban).",
                 d->descriptor,ch->name,dns_gethostname(d));
            wiznet(WIZ_LOGINS,0,NULL,NULL,
                   "Denying access to %s@%s (non-immortalban).",
                   ch->name,dns_gethostname(d));
            write_to_buffer(d,"Access from this site is not allowed for non-immortals.\n\r", 0 );

            if (d->character->pet) {
                CHAR_DATA *pet=d->character->pet;

                char_to_room(pet,get_room_index(ROOM_VNUM_LIMBO));
                stop_follower(pet);
                extract_char(pet,TRUE);
            }
            mud_data.bansite_violations++;
            close_socket( d );
            return;
        }

        write_to_buffer( d, echo_wont, 0 );

        logf( "[%d] %s@%s has connected.",
              d->descriptor, ch->name,dns_gethostname(d));
        wiznet(WIZ_SITES,0,NULL,NULL,
               "%s@%s has connected.",
               ch->name,dns_gethostname(d));

#ifdef HAS_ALTS
        if (ch->pcdata->alts[0]) {
            nanny_showalts(d->character);
            d->connected = CON_CHOOSE_ALT;
            break;
        }
#endif

        if (check_playing(d,ch->name))
            return;

        if ( check_reconnect( d, ch->name, TRUE ) )
            return;

        if ( IS_IMMORTAL(ch) ) {
            show_help(ch,"imotd");
            d->connected = CON_READ_IMOTD;
        } else {
            show_help(ch,"motd");
            d->connected=CON_READ_MOTD;
        }
        break;

#ifdef HAS_ALTS
    case CON_CHOOSE_ALT:
    {
        CHAR_DATA *oldch;
        char buf[MIL],*names;

        if (is_number(argument)) {
            int number=atoi(argument);

            if (number<=1) {
                if ( check_reconnect( d, ch->name, TRUE ) )
                    return;

                if ( IS_IMMORTAL(ch) ) {
                    show_help(ch,"imotd");
                    d->connected = CON_READ_IMOTD;
                } else {
                    show_help(ch,"motd");
                    d->connected=CON_READ_MOTD;
                }
                return;
            }

            number-=2;
            names=d->character->pcdata->alts;
            names=one_argument(names,buf);
            while (number && buf[0]) {
                number--;
                names=one_argument(names,buf);
            }

            if (buf[0]==0) {
                send_to_char("Invalid choice.\n\r",ch);
                nanny_showalts(d->character);
                d->connected = CON_CHOOSE_ALT;
                return;
            }

            buf[0] = UPPER(buf[0]);

            d->uberalt = d->character;
            fOld = load_char_obj( d, buf );
            ch   = d->character;

            if (check_playing(d,ch->name))
                return;

            if ( check_reconnect( d, buf, TRUE ) )
                return;

            if ( IS_IMMORTAL(ch) ) {
                show_help(ch,"imotd");
                d->connected = CON_READ_IMOTD;
            } else {
                show_help(ch,"motd");
                d->connected=CON_READ_MOTD;
            }
        }
        break;
    }
#endif

        /* RT code for breaking link */

    case CON_BREAK_CONNECT:
        switch( *argument )
        {
        case 'y' : case 'Y':
            for ( d_old = descriptor_list; d_old != NULL; d_old = d_next )
            {
                d_next = d_old->next;
                if (d_old == d || d_old->character == NULL)
                    continue;

                if (str_cmp(ch->name,d_old->original ?
                            d_old->original->name : d_old->character->name))
                    continue;

                close_socket(d_old);
            }
            if (check_reconnect(d,ch->name,TRUE))
                return;
            write_to_buffer(d,"Reconnect attempt failed.\n\rName: ",0);
            if ( d->character != NULL )
            {
                free_char( d->character );
                d->character = NULL;
            }
            d->connected = CON_GET_NAME;
            break;

        case 'n' : case 'N':
            write_to_buffer(d,"Name: ",0);
            if ( d->character != NULL )
            {
                free_char( d->character );
                d->character = NULL;
            }
            d->connected = CON_GET_NAME;
            break;

        default:
            write_to_buffer(d,"Please type Y or N? ",0);
            break;
        }
        break;

    case CON_CONFIRM_NEW_NAME:
        switch ( *argument )
        {
        case 'y': case 'Y':
            if (checkban_newbie(dns_gethostname(d)))
            {
                logf( "[%d] Denying access to %s@%s (newbie ban).",
                      d->descriptor, ch->name,dns_gethostname(d));
                wiznet(WIZ_LOGINS,0,NULL,NULL,
                       "Denying access to %s@%s (newbie ban).",
                       ch->name,dns_gethostname(d));
                write_to_buffer(d,
                                "New players are not allowed from your site.\n\r",0);
                close_socket(d);
                mud_data.bannew_violations++;
                return;
            }

            sprintf( buf, "New character.\n\rGive me a password for %s: %s",
                     ch->name, echo_will );
            write_to_buffer( d, buf, 0 );
            d->connected = CON_GET_NEW_PASSWORD;
            mud_data.players_new++;

            /* Newbie alert on wiznet */
            logf( "[%d] %s@%s new player.",
                  d->descriptor, ch->name,dns_gethostname(d));
            wiznet(WIZ_NEWBIE,0,ch,NULL,"Newbie alert! $N sighted.");

            break;

        case 'n': case 'N':
            write_to_buffer( d, "Ok, what IS it, then? ", 0 );
            free_char( d->character );
            d->character = NULL;
            d->connected = CON_GET_NAME;
            break;

        default:
            write_to_buffer( d, "Please type Yes or No? ", 0 );
            break;
        }
        break;

    case CON_GET_NEW_PASSWORD:
#ifdef UNIX
        write_to_buffer( d, "\n\r", 2 );
#endif

        if ( strlen(argument) < 5 )
        {
            write_to_buffer( d,
                             "Password must be at least five characters long.\n\rPassword: ",
                             0 );
            return;
        }

        pwdnew = crypt( argument, ch->name );
        for ( p = pwdnew; *p != '\0'; p++ )
        {
            if ( *p == '~' )
            {
                write_to_buffer( d,
                                 "New password not acceptable, try again.\n\rPassword: ",
                                 0 );
                return;
            }
        }

        free_string( ch->pcdata->pwd );
        ch->pcdata->pwd	= str_dup( pwdnew );
        write_to_buffer( d, "Please retype password: ", 0 );
        d->connected = CON_CONFIRM_NEW_PASSWORD;
        break;

    case CON_CONFIRM_NEW_PASSWORD:
#ifdef UNIX
        write_to_buffer( d, "\n\r", 2 );
#endif

        if ( strcmp( crypt( argument, ch->pcdata->pwd ), ch->pcdata->pwd ) )
        {
            write_to_buffer( d, "Passwords don't match.\n\rRetype password: ",
                             0 );
            d->connected = CON_GET_NEW_PASSWORD;
            return;
        }

        write_to_buffer( d, echo_wont, 0 );

        /* Initialise Creation */
        nanny_set_race(ch,1);	/* Human */
        ch->class=3;	/* Warrior */
        ch->Sex=ch->pcdata->true_sex=SEX_FEMALE;
        ch->alignment=0; /* Neutral */
        ch->gen_data=new_gen_data();
        ch->gen_data->weapon=weapon_lookup(class_table[ch->class].default_weapon);
        nanny_calc_points(ch);
        nanny_reset_skills(ch);
        screen_creation(ch,NULL);

        d->connected = CON_SCREEN_CREATION;
        break;
    case CON_SCREEN_CREATION:
        /* Race part */
        race=race_lookup(argument);
        if(race!=0 && race<MAX_PC_RACE)
        {
            nanny_set_race(ch,race);
            nanny_calc_points(ch);
            nanny_reset_skills(ch);
            sprintf(buf,"Race set to '%s'. ",race_table[race].name);
            screen_creation(ch,buf);
            return;
        }

        /* Class Part */
        iClass=class_lookup(argument);
        if(iClass!=-1 && class_table[iClass].spec_off<0)
        {
            ch->class=iClass;
            ch->gen_data->weapon=weapon_lookup(class_table[ch->class].default_weapon);
            nanny_calc_points(ch);
            nanny_reset_skills(ch);
            sprintf(buf,"Class set to '%s'. ",class_table[iClass].name);
            screen_creation(ch,buf);
            return;
        }

        /* Choose weapon */
        i=weapon_lookup(argument);
        if(i>=0 && skill_rating(*weapon_table[i].gsn,ch)>0)
        {
            ch->gen_data->weapon=i;
            nanny_reset_skills(ch);
            sprintf(buf,"Weapon '%s' selected. ",weapon_table[i].name);
            screen_creation(ch,buf);
            return;
        }

        argument=one_argument(argument,arg);
        switch(which_keyword(arg,"male","female","good","neutral",
                             "evil","done","ansi","color","customize","race",
                             "base","class","sex","alignment","weapons","options",
                             "specialize", "help", NULL)) {
        case -1:
            screen_creation(ch,NULL);
            return;
        case 0:
            sprintf(buf,"Unknown command '%s'. ",arg);
            screen_creation(ch,buf);
            return;
        case 1:
            ch->Sex=ch->pcdata->true_sex=SEX_MALE;
            screen_creation(ch,"Sex set to male. ");
            return;
        case 2:
            ch->Sex=ch->pcdata->true_sex=SEX_FEMALE;
            screen_creation(ch,"Sex set to female. ");
            return;
        case 3:
            ch->alignment=750;
            screen_creation(ch,"Alignment is good. ");
            return;
        case 4:
            ch->alignment=0;
            screen_creation(ch,"Alignment is neutral. ");
            return;
        case 5:
            ch->alignment=-750;
            screen_creation(ch,"Alignment is evil. ");
            return;
        case 6:
            nanny_to_email(ch);
            return;
        case 7:
        case 8:
            STR_TOGGLE_BIT(ch->strbit_act,PLR_COLOUR);
            screen_creation(ch,STR_IS_SET(ch->strbit_act,PLR_COLOUR)?"Ansi color on. ":"Ansi color off. ");
            return;
        case 9:
            ch->gen_data->page=1;
            d->connected = CON_SCREEN_CUSTOMIZE;

            if(!ch->gen_data->customize)
            {
                ch->gen_data->customize=TRUE;
                show_help(ch,"customize_help");
                send_to_char("\n\r[Press enter to continue] ",ch);
                return;
            }

            screen_customize(ch,NULL);
            return;
        case 10: /* race */
            race=race_lookup(argument);
            if(race!=0 && race<MAX_PC_RACE)
            {
                nanny_set_race(ch,race);
                nanny_calc_points(ch);
                nanny_reset_skills(ch);
                sprintf(buf,"Race set to '%s'. ",race_table[race].name);
                screen_creation(ch,buf);
                return;
            }
            sprintf(buf,"Unknown race '%s'. ",argument);
            screen_creation(ch,buf);
            return;
        case 11: /*base*/
        case 12: /*class*/
            iClass=class_lookup(argument);
            if(iClass!=-1 && class_table[iClass].spec_off<0)
            {
                ch->class=iClass;
                ch->gen_data->weapon=weapon_lookup(class_table[ch->class].default_weapon);
                nanny_calc_points(ch);
                nanny_reset_skills(ch);
                sprintf(buf,"Class set to '%s'. ",class_table[iClass].name);
                screen_creation(ch,buf);
                return;
            }
            sprintf(buf,"Unknown class '%s'. ",argument);
            screen_creation(ch,buf);
            return;
        case 13: /* sex */
            switch(which_keyword(argument,"male","female",NULL)) {
            case -1:
            case 0:
                screen_creation(ch,"Choose male or female. ");
                return;
            case 1:
                ch->Sex=ch->pcdata->true_sex=SEX_MALE;
                screen_creation(ch,"Sex set to male. ");
                return;
            case 2:
                ch->Sex=ch->pcdata->true_sex=SEX_FEMALE;
                screen_creation(ch,"Sex set to female. ");
                return;
            }
            break;
        case 14: /* alignment */
            switch(which_keyword(argument,"good","neurtal","evil",NULL)) {
            case -1:
            case 0:
                screen_creation(ch,"Choose good, neutral or evil. ");
                return;
            case 1:
                ch->alignment=750;
                screen_creation(ch,"Alignment is good. ");
                return;
            case 2:
                ch->alignment=0;
                screen_creation(ch,"Alignment is neutral. ");
                return;
            case 3:
                ch->alignment=-750;
                screen_creation(ch,"Alignment is evil. ");
                return;
            }
            break;
        case 15: /* weapons */
            i=weapon_lookup(argument);
            if(i>=0 && skill_rating(*weapon_table[i].gsn,ch)>0)
            {
                ch->gen_data->weapon=i;
                nanny_reset_skills(ch);
                sprintf(buf,"Weapon '%s' selected. ",weapon_table[i].name);
                screen_creation(ch,buf);
                return;
            }
            sprintf(buf,"Unknown weapon '%s'. ",argument);
            screen_creation(ch,buf);
            return;
        case 16: /* Options */
            switch(which_keyword(argument,"ansi","colorl",NULL)) {
            case -1:
            case 0:
                sprintf(buf,"Unknown option '%s'. ",argument);
                screen_creation(ch,buf);
                return;
            case 1:
            case 2:
                STR_TOGGLE_BIT(ch->strbit_act,PLR_COLOUR);
                screen_creation(ch,STR_IS_SET(ch->strbit_act,PLR_COLOUR)?"Ansi color on. ":"Ansi color off");
                return;
            }
            break;
        case 17: /* specialize */
            screen_specialize(ch,NULL);
            ch->desc->connected=CON_SCREEN_SPECIALIZE;
            return;
        case 18: /* help */
            if (argument[0]==0)
                show_help(ch,"creation_help");
            else
                show_help(ch,argument);
            send_to_char("\n\r[Press enter to continue] ",ch);
            return;
        }

        screen_creation(ch,NULL);

        break;
    case CON_SCREEN_CUSTOMIZE:
        argument=one_argument(argument,arg);
        switch(which_keyword(arg,"creation","up","down",
                             "add","drop","remove","done","reset","specialize",
                             "info","help",NULL)) {
        case -1:
            screen_customize(ch,NULL);
            return;
        case 0:
            sprintf(buf,"Unknown command '%s'. ",arg);
            screen_customize(ch,buf);
            return;
        case 1:
            screen_creation(ch,NULL);
            d->connected = CON_SCREEN_CREATION;
            return;
        case 2:
            if(ch->gen_data->page>1) ch->gen_data->page--;
            else
            {
                screen_customize(ch,"You can't go up. ");
                return;
            }
            break;
        case 3:
            if(ch->gen_data->page<ch->gen_data->pages) ch->gen_data->page++;
            else
            {
                screen_customize(ch,"You can't go down. ");
                return;
            }
            break;
        case 4: /* add */
            i=group_lookup(argument);
            if(i!=-1)
            {
                if(ch->gen_data->group_chosen[i]
                        || ch->gen_data->auto_group[i]
                        || ch->pcdata->group_known[i])
                {
                    sprintf(buf,"You already have group '%s'. ",group_name(i,ch));
                    screen_customize(ch,buf);
                }
                else if(!may_choose_group(ch,i))
                {
                    sprintf(buf,"Group '%s' is not available. ",group_name(i,ch));
                    screen_customize(ch,buf);
                }
                else
                {
                    gendata_group_add(ch,group_name(i,ch),TRUE);
                    sprintf(buf,"Group '%s' added. ",group_name(i,ch));
                    screen_customize(ch,buf);
                }
                return;
            }
            i=skill_lookup(argument);
            if(i!=-1)
            {
                if(ch->gen_data->skill_chosen[i]
                        || ch->gen_data->auto_skill[i]
                        || has_gained_skill(ch,i))
                {
                    sprintf(buf,"You already have skill '%s'. ",skill_name(i,ch));
                    screen_customize(ch,buf);
                }
                else if(!may_choose_skill(ch,i))
                {
                    sprintf(buf,"Skill '%s' is not available. ",skill_name(i,ch));
                    screen_customize(ch,buf);
                }
                else
                {
                    gendata_group_add(ch,skill_name(i,ch),TRUE);
                    sprintf(buf,"Skill '%s' added. ",skill_name(i,ch));
                    screen_customize(ch,buf);
                }
                return;
            }

            sprintf(buf,"Unknown skill or group '%s'. ",argument);
            screen_customize(ch,buf);
            return;
        case 5: /* drop */
        case 6: /* remove */
            i=group_lookup(argument);
            if(i!=-1 && group_rating(i,ch)>0)
            {
                if(!ch->gen_data->group_chosen[i])
                {
                    sprintf(buf,"You can't remove group '%s'. ",group_name(i,ch));
                    screen_customize(ch,buf);
                }
                else
                {
                    gendata_group_remove(ch,group_name(i,ch));
                    sprintf(buf,"Group '%s' removed. ",group_name(i,ch));
                    screen_customize(ch,buf);
                }
                return;
            }
            i=skill_lookup(argument);
            if(i!=-1)
            {
                if(!ch->gen_data->skill_chosen[i])
                {
                    sprintf(buf,"You can't remove skill '%s'. ",skill_name(i,ch));
                    screen_customize(ch,buf);
                }
                else
                {
                    gendata_group_remove(ch,skill_name(i,ch));
                    sprintf(buf,"Skill '%s' removed. ",skill_name(i,ch));
                    screen_customize(ch,buf);
                }
                return;
            }

            sprintf(buf,"Unknown skill or group '%s'. ",argument);
            screen_customize(ch,buf);
            return;
        case 7: /* done */
            nanny_to_email(ch);
            return;
        case 8:/* reset */
            nanny_reset_skills(ch);
            screen_customize(ch,"Skills reset. ");
            return;
        case 9: /* specialize */
            screen_specialize(ch,NULL);
            ch->desc->connected=CON_SCREEN_SPECIALIZE;
            return;
        case 10: /* info */
            do_function(ch,&do_groups,argument);
            send_to_char("\n\r[Press enter or give info command to continue] ",ch);
            return;
        case 11: /* help */
            if (argument[0]==0)
                show_help(ch,"nospecialize_help");
            else
                show_help(ch,argument);
            send_to_char("\n\r[Press enter to continue] ",ch);
            return;
        }

        screen_customize(ch,NULL);

        break;
    case CON_SCREEN_SPECIALIZE:
        argument=one_argument(argument,arg);
        switch(which_keyword(arg,"creation","customize","done","help",NULL)) {
        case -1:
            break;
        case 0:
            sprintf(buf,"Unknown command '%s'. ",argument);
            screen_specialize(ch,buf);
            return;
        case 1: /* sceation */
            screen_creation(ch,NULL);
            ch->desc->connected=CON_SCREEN_CREATION;
            return;
        case 2: /* customize */
            ch->gen_data->page=1;
            d->connected = CON_SCREEN_CUSTOMIZE;

            if(!ch->gen_data->customize)
            {
                ch->gen_data->customize=TRUE;
                show_help(ch,"customize_help");
                send_to_char("\n\r[Press enter to continue] ",ch);
                return;
            }

            screen_customize(ch,NULL);
            return;
        case 3: /* done */
            nanny_to_email(ch);
            return;
        case 4: /* help */
            if (argument[0]==0)
                show_help(ch,"nospecialize_help");
            else
                show_help(ch,argument);
            send_to_char("\n\r[Press enter to continue] ",ch);
            return;
        };
        screen_specialize(ch,NULL);
        break;
#ifdef notdef
    case CON_GET_SPEC_CLASS:
        iClass = class_lookup(argument);

        if ( iClass == -1 )
        {
            write_to_buffer( d,
                             "That's not a specialization or class.\n\r"
                             "What IS your specialization? ", 0 );
            return;
        }

        if ( class_table[iClass].spec_off == -1 )
        {
            write_to_buffer( d,
                             "That's not a specialization!\n\r"
                             "What IS your specialization? ",0);
            return;
        }

        if ( class_table[iClass].spec_off != ch->class )
        {
            write_to_buffer( d,
                             "You may not specialize to that specialization!\n\r"
                             "What IS your specialization? ",0);
            return;
        }

        ch->class = iClass;
        ch->pcdata->points+=class_table[iClass].points;

        send_to_char("You have lost the following skills:\n\r",ch);
        list_lost_skills(ch);
        send_to_char("\n\r",ch);

#warning Classes: Add wiznet and normal specialization message here.
        write_to_buffer(d,"Do you wish to customize this character again?\n\r",0);
        write_to_buffer(d,"Customization takes time, but allows a wider range of skills and abilities.\n\r",0);
        write_to_buffer(d,"Customize (Y/N)? ",0);
        d->connected = CON_SPECIALIZE_CHOICE;

        break;
#else
#pragma message("Add new specialization code here.")
#endif

#ifdef notdef
    case CON_SPECIALIZE_CHOICE:
        write_to_buffer(d,"\n\r",2);
        switch ( argument[0] )
        {
        case 'y': case 'Y':
            ch->gen_data->points_chosen = ch->pcdata->points;
            show_help(ch,"group header");
            list_group_costs(ch);
            write_to_buffer(d,"You already have the following skills:\n\r",0);
            do_function(ch, &do_skills, "");
            show_help(ch,"menu choice");
            d->connected = CON_GEN_SPECGROUPS;
            break;
        case 'n': case 'N':
            group_add(ch,class_table[ch->class].default_group,TRUE);
            write_to_buffer(d,"Specialization complete.\n\r",0);
            ch->exp = exp_per_level(ch,ch->pcdata->points) * ch->level;
            char_to_room(ch,ch->was_in_room);
            ch->was_in_room=NULL;
            act("$n appears as a newly born specialist.",ch,NULL,NULL,TO_ROOM);
            d->connected = CON_PLAYING;
            check_mxp(d->character);
            break;
        default:
            write_to_buffer( d, "Please answer [Yes/No]? ", 0 );
            return;
        }
        break;
#else
#pragma message("Add new specialization code here.")
#endif

#ifdef notdef
    case CON_GEN_SPECGROUPS:
        send_to_char("\n\r",ch);
        if (!str_cmp(argument,"done"))
        {
            int basepoints=base_points(ch);

            if (ch->pcdata->points < basepoints+class_table[ch->class].needed_points )
            {
                sprintf(buf,
                        "You must take at least %d points of skills and"
                        "groups.\n\rPress enter to continue.\n\r",
                        basepoints+class_table[ch->class].needed_points);
                send_to_char(buf, ch);
                break;
            }

            sprintf(buf,"Creation points: %d\n\r",ch->pcdata->points);
            send_to_char(buf,ch);
            sprintf(buf,"Experience per level: %d\n\r",
                    exp_per_level(ch,ch->gen_data->points_chosen));
            free_gen_data(ch->gen_data);
            ch->gen_data = NULL;
            send_to_char(buf,ch);
            write_to_buffer( d, "\n\r", 2 );
            write_to_buffer( d, "Specialization complete.\n\r",0);
            ch->exp = exp_per_level(ch,ch->pcdata->points) * ch->level;
            char_to_room(ch,ch->was_in_room);
            ch->was_in_room=NULL;
            act("$n appears as a newly born specialist.",ch,NULL,NULL,TO_ROOM);
            d->connected = CON_PLAYING;
            check_mxp(d->character);
            break;
        }

        if (!parse_gen_groups(ch,argument))
            send_to_char(
                        "Choices are: list,learned,premise,add,drop,info,help and done.\n\r"
                        ,ch);

        show_help(ch,"menu choice");
        break;
#else
#pragma message("Rewrite specialization customize.")
#endif

    case CON_READ_IMOTD:
    case CON_READ_NEWBIEINFO:
        write_to_buffer(d,"\n\r",2);
        show_help(ch,"motd");
        d->connected = CON_READ_MOTD;
        break;

    case CON_READ_MOTD:
        if ( ch->pcdata == NULL || ch->pcdata->pwd[0] == '\0')
        {
            write_to_buffer( d, "Warning! Null password!\n\r",0 );
            write_to_buffer( d, "Please report old password with bug.\n\r",0);
            write_to_buffer( d,
                             "Type 'config password null <new password>' to fix.\n\r",0);
        }

        write_to_buffer( d,
                         "\n\rWelcome to Fatal Dimensions. Please do not feed the mobiles.\n\r",0);

        // add player to char_list
        ch->next	= char_list;
        char_list	= ch;

        // add player to player_list
        ch->next_player = player_list;
        player_list	= ch;

        d->connected	= CON_PLAYING;
        update_wiznet(TRUE,ch->strbit_wiznet);
        check_mxp(d->character);
        reset_char(ch);
        mud_data.players_total++;

        if (ch->clan)
            ch->clan->lastonline=time(NULL);

        if ( ch->level == 0 ) {
            int restart=0;
            OBJ_DATA *obj;
            GetCharProperty(ch,PROPERTY_INT,"restart",&restart);
            /* Fist time in the game */
            ch->perm_stat[class_table[ch->class].attr_prime] += 3;

            ch->level	= 1;
            ch->exp	= exp_per_level(ch,ch->pcdata->points);
            ch->hit	= ch->max_hit;
            ch->mana	= ch->max_mana;
            ch->move	= ch->max_move;
            ch->train	 = 6;
            ch->practice = 9;
            sprintf( buf, "the %s",
                     title_table [ch->class] [ch->level]
                    [show_sex(ch) == SEX_FEMALE ? 1 : 0] );
            set_title( ch, buf );
            STR_SET_BIT(ch->strbit_act,PLR_AUTOASSIST);
            STR_SET_BIT(ch->strbit_act,PLR_AUTOLOOT);
            STR_SET_BIT(ch->strbit_act,PLR_AUTOSAC);
            STR_SET_BIT(ch->strbit_act,PLR_AUTOGOLD);
            STR_SET_BIT(ch->strbit_act,PLR_AUTOSPLIT);
            STR_SET_BIT(ch->strbit_act,PLR_NOSUMMON);
            STR_SET_BIT(ch->strbit_comm,COMM_SHOW_EFFECTS);
            STR_SET_BIT(ch->strbit_comm,COMM_SHOW_ARMOR);
            STR_SET_BIT(ch->strbit_comm,COMM_ANNOUNCE);
            STR_SET_BIT(ch->strbit_comm,COMM_COMBINE);
            STR_SET_BIT(ch->strbit_comm,COMM_SHOW_WEIGHT);

            do_function (ch, &do_outfit,"");
            if (!restart)
                do_function (ch, &do_config,"prompt %h/%Hhp %m/%Mma %vmv | %e >");
            obj_to_char(create_object(get_obj_index(OBJ_VNUM_MAP),0),ch);
            obj=create_object(get_obj_index(OBJ_VNUM_LOOMOFTIME),0);
            obj_to_char(obj,ch);
            wear_obj(ch,obj,FALSE,FALSE);

            char_to_room( ch, get_room_index( ROOM_VNUM_SCHOOL ) );
            send_to_char("\n\r",ch);
            // set the note spool ot now;
            if (!restart)
                for (i=0;i<MAX_NOTES;i++)
                    ch->pcdata->last_notes[i]=current_time;
        } else if ( ch->in_room != NULL ) {
            int alt_room=ch->in_room->area->login_room;
            GetRoomProperty(ch->in_room,PROPERTY_INT,"login-room",&alt_room);
            if (alt_room!=0) {
                ROOM_DATA *new_room;
                new_room=get_room_index(alt_room);
                if (new_room) { ch->in_room=new_room; }
            }
            char_to_room( ch, ch->in_room );
        } else if ( IS_IMMORTAL(ch) ) {
            char_to_room( ch, get_room_index( ROOM_VNUM_CHAT ) );
        } else {
            char_to_room( ch, get_room_index( ROOM_VNUM_TEMPLE ) );
        }

        extract_owned_obj(ch->name,ch->id);
        extract_invalid_owned_obj_from_char(ch);

        mortalcouncil_update(ch);

        act( "$n has entered the game.", ch, NULL, NULL, TO_ROOM );
        if( IS_AFFECTED2( ch, EFF_CANTRIP ) )
            show_cantrip( NULL, ch, TO_ROOM );
        show_ool_cantrip(NULL,ch,TO_ROOM);
        look_room(ch,ch->in_room,TRUE);

        wiznet(WIZ_LOGINS,get_trust(ch),ch,NULL,
               "$N has left real life behind.");
        announce(ch,"$N has left real life behind.");

        if (ch->invis_level) {
            CHAR_DATA *player;
            sprintf(s,"You are wizi at level {r%d{x.\n\r",ch->invis_level);
            send_to_char(s,ch);
            sprintf(s,"Warning! $n is wizi at level {r%d{x.",ch->invis_level);
            for (player=player_list;player!=NULL;player=player->next_player) {
                if ( can_see(player,ch) &&
                     IS_IMMORTAL(player))
                    act(s,ch,NULL,player,TO_VICT);
            }
        }
        if (ch->incog_level) {
            CHAR_DATA *player;
            sprintf(s,"You are cloaked at level {r%d{x.\n\r",ch->incog_level);
            send_to_char(s,ch);
            sprintf(s,"Warning! $n is cloaked at level {r%d{x.",ch->incog_level);
            for (player=player_list;player!=NULL;player=player->next_player) {
                if ( can_see(player,ch) &&
                     IS_IMMORTAL(player))
                    act(s,ch,NULL,player,TO_VICT);
            }
        }

        if (ch->pet != NULL)
        {
            char_to_room(ch->pet,ch->in_room);
            act("$n has entered the game.",ch->pet,NULL,NULL,TO_ROOM);
            if( IS_AFFECTED2( ch->pet, EFF_CANTRIP ) )
                show_cantrip( NULL, ch->pet, TO_ROOM );
        }

        do_function(ch, &do_unread, "");
        break;
    case CON_GET_EMAIL:
        write_to_buffer(d,"\n\r",2);
        SetCharProperty(ch,PROPERTY_STRING,"email",argument);
        show_help(ch,"newbie info");
        d->connected=CON_READ_NEWBIEINFO;
        break;
    }

    return;
}



/*
 * Parse a name for acceptability.
 */
bool check_parse_name( char *name )
{
    DESCRIPTOR_DATA *d,*dnext;
    int count;

    /* check data read from config-file */
    if (checkbadname(name))
        return FALSE;

    /* check clans */
    if (get_clan_by_name(name)!=NULL)
        return FALSE;

    /*
     * Length restrictions.
     */

    if ( strlen(name) <  2 )
        return FALSE;

#ifdef UNIX
    if ( strlen(name) > 12 )
        return FALSE;
#endif

    /*
     * check names of people playing. Yes, this is necessary for multiple
     * newbies with the same name (thanks Saro)
     */
    if (descriptor_list) {
        count=0;
        for (d = descriptor_list; d != NULL; d = dnext) {
            dnext=d->next;
            if (d->connected!=CON_PLAYING&&d->character&&d->character->name
                    && d->character->name[0] && !str_cmp(d->character->name,name)) {
                count++;
                if (d->character->pet) {
                    CHAR_DATA *pet=d->character->pet;

                    char_to_room(pet,get_room_index( ROOM_VNUM_LIMBO));
                    stop_follower(pet);
                    extract_char(pet,TRUE);
                }
                close_socket(d);
            }
        }
        if (count) {
            wiznet(WIZ_LOGINS,0,NULL,NULL,"Double newbie alert (%s)",name);
            return FALSE;
        }
    }

    /*
     * fix to prevent users to use names with a $ in it (acts funny on wiznet)
     */
    {
        char *pc;

        while ((pc=strchr(name,'$')))
            pc[0]='+';
    }

    /*
     * Alphanumerics only.
     * Lock out IllIll twits.
     */
    {
        char *pc;
        bool fIll,adjcaps = FALSE,cleancaps = FALSE;
        int total_caps = 0;

        fIll = TRUE;
        for ( pc = name; *pc != '\0'; pc++ )
        {
            if ( !isalpha(*pc) )
                return FALSE;

            if ( isupper(*pc)) /* ugly anti-caps hack */
            {
                if (adjcaps)
                    cleancaps = TRUE;
                total_caps++;
                adjcaps = TRUE;
            }
            else
                adjcaps = FALSE;

            if ( LOWER(*pc) != 'i' && LOWER(*pc) != 'l' )
                fIll = FALSE;
        }

        if ( fIll )
            return FALSE;

        if (cleancaps || (total_caps > (strlen(name)) / 2 && strlen(name) < 3))
            return FALSE;
    }

    /*
     * Prevent players from naming themselves after mobs.
     */
    {
        extern MOB_INDEX_DATA *mob_index_hash[MAX_KEY_HASH];
        MOB_INDEX_DATA *pMobIndex;
        int iHash;

        for ( iHash = 0; iHash < MAX_KEY_HASH; iHash++ )
        {
            for ( pMobIndex  = mob_index_hash[iHash];
                  pMobIndex != NULL;
                  pMobIndex  = pMobIndex->next )
            {
                if ( is_exact_name( name, pMobIndex->player_name ) )
                    return FALSE;
            }
        }
    }

    return TRUE;
}



/*
 * Look for link-dead player to reconnect.
 */
bool check_reconnect( DESCRIPTOR_DATA *d, char *name, bool fConn )
{
    CHAR_DATA *ch;

    for ( ch = char_list; ch != NULL; ch = ch->next )
    {
        if ( !IS_NPC(ch)
             &&   (!fConn || ch->desc == NULL)
             &&   !str_cmp( d->character->name, ch->name ) )
        {
            if ( fConn == FALSE )
            {
                free_string( d->character->pcdata->pwd );
                d->character->pcdata->pwd = str_dup( ch->pcdata->pwd );
            }
            else
            {
                if (d->character->pet) {
                    CHAR_DATA *pet=d->character->pet;

                    char_to_room(pet,get_room_index( ROOM_VNUM_LIMBO));
                    stop_follower(pet);
                    extract_char(pet,TRUE);
                }
                STR_SET_BIT(d->character->strbit_act,PLR_QUITTING);
                free_char( d->character );
                d->character = ch;
                if (ch->clan) ch->clan->player=ch;
                ch->desc	 = d;
                ch->timer	 = 0;
                send_to_char("Reconnecting.\n\r",ch);
                if (ch->pcdata->missedtells==0)
                    send_to_char("No tells received.\n\r",ch);
                else
                    sprintf_to_char(ch,
                                    "Type '{Wreplay{x' to see your %d missed tell%s.\n\r",
                                    ch->pcdata->missedtells,
                                    ch->pcdata->missedtells==1?"":"s");
                act( "$n has reconnected.", ch, NULL, NULL, TO_ROOM );

                init_char_descriptor(d,ch);

                logf("[%d] %s@%s reconnected.",
                     d->descriptor, ch->name,dns_gethostname(d));
                wiznet(WIZ_LINKS,0,ch,NULL,
                       "%s@%s reconnected.",
                       ch->name,dns_gethostname(d));
                wiznet(WIZ_LINKS,0,ch,NULL,"$N groks the fullness of $S link.");
                d->connected = CON_PLAYING;
                check_mxp(d->character);
            }
            return TRUE;
        }
    }

    return FALSE;
}



/*
 * Check if already playing.
 */
bool check_playing( DESCRIPTOR_DATA *d, char *name )
{
    DESCRIPTOR_DATA *dold;

    for ( dold = descriptor_list; dold; dold = dold->next )
    {
        if ( dold != d
             &&   dold->character != NULL
             &&   dold->connected != CON_GET_NAME
             &&   dold->connected != CON_GET_OLD_PASSWORD
             &&   !str_cmp( name, dold->original
                            ? dold->original->name : dold->character->name ) )
        {
            logf("[%d] Questioning access to %s@%s (already playing).",
                 d->descriptor, d->character->name,dns_gethostname(d));
            wiznet(WIZ_LOGINS,get_trust(d->character),NULL,NULL,
                   "Questioning access to %s@%s (already playing).",
                   d->character->name,dns_gethostname(d));
            write_to_buffer( d, "That character is already playing.\n\r",0);
            write_to_buffer( d, "Do you wish to connect anyway [Yes/No]?",0);
            d->connected = CON_BREAK_CONNECT;
            sprintf_to_char(dold->character,
                            "{rWarning{x: possible hack attempt with your name from %s.\n\r",dns_gethostname(d));
            return TRUE;
        }
    }

    return FALSE;
}


void check_hack( DESCRIPTOR_DATA *d,char *name ) {
    DESCRIPTOR_DATA *dold;

    for ( dold = descriptor_list; dold; dold = dold->next ) {
        if (dold!=d
                &&  dold->character != NULL
                &&  dold->connected != CON_GET_NAME
                &&  dold->connected != CON_GET_OLD_PASSWORD
                &&   !str_cmp( name, dold->original
                               ? dold->original->name : dold->character->name ) ) {
            sprintf_to_char(dold->character,
                            "{rWarning{x: possible hack attempt with your name from %s.\n\r"
                            ,dns_gethostname(d));
        }
    }
}




void stop_idling( CHAR_DATA *ch )
{
    if ( ch == NULL
         ||   ch->desc == NULL
         ||   ch->desc->connected != CON_PLAYING
         ||   ch->was_in_room == NULL
         ||   ch->in_room != get_room_index(ROOM_VNUM_LIMBO))
        return;

    ch->timer = 0;
    char_from_room( ch );
    char_to_room( ch, ch->was_in_room );
    ch->was_in_room	= NULL;
    send_to_char("Your body phases back into the game.\n\r",ch);
    act( "$n has returned from the void.", ch, NULL, NULL, TO_ROOM );
    return;
}


