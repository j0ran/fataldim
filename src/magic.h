/* $Id: magic.h,v 1.36 2001/08/14 05:46:00 edwin Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,	   *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *									   *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael	   *
 *  Chastain, Michael Quan, and Mitchell Tse.				   *
 *									   *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc	   *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.						   *
 *									   *
 *  Much time and thought has gone into this software and you are	   *
 *  benefitting.  We hope that you share your changes too.  What goes	   *
 *  around, comes around.						   *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

void    say_spell      		(CHAR_DATA *ch,int sn);
int	skill_lookup		(const char *name);
int	find_spell		(CHAR_DATA *ch,const char *name);
void	change_alignment	(CHAR_DATA *ch1,CHAR_DATA *ch2,int value);
bool	saves_spell		(int level,CHAR_DATA *victim,int dam_type);
bool	saves_dispel		(int dis_level,int spell_level,int duration);
bool	check_dispel		(int dis_level,CHAR_DATA *victim,int sn);
int	mana_cost		(CHAR_DATA *ch,int min_mana,int level);
int	is_target_reflective	(CHAR_DATA *ch,CHAR_DATA *victim);
void	spell_imprint		(int sn,int level,CHAR_DATA *ch,
				 void *vo,int target);
void	obj_cast_spell		(int sn,int level,CHAR_DATA *ch,
				 CHAR_DATA *victim,OBJ_DATA *obj,
				 char *targetname);
void	show_cantrip		(CHAR_DATA *ch,CHAR_DATA *victim,int type);
void	identify_more		(CHAR_DATA *,OBJ_DATA *);
int	get_obj_level		(OBJ_DATA *obj);

extern	char *	target_name;
