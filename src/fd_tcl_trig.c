//
// $Id: fd_tcl_trig.c,v 1.13 2004/08/31 12:51:08 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#define NEEDS_TCL
#include "merc.h"

#define TCL_TRIG_CMD(name) static int name(Tcl_Interp *interp,int objc,Tcl_Obj *CONST objv[])

TCL_TRIG_CMD(trig_value);
TCL_TRIG_CMD(trig_true);
TCL_TRIG_CMD(trig_words);
TCL_TRIG_CMD(trig_wordsand);
TCL_TRIG_CMD(trig_chance);
TCL_TRIG_CMD(trig_compare);
TCL_TRIG_CMD(trig_bribe);
TCL_TRIG_CMD(trig_hpcnt);
TCL_TRIG_CMD(trig_text1);
TCL_TRIG_CMD(trig_text2);
TCL_TRIG_CMD(trig_exits);
TCL_TRIG_CMD(trig_give);
TCL_TRIG_CMD(trig_wear);
TCL_TRIG_CMD(TrigCmdNotImpl);

struct tcl_Trig_Command_Struct {
    char *cmd;
    int  (*func)(Tcl_Interp *,int,Tcl_Obj *CONST []);
};

static struct tcl_Trig_Command_Struct tclTrigCommands[] = {
	{ "value",	trig_value	},
	{ "true",	trig_true	},
	{ "words",	trig_words	},
	{ "wordsand",	trig_wordsand	},
	{ "chance",	trig_chance	},
	{ "compare",	trig_compare	},
	{ "bribe",	trig_bribe	},
	{ "hpcnt",	trig_hpcnt	},
	{ "text1",	trig_text1	},
	{ "text2",	trig_text2	},
	{ "match",	TrigCmdNotImpl	},
	{ "exits",	trig_exits	},
	{ "give",	trig_give	},
	{ "wear",	trig_wear	},
	{ NULL,		TrigCmdNotImpl	}
};


int tclTrigObjCmd (client, interp, objc, objv)
    ClientData client;			/* Not used. */
    Tcl_Interp *interp;			/* Current interpreter. */
    int objc;				/* Number of arguments. */
    Tcl_Obj *CONST objv[];		/* Argument values. */
{
    int index;

    objc--; objv++;

    if (objc==0) {
        return trig_value(interp,objc,objv);
    }

    if (Tcl_GetIndexFromObjStruct(interp, objv[0], 
                                  (char **)tclTrigCommands, sizeof(struct tcl_Trig_Command_Struct), 
                                  "trig command", 0, &index) != TCL_OK) {
        return TCL_ERROR;
    }

    return (tclTrigCommands[index].func)(interp,objc,objv);
}

TCL_TRIG_CMD(TrigCmdNotImpl) {
    Tcl_Obj *result;

    result=Tcl_NewStringObj("Trig command not implemented yet: ",-1);
    Tcl_AppendObjToObj(result,objv[0]);
    Tcl_SetObjResult(interp,result);

    return TCL_ERROR;
}

/* value */
TCL_TRIG_CMD(trig_value) {
    if (progData->trigValue) {
	if (objc<2)
	    Tcl_SetObjResult(interp,Tcl_NewStringObj(progData->trigValue,-1));
	else {
	    int num;
	    char *res,*tmp;
	    if (Tcl_GetIntFromObj(interp,objv[1],&num)!=TCL_OK)
		return TCL_ERROR;
	    if (progData->trigValue[0]==0) {
	      Tcl_SetObjResult(interp,Tcl_NewStringObj("",-1));
	    } else {
	      res=(char *)malloc(strlen(progData->trigValue)+1);
	      tmp=one_argument(progData->trigValue,res);
	      while (num) {
		  tmp=one_argument(tmp,res);
		  num--;
	      }
	      smash_case(res);
	      Tcl_SetObjResult(interp,Tcl_NewStringObj(res,-1));
	      free(res);
	    }
	}
    }
    return TCL_OK;
}

/* true */
TCL_TRIG_CMD(trig_true) {
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(TRUE));
    return TCL_OK;
}

/* words */
TCL_TRIG_CMD(trig_words) {
    int i;
    if (!progData->trigValue) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("trig words: no trigger value available",-1));
	return TCL_ERROR;
    }

    if (objc<2) {
	Tcl_WrongNumArgs(interp,0,objv,"trig words text ?text text ...?");
	return TCL_ERROR;
    }


    for (i=1;i<objc;i++)
	if (tcl_findword(Tcl_GetString(objv[i]),progData->trigValue))
	    break;

    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(i!=objc));
    return TCL_OK;
}

/* wordsand */
TCL_TRIG_CMD(trig_wordsand) {
    int i;
    if (progData->trigValue==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("trig wordsand: no trigger value available",-1));
	return TCL_ERROR;
    }

    if (objc<2) {
	Tcl_WrongNumArgs(interp,0,objv,"trig wordsand text ?text text ...?");
	return TCL_ERROR;
    }

    for (i=1;i<objc;i++)
	if (!tcl_findword(Tcl_GetString(objv[i]),progData->trigValue))
	    break;

    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(i==objc));
    return TCL_OK;
}

/* chance */
TCL_TRIG_CMD(trig_chance) {
    long percent;

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"trig chance percentage");
	return TCL_ERROR;
    }

    if (Tcl_GetLongFromObj(interp,objv[1],&percent)!=TCL_OK)
	return TCL_ERROR;

    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(number_percent()<=percent));
    return TCL_OK;
}


/* compare */
TCL_TRIG_CMD(trig_compare) {
    Tcl_Obj *prg;
    int error;
    int boolResult;
    char *firstarg;
    Tcl_Obj *compareObj;

    if ((objc!=2) && (objc!=3)) {
	Tcl_WrongNumArgs(interp,0,objv,"trig compare ?argnum? text");
	return TCL_ERROR;
    }

    if (progData->trigValue==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("trig compare: no trigger value available",-1));
	return TCL_ERROR;
    }

    firstarg=Tcl_GetString(objv[1]);
    if (isdigit(firstarg[0])) {
	compareObj=objv[2];
	prg=Tcl_NewStringObj("[trig value ",-1);
	Tcl_AppendObjToObj(prg,objv[1]);
	Tcl_AppendStringsToObj(prg,"]",NULL);
    } else {
	compareObj=objv[1];
	prg=Tcl_NewStringObj("[trig]",-1);
    }

    Tcl_AppendObjToObj(prg,compareObj);
    Tcl_IncrRefCount(prg);
    error=Tcl_ExprBooleanObj(interp,prg,&boolResult);
    Tcl_DecrRefCount(prg);

    if (error!=TCL_OK) return TCL_ERROR;

    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(boolResult));
    return TCL_OK;
}


/* bribe */
TCL_TRIG_CMD(trig_bribe) {
    long amount;
    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"trig bribe amount");
	return TCL_ERROR;
    }

    if (progData->trigValue==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("trig compare: no trigger value available",-1));
	return TCL_ERROR;
    }


    if (Tcl_GetLongFromObj(interp,objv[1],&amount)!=TCL_OK)
	  return TCL_ERROR;
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(atoi(progData->trigValue)>=amount));
    return TCL_OK;
}



/* hpcnt */
TCL_TRIG_CMD(trig_hpcnt) {
    long percentage;
    CHAR_DATA *ch;

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"trig hpcnt percentage");
	return TCL_ERROR;
    }

    ch=progData->mob;
    if (ch==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("trig hpcnt: no mob to execute this on",-1));
	return TCL_ERROR;
    }

    if (Tcl_GetLongFromObj(interp,objv[1],&percentage)!=TCL_OK)
	return TCL_ERROR;
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(((ch->hit * 100) / UMAX(1,ch->max_hit))<percentage));
    return TCL_OK;
}

/* text1 */
TCL_TRIG_CMD(trig_text1) {
    Tcl_SetObjResult(interp,Tcl_NewStringObj(progData->text1?progData->text1:"",-1));
    return TCL_OK;
}
/* text2 */
TCL_TRIG_CMD(trig_text2) {
    Tcl_SetObjResult(interp,Tcl_NewStringObj(progData->text2?progData->text2:"",-1));
    return TCL_OK;
}


#ifdef notdef
    case 10: /*match */
	Tcl_SetStringObj(resultPtr,"trig match: not implemented yet",-1);
	return TCL_ERROR;
	nargs=0;
	{
	    int i=1;
	    Tcl_RegExp regexp;
	    int res;

	    if (progData->trigValue==NULL) {
		Tcl_SetStringObj(resultPtr,"trig match: no trigger value available",-1);
		return TCL_ERROR;
	    }

	    if (objc-i!=1) {
		Tcl_WrongNumArgs(interp,0,objv,"match regexpr");
		return TCL_ERROR;
	    }

	    regexp=Tcl_GetRegExpFromObj(interp,objv[1],REG_ADVANCED | REG_ICASE);

	    res=Tcl_RegExpExec(interp,regexp,progData->trigValue,progData->trigValue);
	    if (res==-1) return TCL_ERROR;

	    Tcl_SetBooleanObj(resultPtr,res);
	}
#endif
/* exits */
TCL_TRIG_CMD(trig_exits) {
    int i=1;
    bool match=FALSE;
    int index;
    int val;

    static tclOptionList *exitDirections[]={
	"0","1","2","3","4","5",
	"north","east","south","west","up","down",
	NULL};

    if (progData->trigValue==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("trig match: no trigger value available",-1));
	return TCL_ERROR;
    }

    if (!is_number(progData->trigValue)) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("trig exit: trigger value isn't a number",-1));
	return TCL_ERROR;
    }

    val=atoi(progData->trigValue);

    while (objc-i>0) {
	if (Tcl_GetIndexFromObj(interp, objv[i], exitDirections, "direction", 0, &index) != TCL_OK)
	    return TCL_ERROR;
	if (index>5) index-=6;

	if (val==index) {
	    match=TRUE;
	    break;
	}

	i++;
    }

    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(match));
    return TCL_OK;
}

/* give objid*/
TCL_TRIG_CMD(trig_give) {
    if (progData->obj==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("trig give: no object found",-1));
	return TCL_ERROR;
    }

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"trig give objid");
	return TCL_ERROR;
    }

    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(tcl_isobjid(progData->obj,Tcl_GetString(objv[1]))));
    return TCL_OK;
}

// wear
TCL_TRIG_CMD(trig_wear) {
    int i=1;
    bool match=FALSE;

    if (progData->trigValue==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("trig wear: no trigger value available",-1));
	return TCL_ERROR;
    }

    while (objc-i>0) {
	if (str_cmp(progData->trigValue,Tcl_GetString(objv[i]))==0) {
	    match=TRUE;
	    break;
	}

	i++;
    }

    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(match));
    return TCL_OK;
}
