//
// $Id: fd_editor.c,v 1.3 2005/12/27 12:05:40 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"

//
// Send lines "first" to "last" to the screen
//
void send_editor_to_char(CHAR_DATA *ch,int first,int last) {
    int		line=1;
    char *	p;
    char *	pBeginLine;
    char	c;
    bool	inEnd;

    char *	pStringBegin=*(ch->desc->pStringBegin);
    char *	pStringEnd=ch->desc->pStringEnd;

    if (pStringBegin[0]!=0) {
	p=pBeginLine=pStringBegin;
	inEnd=FALSE;
    } else if (pStringEnd[0]!=0) {
	p=pBeginLine=pStringEnd;
	inEnd=TRUE;
    } else {
	p=NULL;
	line=0;
    }

    STR_COPY_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR,PLR_WANTS_RAW_COLOUR);
    while (p!=NULL) {
	if (p[0]=='\n' || p[0]==0) {
	    c=p[0];
	    p[0]=0;
	    if (first<line && ( last<0 || line<=last))
		sprintf_to_char(ch,"%3d. %s\n\r",line,pBeginLine);
	    p[0]=c;

	    if (last==line)
		break;

	    if (c==0 || p[2]==0) {
		if (c==0)
		    send_to_char("\n\r",ch);
		if (inEnd || pStringEnd[0]==0)	// adding to the end that is.
		    break;
		inEnd=TRUE;
		p=pBeginLine=pStringEnd;
	    } else {
		pBeginLine=p+2;
		p+=2;
	    }

	    line++;	// if done before the check it would give an
			// invalid number of lines back into currentLinenumbers
	} else
	    p++;
    }
    if (ch->desc->currentLinenumber<0)
	ch->desc->currentLinenumber=line;
    if (ch->desc->currentLinenumber>line)
	ch->desc->currentLinenumber=line;
    STR_REMOVE_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);
}

//
// Put a character in edit-mode
//
void editor_start( CHAR_DATA *ch, char **pString) {
    if (*pString && strlen(*pString)>MSL-10) {
	send_to_char("Text is too large to be edited online.\n\r",ch);
	return;
    }
      
    send_to_char("-=======- Entering EDIT Mode -=======-\n\r",ch);
    send_to_char("    Type .h on a new line for help\n\r",ch);
    send_to_char("  Terminate with a @ on a blank line\n\r",ch);
    sprintf_to_char(ch,"         %d characters free\n\r",
	MAX_STRING_LENGTH-strlen(*pString)-10);
    send_to_char("-====================================-\n\r",ch);

    if (*pString==NULL)
        *pString=str_dup("");

    ch->desc->pStringBegin=pString;
    ch->desc->pStringEnd=str_dup("");
    ch->desc->currentLinenumber=-1;

    send_editor_to_char(ch,-1,-1);

    return;
}

//
// Goto a new line in the editor. If there aren't enough lines, go to the last.
//
void editor_gotoline(CHAR_DATA *ch) {
    char buf[MAX_STRING_LENGTH];
    int line;
    char *p,c;

    strcpy(buf,*ch->desc->pStringBegin);
    strcat(buf,ch->desc->pStringEnd);
    free_string(*ch->desc->pStringBegin);
    free_string(ch->desc->pStringEnd);

    line=0;
    for (p=buf;p[0]!=0 && line!=ch->desc->currentLinenumber;p++)
	if (p[0]=='\r')
	    line++;

    if (p[0]==0) {
	*ch->desc->pStringBegin=str_dup(buf);
	ch->desc->pStringEnd=str_dup("");
    } else {
	c=p[0];
	p[0]=0;
	*ch->desc->pStringBegin=str_dup(buf);
	p[0]=c;
	ch->desc->pStringEnd=str_dup(p);
    }
}

//
// Add a string to the text being edited.
//
void editor_addline(CHAR_DATA *ch,char *argument) {
    char	buf[MAX_STRING_LENGTH];

    smash_tilde(argument);

    if (argument[0]=='.') {
	editor_process_command(ch,argument);
	return;
    }

    if (!str_cmp(argument,"@")) {
	editor_process_command(ch,".q");
	return;
    }

    if (strlen(*ch->desc->pStringBegin)+
	strlen(argument)+
	strlen(ch->desc->pStringEnd)+10>MAX_STRING_LENGTH) {

        send_to_char("\n\rString too long, last line skipped.\n\r",ch);
	// Force character out of editing mode.
        ch->desc->pStringBegin=NULL;
    }

    if(ch->desc->pStringBegin) {
	strcpy(buf,*ch->desc->pStringBegin);
	strcat(buf,argument );
	strcat(buf,"\n\r" );
	free_string(*ch->desc->pStringBegin);
	*ch->desc->pStringBegin=str_dup(buf);
	ch->desc->currentLinenumber++;
    }
}

void editor_process_command(CHAR_DATA *ch,char *argument) {
    int		old_line,line,line_begin,line_end;
    char	command[MSL];
    char	arg1[MSL];
    char	arg2[MSL];
    char	buf[MSL];
    char *	p;
    char *	q;
    char	buf_to[MSL];

    argument=one_argument(argument,command);
    argument=one_argument(argument,arg1);
    argument=one_argument(argument,arg2);

    if (!str_cmp(command,".s")) {
	if (!str_cmp(arg1,"all")) {
	    line=ch->desc->currentLinenumber;
	    ch->desc->currentLinenumber=-1;
	    send_editor_to_char(ch,-1,-1);
	    ch->desc->currentLinenumber=line;
	} else if (arg2[0]) {
	    line_begin=atoi(arg1);
	    line_end=atoi(arg2);
	    if (!is_number(arg1) || !is_number(arg2) ||
		line_begin>line_end || line_begin<0 || line_end<0 ) {
		send_to_char("<number> should be an integer larger than or "
		    "equal to 0, and first number should be smaller than "
		    "the second number, see the help.\n\r",ch);
		return;
	    }
	    line=ch->desc->currentLinenumber;
	    send_editor_to_char(ch,line_begin-1,line_end);
	    ch->desc->currentLinenumber=line;
	} else {
	    send_editor_to_char(ch,
		ch->desc->currentLinenumber-10,ch->desc->currentLinenumber);
	}
	return;
    }

    if (!str_cmp(command,".i")) {
	line=atoi(arg1);
	if (!is_number(arg1) || line<0) {
	    send_to_char("<number> should be an integer larger "
		"than or equal to 0, see the help.\n\r",ch);
	    return;
	}

	ch->desc->currentLinenumber=line-1;
	editor_gotoline(ch);
	send_editor_to_char(ch,
	    ch->desc->currentLinenumber-10,ch->desc->currentLinenumber);
	return;
    }

    if (!str_cmp(command,".c")) {
	free_string(*ch->desc->pStringBegin);
	free_string(ch->desc->pStringEnd);
	*ch->desc->pStringBegin=str_dup("");
	ch->desc->pStringEnd=str_dup("");
	ch->desc->currentLinenumber=-1;
	send_to_char("Text cleared, back to line 1.\n\r",ch);
	send_editor_to_char(ch,-1,-1);
	return;
    }

    if (!str_cmp(command,".f")) {
	strcpy(buf,*ch->desc->pStringBegin);
	strcat(buf,ch->desc->pStringEnd);
	free_string(*ch->desc->pStringBegin);
	free_string(ch->desc->pStringEnd);
	*ch->desc->pStringBegin=format_string(str_dup(buf));
	ch->desc->pStringEnd=str_dup("");
	ch->desc->currentLinenumber=-1;
	send_editor_to_char(ch,-1,-1);
	return;
    }

    if (!str_cmp(command,".w")) {
	STR_TOGGLE_BIT(ch->strbit_act,PLR_WANTS_RAW_COLOUR);
	sprintf_to_char(ch,"Raw colour codes is now %s.\n\r",
	    STR_IS_SET(ch->strbit_act,PLR_WANTS_RAW_COLOUR)?
	    "enabled":"disabled");
	send_editor_to_char(ch,
	    ch->desc->currentLinenumber-10,ch->desc->currentLinenumber);
	return;
    }

    if (!str_cmp(command,".q")) {
	strcpy(buf,*ch->desc->pStringBegin);
	strcat(buf,ch->desc->pStringEnd);
	free_string(*ch->desc->pStringBegin);
	free_string(ch->desc->pStringEnd);
	*ch->desc->pStringBegin=str_dup(buf);
	ch->desc->pStringBegin=NULL;
	return;
    }

    if (!str_cmp(command,".r")) {
	if (arg2[0]==0) {
	    send_to_char("Usage: .r <string> <string>\n\r",ch);
	    return;
	}

	if (ch->desc->currentLinenumber==0) {
	    send_to_char("Go a little more down, "
		"there is more to replace.\n\r",ch);
	    return;
	}
	if (strlen(*ch->desc->pStringBegin)+
	    strlen(ch->desc->pStringEnd)+
	    strlen(arg2)-strlen(arg1)>MAX_STRING_LENGTH-10) {
	    send_to_char("Not enough room to make that change\n\r",ch);
	    return;
	}

	strcpy(buf,*ch->desc->pStringBegin);
	p=strrchr(buf,'\r');
	p[0]=0;
	p=strrchr(buf,'\r');
	if (!p) p=buf;	// first line

	q=strstr(p,arg1);
	if (!q) {
	    sprintf_to_char(ch,
		"Couldn't find string to replace (%s)\n\r",arg1);
	    return;
	}
	strcpy(buf_to,buf);
	buf_to[q-buf]=0;
	strcat(buf_to,arg2);
	strcat(buf_to,q+strlen(arg1));
	strcat(buf_to,"\r");
	free_string(*ch->desc->pStringBegin);
	*ch->desc->pStringBegin=str_dup(buf_to);
	sprintf_to_char(ch,"Replaced '%s' with '%s'.\n\r",arg1,arg2);
	return;
    }

    if (!str_cmp(command,".d")) {
	if (arg1[0]==0) {		// .d
	    if (ch->desc->currentLinenumber==0) {
		send_to_char("You're already at the first line.\n\r",ch);
		return;
	    }

	    strcpy(buf,*ch->desc->pStringBegin);

	    p=strrchr(buf,'\r');
	    if (!p) {
		send_to_char("Internal problem: "
		    "couldn't find the last line.\n\r",ch);
		return;
	    }
	    p[0]=0;
	    p=strrchr(buf,'\r');
	    if (!p)
		buf[0]=0;
	    else
		p[1]=0;

	    free_string(*ch->desc->pStringBegin);
	    *ch->desc->pStringBegin=str_dup(buf);

	    sprintf_to_char(ch,"Line %d deleted.\n\r",
		ch->desc->currentLinenumber);
	    ch->desc->currentLinenumber--;

	    return;
	} else if (arg1[0]!=0) {	// .d <number>
	    old_line=ch->desc->currentLinenumber;
	    line=atoi(arg1);
	    line_end=atoi(arg2);
	    if (!is_number(arg1) || line<=0 ||
	       (arg2[0] && (!is_number(arg2) || line_end<0
			    || line_end<=line )))  {
		send_to_char("<number> should be an integer "
		    "larger than 0, see the help.\n\r",ch);
		return;
	    }
	    line_end=line_end-line+1;

	    strcpy(buf,*ch->desc->pStringBegin);
	    strcat(buf,ch->desc->pStringEnd);
	    p=buf;line--;
	    while (line) {
		q=strchr(p,'\r');
		if (q==NULL) {
		    sprintf_to_char(ch,
			"Line number (%s) not found.\n\r",arg1);
		    return;
		}
		p=q+1;
		line--;
	    }

	    if (arg2[0]!=0) {		// .d <number> <number>
		while (line_end--) {
		    // p is line to delete, and line_end lines more
		    q=strchr(p,'\r');
		    if (q==NULL)
			p[0]=0;
		    else
			strcpy(p,q+1);
		}
	    } else {
		// p is line to delete, up to q
		q=strchr(p,'\r');
		if (q==NULL)
		    p[0]=0;
		else
		    strcpy(p,q+1);
	    }

	    free_string(*ch->desc->pStringBegin);
	    free_string(ch->desc->pStringEnd);
	    *ch->desc->pStringBegin=str_dup(buf);
	    ch->desc->pStringEnd=str_dup("");

	    ch->desc->currentLinenumber=old_line;
	    editor_gotoline(ch);

	    send_editor_to_char(ch,
		ch->desc->currentLinenumber-10,ch->desc->currentLinenumber);
	}
	return;
    }

    if (!str_cmp(command,".h")) {
	send_to_char("Help overview\n\r",ch);
	send_to_char(".c                   clear everything\n\r",ch);
	send_to_char(".d                   delete previous line\n\r",ch);
	send_to_char(".d <number>          delete line <number>\n\r",ch);
	send_to_char(".d <number> <number> delete lines <number> to <number>\n\r",ch);
	send_to_char(".f                   format string\n\r",ch);
	send_to_char(".i <number>          make line <number> current\n\r",ch);
	send_to_char(".r <string> <string> replace <string> with <string> on previous line\n\r",ch);
	send_to_char(".s                   show the last 10 lines\n\r",ch);
	send_to_char(".s all               show the full text\n\r",ch);
	send_to_char(".s <number> <number> show the full text\n\r",ch);
	send_to_char(".w                   raw colour codes\n\r",ch);
	return;
    }

    send_to_char("Invalid command, use .h for the help.\n\r",ch);
    return;
}
