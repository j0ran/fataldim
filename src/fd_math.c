//
// $Id: fd_math.c,v 1.3 2004/09/12 16:44:52 jodocus Exp $
//
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

#include "merc.h"

//
// log10
//
int fd_log10(int number) {
    if (number<10) return 0;
    if (number<100) return 1;
    if (number<1000) return 2;
    if (number<10000) return 3;
    if (number<100000) return 4;
    if (number<1000000) return 5;
    if (number<10000000) return 6;
    if (number<100000000) return 7;
    if (number<1000000000) return 8;
    return -1;
}

//
// Stick a little fuzz on a number.
//
int number_fuzzy(int number) {
    switch (number_bits(2)) {
    case 0: number-=1; break;
    case 3: number+=1; break;
    }
    return UMAX(1,number);
}

//
// Generate a random number.
//
int number_range(int from,int to) {
    int power;
    int number;

    if (from==0 && to==0)
	return 0;

    if ((to=to-from+1)<=1)
	return from;

    for (power=2;power<to;power<<=1)
	;

    while ((number=number_mm() & (power-1))>=to)
	;

    return from+number;
}

//
// Generate a percentile roll.
//
int number_percent(void) {
    int percent;

    while ((percent=number_mm() & (128-1))>99)
	;

    return 1+percent;
}

//
// Generate a number which is 'width' bits wide
//
int number_bits(int width) {
    return number_mm() & ((1<<width)-1);
}


//
// I've gotten too many bad reports on OS-supplied random number generators.
// This is the Mitchell-Moore algorithm from Knuth Volume II.
// Best to leave the constants alone unless you've read Knuth.
// -- Furey
//
// I noticed streaking with this random number generator, so I switched
// back to the system srandom call.  If this doesn't work for you,
// define OLD_RAND to use the old system -- Alander
//
#ifdef OLD_RAND
static  int     rgiState[2+55];
#endif

void init_mm(void) {
#ifdef OLD_RAND
    int *piState;
    int iState;

    piState     = &rgiState[2];

    piState[-2] = 55 - 55;
    piState[-1] = 55 - 24;

    piState[0]  = ((int) current_time) & ((1 << 30) - 1);
    piState[1]  = 1;
    for ( iState = 2; iState < 55; iState++ )
    {
        piState[iState] = (piState[iState-1] + piState[iState-2])
                        & ((1 << 30) - 1);
    }
#else
    srandom(time(NULL)^getpid());
#endif
    return;
}

long number_mm(void) {
#if defined (OLD_RAND)
    int *	piState;
    int		iState1;
    int		iState2;
    int		iRand;

    piState             = &rgiState[2];
    iState1             = piState[-2];
    iState2             = piState[-1];
    iRand               = (piState[iState1] + piState[iState2])
                        & ((1 << 30) - 1);
    piState[iState1]    = iRand;
    if (++iState1==55)
        iState1=0;
    if (++iState2==55)
        iState2=0;
    piState[-2]         = iState1;
    piState[-1]         = iState2;
    return iRand>>6;
#else
    return random()>>6;
#endif
}

//
// Roll some dice.
//
int dice(int number,int size) {
    int idice;
    int sum;

    switch ( size )
    {
    case 0: return 0;
    case 1: return number;
    }

    for (idice=0,sum=0;idice<number;idice++)
	sum+=number_range(1,size);

    return sum;
}

//
// Simple linear interpolation.
//
int interpolate(int level,int value_00,int value_32) {
    return value_00+level*(value_32-value_00)/32;
}

//
// Less simple linear interpolation
//
int new_interpolate(int value,int lowfrom,int hifrom,int lowto,int hito) {
  return lowto+(((((value-lowfrom)*1000)/(hifrom-lowfrom))*(hito-lowto))/1000);
}

//
// Is an ascii-string a number.
//
bool is_number(char *string) {
    if (*string==0)
	return FALSE;   

    if ( *string=='+' || *string=='-' )
	string++;

    for ( ; *string!=0; string++ )
	if (!isdigit(*string))
	    return FALSE;

    return TRUE;
}
