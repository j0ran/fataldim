//
// $Id: fd_tcl_misc.c,v 1.11 2007/03/04 09:51:52 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#define NEEDS_TCL
#include "merc.h"


int tclProgTypeObjCmd (client, interp, objc, objv)
    ClientData client;			/* Not used. */
    Tcl_Interp *interp;			/* Current interpreter. */
    int objc;				/* Number of arguments. */
    Tcl_Obj *CONST objv[];		/* Argument values. */
{
    if (objc!=1) {
	Tcl_WrongNumArgs(interp,1,objv,"");
	return TCL_ERROR;
    }

    switch (progType) {
    case PROGTYPE_NONE: Tcl_SetObjResult(interp,Tcl_NewStringObj("none",-1));break;
    case PROGTYPE_CHAR: Tcl_SetObjResult(interp,Tcl_NewStringObj("char",-1));break;
    case PROGTYPE_OBJECT: Tcl_SetObjResult(interp,Tcl_NewStringObj("object",-1));break;
    case PROGTYPE_ROOM: Tcl_SetObjResult(interp,Tcl_NewStringObj("room",-1));break;
    case PROGTYPE_AREA: Tcl_SetObjResult(interp,Tcl_NewStringObj("area",-1));break;
    default:
	Tcl_SetObjResult(interp,Tcl_NewStringObj("fatal error: unknown mobprog type",-1));
	return TCL_ERROR;
    }

    return TCL_OK;
}

int tclSourceObjCmd (client, interp, objc, objv)
    ClientData client;			/* Not used. */
    Tcl_Interp *interp;			/* Current interpreter. */
    int objc;				/* Number of arguments. */
    Tcl_Obj *CONST objv[];		/* Argument values. */
{
    TCLPROG_CODE *prg;
    long mprog_vnum;

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,1,objv,"vnum");
	return TCL_ERROR;
    }

    if (Tcl_GetLongFromObj(interp,objv[1],&mprog_vnum)!=TCL_OK) return TCL_ERROR;

    if ((prg=get_mprog_index(mprog_vnum)) && !prg->edit) {
	return Tcl_Eval(interp,prg->code);
    } else {
	if (!prg)
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("source: mobprog not found",-1));
	else
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("source: program is being edited",-1));
	return TCL_ERROR;
    }

    return TCL_OK;
}


int tclDirectionNameObjCmd (client, interp, objc, objv)
    ClientData client;			/* Not used. */
    Tcl_Interp *interp;			/* Current interpreter. */
    int objc;				/* Number of arguments. */
    Tcl_Obj *CONST objv[];		/* Argument values. */
{
    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"directionname number");
	return TCL_ERROR;
    }

    {
	long number;

	if (Tcl_GetLongFromObj(interp,objv[1],&number)!=TCL_OK) return TCL_ERROR;

	if (number<-1 || number>5) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("directionname: invalid number",-1));
	    return TCL_ERROR;
	}

	switch(number) {
	case -1: Tcl_SetObjResult(interp,Tcl_NewStringObj("here", -1));break;
	case  0: Tcl_SetObjResult(interp,Tcl_NewStringObj("north",-1));break;
	case  1: Tcl_SetObjResult(interp,Tcl_NewStringObj("east" ,-1));break;
	case  2: Tcl_SetObjResult(interp,Tcl_NewStringObj("south",-1));break;
	case  3: Tcl_SetObjResult(interp,Tcl_NewStringObj("west" ,-1));break;
	case  4: Tcl_SetObjResult(interp,Tcl_NewStringObj("up"   ,-1));break;
	case  5: Tcl_SetObjResult(interp,Tcl_NewStringObj("down" ,-1));break;
	}
    }

    return TCL_OK;
}

int tclRandomObjCmd (client, interp, objc, objv)
    ClientData client;			/* Not used. */
    Tcl_Interp *interp;			/* Current interpreter. */
    int objc;				/* Number of arguments. */
    Tcl_Obj *CONST objv[];		/* Argument values. */
{
    long minval=0,maxval=99;

    if (objc==2) {
	if (Tcl_GetLongFromObj(interp,objv[1],&maxval)!=TCL_OK) return TCL_ERROR;
    } else if (objc==3) {
	if (Tcl_GetLongFromObj(interp,objv[1],&minval)!=TCL_OK) return TCL_ERROR;
	if (Tcl_GetLongFromObj(interp,objv[2],&maxval)!=TCL_OK) return TCL_ERROR;
    } else if (objc!=1) {
	Tcl_WrongNumArgs(interp,1,objv,"?min? ?max?");
	return TCL_ERROR;
    }

    Tcl_SetObjResult(interp,Tcl_NewLongObj(number_range(minval,maxval)));

    return TCL_OK;
}


int tclInterpretObjCmd (client, interp, objc, objv)
    ClientData client;			/* Not used. */
    Tcl_Interp *interp;			/* Current interpreter. */
    int objc;				/* Number of arguments. */
    Tcl_Obj *CONST objv[];		/* Argument values. */
{
    Tcl_Obj *cmd;
    CHAR_DATA *ch=NULL;

    objc--; objv++;

    switch (progType) {
    default:
    case PROGTYPE_NONE:
    case PROGTYPE_CHAR:
	if ((ch=tcl_parse_char_switches(progData->mob,&objc,&objv))==NULL)
	    return TCL_ERROR;
	break;
    case PROGTYPE_OBJECT:
    case PROGTYPE_ROOM:
    case PROGTYPE_AREA:
	if ((ch=tcl_parse_char_switches(progData->ch,&objc,&objv))==NULL)
	    return TCL_ERROR;
	break;
    }

    cmd=Tcl_ConcatObj(objc,objv);
    Tcl_IncrRefCount(cmd);
    {
	char cmdbuf[MAX_INPUT_LENGTH],*str;
	int len;

	str=Tcl_GetString(cmd);  // Get the string

	len=strlen(str);
	len=UMIN(len,(MAX_INPUT_LENGTH-1));

	strncpy(cmdbuf,str,len);
	cmdbuf[len]='\0';

	interpret(ch,cmdbuf);
    }
    Tcl_DecrRefCount(cmd);

    return TCL_OK;
}

int tclFormatObjCmd (client, interp, objc, objv)
    ClientData client;			/* Not used. */
    Tcl_Interp *interp;			/* Current interpreter. */
    int objc;				/* Number of arguments. */
    Tcl_Obj *CONST objv[];		/* Argument values. */
{
    char *buf;
    int l;

    if (objc<2) {
	// empty string is default result
	return TCL_OK;
    }

    buf=str_dup(Tcl_GetString(objv[1]));
    l=strlen(buf);
    if ((l>0) && (buf[l-1]!='\n')) {
      char *buf2;
      buf2=(char *)alloc_mem(l+3);
      strncpy(buf2,buf,l);
      buf2[l]='\n';
      buf2[l+1]='\r';
      buf2[l+2]=0;
      free_string(buf);
      buf=buf2;
    }
    buf=format_string(buf);
    Tcl_SetObjResult(interp,Tcl_NewStringObj(buf,-1));
    free_string(buf);

    return TCL_OK;
}

int tclDebugObjCmd( client, interp, objc, objv)  
    ClientData client;                  /* Not used. */
    Tcl_Interp *interp;                 /* Current interpreter. */
    int objc;                           /* Number of arguments. */
    Tcl_Obj *CONST objv[];              /* Argument values. */
{
    int i;
    char *namespace;
    Tcl_Obj *result;

    Tcl_Eval(interp,"namespace current");
    result=Tcl_GetObjResult(interp);
    namespace=Tcl_GetString(result);

    for (i=1;i<objc;i++) {
	wiznet(WIZ_TCL_DEBUG,0,NULL,NULL,"%s: %s",namespace,Tcl_GetString(objv[i]));
	logf("TCL debug: %s: %s",namespace,Tcl_GetString(objv[i]));
    }
    return TCL_OK;
}

int tclStringChangeCost( client, interp, objc, objv)
    ClientData client;                  /* Not used. */
    Tcl_Interp *interp;                 /* Current interpreter. */
    int objc;                           /* Number of arguments. */
    Tcl_Obj *CONST objv[];              /* Argument values. */
{
    tclOptionList *options[] = { "-insert", "-remove", "-replace", NULL};
    char *str_a,*str_b;
    int len_a,len_b;
    int insert_cost=1;
    int remove_cost=1;
    int replace_cost=1;
    int i,a,b;

    unsigned int *pad[2];
    unsigned int sub,ins,rem;

    i=1;

    while (i<objc && *(str_a=Tcl_GetString(objv[i]))=='-') {
	int index,value;

	if (Tcl_GetIndexFromObj(interp,objv[i],options,"option",0,&index)!=TCL_OK)
	    return TCL_ERROR;

	i++;
	if (objc<=i) {
	    Tcl_WrongNumArgs(interp,objc,objv,"value string_a string_b");
	    return TCL_ERROR;
	}
	if (Tcl_GetIntFromObj(interp,objv[i],&value)!=TCL_OK)
	    return TCL_ERROR;

	switch (index) {
	    case 0: insert_cost=value; break;
	    case 1: remove_cost=value; break;
	    case 2: replace_cost=value; break;
	}
	i++;
    }

    if (objc-i<2) {
	Tcl_WrongNumArgs(interp,i,objv,"string_a string_b");
	return TCL_ERROR;
    }

    str_a=Tcl_GetString(objv[i++]);
    str_b=Tcl_GetString(objv[i++]);

    len_a=strlen(str_a);
    len_b=strlen(str_b);

    if (len_a==0) {
	Tcl_SetObjResult(interp,Tcl_NewIntObj(len_b*insert_cost));
	return TCL_OK;
    }
    if (len_b==0) {
	Tcl_SetObjResult(interp,Tcl_NewIntObj(len_a*remove_cost));
	return TCL_OK;
    }

    pad[0]=malloc(2*(len_b+1)*sizeof(unsigned int));
    pad[1]=pad[0]+(len_b+1);


    pad[0][0]=0;
    for (b=1;b<len_b+1;b++)
      pad[0][b]=b*insert_cost;

    for (a=0;a<len_a;a++) {
	pad[(a+1) & 1][0]=(a+1)*remove_cost;
	for (b=0;b<len_b;b++) {
	    unsigned int j;

	    sub=pad[a & 1][b]+((str_a[a]==str_b[b])?0:replace_cost);
	    ins=pad[(a+1) & 1][b]+insert_cost;
	    rem=pad[a & 1][b+1]+remove_cost;

	    j=(sub<ins)?sub:ins;
	    j=(j<rem)?j:rem;
	    pad[(a+1) & 1][b+1]=j;
	}
    }
    Tcl_SetObjResult(interp,Tcl_NewIntObj(pad[len_a & 1][len_b]));
    free(pad[0]);
    return TCL_OK;
}
