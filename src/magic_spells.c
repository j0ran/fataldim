//
// $Id: magic_spells.c,v 1.37 2008/05/01 19:21:42 jodocus Exp $
//

/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*       ROM 2.4 is copyright 1993-1996 Russ Taylor                         *
*       ROM has been brought to you by the ROM consortium                  *
*           Russ Taylor (rtaylor@pacinfo.com)                              *
*           Gabrielle Taylor (gtaylor@pacinfo.com)                         *
*           Brian Moore (rom@rom.efn.org)                                  *
*       By using this code, you have agreed to follow the terms of the     *
*       ROM license, in the file Rom24/doc/rom.license                     *
***************************************************************************/


#include "merc.h"
#include "interp.h"
#include "color.h"


static void refresh(CHAR_DATA *ch, CHAR_DATA *victim, int level, int multiplier);
static void heal(CHAR_DATA *ch, CHAR_DATA *victim, int level, int multiplier);


/*
 * Spell functions.
 */
void spell_acid_blast( int sn, int level, CHAR_DATA *ch, void *vo, int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    int dam;

    dam = dice( level, 12 );
    if ( saves_spell( level, victim, DAM_ACID ) )
        dam /= 2;
    damage( ch, victim, dam, sn,DAM_ACID,TRUE, NULL);
    return;
}



void spell_armor( int sn, int level, CHAR_DATA *ch, void *vo, int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( is_affected( victim, sn ) ) {
        if (victim == ch)
            send_to_char("You are already armored.\n\r",ch);
        else
            act("$N is already armored.",ch,NULL,victim,TO_CHAR);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where	 = TO_EFFECTS;
    ef.type      = sn;
    ef.level	 = level;
    ef.duration  = 24;
    ef.modifier  = -20;
    ef.location  = APPLY_AC;
    ef.bitvector = EFF_ARMOR;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char( "You feel someone protecting you.\n\r", victim );
    if ( ch != victim )
        act("$N is protected by your magic.",ch,NULL,victim,TO_CHAR);
    return;
}



void spell_bless( int sn, int level, CHAR_DATA *ch, void *vo, int target)
{
    CHAR_DATA *victim;
    OBJ_DATA *obj;
    EFFECT_DATA ef;

    /* deal with the object case first */
    if (target == TARGET_OBJ)
    {
        obj = (OBJ_DATA *) vo;
        if (IS_OBJ_STAT(obj,ITEM_BLESS))
        {
            act("$p is already blessed.",ch,obj,NULL,TO_CHAR);
            return;
        }

        if (IS_OBJ_STAT(obj,ITEM_EVIL))
        {
            EFFECT_DATA *pef;

            pef = effect_find(obj->affected,TO_OBJECT,gsn_curse);
            if (!saves_dispel(level,pef != NULL ? pef->level : obj->level,0))
            {
                if (pef != NULL)
                    effect_remove_obj(obj,pef);
                act("$p glows a pale blue.",ch,obj,NULL,TO_ALL);
                STR_REMOVE_BIT(obj->strbit_extra_flags,ITEM_EVIL);
                return;
            }
            else
            {
                act("The evil of $p is too powerful for you to overcome.",
                    ch,obj,NULL,TO_CHAR);
                return;
            }
        }

        ZEROVAR(&ef,EFFECT_DATA);
        ef.where	= TO_OBJECT;
        ef.type		= sn;
        ef.level	= level;
        ef.duration	= 6 + level;
        ef.location	= APPLY_SAVES;
        ef.modifier	= -1;
        ef.bitvector	= ITEM_BLESS;
        ef.casted_by	= ch?ch->id:0;
        effect_to_obj(obj,&ef);

        act("$p glows with a holy aura.",ch,obj,NULL,TO_ALL);

        if (obj->wear_loc != WEAR_NONE)
            ch->saving_throw -= 1;
        return;
    }

    /* character target */
    victim = (CHAR_DATA *) vo;


    if ( victim->position == POS_FIGHTING || is_affected( victim, sn ) )
    {
        if (victim == ch)
            send_to_char("You are already blessed.\n\r",ch);
        else
            act("$N already has divine favor.",ch,NULL,victim,TO_CHAR);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level	 = level;
    ef.duration  = 6+level;
    ef.location  = APPLY_HITROLL;
    ef.modifier  = level / 8;
    ef.bitvector = EFF_BLESS;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    ef.location  = APPLY_SAVING_SPELL;
    ef.modifier  = 0 - level / 8;
    effect_to_char( victim, &ef );
    send_to_char( "You feel righteous.\n\r", victim );
    if ( ch != victim )
        act("You grant $N the favor of your god.",ch,NULL,victim,TO_CHAR);
    return;
}



void spell_blindness( int sn, int level, CHAR_DATA *ch, void *vo, int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( IS_AFFECTED(victim, EFF_BLIND) || saves_spell(level,victim,DAM_OTHER))
        return;

    if (!IS_SET(victim->parts,PART_EYE)) {
        act("$N doesn't have eyes!",ch,NULL,victim,TO_CHAR);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level     = level;
    ef.location  = APPLY_HITROLL;
    ef.modifier  = -4;
    ef.duration  = 1+level;
    ef.bitvector = EFF_BLIND;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    if (IS_AFFECTED(victim,EFF_HALLUCINATION))
        send_to_char("Oh, bummer! Everything is dark! Help!\n\r",victim);
    else
        send_to_char( "You are blinded!\n\r", victim );
    act("$n appears to be blinded.",victim,NULL,NULL,TO_ROOM);
    return;
}



void spell_burning_hands(int sn,int level, CHAR_DATA *ch, void *vo, int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    static const int dam_each[] =
    {
        0,
        0,  0,  0,  0,	14,	17, 20, 23, 26, 29,
        29, 29, 30, 30,	31,	31, 32, 32, 33, 33,
        34, 34, 35, 35,	36,	36, 37, 37, 38, 38,
        39, 39, 40, 40,	41,	41, 42, 42, 43, 43,
        44, 44, 45, 45,	46,	46, 47, 47, 48, 48
    };
    int dam;

    level	= UMIN(level, sizeof(dam_each)/sizeof(dam_each[0]) - 1);
    level	= UMAX(0, level);
    dam		= number_range( dam_each[level] / 2, dam_each[level] * 2 );
    if ( saves_spell( level, victim,DAM_FIRE) )
        dam /= 2;
    damage( ch, victim, dam, sn, DAM_FIRE,TRUE, NULL);
    return;
}



void spell_call_lightning( int sn, int level,CHAR_DATA *ch,void *vo,int target)
{
    CHAR_DATA *vch;
    CHAR_DATA *vch_next;
    int dam;
    char *god=players_god(ch);

    if ( !IS_OUTSIDE(ch) )
    {
        send_to_char( "You must be out of doors.\n\r", ch );
        return;
    }

    if ( weather_info.sky < SKY_RAINING )
    {
        send_to_char( "You need bad weather.\n\r", ch );
        return;
    }

    dam = dice(level/2, 8);

    act( "$t's lightning strikes your foes!", ch, god, NULL, TO_CHAR );
    act( "$n calls $t's lightning to strike $s foes!", ch, god, NULL, TO_ROOM );

    for ( vch = char_list; vch != NULL; vch = vch_next )
    {
        vch_next	= vch->next;
        if ( vch->in_room == NULL )
            continue;
        if ( vch->in_room == ch->in_room )
        {
            if ( vch != ch && ( IS_NPC(ch) ? !IS_NPC(vch) : IS_NPC(vch) ) )
                damage( ch, vch, saves_spell( level,vch,DAM_LIGHTNING)
                        ? dam / 2 : dam, sn,DAM_LIGHTNING,TRUE, NULL);
            continue;
        }

        if ( vch->in_room->area == ch->in_room->area
             &&   IS_OUTSIDE(vch)
             &&   IS_AWAKE(vch) )
            send_to_char( "Lightning flashes in the sky.\n\r", vch );
    }

    return;
}

/* RT calm spell stops all fighting in the room */

void spell_calm( int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    CHAR_DATA *vch;
    int mlevel = 0;
    int count = 0;
    int high_level = 0;
    int chance;
    EFFECT_DATA ef;

    /* get sum of all mobile levels in the room */
    for (vch = ch->in_room->people; vch != NULL; vch = vch->next_in_room)
    {
        if (vch->position == POS_FIGHTING)
        {
            count++;
            if (IS_NPC(vch))
                mlevel += vch->level;
            else
                mlevel += vch->level/2;
            high_level = UMAX(high_level,vch->level);
        }
    }

    /* compute chance of stopping combat */
    chance = 4 * level - high_level + 2 * count;

    if (IS_IMMORTAL(ch)) /* always works */
        mlevel = 0;

    if (number_range(0, chance) >= mlevel)  /* hard to stop large fights */
    {
        for (vch = ch->in_room->people; vch != NULL; vch = vch->next_in_room)
        {
            if (IS_NPC(vch) && (STR_IS_SET(vch->strbit_imm_flags,IMM_MAGIC) ||
                                STR_IS_SET(vch->strbit_act,ACT_UNDEAD)))
                return;

            if (IS_AFFECTED(vch,EFF_CALM) || IS_AFFECTED(vch,EFF_BERSERK)
                    ||  is_affected(vch,gsn_frenzy) || is_affected(vch,gsn_berserk))
                return;

            send_to_char("A wave of calm passes over you.\n\r",vch);

            if (vch->fighting || vch->position == POS_FIGHTING)
                stop_fighting(vch,FALSE);

            ZEROVAR(&ef,EFFECT_DATA);
            ef.where = TO_EFFECTS;
            ef.type = sn;
            ef.level = level;
            ef.duration = level/4;
            ef.location = APPLY_HITROLL;
            if (IS_PC(vch))
                ef.modifier = -5;
            else
                ef.modifier = -2;
            ef.bitvector = EFF_CALM;
            ef.casted_by = ch?ch->id:0;
            effect_to_char(vch,&ef);

            ef.location = APPLY_DAMROLL;
            effect_to_char(vch,&ef);
        }
    }
}

void spell_cancellation( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    bool found = FALSE;

    level += 2;

    if ((!IS_NPC(ch) && IS_NPC(victim) &&
         !(IS_AFFECTED(victim, EFF_CHARM) && victim->master == ch) ) ||
            (IS_NPC(ch) && !IS_NPC(victim)) )
    {
        send_to_char("You failed, try dispel magic.\n\r",ch);
        return;
    }

    /* unlike dispel magic, the victim gets NO save */

    /* begin running through the spells */

    if (check_dispel(level,victim,gsn_armor))
        found = TRUE;

    if (check_dispel(level,victim,gsn_bless))
        found = TRUE;

    if (check_dispel(level,victim,gsn_blindness))
        found = TRUE;

    if (check_dispel(level,victim,gsn_calm))
        found = TRUE;

    if (check_dispel(level,victim,gsn_change_sex)) {
        act("$n looks more like $mself again.",victim,NULL,NULL,TO_ROOM);
        found = TRUE;
    }

    if (check_dispel(level,victim,gsn_charm_person))
    {
        found = TRUE;
        //	stop_follower(victim);
    }

    if (check_dispel(level,victim,gsn_chill_touch))
        found = TRUE;

    if (check_dispel(level,victim,gsn_curse))
        found = TRUE;

    if (check_dispel(level,victim,gsn_detect_curse))
        found = TRUE;

    if (check_dispel(level,victim,gsn_detect_evil))
        found = TRUE;

    if (check_dispel(level,victim,gsn_detect_good))
        found = TRUE;

    if (check_dispel(level,victim,gsn_detect_hidden))
        found = TRUE;

    if (check_dispel(level,victim,gsn_detect_invis))
        found = TRUE;

    if (check_dispel(level,victim,gsn_detect_magic))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_sound))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_holy))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_light))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_energy))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_lightning))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_silver))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_iron))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_cold))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_fire))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_disease))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_poison))
        found = TRUE;

    if (check_dispel(level,victim,gsn_faerie_fire))
        found = TRUE;

    if (check_dispel(level,victim,gsn_fly))
        found = TRUE;

    if (check_dispel(level,victim,gsn_frenzy))
        found = TRUE;

    if (check_dispel(level,victim,gsn_giant_strength))
        found = TRUE;

    if (check_dispel(level,victim,gsn_hallucination))
        found = TRUE;

    if (check_dispel(level,victim,gsn_haste))
        found = TRUE;

    if (check_dispel(level,victim,gsn_infravision))
        found = TRUE;

    if (check_dispel(level,victim,gsn_invisibility))
        found = TRUE;

    if (check_dispel(level,victim,gsn_mass_invis))
        found = TRUE;

    if (check_dispel(level,victim,gsn_pass_door))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_evil))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_good))
        found = TRUE;

    if (check_dispel(level,victim,gsn_sanctuary))
        found = TRUE;

    if (check_dispel(level,victim,gsn_shield))
        found = TRUE;

    if (check_dispel(level,victim,gsn_sleep))
        found = TRUE;

    if (check_dispel(level,victim,gsn_slow))
        found = TRUE;

    if (check_dispel(level,victim,gsn_stone_skin))
        found = TRUE;

    if (check_dispel(level,victim,gsn_weaken))
        found = TRUE;

    if (check_dispel(level,victim,gsn_wild_shield))
        found = TRUE;

    if (check_dispel(level,victim,gsn_chaos_shield))
        found = TRUE;

    if (check_dispel(level,victim,gsn_water_walk))
        found = TRUE;

    if (check_dispel(level,victim,gsn_entangle))
        found = TRUE;

    if (check_dispel(level,victim,gsn_hunger))
        found = TRUE;

    if (check_dispel(level,victim,gsn_thirst))
        found = TRUE;

    if (found)
        send_to_char("Ok.\n\r",ch);
    else
        send_to_char("Spell failed.\n\r",ch);
}

void spell_cause_light( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    damage( ch, (CHAR_DATA *) vo, dice(1, 8) + level / 3, sn,DAM_HARM,TRUE, NULL);
    return;
}



void spell_cause_critical(int sn,int level,CHAR_DATA *ch,void *vo,int target)
{
    damage( ch, (CHAR_DATA *) vo, dice(3, 8) + level - 6, sn,DAM_HARM,TRUE, NULL);
    return;
}



void spell_cause_serious(int sn,int level,CHAR_DATA *ch,void *vo,int target)
{
    damage( ch, (CHAR_DATA *) vo, dice(2, 8) + level / 2, sn,DAM_HARM,TRUE, NULL);
    return;
}

void spell_chain_lightning(int sn,int level,CHAR_DATA *ch, void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    CHAR_DATA *tmp_vict,*last_vict,*next_vict;
    bool found;
    int dam;

    /* first strike */

    act("A lightning bolt leaps from $n's hand and arcs to $N.",
        ch,NULL,victim,TO_ROOM);
    act("A lightning bolt leaps from your hand and arcs to $N.",
        ch,NULL,victim,TO_CHAR);
    act("A lightning bolt leaps from $n's hand and hits you!",
        ch,NULL,victim,TO_VICT);

    dam = dice(level,6);
    if (saves_spell(level,victim,DAM_LIGHTNING))
        dam /= 3;
    damage(ch,victim,dam,sn,DAM_LIGHTNING,TRUE, NULL);
    last_vict = victim;
    level -= 4;   /* decrement damage */

    /* new targets */
    while (level > 0)
    {
        found = FALSE;
        for (tmp_vict = ch->in_room?ch->in_room->people:NULL;
             tmp_vict != NULL;
             tmp_vict = next_vict)
        {
            next_vict = tmp_vict->next_in_room;
            if (!is_safe_spell(ch,tmp_vict,TRUE) && tmp_vict != last_vict)
            {
                found = TRUE;
                last_vict = tmp_vict;
                act("The bolt arcs to $n!",tmp_vict,NULL,NULL,TO_ROOM);
                act("The bolt hits you!",tmp_vict,NULL,NULL,TO_CHAR);
                dam = dice(level,6);
                if (saves_spell(level,tmp_vict,DAM_LIGHTNING))
                    dam /= 3;
                damage(ch,tmp_vict,dam,sn,DAM_LIGHTNING,TRUE, NULL);
                level -= 4;  /* decrement damage */
            }
        }   /* end target searching loop */

        if (!found) /* no target found, hit the caster */
        {
            if (ch == NULL)
                return;

            if (last_vict == ch) /* no double hits */
            {
                act("The bolt seems to have fizzled out.",ch,NULL,NULL,TO_ROOM);
                act("The bolt grounds out through your body.",
                    ch,NULL,NULL,TO_CHAR);
                return;
            }

            last_vict = ch;
            act("The bolt arcs to $n...whoops!",ch,NULL,NULL,TO_ROOM);
            send_to_char("You are struck by your own lightning!\n\r",ch);
            dam = dice(level,6);
            if (saves_spell(level,ch,DAM_LIGHTNING))
                dam /= 3;
            damage(ch,ch,dam,sn,DAM_LIGHTNING,TRUE, NULL);
            level -= 4;  /* decrement damage */
            if (ch == NULL || !IS_VALID(ch))
                return;
        }
        /* now go back and find more targets */
    }
}


void wearoff_change_sex( CHAR_DATA *ch ) {
    act("Your body feels familiar again.",ch,NULL,NULL,TO_CHAR);
    act("$n looks like $mself again.",ch,NULL,ch,TO_NOTVICT);
    return;
}
void spell_change_sex( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( is_affected( victim, sn ))
    {
        if (victim == ch)
            send_to_char("You've already been changed.\n\r",ch);
        else
            act("$N has already had $s(?) sex changed.",ch,NULL,victim,TO_CHAR);
        return;
    }
    if (saves_spell(level , victim,DAM_OTHER))
        return;

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = 2 * level;
    ef.location  = APPLY_SEX;
    ef.modifier  = 1;			// just add one, that's possible with
    ef.bitvector = EFF_CHANGESEX;	// the new system
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char( "You feel different.\n\r", victim );
    act("$n doesn't look like $mself anymore...",victim,NULL,NULL,TO_ROOM);
    return;
}



void spell_charm_person( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if (is_safe(ch,victim)) return;

    if ( victim == ch )
    {
        send_to_char( "You like yourself even better!\n\r", ch );
        return;
    }

    //
    // if you are charming somebody who is fighting, attack him also.
    //
    if (victim->position==POS_FIGHTING) {
        check_killer(victim,ch);
        multi_hit( victim, ch, TYPE_UNDEFINED );
        return;
    }

    if ( IS_AFFECTED(victim, EFF_CHARM)
         ||   IS_AFFECTED(ch, EFF_CHARM)
         ||   level < victim->level
         ||   STR_IS_SET(victim->strbit_imm_flags,IMM_CHARM)
         ||   saves_spell( level, victim,DAM_CHARM) )
        return;


    if (STR_IS_SET(victim->in_room->strbit_room_flags,ROOM_LAW)) {
        send_to_char(
                    "The mayor does not allow charming in the city limits.\n\r",ch);
        return;
    }

    if ( victim->master )
        stop_follower( victim );
    add_follower( victim, ch );
    victim->leader = ch;

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level	 = level;
    ef.duration  = number_fuzzy( level / 4 );
    ef.location  = 0;
    ef.modifier  = 0;
    ef.bitvector = EFF_CHARM;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    act( "Isn't $n just so nice?", ch, NULL, victim, TO_VICT );
    if ( ch != victim )
        act("$N looks at you with adoring eyes.",ch,NULL,victim,TO_CHAR);
    return;
}



void spell_chill_touch( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    static const int dam_each[] =
    {
        0,
        0,  0,  6,  7,  8,	 9, 12, 13, 13, 13,
        14, 14, 14, 15, 15,	15, 16, 16, 16, 17,
        17, 17, 18, 18, 18,	19, 19, 19, 20, 20,
        20, 21, 21, 21, 22,	22, 22, 23, 23, 23,
        24, 24, 24, 25, 25,	25, 26, 26, 26, 27
    };
    EFFECT_DATA ef;
    int dam;

    level	= UMIN(level, sizeof(dam_each)/sizeof(dam_each[0]) - 1);
    level	= UMAX(0, level);
    dam		= number_range( dam_each[level] / 2, dam_each[level] * 2 );
    if ( !saves_spell( level, victim,DAM_COLD ) )
    {
        act("$n turns blue and shivers.",victim,NULL,NULL,TO_ROOM);

        ZEROVAR(&ef,EFFECT_DATA);
        ef.where     = TO_EFFECTS;
        ef.type      = sn;
        ef.level     = level;
        ef.duration  = 6;
        ef.location  = APPLY_STR;
        ef.modifier  = -1;
        ef.bitvector = EFF_CHILLTOUCH;
        ef.casted_by = ch?ch->id:0;
        effect_join( victim, &ef );
    }
    else
    {
        dam /= 2;
    }

    damage( ch, victim, dam, sn, DAM_COLD,TRUE , NULL);
    return;
}



void spell_colour_spray( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    static const int dam_each[] =
    {
        0,
        0,  0,  0,  0,  0,	 0,  0,  0,  0,  0,
        30, 35, 40, 45, 50,	55, 55, 55, 56, 57,
        58, 58, 59, 60, 61,	61, 62, 63, 64, 64,
        65, 66, 67, 67, 68,	69, 70, 70, 71, 72,
        73, 73, 74, 75, 76,	76, 77, 78, 79, 79
    };
    int dam;

    level	= UMIN(level, sizeof(dam_each)/sizeof(dam_each[0]) - 1);
    level	= UMAX(0, level);
    dam		= number_range( dam_each[level] / 2,  dam_each[level] * 2 );
    if ( saves_spell( level, victim,DAM_LIGHT) )
        dam /= 2;
    else
        spell_blindness(gsn_blindness,level/2,ch,(void *) victim,TARGET_CHAR);

    damage( ch, victim, dam, sn, DAM_LIGHT,TRUE , NULL);
    return;
}



void spell_continual_light(int sn,int level,CHAR_DATA *ch,void *vo,int target)
{
    OBJ_DATA *light;

    if (target_name[0] != '\0')  /* do a glow on some object */
    {
        light = get_obj_carry(ch,target_name,ch);

        if (light == NULL)
        {
            send_to_char("You don't see that here.\n\r",ch);
            return;
        }

        if (IS_OBJ_STAT(light,ITEM_GLOW))
        {
            act("$p is already glowing.",ch,light,NULL,TO_CHAR);
            return;
        }

        STR_SET_BIT(light->strbit_extra_flags,ITEM_GLOW);
        act("$p glows with a white light.",ch,light,NULL,TO_ALL);
        return;
    }

    light = create_object( get_obj_index( OBJ_VNUM_LIGHT_BALL ), 0 );

    if (number_percent()<10*(get_curr_stat(ch,STAT_DEX)-15)) {
        obj_to_char(light,ch);
        op_load_trigger(light,ch);
        act( "$n twiddles $s thumbs and $p appears in $s hands.",
             ch, light, NULL, TO_ROOM );
        act( "You twiddle your thumbs and $p appears in your hands.",
             ch, light, NULL, TO_CHAR );
    } else {
        obj_to_room( light, ch->in_room );
        op_load_trigger(light,ch);
        act( "$n twiddles $s thumbs and $p appears in the room.",
             ch, light, NULL, TO_ROOM );
        act( "You twiddle your thumbs and $p appears in the room.",
             ch, light, NULL, TO_CHAR );
        if (ch->in_room->sector_type==SECT_AIR) {
            if (ch->in_room->sector_type==SECT_AIR)
                object_drop_air(light);
            else if (number_percent()<10)
                object_drop_fall(light);
        }
    }
    return;
}



void spell_control_weather(int sn,int level,CHAR_DATA *ch,void *vo,int target)
{
    if ( !str_cmp( target_name, "better" ) )
        weather_info.change += dice( level / 3, 4 );
    else if ( !str_cmp( target_name, "worse" ) )
        weather_info.change -= dice( level / 3, 4 );
    else
        send_to_char ("Do you want it to get better or worse?\n\r", ch );

    send_to_char( "Ok.\n\r", ch );
    return;
}



void event_create_food(EVENT_DATA *event) {
    OBJ_DATA *		RandFood;
    OBJ_INDEX_DATA *	RandIndex;
    int vnums[]={
        OBJ_VNUM_CREATEFOOD1,
        OBJ_VNUM_CREATEFOOD2,
        OBJ_VNUM_CREATEFOOD3,
        OBJ_VNUM_CREATEFOOD4,
        OBJ_VNUM_CREATEFOOD5
    };
    int vnums_wrong[]={
        OBJ_VNUM_SEVERED_HEAD,
        OBJ_VNUM_TORN_HEART,
        OBJ_VNUM_SLICED_ARM,
        OBJ_VNUM_SLICED_LEG,
        OBJ_VNUM_GUTS,
        OBJ_VNUM_BRAINS
    };
    bool	wrong_spell=FALSE;
    CHAR_DATA *	ch=event->ch;
    int 	level=*(int *)event->data;

    if (!skillcheck2(ch,gsn_create_food,96) && (number_percent()<10)) {
        RandIndex = get_obj_index( vnums_wrong[number_range(0,(sizeof(vnums_wrong)/sizeof(*vnums_wrong))-1)] );
        wrong_spell=TRUE;
    } else
        RandIndex = get_obj_index( vnums[number_range(0,(sizeof(vnums)/sizeof(*vnums))-1)] );
    if(RandIndex==NULL) RandIndex=get_obj_index( OBJ_VNUM_MUSHROOM );

    RandFood = create_object( RandIndex, 0 );
    if (wrong_spell) {
        char buf[MAX_STRING_LENGTH];

        sprintf( buf, RandFood->short_descr, "a human" );
        free_string( RandFood->short_descr );
        RandFood->short_descr = str_dup( buf );

        sprintf( buf, RandFood->description, "a human" );
        free_string( RandFood->description );
        RandFood->description = str_dup( buf );
        RandFood->value[3] = 1;	/* poison */

        RandFood->timer=30;

        act("Oh dear... you think that something has gone wrong...\n\r",
            ch,NULL,NULL,TO_CHAR);
        act("$n turns grey as $e sees what $e did.",ch,NULL,NULL,TO_ROOM);
    }

    RandFood->value[0] = level / 2;
    RandFood->value[1] = level;
    if (number_percent()<10*(get_curr_stat(ch,STAT_DEX)-15)) {
        obj_to_char(RandFood,ch);
        act( "$p suddenly appears and $n catches it.",
             ch, RandFood, NULL, TO_ROOM );
        act( "$p suddenly appears and you catch it.",
             ch, RandFood, NULL, TO_CHAR );
    } else {
        obj_to_room( RandFood, ch->in_room );
        act( "$p suddenly appears, $n fumbles and it falls on the ground.",
             ch, RandFood, NULL, TO_ROOM );
        act( "$p suddenly appears and falls on the ground.",
             ch, RandFood, NULL, TO_CHAR );
        if (ch->in_room->sector_type==SECT_AIR) {
            if (ch->in_room->sector_type==SECT_AIR)
                object_drop_air(RandFood);
            else if (number_percent()<10)
                object_drop_fall(RandFood);
        }
    }
    return;
}

void spell_create_food( int sn, int level, CHAR_DATA *ch, void *vo,int target) {
    int *	plevel;

    plevel=(int *)calloc(1,sizeof(int));
    *plevel=level;
    create_event(2,ch,NULL,NULL,event_create_food,plevel);

    send_to_char("You hold your hands in front of you...\n\r",ch);
    act("$n holds $s hands in front of $m...",ch,NULL,NULL,TO_ROOM);
}

void spell_create_hut( int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    OBJ_DATA *Gate;
    OBJ_INDEX_DATA *GateIndex;
    int vnums[]={
        OBJ_VNUM_HUT1, OBJ_VNUM_HUT2, OBJ_VNUM_HUT3, OBJ_VNUM_HUT4
    };
    int HutNum=0;

    if (level>60) HutNum=3;
    else if (level>30) HutNum=2;
    else if (level>10) HutNum=1;
    while ((HutNum) && (!skillcheck2(ch,sn,200)))
        HutNum--;
    GateIndex = get_obj_index( vnums[HutNum] );
    if(GateIndex==NULL) GateIndex=get_obj_index( OBJ_VNUM_MUSHROOM );

    Gate = create_object( GateIndex, 0 );

    Gate->timer=level*2;
    obj_to_room( Gate, ch->in_room );
    act( "$p suddenly appears.",
         ch, Gate, NULL, TO_ROOM );
    act( "$p suddenly appears.",
         ch, Gate, NULL, TO_CHAR );
    if (ch->in_room->sector_type==SECT_AIR) {
        if (ch->in_room->sector_type==SECT_AIR)
            object_drop_air(Gate);
        else if (number_percent()<10)
            object_drop_fall(Gate);
    }
    op_load_trigger(Gate,ch);
    return;

}

void spell_create_rose( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    OBJ_DATA *rose;
    char s[MSL];
    bool faded=FALSE;
    EXTRA_DESCR_DATA *ed;
    int whichcolor;
    const char *pcolour[]={""," red"," green"," cyan"," blue"," yellow"," magenta"," gray"," white"};
    const char *colour[]={"","{R","{G","{C","{B","{Y","{M","{D","{W"};
    const char *meaning[]={
        "with a soft and romantic fragrance.",
        "with the feeling for love, respect and courage.",
        "with a soft and romantic fragrance.",
        "with a soft and romantic fragrance.",
        "with a soft and romantic fragrance.",
        "with the feeling for joy, friendship, jealousy, gladness and freedom.",
        "with the feeling for enchantment.",
        "with a soft and romantic fragrance.",
        "with the feeling for innocence, secrecy and purity.",

    };

    one_argument(target_name,s);
    whichcolor=which_keyword(target_name,"red","green","cyan","blue",
                             "yellow","magenta","gray","white",NULL);
    if (whichcolor==0)
        faded=TRUE;
    if (whichcolor==-1)
        whichcolor=0;

    rose = create_object(get_obj_index(OBJ_VNUM_ROSE), 0);
    free_string(rose->short_descr);
    free_string(rose->description);

    if (faded)
        strcpy(s,"$n has created a faded rose.");
    else
        sprintf(s,"$n has created a beautiful%s%s rose{x.",
                colour[whichcolor],pcolour[whichcolor]);
    act(s,ch,rose,NULL,TO_ROOM);

    if (faded)
        strcpy(s,"You create a faded rose. Yech.\n\r");
    else
        sprintf(s,"You create a beautiful%s%s rose{x.\n\r",
                colour[whichcolor],pcolour[whichcolor]);
    send_to_char(s,ch);

    ed=new_extra_descr();
    ed->next=NULL;
    if (faded)
        strcpy(s,"A faded rose, beauty is a past of it.\n\r");
    else
        sprintf(s,"It's a beautiful%s%s rose{x, %s\n\r",
                colour[whichcolor],pcolour[whichcolor],meaning[whichcolor]);
    ed->description=str_dup(s);
    if (faded)
        strcpy(s,"faded rose");
    else
        sprintf(s,"rose%s",pcolour[whichcolor]);
    ed->keyword=str_dup(s);
    rose->extra_descr=ed;

    if (faded)
        strcpy(s,"a faded rose");
    else
        sprintf(s,"a beautiful%s%s rose{x",
                colour[whichcolor],pcolour[whichcolor]);
    rose->short_descr=str_dup(s);

    if (faded)
        strcpy(s,"A faded rose is lying on the ground.");
    else
        sprintf(s,"A%s%s rose{x is lying on the ground.",
                pcolour[whichcolor],colour[whichcolor]);
    rose->description=str_dup(s);

    obj_to_char(rose,ch);
    op_load_trigger(rose,ch);
    return;
}

void event_create_spring(EVENT_DATA *event) {
    CHAR_DATA *ch=event->ch;
    OBJ_DATA *spring;
    int *plevel=event->data;

    if (!IS_VALID(ch))
        return;

    spring = create_object( get_obj_index( OBJ_VNUM_SPRING ), 0 );
    spring->timer = *plevel;
    obj_to_room( spring, ch->in_room );
    act( "$p flows from the ground.", ch, spring, NULL, TO_ROOM );
    act( "$p flows from the ground.", ch, spring, NULL, TO_CHAR );
    op_load_trigger(spring,ch);
}
void spell_create_spring(int sn,int level,CHAR_DATA *ch,void *vo,int target)
{
    int *plevel;

    plevel=(int *)calloc(1,sizeof(int));
    *plevel=level;
    create_event(1,ch,NULL,NULL,event_create_spring,plevel);

    act( "You hold your hands close above the ground...",ch,NULL,NULL,TO_CHAR);
    act( "$n holds $s hands close above the ground...",ch,NULL,NULL,TO_ROOM);
    return;
}



void spell_create_water( int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    OBJ_DATA *obj = (OBJ_DATA *) vo;
    int water;

    if ( obj->item_type != ITEM_DRINK_CON )
    {
        send_to_char( "It is unable to hold water.\n\r", ch );
        return;
    }

    if ( obj->value[2] != LIQ_WATER && obj->value[1] != 0 )
    {
        send_to_char( "It contains some other liquid.\n\r", ch );
        return;
    }

    water = UMIN(
                level * (weather_info.sky >= SKY_RAINING ? 4 : 2),
                obj->value[0] - obj->value[1]
            );

    if ( water > 0 )
    {
        obj->value[2] = LIQ_WATER;
        obj->value[1] += water;
        if ( !is_name( "water", obj->name ) )
        {
            char buf[MAX_STRING_LENGTH];

            sprintf( buf, "%s water", obj->name );
            free_string( obj->name );
            obj->name = str_dup( buf );
        }
        if (!skillcheck2(ch,sn,96)&&(number_percent()<10)) {
            obj->value[3] = 1;
            act( "$p is filled, but it looks like blood now.",
                 ch, obj, NULL, TO_CHAR );
            act( "$n turns grey as $e sees what is in the $p.",
                 ch, obj, NULL, TO_ROOM );
        } else {
            act( "$p is filled.", ch, obj, NULL, TO_CHAR );
        }
    }

    return;
}



void spell_cure_blindness(int sn,int level,CHAR_DATA *ch,void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;

    if ( !is_affected( victim, gsn_blindness ) )
    {
        if (victim == ch)
            send_to_char("You aren't blind.\n\r",ch);
        else
            act("$N doesn't appear to be blinded.",ch,NULL,victim,TO_CHAR);
        return;
    }

    if (check_dispel(level,victim,gsn_blindness))
    {
        if (IS_AFFECTED(victim,EFF_HALLUCINATION))
            send_to_char("Far out! Everything is all cosmic again!\n\r",victim);
        else
            send_to_char( "Your vision returns!\n\r", victim );
        act("$n is no longer blinded.",victim,NULL,NULL,TO_ROOM);
        change_alignment(ch,victim,5);
    }
    else
        send_to_char("Spell failed.\n\r",ch);
}



void spell_cure_critical( int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    int heal;

    heal = dice(3, 8) + level - 6;

    if (ch->class == CLASS_CLERIC) {
        heal*=2;
    }
    if (skillcheck(ch,gsn_enhanced_healing)) {
        heal*=2;
        check_improve(ch,gsn_enhanced_healing,TRUE,1);
    } else
        check_improve(ch,gsn_enhanced_healing,FALSE,1);

    victim->hit = UMIN( victim->hit + heal, victim->max_hit );
    update_pos( victim );
    send_to_char( "You feel better!\n\r", victim );
    if ( ch != victim )
        send_to_char( "Ok.\n\r", ch );
    change_alignment(ch,victim,6);
    return;
}

/* RT added to cure plague */
void spell_cure_disease( int sn, int level, CHAR_DATA *ch,void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;

    if ( !is_affected( victim, gsn_plague ) )
    {
        if (victim == ch)
            send_to_char("You aren't ill.\n\r",ch);
        else
            act("$N doesn't appear to be diseased.",ch,NULL,victim,TO_CHAR);
        return;
    }

    if (check_dispel(level,victim,gsn_plague))
    {
        send_to_char("Your sores vanish.\n\r",victim);
        act("$n looks relieved as $s sores vanish.",victim,NULL,NULL,TO_ROOM);
        change_alignment(ch,victim,5);
    }
    else
        send_to_char("Spell failed.\n\r",ch);
}



void spell_cure_light( int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    int heal;

    heal = dice(1, 8) + level / 3;
    if (ch->class == CLASS_CLERIC) {
        heal*=2;
    }
    if (skillcheck(ch,gsn_enhanced_healing)) {
        heal*=2;
        check_improve(ch,gsn_enhanced_healing,TRUE,1);
    } else
        check_improve(ch,gsn_enhanced_healing,FALSE,1);
    victim->hit = UMIN( victim->hit + heal, victim->max_hit );
    update_pos( victim );
    send_to_char( "You feel better!\n\r", victim );
    if ( ch != victim )
        send_to_char( "Ok.\n\r", ch );
    change_alignment(ch,victim,3);
    return;
}



void spell_cure_poison( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;

    if ( !is_affected( victim, gsn_poison ) )
    {
        if (victim == ch)
            send_to_char("You aren't poisoned.\n\r",ch);
        else
            act("$N doesn't appear to be poisoned.",ch,NULL,victim,TO_CHAR);
        return;
    }

    if (check_dispel(level,victim,gsn_poison))
    {
        send_to_char("A warm feeling runs through your body.\n\r",victim);
        act("$n looks much better.",victim,NULL,NULL,TO_ROOM);
        change_alignment(ch,victim,5);
    }
    else
        send_to_char("Spell failed.\n\r",ch);
}

void spell_cure_serious( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    int heal;

    heal = dice(2, 8) + level /2 ;
    if (ch->class == CLASS_CLERIC) {
        heal*=2;
    }
    if (skillcheck(ch,gsn_enhanced_healing)) {
        heal*=2;
        check_improve(ch,gsn_enhanced_healing,TRUE,1);
    } else
        check_improve(ch,gsn_enhanced_healing,FALSE,1);
    victim->hit = UMIN( victim->hit + heal, victim->max_hit );
    update_pos( victim );
    send_to_char( "You feel better!\n\r", victim );
    if ( ch != victim )
        send_to_char( "Ok.\n\r", ch );
    return;
}



void spell_curse( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim;
    OBJ_DATA *obj;
    EFFECT_DATA ef;

    /* deal with the object case first */
    if (target == TARGET_OBJ)
    {
        obj = (OBJ_DATA *) vo;
        if (IS_OBJ_STAT(obj,ITEM_EVIL))
        {
            act("$p is already filled with evil.",ch,obj,NULL,TO_CHAR);
            return;
        }

        if (IS_OBJ_STAT(obj,ITEM_BLESS))
        {
            EFFECT_DATA *pef;

            pef = effect_find(obj->affected,TO_EFFECTS,gsn_blindness);
            if (!saves_dispel(level,pef != NULL ? pef->level : obj->level,0))
            {
                if (pef != NULL)
                    effect_remove_obj(obj,pef);
                act("$p glows with a red aura.",ch,obj,NULL,TO_ALL);
                STR_REMOVE_BIT(obj->strbit_extra_flags,ITEM_BLESS);
                return;
            }
            else
            {
                act("The holy aura of $p is too powerful for you to overcome.",
                    ch,obj,NULL,TO_CHAR);
                return;
            }
        }

        ZEROVAR(&ef,EFFECT_DATA);
        ef.where        = TO_OBJECT;
        ef.type         = sn;
        ef.level        = level;
        ef.duration     = 2 * level;
        ef.location     = APPLY_SAVES;
        ef.modifier     = +1;
        ef.bitvector    = ITEM_EVIL;
        ef.casted_by	= ch?ch->id:0;
        effect_to_obj(obj,&ef);

        act("$p glows with a malevolent aura.",ch,obj,NULL,TO_ALL);

        if (obj->wear_loc != WEAR_NONE)
            ch->saving_throw += 1;
        return;
    }

    /* character curses */
    victim = (CHAR_DATA *) vo;

    if (IS_AFFECTED(victim,EFF_CURSE) || saves_spell(level,victim,DAM_NEGATIVE))
        return;

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = 2*level;
    ef.location  = APPLY_HITROLL;
    ef.modifier  = -1 * (level / 8);
    ef.bitvector = EFF_CURSE;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    ef.location  = APPLY_SAVING_SPELL;
    ef.modifier  = level / 8;
    effect_to_char( victim, &ef );

    send_to_char( "You feel unclean.\n\r", victim );
    if ( ch != victim )
        act("$N looks very uncomfortable.",ch,NULL,victim,TO_CHAR);
    return;
}

/* RT replacement demonfire spell */

void spell_demonfire(int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    int dam;

    if ( !IS_NPC(ch) && !IS_EVIL(ch) )
    {
        victim = ch;
        send_to_char("The demons turn upon you!\n\r",ch);
    }

    ch->alignment = UMAX(-1000, ch->alignment - 50);

    if (victim != ch)
    {
        act("$n calls forth the demons of Hell upon $N!",
            ch,NULL,victim,TO_ROOM);
        act("$n has assailed you with the demons of Hell!",
            ch,NULL,victim,TO_VICT);
        send_to_char("You conjure forth the demons of hell!\n\r",ch);
    }
    dam = dice( level, 10 );
    if ( saves_spell( level, victim,DAM_NEGATIVE) )
        dam /= 2;
    damage( ch, victim, dam, sn, DAM_NEGATIVE ,TRUE, NULL);
    spell_curse(gsn_curse, 3 * level / 4, ch, (void *) victim,TARGET_CHAR);
    char_obj_alignment_check(ch);
}

void spell_detect_evil( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( IS_AFFECTED(victim, EFF_DETECT_EVIL) )
    {
        if (victim == ch)
            send_to_char("You can already sense evil.\n\r",ch);
        else
            act("$N can already detect evil.",ch,NULL,victim,TO_CHAR);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level	 = level;
    ef.duration  = level;
    ef.modifier  = 0;
    ef.location  = APPLY_NONE;
    ef.bitvector = EFF_DETECT_EVIL;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char( "Your eyes tingle.\n\r", victim );
    if ( ch != victim )
        send_to_char( "Ok.\n\r", ch );
    return;
}


void spell_detect_good( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( IS_AFFECTED(victim, EFF_DETECT_GOOD) )
    {
        if (victim == ch)
            send_to_char("You can already sense good.\n\r",ch);
        else
            act("$N can already detect good.",ch,NULL,victim,TO_CHAR);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = level;
    ef.modifier  = 0;
    ef.location  = APPLY_NONE;
    ef.bitvector = EFF_DETECT_GOOD;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char( "Your eyes tingle.\n\r", victim );
    if ( ch != victim )
        send_to_char( "Ok.\n\r", ch );
    return;
}



void spell_detect_hidden(int sn,int level,CHAR_DATA *ch,void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( IS_AFFECTED(victim, EFF_DETECT_HIDDEN) )
    {
        if (victim == ch)
            send_to_char("You are already as alert as you can be. \n\r",ch);
        else
            act("$N can already sense hidden lifeforms.",ch,NULL,victim,TO_CHAR);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = level;
    ef.location  = APPLY_NONE;
    ef.modifier  = 0;
    ef.bitvector = EFF_DETECT_HIDDEN;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char( "Your awareness improves.\n\r", victim );
    if ( ch != victim )
        send_to_char( "Ok.\n\r", ch );
    return;
}



void spell_detect_invis( int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( IS_AFFECTED(victim, EFF_DETECT_INVIS) )
    {
        if (victim == ch)
            send_to_char("You can already see invisible.\n\r",ch);
        else
            act("$N can already see invisible things.",ch,NULL,victim,TO_CHAR);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = level;
    ef.modifier  = 0;
    ef.location  = APPLY_NONE;
    ef.bitvector = EFF_DETECT_INVIS;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char( "Your eyes tingle.\n\r", victim );
    if ( ch != victim )
        send_to_char( "Ok.\n\r", ch );
    return;
}



void spell_detect_magic( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( IS_AFFECTED(victim, EFF_DETECT_MAGIC) )
    {
        if (victim == ch)
            send_to_char("You can already sense magical auras.\n\r",ch);
        else
            act("$N can already detect magic.",ch,NULL,victim,TO_CHAR);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level	 = level;
    ef.duration  = level;
    ef.modifier  = 0;
    ef.location  = APPLY_NONE;
    ef.bitvector = EFF_DETECT_MAGIC;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char( "Your eyes tingle.\n\r", victim );
    if ( ch != victim )
        send_to_char( "Ok.\n\r", ch );
    return;
}



void spell_detect_poison( int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    OBJ_DATA *obj = (OBJ_DATA *) vo;

    if ( obj->item_type == ITEM_DRINK_CON || obj->item_type == ITEM_FOOD )
    {
        if ( obj->value[3] != 0 )
            send_to_char( "You smell poisonous fumes.\n\r", ch );
        else
            send_to_char( "It looks delicious.\n\r", ch );
    }
    else
    {
        send_to_char( "It doesn't look poisoned.\n\r", ch );
    }

    return;
}



void spell_dispel_evil( int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    int dam;

    if ( !IS_NPC(ch) && IS_EVIL(ch) )
        victim = ch;

    if ( IS_GOOD(victim) ) {
        act( "Fatal protects $N.", ch, NULL, victim, TO_ROOM );
        return;
    }

    if ( IS_NEUTRAL(victim) ) {
        act( "$N does not seem to be affected.", ch, NULL, victim, TO_CHAR );
        return;
    }

    if (victim->hit > (ch->level * 4))
        dam = dice( level, 4 );
    else
        dam = UMAX(victim->hit, dice(level,4));
    if ( saves_spell( level, victim,DAM_HOLY) )
        dam /= 2;
    damage( ch, victim, dam, sn, DAM_HOLY ,TRUE, NULL);
    return;
}


void spell_dispel_good( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    int dam;

    if ( !IS_NPC(ch) && IS_GOOD(ch) )
        victim = ch;

    if ( IS_EVIL(victim) ) {
        act( "$N is protected by $S evil.", ch, NULL, victim, TO_ROOM );
        return;
    }

    if ( IS_NEUTRAL(victim) ) {
        act( "$N does not seem to be affected.", ch, NULL, victim, TO_CHAR );
        return;
    }

    if (victim->hit > (ch->level * 4))
        dam = dice( level, 4 );
    else
        dam = UMAX(victim->hit, dice(level,4));
    if ( saves_spell( level, victim,DAM_NEGATIVE) )
        dam /= 2;
    damage( ch, victim, dam, sn, DAM_NEGATIVE ,TRUE, NULL);
    return;
}


/* modified for enhanced use */

void spell_dispel_magic( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    bool found = FALSE;

    if (saves_spell(level, victim,DAM_OTHER))
    {
        send_to_char( "You feel a brief tingling sensation.\n\r",victim);
        send_to_char( "You failed.\n\r", ch);
        return;
    }

    /* begin running through the spells */

    if (check_dispel(level,victim,gsn_armor))
        found = TRUE;

    if (check_dispel(level,victim,gsn_bless))
        found = TRUE;

    if (check_dispel(level,victim,gsn_blindness))
        found = TRUE;

    if (check_dispel(level,victim,gsn_calm))
        found = TRUE;

    if (check_dispel(level,victim,gsn_change_sex))
        found = TRUE;

    if (check_dispel(level,victim,gsn_charm_person))
    {
        found = TRUE;
        //	stop_follower(victim);
    }

    if (check_dispel(level,victim,gsn_chill_touch))
        found = TRUE;

    if (check_dispel(level,victim,gsn_curse))
        found = TRUE;

    if (check_dispel(level,victim,gsn_detect_curse))
        found = TRUE;

    if (check_dispel(level,victim,gsn_detect_evil))
        found = TRUE;

    if (check_dispel(level,victim,gsn_detect_good))
        found = TRUE;

    if (check_dispel(level,victim,gsn_detect_hidden))
        found = TRUE;

    if (check_dispel(level,victim,gsn_detect_invis))
        found = TRUE;

    if (check_dispel(level,victim,gsn_detect_magic))
        found = TRUE;

    if (check_dispel(level,victim,gsn_faerie_fire))
        found = TRUE;

    if (check_dispel(level,victim,gsn_fly))
        found = TRUE;

    if (check_dispel(level,victim,gsn_frenzy))
        found = TRUE;

    if (check_dispel(level,victim,gsn_giant_strength))
        found = TRUE;

    if (check_dispel(level,victim,gsn_haste))
        found = TRUE;

    if (check_dispel(level,victim,gsn_infravision))
        found = TRUE;

    if (check_dispel(level,victim,gsn_invisibility))
        found = TRUE;

    if (check_dispel(level,victim,gsn_mass_invis))
        found = TRUE;

    if (check_dispel(level,victim,gsn_pass_door))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_evil))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_good))
        found = TRUE;

    if (is_affected(victim,gsn_sanctuary) &&
            check_dispel(level,victim,gsn_sanctuary))
        found = TRUE;

    if (IS_AFFECTED(victim,EFF_SANCTUARY)
            && !saves_dispel(level, victim->level,-1)
            && !is_affected(victim,gsn_sanctuary))
    {
        STR_REMOVE_BIT(victim->strbit_affected_by,EFF_SANCTUARY);
        found = TRUE;
    }

    if (check_dispel(level,victim,gsn_shield))
        found = TRUE;

    if (check_dispel(level,victim,gsn_sleep))
        found = TRUE;

    if (check_dispel(level,victim,gsn_slow))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_sound))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_holy))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_light))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_energy))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_lightning))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_silver))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_iron))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_fire))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_cold))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_disease))
        found = TRUE;

    if (check_dispel(level,victim,gsn_protection_poison))
        found = TRUE;

    if (check_dispel(level,victim,gsn_stone_skin))
        found = TRUE;

    if (check_dispel(level,victim,gsn_weaken))
        found = TRUE;

    if (found)
        send_to_char("Ok.\n\r",ch);
    else
        send_to_char("Spell failed.\n\r",ch);
    return;
}

void spell_earthquake( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *vch;
    CHAR_DATA *vch_next;

    send_to_char( "The earth trembles beneath your feet!\n\r", ch );
    act( "$n makes the earth tremble and shiver.", ch, NULL, NULL, TO_ROOM );

    for ( vch = char_list; vch != NULL; vch = vch_next )
    {
        vch_next	= vch->next;
        if ( vch->in_room == NULL )
            continue;
        if ( vch->in_room == ch->in_room )
        {
            if ( vch != ch && !is_safe_spell(ch,vch,TRUE)) {
                if (IS_AFFECTED(vch,EFF_FLYING))
                    damage(ch,vch,0,sn,DAM_BASH,TRUE, NULL);
                else
                    damage( ch,vch,level + dice(2, 8), sn, DAM_BASH,TRUE, NULL);
            }
            continue;
        }

        if ( vch->in_room->area == ch->in_room->area )
            send_to_char( "The earth trembles and shivers.\n\r", vch );
    }

    return;
}

void spell_enchant_armor( int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    OBJ_DATA *obj = (OBJ_DATA *) vo;
    EFFECT_DATA *pef;
    int result, fail;
    int ac_bonus, added;
    bool ac_found = FALSE;

    if (obj->item_type != ITEM_ARMOR)
    {
        send_to_char("That isn't an armor.\n\r",ch);
        return;
    }

    if (obj->wear_loc != WEAR_NONE)
    {
        send_to_char("The item must be carried to be enchanted.\n\r",ch);
        return;
    }

    /* this means they have no bonus */
    ac_bonus = 0;
    fail = 25;	/* base 25% chance of failure */

    /* find the bonuses */

    if (!obj->enchanted)
        for ( pef = obj->pIndexData->affected; pef != NULL; pef = pef->next )
        {
            if ( pef->location == APPLY_AC )
            {
                ac_bonus = pef->modifier;
                ac_found = TRUE;
                fail += 5 * (ac_bonus * ac_bonus);
            }

            else  /* things get a little harder */
                fail += 20;
        }

    for ( pef = obj->affected; pef != NULL; pef = pef->next )
    {
        if ( pef->location == APPLY_AC )
        {
            ac_bonus = pef->modifier;
            ac_found = TRUE;
            fail += 5 * (ac_bonus * ac_bonus);
        }

        else /* things get a little harder */
            fail += 20;
    }

    /* apply other modifiers */
    fail -= level;

    if (IS_OBJ_STAT(obj,ITEM_BLESS))
        fail -= 15;
    if (IS_OBJ_STAT(obj,ITEM_GLOW))
        fail -= 5;

    fail = URANGE(5,fail,85);

    result = number_percent();

    /* the moment of truth */
    if (result < (fail / 5))  /* item destroyed */
    {
        act("$p flares blindingly... and evaporates!",ch,obj,NULL,TO_CHAR);
        act("$p flares blindingly... and evaporates!",ch,obj,NULL,TO_ROOM);
        extract_obj(obj);
        return;
    }

    if (result < (fail / 3)) /* item disenchanted */
    {
        EFFECT_DATA *pef_next;

        act("$p glows brightly, then fades...oops.",ch,obj,NULL,TO_CHAR);
        act("$p glows brightly, then fades.",ch,obj,NULL,TO_ROOM);
        obj->enchanted = TRUE;

        /* remove all effects */
        for (pef = obj->affected; pef != NULL; pef = pef_next)
        {
            pef_next = pef->next;
            free_effect(pef);
        }
        obj->affected = NULL;

        /* clear all flags */
        STR_ZERO_BIT(obj->strbit_extra_flags,MAX_FLAGS);
        return;
    }

    if ( result <= fail )  /* failed, no bad result */
    {
        send_to_char("Nothing seemed to happen.\n\r",ch);
        return;
    }

    /* okay, move all the old flags into new vectors if we have to */
    if (!obj->enchanted)
    {
        EFFECT_DATA *ef_new;
        obj->enchanted = TRUE;

        for (pef = obj->pIndexData->affected; pef != NULL; pef = pef->next)
        {
            ef_new = new_effect();

            ef_new->next = obj->affected;
            obj->affected = ef_new;

            ef_new->where	= pef->where;
            ef_new->type 	= UMAX(0,pef->type);
            ef_new->level	= pef->level;
            ef_new->duration	= pef->duration;
            ef_new->location	= pef->location;
            ef_new->modifier	= pef->modifier;
            ef_new->bitvector	= pef->bitvector;
        }
    }

    if (result <= (90 - level/5))  /* success! */
    {
        act("$p shimmers with a gold aura.",ch,obj,NULL,TO_CHAR);
        act("$p shimmers with a gold aura.",ch,obj,NULL,TO_ROOM);
        STR_SET_BIT(obj->strbit_extra_flags, ITEM_MAGIC);
        added = -1;
    }

    else  /* exceptional enchant */
    {
        act("$p glows a brillant gold!",ch,obj,NULL,TO_CHAR);
        act("$p glows a brillant gold!",ch,obj,NULL,TO_ROOM);
        STR_SET_BIT(obj->strbit_extra_flags,ITEM_MAGIC);
        STR_SET_BIT(obj->strbit_extra_flags,ITEM_GLOW);
        added = -2;
    }

    /* now add the enchantments */

    if (obj->level < LEVEL_HERO)
        obj->level = UMIN(LEVEL_HERO - 1,obj->level + 1);

    if (ac_found)
    {
        for ( pef = obj->affected; pef != NULL; pef = pef->next)
        {
            if ( pef->location == APPLY_AC)
            {
                pef->type = sn;
                pef->modifier += added;
                pef->level = UMAX(pef->level,level);
            }
        }
    }
    else /* add a new effect */
    {
        pef = new_effect();

        pef->where	= TO_OBJECT;
        pef->type	= sn;
        pef->level	= level;
        pef->duration	= -1;
        pef->location	= APPLY_AC;
        pef->modifier	=  added;
        pef->bitvector  = EFF_NONE;
        pef->next	= obj->affected;
        obj->affected	= pef;
    }

}




void spell_enchant_weapon(int sn,int level,CHAR_DATA *ch, void *vo,int target)
{
    OBJ_DATA *obj = (OBJ_DATA *) vo;
    EFFECT_DATA *pef;
    int result, fail;
    int hit_bonus, dam_bonus, added;
    bool hit_found = FALSE, dam_found = FALSE;

    if (obj->item_type != ITEM_WEAPON)
    {
        send_to_char("That isn't a weapon.\n\r",ch);
        return;
    }

    if (obj->wear_loc != WEAR_NONE)
    {
        send_to_char("The item must be carried to be enchanted.\n\r",ch);
        return;
    }

    /* this means they have no bonus */
    hit_bonus = 0;
    dam_bonus = 0;
    fail = 25;	/* base 25% chance of failure */

    /* find the bonuses */

    if (!obj->enchanted)
        for ( pef = obj->pIndexData->affected; pef != NULL; pef = pef->next )
        {
            if ( pef->location == APPLY_HITROLL )
            {
                hit_bonus = pef->modifier;
                hit_found = TRUE;
                fail += 2 * (hit_bonus * hit_bonus);
            }

            else if (pef->location == APPLY_DAMROLL )
            {
                dam_bonus = pef->modifier;
                dam_found = TRUE;
                fail += 2 * (dam_bonus * dam_bonus);
            }

            else  /* things get a little harder */
                fail += 25;
        }

    for ( pef = obj->affected; pef != NULL; pef = pef->next )
    {
        if ( pef->location == APPLY_HITROLL )
        {
            hit_bonus = pef->modifier;
            hit_found = TRUE;
            fail += 2 * (hit_bonus * hit_bonus);
        }

        else if (pef->location == APPLY_DAMROLL )
        {
            dam_bonus = pef->modifier;
            dam_found = TRUE;
            fail += 2 * (dam_bonus * dam_bonus);
        }

        else /* things get a little harder */
            fail += 25;
    }

    /* apply other modifiers */
    fail -= 3 * level/2;

    if (IS_OBJ_STAT(obj,ITEM_BLESS))
        fail -= 15;
    if (IS_OBJ_STAT(obj,ITEM_GLOW))
        fail -= 5;

    fail = URANGE(5,fail,95);

    result = number_percent();

    /* the moment of truth */
    if (result < (fail / 5))  /* item destroyed */
    {
        act("$p shivers violently and explodes!",ch,obj,NULL,TO_CHAR);
        act("$p shivers violently and explodeds!",ch,obj,NULL,TO_ROOM);
        extract_obj(obj);
        return;
    }

    if (result < (fail / 2)) /* item disenchanted */
    {
        EFFECT_DATA *pef_next;

        act("$p glows brightly, then fades...oops.",ch,obj,NULL,TO_CHAR);
        act("$p glows brightly, then fades.",ch,obj,NULL,TO_ROOM);
        obj->enchanted = TRUE;

        /* remove all effects */
        for (pef = obj->affected; pef != NULL; pef = pef_next)
        {
            pef_next = pef->next;
            free_effect(pef);
        }
        obj->affected = NULL;

        /* clear all flags */
        STR_ZERO_BIT(obj->strbit_extra_flags,MAX_FLAGS);
        return;
    }

    if ( result <= fail )  /* failed, no bad result */
    {
        send_to_char("Nothing seemed to happen.\n\r",ch);
        return;
    }

    /* okay, move all the old flags into new vectors if we have to */
    if (!obj->enchanted)
    {
        EFFECT_DATA *ef_new;
        obj->enchanted = TRUE;

        for (pef = obj->pIndexData->affected; pef != NULL; pef = pef->next)
        {
            ef_new = new_effect();

            ef_new->next = obj->affected;
            obj->affected = ef_new;

            ef_new->where	= pef->where;
            ef_new->type 	= UMAX(0,pef->type);
            ef_new->level	= pef->level;
            ef_new->duration	= pef->duration;
            ef_new->location	= pef->location;
            ef_new->modifier	= pef->modifier;
            ef_new->bitvector	= pef->bitvector;
        }
    }

    if (result <= (100 - level/5))  /* success! */
    {
        act("$p glows blue.",ch,obj,NULL,TO_CHAR);
        act("$p glows blue.",ch,obj,NULL,TO_ROOM);
        STR_SET_BIT(obj->strbit_extra_flags, ITEM_MAGIC);
        added = 1;
    }

    else  /* exceptional enchant */
    {
        act("$p glows a brillant blue!",ch,obj,NULL,TO_CHAR);
        act("$p glows a brillant blue!",ch,obj,NULL,TO_ROOM);
        STR_SET_BIT(obj->strbit_extra_flags,ITEM_MAGIC);
        STR_SET_BIT(obj->strbit_extra_flags,ITEM_GLOW);
        added = 2;
    }

    /* now add the enchantments */

    if (obj->level < LEVEL_HERO - 1)
        obj->level = UMIN(LEVEL_HERO - 1,obj->level + 1);

    if (dam_found)
    {
        for ( pef = obj->affected; pef != NULL; pef = pef->next)
        {
            if ( pef->location == APPLY_DAMROLL)
            {
                pef->type = sn;
                pef->modifier += added;
                pef->level = UMAX(pef->level,level);
                if (pef->modifier > 4)
                    STR_SET_BIT(obj->strbit_extra_flags,ITEM_HUM);
            }
        }
    }
    else /* add a new effect */
    {
        pef = new_effect();

        pef->where	= TO_OBJECT;
        pef->type	= sn;
        pef->level	= level;
        pef->duration	= -1;
        pef->location	= APPLY_DAMROLL;
        pef->modifier	=  added;
        pef->bitvector  = EFF_NONE;
        pef->next	= obj->affected;
        obj->affected	= pef;
    }

    if (hit_found)
    {
        for ( pef = obj->affected; pef != NULL; pef = pef->next)
        {
            if ( pef->location == APPLY_HITROLL)
            {
                pef->type = sn;
                pef->modifier += added;
                pef->level = UMAX(pef->level,level);
                if (pef->modifier > 4)
                    STR_SET_BIT(obj->strbit_extra_flags,ITEM_HUM);
            }
        }
    }
    else /* add a new effect */
    {
        pef = new_effect();

        pef->type       = sn;
        pef->level      = level;
        pef->duration   = -1;
        pef->location   = APPLY_HITROLL;
        pef->modifier   =  added;
        pef->bitvector  = EFF_NONE;
        pef->next       = obj->affected;
        obj->affected   = pef;
    }

}



/*
 * Drain XP, MANA, HP.
 * Caster gains HP.
 */
void spell_energy_drain( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    int dam;

    if (victim != ch)
        ch->alignment = UMAX(-1000, ch->alignment - 50);

    if ( saves_spell( level, victim,DAM_NEGATIVE) )
    {
        send_to_char("You feel a momentary chill.\n\r",victim);
        return;
    } else {


        if ( victim->level <= 2 )
        {
            dam		 = ch->hit + 1;
        }
        else
        {
            gain_exp( victim, 0 - number_range( level/2, 3 * level / 2 ), TRUE );
            victim->mana	/= 2;
            victim->move	/= 2;
            dam	                 = dice(1, level);
            ch->hit		+= dam;
        }

        send_to_char("You feel your life slipping away!\n\r",victim);
        send_to_char("Wow....what a rush!\n\r",ch);
        damage( ch, victim, dam, sn, DAM_NEGATIVE ,TRUE, NULL);
    }
    char_obj_alignment_check(ch);

    return;
}



void spell_fireball( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    static const int dam_each[] =
    {
        0,
        0,   0,   0,   0,   0,	  0,   0,   0,   0,   0,
        0,   0,   0,   0,  30,	 35,  40,  45,  50,  55,
        60,  65,  70,  75,  80,	 82,  84,  86,  88,  90,
        92,  94,  96,  98, 100,	102, 104, 106, 108, 110,
        112, 114, 116, 118, 120,	122, 124, 126, 128, 130
    };
    int dam;

    level	= UMIN(level, sizeof(dam_each)/sizeof(dam_each[0]) - 1);
    level	= UMAX(0, level);
    dam		= number_range( dam_each[level] / 2, dam_each[level] * 2 );
    if ( saves_spell( level, victim, DAM_FIRE) )
        dam /= 2;
    damage( ch, victim, dam, sn, DAM_FIRE ,TRUE, NULL);
    return;
}


void spell_fireproof(int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    OBJ_DATA *obj = (OBJ_DATA *) vo;
    EFFECT_DATA ef;

    if (IS_OBJ_STAT(obj,ITEM_BURN_PROOF))
    {
        act("$p is already protected from burning.",ch,obj,NULL,TO_CHAR);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_OBJECT;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = number_fuzzy(level / 4);
    ef.location  = APPLY_NONE;
    ef.modifier  = 0;
    ef.bitvector = ITEM_BURN_PROOF;
    ef.casted_by = ch?ch->id:0;
    effect_to_obj(obj,&ef);

    act("You protect $p from fire.",ch,obj,NULL,TO_CHAR);
    act("$p is surrounded by a protective aura.",ch,obj,NULL,TO_ROOM);
}



void spell_freezeproof(int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    OBJ_DATA *obj = (OBJ_DATA *) vo;
    EFFECT_DATA ef;

    if (IS_OBJ_STAT(obj,ITEM_FREEZE_PROOF))
    {
        act("$p is already protected from freezing.",ch,obj,NULL,TO_CHAR);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_OBJECT;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = number_fuzzy(level / 4);
    ef.location  = APPLY_NONE;
    ef.modifier  = 0;
    ef.bitvector = ITEM_FREEZE_PROOF;
    ef.casted_by = ch?ch->id:0;
    effect_to_obj(obj,&ef);

    act("You protect $p from cold.",ch,obj,NULL,TO_CHAR);
    act("$p is surrounded by a protective aura.",ch,obj,NULL,TO_ROOM);
}



void spell_flamestrike( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    int dam;

    dam = dice(6 + level / 2, 8);
    if ( saves_spell( level, victim,DAM_FIRE) )
        dam /= 2;
    damage( ch, victim, dam, sn, DAM_FIRE ,TRUE, NULL);
    return;
}



void spell_faerie_fire( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( IS_AFFECTED(victim, EFF_FAERIE_FIRE) )
        return;

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level	 = level;
    ef.duration  = level;
    ef.location  = APPLY_AC;
    ef.modifier  = 2 * level;
    ef.bitvector = EFF_FAERIE_FIRE;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char( "You are surrounded by a pink outline.\n\r", victim );
    act( "$n is surrounded by a pink outline.", victim, NULL, NULL, TO_ROOM );
    return;
}



void spell_faerie_fog( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *ich;

    act( "$n conjures a cloud of purple smoke.", ch, NULL, NULL, TO_ROOM );
    send_to_char( "You conjure a cloud of purple smoke.\n\r", ch );

    for ( ich = ch->in_room->people; ich != NULL; ich = ich->next_in_room )
    {
        if (ich->invis_level > 0)
            continue;

        if ( ich==ch || saves_spell( level, ich,DAM_OTHER) )
            continue;

        effect_strip ( ich, gsn_invisibility		);
        effect_strip ( ich, gsn_mass_invis		);
        effect_strip ( ich, gsn_sneak			);
        STR_REMOVE_BIT( ich->strbit_affected_by, EFF_HIDE	);
        STR_REMOVE_BIT( ich->strbit_affected_by, EFF_INVISIBLE	);
        STR_REMOVE_BIT( ich->strbit_affected_by, EFF_SNEAK	);

        act( "$n is revealed!", ich, NULL, NULL, TO_ROOM );
        send_to_char( "You are revealed!\n\r", ich );
    }

    return;
}

void spell_floating_disc( int sn, int level,CHAR_DATA *ch,void *vo,int target )
{
    OBJ_DATA *disc, *floating;

    floating = get_eq_char(ch,WEAR_FLOAT);
    if (floating != NULL && IS_OBJ_STAT(floating,ITEM_NOREMOVE))
    {
        act("You can't remove $p.",ch,floating,NULL,TO_CHAR);
        return;
    }

    disc = create_object(get_obj_index(OBJ_VNUM_DISC), 0);
    disc->value[0]	= ch->level * 10; /* 10 pounds per level capacity */
    disc->value[3]	= ch->level * 5; /* 5 pounds per level max per item */
    disc->timer		= ch->level * 2 - number_range(0,level / 2);

    act("$n has created a floating black disc.",ch,NULL,NULL,TO_ROOM);
    send_to_char("You create a floating disc.\n\r",ch);
    obj_to_char(disc,ch);
    wear_obj(ch,disc,TRUE,TRUE);
    op_load_trigger(disc,ch);
    return;
}


void spell_fly( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( IS_AFFECTED(victim, EFF_FLYING) )
    {
        if (victim == ch)
            send_to_char("You are already airborne.\n\r",ch);
        else
            act("$N doesn't need your help to fly.",ch,NULL,victim,TO_CHAR);
        return;
    }

    if (is_affected(victim,gsn_entangle)) {
        if (victim == ch)
            send_to_char("You are bound to the ground!\n\r",ch);
        else
            act("$N has $s feet entangled to the ground.",
                ch,NULL,victim,TO_CHAR);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level	 = level;
    ef.duration  = level + 3;
    ef.location  = 0;
    ef.modifier  = 0;
    ef.bitvector = EFF_FLYING;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    if (IS_AFFECTED(victim,EFF_HALLUCINATION)) {
        send_to_char("Up, up, and awaaaay! You're walking on air!\n\r",victim);
    } else {
        if (IS_SET(victim->parts,PART_FEET))
            send_to_char( "Your feet rise off the ground.\n\r", victim );
        else
            send_to_char( "You rise off the ground.\n\r", victim );
    }
    if (IS_SET(victim->parts,PART_FEET))
        act( "$n's feet rise off the ground.", victim, NULL, NULL, TO_ROOM );
    else
        act( "$n rises off the ground.", victim, NULL, NULL, TO_ROOM );
    return;
}

/* RT clerical berserking spell */

void spell_frenzy(int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if (is_affected(victim,sn) || IS_AFFECTED(victim,EFF_BERSERK))
    {
        if (victim == ch)
            send_to_char("You are already in a frenzy.\n\r",ch);
        else
            act("$N is already in a frenzy.",ch,NULL,victim,TO_CHAR);
        return;
    }

    if (is_affected(victim,gsn_calm))
    {
        if (victim == ch)
            send_to_char("Why don't you just relax for a while?\n\r",ch);
        else
            act("$N doesn't look like $e wants to fight anymore.",
                ch,NULL,victim,TO_CHAR);
        return;
    }

    if ((IS_GOOD(ch) && !IS_GOOD(victim)) ||
            (IS_NEUTRAL(ch) && !IS_NEUTRAL(victim)) ||
            (IS_EVIL(ch) && !IS_EVIL(victim))
            )
    {
        act("Your god doesn't seem to like $N",ch,NULL,victim,TO_CHAR);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type 	 = sn;
    ef.level	 = level;
    ef.duration	 = level / 3;
    ef.modifier  = level / 6;
    ef.bitvector = EFF_FRENZY;
    ef.casted_by = ch?ch->id:0;
    ef.location  = APPLY_HITROLL;
    effect_to_char(victim,&ef);

    ef.location  = APPLY_DAMROLL;
    effect_to_char(victim,&ef);

    ef.modifier  = 10 * (level / 12);
    ef.location  = APPLY_AC;
    effect_to_char(victim,&ef);

    send_to_char("You are filled with holy wrath!\n\r",victim);
    act("$n gets a wild look in $s eyes!",victim,NULL,NULL,TO_ROOM);
}

/* RT ROM-style gate */

struct gate_data {
    char *target_name;
    int  level;
};
void event_gate_gothrough(EVENT_DATA *event) {
    CHAR_DATA *ch=event->ch;
    CHAR_DATA *victim=event->victim;
    ROOM_INDEX_DATA *from_room=ch->in_room;
    bool gate_pet=FALSE;

    if (!IS_VALID(victim)) {
        send_to_char("The gate collapses just before you enter.\n\r",ch);
        act("The gate collapses just before $n enters.",ch,NULL,NULL,TO_ROOM);
        return;
    }

    if (ch->pet != NULL
            &&  ch->in_room == ch->pet->in_room
            &&  ch->pet->position==POS_STANDING)
        gate_pet = TRUE;

    act("$n steps through the gate and vanishes.",ch,NULL,NULL,TO_ROOM);
    send_to_char("You step through the gate and vanish.\n\r",ch);
    ap_leave_trigger(ch,victim->in_room);
    char_from_room(ch);
    char_to_room(ch,victim->in_room);
    ap_enter_trigger(ch,from_room);

    act("$n has arrived through the gate.",ch,NULL,NULL,TO_ROOM);
    look_room(ch,ch->in_room,TRUE);

    if (gate_pet) {
        act("$n steps through the gate and vanishes.",ch->pet,NULL,NULL,TO_ROOM);
        send_to_char("You step through the gate and vanish.\n\r",ch->pet);
        char_from_room(ch->pet);
        char_to_room(ch->pet,victim->in_room);
        act("$n has arrived through the gate.",ch->pet,NULL,NULL,TO_ROOM);
        look_room(ch->pet,ch->pet->in_room,TRUE);
    } else if (ch->pet) {
        send_to_char("You have the feeling you've forgotten something...\n\r",ch);
    }
}
void event_gate_createone(EVENT_DATA *event) {
    CHAR_DATA *victim;
    CHAR_DATA *ch=event->ch;
    struct gate_data *data=(struct gate_data *)event->data;
    char target_name[MSL];

    strcpy(target_name,data->target_name);
    free_string(data->target_name);

    if ( ( victim = get_char_world( ch, target_name ) ) == NULL
         ||   victim == ch
         ||   victim->in_room == NULL
         ||   !can_see_room(ch,victim->in_room)
         ||	 (is_room_owned(ch,victim->in_room) &&
              !(is_room_owner(ch,victim->in_room) || is_allowed_in_room(ch,victim->in_room)))
         ||   STR_IS_SET(victim->in_room->strbit_room_flags, ROOM_SAFE)
         ||   STR_IS_SET(victim->in_room->strbit_room_flags, ROOM_PRIVATE)
         ||   STR_IS_SET(victim->in_room->strbit_room_flags, ROOM_SOLITARY)
         ||   STR_IS_SET(victim->in_room->strbit_room_flags, ROOM_NO_RECALL)
         ||   STR_IS_SET(ch->in_room->strbit_room_flags, ROOM_NO_RECALL)
         ||   victim->level >= data->level + 10 /* Was +3 */
         /*  ||   (is_clan(victim) && !is_same_clan(ch,victim)) */
         ||   (!IS_NPC(victim) && victim->level >= LEVEL_HERO)  /* NOT trust */
         ||   (IS_NPC(victim) && STR_IS_SET(victim->strbit_imm_flags,IMM_SUMMON))
         ||   (IS_NPC(victim) && saves_spell( data->level, victim,DAM_OTHER) ) )
    {
        send_to_char( "Your gate failed.\n\r", ch );
        return;
    }

    if (ch->in_room==victim->in_room) {
        act("$N is already here.",ch,NULL,victim,TO_CHAR);
        return;
    }

    //
    // default pets don't gate with the player, unless they're awake.
    // if they're resting they try to stand up
    //
    if (ch->pet != NULL && ch->in_room == ch->pet->in_room)
        if (ch->pet->position==POS_RESTING)
            do_function(ch->pet,&do_wake,"");
    create_event(1,ch,victim,NULL,event_gate_gothrough,NULL);

    act("A gate appears in the room!",victim,NULL,NULL,TO_ALL);
    act("You have created a gate to $N.",ch,NULL,victim,TO_CHAR);
    act("$n has created a gate.",ch,NULL,NULL,TO_ROOM);
}
void spell_gate( int sn, int level, CHAR_DATA *ch, void *vo,int target ) {
    struct gate_data *data;

    data=(struct gate_data *)calloc(1,sizeof(struct gate_data));
    data->level=level;
    data->target_name=str_dup(target_name);

    create_event(1,ch,NULL,NULL,event_gate_createone,data);
    send_to_char("You hold your hands in front of you.\n\r",ch);
    act("$n holds $s hands in front of $m.",ch,NULL,NULL,TO_ROOM);
}



void spell_giant_strength(int sn,int level,CHAR_DATA *ch,void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( is_affected( victim, sn ) )
    {
        if (victim == ch)
            send_to_char("You are already as strong as you can get!\n\r",ch);
        else
            act("$N can't get any stronger.",ch,NULL,victim,TO_CHAR);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level	 = level;
    ef.duration  = level;
    ef.location  = APPLY_STR;
    ef.modifier  = 1 + (level >= 18) + (level >= 25) + (level >= 32);
    ef.bitvector = EFF_GIANTSTRENGTH;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char( "Your muscles surge with heightened power!\n\r", victim );
    act("$n's muscles surge with heightened power.",victim,NULL,NULL,TO_ROOM);
    return;
}



void spell_harm( int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    int dam;

    dam = UMAX(  20, victim->hit - dice(1,4) );
    if ( saves_spell( level, victim,DAM_HARM) )
        dam = UMIN( 50, dam / 2 );
    dam = UMIN( 100, dam );
    damage( ch, victim, dam, sn, DAM_HARM ,TRUE, NULL);
    return;
}

/* RT haste spell */

void spell_haste( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( is_affected( victim, sn ) || IS_AFFECTED(victim,EFF_HASTE)
         ||   STR_IS_SET(victim->strbit_off_flags,OFF_FAST))
    {
        if (victim == ch)
            send_to_char("You can't move any faster!\n\r",ch);
        else
            act("$N is already moving as fast as $E can.",
                ch,NULL,victim,TO_CHAR);
        return;
    }

    if (IS_AFFECTED(victim,EFF_SLOW))
    {
        if (!check_dispel(level,victim,gsn_slow))
        {
            if (victim != ch)
                send_to_char("Spell failed.\n\r",ch);
            send_to_char("You feel momentarily faster.\n\r",victim);
            return;
        }
        act("$n is moving less slowly.",victim,NULL,NULL,TO_ROOM);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level     = level;
    if (victim == ch)
        ef.duration  = level/2;
    else
        ef.duration  = level/4;
    ef.location  = APPLY_DEX;
    ef.modifier  = 1 + (level >= 18) + (level >= 25) + (level >= 32);
    ef.bitvector = EFF_HASTE;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char( "You feel yourself moving more quickly.\n\r", victim );
    act("$n is moving more quickly.",victim,NULL,NULL,TO_ROOM);
    if ( ch != victim )
        send_to_char( "Ok.\n\r", ch );
    return;
}


void spell_heal( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    int mult=1;

    if (ch->class == CLASS_CLERIC) {
        mult*=2;
    }
    if (skillcheck(ch,gsn_enhanced_healing)) {
        mult*=2;
        check_improve(ch,gsn_enhanced_healing,TRUE,1);
    } else
        check_improve(ch,gsn_enhanced_healing,FALSE,1);

    heal(ch,victim,level,mult);
}

static void heal(CHAR_DATA *ch, CHAR_DATA *victim, int level, int multiplier) {
    if (victim->position==POS_DEAD) return;
    victim->hit = UMIN( victim->hit + 100*multiplier, victim->max_hit );
    update_pos( victim );
    send_to_char( "A warm feeling fills your body.\n\r", victim );
    if ( ch != victim ) {
        act( "You heal $N.", ch, NULL, victim, TO_CHAR );
        change_alignment(ch,victim,10);
    }
    return;
}

void spell_heat_metal( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    OBJ_DATA *obj_lose, *obj_next;
    int dam = 0;
    bool fail = TRUE;

    if (!saves_spell(level + 2,victim,DAM_FIRE)
            &&  !STR_IS_SET(victim->strbit_imm_flags,IMM_FIRE))
    {
        for ( obj_lose = victim->carrying;
              obj_lose != NULL;
              obj_lose = obj_next)
        {
            obj_next = obj_lose->next_content;
            if ( number_range(1,2 * level) > obj_lose->level
                 &&   !saves_spell(level,victim,DAM_FIRE)
                 &&   !IS_OBJ_STAT(obj_lose,ITEM_NONMETAL)
                 &&   !IS_OBJ_STAT(obj_lose,ITEM_BURN_PROOF))
            {
                switch ( obj_lose->item_type )
                {
                case ITEM_ARMOR:
                    if (obj_lose->wear_loc != WEAR_NONE) /* remove the item */
                    {
                        if (can_drop_obj(victim,obj_lose)
                                &&  (obj_lose->weight / 10) <
                                number_range(1,2 * get_curr_stat(victim,STAT_DEX))
                                &&  remove_obj( victim, obj_lose->wear_loc, TRUE, TRUE ))
                        {
                            act("$n yelps and throws $p to the ground!",
                                victim,obj_lose,NULL,TO_ROOM);
                            act("You remove and drop $p before it burns you.",
                                victim,obj_lose,NULL,TO_CHAR);
                            dam += (number_range(1,obj_lose->level) / 3);
                            obj_from_char(obj_lose);
                            obj_to_room(obj_lose, victim->in_room);
                            fail = FALSE;
                        }
                        else /* stuck on the body! ouch! */
                        {
                            act("Your skin is seared by $p!",
                                victim,obj_lose,NULL,TO_CHAR);
                            dam += (number_range(1,obj_lose->level));
                            fail = FALSE;
                        }

                    }
                    else /* drop it if we can */
                    {
                        if (can_drop_obj(victim,obj_lose))
                        {
                            act("$n yelps and throws $p to the ground!",
                                victim,obj_lose,NULL,TO_ROOM);
                            act("You and drop $p before it burns you.",
                                victim,obj_lose,NULL,TO_CHAR);
                            dam += (number_range(1,obj_lose->level) / 6);
                            obj_from_char(obj_lose);
                            obj_to_room(obj_lose, victim->in_room);
                            fail = FALSE;
                        }
                        else /* cannot drop */
                        {
                            act("Your skin is seared by $p!",
                                victim,obj_lose,NULL,TO_CHAR);
                            dam += (number_range(1,obj_lose->level) / 2);
                            fail = FALSE;
                        }
                    }
                    break;
                case ITEM_WEAPON:
                    if (obj_lose->wear_loc != WEAR_NONE) /* try to drop it */
                    {
                        if (IS_WEAPON_STAT(obj_lose,WEAPON_FLAMING))
                            continue;

                        if (can_drop_obj(victim,obj_lose)
                                &&  remove_obj(victim,obj_lose->wear_loc,TRUE,TRUE))
                        {
                            act("$n is burned by $p, and throws it to the ground.",
                                victim,obj_lose,NULL,TO_ROOM);
                            send_to_char(
                                        "You throw your red-hot weapon to the ground!\n\r",
                                        victim);
                            dam += 1;
                            obj_from_char(obj_lose);
                            obj_to_room(obj_lose,victim->in_room);
                            fail = FALSE;
                        }
                        else /* YOWCH! */
                        {
                            send_to_char("Your weapon sears your flesh!\n\r",
                                         victim);
                            dam += number_range(1,obj_lose->level);
                            fail = FALSE;
                        }
                    }
                    else /* drop it if we can */
                    {
                        if (can_drop_obj(victim,obj_lose))
                        {
                            act("$n throws a burning hot $p to the ground!",
                                victim,obj_lose,NULL,TO_ROOM);
                            act("You and drop $p before it burns you.",
                                victim,obj_lose,NULL,TO_CHAR);
                            dam += (number_range(1,obj_lose->level) / 6);
                            obj_from_char(obj_lose);
                            obj_to_room(obj_lose, victim->in_room);
                            fail = FALSE;
                        }
                        else /* cannot drop */
                        {
                            act("Your skin is seared by $p!",
                                victim,obj_lose,NULL,TO_CHAR);
                            dam += (number_range(1,obj_lose->level) / 2);
                            fail = FALSE;
                        }
                    }
                    break;
                }
            }
        }
    }
    if (fail)
    {
        send_to_char("Your spell had no effect.\n\r", ch);
        send_to_char("You feel momentarily warmer.\n\r",victim);
    }
    else /* damage! */
    {
        if (saves_spell(level,victim,DAM_FIRE))
            dam = 2 * dam / 3;
        damage(ch,victim,dam,sn,DAM_FIRE,TRUE, NULL);
    }
}

/* RT really nasty high-level attack spell */
void spell_holy_word(int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    CHAR_DATA *vch;
    CHAR_DATA *vch_next;
    int dam;

    act("$n utters a word of divine power!",ch,NULL,NULL,TO_ROOM);
    send_to_char("You utter a word of divine power.\n\r",ch);

    for ( vch = ch->in_room->people; vch != NULL; vch = vch_next )
    {
        vch_next = vch->next_in_room;

        if ((IS_GOOD(ch) && IS_GOOD(vch)) ||
                (IS_EVIL(ch) && IS_EVIL(vch)) ||
                (IS_NEUTRAL(ch) && IS_NEUTRAL(vch)) )
        {
            send_to_char("You feel more powerful.\n\r",vch);
            spell_frenzy(gsn_frenzy,level,ch,(void *) vch,TARGET_CHAR);
            spell_bless(gsn_bless,level,ch,(void *) vch,TARGET_CHAR);
        }

        else if ((IS_GOOD(ch) && IS_EVIL(vch)) ||
                 (IS_EVIL(ch) && IS_GOOD(vch)) )
        {
            if (!is_safe_spell(ch,vch,TRUE))
            {
                spell_curse(gsn_curse,level,ch,(void *) vch,TARGET_CHAR);
                send_to_char("You are struck down!\n\r",vch);
                dam = dice(level,6);
                damage(ch,vch,dam,sn,DAM_ENERGY,TRUE, NULL);
            }
        }

        else if (IS_NEUTRAL(ch))
        {
            if (!is_safe_spell(ch,vch,TRUE))
            {
                spell_curse(gsn_curse,level/2,ch,(void *) vch,TARGET_CHAR);
                send_to_char("You are struck down!\n\r",vch);
                dam = dice(level,4);
                damage(ch,vch,dam,sn,DAM_ENERGY,TRUE, NULL);
            }
        }
    }

    send_to_char("You feel drained.\n\r",ch);
    ch->move = 0;
    ch->hit /= 2;
}

/* the latter half of this function has been split off and is now
   shared with lore() ... only people with excellent lore will
   get the true nitty gritty about an object */

void spell_identify( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    OBJ_DATA *obj = (OBJ_DATA *) vo;
    char buf[MAX_STRING_LENGTH];
    const char * weight_mes;

    if (IS_OBJ_STAT(obj,ITEM_UNIDENTIFABLE)) {
        send_to_char("You really have no clue...\n\r",ch);
        return;
    }


    /* the following obj->carried_by condition should always be true
       because you can only identify an object you're carrying. The
       advantage of lore is that you can examine stuff on the ground
       too :) */
    weight_mes =
            obj_weight_message( ch,
                                get_obj_weight( obj ),
                                ( obj->carried_by == ch ? TRUE
                                                        : FALSE )
                                );
    sprintf( buf,
             "Object '%s' is type %s, extra flags %s.\n\r%sValue is %d, level is %d.\n\r",

             obj->name,
             item_type(obj),
             obj_extra_string(obj),
             weight_mes,
             obj->cost,
             obj->level
             );
    send_to_char( buf, ch );
    identify_more( ch, obj );
}

void identify_more( CHAR_DATA * ch, OBJ_DATA * obj ) {
    EFFECT_DATA *pef;
    char buf[MAX_STRING_LENGTH];

    switch ( obj->item_type )
    {
    case ITEM_SCROLL:
    case ITEM_POTION:
    case ITEM_PILL:
        sprintf( buf, "Level %d spells of:", obj->value[0] );
        send_to_char( buf, ch );

        if ( obj->value[1] >= 0 && obj->value[1] < MAX_SKILL )
        {
            send_to_char( " '", ch );
            send_to_char( skill_name(obj->value[1],ch), ch );
            send_to_char( "'", ch );
        }

        if ( obj->value[2] >= 0 && obj->value[2] < MAX_SKILL )
        {
            send_to_char( " '", ch );
            send_to_char( skill_name(obj->value[2],ch), ch );
            send_to_char( "'", ch );
        }

        if ( obj->value[3] >= 0 && obj->value[3] < MAX_SKILL )
        {
            send_to_char( " '", ch );
            send_to_char( skill_name(obj->value[3],ch), ch );
            send_to_char( "'", ch );
        }

        if (obj->value[4] >= 0 && obj->value[4] < MAX_SKILL)
        {
            send_to_char(" '",ch);
            send_to_char(skill_name(obj->value[4],ch),ch);
            send_to_char("'",ch);
        }

        send_to_char( ".\n\r", ch );
        break;

    case ITEM_WAND:
    case ITEM_STAFF:
        sprintf( buf, "Has %d charges of level %d",
                 obj->value[2], obj->value[0] );
        send_to_char( buf, ch );

        if ( obj->value[3] >= 0 && obj->value[3] < MAX_SKILL )
        {
            send_to_char( " '", ch );
            send_to_char( skill_name(obj->value[3],ch), ch );
            send_to_char( "'", ch );
        }

        send_to_char( ".\n\r", ch );
        break;

    case ITEM_DRINK_CON:
        sprintf(buf,"It holds %s-colored %s.\n\r",
                liq_table[obj->value[2]].liq_color,
                liq_table[obj->value[2]].liq_name);
        send_to_char(buf,ch);
        break;

    case ITEM_CONTAINER:
        sprintf(buf,"Capacity: %d#  Maximum weight: %d#  flags: %s\n\r",
                obj->value[0], obj->value[3], obj_container_string(obj));
        send_to_char(buf,ch);
        if (obj->value[4] != 100)
        {
            sprintf(buf,"Weight multiplier: %d%%\n\r",
                    obj->value[4]);
            send_to_char(buf,ch);
        }
        break;

    case ITEM_WEAPON:
        send_to_char("Weapon type is ",ch);
        switch (obj->value[0])
        {
        case(WEAPON_EXOTIC) : send_to_char("exotic.\n\r",ch);	break;
        case(WEAPON_SWORD)  : send_to_char("sword.\n\r",ch);	break;
        case(WEAPON_DAGGER) : send_to_char("dagger.\n\r",ch);	break;
        case(WEAPON_SPEAR)	: send_to_char("spear/staff.\n\r",ch);	break;
        case(WEAPON_MACE) 	: send_to_char("mace/club.\n\r",ch);	break;
        case(WEAPON_AXE)	: send_to_char("axe.\n\r",ch);		break;
        case(WEAPON_FLAIL)	: send_to_char("flail.\n\r",ch);	break;
        case(WEAPON_WHIP)	: send_to_char("whip.\n\r",ch);		break;
        case(WEAPON_POLEARM): send_to_char("polearm.\n\r",ch);	break;
        default		: send_to_char("unknown.\n\r",ch);	break;
        }

        sprintf(buf,"Damage is %dd%d (average %d).\n\r",
                obj->value[1],obj->value[2],
                (1 + obj->value[2]) * obj->value[1] / 2);
        send_to_char( buf, ch );

        if (obj->value[4])  /* weapon flags */
        {
            sprintf(buf,"Weapons flags: %s\n\r",obj_weaponflag_string(obj));
            send_to_char(buf,ch);
        }
        break;

    case ITEM_ARMOR:
        sprintf( buf,
                 "Armor class is %d pierce, %d bash, %d slash, and %d vs. magic.\n\r",
                 obj->value[0], obj->value[1], obj->value[2], obj->value[3] );
        send_to_char( buf, ch );
        break;
    }

    if (!obj->enchanted)
        for ( pef = obj->pIndexData->affected; pef != NULL; pef = pef->next )
        {
            if ( TRUE ) /* pef->location != APPLY_NONE && pef->modifier != 0 ) */
            {
                sprintf( buf, "Affects %s by %d.\n\r",
                         effect_loc_name(pef), pef->modifier );
                send_to_char(buf,ch);
                if (pef->bitvector)
                {
                    switch(pef->where)
                    {
                    case TO_EFFECTS:
                        sprintf(buf,"Adds %s effect.\n\r",
                                effect_bit_name(pef));
                        break;
                    case TO_EFFECTS2:
                        sprintf(buf,"Adds %s effect.\n\r",
                                effect2_bit_name(pef));
                        break;
                    case TO_OBJECT:
                        sprintf(buf,"Adds %s object flag.\n\r",
                                option_find_value(pef->bitvector,extra_options));
                        break;
                    case TO_OBJECT2:
                        sprintf(buf,"Adds %s object flag.\n\r",
                                option_find_value(pef->bitvector,extra2_options));
                        break;
                    case TO_IMMUNE:
                        sprintf(buf,"Adds immunity to %s.\n\r",
                                option_find_value(pef->bitvector,imm_options));
                        break;
                    case TO_RESIST:
                        sprintf(buf,"Adds resistance to %s.\n\r",
                                option_find_value(pef->bitvector,imm_options));
                        break;
                    case TO_VULN:
                        sprintf(buf,"Adds vulnerability to %s.\n\r",
                                option_find_value(pef->bitvector,imm_options));
                        break;
                    case TO_WEAPON:
                        sprintf_to_char(ch,"{yAdds {x%s {yweapon flags.",
                                        option_find_value(pef->bitvector,weaponflag_options));
                        break;
                    case TO_SKILLS:
                        sprintf_to_char(ch,"{yAdds {x%s {yproficiency.",
                                        skill_name(pef->bitvector,ch));
                        break;
                    default:
                        sprintf(buf,"Unknown bit %d: %d\n\r",
                                pef->where,pef->bitvector);
                        break;
                    }
                    send_to_char( buf, ch );
                }
            }
        }

    for ( pef = obj->affected; pef != NULL; pef = pef->next )
    {
        if ( TRUE ) /* pef->location != APPLY_NONE && pef->modifier != 0) */
        {
            sprintf( buf, "Affects %s by %d",
                     effect_loc_name( pef), pef->modifier );
            send_to_char( buf, ch );
            if ( pef->duration > -1)
                sprintf(buf,", %d hours.\n\r",pef->duration);
            else
                sprintf(buf,".\n\r");
            send_to_char(buf,ch);
            if (pef->bitvector)
            {
                switch(pef->where)
                {
                case TO_EFFECTS:
                    sprintf(buf,"Adds %s effect.\n\r",
                            effect_bit_name(pef));
                    break;
                case TO_EFFECTS2:
                    sprintf(buf,"Adds %s effect.\n\r",
                            effect2_bit_name(pef));
                    break;
                case TO_OBJECT:
                    sprintf(buf,"Adds %s object flag.\n\r",
                            option_find_value(pef->bitvector,extra_options));
                    break;
                case TO_OBJECT2:
                    sprintf(buf,"Adds %s object flag.\n\r",
                            option_find_value(pef->bitvector,extra2_options));
                    break;
                case TO_IMMUNE:
                    sprintf(buf,"Adds immunity to %s.\n\r",
                            option_find_value(pef->bitvector,imm_options));
                    break;
                case TO_RESIST:
                    sprintf(buf,"Adds resistance to %s.\n\r",
                            option_find_value(pef->bitvector,imm_options));
                    break;
                case TO_VULN:
                    sprintf(buf,"Adds vulnerability to %s.\n\r",
                            option_find_value(pef->bitvector,imm_options));
                    break;
                case TO_WEAPON:
                    sprintf_to_char(ch,"{yAdds {x%s {yweapon flags.",
                                    option_find_value(pef->bitvector,weaponflag_options));
                    break;
                case TO_SKILLS:
                    sprintf_to_char(ch,"{yAdds {x%s {yproficiency.",
                                    skill_name(pef->bitvector,ch));
                    break;
                default:
                    sprintf(buf,"Unknown bit %d: %d\n\r",
                            pef->where,pef->bitvector);
                    break;
                }
                send_to_char(buf,ch);
            }
        }
    }

    return;
}



void spell_infravision( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( IS_AFFECTED(victim, EFF_INFRARED) )
    {
        if (victim == ch)
            send_to_char("You can already see in the dark.\n\r",ch);
        else
            act("$N already has infravision.",ch,NULL,victim,TO_CHAR);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where	 = TO_EFFECTS;
    ef.type      = sn;
    ef.level	 = level;
    ef.duration  = 2 * level;
    ef.location  = APPLY_NONE;
    ef.modifier  = 0;
    ef.bitvector = EFF_INFRARED;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    act( "$n's eyes glow red.", ch, NULL, NULL, TO_ROOM );
    send_to_char( "Your eyes glow red.\n\r", victim );
    return;
}



void spell_invis( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim;
    OBJ_DATA *obj;
    EFFECT_DATA ef;

    /* object invisibility */
    if (target == TARGET_OBJ)
    {
        obj = (OBJ_DATA *) vo;

        if (IS_OBJ_STAT(obj,ITEM_INVIS))
        {
            act("$p is already invisible.",ch,obj,NULL,TO_CHAR);
            return;
        }

        ZEROVAR(&ef,EFFECT_DATA);
        ef.where	= TO_OBJECT;
        ef.type		= sn;
        ef.level	= level;
        ef.duration	= level + 12;
        ef.location	= APPLY_NONE;
        ef.modifier	= 0;
        ef.bitvector	= ITEM_INVIS;
        ef.casted_by	= ch?ch->id:0;
        effect_to_obj(obj,&ef);

        act("$p fades out of sight.",ch,obj,NULL,TO_ALL);
        return;
    }

    /* character invisibility */
    victim = (CHAR_DATA *) vo;

    if ( IS_AFFECTED(victim, EFF_INVISIBLE) || IS_AFFECTED2(victim,EFF_IMPINVISIBLE))
        return;

    act( "$n fades out of existence.", victim, NULL, NULL, TO_ROOM );

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = level + 12;
    ef.location  = APPLY_NONE;
    ef.modifier  = 0;
    ef.bitvector = EFF_INVISIBLE;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    if (IS_AFFECTED(victim,EFF_HALLUCINATION)) {
        if (IS_AFFECTED(victim,EFF_DETECT_INVIS))
            send_to_char( "Far out, man!  You can't see yourself.\n\r",victim);
        else
            send_to_char(
                        "Far out, man!  You can see right through yourself.\n\r",
                        victim);
    } else
        send_to_char( "You fade out of existence.\n\r", victim );
    return;
}



void spell_know_alignment(int sn,int level,CHAR_DATA *ch,void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    char *msg;
    int ap;

    ap = victim->alignment;

    if ( ap >  700 ) msg = "$N has a pure and good aura.";
    else if ( ap >  350 ) msg = "$N is of excellent moral character.";
    else if ( ap >  100 ) msg = "$N is often kind and thoughtful.";
    else if ( ap > -100 ) msg = "$N doesn't have a firm moral commitment.";
    else if ( ap > -350 ) msg = "$N lies to $S friends.";
    else if ( ap > -700 ) msg = "$N is a black-hearted murderer.";
    else msg = "$N is the embodiment of pure evil!.";

    act( msg, ch, NULL, victim, TO_CHAR );
    return;
}



void spell_lightning_bolt(int sn,int level,CHAR_DATA *ch,void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    static const int dam_each[] =
    {
        0,
        0,  0,  0,  0,  0,	 0,  0,  0, 25, 28,
        31, 34, 37, 40, 40,	41, 42, 42, 43, 44,
        44, 45, 46, 46, 47,	48, 48, 49, 50, 50,
        51, 52, 52, 53, 54,	54, 55, 56, 56, 57,
        58, 58, 59, 60, 60,	61, 62, 62, 63, 64
    };
    int dam;

    level	= UMIN(level, sizeof(dam_each)/sizeof(dam_each[0]) - 1);
    level	= UMAX(0, level);
    dam		= number_range( dam_each[level] / 2, dam_each[level] * 2 );
    if ( saves_spell( level, victim,DAM_LIGHTNING) )
        dam /= 2;
    damage( ch, victim, dam, sn, DAM_LIGHTNING ,TRUE, NULL);
    return;
}



void spell_locate_object( int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    char buf[MAX_INPUT_LENGTH];
    BUFFER *buffer;
    OBJ_DATA *obj;
    OBJ_DATA *in_obj;
    bool found;
    int number = 0, max_found;
    bool roomsfound[MAX_VNUMS];
    bool roomsrefound[MAX_VNUMS];
    bool carrierfound[MAX_VNUMS];
    bool carrierrefound[MAX_VNUMS];

    found = FALSE;
    number = 0;
    max_found = IS_IMMORTAL(ch) ? 200 : 2 * level;

    memset(roomsfound,0,MAX_VNUMS);
    memset(roomsrefound,0,MAX_VNUMS);
    memset(carrierfound,0,MAX_VNUMS);
    memset(carrierrefound,0,MAX_VNUMS);

    buffer = new_buf();

    for ( obj = object_list; obj != NULL; obj = obj->next )
    {
        if ( !can_see_obj( ch, obj ) || !is_name( target_name, obj->name )
             ||   IS_OBJ_STAT(obj,ITEM_NOLOCATE) || number_percent() > 2 * level
             ||   ch->level < obj->level)
            continue;

        found = TRUE;
        number++;

        for ( in_obj = obj; in_obj->in_obj != NULL; in_obj = in_obj->in_obj )
            ;

        buf[0]=0;
        if ( in_obj->carried_by != NULL ) {
            if (can_see(ch,in_obj->carried_by)) {
                if (IS_PC(in_obj->carried_by)
                        || !carrierfound[in_obj->carried_by->pIndexData->vnum]) {
                    sprintf( buf, "There is one carried by %s.\n\r",
                             PERS(in_obj->carried_by, ch) );
                    if (IS_NPC(in_obj->carried_by))
                        carrierfound[in_obj->carried_by->pIndexData->vnum]=TRUE;
                } else if (carrierrefound[in_obj->carried_by->pIndexData->vnum]) {
                    sprintf( buf, "There are even more carried by %s.\n\r",
                             PERS(in_obj->carried_by, ch) );
                    carrierrefound[in_obj->carried_by->pIndexData->vnum]=TRUE;
                }
            }
        } else if (in_obj->in_room) {
            if (!roomsfound[in_obj->in_room->vnum]) {
                sprintf( buf, "There is one in %s.\n\r",
                         in_obj->in_room == NULL ? "somewhere" : in_obj->in_room->name );
                roomsfound[in_obj->in_room->vnum]=TRUE;
            } else if (!roomsrefound[in_obj->in_room->vnum]) {
                sprintf( buf, "There are even more in %s.\n\r",
                         in_obj->in_room == NULL ? "somewhere" : in_obj->in_room->name );
                roomsrefound[in_obj->in_room->vnum]=TRUE;
            }
        } else {
            bugf("OBJ is nowhere! (vnum:%d id:%d short:%s)",in_obj->pIndexData->vnum,
                 in_obj->id,
                 in_obj->short_descr);
            continue;
        }

        if (buf[0]!=0) {
            buf[0] = UPPER(buf[0]);
            add_buf(buffer,buf);

            if (number >= max_found)
                break;
        }
    }

    if ( !found )
        send_to_char( "Nothing like that in heaven or earth.\n\r", ch );
    else
        page_to_char(buf_string(buffer),ch);

    free_buf(buffer);

    return;
}



void spell_magic_missile( int sn, int level, CHAR_DATA *ch,void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    static const int dam_each[] =
    {
        0,
        3,  3,  4,  4,  5,	 6,  6,  6,  6,  6,
        7,  7,  7,  7,  7,	 8,  8,  8,  8,  8,
        9,  9,  9,  9,  9,	10, 10, 10, 10, 10,
        11, 11, 11, 11, 11,	12, 12, 12, 12, 12,
        13, 13, 13, 13, 13,	14, 14, 14, 14, 14
    };
    int dam;

    level	= UMIN(level, sizeof(dam_each)/sizeof(dam_each[0]) - 1);
    level	= UMAX(0, level);
    dam		= number_range( dam_each[level] / 2, dam_each[level] * 2 );
    if ( saves_spell( level, victim,DAM_ENERGY) )
        dam /= 2;
    damage( ch, victim, dam, sn, DAM_ENERGY ,TRUE, NULL);
    return;
}

void spell_mass_healing(int sn, int level, CHAR_DATA *ch, void *vo, int target)
{
    CHAR_DATA *gch;
    int gain=1;

    if (ch->class == CLASS_CLERIC) {
        gain*=2;
    }
    if (skillcheck(ch,gsn_enhanced_healing)) {
        gain*=2;
        check_improve(ch,gsn_enhanced_healing,TRUE,1);
    } else
        check_improve(ch,gsn_enhanced_healing,FALSE,1);

    for ( gch = ch->in_room->people; gch != NULL; gch = gch->next_in_room )
    {
        if ((IS_NPC(ch) && IS_NPC(gch)) ||
                (!IS_NPC(ch) && !IS_NPC(gch)))
        {
            heal(ch,gch,level,gain);
            refresh(ch,gch,level,gain);
        }
    }
}


void spell_mass_invis( int sn, int level, CHAR_DATA *ch, void *vo, int target )
{
    EFFECT_DATA ef;
    CHAR_DATA *gch;

    for ( gch = ch->in_room->people; gch != NULL; gch = gch->next_in_room )
    {
        if ( !is_same_group( gch, ch ) || IS_AFFECTED(gch, EFF_INVISIBLE)
             || IS_AFFECTED2(gch,EFF_IMPINVISIBLE) )
            continue;
        act( "$n slowly fades out of existence.", gch, NULL, NULL, TO_ROOM );
        send_to_char( "You slowly fade out of existence.\n\r", gch );

        ZEROVAR(&ef,EFFECT_DATA);
        ef.where     = TO_EFFECTS;
        ef.type      = sn;
        ef.level     = level/2;
        ef.duration  = 24;
        ef.location  = APPLY_NONE;
        ef.modifier  = 0;
        ef.bitvector = EFF_INVISIBLE;
        ef.casted_by = ch?ch->id:0;
        effect_to_char( gch, &ef );
    }
    send_to_char( "Ok.\n\r", ch );

    return;
}



void spell_pass_door( int sn, int level, CHAR_DATA *ch, void *vo, int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( IS_AFFECTED(victim, EFF_PASS_DOOR) )
    {
        if (victim == ch)
            send_to_char("You are already out of phase.\n\r",ch);
        else
            act("$N is already shifted out of phase.",ch,NULL,victim,TO_CHAR);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = number_fuzzy( level / 4 );
    ef.location  = APPLY_NONE;
    ef.modifier  = 0;
    ef.bitvector = EFF_PASS_DOOR;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    act( "$n turns translucent.", victim, NULL, NULL, TO_ROOM );
    send_to_char( "You turn translucent.\n\r", victim );
    return;
}

/* RT plague spell, very nasty */

void spell_plague( int sn, int level, CHAR_DATA *ch, void *vo, int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if (saves_spell(level,victim,DAM_DISEASE) ||
            (IS_NPC(victim) && STR_IS_SET(victim->strbit_act,ACT_UNDEAD)))
    {
        if (ch == victim)
            send_to_char("You feel momentarily ill, but it passes.\n\r",ch);
        else
            act("$N seems to be unaffected.",ch,NULL,victim,TO_CHAR);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type 	  = sn;
    ef.level	  = level * 3/4;
    ef.duration  = level;
    ef.location  = APPLY_STR;
    ef.modifier  = -5;
    ef.bitvector = EFF_PLAGUE;
    ef.casted_by = ch?ch->id:0;
    effect_join(victim,&ef);

    send_to_char
            ("You scream in agony as plague sores erupt from your skin.\n\r",victim);
    act("$n screams in agony as plague sores erupt from $s skin.",
        victim,NULL,NULL,TO_ROOM);
}

void spell_poison( int sn, int level, CHAR_DATA *ch, void *vo, int target )
{
    CHAR_DATA *victim;
    OBJ_DATA *obj;
    EFFECT_DATA ef;


    if (target == TARGET_OBJ)
    {
        obj = (OBJ_DATA *) vo;

        if (obj->item_type == ITEM_FOOD || obj->item_type == ITEM_DRINK_CON)
        {
            if (IS_OBJ_STAT(obj,ITEM_BLESS) || IS_OBJ_STAT(obj,ITEM_BURN_PROOF))
            {
                act("Your spell fails to corrupt $p.",ch,obj,NULL,TO_CHAR);
                return;
            }
            obj->value[3] = 1;
            act("$p is infused with poisonous vapors.",ch,obj,NULL,TO_ALL);
            return;
        }

        if (obj->item_type == ITEM_WEAPON)
        {
            if (IS_WEAPON_STAT(obj,WEAPON_FLAMING)
                    ||  IS_WEAPON_STAT(obj,WEAPON_FROST)
                    ||  IS_WEAPON_STAT(obj,WEAPON_VAMPIRIC)
                    ||  IS_WEAPON_STAT(obj,WEAPON_SHARP)
                    ||  IS_WEAPON_STAT(obj,WEAPON_VORPAL)
                    ||  IS_WEAPON_STAT(obj,WEAPON_SHOCKING)
                    ||  IS_OBJ_STAT(obj,ITEM_BLESS) || IS_OBJ_STAT(obj,ITEM_BURN_PROOF))
            {
                act("You can't seem to envenom $p.",ch,obj,NULL,TO_CHAR);
                return;
            }

            if (IS_WEAPON_STAT(obj,WEAPON_POISON))
            {
                act("$p is already envenomed.",ch,obj,NULL,TO_CHAR);
                return;
            }

            ZEROVAR(&ef,EFFECT_DATA);
            ef.where	 = TO_WEAPON;
            ef.type	 = sn;
            ef.level	 = level / 2;
            ef.duration	 = level/8;
            ef.location	 = 0;
            ef.modifier	 = 0;
            ef.bitvector = WEAPON_POISON;
            ef.casted_by = ch?ch->id:0;
            effect_to_obj(obj,&ef);

            act("$p is coated with deadly venom.",ch,obj,NULL,TO_ALL);
            return;
        }

        act("You can't poison $p.",ch,obj,NULL,TO_CHAR);
        return;
    }

    victim = (CHAR_DATA *) vo;

    if ( saves_spell( level, victim,DAM_POISON) )
    {
        act("$n turns slightly green, but it passes.",victim,NULL,NULL,TO_ROOM);
        send_to_char("You feel momentarily ill, but it passes.\n\r",victim);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = level;
    ef.location  = APPLY_STR;
    ef.modifier  = -2;
    ef.bitvector = EFF_POISON;
    ef.casted_by = ch?ch->id:0;
    effect_join( victim, &ef );

    send_to_char( "You feel very sick.\n\r", victim );
    act("$n looks very ill.",victim,NULL,NULL,TO_ROOM);
    return;
}



void spell_protection_evil(int sn,int level,CHAR_DATA *ch,void *vo, int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( IS_AFFECTED(victim, EFF_PROTECT_EVIL)
         ||   IS_AFFECTED(victim, EFF_PROTECT_GOOD))
    {
        if (victim == ch)
            send_to_char("You are already protected.\n\r",ch);
        else
            act("$N is already protected.",ch,NULL,victim,TO_CHAR);
        return;
    }

    if (IS_EVIL(victim)) {
        if (victim == ch)
            send_to_char("You are evil yourself.\n\r",ch);
        else
            act("$N is evil $mself.",ch,NULL,victim,TO_CHAR);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = 24;
    ef.location  = APPLY_SAVING_SPELL;
    ef.modifier  = -1;
    ef.bitvector = EFF_PROTECT_EVIL;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char( "You feel holy and pure.\n\r", victim );
    if ( ch != victim )
        act("$N is protected from evil.",ch,NULL,victim,TO_CHAR);
    return;
}

void spell_protection_good(int sn,int level,CHAR_DATA *ch,void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( IS_AFFECTED(victim, EFF_PROTECT_GOOD)
         ||   IS_AFFECTED(victim, EFF_PROTECT_EVIL))
    {
        if (victim == ch)
            send_to_char("You are already protected.\n\r",ch);
        else
            act("$N is already protected.",ch,NULL,victim,TO_CHAR);
        return;
    }

    if (IS_GOOD(victim)) {
        if (victim == ch)
            send_to_char("You are good yourself.\n\r",ch);
        else
            act("$N is good $mself.",ch,NULL,victim,TO_CHAR);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = 24;
    ef.location  = APPLY_SAVING_SPELL;
    ef.modifier  = -1;
    ef.bitvector = EFF_PROTECT_GOOD;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char( "You feel aligned with darkness.\n\r", victim );
    if ( ch != victim )
        act("$N is protected from good.",ch,NULL,victim,TO_CHAR);
    return;
}


void spell_ray_of_truth (int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    int dam, align;

    if (IS_EVIL(ch) )
    {
        victim = ch;
        send_to_char("The energy explodes inside you!\n\r",ch);
    }

    if (victim != ch)
    {
        act("$n raises $s hand, and a blinding ray of light shoots forth!",
            ch,NULL,NULL,TO_ROOM);
        send_to_char(
                    "You raise your hand and a blinding ray of light shoots forth!\n\r",
                    ch);
    }

    if (IS_GOOD(victim))
    {
        act("$n seems unharmed by the light.",victim,NULL,victim,TO_ROOM);
        send_to_char("The light seems powerless to affect you.\n\r",victim);
        return;
    }

    dam = dice( level, 10 );
    if ( saves_spell( level, victim,DAM_HOLY) )
        dam /= 2;

    align = victim->alignment;
    align -= 350;

    if (align < -1000)
        align = -1000 + (align + 1000) / 3;

    dam = (dam * align * align) / 1000000;

    damage( ch, victim, dam, sn, DAM_HOLY ,TRUE, NULL);
    spell_blindness(gsn_blindness,
                    3 * level / 4, ch, (void *) victim,TARGET_CHAR);
}


void spell_recharge( int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    OBJ_DATA *obj = (OBJ_DATA *) vo;
    int chance, percent;

    if (obj->item_type != ITEM_WAND && obj->item_type != ITEM_STAFF)
    {
        send_to_char("That item does not carry charges.\n\r",ch);
        return;
    }

    if (obj->value[0] >= 3 * level / 2)
    {
        send_to_char("Your skills are not great enough for that.\n\r",ch);
        return;
    }

    if (obj->value[1] == 0)
    {
        send_to_char("That item has already been recharged once.\n\r",ch);
        return;
    }

    chance = 40 + 2 * level;

    chance -= obj->value[0]; /* harder to do high-level spells */
    chance -= (obj->value[1] - obj->value[2]) *
            (obj->value[1] - obj->value[2]);

    chance = UMAX(level/2,chance);

    percent = number_percent();

    if (percent < chance / 2)
    {
        act("$p glows softly.",ch,obj,NULL,TO_CHAR);
        act("$p glows softly.",ch,obj,NULL,TO_ROOM);
        obj->value[2] = UMAX(obj->value[1],obj->value[2]);
        obj->value[1] = 0;
        return;
    }

    else if (percent <= chance)
    {
        int chargeback,chargemax;

        act("$p glows softly.",ch,obj,NULL,TO_CHAR);
        act("$p glows softly.",ch,obj,NULL,TO_CHAR);

        chargemax = obj->value[1] - obj->value[2];

        if (chargemax > 0)
            chargeback = UMAX(1,chargemax * percent / 100);
        else
            chargeback = 0;

        obj->value[2] += chargeback;
        obj->value[1] = 0;
        return;
    }

    else if (percent <= UMIN(95, 3 * chance / 2))
    {
        send_to_char("Nothing seems to happen.\n\r",ch);
        if (obj->value[1] > 1)
            obj->value[1]--;
        return;
    }

    else /* whoops! */
    {
        act("$p glows brightly and explodes!",ch,obj,NULL,TO_CHAR);
        act("$p glows brightly and explodes!",ch,obj,NULL,TO_ROOM);
        extract_obj(obj);
    }
}

void spell_refresh( int sn, int level, CHAR_DATA *ch, void *vo,int target ) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    int gain=1;

    if (ch->class == CLASS_CLERIC) {
        gain*=2;
    }
    if (skillcheck(ch,gsn_enhanced_healing)) {
        gain*=2;
        check_improve(ch,gsn_enhanced_healing,TRUE,1);
    } else
        check_improve(ch,gsn_enhanced_healing,FALSE,1);

    refresh(ch,victim,level,gain);
}

static void refresh(CHAR_DATA *ch, CHAR_DATA *victim, int level, int multiplier) {
    int gain=level*multiplier;

    if (victim->position==POS_DEAD) return;

    victim->move = UMIN( victim->move + gain, victim->max_move );
    if (victim->max_move == victim->move)
        send_to_char("You feel fully refreshed!\n\r",victim);
    else
        send_to_char( "You feel less tired.\n\r", victim );
    if ( ch != victim )
        act( "You have refreshed $N.", ch, NULL, victim, TO_CHAR );
    return;
}

void spell_remove_curse( int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    CHAR_DATA *victim;
    OBJ_DATA *obj;
    bool found = FALSE;
    EFFECT_DATA *pef;

    /* do object cases first */
    if (target == TARGET_OBJ)
    {
        obj = (OBJ_DATA *) vo;

        if (IS_OBJ_STAT(obj,ITEM_NODROP) || IS_OBJ_STAT(obj,ITEM_NOREMOVE))
        {
            if (!IS_OBJ_STAT(obj,ITEM_NOUNCURSE)
                    &&  !saves_dispel(level + 2,obj->level,0))
            {
                STR_REMOVE_BIT(obj->strbit_extra_flags,ITEM_NODROP);
                STR_REMOVE_BIT(obj->strbit_extra_flags,ITEM_NOREMOVE);
                act("$p glows blue.",ch,obj,NULL,TO_ALL);
                return;
            }

            act("The curse on $p is beyond your power.",ch,obj,NULL,TO_CHAR);
            return;
        }

        if ((pef=obj_has_effect(obj,EFF_HUNGER))!=NULL) {
            if (!IS_OBJ_STAT(obj,ITEM_NOUNCURSE)
                    &&  !saves_dispel(level + 2,obj->level,0)) {
                effect_enchant(obj);
                pef=obj_has_effect(obj,EFF_HUNGER);
                act("$p glows lightblue.",ch,obj,NULL,TO_ALL);
                effect_remove_obj(obj,pef);
                return;
            }
            act("The curse on $p is beyond your power.",ch,obj,NULL,TO_CHAR);
            return;
        }

        if ((pef=obj_has_effect(obj,EFF_THIRST))!=NULL) {
            if (!IS_OBJ_STAT(obj,ITEM_NOUNCURSE)
                    &&  !saves_dispel(level + 2,obj->level,0)) {
                effect_enchant(obj);
                pef=obj_has_effect(obj,EFF_THIRST);
                act("$p glows darkblue.",ch,obj,NULL,TO_ALL);
                effect_remove_obj(obj,pef);
                return;
            }
            act("The curse on $p is beyond your power.",ch,obj,NULL,TO_CHAR);
            return;
        }
        act("There doesn't seem to be a curse on $p.",ch,obj,NULL,TO_CHAR);
        return;
    }

    /* characters */
    victim = (CHAR_DATA *) vo;

    if (check_dispel(level,victim,gsn_curse))
    {
        send_to_char("You feel better.\n\r",victim);
        act("$n looks more relaxed.",victim,NULL,NULL,TO_ROOM);
    }

    for (obj = victim->carrying; (obj != NULL && !found); obj = obj->next_content) {
        if ((IS_OBJ_STAT(obj,ITEM_NODROP) || IS_OBJ_STAT(obj,ITEM_NOREMOVE))
                &&  !IS_OBJ_STAT(obj,ITEM_NOUNCURSE))
        {   /* attempt to remove curse */
            if (!saves_dispel(level,obj->level,0))
            {
                found = TRUE;
                STR_REMOVE_BIT(obj->strbit_extra_flags,ITEM_NODROP);
                STR_REMOVE_BIT(obj->strbit_extra_flags,ITEM_NOREMOVE);
                act("Your $p glows blue.",victim,obj,NULL,TO_CHAR);
                act("$n's $p glows blue.",victim,obj,NULL,TO_ROOM);
                return;
            }
        }

        if ((pef=obj_has_effect(obj,EFF_HUNGER))!=NULL
                &&  !saves_dispel(level,obj->level,0)
                &&  !IS_OBJ_STAT(obj,ITEM_NOUNCURSE)) {
            effect_enchant(obj);
            pef=obj_has_effect(obj,EFF_HUNGER);
            act("Your $p glows lightblue.",victim,obj,NULL,TO_CHAR);
            act("$n's $p glows lightblue.",victim,obj,NULL,TO_ROOM);
            effect_remove_obj(obj,pef);
            return;
        }

        if ((pef=obj_has_effect(obj,EFF_THIRST))!=NULL
                &&  !saves_dispel(level,obj->level,0)
                &&  !IS_OBJ_STAT(obj,ITEM_NOUNCURSE)) {
            effect_enchant(obj);
            pef=obj_has_effect(obj,EFF_THIRST);
            act("Your $p glows darkblue.",victim,obj,NULL,TO_CHAR);
            act("$n's $p glows darkblue.",victim,obj,NULL,TO_ROOM);
            effect_remove_obj(obj,pef);
            return;
        }
    }
}

void spell_quench( int sn, int level, CHAR_DATA *ch, void *vo,int target ) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;

    if ( IS_AFFECTED(victim, EFF_THIRST) ) {
        if (check_dispel(level,victim,gsn_thirst)) {
            if ( ch != victim )
                act( "$N is not so thirsty anymore.", ch, NULL, victim, TO_CHAR );
        }
        if (IS_PC(victim))
            gain_condition(victim,COND_THIRST,15);
    } else {
        if (IS_PC(victim))
            gain_condition(victim,COND_THIRST,48);
        send_to_char( "The dry sensation in your mouth dissipates.\n\r", victim );
        if ( ch != victim )
            act( "$S thirst is quenched.", ch, NULL, victim, TO_CHAR );
    }
}

void spell_satiate( int sn, int level, CHAR_DATA *ch, void *vo,int target ) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;

    if ( IS_AFFECTED(victim, EFF_HUNGER) ) {
        if (check_dispel(level,victim,gsn_hunger)) {
            if ( ch != victim )
                act( "$N is not so hungry anymore.", ch, NULL, victim, TO_CHAR );
        }
        if (IS_PC(victim))
            gain_condition(victim,COND_HUNGER,15);
    } else {
        if (IS_PC(victim))
            gain_condition(victim,COND_HUNGER,48);
        send_to_char( "The empty feeling in your stomach dissapears.\n\r", victim );
        if ( ch != victim )
            act( "$N is satiated.", ch, NULL, victim, TO_CHAR );
    }
}

void spell_sanctuary( int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( IS_AFFECTED(victim, EFF_SANCTUARY) )
    {
        if (victim == ch)
            send_to_char("You are already in sanctuary.\n\r",ch);
        else
            act("$N is already in sanctuary.",ch,NULL,victim,TO_CHAR);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = level / 6;
    ef.location  = APPLY_NONE;
    ef.modifier  = 0;
    ef.bitvector = EFF_SANCTUARY;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    act( "$n is surrounded by a white aura.", victim, NULL, NULL, TO_ROOM );
    send_to_char( "You are surrounded by a white aura.\n\r", victim );
    return;
}



void spell_shield( int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( is_affected( victim, sn ) )
    {
        if (victim == ch)
            send_to_char("You are already shielded from harm.\n\r",ch);
        else
            act("$N is already protected by a shield.",ch,NULL,victim,TO_CHAR);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = 8 + level;
    ef.location  = APPLY_AC;
    ef.modifier  = -20;
    ef.bitvector = EFF_SHIELD;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    act( "$n is surrounded by a force shield.", victim, NULL, NULL, TO_ROOM );
    send_to_char( "You are surrounded by a force shield.\n\r", victim );
    return;
}



void spell_shocking_grasp(int sn,int level,CHAR_DATA *ch,void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    static const int dam_each[] =
    {
        0,
        0,  0,  0,  0,  0,	 0, 20, 25, 29, 33,
        36, 39, 39, 39, 40,	40, 41, 41, 42, 42,
        43, 43, 44, 44, 45,	45, 46, 46, 47, 47,
        48, 48, 49, 49, 50,	50, 51, 51, 52, 52,
        53, 53, 54, 54, 55,	55, 56, 56, 57, 57
    };
    int dam;

    level	= UMIN(level, sizeof(dam_each)/sizeof(dam_each[0]) - 1);
    level	= UMAX(0, level);
    dam		= number_range( dam_each[level] / 2, dam_each[level] * 2 );
    if ( saves_spell( level, victim,DAM_LIGHTNING) )
        dam /= 2;
    damage( ch, victim, dam, sn, DAM_LIGHTNING ,TRUE, NULL);
    return;
}



void spell_sleep( int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( IS_AFFECTED(victim, EFF_SLEEP)
         ||   (IS_NPC(victim) && STR_IS_SET(victim->strbit_act,ACT_UNDEAD))
         ||   (level + 2) < victim->level
         ||   saves_spell( level-4, victim,DAM_CHARM) )
        return;

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = 4 + level;
    ef.location  = APPLY_NONE;
    ef.modifier  = 0;
    ef.bitvector = EFF_SLEEP;
    ef.casted_by = ch?ch->id:0;
    effect_join( victim, &ef );

    if ( IS_AWAKE(victim) )
    {
        send_to_char( "You feel very sleepy ..... zzzzzz.\n\r", victim );
        act( "$n goes to sleep.", victim, NULL, NULL, TO_ROOM );
        victim->position = POS_SLEEPING;
    }
    return;
}

void spell_slow( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( is_affected( victim, sn ) || IS_AFFECTED(victim,EFF_SLOW))
    {
        if (victim == ch)
            send_to_char("You can't move any slower!\n\r",ch);
        else
            act("$N can't get any slower than that.",
                ch,NULL,victim,TO_CHAR);
        return;
    }

    if (saves_spell(level,victim,DAM_OTHER)
            ||  STR_IS_SET(victim->strbit_imm_flags,IMM_MAGIC))
    {
        if (victim != ch)
            send_to_char("Nothing seemed to happen.\n\r",ch);
        send_to_char("You feel momentarily lethargic.\n\r",victim);
        return;
    }

    if (IS_AFFECTED(victim,EFF_HASTE))
    {
        if (!check_dispel(level,victim,gsn_haste))
        {
            if (victim != ch)
                send_to_char("Spell failed.\n\r",ch);
            send_to_char("You feel momentarily slower.\n\r",victim);
            return;
        }

        act("$n is moving less quickly.",victim,NULL,NULL,TO_ROOM);
        return;
    }


    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = level/2;
    ef.location  = APPLY_DEX;
    ef.modifier  = -1 - (level >= 18) - (level >= 25) - (level >= 32);
    ef.bitvector = EFF_SLOW;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char( "You feel yourself slowing d o w n...\n\r", victim );
    act("$n starts to move in slow motion.",victim,NULL,NULL,TO_ROOM);
    return;
}




void spell_stone_skin( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;
    bool has_barkskin=FALSE;

    if ( is_affected( victim, sn ) ) {
        if (victim == ch)
            send_to_char("Your skin is already as hard as a rock.\n\r",ch);
        else
            act("$N is already as hard as can be.",ch,NULL,victim,TO_CHAR);
        return;
    }

    if (is_affected(victim,gsn_bark_skin)) {
        effect_strip(victim,gsn_bark_skin);
        has_barkskin=TRUE;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = level;
    ef.location  = APPLY_AC;
    ef.modifier  = -40;
    ef.bitvector = EFF_STONESKIN;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    if (has_barkskin) {
        act( "$n's skin petrifies to stone.", victim, NULL, NULL, TO_ROOM );
        send_to_char( "Your skin petrifies to stone.\n\r", victim );
    } else {
        act( "$n's skin turns to stone.", victim, NULL, NULL, TO_ROOM );
        send_to_char( "Your skin turns to stone.\n\r", victim );
    }
    return;
}



void event_summon(EVENT_DATA *event) {
    CHAR_DATA *	ch=event->ch;
    CHAR_DATA *	victim=event->victim;
    ROOM_INDEX_DATA *from_room=victim->in_room;

    if (!IS_VALID(ch)) return;
    if (!IS_VALID(victim)) {
        send_to_char("You feel a disturbance in your powers.\n\r",ch);
        return;
    }

    act( "$n disappears without a trace.", victim, NULL, NULL, TO_ROOM );
    ap_leave_trigger(victim,ch->in_room);
    char_from_room( victim );
    char_to_room( victim, ch->in_room );
    ap_enter_trigger(victim,from_room);
    act( "$n arrives suddenly.", victim, NULL, NULL, TO_ROOM );
    act( "$n has summoned you!", ch, NULL, victim,   TO_VICT );
    look_room(victim,victim->in_room,TRUE);
    return;
}
void spell_summon( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim;

    if ( ( victim = get_char_world( ch, target_name ) ) == NULL
         ||   victim == ch
         ||   victim->in_room == NULL
         ||	 victim->in_room == ch->in_room
         ||   STR_IS_SET(ch->in_room->strbit_room_flags, ROOM_SAFE)
         ||   STR_IS_SET(victim->in_room->strbit_room_flags, ROOM_SAFE)
         ||   STR_IS_SET(victim->in_room->strbit_room_flags, ROOM_PRIVATE)
         ||   STR_IS_SET(victim->in_room->strbit_room_flags, ROOM_SOLITARY)
         ||   STR_IS_SET(victim->in_room->strbit_room_flags, ROOM_NO_RECALL)
         ||   (IS_NPC(victim) && STR_IS_SET(victim->strbit_act,ACT_AGGRESSIVE))
         ||   (IS_NPC(victim) && STR_IS_SET(victim->strbit_act,ACT_TCL_AGGRESSIVE))
         ||   victim->level >= level + 3
         ||   (!IS_NPC(victim) && victim->level >= LEVEL_IMMORTAL)
         ||   victim->fighting != NULL
         ||   (IS_NPC(victim) && STR_IS_SET(victim->strbit_imm_flags,IMM_SUMMON))
         ||	 (IS_NPC(victim) && victim->pIndexData->pShop != NULL)
         ||   (!IS_NPC(victim) && STR_IS_SET(victim->strbit_act,PLR_NOSUMMON))
         ||   (IS_NPC(victim) && saves_spell( level, victim,DAM_OTHER))
         ||   (!IS_NPC(victim) && IS_SET(ch->in_room->area->area_flags, AREA_PKZONE))
         ||   (!is_char_allowed_in_room(victim,ch->in_room,FALSE)) )

    {
        send_to_char( "You failed.\n\r", ch );
        return;
    }

    act("You feel yourself fading out!", ch, NULL, victim,   TO_VICT );
    act("$n starts to fade out.", victim, NULL, NULL, TO_ROOM );
    act("You invoke your powers to summon $N.",ch,NULL,victim,TO_CHAR);
    create_event(1,ch,victim,NULL,event_summon,NULL);
}



void spell_teleport( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    ROOM_INDEX_DATA *pRoomIndex;
    ROOM_INDEX_DATA *from_room=victim->in_room;

    if ( victim->in_room == NULL
         ||   STR_IS_SET(victim->in_room->strbit_room_flags, ROOM_NO_RECALL)
         || ( victim != ch && STR_IS_SET(victim->strbit_imm_flags,IMM_SUMMON))
         || ( !IS_NPC(ch) && victim->fighting != NULL )
         || ( victim != ch
              && ( saves_spell( level - 5, victim,DAM_OTHER))))
    {
        send_to_char( "You failed.\n\r", ch );
        return;
    }

    pRoomIndex = get_random_room(victim);

    if (victim != ch)
        send_to_char("You have been teleported!\n\r",victim);

    act( "$n vanishes!", victim, NULL, NULL, TO_ROOM );
    ap_leave_trigger(victim,pRoomIndex);
    char_from_room( victim );
    char_to_room( victim, pRoomIndex );
    ap_enter_trigger(victim,from_room);
    act( "$n slowly fades into existence.", victim, NULL, NULL, TO_ROOM );
    look_room(victim,victim->in_room,TRUE);
    return;
}



void spell_ventriloquate( int sn, int level, CHAR_DATA *ch,void *vo,int target)
{
    char buf1[MAX_STRING_LENGTH];
    char buf2[MAX_STRING_LENGTH];
    char speaker[MAX_INPUT_LENGTH];
    CHAR_DATA *vch;

    target_name = one_argument( target_name, speaker );

    sprintf( buf1, "%s says '"C_SAY"%s{x'.\n\r",              speaker, target_name );
    sprintf( buf2, "Someone makes %s say '"C_SAY"%s{x'.\n\r", capitalize(speaker), target_name );
    buf1[0] = UPPER(buf1[0]);

    for ( vch = ch->in_room->people; vch != NULL; vch = vch->next_in_room )
    {
        if (!is_exact_name( speaker, vch->name) && IS_AWAKE(vch))
            send_to_char( saves_spell(level,vch,DAM_OTHER) ? buf2 : buf1, vch );
    }

    return;
}



void spell_weaken( int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( is_affected( victim, sn ) || saves_spell( level, victim,DAM_OTHER) )
        return;

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = level / 2;
    ef.location  = APPLY_STR;
    ef.modifier  = -1 * (level / 5);
    ef.bitvector = EFF_WEAKEN;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char( "You feel your strength slip away.\n\r", victim );
    act("$n looks tired and weak.",victim,NULL,NULL,TO_ROOM);
    return;
}



/* RT recall spell is back */

void spell_word_of_recall( int sn, int level, CHAR_DATA *ch,void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    ROOM_INDEX_DATA *location;
    int vnum_room;
    int *pvnum;

    if (IS_NPC(victim))
        return;

    vnum_room=ROOM_VNUM_TEMPLE;

    if (victim->in_room && victim->in_room->area )
        vnum_room=victim->in_room->area->recall;

    if (vnum_room==ROOM_VNUM_TEMPLE && victim->recall)
        vnum_room=victim->recall;

    if (vnum_room==ROOM_VNUM_TEMPLE && victim->clan!=NULL &&
            victim->clan->clan_info->recall)
        vnum_room=victim->clan->clan_info->recall;

    if (( location = get_room_index( vnum_room ) ) == NULL
            && vnum_room != ROOM_VNUM_TEMPLE) {
        vnum_room=ROOM_VNUM_TEMPLE;
        location = get_room_index( vnum_room );
    }

    if (location == NULL)
    {
        send_to_char("You are completely lost.\n\r",victim);
        return;
    }

    if (STR_IS_SET(victim->in_room->strbit_room_flags,ROOM_NO_RECALL) ||
            IS_AFFECTED(victim,EFF_CURSE))
    {
        send_to_char("Spell failed.\n\r",victim);
        return;
    }

    pvnum=(int *)calloc(1,sizeof(int));
    *pvnum=location->vnum;
    create_event(2,victim,NULL,NULL,event_recall,pvnum);
}

/*
 * NPC spells.
 */
void spell_acid_breath( int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    int dam,hp_dam,dice_dam,hpch;

    act("$n spits acid at $N.",ch,NULL,victim,TO_NOTVICT);
    act("You spit acid at $N.",ch,NULL,victim,TO_CHAR);
    if (IS_AFFECTED(victim,EFF_HALLUCINATION))
        act("Ouch! You have been slimed!",ch,NULL,victim,TO_VICT);
    else
        act("$n spits a stream of corrosive acid at you.",
            ch,NULL,victim,TO_VICT);

    hpch = UMAX(12,ch->hit);
    hp_dam = number_range(hpch/11 + 1, hpch/6);
    dice_dam = dice(level,16);

    dam = UMAX(hp_dam + dice_dam/10,dice_dam + hp_dam/10);

    if (saves_spell(level,victim,DAM_ACID))
    {
        acid_effect(victim,level/2,dam/4,TARGET_CHAR);
        damage(ch,victim,dam/2,sn,DAM_ACID,TRUE, NULL);
    }
    else
    {
        acid_effect(victim,level,dam,TARGET_CHAR);
        damage(ch,victim,dam,sn,DAM_ACID,TRUE, NULL);
    }
}



void spell_fire_breath( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    CHAR_DATA *vch, *vch_next;
    int dam,hp_dam,dice_dam;
    int hpch;

    act("$n breathes forth a cone of fire.",ch,NULL,victim,TO_NOTVICT);
    act("$n breathes a cone of hot fire over you!",ch,NULL,victim,TO_VICT);
    act("You breath forth a cone of fire.",ch,NULL,NULL,TO_CHAR);

    hpch = UMAX( 10, ch->hit );
    hp_dam  = number_range( hpch/9+1, hpch/5 );
    dice_dam = dice(level,20);

    dam = UMAX(hp_dam + dice_dam /10, dice_dam + hp_dam / 10);
    fire_effect(victim->in_room,level,dam/2,TARGET_ROOM);

    for (vch = victim->in_room->people; vch != NULL; vch = vch_next)
    {
        vch_next = vch->next_in_room;

        if (is_safe_spell(ch,vch,TRUE)
                ||  (IS_NPC(vch) && IS_NPC(ch)
                     &&   (ch->fighting != vch || vch->fighting != ch)))
            continue;

        if (vch == victim) /* full damage */
        {
            if (saves_spell(level,vch,DAM_FIRE))
            {
                fire_effect(vch,level/2,dam/4,TARGET_CHAR);
                damage(ch,vch,dam/2,sn,DAM_FIRE,TRUE, NULL);
            }
            else
            {
                fire_effect(vch,level,dam,TARGET_CHAR);
                damage(ch,vch,dam,sn,DAM_FIRE,TRUE, NULL);
            }
        }
        else /* partial damage */
        {
            if (saves_spell(level - 2,vch,DAM_FIRE))
            {
                fire_effect(vch,level/4,dam/8,TARGET_CHAR);
                damage(ch,vch,dam/4,sn,DAM_FIRE,TRUE, NULL);
            }
            else
            {
                fire_effect(vch,level/2,dam/4,TARGET_CHAR);
                damage(ch,vch,dam/2,sn,DAM_FIRE,TRUE, NULL);
            }
        }
    }
}

void spell_frost_breath( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    CHAR_DATA *vch, *vch_next;
    int dam,hp_dam,dice_dam, hpch;

    act("$n breathes out a freezing cone of frost!",ch,NULL,victim,TO_NOTVICT);
    act("$n breathes a freezing cone of frost over you!",
        ch,NULL,victim,TO_VICT);
    act("You breath out a cone of frost.",ch,NULL,NULL,TO_CHAR);

    hpch = UMAX(12,ch->hit);
    hp_dam = number_range(hpch/11 + 1, hpch/6);
    dice_dam = dice(level,16);

    dam = UMAX(hp_dam + dice_dam/10,dice_dam + hp_dam/10);
    cold_effect(victim->in_room,level,dam/2,TARGET_ROOM);

    for (vch = victim->in_room->people; vch != NULL; vch = vch_next)
    {
        vch_next = vch->next_in_room;

        if (is_safe_spell(ch,vch,TRUE)
                ||  (IS_NPC(vch) && IS_NPC(ch)
                     &&   (ch->fighting != vch || vch->fighting != ch)))
            continue;

        if (vch == victim) /* full damage */
        {
            if (saves_spell(level,vch,DAM_COLD))
            {
                cold_effect(vch,level/2,dam/4,TARGET_CHAR);
                damage(ch,vch,dam/2,sn,DAM_COLD,TRUE, NULL);
            }
            else
            {
                cold_effect(vch,level,dam,TARGET_CHAR);
                damage(ch,vch,dam,sn,DAM_COLD,TRUE, NULL);
            }
        }
        else
        {
            if (saves_spell(level - 2,vch,DAM_COLD))
            {
                cold_effect(vch,level/4,dam/8,TARGET_CHAR);
                damage(ch,vch,dam/4,sn,DAM_COLD,TRUE, NULL);
            }
            else
            {
                cold_effect(vch,level/2,dam/4,TARGET_CHAR);
                damage(ch,vch,dam/2,sn,DAM_COLD,TRUE, NULL);
            }
        }
    }
}


void spell_gas_breath( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *vch;
    CHAR_DATA *vch_next;
    int dam,hp_dam,dice_dam,hpch;

    act("$n breathes out a cloud of poisonous gas!",ch,NULL,NULL,TO_ROOM);
    act("You breath out a cloud of poisonous gas.",ch,NULL,NULL,TO_CHAR);

    hpch = UMAX(16,ch->hit);
    hp_dam = number_range(hpch/15+1,8);
    dice_dam = dice(level,12);

    dam = UMAX(hp_dam + dice_dam/10,dice_dam + hp_dam/10);
    poison_effect(ch->in_room,level,dam,TARGET_ROOM);

    for (vch = ch->in_room->people; vch != NULL; vch = vch_next)
    {
        vch_next = vch->next_in_room;

        if (is_safe_spell(ch,vch,TRUE)
                ||  (IS_NPC(ch) && IS_NPC(vch)
                     &&   (ch->fighting == vch || vch->fighting == ch)))
            continue;

        if (saves_spell(level,vch,DAM_POISON))
        {
            poison_effect(vch,level/2,dam/4,TARGET_CHAR);
            damage(ch,vch,dam/2,sn,DAM_POISON,TRUE, NULL);
        }
        else
        {
            poison_effect(vch,level,dam,TARGET_CHAR);
            damage(ch,vch,dam,sn,DAM_POISON,TRUE, NULL);
        }
    }
}

void spell_lightning_breath(int sn,int level,CHAR_DATA *ch,void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    int dam,hp_dam,dice_dam,hpch;

    act("$n breathes a bolt of lightning at $N.",ch,NULL,victim,TO_NOTVICT);
    act("$n breathes a bolt of lightning at you!",ch,NULL,victim,TO_VICT);
    act("You breathe a bolt of lightning at $N.",ch,NULL,victim,TO_CHAR);

    hpch = UMAX(10,ch->hit);
    hp_dam = number_range(hpch/9+1,hpch/5);
    dice_dam = dice(level,20);

    dam = UMAX(hp_dam + dice_dam/10,dice_dam + hp_dam/10);

    if (saves_spell(level,victim,DAM_LIGHTNING))
    {
        shock_effect(victim,level/2,dam/4,TARGET_CHAR);
        damage(ch,victim,dam/2,sn,DAM_LIGHTNING,TRUE, NULL);
    }
    else
    {
        shock_effect(victim,level,dam,TARGET_CHAR);
        damage(ch,victim,dam,sn,DAM_LIGHTNING,TRUE, NULL);
    }
}

/*
 * Spells for mega1.are from Glop/Erkenbrand.
 */
void spell_general_purpose(int sn,int level,CHAR_DATA *ch,void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    int dam;

    dam = number_range( 25, 100 );
    if ( saves_spell( level, victim, DAM_PIERCE) )
        dam /= 2;
    damage( ch, victim, dam, sn, DAM_PIERCE ,TRUE, NULL);
    return;
}

void spell_high_explosive(int sn,int level,CHAR_DATA *ch,void *vo,int target)
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    int dam;

    dam = number_range( 30, 120 );
    if ( saves_spell( level, victim, DAM_PIERCE) )
        dam /= 2;
    damage( ch, victim, dam, sn, DAM_PIERCE ,TRUE, NULL);
    return;
}

void spell_knock(int sn,int level,CHAR_DATA *ch,void *vo,int target)
{
    char buf[MAX_STRING_LENGTH];
    OBJ_DATA *obj, *obj_next;
    int door, knockpow, doorcount = 0,diff,objcount = 0;
    EXIT_DATA *exit;

    knockpow = get_skill(ch,gsn_knock) * 0.8;

    for (door = 0 ; door < DIR_MAX ; door++) {
        if ( ( exit = ch->in_room->exit[door]) == NULL )
            continue;
        if (IS_SET(exit->exit_info, EX_ISDOOR)
                &&  IS_SET(exit->exit_info, EX_LOCKED)
                &&  IS_SET(exit->exit_info, EX_CLOSED)
                &&  !IS_SET(exit->exit_info,EX_PICKPROOF)) {
            diff=number_percent();
            if (IS_SET(exit->exit_info,EX_EASY))
                diff-=25;
            if (IS_SET(exit->exit_info,EX_HARD))
                diff+=25;
            if (IS_SET(exit->exit_info,EX_INFURIATING))
                diff+=50;
            if (diff < knockpow) {
                REMOVE_BIT(exit->exit_info, EX_LOCKED);
                doorcount++;
            }
        }
    }

    for (obj = ch->in_room->contents; obj != NULL; obj = obj_next ) {
        obj_next = obj->next_content;
        if (can_see_obj( ch, obj ) ) {
            if (obj->item_type == ITEM_PORTAL) {
                if (!IS_SET(obj->value[1],EX_ISDOOR) ||
                        !IS_SET(obj->value[1],EX_LOCKED) ||
                        !IS_SET(obj->value[1],EX_CLOSED) ||
                        obj->value[4] <= 0               ||
                        IS_SET(obj->value[1],EX_PICKPROOF))
                    continue;
                diff=number_percent();
                if (IS_SET(obj->value[1],EX_EASY))
                    diff-=25;
                if (IS_SET(obj->value[1],EX_HARD))
                    diff+=25;
                if (IS_SET(obj->value[1],EX_INFURIATING))
                    diff+=50;
                if (diff < knockpow) {
                    REMOVE_BIT(obj->value[1],EX_LOCKED);
                    act("$p clicks open.",ch,obj,NULL,TO_ALL);
                    objcount++;
                }
            }
            if (obj->item_type != ITEM_CONTAINER    ||
                    !IS_SET(obj->value[1], CONT_CLOSED) ||
                    obj->value[2] <= 0                  ||
                    !IS_SET(obj->value[1], CONT_LOCKED) ||
                    IS_SET(obj->value[1], CONT_PICKPROOF))
                continue;
            if (number_percent() < knockpow) {
                REMOVE_BIT(obj->value[1], CONT_LOCKED);
                act("$p clicks open.",ch,obj,NULL,TO_ALL);
                objcount++;
            }
        }
    }

    for ( obj = ch->carrying; obj != NULL; obj = obj->next_content ) {
        obj_next = obj->next_content;
        if (can_see_obj( ch, obj ) ) {
            if (obj->item_type == ITEM_PORTAL) {
                if (!IS_SET(obj->value[1],EX_ISDOOR) ||
                        !IS_SET(obj->value[1],EX_CLOSED) ||
                        !IS_SET(obj->value[1],EX_LOCKED) ||
                        obj->value[4] <= 0               ||
                        IS_SET(obj->value[1],EX_PICKPROOF))
                    continue;
                diff=number_percent();
                if (IS_SET(obj->value[1],EX_EASY))
                    diff-=25;
                if (IS_SET(obj->value[1],EX_HARD))
                    diff+=25;
                if (IS_SET(obj->value[1],EX_INFURIATING))
                    diff+=50;
                if (diff < knockpow) {
                    REMOVE_BIT(obj->value[1],EX_LOCKED);
                    act("$p clicks open.",ch,obj,NULL,TO_CHAR);
                    objcount++;
                }
            }
            if (obj->item_type != ITEM_CONTAINER    ||
                    !IS_SET(obj->value[1], CONT_CLOSED) ||
                    obj->value[2] <= 0                  ||
                    !IS_SET(obj->value[1], CONT_LOCKED) ||
                    IS_SET(obj->value[1], CONT_PICKPROOF))
                continue;
            if (number_percent() < knockpow) {
                REMOVE_BIT(obj->value[1], CONT_LOCKED);
                act("$p clicks open.",ch,obj,NULL,TO_CHAR);
                objcount++;
            }
        }
    }

    if (doorcount > 0) {
        if(doorcount == 1)
            sprintf(buf,"A door clicks open.");
        else
            sprintf(buf,"Doors click open.");
        act(buf,ch,NULL,NULL,TO_ALL);
    }

    if (doorcount == 0 && objcount == 0)
        send_to_char("Your wait for the click and hear...... nothing?!?.\n\r",ch);
    return;

}

void spell_lock(int sn,int level,CHAR_DATA *ch,void *vo,int target)
{
    char buf[MAX_STRING_LENGTH];
    OBJ_DATA *obj, *obj_next;
    int door, lockpow, doorcount = 0,diff,objcount = 0;
    EXIT_DATA *exit;

    lockpow = get_skill(ch,gsn_lock) * 0.8;

    for (door = 0 ; door < DIR_MAX ; door++) {
        if ( ( exit = ch->in_room->exit[door]) == NULL )
            continue;
        if (IS_SET(exit->exit_info, EX_ISDOOR)    &&
                IS_SET(exit->exit_info, EX_CLOSED)    &&
                !IS_SET(exit->exit_info, EX_LOCKED)   &&
                !IS_SET(exit->exit_info,EX_PICKPROOF) &&
                IS_SET(exit->rs_flags, EX_LOCKED) ) {
            diff=number_percent();
            if (IS_SET(exit->exit_info,EX_EASY))
                diff-=25;
            if (IS_SET(exit->exit_info,EX_HARD))
                diff+=25;
            if (IS_SET(exit->exit_info,EX_INFURIATING))
                diff+=50;
            if (diff < lockpow) {
                doorcount++;
                SET_BIT(exit->exit_info, EX_LOCKED);
            }
        }
    }

    for (obj = ch->in_room->contents; obj != NULL; obj = obj_next ) {
        obj_next = obj->next_content;
        if (can_see_obj( ch, obj ) ) {
            if (obj->item_type == ITEM_PORTAL) {
                if (!IS_SET(obj->value[1],EX_ISDOOR) ||
                        !IS_SET(obj->value[1],EX_CLOSED) ||
                        IS_SET(obj->value[1],EX_LOCKED)  ||
                        obj->value[4] <= 0               ||
                        IS_SET(obj->value[1],EX_PICKPROOF))
                    continue;
                diff=number_percent();
                if (IS_SET(obj->value[1],EX_EASY))
                    diff-=25;
                if (IS_SET(obj->value[1],EX_HARD))
                    diff+=25;
                if (IS_SET(obj->value[1],EX_INFURIATING))
                    diff+=50;
                if (diff < lockpow) {
                    SET_BIT(obj->value[1],EX_LOCKED);
                    act("$p falls in its lock.",ch,obj,NULL,TO_ALL);
                    objcount++;
                }
            }
            if (obj->item_type != ITEM_CONTAINER    ||
                    !IS_SET(obj->value[1], CONT_CLOSED) ||
                    obj->value[2] <= 0                  ||
                    IS_SET(obj->value[1], CONT_LOCKED)  ||
                    IS_SET(obj->value[1], CONT_PICKPROOF))
                continue;
            if (number_percent() < lockpow) {
                SET_BIT(obj->value[1], CONT_LOCKED);
                act("$p falls in its lock.",ch,obj,NULL,TO_ALL);
                objcount++;
            }
        }
    }

    for ( obj = ch->carrying; obj != NULL; obj = obj->next_content ) {
        obj_next = obj->next_content;
        if (can_see_obj( ch, obj ) ) {
            if (obj->item_type == ITEM_PORTAL) {
                if (!IS_SET(obj->value[1],EX_ISDOOR) ||
                        !IS_SET(obj->value[1],EX_CLOSED) ||
                        IS_SET(obj->value[1],EX_LOCKED)  ||
                        obj->value[4] <= 0               ||
                        IS_SET(obj->value[1],EX_PICKPROOF))
                    continue;
                diff=number_percent();
                if (IS_SET(obj->value[1],EX_EASY))
                    diff-=25;
                if (IS_SET(obj->value[1],EX_HARD))
                    diff+=25;
                if (IS_SET(obj->value[1],EX_INFURIATING))
                    diff+=50;
                if (diff < lockpow) {
                    SET_BIT(obj->value[1],EX_LOCKED);
                    act("$p falls in its lock.",ch,obj,NULL,TO_CHAR);
                    objcount++;
                }
            }
            if (obj->item_type != ITEM_CONTAINER    ||
                    !IS_SET(obj->value[1], CONT_CLOSED) ||
                    obj->value[2] <= 0                  ||
                    IS_SET(obj->value[1], CONT_LOCKED)  ||
                    IS_SET(obj->value[1], CONT_PICKPROOF))
                continue;
            if (number_percent() < lockpow) {
                SET_BIT(obj->value[1], CONT_LOCKED);
                act("$p falls in its lock.",ch,obj,NULL,TO_CHAR);
                objcount++;
            }
        }
    }

    if (doorcount > 0) {
        if(doorcount == 1)
            sprintf(buf,"A door falls in its lock.");
        else
            sprintf(buf,"Doors fall in their lock.");
        act(buf,ch,NULL,NULL,TO_ALL);
    }

    if (doorcount == 0 && objcount == 0)
        send_to_char("You wait for the click and hear...... nothing?!?\n\r",ch);
}

