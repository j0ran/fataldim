/* $Id: fd_data.h,v 1.15 2008/05/11 20:50:47 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,	   *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *									   *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael	   *
 *  Chastain, Michael Quan, and Mitchell Tse.				   *
 *									   *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc	   *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.						   *
 *									   *
 *  Much time and thought has gone into this software and you are	   *
 *  benefitting.  We hope that you share your changes too.  What goes	   *
 *  around, comes around.						   *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

struct flag_type
{
    char *name;
    int bit;
    bool settable;
};

struct position_type
{
    char *name;
    char *short_name;
};

struct sex_type
{
    char *name;
};

struct hunt_type
{
    char *name;
};

struct size_type
{
    char *name;
};

struct attribute_type {
    char *name;
    int attribute;
};

//
// real tables (defined in fd_data_table.c)
//
extern  const	TABLE_TYPE	item_table	[];
extern  const	TABLE_TYPE	reboot_table	[];
extern  const	TABLE_TYPE	socket_table	[];
extern  const	TABLE_TYPE	property_table	[];
extern  const	TABLE_TYPE	telnet_table	[];
extern	const	TABLE_TYPE	sector_table	[];
extern	const	TABLE_TYPE	sex_table	[];
extern	const	TABLE_TYPE	liquid_table	[];
extern	const	TABLE_TYPE	damagetype_table[];
extern  const	TABLE_TYPE	effect_where_table[];
extern 	const	TABLE_TYPE	shoptype_table	[];
extern  const	TABLE_TYPE	weather_sun_table[];
extern  const	TABLE_TYPE	weather_sky_table[];
extern 	const	TABLE_TYPE	questconsumption_table[];
extern 	const	TABLE_TYPE	questeffects_table	[];

//
// game tables (defined in fd_data_const.c)
//
extern	const	struct	position_type	position_table	[];
extern	const	struct	hunt_type	hunt_table	[];
extern	const	struct	size_type	size_table	[];
extern	const	struct	str_app_type	str_app		[26];
extern	const	struct	int_app_type	int_app		[26];
extern	const	struct	wis_app_type	wis_app		[26];
extern	const	struct	dex_app_type	dex_app		[26];
extern	const	struct	con_app_type	con_app		[26];
extern  const   struct  class_type      class_table	[MAX_CLASS];

#define CLASS_MAGE	0
#define CLASS_CLERIC	1
#define CLASS_THIEF	2
#define CLASS_WARRIOR	3

extern  const   struct  weapon_type     weapon_table	[];
extern  const   struct  wiznet_type     wiznet_table	[];
extern  const   struct  attack_type     attack_table	[];
extern          struct  race_type       race_table      [];
extern  const   struct  pc_race_type    pc_race_table	[];
extern  const   struct  spec_type       spec_table	[];
extern  const   struct  liq_type        liq_table	[];
extern  const   struct  skilltable_type skill_table	[MAX_SKILL];
extern  const   struct  grouptable_type group_table	[MAX_GROUP];
//tern          struct  social_type     social_table	[MAX_SOCIALS];
extern                  SOCIAL_TYPE     *social_list;
extern                  EVENT_DATA      *event_list;
extern                  EVENT_DATA      *event_new;
extern  const		CHANNEL_DATA	channel_table	[];
extern  		NOTES_DATA	notes_table	[];
extern  char *  const                   title_table	[MAX_CLASS]
							[MAX_LEVEL+1]
							[2];
extern const OBJ_WEIGHT_INFO obj_char_weight_table	[];
extern const OBJ_WEIGHT_INFO obj_anon_weight_table	[];
extern const LORE_RESPONSES  lore_failure_table		[];
extern const LORE_RESPONSES  lore_success_table		[];
extern const LORE_RESPONSES  lore_ability_table		[];
extern const char *          day_name			[];
extern const char *          month_name			[];
extern  const   struct  spec_type       spec_table      [];


extern char *	const		he_she			[];
extern char *	const		him_her			[];
extern char *	const		his_her			[];
extern char *	const		dir_name		[];
extern char *	const		dir_name_short		[];
extern const int		rev_dir			[];

//
// optioned tables (defined in fd_data_options.c)
//
extern	const	OPTION_TYPE	act_options		[];
extern	const	OPTION_TYPE	area_options		[];
extern	const	OPTION_TYPE	plr_options		[];
extern	const	OPTION_TYPE	effect_options		[];
extern	const	OPTION_TYPE	effect2_options		[];
extern	const	OPTION_TYPE	apply_options		[];
extern	const	OPTION_TYPE	off_options		[];
extern	const	OPTION_TYPE	imm_options		[];
extern	const	OPTION_TYPE	res_options		[];
extern	const	OPTION_TYPE	vuln_options		[];
extern	const	OPTION_TYPE	form_options		[];
extern	const	OPTION_TYPE	part_options		[];
extern	const	OPTION_TYPE	comm_options		[];
extern	const	OPTION_TYPE	extra_options		[];
extern	const	OPTION_TYPE	extra2_options		[];
extern	const	OPTION_TYPE	wear_options		[];
extern	const	OPTION_TYPE	container_options	[];
extern	const	OPTION_TYPE	portal_options		[];
extern	const	OPTION_TYPE	room_options		[];
extern	const	OPTION_TYPE	exit_options		[];
extern	const	OPTION_TYPE	weaponflag_options	[];
extern	const	OPTION_TYPE	gate_options		[];
extern	const	OPTION_TYPE	furniture_options	[];
extern 	const	OPTION_TYPE	wear_loc_options	[];
extern 	const	OPTION_TYPE	itemtype_options	[];
extern	const	char		*attribute_flags	[];
extern	const	char		*target_flags		[];

/* Functions */

bool is_stat	(const struct flag_type *flag_table);
