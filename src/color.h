/* $Id: color.h,v 1.10 2001/07/17 23:45:16 edwin Exp $ */
/*
 * Colour stuff by Lope of Loping Through The MUD
 */
#define CLEAR		"\033[0m"		/* Resets Colour	*/

#define C_RED		"\033[0;31m"	/* Normal Colours	*/
#define C_GREEN		"\033[0;32m"
#define C_YELLOW	"\033[0;33m"
#define C_BLUE		"\033[0;34m"
#define C_MAGENTA	"\033[0;35m"
#define C_CYAN		"\033[0;36m"
#define C_WHITE		"\033[0;37m"
#define C_D_GREY	"\033[1;30m"  	/* Light Colors		*/
#define C_B_RED		"\033[1;31m"
#define C_B_GREEN	"\033[1;32m"
#define C_B_YELLOW	"\033[1;33m"
#define C_B_BLUE	"\033[1;34m"
#define C_B_MAGENTA	"\033[1;35m"
#define C_B_CYAN	"\033[1;36m"
#define C_B_WHITE	"\033[1;37m"

#define B_RED		"\033[41m"	/* Normal Colours	*/
#define B_GREEN		"\033[42m"
#define B_YELLOW	"\033[43m"
#define B_BLUE		"\033[44m"
#define B_MAGENTA	"\033[45m"
#define B_CYAN		"\033[46m"
#define B_WHITE		"\033[47m"

#define C_UNDERLINE	"\033[4m"
#define C_BLINK		"\033[5m"
#define C_INVERSE	"\033[7m"
#define C_STRIKETHROUGH	"\033[8m"

/* x - clear
 * b - blue
 * c - cyan
 * g - green
 * m - magenta
 * r - red
 * w - white
 * y - yellow
 * B - light blue
 * C - light cyan
 * G - light green
 * M - light magenta
 * R - light red
 * W - bright white
 * Y - light yellow
 * D - dark gray
 */

#define C_ROOMNAME	"{w"
#define C_HITYOU	"{r"
#define C_HITROOM	"{m"
#define C_MISYOU	"{g"
#define C_MISROOM	"{g"
#define C_YOUMIS	"{b"
#define C_YOUHIT	"{y"

#define C_QUEST		"{g"
#define C_GOSSIP	"{c"
#define C_TELL		"{r"
#define C_REPLY		"{r"
#define C_SAY		"{y"
#define C_IMMTALK	"{g"
#define C_AUCTION	"{y"
#define C_GRATS		"{b"
#define C_QUOTE		"{w"
#define C_QUESTION	"{y"
#define C_ANSWER	"{y"
#define C_MUSIC		"{c"
#define C_CLAN		"{m"
#define C_SHOUT		"{w"
#define C_YELL		"{w"
#define C_GTELL		"{m"
#define C_PRAY		"{y"
#define C_HEROTALK	"{g"

#define C_WHO_BRACKET	"{c"
#define C_WHO_LEVEL	"{y"
#define C_WHO_RACE	"{r"
#define C_WHO_CLASS	"{g"
