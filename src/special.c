/* $Id: special.c,v 1.43 2006/08/25 09:43:21 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

/*
 * 19980125 EG	Mobiles now also lose mana when casting spells,
 *		this is done in the function do_mob_cast.
 */

#include "merc.h"
#include "interp.h"
#include "color.h"

/*
 * The following special functions are available for mobiles.
 */
DECLARE_SPEC_FUN(	spec_puff		);
DECLARE_SPEC_FUN(	spec_breath_any		);
DECLARE_SPEC_FUN(	spec_breath_acid	);
DECLARE_SPEC_FUN(	spec_breath_fire	);
DECLARE_SPEC_FUN(	spec_breath_frost	);
DECLARE_SPEC_FUN(	spec_breath_gas		);
DECLARE_SPEC_FUN(	spec_breath_lightning	);
DECLARE_SPEC_FUN(	spec_cast_adept		);
DECLARE_SPEC_FUN(	spec_cast_cleric	);
DECLARE_SPEC_FUN(	spec_cast_judge		);
DECLARE_SPEC_FUN(	spec_cast_mage		);
DECLARE_SPEC_FUN(	spec_cast_undead	);
DECLARE_SPEC_FUN(	spec_executioner	);
DECLARE_SPEC_FUN(	spec_fido		);
DECLARE_SPEC_FUN(	spec_guard		);
DECLARE_SPEC_FUN(	spec_janitor		);
DECLARE_SPEC_FUN(	spec_mayor		);
DECLARE_SPEC_FUN(	spec_poison		);
DECLARE_SPEC_FUN(	spec_thief		);
DECLARE_SPEC_FUN(	spec_nasty		);
DECLARE_SPEC_FUN(	spec_troll_member	);
DECLARE_SPEC_FUN(	spec_ogre_member	);
DECLARE_SPEC_FUN(	spec_patrolman		);
DECLARE_SPEC_FUN(       spec_assassin           );
DECLARE_SPEC_FUN(	spec_banker		);
DECLARE_SPEC_FUN(	spec_pinky		);
DECLARE_SPEC_FUN(	spec_brain		);
DECLARE_SPEC_FUN(	spec_manadrain		);
DECLARE_SPEC_FUN(	spec_questmaster	);
DECLARE_SPEC_FUN(	spec_random_text	);
DECLARE_SPEC_FUN(	spec_random_command	);
DECLARE_SPEC_FUN(	spec_command_sequence	);
DECLARE_SPEC_FUN(	spec_stringmaster	);

/* the function table */
const   struct  spec_type    spec_table[] =
{
    {	"spec_breath_any",		spec_breath_any		},
    {	"spec_breath_acid",		spec_breath_acid	},
    {	"spec_breath_fire",		spec_breath_fire	},
    {	"spec_breath_frost",		spec_breath_frost	},
    {	"spec_breath_gas",		spec_breath_gas		},
    {	"spec_breath_lightning",	spec_breath_lightning	},
    {	"spec_cast_adept",		spec_cast_adept		},
    {	"spec_cast_cleric",		spec_cast_cleric	},
    {	"spec_cast_judge",		spec_cast_judge		},
    {	"spec_cast_mage",		spec_cast_mage		},
    {	"spec_cast_undead",		spec_cast_undead	},
    {	"spec_executioner",		spec_executioner	},
    {	"spec_fido",			spec_fido		},
    {	"spec_puff",			spec_puff		},
    {	"spec_guard",			spec_guard		},
    {	"spec_janitor",			spec_janitor		},
    {	"spec_mayor",			spec_mayor		},
    {	"spec_poison",			spec_poison		},
    {	"spec_thief",			spec_thief		},
    {	"spec_nasty",			spec_nasty		},
    {	"spec_troll_member",		spec_troll_member	},
    {	"spec_ogre_member",		spec_ogre_member	},
    {	"spec_patrolman",		spec_patrolman		},
    {	"spec_assassin",		spec_assassin		},
    {	"spec_banker",			spec_banker		},
    {	"spec_pinky",			spec_pinky		},
    {	"spec_brain",			spec_brain		},
    {	"spec_manadrain",		spec_manadrain		},
    {	"spec_questmaster",		spec_questmaster	},
    {	"spec_random_text",		spec_random_text	},
    {	"spec_random_command",		spec_random_command	},
    {	"spec_command_sequence",	spec_command_sequence	},
    {	"spec_stringmaster",		spec_stringmaster	},


    /*
     * End of list
     */
    {	"",				NULL			}
};

/*****************************************************************************
 Name:		spec_string
 Purpose:	Given a function, return the appropriate name.
 Called by:	<???>
 ****************************************************************************/
char *spec_string( SPEC_FUN *fun )	/* OLC */
{
    int cmd;

    for ( cmd = 0; spec_table[cmd].function; cmd++ )	/* OLC 1.1b */
	if ( fun == spec_table[cmd].function )
	    return spec_table[cmd].name;

    return 0;
}



/*****************************************************************************
 Name:		spec_lookup
 Purpose:	Given a name, return the appropriate spec fun.
 Called by:	do_mset(act_wiz.c) load_specials,reset_area(db.c)
 ****************************************************************************/
SPEC_FUN *spec_lookup( const char *name )	/* OLC */
{
    int cmd;

    for ( cmd = 0; *spec_table[cmd].name; cmd++ )	/* OLC 1.1b */
	if ( !str_cmp( name, spec_table[cmd].name ) )
	    return spec_table[cmd].function;

    return 0;
}

bool spec_troll_member( CHAR_DATA *ch)
{
    CHAR_DATA *vch, *victim = NULL;
    int count = 0;
    char *message;

    if (!IS_AWAKE(ch) || IS_AFFECTED(ch,EFF_CALM) || ch->in_room == NULL
    ||  IS_AFFECTED(ch,EFF_CHARM) || ch->fighting != NULL)
	return FALSE;

    /* find an ogre to beat up */
    for (vch = ch->in_room->people;  vch != NULL;  vch = vch->next_in_room)
    {
	if (!IS_NPC(vch) || ch == vch)
	    continue;

	if (vch->pIndexData->vnum == MOB_VNUM_PATROLMAN)
	    return FALSE;

	if (vch->pIndexData->group == GROUP_VNUM_OGRES
	&&  ch->level > vch->level - 2 && !is_safe(ch,vch))
	{
	    if (number_range(0,count) == 0)
		victim = vch;

	    count++;
	}
    }

    if (victim == NULL)
	return FALSE;

    /* say something, then raise hell */
    switch (number_range(0,6))
    {
	default:  message = NULL; 	break;
	case 0:	message = "$n yells '"C_YELL"I've been looking for you, punk!{x'";
		break;
	case 1: message = "With a scream of rage, $n attacks $N.";
		break;
	case 2: message =
		"$n says '"C_SAY"What's slimy Ogre trash like you doing around here?{x'";
		break;
	case 3: message = "$n cracks $s knuckles and says '"C_SAY"Do ya feel lucky?{x'";
		break;
	case 4: message = "$n says '"C_SAY"There's no cops to save you this time!{x'";
		break;
	case 5: message = "$n says '"C_SAY"Time to join your brother, spud.{x'";
		break;
	case 6: message = "$n says '"C_SAY"Let's rock.{x'";
		break;
    }

    if (MOB_HAS_TRIGGER(ch,MTRIG_PRESPEC) &&
	!mp_prespec_trigger(ch,victim,NULL))
	return FALSE;
    if (message != NULL)
    	act(message,ch,NULL,victim,TO_ALL);
    multi_hit( ch, victim, TYPE_UNDEFINED );
    if (MOB_HAS_TRIGGER(ch,MTRIG_SPEC))
	mp_spec_trigger(ch,victim,NULL);
    return TRUE;
}

bool spec_ogre_member( CHAR_DATA *ch)
{
    CHAR_DATA *vch, *victim = NULL;
    int count = 0;
    char *message;

    if (!IS_AWAKE(ch) || IS_AFFECTED(ch,EFF_CALM) || ch->in_room == NULL
    ||  IS_AFFECTED(ch,EFF_CHARM) || ch->fighting != NULL)
        return FALSE;

    /* find an troll to beat up */
    for (vch = ch->in_room->people;  vch != NULL;  vch = vch->next_in_room)
    {
        if (!IS_NPC(vch) || ch == vch)
            continue;

        if (vch->pIndexData->vnum == MOB_VNUM_PATROLMAN)
            return FALSE;

        if (vch->pIndexData->group == GROUP_VNUM_TROLLS
        &&  ch->level > vch->level - 2 && !is_safe(ch,vch))
        {
            if (number_range(0,count) == 0)
                victim = vch;

            count++;
        }
    }

    if (victim == NULL)
        return FALSE;

    /* say something, then raise hell */
    switch (number_range(0,6))
    {
	default: message = NULL;	break;
        case 0: message = "$n yells '"C_YELL"I've been looking for you, punk!{x'";
                break;
        case 1: message = "With a scream of rage, $n attacks $N.'";
                break;
        case 2: message =
                "$n says '"C_SAY"What's Troll filth like you doing around here?{x'";
                break;
        case 3: message = "$n cracks $s knuckles and says '"C_SAY"Do ya feel lucky?{x'";
                break;
        case 4: message = "$n says '"C_SAY"There's no cops to save you this time!{x'";
                break;
        case 5: message = "$n says '"C_SAY"Time to join your brother, spud.{x'";
                break;
        case 6: message = "$n says '"C_SAY"Let's rock.{x'";
                break;
    }

    if (MOB_HAS_TRIGGER(ch,MTRIG_PRESPEC) &&
	!mp_prespec_trigger(ch,victim,NULL))
	return FALSE;
    if (message != NULL)
    	act(message,ch,NULL,victim,TO_ALL);
    multi_hit( ch, victim, TYPE_UNDEFINED );
    if (MOB_HAS_TRIGGER(ch,MTRIG_SPEC))
	mp_spec_trigger(ch,victim,NULL);
    return TRUE;
}

bool spec_patrolman(CHAR_DATA *ch)
{
    CHAR_DATA *vch,*victim = NULL;
    OBJ_DATA *obj;
    char *message;
    int count = 0;

    if (!IS_AWAKE(ch) || IS_AFFECTED(ch,EFF_CALM) || ch->in_room == NULL
    ||  IS_AFFECTED(ch,EFF_CHARM) || ch->fighting != NULL)
        return FALSE;

    /* look for a fight in the room */
    for (vch = ch->in_room->people; vch != NULL; vch = vch->next_in_room)
    {
	if (vch == ch)
	    continue;

	if (vch->fighting != NULL)  /* break it up! */
	{
	    if (number_range(0,count) == 0)
	        victim = (vch->level > vch->fighting->level)
		    ? vch : vch->fighting;
	    count++;
	}
    }

    if (victim == NULL || (IS_NPC(victim) && victim->spec_fun == ch->spec_fun))
	return FALSE;

    if (MOB_HAS_TRIGGER(ch,MTRIG_PRESPEC) &&
	!mp_prespec_trigger(ch,victim,NULL))
	return FALSE;

    if (((obj = get_eq_char(ch,WEAR_NECK_1)) != NULL
    &&   obj->pIndexData->vnum == OBJ_VNUM_WHISTLE)
    ||  ((obj = get_eq_char(ch,WEAR_NECK_2)) != NULL
    &&   obj->pIndexData->vnum == OBJ_VNUM_WHISTLE))
    {
	act("You blow down hard on $p.",ch,obj,NULL,TO_CHAR);
	act("$n blows on $p, ***WHEEEEEEEEEEEET***",ch,obj,NULL,TO_ROOM);

    	for ( vch = char_list; vch != NULL; vch = vch->next )
    	{
            if ( vch->in_room == NULL )
            	continue;

            if (vch->in_room != ch->in_room
	    &&  vch->in_room->area == ch->in_room->area)
            	send_to_char( "You hear a shrill whistling sound.\n\r", vch );
    	}
    }

    switch (number_range(0,6))
    {
	default:	message = NULL;		break;
	case 0:	message = "$n yells '"C_YELL"All roit! All roit! break it up!{x'";
		break;
	case 1: message =
		"$n says '"C_SAY"Society's to blame, but what's a bloke to do?{x'";
		break;
	case 2: message =
		"$n mumbles '"C_SAY"bloody kids will be the death of us all.{x'";
		break;
	case 3: message = "$n shouts '"C_SHOUT"Stop that! Stop that!{x' and attacks.";
		break;
	case 4: message = "$n pulls out $s billy and goes to work.";
		break;
	case 5: message =
		"$n sighs in resignation and proceeds to break up the fight.";
		break;
	case 6: message = "$n says '"C_SAY"Settle down, you hooligans!{x'";
		break;
    }

    if (message != NULL)
	act(message,ch,NULL,NULL,TO_ALL);

    multi_hit(ch,victim,TYPE_UNDEFINED);

    if (MOB_HAS_TRIGGER(ch,MTRIG_SPEC))
	mp_spec_trigger(ch,victim,NULL);
    return TRUE;
}


bool spec_nasty( CHAR_DATA *ch )
{
    CHAR_DATA *victim, *v_next;
    long gold;

    if (!IS_AWAKE(ch)) {
       return FALSE;
    }

    if (ch->position != POS_FIGHTING) {
        for ( victim = ch->in_room->people; victim != NULL; victim = v_next) {
	    v_next = victim->next_in_room;
	    if (!IS_NPC(victim)
	       && (victim->level > ch->level)
	       && (victim->level < ch->level + 10)) {
		if (MOB_HAS_TRIGGER(ch,MTRIG_PRESPEC) &&
		    !mp_prespec_trigger(ch,victim,NULL))
		    return FALSE;
	        do_function(ch,&do_backstab,victim->name);
	        if (ch->position != POS_FIGHTING)
	 	    do_function(ch,&do_murder,victim->name);
		if (MOB_HAS_TRIGGER(ch,MTRIG_SPEC))
		    mp_spec_trigger(ch,victim,NULL);
	        /* should steal some coins right away? :) */
	        return TRUE;
	    }
        }
        return FALSE;    /*  No one to attack */
    }

    /* okay, we must be fighting.... steal some coins and flee */
    if ( (victim = ch->fighting) == NULL)
        return FALSE;   /* let's be paranoid.... */

    if (number_bits(1)) return FALSE;

    if (MOB_HAS_TRIGGER(ch,MTRIG_PRESPEC) &&
	!mp_prespec_trigger(ch,victim,NULL))
	return FALSE;

    if ( number_bits(1) ) {
        act( "$n rips apart your coin purse, spilling your gold!",
	    ch, NULL, victim, TO_VICT);
	act( "You slash apart $N's coin purse and gather $S gold.",
            ch, NULL, victim, TO_CHAR);
        act( "$N's coin purse is ripped apart!",
            ch, NULL, victim, TO_NOTVICT);
        gold = victim->gold / 10;  /* steal 10% of his gold */
        victim->gold -= gold;
        ch->gold     += gold;
    } else {
        do_function(ch,&do_flee,"");
    }
    if (MOB_HAS_TRIGGER(ch,MTRIG_SPEC))
	mp_spec_trigger(ch,victim,NULL);
    return TRUE;
}

/*
 * Core procedure for dragons.
 */
bool dragon( CHAR_DATA *ch, char *spell_name )
{
    CHAR_DATA *victim;
    CHAR_DATA *v_next;
    int sn;

    if ( ch->position != POS_FIGHTING )
	return FALSE;

    for ( victim = ch->in_room->people; victim != NULL; victim = v_next )
    {
	v_next = victim->next_in_room;
	if ( victim->fighting == ch && number_bits( 3 ) == 0 )
	    break;
    }

    if ( victim == NULL )
	return FALSE;

    if ( ( sn = skill_lookup( spell_name ) ) < 0 )
	return FALSE;
    if (MOB_HAS_TRIGGER(ch,MTRIG_PRESPEC) &&
	!mp_prespec_trigger(ch,victim,NULL))
	return FALSE;
    (*skill_spell_fun(sn,ch)) ( sn, ch->level, ch, victim, TARGET_CHAR);
    if (MOB_HAS_TRIGGER(ch,MTRIG_SPEC))
	mp_spec_trigger(ch,victim,NULL);
    return TRUE;
}



/*
 * Special procedures for mobiles.
 */
bool spec_breath_any( CHAR_DATA *ch )
{
    if ( ch->position != POS_FIGHTING )
	return FALSE;

    switch ( number_bits( 3 ) )
    {
    case 0: return spec_breath_fire		( ch );
    case 1:
    case 2: return spec_breath_lightning	( ch );
    case 3: return spec_breath_gas		( ch );
    case 4: return spec_breath_acid		( ch );
    case 5:
    case 6:
    case 7: return spec_breath_frost		( ch );
    }

    return FALSE;
}



bool spec_breath_acid( CHAR_DATA *ch )
{
    return dragon( ch, "acid breath" );
}



bool spec_breath_fire( CHAR_DATA *ch )
{
    return dragon( ch, "fire breath" );
}



bool spec_breath_frost( CHAR_DATA *ch )
{
    return dragon( ch, "frost breath" );
}



bool spec_breath_gas( CHAR_DATA *ch )
{
    if ( ch->position != POS_FIGHTING )
	return FALSE;

    if (MOB_HAS_TRIGGER(ch,MTRIG_PRESPEC) &&
	!mp_prespec_trigger(ch,ch->fighting,NULL))
	return FALSE;
    (*skill_spell_fun(gsn_gas_breath,ch)) ( gsn_gas_breath, ch->level,
					       ch, NULL,TARGET_CHAR);
    return TRUE;
}



bool spec_breath_lightning( CHAR_DATA *ch )
{
    return dragon( ch, "lightning breath" );
}



bool spec_cast_adept( CHAR_DATA *ch )
{
    CHAR_DATA *victim;
    CHAR_DATA *v_next;
    int speclevel=11;	/* Special spells maxmimum level */
    int normlevel=17;	/* Normal spells maximum level */
    int i;

    if ( !IS_AWAKE(ch) )
	return FALSE;

    for ( victim = ch->in_room->people; victim != NULL; victim = v_next )
    {
	v_next = victim->next_in_room;
	if ( victim != ch && can_see( ch, victim ) && number_bits( 1 ) == 0
	     && !IS_NPC(victim) && victim->level < normlevel)
	    break;
    }

    if ( victim == NULL )
	return FALSE;

    i=number_bits( 4 );
    if (i>13) return FALSE;

    if (MOB_HAS_TRIGGER(ch,MTRIG_PRESPEC) &&
	!mp_prespec_trigger(ch,victim,NULL))
	return FALSE;
    switch ( i ) {
    case 0:
	say_spell(ch,gsn_armor);
	spell_armor(gsn_armor,ch->level,ch,victim,TARGET_CHAR);
	break;

    case 1:
	say_spell(ch,gsn_bless);
	spell_bless(gsn_bless, ch->level,ch,victim,TARGET_CHAR);
	break;

    case 2:
	say_spell(ch,gsn_cure_blindness);
	spell_cure_blindness(gsn_cure_blindness,ch->level,
			     ch, victim,TARGET_CHAR);
	break;

    case 3:
	say_spell(ch,gsn_cure_light);
	spell_cure_light(gsn_cure_light,ch->level, ch, victim,TARGET_CHAR);
	break;

    case 4:
	say_spell(ch,gsn_cure_poison);
	spell_cure_poison(gsn_cure_poison, ch->level, ch, victim,TARGET_CHAR);
	break;

    case 5:
	say_spell(ch,gsn_refresh);
	spell_refresh( gsn_refresh,ch->level,ch,victim,TARGET_CHAR);
	break;

    case 6:
	say_spell(ch,gsn_cure_disease);
	spell_cure_disease(gsn_cure_disease,ch->level,ch,victim,TARGET_CHAR);
	break;

    case 7:
	if(victim->level>speclevel || number_range(0,8)) return FALSE;
//	say_spell(ch,sn); this one is just too good
	act("$n utters the words '"C_SAY"sandwich with peanutbutter{x'.",
	    ch,NULL,NULL,TO_ROOM);
	spell_stone_skin(gsn_stone_skin,ch->level,ch,victim,TARGET_CHAR);
	break;

    case 8:
	if(victim->level>speclevel || number_range(0,8)) return FALSE;
	say_spell(ch,gsn_shield);
	spell_shield(gsn_shield,ch->level,ch,victim,TARGET_CHAR);
	break;

    case 9:
	if(victim->level>speclevel || number_range(0,8)) return FALSE;
	say_spell(ch,gsn_sanctuary);
	spell_sanctuary(gsn_sanctuary,ch->level,ch,victim,TARGET_CHAR);
	break;

    case 10:
	if(victim->level>speclevel || number_range(0,8)) return FALSE;
//	say_spell(ch,sn); this one also
	act("$n utters the words '"C_SAY"wadisidiquiq{x'.",
	    ch,NULL,NULL,TO_ROOM);
	spell_haste(gsn_haste,ch->level,ch,victim,TARGET_CHAR);
	break;

    case 11:
	if(victim->level>speclevel || number_range(0,8)) return FALSE;
	say_spell(ch,gsn_protection_evil);
	spell_protection_evil(gsn_protection_evil,
			      ch->level,ch,victim,TARGET_CHAR);
	break;

    case 12:
	if(victim->level>speclevel || number_range(0,8)) return FALSE;
	say_spell(ch,gsn_protection_good);
	spell_protection_good(gsn_protection_good,ch->level,
			      ch,victim,TARGET_CHAR);
	break;

    case 13:
	if(victim->level>speclevel || number_range(0,8)) return FALSE;
//	say_spell(ch,sn); another one
	act("$n utters the words '"C_SAY"fatal lives{x'.",ch,NULL,NULL,TO_ROOM);
	spell_heal(gsn_heal,ch->level,ch,victim,TARGET_CHAR);
	break;
    }

    if (MOB_HAS_TRIGGER(ch,MTRIG_SPEC))
	mp_spec_trigger(ch,victim,NULL);
    return TRUE;
}


/*
 * This function is to prevent the skill lookup, mana checking etc for
 * the functions spec_cast_(cleric|judge|mage|undead)
 */
bool do_mob_cast(CHAR_DATA *ch, CHAR_DATA *victim, char *spell) {
    int sn,mana;

    if ( ( sn = skill_lookup( spell ) ) < 0 )
	return FALSE;

    if (ch->level + 2 == skill_level(sn,ch))
        mana = 50;
    else
        mana = UMAX(
            skill_min_mana(sn,ch),
            100 / ( 2 + ch->level - skill_level(sn,ch)) );

    if ( ch->mana < mana )
    {
        send_to_char( "You don't have enough mana.\n\r", ch );
        return FALSE;
    }
    ch->mana -= mana;

    if (MOB_HAS_TRIGGER(ch,MTRIG_PRESPEC) &&
	!mp_prespec_trigger(ch,victim,NULL))
	return FALSE;
    (*skill_spell_fun(sn,ch)) ( sn, ch->level, ch, victim,TARGET_CHAR);
    if (MOB_HAS_TRIGGER(ch,MTRIG_SPEC))
	mp_spec_trigger(ch,victim,NULL);
    return TRUE;
}


bool spec_cast_cleric( CHAR_DATA *ch )
{
    CHAR_DATA *victim;
    CHAR_DATA *v_next;
    char *spell;

    if ( ch->position != POS_FIGHTING || ch->level<6) /* IMPORTANT */
	return FALSE;

    for ( victim = ch->in_room->people; victim != NULL; victim = v_next )
    {
	v_next = victim->next_in_room;
	if ( victim->fighting == ch && number_bits( 2 ) == 0 )
	    break;
    }

    if ( victim == NULL )
	return FALSE;

    for ( ;; )
    {
	int min_level;

	switch ( number_bits( 4 ) )
	{
	case  0: min_level =  6; spell = "blindness";      break;
	case  1: min_level =  7; spell = "cause serious";  break;
	case  2: min_level =  8; spell = "earthquake";     break;
	case  3: min_level = 13; spell = "cause critical"; break;
	case  4: min_level = 15; spell = "dispel evil";    break;
	case  5: min_level = 18; spell = "curse";          break;
	case  6: min_level = 19; spell = "change sex";     break;
	case  7: min_level = 20; spell = "flamestrike";    break;
	case  8:
	case  9:
	case 10: min_level = 23; spell = "harm";           break;
	case 11: min_level = 17; spell = "plague";	   break;
	case 12: min_level = 16; spell = "hallucination";  break;
	default: min_level = 24; spell = "dispel magic";   break;
	}

	if ( ch->level >= min_level )
	    break;
    }

    return do_mob_cast(ch,victim,spell);
}

bool spec_cast_judge( CHAR_DATA *ch )
{
    CHAR_DATA *victim;
    CHAR_DATA *v_next;
    char *spell;

    if ( ch->position != POS_FIGHTING )
        return FALSE;

    for ( victim = ch->in_room->people; victim != NULL; victim = v_next )
    {
        v_next = victim->next_in_room;
        if ( victim->fighting == ch && number_bits( 2 ) == 0 )
            break;
    }

    if ( victim == NULL )
        return FALSE;

    spell = "high explosive";

    return do_mob_cast(ch,victim,spell);
}



bool spec_cast_mage( CHAR_DATA *ch )
{
    CHAR_DATA *victim;
    CHAR_DATA *v_next;
    char *spell;

    if ( ch->position != POS_FIGHTING || ch->level<4) /* IMPORTANT */
	return FALSE;

    for ( victim = ch->in_room->people; victim != NULL; victim = v_next )
    {
	v_next = victim->next_in_room;
	if ( victim->fighting == ch && number_bits( 2 ) == 0 )
	    break;
    }

    if ( victim == NULL )
	return FALSE;

    for ( ;; )
    {
	int min_level;

	switch ( number_bits( 4 ) )
	{
	case  0: min_level = 12; spell = "blindness";      break;
	case  1: min_level =  4; spell = "chill touch";    break;
	case  2: min_level = 11; spell = "weaken";         break;
	case  3: min_level = 13; spell = "teleport";       break;
	case  4: min_level = 16; spell = "colour spray";   break;
	case  5: min_level = 18; spell = "change sex";     break;
	case  6: min_level = 19; spell = "energy drain";   break;
	case  7:
	case  8:
	case  9: min_level = 22; spell = "fireball";       break;
	case 10: min_level = 23; spell = "plague";	   break;
	case 11: min_level = 16; spell = "hallucination";  break;
	default: min_level = 28; spell = "acid blast";     break;
	}

	if ( ch->level >= min_level )
	    break;
    }

    return do_mob_cast(ch,victim,spell);
}



bool spec_cast_undead( CHAR_DATA *ch )
{
    CHAR_DATA *victim;
    CHAR_DATA *v_next;
    char *spell;

    if ( ch->position != POS_FIGHTING )
	return FALSE;

    for ( victim = ch->in_room->people; victim != NULL; victim = v_next )
    {
	v_next = victim->next_in_room;
	if ( victim->fighting == ch && number_bits( 2 ) == 0 )
	    break;
    }

    if ( victim == NULL )
	return FALSE;

    for ( ;; )
    {
	int min_level;

	switch ( number_bits( 4 ) )
	{
	case  0: min_level =  0; spell = "curse";          break;
	case  1: min_level =  3; spell = "weaken";         break;
	case  2: min_level =  6; spell = "chill touch";    break;
	case  3: min_level =  9; spell = "blindness";      break;
	case  4: min_level = 12; spell = "poison";         break;
	case  5: min_level = 15; spell = "energy drain";   break;
	case  6: min_level = 18; spell = "harm";           break;
	case  7: min_level = 21; spell = "teleport";       break;
	case  8: min_level = 20; spell = "plague";	   break;
	case  9: min_level = 20; spell = "hunger";	   break;
	case 10: min_level = 20; spell = "thirst";	   break;
	default: min_level = 18; spell = "harm";           break;
	}

	if ( ch->level >= min_level )
	    break;
    }

    return do_mob_cast(ch,victim,spell);
}


bool spec_executioner( CHAR_DATA *ch )
{
    char buf[MAX_STRING_LENGTH];
    CHAR_DATA *victim;
    CHAR_DATA *v_next;
    char *crime;
    extern int top_swearreply;

    if ( !IS_AWAKE(ch) || ch->fighting != NULL )
	return FALSE;

    crime = "";
    for ( victim = ch->in_room->people; victim != NULL; victim = v_next )
    {
	v_next = victim->next_in_room;

	if ( !IS_NPC(victim) && STR_IS_SET(victim->strbit_act, PLR_KILLER)
	&&   can_see(ch,victim))
	    { crime = "KILLER"; break; }

	if ( !IS_NPC(victim) && STR_IS_SET(victim->strbit_act, PLR_THIEF)
	&&   can_see(ch,victim))
	    { crime = "THIEF"; break; }

	if ( !IS_NPC(victim) && victim->pcdata->swearing>=top_swearreply
	&&   can_see(ch,victim))
	    { crime = NULL; break; }
    }

    if ( victim == NULL )
	return FALSE;

    if(crime)
    	sprintf( buf, "%s is a %s!  PROTECT THE INNOCENT!  MORE BLOOOOD!!!",
	    victim->name, crime );
    else
        sprintf( buf, "%s is a word abuser! MORE BLOOOOD!!!!",
	    victim->name);

    if (MOB_HAS_TRIGGER(ch,MTRIG_PRESPEC) &&
	!mp_prespec_trigger(ch,victim,NULL))
	return FALSE;
    STR_REMOVE_BIT(ch->strbit_comm,COMM_NOSHOUT);
    do_function(ch,&do_yell,buf );
    multi_hit( ch, victim, TYPE_UNDEFINED );
    if (MOB_HAS_TRIGGER(ch,MTRIG_SPEC))
	mp_spec_trigger(ch,victim,NULL);
    return TRUE;
}



bool spec_fido( CHAR_DATA *ch )
{
    OBJ_DATA *corpse;
    OBJ_DATA *c_next;
    OBJ_DATA *obj;
    OBJ_DATA *obj_next;

    if ( !IS_AWAKE(ch) )
	return FALSE;

    for ( corpse = ch->in_room->contents; corpse != NULL; corpse = c_next )
    {
	c_next = corpse->next_content;
	if ( corpse->item_type != ITEM_CORPSE_NPC )
	    continue;

	act( "$n savagely devours a corpse.", ch, NULL, NULL, TO_ROOM );
	for ( obj = corpse->contains; obj; obj = obj_next )
	{
	    obj_next = obj->next_content;
	    obj_from_obj( obj );
	    obj_to_room( obj, ch->in_room );
	}
	if (MOB_HAS_TRIGGER(ch,MTRIG_PRESPEC) &&
	    !mp_prespec_trigger(ch,NULL,corpse))
	    return FALSE;
	extract_obj( corpse );
	if (MOB_HAS_TRIGGER(ch,MTRIG_SPEC))
	    mp_spec_trigger(ch,NULL,NULL);
	return TRUE;
    }

    return FALSE;
}



bool spec_guard( CHAR_DATA *ch )
{
    char buf[MAX_STRING_LENGTH];
    CHAR_DATA *victim;
    CHAR_DATA *v_next;
    CHAR_DATA *ech;
    char *crime;
    int max_evil;
    extern int top_swearreply;

    if ( !IS_AWAKE(ch) || ch->fighting != NULL )
	return FALSE;

    max_evil = 300;
    ech      = NULL;
    crime    = "";

    for ( victim = ch->in_room->people; victim != NULL; victim = v_next )
    {
	v_next = victim->next_in_room;

	if ( !IS_NPC(victim) && STR_IS_SET(victim->strbit_act, PLR_KILLER)
	&&   can_see(ch,victim))
	    { crime = "KILLER"; break; }

	if ( !IS_NPC(victim) && STR_IS_SET(victim->strbit_act, PLR_THIEF)
	&&   can_see(ch,victim))
	    { crime = "THIEF"; break; }

	if ( !IS_NPC(victim) && victim->pcdata->swearing>=top_swearreply
	&&   can_see(ch,victim))
	    { crime = NULL; break; }

	if ( victim->fighting != NULL
	&&   victim->fighting != ch
	&&   victim->alignment < max_evil )
	{
	    max_evil = victim->alignment;
	    ech      = victim;
	}
    }

    if ( victim != NULL )
    {
	if(crime)
	    sprintf( buf, "%s is a %s!  PROTECT THE INNOCENT!!  BANZAI!!",
	    	victim->name, crime );
	else
	    sprintf( buf, "%s is a word abuser! You will be killed for that!",
		victim->name);

	if (MOB_HAS_TRIGGER(ch,MTRIG_PRESPEC) &&
	    !mp_prespec_trigger(ch,victim,NULL))
	    return FALSE;
 	STR_REMOVE_BIT(ch->strbit_comm,COMM_NOSHOUT);
	do_function(ch,&do_yell,buf );
	multi_hit( ch, victim, TYPE_UNDEFINED );
	if (MOB_HAS_TRIGGER(ch,MTRIG_SPEC))
	    mp_spec_trigger(ch,victim,NULL);
	return TRUE;
    }

    if ( ech != NULL )
    {
	if (MOB_HAS_TRIGGER(ch,MTRIG_PRESPEC) &&
	    !mp_prespec_trigger(ch,ech,NULL))
	    return FALSE;
	act( "$n screams 'PROTECT THE INNOCENT!!  BANZAI!!",
	    ch, NULL, NULL, TO_ROOM );
	multi_hit( ch, ech, TYPE_UNDEFINED );
	if (MOB_HAS_TRIGGER(ch,MTRIG_SPEC))
	    mp_spec_trigger(ch,ech,NULL);
	return TRUE;
    }

    return FALSE;
}



bool spec_janitor( CHAR_DATA *ch )
{
    OBJ_DATA *trash;
    OBJ_DATA *trash_next;
    CHAR_DATA *gch;
    bool in_use;

    if ( !IS_AWAKE(ch) )
	return FALSE;

    for ( trash = ch->in_room->contents; trash != NULL; trash = trash_next )
    {
	trash_next = trash->next_content;
	if ( !IS_SET( trash->wear_flags, ITEM_TAKE ) || !can_loot(ch,trash))
	    continue;
	if ( trash->item_type == ITEM_DRINK_CON
	||   trash->item_type == ITEM_TRASH
	||   trash->cost < 10 )
	{
	    /* Make sure they're not picking up objects people are using */
	    in_use=FALSE;
	    if (trash->in_room != NULL) {
		for (gch = trash->in_room->people; gch != NULL; gch = gch->next_in_room) {
		    if (gch->on == trash) {
			in_use=TRUE;
			break;
		    }
		}
	    }
	    if (in_use) continue;

	    if (MOB_HAS_TRIGGER(ch,MTRIG_PRESPEC) &&
		!mp_prespec_trigger(ch,NULL,trash))
		return FALSE;

	    act( "$n picks up some trash.", ch, NULL, NULL, TO_ROOM );
	    obj_from_room( trash );
	    obj_to_char( trash, ch );
	    if (MOB_HAS_TRIGGER(ch,MTRIG_SPEC))
		mp_spec_trigger(ch,NULL,trash);
	    if (trash->item_type == ITEM_TRASH)
		extract_obj(trash);
	    return TRUE;
	}
    }

    return FALSE;
}



bool spec_mayor( CHAR_DATA *ch )
{
    static const char open_path[] =
	"W3a3003b33000c111d0d111Oe333333Oe22c222112212111a1S.";

    static const char close_path[] =
	"W3a3003b33000c111d0d111CE333333CE22c222112212111a1S.";

    static const char *path;
    static int pos;
    static bool move;

    if ( !move )
    {
	if ( time_info.hour ==  6 )
	{
	    path = open_path;
	    move = TRUE;
	    pos  = 0;
	}

	if ( time_info.hour == 20 )
	{
	    path = close_path;
	    move = TRUE;
	    pos  = 0;
	}
    }

    if ( ch->fighting != NULL )
	return spec_cast_mage( ch );
    if ( !move || ch->position < POS_SLEEPING )
	return FALSE;

    switch ( path[pos] )
    {
    case '0':
    case '1':
    case '2':
    case '3':
	move_char( ch, path[pos] - '0', FALSE, FALSE );
	break;

    case 'W':
	ch->position = POS_STANDING;
	act( "$n awakens and groans loudly.", ch, NULL, NULL, TO_ROOM );
	break;

    case 'S':
	ch->position = POS_SLEEPING;
	act( "$n lies down and falls asleep.", ch, NULL, NULL, TO_ROOM );
	break;

    case 'a':
	act( "$n says '"C_SAY"Hello Honey!{x'", ch, NULL, NULL, TO_ROOM );
	break;

    case 'b':
	act( "$n says '"C_SAY"What a view!  I must do something about that dump!{x'",
	    ch, NULL, NULL, TO_ROOM );
	break;

    case 'c':
	act( "$n says '"C_SAY"Vandals!  Youngsters have no respect for anything!{x'",
	    ch, NULL, NULL, TO_ROOM );
	break;

    case 'd':
	act( "$n says '"C_SAY"Good day, citizens!{x'", ch, NULL, NULL, TO_ROOM );
	break;

    case 'e':
	act( "$n says '"C_SAY"I hereby declare the city of Midgaard open!{x'",
	    ch, NULL, NULL, TO_ROOM );
	break;

    case 'E':
	act( "$n says '"C_SAY"I hereby declare the city of Midgaard closed!{x'",
	    ch, NULL, NULL, TO_ROOM );
	break;

    case 'O':
/*	do_function(ch,&do_unlock, "gate" ); */
	do_function(ch,&do_open, "gate" );
	break;

    case 'C':
	do_function(ch,&do_close, "gate" );
/*	do_function(ch,&do_lock, "gate" ); */
	break;

    case '.' :
	move = FALSE;
	break;
    }

    pos++;
    return FALSE;
}



bool spec_poison( CHAR_DATA *ch )
{
    CHAR_DATA *victim;

    if ( ch->position != POS_FIGHTING
    || ( victim = ch->fighting ) == NULL
    ||   number_percent( ) > 2 * ch->level )
	return FALSE;

    if (MOB_HAS_TRIGGER(ch,MTRIG_PRESPEC) &&
	!mp_prespec_trigger(ch,victim,NULL))
	return FALSE;
    act( "You bite $N!",  ch, NULL, victim, TO_CHAR    );
    act( "$n bites $N!",  ch, NULL, victim, TO_NOTVICT );
    act( "$n bites you!", ch, NULL, victim, TO_VICT    );
    spell_poison( gsn_poison, ch->level, ch, victim,TARGET_CHAR);
    if (MOB_HAS_TRIGGER(ch,MTRIG_SPEC))
	mp_spec_trigger(ch,victim,NULL);
    return TRUE;
}



bool spec_thief( CHAR_DATA *ch )
{
    CHAR_DATA *victim;
    CHAR_DATA *v_next;
    long gold,silver;

    if ( ch->position != POS_STANDING )
	return FALSE;

    for ( victim = ch->in_room->people; victim != NULL; victim = v_next )
    {
	v_next = victim->next_in_room;

	if ( IS_NPC(victim)
	||   victim->level >= LEVEL_IMMORTAL
	||   number_bits( 5 ) != 0
	||   !can_see(ch,victim))
	    continue;

	if ( IS_AWAKE(victim) && number_range( 0, ch->level ) == 0 )
	{
	    act( "You discover $n's hands in your wallet!",
		ch, NULL, victim, TO_VICT );
	    act( "$N discovers $n's hands in $S wallet!",
		ch, NULL, victim, TO_NOTVICT );
	    return TRUE;
	}
	else
	{
	    if (MOB_HAS_TRIGGER(ch,MTRIG_PRESPEC) &&
		!mp_prespec_trigger(ch,victim,NULL))
		return FALSE;
	    gold = victim->gold * UMIN(number_range(1,20),ch->level / 2) / 100;
	    gold = UMIN(gold, ch->level * ch->level * 10 );
	    ch->gold     += gold;
	    victim->gold -= gold;
	    silver = victim->silver * UMIN(number_range(1,20),ch->level/2)/100;
	    silver = UMIN(silver,ch->level*ch->level * 25);
	    ch->silver	+= silver;
	    victim->silver -= silver;
	    if (MOB_HAS_TRIGGER(ch,MTRIG_SPEC))
		mp_spec_trigger(ch,victim,NULL);
	    return TRUE;
	}
    }

    return FALSE;
}

bool spec_assassin( CHAR_DATA *ch )
{
    CHAR_DATA *victim;
    char *message;

    if ( ch->fighting != NULL ) return FALSE;

    for ( victim = ch->in_room->people; victim != NULL; victim = victim->next_in_room )
    {
	/* this should kill mobs as well as players */
	if (victim->class != 2)  /* thieves */
	    break;
    }

    if ( victim == NULL || victim == ch || IS_IMMORTAL(victim) )
        return FALSE;
    if ( victim->level > ch->level + 7 || IS_NPC(victim))
        return FALSE;

    switch (number_range(0,6))
    {
        default:  message = NULL;       break;
        case 0:  message =  "$n screams '" C_SHOUT "Meet The Grim Reaper{x'";
                 break;
        case 1:  message =  "$n yells '" C_SHOUT "Death to is the true end...{x'";
                 break;
        case 2:  message = "$n shouts '" C_SHOUT "Time to Die....{x'";
                 break;
        case 3:  message = "$n rasps '" C_SAY "Cabrone....{x'";
                 break;
        case 4:  message = "$n drools '" C_SAY "Welcome to your fate....{x'";
                 break;
        case 5:  message = "$n screams '" C_SHOUT "The Executioner has arrived.....{x'";
                 break;
        case 6:  message = "$n says '" C_SAY "Ever dance with the devil....{x'";
		 break;
    }
    if (MOB_HAS_TRIGGER(ch,MTRIG_PRESPEC) &&
	!mp_prespec_trigger(ch,victim,NULL))
	return FALSE;
    if (message != NULL)
	act(message,ch,NULL,victim,TO_ALL);
    multi_hit( ch, victim, gsn_backstab );
    if (MOB_HAS_TRIGGER(ch,MTRIG_SPEC))
        mp_spec_trigger(ch,victim,NULL);
    return TRUE;
}

bool spec_manadrain(CHAR_DATA *ch) {
    CHAR_DATA *victim;

    if ( ch->fighting == NULL ) return FALSE;
    victim=ch->fighting;

    if (MOB_HAS_TRIGGER(ch,MTRIG_PRESPEC) &&
	!mp_prespec_trigger(ch,victim,NULL))
	return FALSE;

    if (victim != ch)
	ch->alignment = UMAX(-1000, ch->alignment - 50);

    if ( saves_spell( ch->level, victim, DAM_NEGATIVE) ) {
	send_to_char("You feel a momentary chill.\n\r",victim);
	return FALSE;
    }

    victim->mana -= ch->level;   /* on request of Harlequin */
    if (victim->mana<0) victim->mana=0;

    send_to_char("You feel your mental strength slipping away!\n\r",victim);
    act("$N look a lot less vivid.",ch,NULL,victim,TO_NOTVICT);
    send_to_char("Wow.... what a rush!\n\r",ch);

    if (MOB_HAS_TRIGGER(ch,MTRIG_SPEC))
        mp_spec_trigger(ch,victim,NULL);
    return TRUE;
}

/* Just a method to see the mob is a banker. */
bool spec_banker( CHAR_DATA *ch )
{
    return FALSE;
}

static int pinkbrainstat=3;
static int numpinkyquotes=0;
static char **pinkyquotes=NULL;
static bool triedit=FALSE;
static int pinkyquote;

void read_pinkyquotes(void)
{
    char buf[MAX_STRING_LENGTH],*s;
    FILE *f;

    if(!triedit)
    {
    	triedit=TRUE;

    	if((f=fopen(mud_data.pinky_file,"r"))==NULL) {
	    logf("[0] read_pinkyquotes(): fopen(%s): %s",
		mud_data.pinky_file,ERROR);
	    bugf("PINKY: Couldn't open pinky-quotes.");
	    return;
	}
    	while(fgets(buf,MAX_STRING_LENGTH,f))
    	{
    	    if(*buf=='$') break;
    	    numpinkyquotes++;
    	}
    	rewind(f);
    	pinkyquotes=calloc(numpinkyquotes,sizeof(char *));
    	numpinkyquotes=0;
    	while(fgets(buf,MAX_STRING_LENGTH,f))
    	{
    	    if(*buf=='$') break;
    	    if((s=strchr(buf,'\n'))) *s='\0';
    	    pinkyquotes[numpinkyquotes++]=strdup(buf);
    	}
    	fclose(f);
    }
}

bool spec_pinky( CHAR_DATA *ch )
{
    CHAR_DATA *vch,*brain;

    if(!IS_AWAKE(ch))
    {
    	switch(number_bits(3)) {
    	  case 0:
    		do_function(ch,&do_emote,"snores loudly.");
    		break;
    	};

    	return TRUE;
    }

    if (ch->position<POS_STANDING) return FALSE;

    if (ch->master && IS_NPC(ch->master) && ch->master->spec_fun==spec_brain
    && ch->in_room==ch->master->in_room ) {
    	/* We are following brain, he's in this room.. Okay. */
    }
    else
    {
    	/* search for brain and follow him */
	for (vch = ch->in_room->people;  vch != NULL;  vch = vch->next_in_room)
	{
	    if(IS_NPC(vch) && vch->spec_fun==spec_brain)
	    {
	        ch->master=vch;
	        do_function(ch,&do_say,
		    "I have found you Brain, I won't leave you.");
	        break;
	    }
	}

	if(vch==NULL)
	{
	    if(number_bits(3)==0)
	    {
	    	do_function(ch,&do_say,"Brain! Where are you!");
	    	do_function(ch,&do_emote,"bursts out into tears.");
	    }
	    return TRUE;
	}
    }


    brain=ch->master;
    if(!brain) return TRUE;

    if(!IS_AWAKE(brain))
    {
    	switch(number_bits(3)) {
    	  case 0:
    		do_function(brain,&do_emote,"turns over in his sleeps.");
    		break;
    	  case 1:
    	  	do_function(brain,&do_emote,
		    "mumbles 'You will all follow The Brain.'");
    	  	break;
    	};

    	return TRUE;
    }

    if(brain->position>POS_STANDING) return FALSE;

    if(!triedit) read_pinkyquotes();

    if(pinkbrainstat) pinkbrainstat++;

    /* Here comes the normal stuff */
    if(time_info.hour==22 && pinkbrainstat==0)
    {
    	do_function(ch,&do_say,"What are we gonna do tonight Brain?");
    	pinkbrainstat=1;
    }
    else if(pinkbrainstat==0)
    {
    	/* Do nothing!!! */
    }
    else if(pinkbrainstat==2)
    {
    	do_function(brain,&do_say,"The same thing we do every night, Pinky...");
    	do_function(brain,&do_say,"Try to take over the World!");
    }
    else if(pinkbrainstat%15==0)
    {
    	do_function(brain,&do_say,
	    "Pinky? Are you pondering what I'm pondering?");
	pinkyquote=number_range(0,numpinkyquotes);
    }
    else if (pinkyquote==numpinkyquotes) {
	switch (pinkbrainstat%15) {
	    case 1 : do_function(ch,&do_say,   "Whoof, oh, I'd have to say the odds are slim, Brain."); break;
	    case 2 : do_function(brain,&do_say,"True."); break;
	    case 3 : do_function(ch,&do_say,   "I mean, really, when have I ever been pondering what you've been pondering?"); break;
	    case 4 : do_function(brain,&do_say,"To my knowledge, never."); break;
	    case 5 : do_function(ch,&do_say,   "Exactly. So, what are the chances that this time, I'm pondering what you're pondering?"); break;
	    case 6 : do_function(brain,&do_say,"Next to nil."); break;
	    case 7 : do_function(ch,&do_say,   "Well, that's exactly what I'm thinking, too."); break;
	    case 8 : do_function(brain,&do_say,"Therefore, you are pondering what I'm pondering."); break;
	    case 9 : do_function(ch,&do_say,   "Poit, I guess I am!"); break;
	    default : pinkyquote=-1;
	}
    } else {
	if(pinkbrainstat%15==1) {
	    if(numpinkyquotes==0)
		do_function(ch,&do_say,"I have no idea Brain.");
	    else
		do_function(ch,&do_say,
		    pinkyquotes[pinkyquote]);
	}
	pinkyquote=-1;
    }

    if(time_info.hour==21 && pinkyquote==-1) pinkbrainstat=0;

    switch(number_range(0,60)) {
      case 0: do_function(ch,&do_say,"Narf");break;
      case 1: do_function(ch,&do_say,"Zoink");break;
      case 2: do_function(ch,&do_say,"Poit");break;
    }

    return TRUE;
}

bool spec_brain( CHAR_DATA *ch )
{
    return FALSE;
}

bool spec_questmaster( CHAR_DATA *ch ) {
    return FALSE;
}

bool spec_stringmaster( CHAR_DATA *ch ) {
    return FALSE;
}

TCLPROG_CODE *has_special_trigger(CHAR_DATA *mob) {
    int vnum=0;

    if(GetCharProperty(mob,PROPERTY_INT,"special-mprog",&vnum))
    {
      TCLPROG_CODE *pCode=get_mprog_index(vnum);
      if(pCode) return pCode;
    }

    bugf(
	"spec_XXX call without special trigger for %s [vnum %d] (no specialmprog property or wrong vnum)",
	mob->short_descr,mob->pIndexData->vnum);

    return NULL;
}

int char_count(char *s,char c) {
    int i=0;

    for (;s[0];s++)
	if (s[0]==c)
	    i++;
    return i;
}

char *char_offset(char *s,char c,int i) {

    if (i<=0)
	return s;
    for (;s[0];s++)
	if (s[0]==c) {
	    if (--i==0)
		return s;
	}
    return NULL;
}

bool spec_random_text( CHAR_DATA *ch ) {
    TCLPROG_CODE *prg;
    int i,r;
    char *p,*q;
    char s[MAX_STRING_LENGTH];
    int chance=100;

    if ((prg=has_special_trigger(ch))==NULL)
	return FALSE;

    GetCharProperty(ch,PROPERTY_INT,"special-chance",&chance);

    if (chance>number_percent()) {
	if (MOB_HAS_TRIGGER(ch,MTRIG_PRESPEC) &&
	    !mp_prespec_trigger(ch,NULL,NULL))
	    return FALSE;
	i=char_count(prg->code,'\r');
	r=number_range(1,i);
	if ((p=char_offset(prg->code,'\r',r-1))==NULL)
	    return TRUE;
	if (p[0]=='\r')
	    p++;
	if (p[0]==0)
	    return TRUE;
	q=strchr(p,'\n');
	if (q) {
	    strncpy(s,p,q-p);
	    s[q-p]=0;
	} else {
	    strcpy(s,p);
	}
	do_function(ch,&do_say,s);
	if (MOB_HAS_TRIGGER(ch,MTRIG_SPEC))
	    mp_spec_trigger(ch,NULL,NULL);
    }
    return TRUE;
}

bool spec_random_command( CHAR_DATA *ch ) {
    TCLPROG_CODE *prg;
    int i,r;
    char *p,*q;
    char s[MAX_STRING_LENGTH];
    int chance=100;

    if ((prg=has_special_trigger(ch))==NULL)
	return FALSE;

    GetCharProperty(ch,PROPERTY_INT,"special-chance",&chance);

    if (chance>number_percent()) {
	if (MOB_HAS_TRIGGER(ch,MTRIG_PRESPEC) &&
	    !mp_prespec_trigger(ch,NULL,NULL))
	    return FALSE;
	i=char_count(prg->code,'\r');
	r=number_range(1,i);
	if ((p=char_offset(prg->code,'\r',r-1))==NULL)
	    return TRUE;
	if (p[0]=='\r')
	    p++;
	if (p[0]==0)
	    return TRUE;
	q=strchr(p,'\n');
	if (q) {
	    strncpy(s,p,q-p);
	    s[q-p]=0;
	} else {
	    strcpy(s,p);
	}
	interpret(ch,s);
	if (MOB_HAS_TRIGGER(ch,MTRIG_SPEC))
	    mp_spec_trigger(ch,NULL,NULL);
    }
    return TRUE;
}

bool spec_command_sequence( CHAR_DATA *ch ) {
    TCLPROG_CODE *prg;
    int r;
    char *p,*q;
    char s[MAX_STRING_LENGTH];
    int chance=100;


    if ((prg=has_special_trigger(ch))==NULL)
	return FALSE;

    GetCharProperty(ch,PROPERTY_INT,"special-chance",&chance);

    if (chance>number_percent()) {
	if (MOB_HAS_TRIGGER(ch,MTRIG_PRESPEC) &&
	    !mp_prespec_trigger(ch,NULL,NULL))
	    return FALSE;
    char_count(prg->code,'\r');
	if (!GetCharProperty(ch,PROPERTY_INT,"special_step",&r))
	    r=1;
	else
	    r++;
	SetCharProperty(ch,PROPERTY_INT,"special_step",&r);

	if ((p=char_offset(prg->code,'\r',r-1))==NULL) {
	    r=0;
	    SetCharProperty(ch,PROPERTY_INT,"special_step",&r);
	    return TRUE;
	}
	if (p[0]=='\r')
	    p++;
	if (p[0]==0)
	    return TRUE;
	q=strchr(p,'\n');
	if (q) {
	    strncpy(s,p,q-p);
	    s[q-p]=0;
	} else {
	    strcpy(s,p);
	}
	interpret(ch,s);

	if (MOB_HAS_TRIGGER(ch,MTRIG_SPEC))
	    mp_spec_trigger(ch,NULL,NULL);
    }
    return TRUE;
}

bool spec_puff(CHAR_DATA *ch) {

    if (number_percent()>10)
	return FALSE;

    switch (number_range(0,11)) {
	case 0:
	    do_function(ch,&do_say,"My god! It's full of stars!");
	    break;
	case 1:
	    do_function(ch,&do_say,"How'd all those fish get up here?");
	    break;
	case 2:
	    do_function(ch,&do_say,"I'm a very female dragon.");
	    break;
	case 3:
	    do_function(ch,&do_say,"I've got a peaceful, easy feeling.");
	    break;
	case 4:
	    do_function(ch,&do_say,"I....I have become....comfortably numb!");
	    break;
	case 5:
	    do_function(ch,&do_say,"Did you know that I'm written in C?");
	    break;
	case 6:
	    do_function(ch,&do_say,"The colors, the colors!");
	    break;
	case 7:
	    do_function(ch,&do_say,"Into the distance, black on black....");
	    break;
	case 8:
	    act("For a moment, $n flickers and phases.",ch,NULL,NULL,TO_ROOM);
	    act("For a moment, you flicker and phase.",ch,NULL,NULL,TO_CHAR);
	    break;
	case 9:
	    do_function(ch,&do_say,"Tongue-tied and twisted, just an earthbound misfit, ...");
	    break;
    }
    return TRUE;
}


