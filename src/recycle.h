/* $Id: recycle.h,v 1.34 2006/08/25 09:43:21 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,	   *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *									   *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael	   *
 *  Chastain, Michael Quan, and Mitchell Tse.				   *
 *									   *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc	   *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.						   *
 *									   *
 *  Much time and thought has gone into this software and you are	   *
 *  benefitting.  We hope that you share your changes too.  What goes	   *
 *  around, comes around.						   *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

#ifndef INCLUDED_RECYCLE_H
#define INCLUDED_RECYCLE_H

// externs
extern char str_empty[1];

// stuff for providing a crash-proof buffer
#define MAX_BUF		16384
#define MAX_BUF_LIST 	10
#define BASE_BUF 	1024

// valid states
#define BUFFER_SAFE	0
#define BUFFER_OVERFLOW	1
#define BUFFER_FREED 	2

// note recycling
NOTE_DATA *	new_note		(void);
void		free_note		(NOTE_DATA *note);
extern int	note_inuse;
extern int	note_allocated;

// noteref recycling
#ifdef HAS_POP3
NOTE_REF *	new_note_ref		(void);
void		free_note_ref		(NOTE_REF *noteref);
extern int	note_ref_inuse;
extern int	note_ref_allocated;
#endif

// notebook recycling
NOTEBK_DATA *	new_notebook		(void);
void		free_notebook		(NOTEBK_DATA *notebook);
extern int	notebook_inuse;
extern int	notebook_allocated;

// ban data recycling
BAN_DATA *	new_ban			(void);
void		free_ban		(BAN_DATA *ban);
extern int	ban_inuse;
extern int	ban_allocated;

// lockout data recycling
LOCKOUT_DATA *	new_lockout		(void);
void		free_lockout		(LOCKOUT_DATA *lockout);
extern int	lockout_inuse;
extern int	lockout_allocated;

// descriptor recycling
DESCRIPTOR_DATA *new_descriptor		(int destype);
void		free_descriptor		(DESCRIPTOR_DATA *d);
extern int	descriptor_inuse;
extern int	descriptor_allocated;

// char gen data recycling
GEN_DATA  *	new_gen_data		(void);
void		free_gen_data		(GEN_DATA * gen);
extern int	gen_data_inuse;
extern int	gen_data_allocated;

// extra descr recycling
EXTRA_DESCR_DATA *new_extra_descr	(void);
void		free_extra_descr	(EXTRA_DESCR_DATA *ed);
extern int	extra_descr_inuse;
extern int	extra_descr_allocated;

// effect recycling
EFFECT_DATA *	new_effect		(void);
void		free_effect		(EFFECT_DATA *af);
extern int	effect_inuse;
extern int	effect_allocated;

// object recycling
OBJ_DATA *	new_obj		(void);
void		free_obj		(OBJ_DATA *obj);
extern int	obj_inuse;
extern int	obj_allocated;

// character recyling
CHAR_DATA *	new_char		(void);
void		free_char		(CHAR_DATA *ch);
extern int	char_inuse;
extern int	char_allocated;

PC_DATA	 *	new_pcdata		(void);
void		free_pcdata		(PC_DATA *pcdata);
extern int	pcdata_inuse;
extern int	pcdata_allocated;

// mob id and memory procedures
long 		get_pc_id		(void);
long		get_mob_id		(void);
long		get_obj_id		(void);
MEM_DATA *	new_mem_data		(void);
void		free_mem_data		(MEM_DATA *memory);
MEM_DATA *	find_memory		(MEM_DATA *memory, long id);
extern int	mem_data_inuse;
extern int	mem_data_allocated;

// Property recycling
PROPERTY_INDEX_TYPE *new_property_index	(void);
void		free_property_index	(PROPERTY_INDEX_TYPE *property_index);
extern int	property_index_inuse;
extern int	property_index_allocated;

PROPERTY *	new_property		(void);
void		free_property		(PROPERTY *property);
extern int	property_inuse;
extern int	property_allocated;

// TCL programming recycling
TCLPROG_CODE *	new_mprog_code_index	(void);
void		free_mprog_code_index	(TCLPROG_CODE *pCode);
TCLPROG_LIST *	new_mprog_list		(void);
void		free_mprog_list		(TCLPROG_LIST *list);
extern int	mprog_code_index_inuse;
extern int	mprog_code_index_allocated;
extern int	mprog_list_inuse;
extern int	mprog_list_allocated;

TCLPROG_CODE *	new_oprog_code_index	(void);
void		free_oprog_code_index	(TCLPROG_CODE *pCode);
TCLPROG_LIST *	new_oprog_list		(void);
void		free_oprog_list		(TCLPROG_LIST *list);
extern int	oprog_code_index_inuse;
extern int	oprog_code_index_allocated;
extern int	oprog_list_inuse;
extern int	oprog_list_allocated;

TCLPROG_CODE *	new_rprog_code_index	(void);
void		free_rprog_code_index	(TCLPROG_CODE *pCode);
TCLPROG_LIST *	new_rprog_list		(void);
void		free_rprog_list		(TCLPROG_LIST *list);
extern int	rprog_code_index_inuse;
extern int	rprog_code_index_allocated;
extern int	rprog_list_inuse;
extern int	rprog_list_allocated;

TCLPROG_CODE *	new_aprog_code_index	(void);
void		free_aprog_code_index	(TCLPROG_CODE *pCode);
TCLPROG_LIST *	new_aprog_list		(void);
void		free_aprog_list		(TCLPROG_LIST *list);
extern int	aprog_code_index_inuse;
extern int	aprog_code_index_allocated;
extern int	aprog_list_inuse;
extern int	aprog_list_allocated;

// clan recycle
CLANMEMBER_TYPE *new_clanmember		(void);
void		free_clanmember		(CLANMEMBER_TYPE *pMember);
extern int	clanmember_inuse;
extern int	clanmember_allocated;

RANK_INFO *	new_clanrank		(void);
void		free_clanrank		(RANK_INFO *pRank);
extern int	clanrank_inuse;
extern int	clanrank_allocated;

CLAN_INFO *	new_clan		(void);
void		free_clan		(CLAN_INFO *pClan);
extern int	clan_inuse;
extern int	clan_allocated;

// mob memory recycling
MEM_DATA *	new_mobmem		(void);
void		free_mobmem		(MEM_DATA *pMem);
extern int	mobmem_inuse;
extern int	mobmem_allocated;

// filesystem recycling
FS_ENTRY *	new_fsentry		(void);
void		free_fsentry		(FS_ENTRY *fsentry);
extern int	fsentry_inuse;
extern int	fsentry_allocated;

// event recycling
EVENT_DATA *	new_event		(void);
void		free_event		(EVENT_DATA *event);
EVENT_DATA *	create_event		(int delay,CHAR_DATA *ch,
					 CHAR_DATA *victim,OBJ_DATA *object,
					 EVENT_FUN *fun,void *data);
void		remove_event		(EVENT_DATA *event);
bool		has_event		(CHAR_DATA *ch,EVENT_FUN *fun);
extern int	event_inuse;
extern int	event_allocated;

// reset recycling
RESET_DATA *	new_reset		(void);
void		free_reset		(RESET_DATA *pReset);
extern int	reset_inuse;
extern int	reset_allocated;

// area recycling
AREA_DATA *	new_area		(void);
void		free_area		(AREA_DATA *pArea);
extern int	area_inuse;
extern int	area_allocated;

// exit recycling
EXIT_DATA *	new_exit		(void);
void		free_exit		(EXIT_DATA *pExit);
extern int	exit_inuse;
extern int	exit_allocated;

// room index recycling
ROOM_INDEX_DATA *new_room_index		(void);
void		free_room_index		(ROOM_INDEX_DATA *pRoom);
extern int	room_index_inuse;
extern int	room_index_allocated;

// effect recycling
EFFECT_DATA *	new_effect		(void);
void		free_effect		(EFFECT_DATA* pAf);
extern int	effect_inuse;
extern int	effect_allocated;

// shop recycling
SHOP_DATA *	new_shop		(void);
void		free_shop		(SHOP_DATA *pShop);
extern int	shop_inuse;
extern int	shop_allocated;

// object index recycling
OBJ_INDEX_DATA *new_obj_index		(void);
void		free_obj_index		(OBJ_INDEX_DATA *pObj);
extern int	obj_index_inuse;
extern int	obj_index_allocated;

// mob index recycling
MOB_INDEX_DATA *new_mob_index		(void);
void		free_mob_index		(MOB_INDEX_DATA *pMob);
extern int	mob_index_inuse;
extern int	mob_index_allocated;

// help recycling
HELP_DATA *	new_help		(void);
void		free_help		(HELP_DATA *pHelp);
extern int	help_inuse;
extern int	help_allocated;

// buffer procedures
BUFFER	 *	new_buf		(void);
BUFFER	 *	new_buf_size		(int size);
void		free_buf		(BUFFER *buffer);
bool		add_buf			(BUFFER *buffer, char *string);
void		clear_buf		(BUFFER *buffer);
char	 *	buf_string		(BUFFER *buffer);
extern int	buffer_inuse;
extern int	buffer_allocated;

// social recycling
SOCIAL_TYPE *	new_social		(void);
void		add_social		(SOCIAL_TYPE *pSocial);
void		remove_social		(SOCIAL_TYPE *pSocial);
void		free_social		(SOCIAL_TYPE *pSocial);
extern int	social_inuse;
extern int	social_allocated;

// alias recycling
ALIAS_TYPE *	new_alias		(void);
void		free_alias		(ALIAS_TYPE *pAlias);
extern int	alias_inuse;
extern int	alias_allocated;

// dream recycling
DREAM_TYPE *	new_dream		(void);
void		free_dream		(DREAM_TYPE *dream);
extern int	dream_inuse;
extern int	dream_allocated;

// badnames recycling
BADNAME_DATA *	new_badname		(void);
void		free_badname		(BADNAME_DATA *badname);
extern int	badname_inuse;
extern int	badname_allocated;


#endif	// INCLUDED_RECYCLE_H
