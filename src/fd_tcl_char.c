//
// $Id: fd_tcl_char.c,v 1.63 2009/03/08 13:18:03 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#define NEEDS_TCL
#include "merc.h"
#include "fd_tcl_char.h"
#include "interp.h"

struct tcl_Char_Command_Struct {
    char *cmd;
    int  (*func)(Tcl_Interp *,CHAR_DATA *,int,Tcl_Obj *CONST []);
} tclCharCommands[] = {
	{ "name",		char_name		},
	{ "id",			char_id			},
	{ "isimmortal",		char_isimmortal		},
	{ "ispc",		char_ispc		},
	{ "isnpc",		char_isnpc		},
	{ "isgood",		char_isgood		},
	{ "isevil",		char_isevil		},
	{ "isneutral",		char_isneutral		},
	{ "ischarmed",		char_ischarmed		},
	{ "follows",		char_follows		},
	{ "isactive",		char_isactive		},
	{ "vnum",		char_vnum		},
	{ "hp",			char_hp			},
	{ "maxhp",		char_maxhp		},
	{ "move",		char_move		},
	{ "maxmove",		char_maxmove		},
	{ "room",		char_room		},
	{ "level",		char_level		},
	{ "sex",		char_sex		},
	{ "align",		char_align		},
	{ "money",		char_money		},
	{ "position",		char_position		},
	{ "fullname",		char_fullname		},
	{ "nametitle",		char_nametitle		},
	{ "title",		char_title		},
	{ "hasjob",		char_hasjob		},
	{ "isaff",		char_iseff		},
	{ "iseff",		char_iseff		},
	{ "eff",		char_eff		},
	{ "aff",		char_eff		},
	{ "isact",		char_isact		},
	{ "acts",		char_acts		},
	{ "isoff",		char_isoff		},
	{ "off",		char_off		},
	{ "isimm",		char_isimm		},
	{ "imm",		char_imm		},
	{ "isres",		char_isres		},
	{ "res",		char_res		},
	{ "isvuln",		char_isvuln		},
	{ "vuln",		char_vuln		},
	{ "clan",		char_clan		},
	{ "rank",		char_rank		},
	{ "race",		char_race		},
	{ "class",		char_class		},
	{ "he",			char_he			},
	{ "him",		char_him		},
	{ "his",		char_his		},
	{ "echo",		char_echo		},
	{ "echoaround",		char_echoaround		},
	{ "cansee",		char_cansee		},
	{ "hpcnt",		char_hpcnt		},
	{ "short",		char_short		},
	{ "goto",		char_goto		},
	{ "act",		char_act		},
	{ "iscarrying",		char_iscarrying		},
	{ "iswearing",		char_iswearing		},
	{ "wears",		char_wears		},
	{ "remove",		char_remove		},
	{ "do",			char_do			},
	{ "damage",		char_damage		},
	{ "group",		char_group		},
	{ "followers",		char_followers		},
	{ "property",		char_property		},
	{ "remeff",		char_remeff		},
	{ "remaff",		char_remeff		},
	{ "beenhere",		char_beenhere		},
	{ "killedmob",		char_killedmob		},
	{ "questpoints",	char_questpoints	},
	{ "quest",		char_quest		},
	{ "exp",		char_exp		},
	{ "tnl",		char_tnl		},
	{ "expforalevel",	char_expforalevel	},
	{ "dex",		char_dex		},
	{ "int",		char_int		},
	{ "wis",		char_wis		},
	{ "str",		char_str		},
	{ "con",		char_con		},
	{ "removespell",	char_removespell	},
	{ "timeskilled",	char_timeskilled	},
	{ "unequip",		char_unequip,		},
	{ "equip",		char_equip,		},
	{ "hunger",		char_hunger		},
	{ "thirst",		char_thirst		},
	{ "adrenaline",		char_adrenaline		},
	{ "drunk",		char_drunk		},
	{ "carries",		char_carries		},
	{ "ool_penalty",	char_ool_penalty	},
	{ "canseeobj",		char_canseeobj		},
	{ "oload",		char_oload		},
	{ "addaff",		char_addeff		},
	{ "addeff",		char_addeff		},
	{ "delproperty",	char_delproperty	},
	{ "mana",		char_mana		},
	{ "maxmana",		char_maxmana		},
	{ "fighting",		char_fighting		},
	{ "die",		char_die		},
	{ "wimpy", 		char_wimpy		},
	{ "objhere",		char_objhere		},
	{ "listeff",		char_listeff		},
	{ "listaff",		char_listeff		},
	{ "selectobj",		char_selectobj		},
	{ "exists",		CharCmdNotImpl		},
	{ NULL,			CharCmdNotImpl		},
};


int tclCharObjCmd (client, interp, objc, objv)
    ClientData client;			/* Not used. */
    Tcl_Interp *interp;			/* Current interpreter. */
    int objc;				/* Number of arguments. */
    Tcl_Obj *CONST objv[];		/* Argument values. */
{
    CHAR_DATA *ch;
    int index;

    // we know objv[0] is 'char'          
    objc--; objv++;

    ch=tcl_parse_char_switches(progData->ch,&objc,&objv);

    if (objc==1 && !str_cmp(Tcl_GetString(objv[0]),"exists")) {
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(ch!=NULL));
	return TCL_OK;
    }

    if (!ch) return TCL_ERROR;

    if (objc==0) {
	// No arguments given return name.
	return char_name(interp,ch,objc,objv);
    }

    if (Tcl_GetIndexFromObjStruct(interp, objv[0], 
				  (char **)tclCharCommands, sizeof(struct tcl_Char_Command_Struct), 
				  "char command", 0, &index) != TCL_OK) {
	return TCL_ERROR;
    }

    return (tclCharCommands[index].func)(interp,ch,objc,objv);
}

TCL_CHAR_CMD(CharCmdNotImpl) {
    Tcl_Obj *result;

    result=Tcl_NewStringObj("Char command not implemented yet: ",-1);
    Tcl_AppendObjToObj(result,objv[0]);
    Tcl_SetObjResult(interp,result);

    return TCL_ERROR;
}

/* name */
TCL_CHAR_CMD(char_name) {
    Tcl_SetObjResult(interp,Tcl_NewStringObj(tcl_firstname(ch),-1));
    return TCL_OK;
}

/* id */
TCL_CHAR_CMD(char_id) {
    Tcl_SetObjResult(interp,Tcl_NewLongObj(ch->id));
    return TCL_OK;
}

/* isimmortal */
TCL_CHAR_CMD(char_isimmortal) {
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(IS_IMMORTAL(ch)));
    return TCL_OK;
}

/* ispc */
TCL_CHAR_CMD(char_ispc) {
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(IS_PC(ch)));
    return TCL_OK;
}

/* isnpc */
TCL_CHAR_CMD(char_isnpc) {
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(IS_NPC(ch)));
    return TCL_OK;
}

/* isgood */
TCL_CHAR_CMD(char_isgood) {
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(IS_GOOD(ch)));
    return TCL_OK;
}

/* isevil */
TCL_CHAR_CMD(char_isevil) {
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(IS_EVIL(ch)));
    return TCL_OK;
}

/* isneutral */
TCL_CHAR_CMD(char_isneutral) {
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(IS_NEUTRAL(ch)));
    return TCL_OK;
}

/* ischarmed */
TCL_CHAR_CMD(char_ischarmed) {
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(IS_AFFECTED(ch,EFF_CHARM)));
    return TCL_OK;
}

/* follows */
TCL_CHAR_CMD(char_follows) {
    static tclOptionList *followsSwitches[]={"-name",NULL};
    int sindex=-1;

    if (objc>1) {
	if (Tcl_GetIndexFromObj(interp, objv[1], followsSwitches, "switch", 0, &sindex) != TCL_OK)
	    return TCL_ERROR;
    }

    if (sindex==-1) {
	if (ch->master && ch->master->in_room==ch->in_room)
	    Tcl_SetObjResult(interp,Tcl_NewLongObj(ch->master->id));
	else
	    Tcl_SetObjResult(interp,Tcl_NewLongObj(0));
    } else {
	if (ch->master && ch->master->in_room==ch->in_room)
	    Tcl_SetObjResult(interp,Tcl_NewStringObj(tcl_firstname(ch->master),-1));
	else
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("",-1));
    }
    return TCL_OK;
}

/* isactive */
TCL_CHAR_CMD(char_isactive) {
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(ch->position>POS_SLEEPING));
    return TCL_OK;
}

/* vnum */
TCL_CHAR_CMD(char_vnum) {
    if (IS_NPC(ch))
	Tcl_SetObjResult(interp,Tcl_NewLongObj(ch->pIndexData->vnum));
    else
	Tcl_SetObjResult(interp,Tcl_NewLongObj(0));
    return TCL_OK;
}

/* hp ?new_value? */
TCL_CHAR_CMD(char_hp) {
    if (objc==1) {
	Tcl_SetObjResult(interp,Tcl_NewLongObj(ch->hit));
    } else {
	long newhit;
	if (Tcl_GetLongFromObj(interp,objv[1],&newhit)==TCL_ERROR)
	    return TCL_ERROR;
	ch->hit=newhit;
    }
    return TCL_OK;
}

/* maxhp ?new_value? */
TCL_CHAR_CMD(char_maxhp) {
    if (objc==1) {
	Tcl_SetObjResult(interp,Tcl_NewLongObj(ch->max_hit));
    } else {
	long newmaxhit;
	if (Tcl_GetLongFromObj(interp,objv[1],&newmaxhit)==TCL_ERROR)
	    return TCL_ERROR;
	logf("[%d] max_hit on %s changed %d -> %d",
	    GET_DESCRIPTOR(ch),ch->name,ch->max_hit,newmaxhit);
	ch->max_hit=newmaxhit;
    }
    return TCL_OK;
}

/* move ?new_value? */
TCL_CHAR_CMD(char_move) {
    if (objc==1) {
	Tcl_SetObjResult(interp,Tcl_NewLongObj(ch->move));
    } else {
	long newmove;
	if (Tcl_GetLongFromObj(interp,objv[1],&newmove)==TCL_ERROR)
	    return TCL_ERROR;
	ch->move=newmove;
    }
    return TCL_OK;
}

/* maxmove ?new_value? */
TCL_CHAR_CMD(char_maxmove) {
    if (objc==1) {
	Tcl_SetObjResult(interp,Tcl_NewLongObj(ch->max_move));
    } else {
	long newmaxmove;
	if (Tcl_GetLongFromObj(interp,objv[1],&newmaxmove)==TCL_ERROR)
	    return TCL_ERROR;
	logf("[%d] max_move on %s changed %d -> %d",
	    GET_DESCRIPTOR(ch),ch->name,ch->max_move,newmaxmove);
	ch->max_move=newmaxmove;
    }
    return TCL_OK;
}

/* room */
TCL_CHAR_CMD(char_room) {
    if (ch->in_room)
	Tcl_SetObjResult(interp,Tcl_NewLongObj(ch->in_room->vnum));
    else
	Tcl_SetObjResult(interp,Tcl_NewLongObj(0));
    return TCL_OK;
}

/* level */
TCL_CHAR_CMD(char_level) {
    Tcl_SetObjResult(interp,Tcl_NewLongObj(ch->level));
    return TCL_OK;
}

/* sex */
TCL_CHAR_CMD(char_sex) {
    switch(show_sex(ch)) {
	case SEX_MALE: Tcl_SetObjResult(interp,Tcl_NewStringObj("male",-1));break;
	case SEX_FEMALE: Tcl_SetObjResult(interp,Tcl_NewStringObj("female",-1));break;
	default: Tcl_SetObjResult(interp,Tcl_NewStringObj("it",-1));break;
    }
    return TCL_OK;
}

/* align */
TCL_CHAR_CMD(char_align) {
    if (objc==1) {
	Tcl_SetObjResult(interp,Tcl_NewLongObj(ch->alignment));
	return TCL_OK;
    }

    if (objc==2) {
	static tclOptionList *alignSwitches[]={"-text",NULL};
	int sindex=-1;
	int newAlignment=0;
	
	if (Tcl_GetIntFromObj(interp, objv[1], &newAlignment)==TCL_OK) {
	  if(newAlignment>=-1000 && newAlignment<=1000) {
	    ch->alignment = newAlignment;
	    char_obj_alignment_check(ch);
	    return TCL_OK;
	  } else {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("char: new alignment must be in range -1000..1000",-1));
	    return TCL_ERROR;
	  }
	}
	
	if (Tcl_GetIndexFromObj(interp, objv[1], alignSwitches, "switch", 0, &sindex) != TCL_OK)
	    return TCL_ERROR;

	/* Only one argument so this is the correct one. */
	if (IS_EVIL(ch))
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("evil",-1));
	else if (IS_GOOD(ch))
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("good",-1));
	else
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("neutral",-1));
    }
    return TCL_OK;
}

/* money [-gold] [-silver] [-total] [-amountismax] [[-deduct|-add] <amount>] */
TCL_CHAR_CMD(char_money) {
    static tclOptionList *moneySwitches[]={"-gold","-silver","-total","-amountismax","-deduct","-add",NULL};
    int sindex=-1;
    int i;
    int amountIsMax=0;
    int deduct=0;
    int add=0;
    int amount[3]; // total,gold,silver

    if (objc==1) {
	Tcl_SetObjResult(interp,Tcl_NewLongObj(ch->gold * 100 + ch->silver));
	return TCL_OK;
    }

    for (i=1;i<objc;i++) {
	if (*Tcl_GetString(objv[i])!='-') break;
	if (Tcl_GetIndexFromObj(interp, objv[i], moneySwitches, "switch", 0, &sindex) != TCL_OK)
	    return TCL_ERROR;

	switch (sindex) {
	case 0:
	    Tcl_SetObjResult(interp,Tcl_NewLongObj(ch->gold));
	    return TCL_OK;
	case 1:
	    Tcl_SetObjResult(interp,Tcl_NewLongObj(ch->silver));
	    return TCL_OK;
	case 2:
	    Tcl_SetObjResult(interp,Tcl_NewLongObj(ch->gold * 100 + ch->silver));
	    return TCL_OK;
	case 3: 
	    amountIsMax=1;
	    break;
	case 4:
	    deduct=1;
	    break;
	case 5: 
	    add=1;
	    break;
	}
    }
    if (add & deduct) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("char money: can't add and deduct at the same time",-1));
	return TCL_ERROR;
    }

    if (objc==i) {
	Tcl_WrongNumArgs(interp,objc,objv,"<amount> ?<amount>?");
	return TCL_ERROR;
    }
    if (objc-i==1) {
	amount[1]=-1;
	amount[2]=-1;
	if (Tcl_GetIntFromObj(interp,objv[i++],&amount[0])!=TCL_OK)
	    return TCL_ERROR;
	if (amount[0]<0) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("char money: amount should be a non-negative number ",-1));
	    return TCL_ERROR;
	}
    } else {
	amount[0]=-1;
	if (Tcl_GetIntFromObj(interp,objv[i++],&amount[1])!=TCL_OK)
	    return TCL_ERROR;
	if (amount[1]<0) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("char money: amount of gold should be non-negative",-1));
	    return TCL_ERROR;
	}
	if (Tcl_GetIntFromObj(interp,objv[i++],&amount[2])!=TCL_OK)
	    return TCL_ERROR;
	if (amount[2]<0) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("char money: amount of silver should be non-negative",-1));
	    return TCL_ERROR;
	}
    }

    if (deduct) {
	if (amount[0]==-1) {
	    if (amount[1]>ch->gold) {
		if (amountIsMax) {
		    amount[1]=ch->gold;
		} else {
		    Tcl_SetObjResult(interp,Tcl_NewStringObj("char money -deduct: amount more than gold of player",-1));
		    return TCL_ERROR;
		}
	    }
	    if (amount[2]>ch->silver) {
		if (amountIsMax) {
		    amount[2]=ch->silver;
		} else {
		    Tcl_SetObjResult(interp,Tcl_NewStringObj("char money -deduct: amount more than silver of player",-1));
		    return TCL_ERROR;
		}
	    }
	    deduct_cost(ch,amount[1],amount[2],FALSE);
	} else {
	    if (amount[0]>ch->gold*100+ch->silver) {
		if (amountIsMax) {
		    amount[0]=ch->gold*100+ch->silver;
		} else {
		    Tcl_SetObjResult(interp,Tcl_NewStringObj("char money -deduct: amount more than worth of player",-1));
		    return TCL_ERROR;
		}
	    }
	    deduct_cost(ch,0,amount[0],FALSE);
	}
	return TCL_OK;
    }
    if (add) {
	if (amount[0]==-1) {
	    ch->gold+=amount[1];
	    ch->silver+=amount[2];
	} else {
	    ch->gold+=amount[0]/100;
	    ch->silver+=amount[0]%100;
	}
	return TCL_OK;
    }

    if (amount[0]==-1) {
	ch->gold=amount[1];
	ch->silver=amount[2];
    } else {
	ch->gold=amount[0]/100;
	ch->silver=amount[0]%100;
    }
    return TCL_OK;
}

/* position */
TCL_CHAR_CMD(char_position) {
    if (objc==1) {
	Tcl_SetObjResult(interp,Tcl_NewLongObj(ch->position));
    } else {
	if (objc==2) {
	    static tclOptionList *positionSwitches[]={"-text",NULL};
	    int sindex=-1;

	    if (Tcl_GetIndexFromObj(interp, objv[1], positionSwitches, "switch", 0, &sindex) != TCL_OK)
		return TCL_ERROR;

	    /* Only one argument so this is the correct one. */
	    Tcl_SetObjResult(interp,Tcl_NewStringObj(position_table[ch->position].short_name,-1));
	}
    }
    return TCL_OK;
}

/* fullname */
TCL_CHAR_CMD(char_fullname) {
    Tcl_SetObjResult(interp,Tcl_NewStringObj(ch->name,-1));
    return TCL_OK;
}

/* nametitle */
TCL_CHAR_CMD(char_nametitle) {
    if (IS_NPC(ch))
	Tcl_SetObjResult(interp,Tcl_NewStringObj(tcl_firstname(ch),-1));
    else {
	Tcl_Obj *result;
	result=Tcl_NewStringObj(ch->name,-1);
	Tcl_AppendToObj(result,ch->pcdata->title,-1);
	Tcl_SetObjResult(interp,result);
    }
    return TCL_OK;
}

/* title */
TCL_CHAR_CMD(char_title) {
    if (IS_PC(ch))
	Tcl_SetObjResult(interp,Tcl_NewStringObj(ch->pcdata->title,-1));
    return TCL_OK;
}

/* hasjob */
TCL_CHAR_CMD(char_hasjob) {
    if (!IS_PC(ch)) 
	return TCL_OK;

    if (objc<2) {
	// produce a list
	Tcl_Obj *result;
	JOB_TYPE *job;
	extern JOB_TYPE *job_list;

	result=Tcl_NewListObj(0,NULL);

	for (job=job_list;job;job=job->next) {
	    if (STR_IS_SET(ch->pcdata->jobs,job->vnum)) {
		Tcl_ListObjAppendElement(interp,result,Tcl_NewStringObj(job->name,-1));
	    }
	}
	Tcl_SetObjResult(interp,result);
    } else {
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(char_has_job(ch,Tcl_GetString(objv[1]))));
    }
    return TCL_OK;
}

/* iseff effect */
TCL_CHAR_CMD(char_iseff) {
    long bitvec;

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"char iseff <effect>");
	return TCL_ERROR;
    }
    if (!str_cmp(Tcl_GetString(objv[0]),"isaff"))
	bugf("TCL: %s: char isaff DEPRICATED, use char iseff",current_namespace);

    bitvec=option_find_name(Tcl_GetString(objv[1]),effect_options,FALSE);
    if (bitvec==NO_FLAG) {
	bitvec=option_find_name(Tcl_GetString(objv[1]),effect2_options,FALSE);
	if (bitvec==NO_FLAG) {
	    Tcl_Obj *result;
	    result=Tcl_NewStringObj("char: unknown iseff flag ",-1);
	    Tcl_AppendStringsToObj(result,Tcl_GetString(objv[1]),".",NULL);
	    Tcl_SetObjResult(interp,result);
	    return TCL_ERROR;
	}
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(IS_AFFECTED2(ch,bitvec)));
    } else
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(IS_AFFECTED(ch,bitvec)));
    return TCL_OK;
}

/* eff */
TCL_CHAR_CMD(char_eff) {
    Tcl_Obj *result;
    if (!str_cmp(Tcl_GetString(objv[0]),"aff"))
	bugf("TCL: %s: char aff DEPRICATED, use char eff",current_namespace);
    result=Tcl_NewStringObj(char_affected_string(ch),-1);
    Tcl_AppendStringsToObj(result," ",char_affected2_string(ch),NULL);
    Tcl_SetObjResult(interp,result);
    return TCL_OK;
}

/* isact effect */
TCL_CHAR_CMD(char_isact) {
    long bitvec;

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"char isact <act>");
	return TCL_ERROR;
    }

    if (IS_NPC(ch))
	bitvec=option_find_name(Tcl_GetString(objv[1]),act_options,FALSE);
    else
	bitvec=option_find_name(Tcl_GetString(objv[1]),plr_options,FALSE);

    if (bitvec==NO_FLAG) {
	Tcl_Obj *result;
	result=Tcl_NewStringObj("char: unknown isact flag ",-1);
	Tcl_AppendStringsToObj(result,Tcl_GetString(objv[1]),".",NULL);
	Tcl_SetObjResult(interp,result);
	return TCL_ERROR;
    }

    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(STR_IS_SET(ch->strbit_act,bitvec)));
    return TCL_OK;
}

/* acts [flags] */
TCL_CHAR_CMD(char_acts) {
    long bitvec;

    if (objc==1) {
	if (IS_NPC(ch))
	    Tcl_SetObjResult(interp,Tcl_NewStringObj(char_act_string(ch),-1));
	else
	    Tcl_SetObjResult(interp,Tcl_NewStringObj(char_plr_string(ch),-1));
    } else {
	if (IS_NPC(ch)) {
	    if ((bitvec=option_find_name(Tcl_GetString(objv[1]),act_options,FALSE))==NO_FLAG) {
		Tcl_Obj *result;
		result=Tcl_NewStringObj("char: unknown acts flag ",-1);
		Tcl_AppendStringsToObj(result,Tcl_GetString(objv[1]),".",NULL);
		Tcl_SetObjResult(interp,result);
		return TCL_ERROR;
	    }
	} else {
	    if ((bitvec=option_find_name(Tcl_GetString(objv[1]),plr_options,FALSE))==NO_FLAG) {
		Tcl_Obj *result;
		result=Tcl_NewStringObj("char: unknown acts flag ",-1);
		Tcl_AppendStringsToObj(result,Tcl_GetString(objv[1]),".",NULL);
		Tcl_SetObjResult(interp,result);
		return TCL_ERROR;
	    }
	}
	if (objc>=3) {
	    int newvalue;

	    if (Tcl_GetBooleanFromObj(interp,objv[2],&newvalue)!=TCL_OK)
		return TCL_ERROR;

	    if (newvalue) 
		STR_SET_BIT(ch->strbit_act,bitvec);
	    else
		STR_REMOVE_BIT(ch->strbit_act,bitvec);
	} else 
	    STR_TOGGLE_BIT(ch->strbit_act,bitvec);
    }
    return TCL_OK;
}

/* isoff off */
TCL_CHAR_CMD(char_isoff) {
    long bitvec;

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"char isoff <off>");
	return TCL_ERROR;
    }

    bitvec=option_find_name(Tcl_GetString(objv[1]),off_options,FALSE);

    if (bitvec==NO_FLAG) {
	Tcl_Obj *result;
	result=Tcl_NewStringObj("char: unknown isoff flag ",-1);
	Tcl_AppendStringsToObj(result,Tcl_GetString(objv[1]),".",NULL);
	Tcl_SetObjResult(interp,result);
	return TCL_ERROR;
    }

    if (IS_NPC(ch)) Tcl_SetObjResult(interp,Tcl_NewBooleanObj(STR_IS_SET(ch->strbit_off_flags,bitvec)));
    else Tcl_SetObjResult(interp,Tcl_NewBooleanObj(FALSE));

    return TCL_OK;
}

/* off */
TCL_CHAR_CMD(char_off) {
    if (IS_NPC(ch))
	Tcl_SetObjResult(interp,Tcl_NewStringObj(char_off_string(ch),-1));
    return TCL_OK;
}

/* isimm effect */
TCL_CHAR_CMD(char_isimm) {
    long bitvec;

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"char isimm <imm>");
	return TCL_ERROR;
    }

    bitvec=option_find_name(Tcl_GetString(objv[1]),imm_options,FALSE);

    if (bitvec==NO_FLAG) {
	Tcl_Obj *result;
	result=Tcl_NewStringObj("char: unknown isimm flag ",-1);
	Tcl_AppendStringsToObj(result,Tcl_GetString(objv[1]),".",NULL);
	Tcl_SetObjResult(interp,result);
	return TCL_ERROR;
    }

    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(STR_IS_SET(ch->strbit_imm_flags,bitvec)));
    return TCL_OK;
}

/* imm */
TCL_CHAR_CMD(char_imm) {
    Tcl_SetObjResult(interp,Tcl_NewStringObj(char_imm_string(ch),-1));
    return TCL_OK;
}

/* isres effect */
TCL_CHAR_CMD(char_isres) {
    long bitvec;

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"char isres <res>");
	return TCL_ERROR;
    }

    bitvec=option_find_name(Tcl_GetString(objv[1]),res_options,FALSE);
    if (bitvec==NO_FLAG) {
	Tcl_Obj *result;
	result=Tcl_NewStringObj("char: unknown isres flag ",-1);
	Tcl_AppendStringsToObj(result,Tcl_GetString(objv[1]),".",NULL);
	Tcl_SetObjResult(interp,result);
	return TCL_ERROR;
    }

    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(STR_IS_SET(ch->strbit_res_flags,bitvec)));

    return TCL_OK;
}

/* res */
TCL_CHAR_CMD(char_res) {
    Tcl_SetObjResult(interp,Tcl_NewStringObj(char_res_string(ch),-1));
    return TCL_OK;
}

/* isvuln effect */
TCL_CHAR_CMD(char_isvuln) {
    long bitvec;

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"char isvuln <vuln>");
	return TCL_ERROR;
    }

    bitvec=option_find_name(Tcl_GetString(objv[1]),vuln_options,FALSE);

    if (bitvec==NO_FLAG) {
	Tcl_Obj *result;
	result=Tcl_NewStringObj("char: unknown isvuln flag ",-1);
	Tcl_AppendStringsToObj(result,Tcl_GetString(objv[1]),".",NULL);
	Tcl_SetObjResult(interp,result);
	return TCL_ERROR;
    }

    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(STR_IS_SET(ch->strbit_vuln_flags,bitvec)));
    return TCL_OK;
}

/* vuln */
TCL_CHAR_CMD(char_vuln) {
    Tcl_SetObjResult(interp,Tcl_NewStringObj(char_vuln_string(ch),-1));
    return TCL_OK;
}

/* clan */
TCL_CHAR_CMD(char_clan) {
    if (ch->clan)
	Tcl_SetObjResult(interp,Tcl_NewStringObj(ch->clan->clan_info->name,-1));
    return TCL_OK;
}

/* rank */
TCL_CHAR_CMD(char_rank) {
    if (ch->clan)
	Tcl_SetObjResult(interp,Tcl_NewStringObj(ch->clan->rank_info->rank_name,-1));
    return TCL_OK;
}

/* race */
TCL_CHAR_CMD(char_race) {
    Tcl_SetObjResult(interp,Tcl_NewStringObj(race_table[ch->race].name,-1));
    return TCL_OK;
}

/* class */
TCL_CHAR_CMD(char_class) {
    Tcl_SetObjResult(interp,Tcl_NewStringObj(IS_NPC(ch)?"mobile":class_table[ch->class].name,-1));
    return TCL_OK;
}

/* he */
TCL_CHAR_CMD(char_he) {
    Tcl_SetObjResult(interp,Tcl_NewStringObj(he_she[URANGE(0,show_sex(ch),2)],-1));
    return TCL_OK;
}

/* him */
TCL_CHAR_CMD(char_him) {
    Tcl_SetObjResult(interp,Tcl_NewStringObj(him_her[URANGE(0,show_sex(ch),2)],-1));
    return TCL_OK;
}

/* his */
TCL_CHAR_CMD(char_his) {
    Tcl_SetObjResult(interp,Tcl_NewStringObj(his_her[URANGE(0,show_sex(ch),2)],-1));
    return TCL_OK;
}

/* echo */
TCL_CHAR_CMD(char_echo) {
    Tcl_Obj *concatObj;

    if (objc<=1) {
	Tcl_WrongNumArgs(interp,0,objv,"char echo text ?text text ...?");
	return TCL_ERROR;
    }

    concatObj=Tcl_ConcatObj(objc-1,objv+1);
    Tcl_IncrRefCount(concatObj);
    Tcl_AppendToObj(concatObj,"\n\r",-1);
    send_to_char(Tcl_GetString(concatObj),ch);
    Tcl_DecrRefCount(concatObj);
    return TCL_OK;
}

/* echoaround */
TCL_CHAR_CMD(char_echoaround) {
    Tcl_Obj *concatObj;
    CHAR_DATA *wch;

    if (objc<=1) {
	Tcl_WrongNumArgs(interp,0,objv,"char echoaround text ?text text ...?");
	return TCL_ERROR;
    }

    concatObj=Tcl_ConcatObj(objc-1,objv+1);
    Tcl_IncrRefCount(concatObj);
    Tcl_AppendToObj(concatObj,"\n\r",-1);

    for (wch=ch->in_room->people;wch;wch=wch->next_in_room) {
	if (wch==ch) continue;
	send_to_char(Tcl_GetString(concatObj),wch);
    }

    Tcl_DecrRefCount(concatObj);
    return TCL_OK;
}

/* cansee */
TCL_CHAR_CMD(char_cansee) {
    CHAR_DATA *target;

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"char cansee charid");
	return TCL_ERROR;
    }

    if (!(target=tcl_charid_char(ch,Tcl_GetString(objv[1]),NULL))) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("char cansee: target not found",-1));
	return TCL_ERROR;
    }

    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(can_see(ch,target)));
    return TCL_OK;
}

/* hpcnt */
TCL_CHAR_CMD(char_hpcnt) {
    char *opt;
    int i;
    int hit=ch->hit;
    int as_text=0;

    i=0;
    while (objc>++i) {
        opt=Tcl_GetString(objv[i]);
        if (!str_cmp("-text",opt)) {
	    as_text=1;
	    continue;
	}
	if (Tcl_GetIntFromObj(interp,objv[i],&hit)!=TCL_OK) {
	    return TCL_ERROR;
	}
    }
    if (as_text) {
	Tcl_Obj *result;
	int pct=(hit * 100) / UMAX(1,ch->max_hit);
	if (pct >= 100)
	    result=Tcl_NewStringObj("is in excellent condition.",-1);
	else if (pct >= 90)
	    result=Tcl_NewStringObj("has a few scratches.",-1);
	else if (pct >= 75)
	    result=Tcl_NewStringObj("has some small wounds and bruises.",-1);
	else if (pct >=  50)
	    result=Tcl_NewStringObj("has quite a few wounds.",-1);
	else if (pct >= 30)
	    result=Tcl_NewStringObj("has some big nasty wounds and scratches.",-1);
	else if (pct >= 15)
	    result=Tcl_NewStringObj("looks pretty hurt.",-1);
	else if (pct >= 0)
	    result=Tcl_NewStringObj("is in awful condition.",-1);
	else
	    result=Tcl_NewStringObj("is bleeding to death.",-1);
	Tcl_SetObjResult(interp,result);
    } else {
	Tcl_SetObjResult(interp,Tcl_NewLongObj((hit * 100) / UMAX(1,ch->max_hit)));
    }
    return TCL_OK;
}

/* short */
TCL_CHAR_CMD(char_short) {
    if (IS_NPC(ch)) 
	Tcl_SetObjResult(interp,Tcl_NewStringObj(ch->short_descr,-1));
    else
	Tcl_SetObjResult(interp,Tcl_NewStringObj(ch->name,-1));
    return TCL_OK;
}

/* goto <location> */
TCL_CHAR_CMD(char_goto) {
    ROOM_INDEX_DATA *room;
    ROOM_INDEX_DATA *from_room=ch->in_room;

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"char goto location");
	return TCL_ERROR;
    }

    if (!(room=tcl_location(Tcl_GetString(objv[1])))) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("char goto: location not found.",-1));
	return TCL_ERROR;
    }
    if ( ch->fighting != NULL )
	stop_fighting( ch, TRUE );
    ap_leave_trigger(ch,room);
    char_from_room(ch);
    char_to_room(ch,room);
    ap_enter_trigger(ch,from_room);
    return TCL_OK;
}

/* act */
TCL_CHAR_CMD(char_act) {
    if (tcl_act(ch,objc,objv)!=TCL_OK)
	return TCL_ERROR;
    return TCL_OK;
}

/* iscarrying */
TCL_CHAR_CMD(char_iscarrying) {
    OBJ_DATA *obj;
    char *id;

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"char iscarrying objid");
	return TCL_ERROR;
    }

    id=Tcl_GetString(objv[1]);

    for (obj=tcl_objid(ch->carrying,id,FALSE);
	 obj;
	 obj=tcl_objid(obj->next_content,id,FALSE)) {
	if (obj->wear_loc==WEAR_NONE) break;
    }

    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(obj!=NULL));
    return TCL_OK;
}

/* iswearing */
TCL_CHAR_CMD(char_iswearing) {
    OBJ_DATA *obj;
    char *id;

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"char iswearing objid");
	return TCL_ERROR;
    }

    id=Tcl_GetString(objv[1]);

    for (obj=tcl_objid(ch->carrying,id,FALSE);
	obj;
	obj=tcl_objid(obj->next_content,id,FALSE)) {
	if (obj->wear_loc!=WEAR_NONE) break;
    }

    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(obj!=NULL));
    return TCL_OK;
}

TCL_CHAR_CMD(char_wears) {
    Tcl_Obj *result;
    int result_type=0;
    OBJ_DATA *obj;
    unsigned char wearlocs[(MAX_WEAR+7)/8];
    bool fAll=TRUE;

    bzero(wearlocs,sizeof(wearlocs));

    if (objc>=2) {
	static tclOptionList *Switches[]={"id","name","vnum",NULL};

	if (Tcl_GetIndexFromObj(interp,objv[1],Switches,"Result type",0,&result_type)!=TCL_OK)
	    return TCL_ERROR;
    }
    if (objc>=3) {
	int i;

	for (i=2;i<objc;i++) {
	    int loc;
	    if (Tcl_GetIndexFromObjStruct(interp,objv[i],
					  (char **)&(wear_loc_options->name),sizeof(OPTION_TYPE),
					  "Wear location",0,&loc)!=TCL_OK)
		return TCL_ERROR;

	    if (wear_loc_options[loc].value!=WEAR_NONE)
		STR_SET_BIT(wearlocs,wear_loc_options[loc].value);
	}
	fAll=FALSE;
    }

    result=Tcl_NewListObj(0,NULL);

    for (obj=ch->carrying;obj;obj=obj->next_content) {
	if (obj->wear_loc==WEAR_NONE) continue;
	if (fAll || STR_IS_SET(wearlocs,obj->wear_loc))
	    switch (result_type) {
		case 0: Tcl_ListObjAppendElement(interp,result,Tcl_NewLongObj(obj->id));
			break;
		case 1: Tcl_ListObjAppendElement(interp,result,Tcl_NewStringObj(tcl_firstobjname(obj),-1));
			break;
		case 2: Tcl_ListObjAppendElement(interp,result,Tcl_NewIntObj(obj->pIndexData->vnum));
			break;
	    }
    }

    Tcl_SetObjResult(interp,result);
    return TCL_OK;
}

/* remove ?-all? ?objid? */
TCL_CHAR_CMD(char_remove) {
    if (tcl_remove(ch,objc,objv)!=TCL_OK) return TCL_ERROR;
    return TCL_OK;
}

/* do tcl-commands */
TCL_CHAR_CMD(char_do) {
    int i=1;
    int error;
    CHAR_DATA *old;

    if (objc-i!=1) {
	Tcl_WrongNumArgs(interp,0,objv,"char do tclscript");
	return TCL_ERROR;
    }

    old=progData->ch;
    progData->ch=ch;

    error=Tcl_EvalObjEx(interp,objv[i],0);

    progData->ch=old;

    return error;
}

/* damage -lethal min max */
TCL_CHAR_CMD(char_damage) {
    int i=1,index;
    int min,max;
    char *s;
    bool lethal=FALSE;
    static tclOptionList *damageSwitches[]={"-lethal",NULL};

    while (objc-i>0) {
	s=Tcl_GetString(objv[i]);
	if (*s!='-') break;

	if (Tcl_GetIndexFromObj(interp, objv[i], damageSwitches, "switch", 0, &index) != TCL_OK)
	    return TCL_ERROR;

	if (index==0) lethal=TRUE;

	i++;
    }

    if (objc-i!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"room damage ?-self? ?-lethal? min max");
	return TCL_ERROR;
    }

    if (Tcl_GetIntFromObj(interp,objv[i],&min)!=TCL_OK) return TCL_ERROR;
    if (Tcl_GetIntFromObj(interp,objv[i+1],&max)!=TCL_OK) return TCL_ERROR;

    damage( ch, ch,
	lethal ?
	number_range(min,max) : UMIN(ch->hit,number_range(min,max)),
	TYPE_UNDEFINED, DAM_NONE, FALSE, NULL );
    return TCL_OK;
}

/* group <tcl-script> */
TCL_CHAR_CMD(char_group) {
    CHAR_DATA *vch,*vch_next;
    CHAR_DATA *oldch;
    int error=0;
    bool recursive=FALSE;
    int i=1;
    int group_count=0;

    if (objc>i && !str_cmp("-recursive",Tcl_GetString(objv[i]))) {
	recursive=TRUE;
	i++;
    }

    oldch=progData->ch;
    if (!ch->in_room) return TCL_OK;

    for (vch = ch->in_room->people; vch != NULL; vch = vch_next ) {
	vch_next = vch->next_in_room;

	if ( (!recursive && is_same_group(ch,vch) )||
	     ( recursive && is_same_group_recursive(ch,vch))) {
	    group_count++;
	    if (objc>i) {
		progData->ch=vch;
		error=Tcl_EvalObjEx(interp,objv[i],0);
		if (error!=TCL_OK) break;
	    }
	}
    }
    progData->ch=oldch;

    if (error==TCL_OK) 
	Tcl_SetObjResult(interp,Tcl_NewIntObj(group_count));
    return error;
}

/* followers <tcl-script> */
TCL_CHAR_CMD(char_followers) {
    CHAR_DATA *vch,*vch_next;
    CHAR_DATA *oldch;
    int error=0;
    bool recursive=FALSE;
    int i=1;
    int group_count=0;

    if (objc>i && !str_cmp("-recursive",Tcl_GetString(objv[i]))) {
	recursive=TRUE;
	i++;
    }

    oldch=progData->ch;
    if (!ch->in_room) return TCL_OK;

    for (vch = ch->in_room->people; vch != NULL; vch = vch_next ) {
	vch_next = vch->next_in_room;

	if ( (!recursive && has_same_master(ch,vch) )||
	     ( recursive && has_same_master_recursive(ch,vch))) {
	    group_count++;
	    if (objc>i) {
		progData->ch=vch;
		error=Tcl_EvalObjEx(interp,objv[i],0);
		if (error!=TCL_OK) break;
	    }
	}
    }
    progData->ch=oldch;

    if (error==TCL_OK) 
	Tcl_SetObjResult(interp,Tcl_NewIntObj(group_count));
    return error;
}

// property <key> <type> [value]
TCL_CHAR_CMD(char_property) {
    char *key,*stype,*svalue;
    int itype;
    bool readonly;

    if (objc!=3 && objc!=4) {
	Tcl_WrongNumArgs(interp,0,objv,"char property key type ?newvalue?");
	return TCL_ERROR;
    }

    readonly=(objc==3);

    key=Tcl_GetString(objv[1]);
    stype=Tcl_GetString(objv[2]);
    if (!readonly)
	svalue=Tcl_GetString(objv[3]);

    itype=PROPERTY_UNDEF;
    if (str_cmp(stype,"int")   ==0) itype=PROPERTY_INT;
    if (str_cmp(stype,"long")  ==0) itype=PROPERTY_LONG;
    if (str_cmp(stype,"bool")  ==0) itype=PROPERTY_BOOL;
    if (str_cmp(stype,"string")==0) itype=PROPERTY_STRING;
    if (str_cmp(stype,"char")  ==0) itype=PROPERTY_CHAR;
    if (itype==PROPERTY_UNDEF) {
	Tcl_WrongNumArgs(interp,0,objv,"type should be: int, long, bool, string or char");
	return TCL_ERROR;
    }

    if (readonly) {
	if (itype==PROPERTY_INT) {
	    int ivalue;
	    if (GetCharProperty(ch,PROPERTY_INT,key,&ivalue)==0)
		Tcl_SetObjResult(interp,Tcl_NewIntObj(0));
	    else
		Tcl_SetObjResult(interp,Tcl_NewIntObj(ivalue));
	}

	if (itype==PROPERTY_LONG) {
	    long lvalue;
	    if (GetCharProperty(ch,PROPERTY_LONG,key,&lvalue)==0)
		Tcl_SetObjResult(interp,Tcl_NewLongObj(0));
	    else
		Tcl_SetObjResult(interp,Tcl_NewLongObj(lvalue));
	}

	if (itype==PROPERTY_BOOL) {
	    bool bvalue;
	    if (GetCharProperty(ch,PROPERTY_BOOL,key,&bvalue)==0)
		Tcl_SetObjResult(interp,Tcl_NewBooleanObj(FALSE));
	    else
		Tcl_SetObjResult(interp,Tcl_NewBooleanObj(bvalue));
	}

	if (itype==PROPERTY_STRING) {
	    char svalue[128];
	    if (GetCharProperty(ch,PROPERTY_STRING,key,svalue)==0)
		Tcl_SetObjResult(interp,Tcl_NewStringObj("",-1));
	    else
		Tcl_SetObjResult(interp,Tcl_NewStringObj(svalue,-1));
	}

	if (itype==PROPERTY_CHAR) {
	    char cvalue,svalue[2];
	    if (GetCharProperty(ch,PROPERTY_CHAR,key,&cvalue)==0) {
		svalue[0]=' ';svalue[1]=0;
		Tcl_SetObjResult(interp,Tcl_NewStringObj(svalue,-1));
	    } else {
		svalue[0]=cvalue;svalue[1]=0;
		Tcl_SetObjResult(interp,Tcl_NewStringObj(svalue,-1));
	    }
	}
    } else {
	if (itype==PROPERTY_INT) {
	    int ivalue;
	    if (Tcl_GetIntFromObj(interp,objv[3],&ivalue)!=TCL_OK) 
		return TCL_ERROR;
	    SetCharProperty(ch,PROPERTY_INT,key,&ivalue);
	}

	if (itype==PROPERTY_LONG) {
	    long lvalue;
	    if (Tcl_GetLongFromObj(interp,objv[3],&lvalue)!=TCL_OK)   
		return TCL_ERROR;
	    SetCharProperty(ch,PROPERTY_LONG,key,&lvalue);
	}

	if (itype==PROPERTY_BOOL) {
	    int i;
	    bool bvalue;
	    if (Tcl_GetBooleanFromObj(interp,objv[3],&i)!=TCL_OK)   
		return TCL_ERROR;
	    bvalue=i!=0;
	    SetCharProperty(ch,PROPERTY_BOOL,key,&bvalue);
	}

	if (itype==PROPERTY_STRING) {
	    SetCharProperty(ch,PROPERTY_STRING,key,svalue);
	}

	if (itype==PROPERTY_CHAR) {
	    char cvalue=svalue[1];
	    SetCharProperty(ch,PROPERTY_CHAR,key,&cvalue);
	}
    }
    return TCL_OK;
}

// remeff <effect>
TCL_CHAR_CMD(char_remeff) {
    long bitvec;

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"char remeff <effect>");
	return TCL_ERROR;
    }
    if (!str_cmp(Tcl_GetString(objv[0]),"remaff"))
	bugf("TCL: %s: char remaff DEPRICATED, use char remeff",current_namespace);

    bitvec=option_find_name(Tcl_GetString(objv[1]),effect_options,TRUE);
    if (bitvec==NO_FLAG) {
	bitvec=option_find_name(Tcl_GetString(objv[1]),effect2_options,TRUE);
	if (bitvec==NO_FLAG) {
	    Tcl_Obj *result;
	    result=Tcl_NewStringObj("char: unknown remeff flag ",-1);
	    Tcl_AppendStringsToObj(result,Tcl_GetString(objv[1]),".",NULL);
	    Tcl_SetObjResult(interp,result);
	    return TCL_ERROR;
	}
	STR_REMOVE_BIT(ch->strbit_affected_by2,bitvec);
    } else {
	STR_REMOVE_BIT(ch->strbit_affected_by,bitvec);
    }
    return TCL_OK;
}

// beenhere [room vnum]
TCL_CHAR_CMD(char_beenhere) {
    int vnum;
    if (IS_NPC(ch)) {
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(FALSE));
	return TCL_OK;
    }

    if (objc==1) {
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(ch->pcdata->newhere));
	return TCL_OK;
    }

    if (Tcl_GetIntFromObj(interp,objv[1],&vnum)==TCL_ERROR)
	return TCL_ERROR;

    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(STR_IS_SET(ch->pcdata->beeninroom,vnum)));
    return TCL_OK;
}

// killedmob <vnum>
TCL_CHAR_CMD(char_killedmob) {
    int vnum;

    if (IS_NPC(ch)) {
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(FALSE));
	return TCL_OK;
    }

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"char killedmob vnum");
	return TCL_ERROR;
    }

    if (Tcl_GetIntFromObj(interp,objv[1],&vnum)!=TCL_OK) return TCL_ERROR;
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(STR_IS_SET(ch->pcdata->killedthatmob,vnum)));

    return TCL_OK;
}

// questpoints [incr-value]
TCL_CHAR_CMD(char_questpoints) {
    int newqps;

    if (IS_NPC(ch))
	return TCL_OK;

    if (objc==1) {
	Tcl_SetObjResult(interp,Tcl_NewIntObj(ch->pcdata->questpoints));
    } else {
	if (Tcl_GetIntFromObj(interp,objv[1],&newqps)==TCL_ERROR)
	    return TCL_ERROR;
	ch->pcdata->questpoints+=newqps;
	logf("[%d] %s gained %d questpoints.",
	    GET_DESCRIPTOR(ch),NAME(ch),newqps);
    }
    return TCL_OK;
}

// quest info|reject|request|complete
TCL_CHAR_CMD(char_quest) {
    static tclOptionList *actions[]={"info","reject","request","complete",NULL};
    int action;

    if (Tcl_GetIndexFromObj(interp,objv[1],actions,"action",0,&action)!=TCL_OK)
	return TCL_ERROR;

    switch (action) {
	case 0: quest(ch,QUEST_INFO); break;
	case 1: quest(ch,QUEST_REJECT); break;
	case 2: quest(ch,QUEST_REQUEST); break;
	case 3: quest(ch,QUEST_COMPLETE); break;
    }

    return TCL_OK;
}

// exp [incr-value]
TCL_CHAR_CMD(char_exp) {
    int newexp;

    if (IS_NPC(ch))
	return TCL_OK;

    if (objc==1) {
	Tcl_SetObjResult(interp,Tcl_NewIntObj(ch->exp));
    } else {
	if (Tcl_GetIntFromObj(interp,objv[1],&newexp)==TCL_ERROR)
	    return TCL_ERROR;
	logf("[%d] %s gained %d exp.",GET_DESCRIPTOR(ch),NAME(ch),newexp);
	gain_exp(ch,newexp,FALSE);
    }
    return TCL_OK;
}

// tnl
TCL_CHAR_CMD(char_tnl) {
    if (IS_NPC(ch))
	Tcl_SetObjResult(interp,Tcl_NewIntObj(0));
    else
	Tcl_SetObjResult(interp,Tcl_NewIntObj(exp_to_level(ch)));
    return TCL_OK;
}

// expforalevel
TCL_CHAR_CMD(char_expforalevel) {
    if (IS_NPC(ch))
	Tcl_SetObjResult(interp,Tcl_NewIntObj(0));
    else
	Tcl_SetObjResult(interp,Tcl_NewIntObj(exp_per_level(ch,ch->pcdata->points)));
    return TCL_OK;
}

// dex
TCL_CHAR_CMD(char_dex) {
    Tcl_SetObjResult(interp,Tcl_NewIntObj(get_curr_stat(ch,STAT_DEX)));
    return TCL_OK;
}

// int
TCL_CHAR_CMD(char_int) {
    Tcl_SetObjResult(interp,Tcl_NewIntObj(get_curr_stat(ch,STAT_INT)));
    return TCL_OK;
}

// wis
TCL_CHAR_CMD(char_wis) {
    Tcl_SetObjResult(interp,Tcl_NewIntObj(get_curr_stat(ch,STAT_WIS)));
    return TCL_OK;
}

// str
TCL_CHAR_CMD(char_str) {
    Tcl_SetObjResult(interp,Tcl_NewIntObj(get_curr_stat(ch,STAT_STR)));
    return TCL_OK;
}

// con
TCL_CHAR_CMD(char_con) {
    Tcl_SetObjResult(interp,Tcl_NewIntObj(get_curr_stat(ch,STAT_CON)));
    return TCL_OK;
}

// removespell
TCL_CHAR_CMD(char_removespell) {
    int sn=skill_lookup(Tcl_GetString(objv[1]));

    if (sn<0) {
	Tcl_Obj *result;
	result=Tcl_NewStringObj("char removespell: unknown effect '",-1);
	Tcl_AppendStringsToObj(result,Tcl_GetString(objv[1]),"'.",NULL);
	Tcl_SetObjResult(interp,result);
	return TCL_ERROR;
    }

    //
    // This is not very noisy. There is no call to the wear-off-function,
    // there is no message to the person, there is no message to room.
    //
    effect_strip(ch,sn);
    return TCL_OK;
}

// timeskilled
TCL_CHAR_CMD(char_timeskilled) {
    if (IS_NPC(ch))
	Tcl_SetObjResult(interp,Tcl_NewIntObj(ch->pIndexData->killed));
    else
	Tcl_SetObjResult(interp,Tcl_NewIntObj(ch->pcdata->killed));
    return TCL_OK;
}

// unequip
TCL_CHAR_CMD(char_unequip) {
    int wear_loc;
    int i=1;
    bool silent=FALSE;
    static tclOptionList *unequipSwitches[]={"-silent",NULL};
    OBJ_DATA *obj;

    if (objc<2) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("char unequip: location not given",-1));
	return TCL_ERROR;
    }

    while (objc-i>0) {
        int index;
	char *s;

        s=Tcl_GetString(objv[i]);
        if (*s!='-') break;

        if (Tcl_GetIndexFromObj(interp, objv[i], unequipSwitches, "switch", 0, &index) != TCL_OK)
            return TCL_ERROR;

        if (index==0) silent=TRUE;

        i++;
    }

    if (Tcl_GetIndexFromObjStruct(interp,objv[i],wear_loc_options,sizeof(*wear_loc_options),"location",0,&wear_loc)!=TCL_OK)
	return TCL_ERROR;

    wear_loc=wear_loc_options[wear_loc].value;
    if ((wear_loc==NO_FLAG) || (wear_loc==WEAR_NONE)) {
	//Tcl_SetObjResult(interp,Tcl_NewStringObj("char unequip: location not found",-1));
	return TCL_ERROR;
    } else {
	if ((obj=get_eq_char(ch,wear_loc))!=NULL) {
	    if (silent) {
		unequip_char(ch,obj);
		if (wear_loc==WEAR_WIELD &&
		    (obj=get_eq_char(ch,WEAR_SECONDARY))) {
		    unequip_char( ch, obj);
		    equip_char( ch, obj, WEAR_WIELD );
		}
	    } else {
		remove_obj(ch,wear_loc,TRUE,TRUE);
	    }
	}
    }

    return TCL_OK;
}

TCL_CHAR_CMD(char_equip) {
    int i=1;
    bool silent=FALSE;
    bool replace=FALSE;
    static tclOptionList *equipSwitches[]={"-silent","-replace",NULL};
    char *id;
    OBJ_DATA *obj;

    while (objc-i>0) {
        int index;
	char *s;

        s=Tcl_GetString(objv[i]);
        if (*s!='-') break;

        if (Tcl_GetIndexFromObj(interp, objv[i], equipSwitches, "switch", 0, &index) != TCL_OK)
            return TCL_ERROR;

        if (index==0) silent=TRUE;
        if (index==1) replace=TRUE;

        i++;
    }

    logf("char_equip: objc=%d i=%d",objc,i);
    if (objc-i<1) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("char equip: no object given",-1));
	return TCL_ERROR;
    }
    id=Tcl_GetString(objv[i]);
    logf("char_equip: id=%s",id);

    for (obj=tcl_objid(ch->carrying,id,FALSE); obj; obj=tcl_objid(obj->next_content,id,FALSE)) {
	if (obj->wear_loc==WEAR_NONE) break;
    }

    if (obj==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("char equip: no object found",-1));
	return TCL_ERROR;
    }

    wear_obj(ch,obj,replace,!silent);

    return TCL_OK;
}

// hunger [value]
TCL_CHAR_CMD(char_hunger) {
    int value;

    if (IS_NPC(ch))
	return TCL_OK;

    if (objc==1) {
	Tcl_SetObjResult(interp,Tcl_NewIntObj(ch->pcdata->condition[COND_HUNGER]));
    } else {
	if (Tcl_GetIntFromObj(interp,objv[1],&value)==TCL_ERROR)
	    return TCL_ERROR;
	ch->pcdata->condition[COND_HUNGER]=URANGE(0,value,48);
    }
    return TCL_OK;
}

// thirst [value]
TCL_CHAR_CMD(char_thirst) {
    int value;

    if (IS_NPC(ch))
	return TCL_OK;

    if (objc==1) {
	Tcl_SetObjResult(interp,Tcl_NewIntObj(ch->pcdata->condition[COND_THIRST]));
    } else {
	if (Tcl_GetIntFromObj(interp,objv[1],&value)==TCL_ERROR)
	    return TCL_ERROR;
	ch->pcdata->condition[COND_THIRST]=URANGE(0,value,48);
    }
    return TCL_OK;
}

// adrenaline [value]
TCL_CHAR_CMD(char_adrenaline) {
    int value;

    if (IS_NPC(ch))
	return TCL_OK;

    if (objc==1) {
	Tcl_SetObjResult(interp,Tcl_NewIntObj(ch->pcdata->condition[COND_ADRENALINE]));
    } else {
	if (Tcl_GetIntFromObj(interp,objv[1],&value)==TCL_ERROR)
	    return TCL_ERROR;
	ch->pcdata->condition[COND_ADRENALINE]=URANGE(0,value,48);
    }
    return TCL_OK;
}

// drunk [value]
TCL_CHAR_CMD(char_drunk) {
    int value;

    if (IS_NPC(ch))
	return TCL_OK;

    if (objc==1) {
	Tcl_SetObjResult(interp,Tcl_NewIntObj(ch->pcdata->condition[COND_DRUNK]));
    } else {
	if (Tcl_GetIntFromObj(interp,objv[1],&value)==TCL_ERROR)
	    return TCL_ERROR;
	ch->pcdata->condition[COND_DRUNK]=URANGE(0,value,48);
    }
    return TCL_OK;
}

// carries [-id | -vnum | -name] [vnum]
TCL_CHAR_CMD(char_carries) {
    int switchnr=0,vnum=0;
    static tclOptionList *carriesSwitches[]={"-id","-vnum","-name",NULL};
    OBJ_DATA *obj;
    char *ps;
    Tcl_Obj *result;
    int i=1;

    if (objc>1) {
	ps=Tcl_GetString(objv[1]);
	if (ps[0]=='-') {
	    i++;
	    if (Tcl_GetIndexFromObj(interp,objv[1],carriesSwitches,"switch",0,&switchnr)!=TCL_OK)
	    return TCL_ERROR;
	}
    }

    if (i<objc) {
	if (Tcl_GetIntFromObj(interp,objv[2],&vnum)!=TCL_OK)
	    return TCL_ERROR;
    }

    result=Tcl_NewListObj(0,NULL);
    switch (switchnr) {
    case 0: // return a list of ids
	for (obj=ch->carrying;obj!=NULL;obj=obj->next_content) {
	    if (vnum==0 || vnum==obj->pIndexData->vnum) {
		Tcl_ListObjAppendElement(interp,result,Tcl_NewLongObj(obj->id));
	    }
	}
	break;
    case 1: // return a list of vnums
	for (obj=ch->carrying;obj!=NULL;obj=obj->next_content) {
	    if (vnum==0 || vnum==obj->pIndexData->vnum) {
		Tcl_ListObjAppendElement(interp,result,Tcl_NewIntObj(obj->pIndexData->vnum));
	    }
	}
	break;
    case 2: // return a list of names
	for (obj=ch->carrying;obj!=NULL;obj=obj->next_content)
	    if (vnum==0 || vnum==obj->pIndexData->vnum) {
		Tcl_ListObjAppendElement(interp,result,Tcl_NewStringObj(tcl_firstobjname(obj),-1));
	    }
	break;
    }
    Tcl_SetObjResult(interp,result);
    return TCL_OK;
}

// ool_penalty
TCL_CHAR_CMD(char_ool_penalty) {
    Tcl_SetObjResult(interp,Tcl_NewIntObj(get_ool_penalty_state(ch)));
    return TCL_OK;
}

/* canseeobj */
TCL_CHAR_CMD(char_canseeobj) {
    OBJ_DATA *target;

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"char cansee objid");
	return TCL_ERROR;
    }

    if (!(target=tcl_objid(ch->in_room->contents,Tcl_GetString(objv[1]),FALSE))) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("char canseeobj: target not found",-1));
	return TCL_ERROR;
    }

    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(can_see_obj(ch,target)));
    return TCL_OK;
}

/* oload vnum ?level? ?wear|take? */
TCL_CHAR_CMD(char_oload) {
    long vnum,level=-1;
    int wear=-1;
    int i;
    OBJ_INDEX_DATA *pObjIndex;
    OBJ_DATA *obj;
    static tclOptionList *oloadOptions[]={"take","wear","wearnoshow",NULL};

    if (objc<=1 || objc>4) {
	Tcl_WrongNumArgs(interp,0,objv,"char oload vnum ?level? ?wear|take?");
	return TCL_ERROR;
    }

    if (Tcl_GetLongFromObj(interp,objv[1],&vnum)!=TCL_OK)
	return TCL_ERROR;

    level=-1;

    for (i=2;i<objc;i++) {
	if (level==-1 && Tcl_GetLongFromObj(interp,objv[i],&level)==TCL_OK) continue;
	if (wear==-1 && Tcl_GetIndexFromObj(interp,objv[i],oloadOptions,"wearoption", 0, &wear)==TCL_OK) continue;
	return TCL_ERROR;
    }

    if (level==-1) level=get_trust(ch);
    if (wear==-1) wear=0;

    if (level<0 || level>get_trust(ch)) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("char oload: invalid level value",-1));
	return TCL_ERROR;
    }

    if ((pObjIndex=get_obj_index(vnum))==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("char oload: invalid vnum",-1));
	return TCL_ERROR;
    }

    obj=create_object(pObjIndex,level);
    if ( CAN_WEAR(obj, ITEM_TAKE) ) {
	obj_to_char( obj, ch );
	if (wear==1) wear_obj(ch,obj,TRUE,TRUE);
	if (wear==2) wear_obj(ch,obj,TRUE,FALSE);
    } else {
	obj_to_room(obj,ch->in_room);
    }
    op_load_trigger(obj,ch);

    Tcl_SetObjResult(interp,Tcl_NewLongObj(obj->id));
    return TCL_OK;
}

// addeff <where> <type> <level> <duration> <location> <mod> <vector> [<arg1>]
TCL_CHAR_CMD(char_addeff) {
    int i=-1;
    EFFECT_DATA eff;
    static tclOptionList *addeffOptions[]={
      "effects", "immune", "resist", "vuln", "effects2", "skill",
      "affects",(char *)NULL
    };

    if (objc!=8 && objc!=9) {
	Tcl_WrongNumArgs(interp,0,objv,"char addeff <where> <type> <level> <duration> <location> <mod> <vector> [<arg1>]");
	return TCL_ERROR;
    }
    if (!str_cmp(Tcl_GetString(objv[0]),"addaff"))
	bugf("TCL: %s: char addaff DEPRICATED, use char addeff",current_namespace);

    // where = EFFECTS IMMUNE RESIST VULN EFFECTS2
    // type  = gsn
    // duration in ticks
    // location is in apply_options[]
    // mod   = +- number
    // 

    if (Tcl_GetIndexFromObj(interp,objv[1],addeffOptions,"where option",0,&i)!=TCL_OK) {
	return TCL_ERROR;
    }
    switch (i) {
	case 6: // fallthrough
		bugf("TCL: %s: where option 'affects' depricated, use 'effects'",current_namespace);
	case 0: eff.where=TO_EFFECTS;  break;
	case 1: eff.where=TO_IMMUNE;   break;
	case 2: eff.where=TO_RESIST;   break;
	case 3: eff.where=TO_VULN;     break;
	case 4: eff.where=TO_EFFECTS2; break;
	case 5: eff.where=TO_SKILLS;   break;
	default:
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("char addeff: you should not get this error",-1));
	    return TCL_ERROR;
    };
    // where is ok, next: type
    eff.type=skill_lookup(Tcl_GetString(objv[2]));
    if (eff.type==-1 && str_cmp("(unknown)",Tcl_GetString(objv[2]))) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("char addeff: Invalid spell/skill name",-1));
	return TCL_ERROR;
    }
    // ..., next: level
    if (Tcl_GetIntFromObj(interp,objv[3],&eff.level)!=TCL_OK ||
	eff.level<0 || eff.level>100) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("char addeff: Invalid level",-1));
	return TCL_ERROR;
    }
    // ..., next: duration
    if (Tcl_GetIntFromObj(interp,objv[4],&eff.duration)!=TCL_OK ||
	eff.duration<-1) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("char addeff: Invalid duration",-1));
	return TCL_ERROR;
    }
    // ..., next: location
    if ((eff.location=option_find_name(Tcl_GetString(objv[5]),apply_options,TRUE))==NO_FLAG) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("char addeff: Invalid location",-1));
	return TCL_ERROR;
    }
    // ..., next: mod
    if (Tcl_GetIntFromObj(interp,objv[6],&eff.modifier)!=TCL_OK) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("char addeff: Invalid modifier",-1));
	return TCL_ERROR;
    }
    // ..., next: vector
    // where==TO_EFFECTS : bitvector= effect_options
    //        IMM/RES/VULN:		imm_options
    //        TO_EFFECTS2: bitvector= effect2_options
    switch (eff.where) {
	case TO_EFFECTS:   i=option_find_name(Tcl_GetString(objv[7]),effect_options,FALSE);	break;
	case TO_IMMUNE:
	case TO_RESIST:
	case TO_VULN:      i=option_find_name(Tcl_GetString(objv[7]),imm_options,FALSE);	break;
	case TO_EFFECTS2:  i=option_find_name(Tcl_GetString(objv[7]),effect2_options,FALSE);	break;
    }
    if (i==NO_FLAG) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("char addeff: Invalid vector",-1));
	return TCL_ERROR;
    }
    eff.bitvector=i;
    // optional arg1
    if (objc==9)
	if (Tcl_GetIntFromObj(interp,objv[8],&eff.arg1)!=TCL_OK) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("char addeff: Invalid arg1",-1));
	    return TCL_ERROR;
	}
    
    if (!is_affected(ch,eff.type) || 
	ch->affected->type==eff.type)  /* since is_affected returned true ch->affected!=NULL */
	effect_to_char(ch,&eff);

    return TCL_OK;
}

// delproperty <name> <type>
TCL_CHAR_CMD(char_delproperty) {
    char *name;
    int type;
    static tclOptionList *types[]={ "int", "long", "bool", "string", "char", (char *)NULL };

    if (objc!=3) {
	Tcl_WrongNumArgs(interp,0,objv,"char delproperty <name> <type>");
	return TCL_ERROR;
    }

    name=Tcl_GetString(objv[1]);
    if (Tcl_GetIndexFromObj(interp,objv[2],types,"type",0,&type)!=TCL_OK)
	return TCL_ERROR;

    DeleteCharProperty(ch,type+1,name);

    return TCL_OK;
}

/* mana ?new_value? */
TCL_CHAR_CMD(char_mana) {
    if (objc==1) {
	Tcl_SetObjResult(interp,Tcl_NewLongObj(ch->mana));
    } else {
	long newmana;
	if (Tcl_GetLongFromObj(interp,objv[1],&newmana)==TCL_ERROR)
	    return TCL_ERROR;
	ch->mana=newmana;
    }
    return TCL_OK;
}

/* maxmana ?new_value? */
TCL_CHAR_CMD(char_maxmana) {
    if (objc==1) {
	Tcl_SetObjResult(interp,Tcl_NewLongObj(ch->max_mana));
    } else {
	long newmaxmana;
	if (Tcl_GetLongFromObj(interp,objv[1],&newmaxmana)==TCL_ERROR)
	    return TCL_ERROR;
	ch->max_mana=newmaxmana;
    }
    return TCL_OK;
}

// fighting [-id | -vnum | -name]
TCL_CHAR_CMD(char_fighting) {
    int switchnr=0;
    static tclOptionList *fightingSwitches[]={"-id","-vnum","-name",NULL};
    int i=1;

    if (objc>1) {
	if (Tcl_GetString(objv[1])[0]=='-') {
	    i++;
	    if (Tcl_GetIndexFromObj(interp,objv[1],fightingSwitches,"switch",0,&switchnr)!=TCL_OK)
	    return TCL_ERROR;
	}
    }

    switch (switchnr) {
    case 0: // return the ids
	Tcl_SetObjResult(interp,Tcl_NewLongObj(ch->fighting?ch->fighting->id:0));
	break;
    case 1: // return the vnum
	Tcl_SetObjResult(interp,Tcl_NewLongObj((ch->fighting && IS_NPC(ch->fighting))?ch->fighting->pIndexData->vnum:0));
	break;
    case 2: // return the name
	Tcl_SetObjResult(interp,Tcl_NewStringObj(ch->fighting?NAME(ch->fighting):"",-1));
	break;
    }
    return TCL_OK;
}

/* die ?mobid? */
TCL_CHAR_CMD(char_die) {
    CHAR_DATA *killer;
    OBJ_DATA *corpse;

    if (objc<=1) {
	killer=NULL;
    } else {
	killer=tcl_charid_char(ch,Tcl_GetString(objv[1]),NULL);
	if (!killer) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("char die: specified killer not found.",-1));
	    return TCL_ERROR;
	}
    }
    if (killer) {
	char buf[MSL];
	sprintf( buf, "$N got toasted by %s at %s [room %d]",
	    (IS_NPC(killer) ? killer->short_descr : killer->name),
	    ch->in_room->name, ch->in_room->vnum);

	if (IS_NPC(ch))
	    wiznet(WIZ_MOBDEATHS,0,ch,NULL,"%s",buf);
	else
	    wiznet(WIZ_DEATHS,0,ch,NULL,"%s",buf);
	if (IS_PC(ch))
	    announce( ch, "%s got killed by %s at %s.",
		ch->name,
		IS_NPC(killer)?killer->short_descr:killer->name,
		ch->in_room->name);
    } else {
	char buf[MSL];

	sprintf( buf, "$N died in a deathtrap at %s [room %d]",
	    ch->in_room->name, ch->in_room->vnum);
	if (IS_NPC(ch))
	    wiznet(WIZ_MOBDEATHS,0,ch,NULL,"%s",buf);
	else
	    wiznet(WIZ_DEATHS,0,ch,NULL,"%s",buf);

	if (IS_PC(ch))
	    announce(ch,"%s died in %s.",ch->name,ch->in_room->name);
    }
    corpse=raw_kill(ch,killer);
    if (corpse) Tcl_SetObjResult(interp,Tcl_NewLongObj(corpse->id));
    else Tcl_SetObjResult(interp,Tcl_NewLongObj(0));

    return TCL_OK;
}

/* wimpy ?new_value? */
TCL_CHAR_CMD(char_wimpy) {
    if (objc==1) {
	Tcl_SetObjResult(interp,Tcl_NewIntObj(ch->wimpy));
    } else {
	int newwimpy;
	if (Tcl_GetIntFromObj(interp,objv[1],&newwimpy)==TCL_ERROR)
	    return TCL_ERROR;
	ch->wimpy=newwimpy;
    }
    return TCL_OK;
}

/* objhere name */
TCL_CHAR_CMD(char_objhere) {
    char *name;
    OBJ_DATA *obj;

    name = Tcl_GetString(objv[1]);
    obj = get_obj_here(ch, name);
    if (obj)
	Tcl_SetObjResult(interp,Tcl_NewLongObj(obj->id));
    else
	Tcl_SetObjResult(interp,Tcl_NewLongObj(0));
    return TCL_OK;
}

TCL_CHAR_CMD(char_listeff) {
    EFFECT_DATA *eff;
    Tcl_Obj *result;

    if (!str_cmp(Tcl_GetString(objv[0]),"isaff"))
	bugf("TCL: %s: char listaff DEPRICATED, use char listeff",current_namespace);

    result=Tcl_NewListObj(0,NULL);

    for (eff=ch->affected;eff;eff=eff->next) {
        Tcl_ListObjAppendElement(interp,result,Effect_to_ListObj(eff));
    }

    Tcl_SetObjResult(interp,result);

    return TCL_OK;
}

TCL_CHAR_CMD(char_selectobj) {
    int switchnr=0;
    int i=1;
    int counted=0;
    static tclOptionList *selectobjSwitches[]={"-roomfirst","-charfirst","-room","-char","-equipment","-inventory",NULL};
    #define MAX_PREF 3
    enum {Snone,Sroom,Schar,Seq,Sinv} preference[MAX_PREF]={Snone,Snone,Snone};
    int prefs=0;
    char *needle;

    while (objc>i && Tcl_GetString(objv[i])[0]=='-') {
	if (Tcl_GetIndexFromObj(interp,objv[i],selectobjSwitches,"switch",0,&switchnr)!=TCL_OK)
	    return TCL_ERROR;
	i++;
	switch (switchnr) {
	    case 0: // roomfirst
		if (prefs!=0) {
		    Tcl_SetObjResult(interp,Tcl_NewStringObj("char selectobj: illegal combination of switches.",-1));
		    return TCL_ERROR;
		}
		preference[0]=Sroom;
		preference[1]=Sinv;
		preference[2]=Seq;
		prefs=MAX_PREF;
		break;
	    case 1: // charfirst
		if (prefs!=0) {
		    Tcl_SetObjResult(interp,Tcl_NewStringObj("char selectobj: illegal combination of switches.",-1));
		    return TCL_ERROR;
		}
		preference[0]=Sinv;
		preference[1]=Seq;
		preference[2]=Sroom;
		prefs=MAX_PREF;
		break;
	    case 2: // room
		if (prefs>=MAX_PREF) {
		    Tcl_SetObjResult(interp,Tcl_NewStringObj("char selectobj: illegal combination of switches.",-1));
		    return TCL_ERROR;
		}
		preference[prefs++]=Sroom;
		break;
	    case 3: // char
		if (prefs>=MAX_PREF) {
		    Tcl_SetObjResult(interp,Tcl_NewStringObj("char selectobj: illegal combination of switches.",-1));
		    return TCL_ERROR;
		}
		preference[prefs++]=Schar;
		break;
	    case 4: // equipment
		if (prefs>=MAX_PREF) {
		    Tcl_SetObjResult(interp,Tcl_NewStringObj("char selectobj: illegal combination of switches.",-1));
		    return TCL_ERROR;
		}
		preference[prefs++]=Seq;
		break;
	    case 5: // inventory
		if (prefs>=MAX_PREF) {
		    Tcl_SetObjResult(interp,Tcl_NewStringObj("char selectobj: illegal combination of switches.",-1));
		    return TCL_ERROR;
		}
		preference[prefs++]=Sinv;
		break;
	}
    }
    if (prefs==0) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("char selectobj: No search space specified.",-1));
	return TCL_ERROR;
    }
    if (objc<=i) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("char selectobj: No search string specified.",-1));
	return TCL_ERROR;
    }
    needle=Tcl_GetString(objv[i]);
    for (i=0;i<MAX_PREF;i++) {
	OBJ_DATA *obj=NULL;

	switch (preference[i]) {
	    case Sroom: obj=get_obj_list_numbered(ch,needle,ch->in_room->contents,&counted); break;
	    case Schar: obj=get_obj_list_numbered(ch,needle,ch->carrying,&counted); break;
	    case Seq:   obj=get_obj_wear_numbered(ch,needle,&counted); break;
	    case Sinv:  obj=get_obj_carry_numbered(ch,needle,ch,&counted); break;
	    default: break;
	}
	if (obj) {
	    progData->lastObj=obj;
	    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(1));
	    return TCL_OK;
	}
    }
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(0));
    return TCL_OK;
    #undef MAX_PREF
}
