//
// $Id: act_food.c,v 1.5 2006/08/25 09:43:19 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"

void do_fill( CHAR_DATA *ch, char *argument ) {
    char skin[MAX_INPUT_LENGTH],source[MIL];
    char buf[MAX_STRING_LENGTH];
    OBJ_DATA *obj;
    OBJ_DATA *fountain;
    bool found;

    argument=one_argument( argument, skin );
    argument=one_argument( argument, source );

    if ( skin[0] == '\0' ) {
        send_to_char( "Fill what?\n\r", ch );
        return;
    }

    if ( ( obj = get_obj_carry( ch, skin, ch ) ) == NULL ) {
        send_to_char( "You do not have that item.\n\r", ch );
        return;
    }

    found = FALSE;
    if (source[0]) {
        if ((fountain=get_obj_here(ch,source))==NULL) {
            send_to_char("That's not here!\n\r",ch);
            return;
        }
        if (fountain->item_type!=ITEM_FOUNTAIN) {
            send_to_char("That's no fountain!\n\r",ch);
            return;
        }
    } else {
        for ( fountain = ch->in_room->contents; fountain != NULL;
              fountain = fountain->next_content ) {
            if ( fountain->item_type == ITEM_FOUNTAIN ) {
                found = TRUE;
                break;
            }
        }
        if ( !found ) {
            send_to_char( "There is no fountain here!\n\r", ch );
            return;
        }
    }

    if ( obj->item_type != ITEM_DRINK_CON ) {
        send_to_char( "You can't fill that.\n\r", ch );
        return;
    }

    if (!IS_NPC(ch) && obj->level>ch->level) {
        act( "The $d refuses to be filled by you.",
             ch, NULL, obj->name,TO_CHAR);
        return;
    }

    if ( obj->value[1] != 0 && obj->value[2] != fountain->value[2] )
    {
        send_to_char( "There is already another liquid in it.\n\r", ch );
        return;
    }

    if ( obj->value[1] >= obj->value[0] )
    {
        act( "$p is full.", ch ,obj,NULL,TO_CHAR);
        return;
    }
    if ( obj->value[0] == -1) {
        act("$p doesn't need filling.", ch, obj, NULL, TO_CHAR);
        return;
    }

    if (IS_PC(ch))
        STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);

    sprintf(buf,"You fill $p with %s from $P.",
            liq_table[fountain->value[2]].liq_name);
    act( buf, ch, obj,fountain, TO_CHAR );
    sprintf(buf,"$n fills $p with %s from $P.",
            liq_table[fountain->value[2]].liq_name);
    act(buf,ch,obj,fountain,TO_ROOM);
    obj->value[2] = fountain->value[2];
    obj->value[1] = obj->value[0];
    return;
}

void do_pour (CHAR_DATA *ch, char *argument)
{
    char arg[MAX_STRING_LENGTH],buf[MAX_STRING_LENGTH];
    OBJ_DATA *out, *in;
    CHAR_DATA *vch = NULL;
    int amount;

    argument = one_argument(argument,arg);

    if (arg[0] == '\0' || argument[0] == '\0')
    {
        send_to_char("Pour what into what?\n\r",ch);
        return;
    }

    if ((out = get_obj_carry(ch,arg, ch)) == NULL)
    {
        send_to_char("You don't have that item.\n\r",ch);
        return;
    }

    if (out->item_type != ITEM_DRINK_CON)
    {
        send_to_char("That's not a drink container.\n\r",ch);
        return;
    }

    if (!str_cmp(argument,"out"))
    {
        if (!IS_NPC(ch) && out->level>ch->level) {
            act("The $d doesn't want to be poured out by you.",
                ch,NULL,out->name,TO_CHAR);
            return;
        }
        if (out->value[1] == 0)
        {
            send_to_char("It's already empty.\n\r",ch);
            return;
        }
        if (out->value[0] == -1) {
            send_to_char("That would not be wise.\n\r",ch);
            return;
        }

        if (IS_PC(ch))
            STR_SET_BIT(ch->pcdata->usedthatobject,out->pIndexData->vnum);

        out->value[1] = 0;
        out->value[3] = 0;
        sprintf(buf,"You invert $p, spilling %s all over the ground.",
                liq_table[out->value[2]].liq_name);
        act(buf,ch,out,NULL,TO_CHAR);

        sprintf(buf,"$n inverts $p, spilling %s all over the ground.",
                liq_table[out->value[2]].liq_name);
        act(buf,ch,out,NULL,TO_ROOM);
        return;
    }

    if ((in = get_obj_here(ch,argument)) == NULL)
    {
        vch = get_char_room(ch,argument);

        if (vch == NULL)
        {
            send_to_char("Pour into what?\n\r",ch);
            return;
        }

        in = get_eq_char(vch,WEAR_HOLD);

        if (in == NULL)
        {
            send_to_char("They aren't holding anything.",ch);
            return;
        }
    }

    if (in->item_type != ITEM_DRINK_CON)
    {
        send_to_char("You can only pour into other drink containers.\n\r",ch);
        return;
    }

    if (in == out)
    {
        send_to_char("You cannot change the laws of physics!\n\r",ch);
        return;
    }

    if (!IS_NPC(ch) && in->level>ch->level) {
        act("The $d won't accept any liquid poured in by you.",
            ch,NULL,in->name,TO_CHAR);
        return;
    }
    if (!IS_NPC(ch) && out->level>ch->level) {
        act("The $d doesn't want to be poured out by you.",
            ch,NULL,out->name,TO_CHAR);
        return;
    }

    if (in->value[1] != 0 && in->value[2] != out->value[2])
    {
        send_to_char("They don't hold the same liquid.\n\r",ch);
        return;
    }

    if (out->value[1] == 0)
    {
        act("There's nothing in $p to pour.",ch,out,NULL,TO_CHAR);
        return;
    }

    if (in->value[1] >= in->value[0])
    {
        act("$p is already filled to the top.",ch,in,NULL,TO_CHAR);
        return;
    }

    if (out->value[0]==-1) {
        amount = in->value[0] - in->value[1];
        in->value[1] += amount;
        in->value[2] = out->value[2];
    } else {
        amount = UMIN(out->value[1],in->value[0] - in->value[1]);

        in->value[1] += amount;
        out->value[1] -= amount;
        in->value[2] = out->value[2];
    }

    if (vch == NULL)
    {
        sprintf(buf,"You pour %s from $p into $P.",
                liq_table[out->value[2]].liq_name);
        act(buf,ch,out,in,TO_CHAR);
        sprintf(buf,"$n pours %s from $p into $P.",
                liq_table[out->value[2]].liq_name);
        act(buf,ch,out,in,TO_ROOM);
    } else {
        sprintf(buf,"You pour some %s for $N.",
                liq_table[out->value[2]].liq_name);
        act(buf,ch,NULL,vch,TO_CHAR);
        sprintf(buf,"$n pours you some %s.",
                liq_table[out->value[2]].liq_name);
        act(buf,ch,NULL,vch,TO_VICT);
        sprintf(buf,"$n pours some %s for $N.",
                liq_table[out->value[2]].liq_name);
        act(buf,ch,NULL,vch,TO_NOTVICT);
    }
    if (IS_PC(ch)) {
        STR_SET_BIT(ch->pcdata->usedthatobject,in->pIndexData->vnum);
        STR_SET_BIT(ch->pcdata->usedthatobject,out->pIndexData->vnum);
    }
}

void apply_quest_effect(CHAR_DATA *ch,OBJ_DATA *obj) {
    int level,i;

    if (IS_NPC(ch)) return;

    if (obj->item_type!=ITEM_QUEST) return;

    /* Log these kind of things */
    logf("[%d] QUEST: %s has consumed quest object %d.",
         GET_DESCRIPTOR(ch),ch->name,obj->pIndexData->vnum);
    wiznet(WIZ_QUESTS,0,ch,obj,"QUEST: $N has consumed quest object %d: $p.",
           obj->pIndexData->vnum);

    if (IS_IMMORTAL(ch)) {
        send_to_char("These quest objects are not for immortals.\n\r", ch );
        return;
    }

    if (obj->value[2]<=0) return; /* Nothing to change */

    if (IS_SET(obj->pIndexData->area->area_flags,AREA_UNFINISHED)) {
        send_to_char("Quest items from unfinished area's don't work.\n\r", ch);
        return;
    }

    switch(obj->value[1]) {
    case QUESTEFF_LEVEL:
        level=ch->level+obj->value[2];
        if(level>LEVEL_HERO) level=LEVEL_HERO;
        send_to_char( "**** OOOOHHHHHHHHHH  YYYYEEEESSS ****\n\r", ch );
        for ( i = ch->level ; i < level; i++ )
        {
            ch->level += 1;
            advance_level( ch,TRUE);
        }
        sprintf_to_char(ch,"You are now level %d.\n\r",ch->level);
        ch->exp   = exp_per_level(ch,ch->pcdata->points)
                * UMAX( 1, ch->level );
        break;
    case QUESTEFF_PRACTICES:
        ch->practice+=obj->value[2];
        sprintf_to_char(ch, "You have gained %d practice%s.\n\r",
                        obj->value[2],obj->value[2]==1?"":"s");
        break;
    case QUESTEFF_TRAINS:
        ch->train+=obj->value[2];
        sprintf_to_char(ch, "You have gained %d training session%s.\n\r",
                        obj->value[2],obj->value[2]==1?"":"s");
        break;
    case QUESTEFF_QUESTPOINTS:
        sprintf_to_char(ch, "You receive %d questpoint%ss.\n\r",
                        obj->value[2], (obj->value[2]==1)? "" : "s" );
        if (IS_PC(ch))
            ch->pcdata->questpoints+=obj->value[2];
        break;
    case QUESTEFF_EXP:
        sprintf_to_char(ch, "You receive %d experience point%s.\n\r", obj->value[2], (obj->value[2]==1)? "" : "s" );
        gain_exp( ch, obj->value[2], TRUE );
        break;
    case QUESTEFF_LONER:
        ch->pcdata->clan_recruit=get_clan_by_name("Loner");
        send_to_char("You now have recruited yourself for the loner clan.\n\r"
                     "Type '{Wrecruit accept{x' to join the clan.\n\r"
                     "Type '{Wrecruit{x' to cancel the recruiting procedure.\n\r",
                     ch);
        break;

    default:
        bugf("Apply_quest_effect: Unknown quest effect (%d) for %s",
             obj->value[1],NAME(ch));
        break;
    }
    save_char_obj(ch);
}

void do_drink( CHAR_DATA *ch, char *argument ) {
    char arg[MAX_INPUT_LENGTH];
    OBJ_DATA *obj;
    int amount;
    int liquid;

    one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
        for ( obj = ch->in_room->contents; obj; obj = obj->next_content )
        {
            if ( obj->item_type == ITEM_FOUNTAIN )
                break;
        }

        if ( obj == NULL )
        {
            send_to_char( "Drink what?\n\r", ch );
            return;
        }
    }
    else
    {
        if ( ( obj = get_obj_here( ch, arg ) ) == NULL )
        {
            send_to_char( "You can't find it.\n\r", ch );
            return;
        }
    }

    if ( !IS_NPC(ch) && ch->pcdata->condition[COND_DRUNK] > 10 )
    {
        send_to_char( "You fail to reach your mouth.  *Hic*\n\r", ch );
        return;
    }

    switch ( obj->item_type )
    {
    default:
        send_to_char( "You can't drink from that.\n\r", ch );
        return;

    case ITEM_FOUNTAIN:
        if ( obj->value[1] == 0 ) {
            send_to_char( "It is already empty.\n\r", ch );
            return;
        }

        if ( ( liquid = obj->value[2] )  < 0 ) {
            bugf("Do_drink: bad liquid number %d for %s.", liquid,NAME(ch));
            liquid = obj->value[2] = 0;
        }
        amount = liq_table[liquid].liq_effect[4] * 3;
        if (obj->value[1]>0) amount = UMIN(amount, obj->value[1]);
        break;

    case ITEM_DRINK_CON:
        if (!IS_NPC(ch) && obj->level>ch->level) {
            act("The $d refuses to release its liquid to you.",
                ch, NULL, obj->name, TO_CHAR );
            return;
        }

        if ( obj->value[1] == 0 ) {
            send_to_char( "It is already empty.\n\r", ch );
            return;
        }

        if ( ( liquid = obj->value[2] )  < 0 ) {
            bugf( "Do_drink: bad liquid number %d for %s.", liquid,NAME(ch));
            liquid = obj->value[2] = 0;
        }

        amount = liq_table[liquid].liq_effect[4];
        if (obj->value[1]>0) amount = UMIN(amount, obj->value[1]);
        break;
    case ITEM_QUEST:
        if(obj->value[0]!=QUESTCONS_DRINK)
        {
            send_to_char( "You can't drink from that.\n\r", ch );
            return;
        }
        if (obj_is_owned_by_another(ch,obj)) {
            act("$p is owned by somebody else.",ch,obj,NULL,TO_CHAR);
            return;
        }

        if (IS_PC(ch))
            STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);
        act( "$n drinks from $p.", ch, obj, NULL, TO_ROOM );
        act( "You drink from $p.", ch, obj, NULL, TO_CHAR );
        apply_quest_effect(ch,obj);
        act( "$p magically dissapears.",ch,obj,NULL,TO_ALL);
        extract_obj(obj);
        return;
    }
    if (!IS_NPC(ch) && !IS_IMMORTAL(ch)
            &&  ch->pcdata->condition[COND_FULL] > 45)
    {
        send_to_char("You're too full to drink more.\n\r",ch);
        return;
    }

    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREDRINK ) )
        if (op_predrink_trigger( obj, ch )==FALSE)
            return;
    if (!IS_VALID(obj))
        return;

    act( "$n drinks $T from $p.",
         ch, obj, liq_table[liquid].liq_name, TO_ROOM );
    act( "You drink $T from $p.",
         ch, obj, liq_table[liquid].liq_name, TO_CHAR );
    if (IS_PC(ch))
        STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);

    gain_condition( ch, COND_DRUNK,
                    amount * liq_table[liquid].liq_effect[COND_DRUNK] / 36 );
    if ( !IS_NPC(ch) && ch->pcdata->condition[COND_DRUNK]  > 10 )
        send_to_char( "You feel drunk.\n\r", ch );
    if (!IS_AFFECTED(ch,EFF_THIRST)) {
        gain_condition( ch, COND_FULL,
                        amount * liq_table[liquid].liq_effect[COND_FULL] / 4 );
        if ( !IS_NPC(ch) && ch->pcdata->condition[COND_FULL]   > 40 )
            send_to_char( "You are full.\n\r", ch );

        gain_condition( ch, COND_THIRST,
                        amount * liq_table[liquid].liq_effect[COND_THIRST] / 10 );
        if ( !IS_NPC(ch) && ch->pcdata->condition[COND_THIRST] > 40 )
            send_to_char( "Your thirst is quenched.\n\r", ch );
    }
    if (!IS_AFFECTED(ch,EFF_HUNGER)) {
        gain_condition(ch, COND_HUNGER,
                       amount * liq_table[liquid].liq_effect[COND_HUNGER] / 2 );
    }

    if ( obj->value[3] != 0 )
    {
        /* The drink was poisoned ! */
        EFFECT_DATA ef;

        act( "$n chokes and gags.", ch, NULL, NULL, TO_ROOM );
        send_to_char( "You choke and gag.\n\r", ch );

        ZEROVAR(&ef,EFFECT_DATA);
        ef.where     = TO_EFFECTS;
        ef.type      = gsn_poison;
        ef.level	 = number_fuzzy(amount);
        ef.duration  = 3 * amount;
        ef.location  = APPLY_NONE;
        ef.modifier  = 0;
        ef.bitvector = EFF_POISON;
        ef.casted_by = 0;
        effect_join( ch, &ef );
    }

    if (obj->value[0] > 0)
        obj->value[1] -= amount;

    if ( OBJ_HAS_TRIGGER( obj, OTRIG_DRINK ) )
        op_drink_trigger( obj, ch );
    if (!IS_VALID(obj))
        return;

    return;
}



void do_eat( CHAR_DATA *ch, char *argument ) {
    char arg[MAX_INPUT_LENGTH];
    OBJ_DATA *obj;
    bool b;

    one_argument( argument, arg );
    if ( arg[0] == '\0' )
    {
        send_to_char( "Eat what?\n\r", ch );
        return;
    }

    if ( ( obj = get_obj_carry( ch, arg, ch ) ) == NULL )
    {
        send_to_char( "You do not have that item.\n\r", ch );
        return;
    }

    if ( !IS_IMMORTAL(ch) )
    {
        if ( obj->item_type != ITEM_FOOD && obj->item_type != ITEM_PILL
             && (obj->item_type!=ITEM_QUEST || obj->value[0]!=QUESTCONS_EAT))
        {
            send_to_char( "That's not edible.\n\r", ch );
            return;
        }

        if ( !IS_NPC(ch) && obj->item_type!=ITEM_QUEST
             && (ch->pcdata->condition[COND_FULL] > 40 ) )
        {
            send_to_char( "You are too full to eat more.\n\r", ch );
            return;
        }

        if (obj->item_type==ITEM_QUEST && obj_is_owned_by_another(ch,obj)) {
            act("$p is owned by somebody else.",ch,obj,NULL,TO_CHAR);
            return;
        }

        if (obj->level>ch->level) {
            send_to_char("That wouldn't agree with you.\n\r",ch);
            return;
        }
    }

    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREEAT ) )
        if (op_preeat_trigger( obj, ch )==FALSE)
            return;
    if (!IS_VALID(obj))
        return;

    act( "$n eats $p.",  ch, obj, NULL, TO_ROOM );
    if (IS_AFFECTED(ch,EFF_HALLUCINATION))
        act( "That was a primo $p.", ch, obj, NULL, TO_CHAR );
    else
        act( "You eat $p.", ch, obj, NULL, TO_CHAR );
    if (IS_PC(ch))
        STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);

    switch ( obj->item_type )
    {

    case ITEM_FOOD:
        if ( !IS_NPC(ch) )
        {
            int condition;


            condition = ch->pcdata->condition[COND_HUNGER];
            if (!IS_AFFECTED(ch,EFF_HUNGER)) {
                gain_condition( ch, COND_FULL, obj->value[0] );
                gain_condition( ch, COND_HUNGER, obj->value[1]);
                if ( condition == 0 && ch->pcdata->condition[COND_HUNGER] > 0 )
                    send_to_char( "You are no longer hungry.\n\r", ch );
                else if ( ch->pcdata->condition[COND_FULL] > 40 )
                    send_to_char( "You are full.\n\r", ch );
            }
        }

        b=FALSE;
        GetObjectProperty(obj,PROPERTY_BOOL,"fortunecookie",&b);
        if (b) {
            FILE *fin;
            char s[MAX_STRING_LENGTH];

            if ((fin=popen("/usr/games/fortune","r"))==NULL) {
                bugf("Can't locate /usr/games/fortune");
            } else {
                act("$p has a piece of paper inside. It reads:\n\r",
                    ch,obj,NULL,TO_CHAR);
                while (!feof(fin)) {
                    bzero(s,sizeof(s));
                    fread(s,sizeof(s)-1,1,fin);
                    send_to_char(s,ch);
                }
                pclose(fin);
            }
        }

        if ( obj->value[3] != 0 )
        {
            /* The food was poisoned! */
            EFFECT_DATA ef;

            act( "$n chokes and gags.", ch, 0, 0, TO_ROOM );
            if (IS_AFFECTED(ch,EFF_HALLUCINATION))
                send_to_char("You feel rather trippy.\n\r",ch);
            else
                send_to_char( "You choke and gag.\n\r", ch );

            ZEROVAR(&ef,EFFECT_DATA);
            ef.where	 = TO_EFFECTS;
            ef.type      = gsn_poison;
            ef.level 	 = number_fuzzy(obj->value[0]);
            ef.duration  = 2 * obj->value[0];
            ef.location  = APPLY_NONE;
            ef.modifier  = 0;
            ef.bitvector = EFF_POISON;
            ef.casted_by = 0;
            effect_join( ch, &ef );
        }
        break;

    case ITEM_PILL:
        obj_cast_spell( obj->value[1], obj->value[0], ch, ch, NULL, "" );
        obj_cast_spell( obj->value[2], obj->value[0], ch, ch, NULL, "" );
        obj_cast_spell( obj->value[3], obj->value[0], ch, ch, NULL, "" );
        break;
    case ITEM_QUEST:
        apply_quest_effect(ch,obj);
        break;
    }

    if (OBJ_HAS_TRIGGER(obj,OTRIG_EAT))
        op_eat_trigger(obj,ch);
    if (!IS_VALID(obj))
        return;

    extract_obj( obj );
    return;
}
