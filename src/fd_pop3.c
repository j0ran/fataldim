//
/* $Id: fd_pop3.c,v 1.17 2001/08/15 02:20:39 edwin Exp $ */
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifdef HAS_POP3

#include "merc.h"
#include "color.h"

DECLARE_POP3_FUN(pop3_quit);
DECLARE_POP3_FUN(pop3_user);
DECLARE_POP3_FUN(pop3_pass);
DECLARE_POP3_FUN(pop3_list);
DECLARE_POP3_FUN(pop3_retr);
DECLARE_POP3_FUN(pop3_noop);
DECLARE_POP3_FUN(pop3_stat);
DECLARE_POP3_FUN(pop3_dele);
DECLARE_POP3_FUN(pop3_rset);
DECLARE_POP3_FUN(pop3_last);

#define FROM		"From: "
#define TO		"To: "
#define SUBJECT		"Subject: "
#define DATE		"Date: "
#define NOTETYPE	"X-NoteType: "

extern bool hide_note (CHAR_DATA *ch, NOTE_DATA *pnote);
void update_read(CHAR_DATA *, NOTE_DATA *);

static char *pop3_unknown_command_user_rsp="-ERR Invalid command; valid commands: USER, QUIT.\r\n";
static char *pop3_unknown_command_pass_rsp="-ERR Invalid command; valid commands: PASS, QUIT.\r\n";
static char *pop3_unknown_command_rsp="-ERR Invalid command; valid commands: QUIT, LIST, RETR, STAT, DELE, NOOP.\r\n";
static char *pop3_invalid_userpass_rsp="-ERR invalid usercode or password, please try again.\r\n";
       char *pop3_greeting_msg="+OK " MUDHOST " POP3 V0.5 ready.\r\n";
static char *pop3_signoff_msg="+OK " MUDHOST " POP3 V0.5 shutdown.\r\n";
static char *pop3_user_rsp="+OK please send PASS command.\r\n";
static char *pop3_ready_msg="+OK %d messages ready.\r\n";
static char *pop3_list_msg="+OK %d messages (%d octets)\r\n";
static char *pop3_outofrange_rsp="-ERR invalid message; number out of range.\r\n";
static char *pop3_errdelete_rsp="-ERR message %d has been marked for deletion/readed.\r\n";
static char *pop3_marked_rsp="+OK message %d marked for deletion/readed.\r\n";
static char *pop3_last_rsp="+OK %d\r\n";

static struct pop3_cmd {
    char *name;
    POP3_FUN *pop3_fun;
    int state;
} pop3_cmds[]={
    { "USER", pop3_user, CON_POP3_GET_USER },
    { "QUIT", pop3_quit, CON_POP3_GET_USER },
    { "PASS", pop3_pass, CON_POP3_GET_PASS },
    { "QUIT", pop3_quit, CON_POP3_GET_PASS },
    { "QUIT", pop3_quit, CON_POP3_CONNECTED },
    { "LIST", pop3_list, CON_POP3_CONNECTED },
    { "RETR", pop3_retr, CON_POP3_CONNECTED },
    { "NOOP", pop3_noop, CON_POP3_CONNECTED },
    { "STAT", pop3_stat, CON_POP3_CONNECTED },
    { "DELE", pop3_dele, CON_POP3_CONNECTED },
    { "RSET", pop3_rset, CON_POP3_CONNECTED },
    { "LAST", pop3_last, CON_POP3_CONNECTED },
    { NULL, NULL }
};

void run_pop3_server( DESCRIPTOR_DATA *d )
{
    char cmd[MAX_INPUT_LENGTH],*argument,*msg;
    struct pop3_cmd *command;

    argument=d->incomm;
    argument=one_argument(argument,cmd);

    if(*cmd) for(command=pop3_cmds;command->name;command++)
    {
        if(!str_cmp(command->name,cmd) && command->state==d->connected)
        {
	    wiznet(WIZ_POP3_DEBUG,0,NULL,NULL,
		"POP3-debug: Got command: '%s', parameters '%s'",
		cmd,argument);
            (*command->pop3_fun)(d,argument);
            return;
        }
    }
    wiznet(WIZ_POP3,0,NULL,NULL,
	"Got unknown command: '%s', parameters '%s'",
	cmd,argument);

    switch(d->connected) {
        case CON_POP3_GET_USER: msg=pop3_unknown_command_user_rsp;break;
        case CON_POP3_GET_PASS: msg=pop3_unknown_command_pass_rsp;break;
        default:                msg=pop3_unknown_command_rsp;break;
    }
    write_to_buffer(d,msg,0);
}

int note_to_noteref(DESCRIPTOR_DATA *d,int type) {
    NOTE_REF *noteref,*lastref;
    NOTE_DATA	*note;
    int count=0;
    char *text;
    char buf[MAX_STRING_LENGTH];
    char *p;

    if(d->pop3_char->pcdata->noteref)
        for(lastref=d->pop3_char->pcdata->noteref;lastref->next;lastref=lastref->next);
    else lastref=NULL;

    note=notes_table[type].list;
    while (note!=NULL) {
        if (!hide_note(d->pop3_char,note)) {
            noteref=new_note_ref();
            noteref->next=NULL;
            noteref->note=note;

	    noteref->size=0;
            noteref->size+=strlen(FROM)+2+strlen(noteref->note->sender);
            noteref->size+=strlen(TO)+2+strlen(noteref->note->to_list);
            noteref->size+=strlen(SUBJECT)+3+strlen(notes_table[type].name)+
			    2+strlen(noteref->note->subject);
            noteref->size+=strlen(DATE)+2+strlen(noteref->note->date);
            noteref->size+=strlen(NOTETYPE)+2+strlen(notes_table[type].name);
	    noteref->size+=2;

	    text=noteref->note->text;
	    p=buf;
	    while (*text) {
		/* Get one line */
		if(*text=='.') *(p++)='.';
		while(*text && *text!='\n' && *text!='\r') *(p++)=*(text++);
		*(p++)='\r';
		*(p++)='\n';
		*(p++)='\0';

		noteref->size+=strlen(buf);

		p=buf;
		if(*text) text++;
		if(*text=='\n' || *text=='\r') text++;
	    }

            noteref->deleted=FALSE;
            if(lastref) lastref->next=noteref;
            else d->pop3_char->pcdata->noteref=noteref;
            lastref=noteref;
            count++;
        }
        note=note->next;
    }

    return count;
}

/* The server commands. */

void pop3_quit( DESCRIPTOR_DATA *d, char *argument )
{
    /* Set all deleted messages as read. */
    NOTE_REF *noteref;
    DESCRIPTOR_DATA *dlist;
    CHAR_DATA *ch;

    if(d->connected==CON_POP3_CONNECTED)
    {
      /* All this code could go wrong, but must of the time it goes
       * okay. If it goes wrong, the messages won't be marked as read.
       */

      for(dlist=descriptor_list;dlist;dlist=dlist->next)
      {
          if(dlist!=d
          && dlist->character!=NULL
          && dlist->connected != CON_GET_NAME
          && dlist->connected != CON_GET_OLD_PASSWORD
  	  &&   !str_cmp( d->pop3_char->name, dlist->original
	           ? dlist->original->name : dlist->character->name ) )
	  {
	      /* This is easy.. Just update his structure. */
	      if(dlist->original) ch=dlist->original;
	      else ch=dlist->character;

              for(noteref=d->pop3_char->pcdata->noteref;noteref;noteref=noteref->next)
              {
                  if(noteref->note && noteref->deleted)
                      update_read(ch,noteref->note);
              }

              break;
	  }
      }

      if(dlist==NULL)
      {
          /* Load character, do stuff and save it again... */
          /* It could go wrong, but not often. */
          if(!load_char_obj(d,d->pop3_char->name))
          {
              free_char(d->character);
              d->character=NULL;
          }
          else
          {
              ch=d->character;
              for(noteref=d->pop3_char->pcdata->noteref;noteref;noteref=noteref->next)
              {
                  if(noteref->note && noteref->deleted)
                      update_read(ch,noteref->note);
              }
              save_char_obj(ch);
              free_char(ch);
              d->character=NULL;
          }
      }
    }

    write_to_buffer(d,pop3_signoff_msg,0);
    close_socket(d);
}

void pop3_user( DESCRIPTOR_DATA *d, char *argument )
{
    if(!load_char_obj( d, argument ))
    {
        free_char(d->character);
        d->character=NULL;
    }
    write_to_buffer(d, pop3_user_rsp,0);
    d->pop3_char=d->character;
    d->character=NULL;
    d->connected=CON_POP3_GET_PASS;
}

void pop3_pass( DESCRIPTOR_DATA *d, char *argument )
{
    int		count,type;
    char	buf[100];
    DESCRIPTOR_DATA *dlist;

    if(d->pop3_char==NULL
    || strcmp(crypt(argument,d->pop3_char->pcdata->pwd),d->pop3_char->pcdata->pwd))
    {
        write_to_buffer(d,pop3_invalid_userpass_rsp,0);
        free_char(d->pop3_char);
        d->pop3_char=NULL;
        d->connected=CON_POP3_GET_USER;
        return;
    }

    /* Check if player is logged in already, if so copy the note
       read data to the pop3_char */
    for (dlist=descriptor_list;dlist;dlist=dlist->next) {
        if(dlist!=d
        && dlist->character!=NULL
        && dlist->connected != CON_GET_NAME
        && dlist->connected != CON_GET_OLD_PASSWORD
	&&   !str_cmp( d->pop3_char->name, dlist->original
        ? dlist->original->name : dlist->character->name ) )
        {
            CHAR_DATA *	ch;
	    int		type;

            if(dlist->original) ch=dlist->original;
            else ch=dlist->character;

	    for (type=0;type<MAX_NOTES;type++)
		d->pop3_char->pcdata->last_notes[type]=ch->pcdata->last_notes[type];
            break;
        }
    }

    // Make a list of all unread notes.
    count=0;
    for (type=0;type<MAX_NOTES;type++)
	count+=note_to_noteref(d,type);

    sprintf(buf,pop3_ready_msg,count);
    write_to_buffer(d,buf,0);
    d->connected=CON_POP3_CONNECTED;
}

void pop3_list( DESCRIPTOR_DATA *d, char *argument )
{
    int count=0,num=1;
    NOTE_REF *noteref;
    char buf[100];

    for(noteref=d->pop3_char->pcdata->noteref;noteref;noteref=noteref->next)
        count++;

    sprintf(buf,pop3_list_msg,count,0);
    write_to_buffer(d,buf,0);
    for(noteref=d->pop3_char->pcdata->noteref;noteref;noteref=noteref->next)
    {
        if(!noteref->deleted)
        {
            sprintf(buf,"%d %d\r\n",num,noteref->size);
            write_to_buffer(d,buf,0);
        }
        num++;
    }
    write_to_buffer(d,".\r\n",0);
}


void pop3_retr( DESCRIPTOR_DATA *d, char *argument )
{
    int		num=1,notenum;
    NOTE_REF *	noteref;
    char 	buf[MAX_STRING_LENGTH],*p;
    char *	text;

    if ((notenum=atoi(argument)==0)) {
        write_to_buffer(d,pop3_outofrange_rsp,0);
        return;
    }

    for (noteref=d->pop3_char->pcdata->noteref;
	noteref!=NULL;
	noteref=noteref->next) {
        if (num>=notenum) break;
        num++;
    }

    if (noteref==NULL) {
        write_to_buffer(d,pop3_outofrange_rsp,0);
        return;
    }

    if (noteref->deleted || noteref->note==NULL) {
        sprintf(buf,pop3_errdelete_rsp,notenum);
        write_to_buffer(d,buf,0);
        return;
    }

    sprintf(buf,
        "+OK %d octets\r\n"
        FROM"%s\r\n"
        TO"%s\r\n"
        SUBJECT"[%s] %s\r\n"
        DATE"%s\r\n"
        NOTETYPE"%s\r\n"
        "\r\n",
        noteref->size,
        noteref->note->sender,
        noteref->note->to_list,
        notes_table[noteref->note->type].name,
        noteref->note->subject,
        noteref->note->date,
        notes_table[noteref->note->type].name);
    write_to_buffer(d,buf,0);

    text=noteref->note->text;
    p=buf;
    while(*text)
    {
        /* Get one line */
        if(*text=='.') *(p++)='.';
        while(*text && *text!='\n' && *text!='\r') *(p++)=*(text++);
        *(p++)='\r';
        *(p++)='\n';
        *(p++)='\0';

        write_to_buffer(d,buf,0);

        p=buf;
        if(*text) text++;
        if(*text=='\n' || *text=='\r') text++;
    }

    write_to_buffer(d,".\r\n",0);
}


void pop3_noop( DESCRIPTOR_DATA *d, char *argument )
{
    write_to_buffer(d,"+OK\r\n",4);
}


void pop3_stat( DESCRIPTOR_DATA *d, char *argument )
{
    int cnt,octet;
    NOTE_REF *noteref;
    char buf[30];

    cnt=0;
    octet=0;
    for(noteref=d->pop3_char->pcdata->noteref;noteref;noteref=noteref->next)
    {
        octet+=noteref->size;
        cnt++;
    }

    sprintf(buf,"+OK %d %d\r\n",cnt,octet);
    write_to_buffer(d,buf,0);
}


void pop3_dele( DESCRIPTOR_DATA *d, char *argument )
{
    int num=1,notenum;
    NOTE_REF *noteref;
    char buf[MAX_STRING_LENGTH];

    if(!(notenum=atoi(argument)))
    {
        write_to_buffer(d,pop3_outofrange_rsp,0);
        return;
    }

    for(noteref=d->pop3_char->pcdata->noteref;noteref;noteref=noteref->next)
    {
        if(num>=notenum) break;
        num++;
    }

    if(!noteref)
    {
        write_to_buffer(d,pop3_outofrange_rsp,0);
        return;
    }

    if(noteref->deleted || !noteref->note)
    {
        sprintf(buf,pop3_errdelete_rsp,notenum);
        write_to_buffer(d,buf,0);
        return;
    }

    noteref->deleted=TRUE;

    sprintf(buf,pop3_marked_rsp,notenum);
    write_to_buffer(d,buf,0);
}

void pop3_rset( DESCRIPTOR_DATA *d, char *argument )
{
    int num=0;
    NOTE_REF *noteref;
    char buf[100];

    for(noteref=d->pop3_char->pcdata->noteref;noteref;noteref=noteref->next)
    {
        if(noteref->note)
        {
            num++;
            noteref->deleted=FALSE;
        }
    }

    sprintf(buf,pop3_ready_msg,num);
    write_to_buffer(d,buf,0);
}

void pop3_last( DESCRIPTOR_DATA *d, char *argument )
{
    int num=0;
    NOTE_REF *noteref;
    char buf[30];

    for(noteref=d->pop3_char->pcdata->noteref;noteref;noteref=noteref->next) num++;

    sprintf(buf,pop3_last_rsp,num);
    write_to_buffer(d,buf,0);
}


#endif
