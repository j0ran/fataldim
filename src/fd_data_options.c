/* $Id: fd_data_options.c,v 1.36 2008/05/01 19:21:42 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

#include "merc.h"

const OPTION_TYPE act_options[] =
{
    {	"npc",			ACT_IS_NPC,		FALSE	},
    {	"sentinel",		ACT_SENTINEL,		TRUE,	"mob will stay in the room where it was resetted"	},
    {	"scavenger",		ACT_SCAVENGER,		TRUE,	"mob will pick up and if possible use items"	},
    {	"remove_obj",		ACT_REMOVE_OBJ,		TRUE,	"shopkeeper will destroy items sold to him"	},
    {	"aggressive",		ACT_AGGRESSIVE,		TRUE,	"mob will attack players within 5 levels difference"	},
    {	"tcl_aggressive",	ACT_TCL_AGGRESSIVE,	TRUE,	"mob is an aggressive mob by his program"	},
    {	"stay_area",		ACT_STAY_AREA,		TRUE,	"mob will not cross area boundries"	},
    {	"wimpy",		ACT_WIMPY,		TRUE,	"mob will flee"	},
    {	"pet",			ACT_PET,		TRUE,	"mob is a pet"	},
    {	"train",		ACT_TRAIN,		TRUE,	"mob is a trainer"	},
    {	"practice",		ACT_PRACTICE,		TRUE,	"mob is a teacher"	},
    {	"unseen_servant",	ACT_UNSEENSERVANT,	TRUE,	"mob is invisible for mortal characters"	},
    {	"undead",		ACT_UNDEAD,		TRUE,	"mob is an undead"	},
    {	"cleric",		ACT_CLERIC,		TRUE,	"mob knows and uses clerical spells"	},
    {	"mage",			ACT_MAGE,		TRUE,	"mob knows and uses mage spells"	},
    {	"thief",		ACT_THIEF,		TRUE,	"mob knows and uses thief skills"	},
    {	"warrior",		ACT_WARRIOR,		TRUE,	"mob knows and uses warrior skills"	},
    {	"no_align",		ACT_NOALIGN,		TRUE,	"death of this mob won't effect alignment of the killer"	},
    {	"no_purge",		ACT_NOPURGE,		TRUE,	"mob won't be purged when told to. DO NOT USE"	},
    {	"outdoors",		ACT_OUTDOORS,		TRUE,	"mob will stay outdoors"	},
    {	"indoors",		ACT_INDOORS,		TRUE,	"mob will stay indoors"	},
    {	"is_healer",		ACT_IS_HEALER,		TRUE,	"mob is a healer"	},
    {	"gain",			ACT_GAIN,		TRUE,	"you can gain skills at this mob"	},
    {	"update_always",	ACT_UPDATE_ALWAYS,	TRUE,	"mob will be updated every reset"	},
    {	"is_changer",		ACT_IS_CHANGER,		TRUE,	"mob is a money changer"	},
    {	"peacefull",		ACT_PEACEFULL,		TRUE,	"iyou can't attack this mob"	},
    {	"noquest",		ACT_NOQUEST,		TRUE,	"you won't get quests to kill this mob"	},
    {	"ignore_position",	ACT_MOBPROG_IGNOREPOS,	TRUE,	"programs will always run, regardingless of his position"	},
    {	"ingore_neghealing",	ACT_IGNORENEGHEALING,	TRUE,	"mobs won't deteriorate from a negative healing environment"	},
    {	"corrupt",		ACT_CORRUPT,		TRUE,	"shopkeeper is a corrupt one"	},
    {	"mudwanderer",		ACT_MUDWANDERER,	TRUE,	"mob can wander through the entire mud. ASK PERMISSION"	},
    {	"pc_blocks_respawn",	ACT_PC_BLOCKS_RESPAWN,	TRUE,	"mob will not respawn when people are present in the area"	},
    {   "door_wanderer",	ACT_DOOR_WANDERER,	TRUE,	"mob can open doors while wandering"	},
    {   "lock_wanderer",	ACT_LOCK_WANDERER,	TRUE,	"mob can unlock&open doors while wandering"	},
    {	NULL,			0,			0	}
};

const OPTION_TYPE plr_options[] =
{
    {	"npc",			PLR_IS_NPC,		FALSE	},
    {	"autoassist",		PLR_AUTOASSIST,		FALSE	},
    {	"autoexit",		PLR_AUTOEXIT,		FALSE	},
    {	"autoloot",		PLR_AUTOLOOT,		FALSE	},
    {	"autosac",		PLR_AUTOSAC,		FALSE	},
    {	"autogold",		PLR_AUTOGOLD,		FALSE	},
    {	"autosplit",		PLR_AUTOSPLIT,		FALSE	},
    {	"freeze_title",		PLR_FREEZETITLE,	FALSE	},
    {	"holylight",		PLR_HOLYLIGHT,		FALSE	},
    {	"holylight++",		PLR_HOLYLIGHT_PLUSPLUS,	FALSE	},
    {	"can_loot",		PLR_CANLOOT,		FALSE	},
    {	"no_summon",		PLR_NOSUMMON,		FALSE	},
    {	"no_follow",		PLR_NOFOLLOW,		FALSE	},
    {	"color",		PLR_COLOUR,		FALSE	},
    {	"permit",		PLR_PERMIT,		TRUE	},
    {	"log",			PLR_LOG,		FALSE	},
    {	"deny",			PLR_DENY,		FALSE	},
    {	"freeze",		PLR_FREEZE,		FALSE	},
    {	"thief",		PLR_THIEF,		FALSE	},
    {	"killer",		PLR_KILLER,		FALSE	},
    {	"questing",		PLR_QUESTOR,		FALSE	},
    {	"has_raw_colour",	PLR_HAS_RAW_COLOUR,	FALSE	},
    {	"raw_colour",		PLR_WANTS_RAW_COLOUR,	FALSE	},
    {	"pueblo",		PLR_PUEBLO,		TRUE    },
    {	"mxp",			PLR_MXP,		TRUE    },
    {	"quitting",		PLR_QUITTING,		TRUE    },
    {	"pkilling",		PLR_PKILLING,		TRUE	},
    {	"ordered",		PLR_ORDERED,		TRUE	},
    {	"nosave",		PLR_NOSAVE,		TRUE	},
    {	"compass",		PLR_COMPASS,		TRUE	},
    {	"switched",		PLR_SWITCHED,		TRUE	},
#ifdef HAS_ALTS
    {	"alt",			PLR_IS_ALT,		TRUE	},
#endif
#ifdef HAS_MCCP
    {	"mccp",			PLR_MCCP,		TRUE	},
    {	"mccp2",		PLR_MCCP2,		TRUE	},
#endif
    {	"hardcore",		PLR_HARDCORE,		TRUE	},
    {	NULL,			0,			0	}
};

const OPTION_TYPE effect_options[] =
{
//  {	"none",			EFF_NONE,		TRUE	},
    {	"blind",		EFF_BLIND,		TRUE	},
    {	"invisible",		EFF_INVISIBLE,		TRUE	},
    {	"detect_evil",		EFF_DETECT_EVIL,	TRUE	},
    {	"detect_invis",		EFF_DETECT_INVIS,	TRUE	},
    {	"detect_magic",		EFF_DETECT_MAGIC,	TRUE	},
    {	"detect_hidden",	EFF_DETECT_HIDDEN,	TRUE	},
    {	"detect_good",		EFF_DETECT_GOOD,	TRUE	},
    {	"sanctuary",		EFF_SANCTUARY,		TRUE	},
    {	"faerie_fire",		EFF_FAERIE_FIRE,	TRUE	},
    {	"infrared",		EFF_INFRARED,		TRUE	},
    {	"curse",		EFF_CURSE,		TRUE	},
    {	"poison",		EFF_POISON,		TRUE	},
    {	"protect_evil",		EFF_PROTECT_EVIL,	TRUE	},
    {	"protect_good",		EFF_PROTECT_GOOD,	TRUE	},
    {	"sneak",		EFF_SNEAK,		TRUE	},
    {	"hide",			EFF_HIDE,		TRUE	},
    {	"sleep",		EFF_SLEEP,		FALSE	},
    {	"charm",		EFF_CHARM,		FALSE	},
    {	"flying",		EFF_FLYING,		TRUE	},
    {	"pass_door",		EFF_PASS_DOOR,		TRUE	},
    {	"haste",		EFF_HASTE,		TRUE	},
    {	"calm",			EFF_CALM,		TRUE	},
    {	"plague",		EFF_PLAGUE,		FALSE	},
    {	"weaken",		EFF_WEAKEN,		TRUE	},
    {	"dark_vision",		EFF_DARK_VISION,	TRUE	},
    {	"berserk",		EFF_BERSERK,		FALSE	},
    {	"swim",			EFF_SWIM,		TRUE	},
    {	"regeneration",		EFF_REGENERATION,	TRUE	},
    {	"slow",			EFF_SLOW,		TRUE	},
    {	"waterbreathing",	EFF_WATERBREATHING,	TRUE	},
    {	"bless",		EFF_BLESS,		TRUE	},
    {	"armor",		EFF_ARMOR,		TRUE	},
    {	"change_sex",		EFF_CHANGESEX,		TRUE	},
    {	"frenzy",		EFF_FRENZY,		TRUE	},
    {	"giant_strength",	EFF_GIANTSTRENGTH,	TRUE	},
    {	"shield",		EFF_SHIELD,		TRUE	},
    {	"stone_skin",		EFF_STONESKIN,		TRUE	},
    {	"chill_touch",		EFF_CHILLTOUCH,		FALSE	},
    {	"chaos_shield",		EFF_CHAOS_SHIELD,	TRUE	},
    {	"wild_shield",		EFF_WILD_SHIELD,	TRUE	},
    {	"waterwalk",		EFF_WATERWALK,		TRUE	},
    {	"bark_skin",		EFF_BARKSKIN,		TRUE	},
    {	"entangle",		EFF_ENTANGLE,		FALSE	},
    {	"battle_hymn",		EFF_BATTLE_HYMN,	FALSE	},
    {	"war_dirge",		EFF_WAR_DIRGE,		FALSE	},
    {	"divine_relic",		EFF_DIVINE_RELIC,	FALSE	},
    {	"unholy_relic",		EFF_UNHOLY_RELIC,	FALSE	},
    {	"healing",		EFF_HEALING,		FALSE	},
    {	"invited",		EFF_INVITED,		FALSE	},
    {	"detect_curse",		EFF_DETECT_CURSE,	TRUE	},
    {	"hallucination",	EFF_HALLUCINATION,	TRUE	},
    {	"hunger",		EFF_HUNGER,		TRUE	},
    {	"thirst",		EFF_THIRST,		TRUE	},
    {	NULL,			0,			0	}
};

const OPTION_TYPE effect2_options[]={
    {	"imp-invisible",	EFF_IMPINVISIBLE,	TRUE	},
    {	"cantrip",		EFF_CANTRIP,		FALSE	},
    {	NULL,			0,			0	}
};

const OPTION_TYPE off_options[] =
{
    {	"area_attack",		OFF_AREA_ATTACK,	TRUE,	"mob can attack everyone it's fighting with"	},
    {	"backstab",		OFF_BACKSTAB,		TRUE	},
    {	"bash",			OFF_BASH,		TRUE	},
    {	"berserk",		OFF_BERSERK,		TRUE	},
    {	"disarm",		OFF_DISARM,		TRUE	},
    {	"dodge",		OFF_DODGE,		TRUE	},
    {	"fade",			OFF_FADE,		TRUE,	"mob will try to hide"	},
    {	"fast",			OFF_FAST,		TRUE,	"mob is hasted"	},
    {	"kick",			OFF_KICK,		TRUE	},
    {	"dirt_kick",		OFF_KICK_DIRT,		TRUE	},
    {	"parry",		OFF_PARRY,		TRUE	},
    {	"rescue",		OFF_RESCUE,		TRUE	},
    {	"use_tail",		OFF_TAIL,		TRUE	},
    {	"trip",			OFF_TRIP,		TRUE	},
    {	"crush",		OFF_CRUSH,		TRUE	},
    {	"assist_all",		ASSIST_ALL,		TRUE,	"mob will assist anyone"	},
    {	"assist_align",		ASSIST_ALIGN,		TRUE,	"mob will assist anyone with the same alignment"	},
    {	"assist_race",		ASSIST_RACE,		TRUE,	"mob will assist people with the same race"	},
    {	"assist_players",	ASSIST_PLAYERS,		TRUE,	"mob will assist players"	},
    {	"assist_guard",		ASSIST_GUARD,		TRUE,	"mob will assist guards"	},
    {	"assist_vnum",		ASSIST_VNUM,		TRUE,	"mob will assist mobs with the same vnum"	},
    {	"remember",		OFF_REMEMBER,		TRUE,	"mob will attack again when you have fled and returned"	},
    {	"hunter",		OFF_HUNTER,		TRUE,	"mob will come after you when you flee"	},
    {	"headbutt",		OFF_HEADBUTT,		TRUE	},
    {	"eyepoke",		OFF_EYEPOKE,		TRUE	},
    {	NULL,			0,			0	}
};

const OPTION_TYPE imm_options[] =
{
    {	"none",			IMM_NONE,		TRUE	},
    {	"summon",		IMM_SUMMON,		TRUE	},
    {	"charm",		IMM_CHARM,		TRUE	},
    {	"magic",		IMM_MAGIC,		TRUE	},
    {	"weapon",		IMM_WEAPON,		TRUE	},
    {	"bash",			IMM_BASH,		TRUE	},
    {	"pierce",		IMM_PIERCE,		TRUE	},
    {	"slash",		IMM_SLASH,		TRUE	},
    {	"fire",			IMM_FIRE,		TRUE	},
    {	"cold",			IMM_COLD,		TRUE	},
    {	"lightning",		IMM_LIGHTNING,		TRUE	},
    {	"acid",			IMM_ACID,		TRUE	},
    {	"poison",		IMM_POISON,		TRUE	},
    {	"negative",		IMM_NEGATIVE,		TRUE	},
    {	"holy",			IMM_HOLY,		TRUE	},
    {	"energy",		IMM_ENERGY,		TRUE	},
    {	"mental",		IMM_MENTAL,		TRUE	},
    {	"disease",		IMM_DISEASE,		TRUE	},
    {	"drowning",		IMM_DROWNING,		TRUE	},
    {	"light",		IMM_LIGHT,		TRUE	},
    {	"sound",		IMM_SOUND,		TRUE	},
    {	"wood",			IMM_WOOD,		TRUE	},
    {	"silver",		IMM_SILVER,		TRUE	},
    {	"iron",			IMM_IRON,		TRUE	},
    {	NULL,			0,			0	}
};

const OPTION_TYPE res_options[] =
{
    {	"none",			RES_NONE,		TRUE	},
    {	"summon",		RES_SUMMON,		TRUE	},
    {	"charm",		RES_CHARM,		TRUE	},
    {	"magic",		RES_MAGIC,		TRUE	},
    {	"weapon",		RES_WEAPON,		TRUE	},
    {	"bash",			RES_BASH,		TRUE	},
    {	"pierce",		RES_PIERCE,		TRUE	},
    {	"slash",		RES_SLASH,		TRUE	},
    {	"fire",			RES_FIRE,		TRUE	},
    {	"cold",			RES_COLD,		TRUE	},
    {	"lightning",		RES_LIGHTNING,		TRUE	},
    {	"acid",			RES_ACID,		TRUE	},
    {	"poison",		RES_POISON,		TRUE	},
    {	"negative",		RES_NEGATIVE,		TRUE	},
    {	"holy",			RES_HOLY,		TRUE	},
    {	"energy",		RES_ENERGY,		TRUE	},
    {	"mental",		RES_MENTAL,		TRUE	},
    {	"disease",		RES_DISEASE,		TRUE	},
    {	"drowning",		RES_DROWNING,		TRUE	},
    {	"light",		RES_LIGHT,		TRUE	},
    {	"sound",		RES_SOUND,		TRUE	},
    {	"wood",			RES_WOOD,		TRUE	},
    {	"silver",		RES_SILVER,		TRUE	},
    {	"iron",			RES_IRON,		TRUE	},
    {	NULL,			0,			0	}
};

const OPTION_TYPE vuln_options[] =
{
    {	"none",			VULN_NONE,		TRUE	},
    {	"summon",		VULN_SUMMON,		TRUE	},
    {	"charm",		VULN_CHARM,		TRUE	},
    {	"magic",		VULN_MAGIC,		TRUE	},
    {	"weapon",		VULN_WEAPON,		TRUE	},
    {	"bash",			VULN_BASH,		TRUE	},
    {	"pierce",		VULN_PIERCE,		TRUE	},
    {	"slash",		VULN_SLASH,		TRUE	},
    {	"fire",			VULN_FIRE,		TRUE	},
    {	"cold",			VULN_COLD,		TRUE	},
    {	"lightning",		VULN_LIGHTNING,		TRUE	},
    {	"acid",			VULN_ACID,		TRUE	},
    {	"poison",		VULN_POISON,		TRUE	},
    {	"negative",		VULN_NEGATIVE,		TRUE	},
    {	"holy",			VULN_HOLY,		TRUE	},
    {	"energy",		VULN_ENERGY,		TRUE	},
    {	"mental",		VULN_MENTAL,		TRUE	},
    {	"disease",		VULN_DISEASE,		TRUE	},
    {	"drowning",		VULN_DROWNING,		TRUE	},
    {	"light",		VULN_LIGHT,		TRUE	},
    {	"sound",		VULN_SOUND,		TRUE	},
    {	"wood",			VULN_WOOD,		TRUE	},
    {	"silver",		VULN_SILVER,		TRUE	},
    {	"iron",			VULN_IRON,		TRUE	},
    {	NULL,			0,			0	}
};

const OPTION_TYPE form_options[] =
{
    {	"edible",		FORM_EDIBLE,		TRUE	},
    {	"poison",		FORM_POISON,		TRUE	},
    {	"magical",		FORM_MAGICAL,		TRUE	},
    {	"instant_decay",	FORM_INSTANT_DECAY,	TRUE	},
    {	"other",		FORM_OTHER,		TRUE	},
    {	"animal",		FORM_ANIMAL,		TRUE	},
    {	"sentient",		FORM_SENTIENT,		TRUE	},
    {	"undead",		FORM_UNDEAD,		TRUE	},
    {	"construct",		FORM_CONSTRUCT,		TRUE	},
    {	"mist",			FORM_MIST,		TRUE	},
    {	"intangible",		FORM_INTANGIBLE,	TRUE	},
    {	"biped",		FORM_BIPED,		TRUE	},
    {	"centaur",		FORM_CENTAUR,		TRUE	},
    {	"insect",		FORM_INSECT,		TRUE	},
    {	"spider",		FORM_SPIDER,		TRUE	},
    {	"crustacean",		FORM_CRUSTACEAN,	TRUE	},
    {	"worm",			FORM_WORM,		TRUE	},
    {	"blob",			FORM_BLOB,		TRUE	},
    {	"mammal",		FORM_MAMMAL,		TRUE	},
    {	"bird",			FORM_BIRD,		TRUE	},
    {	"reptile",		FORM_REPTILE,		TRUE	},
    {	"snake",		FORM_SNAKE,		TRUE	},
    {	"dragon",		FORM_DRAGON,		TRUE	},
    {	"amphibian",		FORM_AMPHIBIAN,		TRUE	},
    {	"fish",			FORM_FISH ,		TRUE	},
    {	"cold_blood",		FORM_COLD_BLOOD,	TRUE	},
    {	NULL,			0,			0	}
};

const OPTION_TYPE part_options[] =
{
    {	"head",			PART_HEAD,		TRUE	},
    {	"arms",			PART_ARMS,		TRUE	},
    {	"legs",			PART_LEGS,		TRUE	},
    {	"heart",		PART_HEART,		TRUE	},
    {	"brains",		PART_BRAINS,		TRUE	},
    {	"guts",			PART_GUTS,		TRUE	},
    {	"hands",		PART_HANDS,		TRUE	},
    {	"feet",			PART_FEET,		TRUE	},
    {	"fingers",		PART_FINGERS,		TRUE	},
    {	"ear",			PART_EAR,		TRUE	},
    {	"eye",			PART_EYE,		TRUE	},
    {	"long_tongue",		PART_LONG_TONGUE,	TRUE	},
    {	"eyestalks",		PART_EYESTALKS,		TRUE	},
    {	"tentacles",		PART_TENTACLES,		TRUE	},
    {	"fins",			PART_FINS,		TRUE	},
    {	"wings",		PART_WINGS,		TRUE	},
    {	"tail",			PART_TAIL,		TRUE	},
    {	"claws",		PART_CLAWS,		TRUE	},
    {	"fangs",		PART_FANGS,		TRUE	},
    {	"horns",		PART_HORNS,		TRUE	},
    {	"scales",		PART_SCALES,		TRUE	},
    {	"tusks",		PART_TUSKS,		TRUE	},
    {	NULL,			0,			0	}
};

const OPTION_TYPE comm_options[] =
{
    {	"quiet",		COMM_QUIET,		TRUE	},
    {	"deaf",			COMM_DEAF,		TRUE	},
    {	"no_wiz",		COMM_NOWIZ,		TRUE	},
    {	"no_auction",		COMM_NOAUCTION,		TRUE	},
    {	"no_gossip",		COMM_NOGOSSIP,		TRUE	},
    {	"no_question",		COMM_NOQUESTION,	TRUE	},
    {	"no_answer",		COMM_NOANSWER,		TRUE	},
    {	"no_music",		COMM_NOMUSIC,		TRUE	},
    {	"no_clan",		COMM_NOCLAN,		TRUE	},
    {	"no_quote",		COMM_NOQUOTE,		TRUE	},
    {	"shoutsoff",		COMM_SHOUTSOFF,		TRUE,	"Won't hear shouts"	},
    {	"announce",		COMM_ANNOUNCE,		TRUE	},
    {	"no_quest",		COMM_NOQUEST,		TRUE 	},
    {	"compact",		COMM_COMPACT,		TRUE	},
    {	"brief",		COMM_BRIEF,		TRUE	},
    {	"combine",		COMM_COMBINE,		TRUE	},
    {	"show_effects",		COMM_SHOW_EFFECTS,	TRUE	},
    {	"show_armor",		COMM_SHOW_ARMOR,	TRUE	},
    {	"no_grats",		COMM_NOGRATS,		TRUE	},
    {	"no_emote",		COMM_NOEMOTE,		FALSE	},
    {	"no_shout",		COMM_NOSHOUT,		FALSE	},
    {	"no_tell",		COMM_NOTELL,		FALSE	},
    {	"no_yell",		COMM_NOYELL,		FALSE	},
    {	"no_channels",		COMM_NOCHANNELS,	FALSE	},
    {	"snoop_proof",		COMM_SNOOP_PROOF,	FALSE	},
    {	"afk",			COMM_AFK,		TRUE	},
    {	"afk-by-idle",		COMM_AFK_BY_IDLE,	FALSE	},
    {	"show_pain",		COMM_SHOW_PAIN,		TRUE	},
    {	"show_weight",		COMM_SHOW_WEIGHT,	TRUE	},
    {	"show_extra",		COMM_SHOW_EXTRADAM,	TRUE    },
#ifdef I3
    {	"i3-muted",		COMM_I3MUTE,		TRUE	},
#endif
    {	"no_herotalk",		COMM_NOHEROTALK,	TRUE	},
    {	"no_hunger",		COMM_SHOW_NOHUNGER,	TRUE	},
    {	"no_page",		COMM_NOPAGE,		TRUE	},
    {	"no_color",		COMM_NOCOLOR,		TRUE	},
    {	"no_immtalk",		COMM_NOIMMTALK,		TRUE	},
    {	"no_note",		COMM_NONOTE,		TRUE	},
    {	NULL,			0,			0	}
};

const OPTION_TYPE area_options[] =
{
    {	"none",			AREA_NONE,		FALSE	},
    {	"changed",		AREA_CHANGED,		FALSE,	"Area has been altered since last save"	},
    {	"added",		AREA_ADDED,		FALSE,	"Area has never been saved"	},
    {	"loading",		AREA_LOADING,		FALSE,	"Area is being loaded"	},
    {	"verbose",		AREA_VERBOSE,		FALSE,	"Supply some extra data in the area file. OBSOLETE"	},
    {	"delete",		AREA_DELETE,		FALSE,	"Area won't be loaded at next startup"	},
    {	"unfinished",		AREA_UNFINISHED,	FALSE,	"Area is not finished yet: no xp, no quests, obj will decay"	},
    {	"pkzone",		AREA_PKZONE,		TRUE,	"Removes the nasty side effects of dieing when killed by a PC\n"
								"Player killing is allowed for clan members"	},
    {	"conquest",		AREA_CONQUEST,		TRUE,	"Area can be conquered by clanmembers"	},
    {	"arena",		AREA_ARENA,		TRUE,	"Removes the nasty side effects of dieing.\n"
								"Player killing is allowed for all PC's"	},
    {	"pc_blocks_respawn",	AREA_PC_BLOCKS_RESPAWN,	TRUE,	"Don't reset mob/obj when a player is in the area"	},
    {	"protected",		AREA_PROTECTED,		TRUE,	"This area will be overwritten from a buildmud"	},
    {	NULL,			0,			0	}
};


const OPTION_TYPE exit_options[] =
{
    {	"door",			EX_ISDOOR,		TRUE,	"exit is a door"	},
    {	"closed",		EX_CLOSED,		TRUE,	"door is closed"	},
    {	"locked",		EX_LOCKED,		TRUE,	"door is locked"	},
    {	"nowarnings",		EX_NOWARN,		TRUE,	"Don't whine about the exit when booting"	},
    {	"pickproof",		EX_PICKPROOF,		TRUE,	"Lock can't be picked"	},
    {	"nopass",		EX_NOPASS,		TRUE,	"Passdoor won't work with this door"	},
    {	"easy",			EX_EASY,		TRUE,	"Lock is easy to pick"	},
    {	"hard",			EX_HARD,		TRUE,	"Lock is hard to pick"	},
    {	"infuriating",		EX_INFURIATING,		TRUE,	"Lock is almost impossiblesy to pick"	},
    {	"noclose",		EX_NOCLOSE,		TRUE,	"Door cannot be closed/locked"	},
    {	"nolock",		EX_NOLOCK,		TRUE,	"Door cannot be locked"	},
    {	"trap",			EX_TRAP,		TRUE,	"Exit is trapped. One of trap_* needs to be selected too"	},
    {	"trap_darts",		EX_TRAP_DARTS,		TRUE,	"The trap shoots darts"	},
    {	"trap_poison",		EX_TRAP_POISON,		TRUE,	"The trap cointains a poison"	},
    {	"trap_exploding",	EX_TRAP_EXPLODING,	TRUE,	"The trap will explode (area attack)"	},
    {	"trap_sleepgas",	EX_TRAP_SLEEPGAS,	TRUE,	"The trap contains sleeping gas"	},
    {	"trap_death",		EX_TRAP_DEATH,		TRUE,	"a death trap. ASK PERMISSION"	},
    {	"secret",		EX_SECRET,		TRUE,	"Exit is not visible"	},
    {	"gate_recall",		EX_GATE_RECALL,		TRUE,	"Exit leads to room last stored by a gate with 'store'"	},
    {	"tcl_lock",		EX_TCLLOCK,		TRUE,	"The lock on this exit is done by the can(un)lock roomtrigger" },
    {	NULL,			0,			0	}
};



const OPTION_TYPE door_resets[] =
{
    {	"open and unlocked",	0,			TRUE	},
    {	"closed and unlocked",	1,			TRUE	},
    {	"closed and locked",	2,			TRUE	},
    {	NULL,			0,			0	}
};



const OPTION_TYPE room_options[] =
{
    {	"dark",			ROOM_DARK,		TRUE,	"A light is always needed before anything can be seen."	},
    {	"light",		ROOM_LIGHT,		TRUE,	"A light is never required to light up the room."	},
    {	"no_mob",		ROOM_NO_MOB,		TRUE,	"Mobs can't enter"	},
    {	"indoors",		ROOM_INDOORS,		TRUE,	"key to the in- and outdoors mob act flag"	},
    {	"pitchblack",		ROOM_PITCHBLACK,	TRUE,	"room is always dark. Lights do not affect it"	},
    {	"private",		ROOM_PRIVATE,		TRUE,	"At most 2 chars can be in the room at the same time"	},
    {	"safe",			ROOM_SAFE,		TRUE,	"no combat is possible here"	},
    {	"solitary",		ROOM_SOLITARY,		TRUE,	"Only 1 char can be in the room at the same time"	},
    {	"petshop",		ROOM_PET_SHOP,		TRUE,	"this is a pet shop."	},
    {	"no_recall",		ROOM_NO_RECALL,		TRUE,	"leaving the room by magical or divine means is not possible."	},
    {	"imp-only",		ROOM_IMP_ONLY,		TRUE,	"Only the implementor and level 100 trustees can enter"	},
    {	"gods-only",		ROOM_GODS_ONLY,		TRUE,	"Only immortals can enter"	},
    {	"heroes-only",		ROOM_HEROES_ONLY,	TRUE,	"Only heros can enter"	},
    {	"newbies-only",		ROOM_NEWBIES_ONLY,	TRUE,	"Only newbies can enter"	},
    {	"law",			ROOM_LAW,		TRUE,	"Aggressive mobs are not allowed"	},
    {	"nowhere",		ROOM_NOWHERE,		TRUE,	"Players are hidden from 'players near you' in where"	},
    {	"deathtrap",		ROOM_DEATHTRAP,		TRUE,	"Non immortal chars die instantly. ASK PERMISSION"	},
    {	"nomagic",		ROOM_NOMAGIC,		TRUE,	"No casting spells here"	},
    {	"saveobj",		ROOM_SAVEOBJ,		TRUE,	"All obj in the room are saved regularly and loaded on startup"	},
    {	"nonewbie",		ROOM_NONEWBIE,		TRUE,	"Newbies cannot enter"	},
    {	"clanonly",		ROOM_CLANONLY,		TRUE,	"Only members of a clan (any clan) can enter" 	},
    {	"nosacrifice",		ROOM_NOSACRIFICE,	TRUE,	"Sacrifice and donate are limited to one obj at a time"	},
    {	"nogetall",		ROOM_NOGETALL,		TRUE,	"'get all [...]' is limited to one obj at a time"	},
    {	"noquest",		ROOM_NOQUEST,		TRUE,	"Don't hand out a quest for obj in this room"	},
    {	NULL,			0,			0	}
};



const OPTION_TYPE itemtype_options[] =
{
    {	"light",		ITEM_LIGHT,		TRUE	},
    {	"scroll",		ITEM_SCROLL,		TRUE	},
    {	"wand",			ITEM_WAND,		TRUE	},
    {	"staff",		ITEM_STAFF,		TRUE	},
    {	"weapon",		ITEM_WEAPON,		TRUE	},
    {	"treasure",		ITEM_TREASURE,		TRUE	},
    {	"armor",		ITEM_ARMOR,		TRUE	},
    {	"potion",		ITEM_POTION,		TRUE	},
    {	"clothing",		ITEM_CLOTHING,		TRUE	},
    {	"furniture",		ITEM_FURNITURE,		TRUE	},
    {	"trash",		ITEM_TRASH,		TRUE	},
    {	"container",		ITEM_CONTAINER,		TRUE	},
    {	"drink-container",	ITEM_DRINK_CON,		TRUE	},
    {	"key",			ITEM_KEY,		TRUE	},
    {	"food",			ITEM_FOOD,		TRUE	},
    {	"money",		ITEM_MONEY,		TRUE	},
    {	"boat",			ITEM_BOAT,		TRUE	},
    {	"npc_corpse",		ITEM_CORPSE_NPC,	TRUE	},
    {	"pc_corpse",		ITEM_CORPSE_PC,		FALSE	},
    {	"fountain",		ITEM_FOUNTAIN,		TRUE	},
    {	"pill",			ITEM_PILL,		TRUE	},
    {	"protect",		ITEM_PROTECT,		TRUE	},
    {	"map",			ITEM_MAP,		TRUE	},
    {	"portal",		ITEM_PORTAL,		TRUE	},
    {	"warp_stone",		ITEM_WARP_STONE,	TRUE	},
    {	"room_key",		ITEM_ROOM_KEY,		TRUE	},
    {	"gem",			ITEM_GEM,		TRUE	},
    {	"jewelry",		ITEM_JEWELRY,		TRUE	},
#ifdef HAS_JUKEBOX
    {	"jukebox",		ITEM_JUKEBOX,		TRUE	},
#endif
    {	"quest",		ITEM_QUEST,		TRUE	},
    {	"clan",			ITEM_CLAN,		TRUE	},
    {	NULL,			0,			0	}
};


const OPTION_TYPE extra_options[] =
{
    {	"glow",			ITEM_GLOW,		TRUE,	"can be seen in a dark room"	},
    {	"hum",			ITEM_HUM,		TRUE,	"shows '(humming)' in front of the description"	},
    {	"dark",			ITEM_DARK,		TRUE,	"adds the dark flag"	},
    {	"lock",			ITEM_LOCK,		TRUE,	"adds the lock flag"	},
    {	"evil",			ITEM_EVIL,		TRUE,	"give the obj a 'red aura'"	},
    {	"invis",		ITEM_INVIS,		TRUE,	"obj is invisible"	},
    {	"magic",		ITEM_MAGIC,		TRUE,	"shows '(magical)' in front of the description"	},
    {	"no_drop",		ITEM_NODROP,		TRUE,	"You can't let go of it"	},
    {	"bless",		ITEM_BLESS,		TRUE,	"Makes doing bad things to it a bit more difficoult."	},
    {	"anti-good",		ITEM_ANTI_GOOD,		TRUE,	"Item can't be used by good chars"	},
    {	"anti-evil",		ITEM_ANTI_EVIL,		TRUE,	"Item can't be used by evil chars"	},
    {	"anti-neutral",		ITEM_ANTI_NEUTRAL,	TRUE,	"Item can't be used by neutral chars"	},
    {	"no_remove",		ITEM_NOREMOVE,		TRUE,	"Item cannot be removed from equipment"	},
    {	"inventory",		ITEM_INVENTORY,		TRUE,	"Shopkeeper has an unlimited supply"	},
    {	"no_purge",		ITEM_NOPURGE,		TRUE,	"Item will not be purged.\n"
								"Acid, cold, fire, and shock don't affect it"	},
    {	"rot_death",		ITEM_ROT_DEATH,		TRUE,	"Item is destroyed when it's carrier dies"	},
    {	"vis_death",		ITEM_VIS_DEATH,		TRUE,	"Item cannot be seen until its carrier has died"	},
    {	"non_metal",		ITEM_NONMETAL,		TRUE,	"Item is not affected by 'heat metal'"	},
    {	"no_show",		ITEM_NOSHOW,		TRUE,	"Item is not visible, but can be used"	},
    {	"no_locate",		ITEM_NOLOCATE,		TRUE,	"'Locate object' doesn't work"	},
    {	"melt_drop",		ITEM_MELT_DROP,		TRUE,	"Item 'dissolves into smoke' when it hits the ground"	},
    {	"had_timer",		ITEM_HAD_TIMER,		FALSE	},
    {	"sell_extract",		ITEM_SELL_EXTRACT,	TRUE,	"Extract the item when it is bought by a shopkeeper"	},
    { 	"freeze_proof",		ITEM_FREEZE_PROOF,	TRUE,	"Item can't be damaged by cold"	},
    { 	"burn_proof",		ITEM_BURN_PROOF,	TRUE,	"Item can't be damaged by heat"	},
    {	"no_uncurse",		ITEM_NOUNCURSE,		TRUE,	"'remove curse' doesn't work"	},
    {	"no_destroy",		ITEM_NODESTROY,		TRUE,	"mobs should check this before destroying no_uncurse objects"	},
    {	"no_identify",		ITEM_UNIDENTIFABLE,	TRUE,	"lore and identify don't work"	},
    {	"no_steal",		ITEM_NOSTEAL,		TRUE,	"Item can't be stolen"	},
    {	"no_sell",		ITEM_NOSELL,		TRUE,	"Prevent a PC from selling the item"	},
    {	"reflecting",		ITEM_REFLECTING,	TRUE,	"Item can reflect magic back to the caster"	},
    {	"noquest",		ITEM_NOQUEST,		TRUE,	"Don't hand out fetch quests to this item"	},
    {	"pc_blocks_respawn",	ITEM_PC_BLOCKS_RESPAWN,	TRUE,	"Don't reset the item when a player is in the area"	},
    {	"saveobj",		ITEM_SAVEOBJ,		TRUE,	"Save the item across reboots"	},
    {	"no_leftovers",		ITEM_NO_LEFTOVERS,	TRUE,	"Don't create an empty vial/parchment after quaff/recite"	},
    {	"insurable",		ITEM_INSURABLE,		TRUE,	"Item can be insured"	},
    {	"insured",		ITEM_INSURED,		TRUE,	"Item will be rescued from destruction"	},
    {	"questitem",		ITEM_QUESTITEM,		TRUE,	"Item has been bought in a questshop"	},
    {	"unusable",		ITEM_UNUSABLE,		TRUE,	"Item is invisible and cannot be used (but can hold triggers)"	},
    {	"installed",		ITEM_INSTALLED,		FALSE,	"Item was installed and not just droped"	},
    {	NULL,			0,			0	}
};

const OPTION_TYPE extra2_options[] =
{
    { 	"anti-class",		ITEM_ANTI_CLASS,	FALSE	},
    {	"anti-race",		ITEM_ANTI_RACE,		FALSE	},
    {	"class-only",		ITEM_CLASS_ONLY,	FALSE	},
    {	"race-only",		ITEM_RACE_ONLY,		FALSE	},
    {	"race-poison",		ITEM_RACE_POISON,	FALSE	},
    {	NULL,			0,			0	}
};


const OPTION_TYPE wear_options[] =
{
    {	"take",			ITEM_TAKE,		TRUE	},
    {	"finger",		ITEM_WEAR_FINGER,	TRUE	},
    {	"neck",			ITEM_WEAR_NECK,		TRUE	},
    {	"body",			ITEM_WEAR_BODY,		TRUE	},
    {	"head",			ITEM_WEAR_HEAD,		TRUE	},
    {	"legs",			ITEM_WEAR_LEGS,		TRUE	},
    {	"feet",			ITEM_WEAR_FEET,		TRUE	},
    {	"hands",		ITEM_WEAR_HANDS,	TRUE	},
    {	"arms",			ITEM_WEAR_ARMS,		TRUE	},
    {	"shield",		ITEM_WEAR_SHIELD,	TRUE	},
    {	"about",		ITEM_WEAR_ABOUT,	TRUE	},
    {	"waist",		ITEM_WEAR_WAIST,	TRUE	},
    {	"wrist",		ITEM_WEAR_WRIST,	TRUE	},
    {	"wield",		ITEM_WIELD,		TRUE	},
    {	"hold",			ITEM_HOLD,		TRUE	},
    {	"no_sac",		ITEM_NO_SAC,		TRUE	},
    {	"wear_float",		ITEM_WEAR_FLOAT,	TRUE	},
    {	NULL,			0,			0	}
};

/*
 * Used when adding an effect to tell where it goes.
 * See addeffect and deleffect in act_olc.c
 */
const OPTION_TYPE apply_options[] =
{
    {	"none",			APPLY_NONE,		TRUE	},
    {	"str",			APPLY_STR,		TRUE	},
    {	"dex",			APPLY_DEX,		TRUE	},
    {	"int",			APPLY_INT,		TRUE	},
    {	"wis",			APPLY_WIS,		TRUE	},
    {	"con",			APPLY_CON,		TRUE	},
    {	"sex",			APPLY_SEX,		TRUE	},
    {	"class",		APPLY_CLASS,		TRUE	},
    {	"level",		APPLY_LEVEL,		TRUE	},
    {	"age",			APPLY_AGE,		TRUE	},
    {	"height",		APPLY_HEIGHT,		TRUE	},
    {	"weight",		APPLY_WEIGHT,		TRUE	},
    {	"mana",			APPLY_MANA,		TRUE	},
    {	"hp",			APPLY_HIT,		TRUE	},
    {	"move",			APPLY_MOVE,		TRUE	},
    {	"gold",			APPLY_GOLD,		TRUE	},
    {	"exp",			APPLY_EXP,		TRUE	},
    {	"ac",			APPLY_AC,		TRUE	},
    {	"hitroll",		APPLY_HITROLL,		TRUE	},
    {	"damroll",		APPLY_DAMROLL,		TRUE	},
    {	"saving-para",		APPLY_SAVING_PARA,	TRUE	},
    {	"saving-rod",		APPLY_SAVING_ROD,	TRUE	},
    {	"saving-petri",		APPLY_SAVING_PETRI,	TRUE	},
    {	"saving-breath",	APPLY_SAVING_BREATH,	TRUE	},
    {	"saving-spell",		APPLY_SAVING_SPELL,	TRUE	},
    {	"spell/skill",		APPLY_SPELL_EFFECT,	FALSE	},
    {	"spell",		APPLY_SPELL_EFFECT,	TRUE	},
    {	"skill",		APPLY_SPELL_EFFECT,	TRUE	},
    {	"alignment",		APPLY_ALIGNMENT,	TRUE	},
    {	NULL,			0,			0	}
};



/*
 * What is seen.
 */
const OPTION_TYPE wear_loc_strings[] =
{
    {	"in the inventory",	WEAR_NONE,		FALSE	},
    {	"as a light",		WEAR_LIGHT,		TRUE	},
    {	"on the left finger",	WEAR_FINGER_L,		TRUE	},
    {	"on the right finger",	WEAR_FINGER_R,		TRUE	},
    {	"around the neck (1)",	WEAR_NECK_1,		TRUE	},
    {	"around the neck (2)",	WEAR_NECK_2,		TRUE	},
    {	"on the body",		WEAR_BODY,		TRUE	},
    {	"over the head",	WEAR_HEAD,		TRUE	},
    {	"on the legs",		WEAR_LEGS,		TRUE	},
    {	"on the feet",		WEAR_FEET,		TRUE	},
    {	"on the hands",		WEAR_HANDS,		TRUE	},
    {	"on the arms",		WEAR_ARMS,		TRUE	},
    {	"as a shield",		WEAR_SHIELD,		TRUE	},
    {	"about the shoulders",	WEAR_ABOUT,		TRUE	},
    {	"around the waist",	WEAR_WAIST,		TRUE	},
    {	"on the left wrist",	WEAR_WRIST_L,		TRUE	},
    {	"on the right wrist",	WEAR_WRIST_R,		TRUE	},
    {	"wielded",		WEAR_WIELD,		TRUE	},
    {	"held in the hands",	WEAR_HOLD,		TRUE	},
    {	"floating",		WEAR_FLOAT,		TRUE	},
    {	"wielded secondary",	WEAR_SECONDARY,		TRUE	},
    {	NULL,			0				}
};


/*
 * What is typed.
 * Neck2 should not be settable for loaded mobiles.
 */
const OPTION_TYPE wear_loc_options[] =
{
    {	"none",			WEAR_NONE,		FALSE	},
    {	"light",		WEAR_LIGHT,		TRUE	},
    {	"lfinger",		WEAR_FINGER_L,		TRUE	},
    {	"rfinger",		WEAR_FINGER_R,		TRUE	},
    {	"neck1",		WEAR_NECK_1,		TRUE	},
    {	"neck2",		WEAR_NECK_2,		TRUE	},
    {	"body",			WEAR_BODY,		TRUE	},
    {	"head",			WEAR_HEAD,		TRUE	},
    {	"legs",			WEAR_LEGS,		TRUE	},
    {	"feet",			WEAR_FEET,		TRUE	},
    {	"hands",		WEAR_HANDS,		TRUE	},
    {	"arms",			WEAR_ARMS,		TRUE	},
    {	"shield",		WEAR_SHIELD,		TRUE	},
    {	"about",		WEAR_ABOUT,		TRUE	},
    {	"waist",		WEAR_WAIST,		TRUE	},
    {	"lwrist",		WEAR_WRIST_L,		TRUE	},
    {	"rwrist",		WEAR_WRIST_R,		TRUE	},
    {	"wielded",		WEAR_WIELD,		TRUE	},
    {	"hold",			WEAR_HOLD,		TRUE	},
    {	"floating",		WEAR_FLOAT,		TRUE	},
    {	"secondary",		WEAR_SECONDARY,		TRUE	},
    {	NULL,			0,				}
};


const OPTION_TYPE container_options[] =
{
    {	"closeable",		CONT_CLOSEABLE,		TRUE	},
    {	"pickproof",		CONT_PICKPROOF,		TRUE	},
    {	"closed",		CONT_CLOSED,		TRUE	},
    {	"locked",		CONT_LOCKED,		TRUE	},
    {	"put_on",		CONT_PUT_ON,		TRUE	},
    {	"trap",			CONT_TRAP,		TRUE	},
    {	"trap_darts",		CONT_TRAP_DARTS,	TRUE    },
    {	"trap_poison",		CONT_TRAP_POISON,	TRUE	},
    {	"trap_exploding",	CONT_TRAP_EXPLODING,	TRUE	},
    {	"trap_extract",		CONT_TRAP_EXTRACT,	TRUE	},
    {	"trap_sleepgas",	CONT_TRAP_SLEEPGAS,	TRUE	},
    {	"trap_death",		CONT_TRAP_DEATH,	TRUE	},
    {	"nogetall",		CONT_NOGETALL,		TRUE	},
    {	NULL,			0,			0	}
};




const OPTION_TYPE weaponflag_options[] = {
    {	"flaming",		WEAPON_FLAMING,		TRUE,	"Adds a chance of fire-effect with every hit"	},
    {	"frost",		WEAPON_FROST,		TRUE,	"Adds a chance of cold-effect with every hit"	},
    {	"vampiric",		WEAPON_VAMPIRIC,	TRUE,	"Weapon can remove HP from victim and add to attacker."	},
    {	"sharp",		WEAPON_SHARP,		TRUE,	"weapon does 125% damage"	},
    {	"vorpal",		WEAPON_VORPAL,		TRUE,	"Only effect seems to be: weapon can't become poisened"	},
    {	"two_hands",		WEAPON_TWO_HANDS,	TRUE,	"Chars need 2 hands to wield this"	},
    {	"shocking",		WEAPON_SHOCKING,	TRUE,	"Adds a chance of shock-effect with every hit"	},
    {	"poison",		WEAPON_POISON,		TRUE,	"Weapon is poisened"	},
    {	NULL,			0,			0	}
};

const OPTION_TYPE gate_options[] = {
    {	"normal_exit",		GATE_NORMAL_EXIT,	TRUE,	"No magic involved."	},
    {	"no_curse",		GATE_NOCURSE,		TRUE,	"Cursed char shall not pass"	},
    {	"go_with",		GATE_GOWITH,		TRUE,	"Obj ends up on the other side after the traveler passes"	},
    {	"buggy",		GATE_BUGGY,		TRUE,	"5% chance of ending up in a random room"	},
    {	"random",		GATE_RANDOM,		TRUE,	"Destination is random"	},
    {	"climb",		GATE_CLIMB,		TRUE,	"You have to climb in"	},
    {	"jump",			GATE_JUMP,		TRUE,	"You have to jump in"	},
    {	"store",		GATE_STORE,		TRUE,	"Store the roomnumber the char came from"	},
    {	"recall",		GATE_RECALL,		TRUE,	"Destination is the last stored roomnumber"	},
    {	"nogetall",		GATE_NOGETALL,		TRUE,	"Limit 'get all ...' to 1 item"	},
    {	NULL,			0,			0	}
};

const OPTION_TYPE furniture_options[] = {
    {	"stand_at",		STAND_AT,		TRUE	},
    {	"stand_on",		STAND_ON,		TRUE	},
    {	"stand_in",		STAND_IN,		TRUE	},
    {	"sit_at",		SIT_AT,			TRUE	},
    {	"sit_on",		SIT_ON,			TRUE	},
    {	"sit_in",		SIT_IN,			TRUE	},
    {	"rest_at",		REST_AT,		TRUE	},
    {	"rest_on",		REST_ON,		TRUE	},
    {	"rest_in",		REST_IN,		TRUE	},
    {	"sleep_at",		SLEEP_AT,		TRUE	},
    {	"sleep_on",		SLEEP_ON,		TRUE	},
    {	"sleep_in",		SLEEP_IN,		TRUE	},
    {	"put_at",		PUT_AT,			TRUE	},
    {	"put_on",		PUT_ON,			TRUE	},
    {	"put_in",		PUT_IN,			TRUE	},
    {	"put_inside",		PUT_INSIDE,		TRUE	},
    {	NULL,			0,			0	}
};
