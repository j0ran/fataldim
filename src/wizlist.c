/* $Id: wizlist.c,v 1.8 2001/08/14 05:46:03 edwin Exp $ */
#include <sys/types.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#if !defined(__FreeBSD__)
#include <malloc.h>
#endif

#include <sys/dir.h>

typedef char bool;

#define MAX_STRING_LENGTH 1024
#define MAX_INPUT_LENGTH 1024
#define PLAYER_DIR "../player/"
#define TRUE (-1)
#define FALSE (0)
#define LOWER(c)		((c) >= 'A' && (c) <= 'Z' ? (c)+'a'-'A' : (c))
#define UPPER(c)		((c) >= 'a' && (c) <= 'z' ? (c)+'A'-'a' : (c))

/*
 * Player info structure.
 */
struct player_info {
    int level;
    time_t lastlogin;
    bool online;
    int clan;
    int rank;
    char name[80];
    char email[80];
    char title[160];
};

bool str_cmp( const char *astr, const char *bstr )
{
    if ( astr == NULL )
    {
        return TRUE;
    }

    if ( bstr == NULL )
    {
        return TRUE;
    }

    for ( ; *astr || *bstr; astr++, bstr++ )
    {
        if ( LOWER(*astr) != LOWER(*bstr) )
            return TRUE;
    }

    return FALSE;
}

char *capitalize( const char *str )
{
    static char strcap[MAX_STRING_LENGTH];
    int i;

    for ( i = 0; str[i] != '\0'; i++ )
        strcap[i] = LOWER(str[i]);
    strcap[i] = '\0';
    strcap[0] = UPPER(strcap[0]);
    return strcap;
}

char *one_argument( char *argument, char *arg_first )
{
    char cEnd;

    while ( isspace(*argument) )
        argument++;

    cEnd = ' ';
    if ( *argument == '\'' || *argument == '"' )
        cEnd = *argument++;

    while ( *argument != '\0' )
    {
        if ( *argument == cEnd )
        {
            argument++;
            break;
        }
        *arg_first = LOWER(*argument);
        arg_first++;
        argument++;
    }
    *arg_first = '\0';

    while ( isspace(*argument) )
        argument++;

    return argument;
}


bool getplayerinfo(char *name,struct player_info *pi)
{
    char strsave[MAX_INPUT_LENGTH];
    char buf[MAX_STRING_LENGTH];
    char word[MAX_STRING_LENGTH];
    char *arg;
    char *s;
    FILE *fp;

    pi->level=-1;
    pi->lastlogin=0;
    pi->clan=0;
    pi->rank=0;
    pi->online=FALSE;
    strcpy(pi->name,name);
    strcpy(pi->email,"none");
    pi->title[0]='\0';

    sprintf( strsave, "%s%s", PLAYER_DIR, capitalize( name ) );
    if ( name[0]=='.' || ( fp = fopen( strsave, "r" ) ) == NULL )
        return FALSE;

    while(fgets(buf,sizeof(buf),fp))
    {
        arg=one_argument( buf, word );

        if(pi->level==-1 && (!str_cmp(word,"Level") || !str_cmp(word,"Levl")))
            pi->level=atoi(arg);
        else if(pi->lastlogin==0 && !str_cmp(word,"LogO"))
            pi->lastlogin=atoi(arg);
        else if(!str_cmp(word,"Rank"))
            pi->rank=atoi(arg);
        else if(!str_cmp(word,"Mail"))
        {
            if((s=strchr(arg,'~'))) *s='\0';
            strncpy(pi->email,arg,sizeof(pi->email));
            pi->email[sizeof(pi->email)-1]='\0';
        }
        else if(!str_cmp(word,"Titl"))
        {
            if((s=strchr(arg,'~'))) *s='\0';
            strncpy(pi->title,arg,sizeof(pi->title));
            pi->title[sizeof(pi->title)-1]='\0';
        }
        else if(!str_cmp(word,"End")) break;
    }

    fclose(fp);
    return TRUE;
}

char *sword[35]={
    "        ________        ",
    "      /+_+_+_+_+_\\      ",
    "      \\__________/      ",
    "        |::XXXX|        ",
    "        |X::XXX|        ",
    "        |XX::XX|        ",
    "        |XX::XX|        ",
    "        |XXX::X|        ",
    "        |XXXX::|        ",
    "        |::XXXX|        ",
    "        |X::XXX|        ",
    "        |XX::XX|        ",
    "        |XXXX::|        ",
    "        |::XXXX|        ",
    "/<<>>\\/<<>>\\/<<>>\\/<<>>\\",
    "\\<<>>/\\<<>>/\\<<>>/\\<<>>/",	/* 15 */


    "       |.  /  . |       ",		/* 16 */
    "       | .//\\.  |       ",
    "       |.  /  . |       ",
    "       | .//\\.  |       ",
    "       |.  /  . |       ",
    "       | .//\\.  |       ",
    "       |.  /  . |       ",
    "       | .//\\.  |       ",
    "       | .//\\.  |       ",
    "       |.  /  . |       ",
    "       | .//\\.  |       ",
    "       | .//\\.  |       ",
    "       |.  /\\ . |       ",
    "       | .//\\.  |       ",	/* 29 */


    "        \\ //\\. /        ",	/* 30 */
    "         \\ /. /         ",
    "          \\ //          ",
    "           \\/           ",
    "                        "		/* 34 */
};

struct player_info wizards[9][50];

char *levels[9]={
    ">>>  Implementors  <<<",
    ">>>    Creators    <<<",
    ">>>   Supremeties  <<<",
    ">>>    Deities     <<<",
    ">>>      Gods      <<<",
    ">>>   Immortals    <<<",
    ">>>    Demigods    <<<",
    ">>>     Angels     <<<",
    ">>>    Avantars    <<<"
};

int next_sword_line(int cur)
{
    if(cur<29) return cur+1;
    return 16;
}

void print_html(int html,char *s)
{
    if(html)
    {
        while(*s)
        {
            switch(*s) {
            case '<': printf("&lt;");break;
            case '>': printf("&gt;");break;
            case '&': printf("&amp;");break;
            case '{': if(s[1]=='{') printf("{");
                if(s[1]) s++;
                break;
            default : putchar(*s);break;
            }
            s++;
        }
    }
    else printf("%s",s);
}

int main(int argc,char *argv[])
{
    bool wizfound;
    DIR *dp;
    struct dirent *de;
    int i,j,k,lev,l;
    struct player_info pi;
    int days=30;
    int html=0;
    char buf[1000];

    for(i=1;i<argc;i++)
    {
        if(argv[i][0]=='-')
        {
            switch(argv[i][1]) {
            case 'd': days=atoi(argv[i]+2);break;
            case 'h': html=TRUE;break;
            default : goto showarg;
            }
        }
        else goto showarg;
    }


    wizfound=FALSE;
    dp=opendir(PLAYER_DIR);

    while((de=readdir(dp))!=NULL)
    {
        if(de->d_name[0]=='.') continue;

        if(getplayerinfo(de->d_name,&pi))
        {
            if(pi.level<=91) continue;
            if((difftime(time(NULL),pi.lastlogin)/86400)>days) continue;

            i=100-pi.level;
            for(j=0;j<50;j++) if(wizards[i][j].level==0)
            {
                wizards[i][j]=pi;
                wizfound=TRUE;
                break;
            }
        }
    }

    closedir(dp);

    /* Generate wizlist output */
    if(html)
    {
        printf("<HTML>\n"
               "<HEAD><TITLE>Fatal Dimensions Wizlist</TITLE></HEAD>\n"
               "\n"
               "<BODY BGCOLOR=\"#FFFFFF\">\n"
               "<H1 align=Center>Fatal Dimensions Wizlist</H1>\n");
    }
    else
    {
        printf("#HELPS\n"
               "-1 WIZLIST~\n"
               ".\n");
    }

    if(!wizfound)
    {
        if(html)
        {
            printf("<P>The wizlist will be updated after the next reboot.</P>\n\r");
            printf("<HR>\n"
                   "Created by Joran's Wizlist Creator.\n");
            printf("</BODY></HTML>\n");
        }
        else
        {
            printf("Wizlist will be updated after the next reboot.\n\n");
            printf("-Created by Joran's Wizlist Creator.\n"
                   "~\n\n"
                   "0 $~\n"
                   "#$\n");
        }

        exit(0);
    }

    if(html) printf("<CODE><PRE>\n");

    lev=0;
    for(i=0;i<9;i++)
    {
        for(j=0;j<50;j++) if(wizards[i][j].level) break;
        if(j!=50) break;
    }
    k=0;l=0;
    for(;;)
    {
        if(i<9)
        {
            if(lev!=wizards[i][j].level)
            {
                if(i!=0)
                {
                    sprintf(buf,"%s\n",sword[k]);
                    print_html(html,buf);
                    k=next_sword_line(k);
                }
                sprintf(buf,"%s %s\n",sword[k],levels[i]);
                print_html(html,buf);
                k=next_sword_line(k);
                sprintf(buf,"%s\n",sword[k]);
                print_html(html,buf);
                k=next_sword_line(k);
                lev=wizards[i][j].level;
                l+=3;
            }
            sprintf(buf,"%s %s %s\n",sword[k],wizards[i][j].name,wizards[i][j].title);
            print_html(html,buf);
        }
        else
        {
            sprintf(buf,"%s\n",sword[k]);
            print_html(html,buf);
        }
        k=next_sword_line(k);
        l++;

        wizards[i][j].level=0;
        for(i=0;i<9;i++)
        {
            for(j=0;j<50;j++) if(wizards[i][j].level) break;
            if(j!=50) break;
        }
        if(i==9) break; /* No more wizards */
    }

    if(l>=30) k=30;
    for(;k<35;k++)
    {
        sprintf(buf,"%s\n",sword[k]);
        print_html(html,buf);
    }

    if (html) {
        printf("</PRE></CODE>\n");
        printf("<HR>\n"
               "Created by Joran's Wizlist Creator.\n");
        printf("</BODY></HTML>\n");
    } else {
        printf("-Created by Joran's Wizlist Creator.\n"
               "~\n\n"
               "0 $~\n"
               "#$\n");
    }

    exit(0);

showarg:
    printf("%s [-d<days>] [-h]\n",argv[0]);
    exit(1);
}
