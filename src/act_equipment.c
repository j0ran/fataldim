//
// $Id: act_equipment.c,v 1.9 2008/03/30 11:30:14 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"

/*
 * Remove an object.
 */
bool remove_obj( CHAR_DATA *ch, int iWear, bool fReplace, bool show ) {
    OBJ_DATA *obj;

    if ( ( obj = get_eq_char( ch, iWear ) ) == NULL )
	return TRUE;

    if ( !fReplace )
	return FALSE;

    if ( STR_IS_SET(obj->strbit_extra_flags, ITEM_NOREMOVE) ) {
	act( "You can't remove $p.", ch, obj, NULL, TO_CHAR );
	return FALSE;
    }

    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREREMOVE ) )
	if (op_preremove_trigger( obj, ch,iWear )==FALSE)
	    return FALSE;
    if (!IS_VALID(obj))
	return FALSE;

    unequip_char( ch, obj );
    if (show) {
	act( "$n stops using $p.", ch, obj, NULL, TO_ROOM );
	act( "You stop using $p.", ch, obj, NULL, TO_CHAR );
    }
    if (IS_PC(ch))
	STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);

    if(iWear==WEAR_WIELD)
    {
        OBJ_DATA *sec;

        sec=get_eq_char(ch,WEAR_SECONDARY);
        if(sec)
        {
          unequip_char( ch, sec);
	  equip_char( ch, sec, WEAR_WIELD );
	  if (show) {
	      act( "$n now uses $p as primary weapon.",ch,sec,NULL,TO_ROOM);
	      act( "You now use $p as primary weapon.",ch,sec,NULL,TO_CHAR);
	  }

          return FALSE;
        }
    }

    if (OBJ_HAS_TRIGGER(obj,OTRIG_REMOVE))
	op_remove_trigger(obj,ch,iWear);
    if (!IS_VALID(obj))
	return FALSE;

    return TRUE;
}

/*
 * Wear one object.
 * Optional replacement of existing objects.
 * Big repetitive code, ick.
 */
void wear_obj( CHAR_DATA *ch, OBJ_DATA *obj, bool fReplace, bool show ) {
    if ( !IS_NPC(ch) && ch->level < get_obj_level(obj) ) {
	sprintf_to_char(ch, "You must be level %d to use this object.\n\r",
	    get_obj_level(obj) );
	if (show) {
	    act( "$n tries to use $p, but is too inexperienced.",
		ch, obj, NULL, TO_ROOM );
	}
	return;
    }
    if (!IS_IMMORTAL(ch) && obj_is_owned_by_another(ch,obj)) {
	act("$p is owned by somebody else.",ch,obj,NULL,TO_CHAR);
	return;
    }

    if ( obj->item_type == ITEM_LIGHT )
    {
	if ( !remove_obj( ch, WEAR_LIGHT, fReplace, show ) )
	    return;
	if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREWEAR ) )
	    if (op_prewear_trigger( obj, ch, WEAR_LIGHT )==FALSE)
		return;
	if (!IS_VALID(obj))
	    return;
	if (show) {
	    act( "$n lights $p and holds it.", ch, obj, NULL, TO_ROOM );
	    act( "You light $p and hold it.",  ch, obj, NULL, TO_CHAR );
	}
	equip_char( ch, obj, WEAR_LIGHT );
	if (OBJ_HAS_TRIGGER(obj,OTRIG_WEAR))
	    op_wear_trigger(obj,ch,WEAR_LIGHT);
	if (!IS_VALID(obj))
	    return;
	return;
    }

    if ( CAN_WEAR( obj, ITEM_WEAR_FINGER ) )
    {
	if ( get_eq_char( ch, WEAR_FINGER_L ) != NULL
	&&   get_eq_char( ch, WEAR_FINGER_R ) != NULL
	&&   !remove_obj( ch, WEAR_FINGER_L, fReplace, show )
	&&   !remove_obj( ch, WEAR_FINGER_R, fReplace, show ) )
	    return;

	if ( get_eq_char( ch, WEAR_FINGER_L ) == NULL )
	{
	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREWEAR ) )
		if (op_prewear_trigger( obj, ch,WEAR_FINGER_L )==FALSE)
		    return;
	    if (!IS_VALID(obj))
		return;
	    if (show) {
		act("$n wears $p on $s left finger.",  ch,obj,NULL,TO_ROOM );
		act("You wear $p on your left finger.",ch,obj,NULL,TO_CHAR );
	    }
	    equip_char( ch, obj, WEAR_FINGER_L );
	    if (OBJ_HAS_TRIGGER(obj,OTRIG_WEAR))
		op_wear_trigger(obj,ch,WEAR_FINGER_L);
	    if (!IS_VALID(obj))
		return;
	    return;
	}

	if ( get_eq_char( ch, WEAR_FINGER_R ) == NULL )
	{
	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREWEAR ) )
		if (op_prewear_trigger( obj, ch,WEAR_FINGER_R )==FALSE)
		    return;
	    if (!IS_VALID(obj))
		return;
	    if (show) {
		act("$n wears $p on $s right finger.",  ch,obj,NULL,TO_ROOM);
		act("You wear $p on your right finger.",ch,obj,NULL,TO_CHAR);
	    }
	    equip_char( ch, obj, WEAR_FINGER_R );
	    if (OBJ_HAS_TRIGGER(obj,OTRIG_WEAR))
		op_wear_trigger(obj,ch,WEAR_FINGER_R);
	    if (!IS_VALID(obj))
		return;
	    return;
	}

	bugf( "Wear_obj: no free finger for %s.",NAME(ch));
	send_to_char( "You already wear two rings.\n\r", ch );
	return;
    }

    if ( CAN_WEAR( obj, ITEM_WEAR_NECK ) )
    {
	if ( get_eq_char( ch, WEAR_NECK_1 ) != NULL
	&&   get_eq_char( ch, WEAR_NECK_2 ) != NULL
	&&   !remove_obj( ch, WEAR_NECK_1, fReplace, show )
	&&   !remove_obj( ch, WEAR_NECK_2, fReplace, show ) )
	    return;

	if ( get_eq_char( ch, WEAR_NECK_1 ) == NULL )
	{
	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREWEAR ) )
		if (op_prewear_trigger( obj, ch, WEAR_NECK_1 )==FALSE)
		    return;
	    if (!IS_VALID(obj))
		return;
	    if (show) {
		act( "$n wears $p around $s neck.",   ch, obj, NULL, TO_ROOM );
		act( "You wear $p around your neck.", ch, obj, NULL, TO_CHAR );
	    }
	    equip_char( ch, obj, WEAR_NECK_1 );
	    if (OBJ_HAS_TRIGGER(obj,OTRIG_WEAR))
		op_wear_trigger(obj,ch, WEAR_NECK_1);
	    if (!IS_VALID(obj))
		return;
	    return;
	}

	if ( get_eq_char( ch, WEAR_NECK_2 ) == NULL )
	{
	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREWEAR) )
		if (op_prewear_trigger( obj, ch, WEAR_NECK_2 )==FALSE)
		    return;
	    if (!IS_VALID(obj))
		return;
	    if (show) {
		act( "$n wears $p around $s neck.",   ch, obj, NULL, TO_ROOM );
		act( "You wear $p around your neck.", ch, obj, NULL, TO_CHAR );
	    }
	    equip_char( ch, obj, WEAR_NECK_2 );
	    if (OBJ_HAS_TRIGGER(obj,OTRIG_WEAR))
		op_wear_trigger(obj,ch,WEAR_NECK_2);
	    if (!IS_VALID(obj))
		return;
	    return;
	}

	bugf( "Wear_obj: no free neck for %s.",NAME(ch));
	send_to_char( "You already wear two neck items.\n\r", ch );
	return;
    }

    if ( CAN_WEAR( obj, ITEM_WEAR_BODY ) )
    {
	if ( !remove_obj( ch, WEAR_BODY, fReplace, show ) )
	    return;
	if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREWEAR ) )
	    if (op_prewear_trigger( obj, ch, WEAR_BODY )==FALSE)
		return;
	if (!IS_VALID(obj))
	    return;
	if (show) {
	    act( "$n wears $p on $s body.",   ch, obj, NULL, TO_ROOM );
	    act( "You wear $p on your body.", ch, obj, NULL, TO_CHAR );
	}
	equip_char( ch, obj, WEAR_BODY );
	if (OBJ_HAS_TRIGGER(obj,OTRIG_WEAR))
	    op_wear_trigger(obj,ch, WEAR_BODY);
	if (!IS_VALID(obj))
	    return;
	return;
    }

    if ( CAN_WEAR( obj, ITEM_WEAR_HEAD ) )
    {
	if ( !remove_obj( ch, WEAR_HEAD, fReplace, show ) )
	    return;
	if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREWEAR ) )
	    if (op_prewear_trigger( obj, ch, WEAR_HEAD )==FALSE)
		return;
	if (!IS_VALID(obj))
	    return;
	if (show) {
	    act( "$n wears $p on $s head.",   ch, obj, NULL, TO_ROOM );
	    act( "You wear $p on your head.", ch, obj, NULL, TO_CHAR );
	}
	equip_char( ch, obj, WEAR_HEAD );
	if (OBJ_HAS_TRIGGER(obj,OTRIG_WEAR))
	    op_wear_trigger(obj,ch,WEAR_HEAD);
	if (!IS_VALID(obj))
	    return;
	return;
    }

    if ( CAN_WEAR( obj, ITEM_WEAR_LEGS ) )
    {
	if ( !remove_obj( ch, WEAR_LEGS, fReplace, show ) )
	    return;
	if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREWEAR ) )
	    if (op_prewear_trigger( obj, ch, WEAR_LEGS )==FALSE)
		return;
	if (!IS_VALID(obj))
	    return;
	if (show) {
	    act( "$n wears $p on $s legs.",   ch, obj, NULL, TO_ROOM );
	    act( "You wear $p on your legs.", ch, obj, NULL, TO_CHAR );
	}
	equip_char( ch, obj, WEAR_LEGS );
	if (OBJ_HAS_TRIGGER(obj,OTRIG_WEAR))
	    op_wear_trigger(obj,ch,WEAR_LEGS);
	if (!IS_VALID(obj))
	    return;
	return;
    }

    if ( CAN_WEAR( obj, ITEM_WEAR_FEET ) )
    {
	if ( !remove_obj( ch, WEAR_FEET, fReplace, show ) )
	    return;
	if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREWEAR ) )
	    if (op_prewear_trigger( obj, ch, WEAR_FEET )==FALSE)
		return;
	if (!IS_VALID(obj))
	    return;
	if (show) {
	    act( "$n wears $p on $s feet.",   ch, obj, NULL, TO_ROOM );
	    act( "You wear $p on your feet.", ch, obj, NULL, TO_CHAR );
	}
	equip_char( ch, obj, WEAR_FEET );
	if (OBJ_HAS_TRIGGER(obj,OTRIG_WEAR))
	    op_wear_trigger(obj,ch,WEAR_FEET);
	if (!IS_VALID(obj))
	    return;
	return;
    }

    if ( CAN_WEAR( obj, ITEM_WEAR_HANDS ) )
    {
	if ( !remove_obj( ch, WEAR_HANDS, fReplace, show ) )
	    return;
	if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREWEAR ) )
	    if (op_prewear_trigger( obj, ch, WEAR_HANDS )==FALSE)
		return;
	if (!IS_VALID(obj))
	    return;
	if (show) {
	    act( "$n wears $p on $s hands.",   ch, obj, NULL, TO_ROOM );
	    act( "You wear $p on your hands.", ch, obj, NULL, TO_CHAR );
	}
	equip_char( ch, obj, WEAR_HANDS );
	if (OBJ_HAS_TRIGGER(obj,OTRIG_WEAR))
	    op_wear_trigger(obj,ch,WEAR_HANDS);
	if (!IS_VALID(obj))
	    return;
	return;
    }

    if ( CAN_WEAR( obj, ITEM_WEAR_ARMS ) )
    {
	if ( !remove_obj( ch, WEAR_ARMS, fReplace, show ) )
	    return;
	if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREWEAR ) )
	    if (op_prewear_trigger( obj, ch, WEAR_ARMS )==FALSE)
		return;
	if (!IS_VALID(obj))
	    return;
	if (show) {
	    act( "$n wears $p on $s arms.",   ch, obj, NULL, TO_ROOM );
	    act( "You wear $p on your arms.", ch, obj, NULL, TO_CHAR );
	}
	equip_char( ch, obj, WEAR_ARMS );
	if (OBJ_HAS_TRIGGER(obj,OTRIG_WEAR))
	    op_wear_trigger(obj,ch,WEAR_ARMS);
	if (!IS_VALID(obj))
	    return;
	return;
    }

    if ( CAN_WEAR( obj, ITEM_WEAR_ABOUT ) )
    {
	if ( !remove_obj( ch, WEAR_ABOUT, fReplace, show ) )
	    return;
	if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREWEAR ) )
	    if (op_prewear_trigger( obj, ch, WEAR_ABOUT )==FALSE)
		return;
	if (!IS_VALID(obj))
	    return;
	if (show) {
	    act( "$n wears $p about $s body.",   ch, obj, NULL, TO_ROOM );
	    act( "You wear $p about your body.", ch, obj, NULL, TO_CHAR );
	}
	equip_char( ch, obj, WEAR_ABOUT );
	if (OBJ_HAS_TRIGGER(obj,OTRIG_WEAR))
	    op_wear_trigger(obj,ch,WEAR_ABOUT);
	if (!IS_VALID(obj))
	    return;
	return;
    }

    if ( CAN_WEAR( obj, ITEM_WEAR_WAIST ) )
    {
	if ( !remove_obj( ch, WEAR_WAIST, fReplace, show ) )
	    return;
	if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREWEAR ) )
	    if (op_prewear_trigger( obj, ch, WEAR_WAIST )==FALSE)
		return;
	if (!IS_VALID(obj))
	    return;
	if (show) {
	    act( "$n wears $p about $s waist.",   ch, obj, NULL, TO_ROOM );
	    act( "You wear $p about your waist.", ch, obj, NULL, TO_CHAR );
	}
	equip_char( ch, obj, WEAR_WAIST );
	if (OBJ_HAS_TRIGGER(obj,OTRIG_WEAR))
	    op_wear_trigger(obj,ch, WEAR_WAIST);
	if (!IS_VALID(obj))
	    return;
	return;
    }

    if ( CAN_WEAR( obj, ITEM_WEAR_WRIST ) )
    {
	if ( get_eq_char( ch, WEAR_WRIST_L ) != NULL
	&&   get_eq_char( ch, WEAR_WRIST_R ) != NULL
	&&   !remove_obj( ch, WEAR_WRIST_L, fReplace, show )
	&&   !remove_obj( ch, WEAR_WRIST_R, fReplace, show ) )
	    return;

	if ( get_eq_char( ch, WEAR_WRIST_L ) == NULL )
	{
	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREWEAR ) )
		if (op_prewear_trigger( obj, ch, WEAR_WRIST_L )==FALSE)
		    return;
	    if (!IS_VALID(obj))
		return;
	    if (show) {
		act( "$n wears $p around $s left wrist.",ch,obj,NULL,TO_ROOM );
		act( "You wear $p around your left wrist.",
		    ch, obj, NULL, TO_CHAR );
	    }
	    equip_char( ch, obj, WEAR_WRIST_L );
	    if (OBJ_HAS_TRIGGER(obj,OTRIG_WEAR))
		op_wear_trigger(obj,ch, WEAR_WRIST_L);
	    if (!IS_VALID(obj))
		return;
	    return;
	}

	if ( get_eq_char( ch, WEAR_WRIST_R ) == NULL )
	{
	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREWEAR ) )
		if (op_prewear_trigger( obj, ch, WEAR_WRIST_R )==FALSE)
		    return;
	    if (!IS_VALID(obj))
		return;
	    if (show) {
		act( "$n wears $p around $s right wrist.",ch,obj,NULL,TO_ROOM );
		act( "You wear $p around your right wrist.",
		    ch, obj, NULL, TO_CHAR );
	    }
	    equip_char( ch, obj, WEAR_WRIST_R );
	    if (OBJ_HAS_TRIGGER(obj,OTRIG_WEAR))
		op_wear_trigger(obj,ch, WEAR_WRIST_R);
	    if (!IS_VALID(obj))
		return;
	    return;
	}

	bugf( "Wear_obj: no free wrist for %s.",NAME(ch));
	send_to_char( "You already wear two wrist items.\n\r", ch );
	return;
    }

    if ( CAN_WEAR( obj, ITEM_WEAR_SHIELD ) )
    {
	OBJ_DATA *weapon;

	if ( !remove_obj( ch, WEAR_SHIELD, fReplace, show ) )
	    return;

	weapon = get_eq_char(ch,WEAR_WIELD);
	if (weapon != NULL && ch->size < SIZE_LARGE
	&&  IS_WEAPON_STAT(weapon,WEAPON_TWO_HANDS)) {
	    send_to_char("Both your hands are needed for your weapon!\n\r",ch);
	    return;
	}
        if ( get_eq_char (ch, WEAR_SECONDARY) != NULL)
        {
            send_to_char(
		"You cannot use a shield while using two weapons.\n\r",ch);
            return;
        }

	if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREWEAR ) )
	    if (op_prewear_trigger( obj, ch, WEAR_SHIELD )==FALSE)
		return;
	if (!IS_VALID(obj))
	    return;
	if (show) {
	    act( "$n wears $p as a shield.", ch, obj, NULL, TO_ROOM );
	    act( "You wear $p as a shield.", ch, obj, NULL, TO_CHAR );
	}
	equip_char( ch, obj, WEAR_SHIELD );
	if (OBJ_HAS_TRIGGER(obj,OTRIG_WEAR))
	    op_wear_trigger(obj,ch, WEAR_SHIELD);
	if (!IS_VALID(obj))
	    return;
	return;
    }

    if ( CAN_WEAR( obj, ITEM_WIELD ) )
    {
	int sn,skill;

	if ( !remove_obj( ch, WEAR_WIELD, fReplace, show ) )
	    return;

	if ( !IS_NPC(ch)
	&& get_obj_weight(obj) > (str_app[get_curr_stat(ch,STAT_STR)].wield
		* 10))
	{
	    send_to_char( "It is too heavy for you to wield.\n\r", ch );
	    return;
	}

	if (!IS_NPC(ch) && ch->size < SIZE_LARGE
	&&  IS_WEAPON_STAT(obj,WEAPON_TWO_HANDS)
	&&  get_eq_char(ch,WEAR_SHIELD) != NULL) {
	    send_to_char("You need two hands free for that weapon.\n\r",ch);
	    return;
	}

	if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREWEAR ) )
	    if (op_prewear_trigger( obj, ch, WEAR_WIELD )==FALSE)
		return;
	if (!IS_VALID(obj))
	    return;
	if (show) {
	    act( "$n wields $p.", ch, obj, NULL, TO_ROOM );
	    act( "You wield $p.", ch, obj, NULL, TO_CHAR );
	}
	equip_char( ch, obj, WEAR_WIELD );

        sn = get_weapon_sn(ch,FALSE);

	if (sn == gsn_hand_to_hand)
	   return;

        skill = get_weapon_skill(ch,sn);

	show_weapon_exp(ch,obj,skill);

	if (OBJ_HAS_TRIGGER(obj,OTRIG_WEAR))
	    op_wear_trigger(obj,ch, WEAR_WIELD);
	if (!IS_VALID(obj))
	    return;

	return;
    }

    if ( CAN_WEAR( obj, ITEM_HOLD ) )
    {
	if ( !remove_obj( ch, WEAR_HOLD, fReplace, show ) )
	    return;

        if (get_eq_char (ch, WEAR_SECONDARY) != NULL)
        {
	    if ( !remove_obj( ch, WEAR_SECONDARY, fReplace, show ) ) {
		send_to_char ("You cannot hold an item while using two weapons.\n\r",ch);
		return;
	    }
        }

	if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREWEAR ) )
	    if (op_prewear_trigger( obj, ch, WEAR_HOLD )==FALSE)
		return;
	if (!IS_VALID(obj))
	    return;
	if (show) {
	    act( "$n holds $p in $s hand.",   ch, obj, NULL, TO_ROOM );
	    act( "You hold $p in your hand.", ch, obj, NULL, TO_CHAR );
	}
	equip_char( ch, obj, WEAR_HOLD );
	if (OBJ_HAS_TRIGGER(obj,OTRIG_WEAR))
	    op_wear_trigger(obj,ch, WEAR_HOLD);
	if (!IS_VALID(obj))
	    return;
	return;
    }

    if ( CAN_WEAR(obj,ITEM_WEAR_FLOAT) )
    {
	if (!remove_obj(ch,WEAR_FLOAT, fReplace, show) )
	    return;
	if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREWEAR ) )
	    if (op_prewear_trigger( obj, ch, WEAR_FLOAT )==FALSE)
		return;
	if (!IS_VALID(obj))
	    return;
	if (show) {
	    act("$n releases $p to float next to $m.",ch,obj,NULL,TO_ROOM);
	    act("You release $p and it floats next to you.",ch,obj,NULL,TO_CHAR);
	}
	equip_char(ch,obj,WEAR_FLOAT);
	if (OBJ_HAS_TRIGGER(obj,OTRIG_WEAR))
	    op_wear_trigger(obj,ch, WEAR_FLOAT);
	if (!IS_VALID(obj))
	    return;
	return;
    }

    if ( fReplace )
	send_to_char( "You can't wear, wield, or hold that.\n\r", ch );

    return;
}



void do_wear( CHAR_DATA *ch, char *argument ) {
    char arg[MAX_INPUT_LENGTH];
    OBJ_DATA *obj;

    one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Wear, wield, or hold what?\n\r", ch );
	return;
    }

    if ( !str_cmp( arg, "all" ) )
    {
	OBJ_DATA *obj_next;

	for ( obj = ch->carrying; obj != NULL; obj = obj_next )
	{
	    obj_next = obj->next_content;
	    if ( obj->wear_loc == WEAR_NONE && can_see_obj( ch, obj ) )
		wear_obj( ch, obj, FALSE, TRUE );
	}
	return;
    }
    else
    {
	if ( ( obj = get_obj_carry( ch, arg, ch ) ) == NULL )
	{
	    send_to_char( "You do not have that item.\n\r", ch );
	    return;
	}

	wear_obj( ch, obj, TRUE, TRUE );
    }

    return;
}



void do_remove( CHAR_DATA *ch, char *argument ) {
    char arg[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    OBJ_DATA *obj;
    int wear_loc;

    argument=one_argument(argument,arg);
    one_argument(argument,arg2);

    if ( arg[0] == '\0' )
    {
	send_to_char( "Remove what?\n\r", ch );
	return;
    }

    if (!str_cmp(arg,"from")) {
	if (arg2[0]==0) {
	    send_to_char("Remove from what?\n\r",ch);
	    return;
	}

	wear_loc=option_find_name(arg2,wear_loc_options,TRUE);
	if (wear_loc!=NO_FLAG && wear_loc!=WEAR_NONE) {
	    if (get_eq_char(ch,wear_loc)!=NULL)
		remove_obj(ch,wear_loc,TRUE,TRUE);
	    else
		send_to_char("You're not wearing something on that.\n\r", ch );
	    return;
	}

	send_to_char("Remove from what?\n\rValid locations are:\n\r",ch);
	// skip the "none" ?
	sprintf_to_char(ch,"%s\n\r",option_all(wear_loc_options));
	return;
    }

    if ( ( obj = get_obj_wear( ch, arg ) ) == NULL )
    {
	send_to_char( "You do not have that item.\n\r", ch );
	return;
    }

    remove_obj( ch, obj->wear_loc, TRUE, TRUE );
    return;
}



void do_second (CHAR_DATA *ch, char *argument)
/* wear object as a secondary weapon */
{
    OBJ_DATA *obj,*obj_wielded;
    char buf[MAX_STRING_LENGTH]; /* overkill, but what the heck */
    int sn,skill;

    if(!has_skill_available(ch,gsn_second_weapon))
    {
	send_to_char("You don't know how to use two weapons.\n\r",ch);
	act( "$n tries to wield a second weapon, but doesn't know how.",
	    ch, NULL, NULL, TO_ROOM );
	return;
    }

    if (argument[0] == '\0') /* empty */
    {
        send_to_char ("Wear which weapon in your off-hand?\n\r",ch);
        return;
    }

    obj = get_obj_carry (ch, argument,ch); /* find the obj withing ch's inventory */

    if (obj == NULL)
    {
        send_to_char ("You have no such thing in your backpack.\n\r",ch);
        return;
    }


    /* check if the char is using a shield or a held weapon */

    if ( (get_eq_char (ch,WEAR_SHIELD) != NULL) ||
         (get_eq_char (ch,WEAR_HOLD)   != NULL) )
    {
	if ( !remove_obj( ch, WEAR_SHIELD, TRUE, TRUE )
	||   !remove_obj( ch, WEAR_HOLD, TRUE, TRUE )) {
	    send_to_char ("You cannot use a secondary weapon while using a shield, holding an item or wearing a two-handed weapon\n\r",ch);
	    return;
	}
    }

    if (!CAN_WEAR(obj,ITEM_WIELD)) {
	send_to_char("Try to use a weapon to fight with.\n\r",ch);
	return;
    }

    if ( ch->level < obj->level )
    {
        sprintf( buf, "You must be level %d to use this object.\n\r",
            obj->level );
        send_to_char( buf, ch );
        act( "$n tries to use $p, but is too inexperienced.",
            ch, obj, NULL, TO_ROOM );
        return;
    }

    /* check that the character is using a first weapon at all */
    if ((obj_wielded=get_eq_char (ch, WEAR_WIELD))== NULL) /* oops - != here was a bit wrong :) */
    {
        send_to_char ("You need to wield a primary weapon, before using a secondary one!\n\r",ch);
        return;
    }

    if (IS_WEAPON_STAT(obj_wielded,WEAPON_TWO_HANDS)) {
	act("You need both your hands to carry $p.",
	    ch,obj_wielded,NULL,TO_CHAR);
	return;
    }

    if (IS_WEAPON_STAT(obj,WEAPON_TWO_HANDS)) {
	act("You would need three hands to carry $p also.",
	    ch,obj,NULL,TO_CHAR);
	return;
    }


    /* check for str - secondary weapons have to be lighter */
    if ( get_obj_weight( obj ) > ( str_app[get_curr_stat(ch,STAT_STR)].wield / 2) )
    {
        send_to_char( "This weapon is too heavy to be used as a secondary weapon by you.\n\r", ch );
        return;
    }

    /* check if the secondary weapon is at least half as light as the primary weapon */
    if (get_obj_weight(obj)>10 &&
	( get_obj_weight (obj)*3) > (get_obj_weight(get_eq_char(ch,WEAR_WIELD))*2) )
    {
        send_to_char ("Your secondary weapon has to be lighter than the primary one.\n\r",ch);
        return;
    }

    /* at last - the char uses the weapon */

    if (!remove_obj(ch, WEAR_SECONDARY, TRUE, TRUE)) /* remove the current weapon if any */
        return;                                /* remove obj tells about any no_remove */

    /* char CAN use the item! that didn't take long at aaall */

    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREWEAR ) )
	if (op_prewear_trigger( obj, ch, WEAR_SECONDARY )==FALSE)
	    return;

    act ("$n wields $p in $s off-hand.",ch,obj,NULL,TO_ROOM);
    act ("You wield $p in your off-hand.",ch,obj,NULL,TO_CHAR);
    equip_char ( ch, obj, WEAR_SECONDARY);
    if (OBJ_HAS_TRIGGER(obj,OTRIG_WEAR))
	op_wear_trigger(obj,ch,WEAR_SECONDARY);


    if (IS_PC(ch))
	STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);

    /* show experiance with weapon */
    sn = get_weapon_sn(ch,TRUE);

    if (sn == gsn_hand_to_hand)
        return;

    skill = get_weapon_skill(ch,sn);

    show_weapon_exp(ch,obj,skill);

    return;
}



void show_weapon_exp(CHAR_DATA *ch,OBJ_DATA *obj,int skill) {
    if (skill >= 100)
        act("$p feels like a part of you!",ch,obj,NULL,TO_CHAR);
    else if (skill > 85)
        act("You feel quite confident with $p.",ch,obj,NULL,TO_CHAR);
    else if (skill > 70)
        act("You are skilled with $p.",ch,obj,NULL,TO_CHAR);
    else if (skill > 50)
        act("Your skill with $p is adequate.",ch,obj,NULL,TO_CHAR);
    else if (skill > 25)
        act("$p feels a little clumsy in your hands.",ch,obj,NULL,TO_CHAR);
    else if (skill > 1)
        act("You fumble and almost drop $p.",ch,obj,NULL,TO_CHAR);
    else
        act("You don't even know which end is up on $p.",
            ch,obj,NULL,TO_CHAR);
}
