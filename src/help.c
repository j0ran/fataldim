//
// $Id: help.c,v 1.4 2001/08/20 00:46:59 edwin Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"

void	show_help(CHAR_DATA *ch,char *topic) {
    HELP_DATA *	pHelp;
    BUFFER *	output;
    bool	found=FALSE;
    bool	wiznetwarning=TRUE;
    char	argall[MIL];
    char	argone[MIL];
    char	buf1[MSL],buf2[MSL];
    char *	old_topic=topic;
    char *	arg;
    char *	argtemp;
    int		helpitems=0;

    output=new_buf();

    if (topic[0]==0)
	topic="summary";

    /* this parts handles help a b so that it returns help 'a b' */
    argall[0]=0;
    while (topic[0]!=0) {
	topic=one_argument(topic,argone);
	if (argall[0]!=0)
	    strcat(argall," ");
	strcat(argall,argone);
    }

    arg=one_argument(argall,argone);
    while (argone[0]!=0) {

	helpitems=0;

	for (pHelp=help_first; pHelp!=NULL; pHelp=pHelp->next ) {

	    if (pHelp->level>get_trust(ch)) {
		wiznetwarning=FALSE;
		continue;
	    }

	    if (pHelp->deleted)
		continue;

	    if (argone[1]==0) {
		argtemp=pHelp->keyword;
		argtemp=one_argument(argtemp,buf2);
		while (buf2[0]!=0) {
		    if (toupper(argone[0])==toupper(buf2[0])) {
			sprintf(buf1,"%-19s",buf2);
			add_buf(output,buf1);
			found=TRUE;
			wiznetwarning=FALSE;
			helpitems++;
			if (helpitems%4==0)
			    add_buf(output,"\n\r");
		    }
		    argtemp=one_argument(argtemp,buf2);
		}
		continue;
	    }

	    if (pHelp->keyword[0]=='*'?is_exact_name(argone,pHelp->keyword+1):
		is_name(argone,pHelp->keyword) ) {

		if (found)
		    add_buf(output,
"\n\r============================================================\n\r\n\r");

		if (pHelp->text[0]=='.')
		    add_buf(output,pHelp->text+1);
		else
		    add_buf(output,pHelp->text);

		found=TRUE;
		wiznetwarning=FALSE;

		// small hack :)
		if (ch->desc != NULL
		&&  ch->desc->connected != CON_PLAYING
		&&  ch->desc->connected != CON_GEN_GROUPS)
		    break;
	    }
	}
	if (helpitems!=0 && helpitems%4!=0)
	    add_buf(output,"\n\r");
	arg=one_argument(arg,argone);
    }

    if (wiznetwarning) {
	wiznet(WIZ_HELP,0,ch,NULL,"help topic not found for $N: %s",old_topic);
	logf("[%d] show_help(): topic not found for %s: %s",
	    GET_DESCRIPTOR(ch),ch->name,old_topic);
    }

    if (!found)
	send_to_char("No help on that word.\n\r",ch);
    else
	page_to_char(buf_string(output),ch);

    free_buf(output);
}

void do_help(CHAR_DATA *ch,char *argument) {
    show_help(ch,argument);
}

void do_olchelp(CHAR_DATA *ch,char *argument) {
    show_help(ch,argument);
}

