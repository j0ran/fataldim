/* $Id: fight.c,v 1.167 2008/05/01 19:21:42 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

/*
 * 19980318 EG  Modified one_hit() to reduce the movement-points
		when a char/mob is hit.
 * 19980318 EG  Modified do_flee() to prevent fleeing when 0 movement-points
 */
#include "merc.h"
#include "interp.h"
#include "color.h"

// taken from <math.h>
extern double floor __P((double));

bool in_safe_kill_room(CHAR_DATA *killer, CHAR_DATA *victim)
{
    bool b=FALSE;

    if (IS_NPC(victim))
	return FALSE;
    if (!victim->in_room)
	return FALSE;
    if (!killer)
	return FALSE;
    if (IS_SET(victim->in_room->area->area_flags, AREA_ARENA))
	return TRUE;
    if (IS_SET(victim->in_room->area->area_flags, AREA_PKZONE)
	&&  !IS_NPC(killer)) return TRUE;
    GetRoomProperty(killer->in_room,PROPERTY_BOOL,"is-arena",&b);
    if (b)
	return TRUE;

    return FALSE;
}


/*
 * Control the fights going on.
 * Called periodically by update_handler.
 */
void violence_update( void )
{
    CHAR_DATA *ch;
    CHAR_DATA *victim;

    for ( ch = char_list; ch != NULL; ch = ch->next )
    {
	if (!IS_VALID(ch))
	    return;

        /*
         * Hunting mobs.
         */
	if ( IS_NPC(ch)
	&& ch->fighting == NULL
	&& IS_AWAKE(ch)
	&& ch->hunt_type != HUNT_NONE )
	{
	    hunt_victim(ch);
	    continue;
	}

	if ( ( victim = ch->fighting ) == NULL || ch->in_room == NULL )
	    continue;

	if (ch->phasedskill_proc) {
	    (ch->phasedskill_proc)(ch);
	    if (IS_SET(ch->phasedskill_flags,PHASED_DOESOWNDAMAGE))
		continue;
	}

	if ( IS_AWAKE(ch) && ch->in_room == victim->in_room )
	    multi_hit( ch, victim, TYPE_UNDEFINED );
	else
	    stop_fighting( ch, FALSE );

	if ( ( victim = ch->fighting ) == NULL )
	    continue;

	/*
	 * Fun for the whole family!
	 */
	check_assist(ch,victim);

	if ( IS_NPC( ch ) )
	{
	    if ( MOB_HAS_TRIGGER( ch, MTRIG_FIGHT ) )
		mp_fight_trigger(ch,victim);
	    if (!IS_VALID(ch))
		return;
	    if ( MOB_HAS_TRIGGER( ch, MTRIG_HITPOINTCOUNT ) )
		mp_hpcnt_trigger( ch, victim );
	    if (!IS_VALID(ch))
		return;
	}
    }

    return;
}

/* for auto assisting */
void check_assist(CHAR_DATA *ch,CHAR_DATA *victim)
{
    CHAR_DATA *rch, *rch_next;

    for (rch = ch->in_room->people; rch != NULL; rch = rch_next)
    {
	rch_next = rch->next_in_room;

	if (IS_AWAKE(rch) && rch->fighting == NULL && can_see(rch,victim))
	{

	    /* quick check for ASSIST_PLAYER */
	    if (!IS_NPC(ch) && IS_NPC(rch)
	    && STR_IS_SET(rch->strbit_off_flags,ASSIST_PLAYERS)
	    &&  rch->level + 6 > victim->level)
	    {
		do_function(rch, &do_emote, "screams and attacks!");
		multi_hit(rch,victim,TYPE_UNDEFINED);
		continue;
	    }

	    /* PCs next */
	    if (!IS_NPC(ch) || IS_AFFECTED(ch,EFF_CHARM))
	    {
		if ( ( (!IS_NPC(rch) && STR_IS_SET(rch->strbit_act,PLR_AUTOASSIST))
		||     IS_AFFECTED(rch,EFF_CHARM))
		&&   is_same_group(ch,rch)
		&&   !is_safe(rch, victim))
		    multi_hit (rch,victim,TYPE_UNDEFINED);

		continue;
	    }

	    /* now check the NPC cases */

 	    if (IS_NPC(ch) && !IS_AFFECTED(ch,EFF_CHARM))
	    {
		if ((IS_NPC(rch) &&STR_IS_SET(rch->strbit_off_flags,ASSIST_ALL))

		||   (IS_NPC(rch) && rch->group && rch->group == ch->group)

		||   (IS_NPC(rch) && rch->race == ch->race
		   && STR_IS_SET(rch->strbit_off_flags,ASSIST_RACE))

		||   (IS_NPC(rch) && STR_IS_SET(rch->strbit_off_flags,ASSIST_ALIGN)
		   &&   ((IS_GOOD(rch)    && IS_GOOD(ch))
		     ||  (IS_EVIL(rch)    && IS_EVIL(ch))
		     ||  (IS_NEUTRAL(rch) && IS_NEUTRAL(ch))))

		||   (rch->pIndexData == ch->pIndexData
		   && STR_IS_SET(rch->strbit_off_flags,ASSIST_VNUM)))

	   	{
		    CHAR_DATA *vch;
		    CHAR_DATA *target;
		    int number;

		    if (number_bits(1) == 0)
			continue;

		    target = NULL;
		    number = 0;
		    for (vch = ch->in_room->people; vch; vch = vch->next)
		    {
			if (can_see(rch,vch)
			&&  is_same_group(vch,victim)
			&&  number_range(0,number) == 0)
			{
			    target = vch;
			    number++;
			}
		    }

		    if (target != NULL)
		    {
			do_function(rch,&do_emote,"screams and attacks!");
			multi_hit(rch,target,TYPE_UNDEFINED);
		    }
		}
	    }
	}
    }
}


/*
 * Do one group of attacks.
 */
void multi_hit( CHAR_DATA *ch, CHAR_DATA *victim, int dt )
{
    OBJ_DATA *wield,*weapon;

    /* decrement the wait */
    if (ch->desc == NULL)
	ch->wait = UMAX(0,ch->wait - PULSE_VIOLENCE);

    if (ch->desc == NULL)
	ch->daze = UMAX(0,ch->daze - PULSE_VIOLENCE);


    /* no attacks for stunnies -- just a check */
    if (ch->position < POS_RESTING)
	return;

    if (victim->position==POS_DEAD) {
	stop_fighting(ch,FALSE);
	return;
    }

    if ( ch->fighting == NULL && ch!=victim)
	set_fighting( ch, victim );

    if (IS_NPC(ch))
    {
	mob_hit(ch,victim,dt);
	return;
    }

    /* first attack */
    one_hit( ch, victim, dt, FALSE );

    // do the same with the secondary weapon
    wield=get_eq_char (ch, WEAR_SECONDARY);
    if (wield && skillcheck(ch,gsn_second_weapon)) {
        one_hit( ch, victim, dt, TRUE );
	check_improve(ch,gsn_second_weapon,TRUE,5);
        if ( ch->fighting != victim )
            return;
    }

    // no weapons? perhaps hand-to-hand (except for backstabbing)
    if (!wield && skillcheck(ch,gsn_hand_to_hand) &&
	dt!=gsn_backstab ) {
	weapon = get_eq_char(ch,WEAR_WIELD);
	if (weapon != NULL && IS_WEAPON_STAT(weapon,WEAPON_TWO_HANDS)) {
	    /* two handed weapons don't allow punching */
	} else {
	    one_hit( ch, victim, dt, TRUE );
	    check_improve(ch,gsn_hand_to_hand,TRUE,5);
	    if ( ch->fighting != victim )
		return;
	}
    }


    if (ch->fighting != victim)
	return;

    if (IS_AFFECTED(ch,EFF_HASTE))
	one_hit(ch,victim,dt, FALSE);

    if ( ch->fighting != victim || dt == gsn_backstab )
	return;

    /* second attack */

    if ( skillcheck2(ch,gsn_second_attack,IS_AFFECTED(ch,EFF_SLOW)?25:50))
    {
	one_hit( ch, victim, dt, FALSE );
	check_improve(ch,gsn_second_attack,TRUE,5);

	if ( ch->fighting != victim ) return;
	/* Also second attack for second weapon.. NOOO!!!! Removed :) */
    }

    /* third attack */

    if ( !IS_AFFECTED(ch,EFF_SLOW) &&
	 skillcheck2(ch,gsn_third_attack,25))
    {
	one_hit( ch, victim, dt, FALSE );
	check_improve(ch,gsn_third_attack,TRUE,6);
	if ( ch->fighting != victim )
	    return;
    }

    return;
}

int same_group(CHAR_DATA *ch1, CHAR_DATA *ch2)
{
	return (ch1->group) && (ch1->group==ch2->group);
}

int same_alignment(CHAR_DATA *ch1, CHAR_DATA *ch2)
{
	return (IS_GOOD(ch1) && IS_GOOD(ch2))
		 || (IS_EVIL(ch1) && IS_EVIL(ch2))
		 || (IS_NEUTRAL(ch1) && IS_NEUTRAL(ch2));
}

int same_race(CHAR_DATA *ch1, CHAR_DATA *ch2)
{
	return (ch1->race)==(ch2->race);
}

int same_mob(CHAR_DATA *ch1, CHAR_DATA *ch2)
{
	return (ch1->pIndexData)==(ch2->pIndexData);
}

/* procedure for all mobile attacks */
void mob_hit (CHAR_DATA *ch, CHAR_DATA *victim, int dt)
{
    int number;
    CHAR_DATA *vch, *vch_next;
    OBJ_DATA *obj;

    one_hit(ch,victim,dt, FALSE);

    if (ch->fighting != victim)
	return;

    /* Area attack -- BALLS nasty! */

    if (STR_IS_SET(ch->strbit_off_flags,OFF_AREA_ATTACK))
    {
	for (vch = ch->in_room->people; vch != NULL; vch = vch_next)
	{
	    vch_next = vch->next;
	    if ((vch != victim && vch->fighting == ch))
		one_hit(ch,vch,dt, FALSE);
	}
    }

    if (IS_AFFECTED(ch,EFF_HASTE)
    ||  (STR_IS_SET(ch->strbit_off_flags,OFF_FAST) && !IS_AFFECTED(ch,EFF_SLOW)))
	one_hit(ch,victim,dt, FALSE);

    if (ch->fighting != victim || dt == gsn_backstab)
	return;

    if (skillcheck2(ch,gsn_second_attack,
		    (IS_AFFECTED(ch,EFF_SLOW) && !STR_IS_SET(ch->strbit_off_flags,OFF_FAST))?25:50))
    {
	one_hit(ch,victim,dt, FALSE);
	if (ch->fighting != victim)
	    return;
    }

    if ((!IS_AFFECTED(ch,EFF_SLOW) || STR_IS_SET(ch->strbit_off_flags,OFF_FAST)) &&
	skillcheck2(ch,gsn_third_attack,25))
    {
	one_hit(ch,victim,dt, FALSE);
	if (ch->fighting != victim)
	    return;
    }

    /* oh boy!  Fun stuff! */

    if (ch->wait > 0)
	return;

    number = number_range(0,2);

    if (number == 1 && STR_IS_SET(ch->strbit_act,ACT_MAGE))
    {
	/*  { mob_cast_mage(ch,victim); return; } */ ;
    }

    if (number == 2 && STR_IS_SET(ch->strbit_act,ACT_CLERIC))
    {
	/* { mob_cast_cleric(ch,victim); return; } */ ;
    }

    // carrying a wand?
    obj=get_eq_char(ch,WEAR_HOLD);
    if (obj!=NULL && obj->item_type==ITEM_WAND && number_percent()<50) {
	do_function(ch,&do_zap,victim->name);
	if (ch->fighting != victim)
	    return;
    }

    /* now for the skills */

    number = number_range(0,10);

    switch(number)
    {
    case (0) :
	if (STR_IS_SET(ch->strbit_off_flags,OFF_BASH))
	    do_function(ch,&do_bash,"");
	break;

    case (1) :
	if (STR_IS_SET(ch->strbit_off_flags,OFF_BERSERK) && !IS_AFFECTED(ch,EFF_BERSERK))
	    do_function(ch,&do_berserk,"");
	break;

    case (2) :
	if (STR_IS_SET(ch->strbit_off_flags,OFF_DISARM)
	|| (get_weapon_sn(ch,FALSE) != gsn_hand_to_hand
	&& (STR_IS_SET(ch->strbit_act,ACT_WARRIOR)
   	||  STR_IS_SET(ch->strbit_act,ACT_THIEF))))
	    do_function(ch,&do_disarm,"");
	break;

    case (3) :
	if (STR_IS_SET(ch->strbit_off_flags,OFF_KICK))
	    do_function(ch,&do_kick,"");
	break;

    case (4) :
	if (STR_IS_SET(ch->strbit_off_flags,OFF_KICK_DIRT))
	    do_function(ch,&do_dirt,"");
	break;

    case (5) :
	if (STR_IS_SET(ch->strbit_off_flags,OFF_TAIL))
	    do_function(ch,&do_tail,"");
	break;

    case (6) :
	if (STR_IS_SET(ch->strbit_off_flags,OFF_TRIP))
	    do_function(ch,&do_trip,"");
	break;

    case (7) :
	if (STR_IS_SET(ch->strbit_off_flags,OFF_CRUSH))
	{
	    /* do_function(ch,&do_crush,"") */ ;
	}
	break;
    case (8) :
	if (STR_IS_SET(ch->strbit_off_flags,OFF_BACKSTAB))
	    do_function(ch,&do_backstab,"");
    case (9) :
	if (STR_IS_SET(ch->strbit_off_flags,OFF_HEADBUTT))
	    do_function(ch,&do_headbutt,"");
	break;
    case (10) :
	if (STR_IS_SET(ch->strbit_off_flags,OFF_RESCUE))
	{
	    int percent;

	    /* Okay... Rescue een mede mob als je nog een vol aantal
	       hp hebt. >70% */
	    if (ch->max_hit > 0) percent = ch->hit * 100 / ch->max_hit;
	    else percent = -1;

	    if(percent>=70)
	    {
		CHAR_DATA *rch,*rch_next;

		/* Scan chars in room, and try to find an allie to rescue */

		for(rch=ch->in_room->people;rch!=NULL;rch=rch_next)
		{
		    rch_next = rch->next_in_room;

		    if(rch->fighting==NULL) continue;

		    if(IS_NPC(ch) && !IS_NPC(rch)
		    && STR_IS_SET(ch->strbit_off_flags,ASSIST_PLAYERS)
		    && ch->level + 6 > rch->level)
		    {
			if (rch->max_hit > 0) percent = rch->hit * 100 / rch->max_hit;
			else percent = -1;
			if(percent<40)
			{
			    rescue(ch,rch);
			    break;
			}
		    }

		    if(IS_NPC(rch) && !IS_AFFECTED(rch,EFF_CHARM))
		    {
			if ( same_group(ch,rch)
			|| ( same_race(ch,rch) && STR_IS_SET(ch->strbit_off_flags,ASSIST_RACE) )
			|| ( STR_IS_SET(ch->strbit_off_flags,ASSIST_ALIGN)  &&  same_alignment(ch,rch) )
			|| ( STR_IS_SET(ch->strbit_off_flags,ASSIST_VNUM) && same_mob(ch,rch) ) ) {
			    if (rch->max_hit > 0) percent = rch->hit * 100 / rch->max_hit;
			    else percent = -1;
			    if(percent<40)
			    {
				rescue(ch,rch);
			    	break;
			    }
			}
		    }
		}
	    }
	}
	break;
    case (11) :
	if (STR_IS_SET(ch->strbit_off_flags,OFF_EYEPOKE))
	    do_function(ch,&do_eyepoke,"");
	break;
    }
}

void one_hit( CHAR_DATA *ch, CHAR_DATA *victim, int dt, bool secondary ) {
    one_hit_variable(ch,victim,dt,100,secondary);
}


/*
 * Hit one guy once.
 */
void one_hit_variable( CHAR_DATA *ch, CHAR_DATA *victim, int dt, int multiplier,bool secondary )
{
    OBJ_DATA *wield;
    int victim_ac;
    int thac0;
    int thac0_00;
    int thac0_32;
    int dam;
    int diceroll;
    int sn,skill;
    int dam_type;
    bool result;
    bool hassecond;

    sn = -1;


    /* just in case */
    if (victim == ch || ch == NULL || victim == NULL)
	return;

    /*
     * Can't beat a dead char!
     * Guard against weird room-leavings.
     */
    if ( victim->position == POS_DEAD || ch->in_room != victim->in_room )
	return;

    /*
     * Figure out the type of damage message.
     * if secondary == true, use the second weapon.
     */
    if (!secondary)
    {
        wield = get_eq_char( ch, WEAR_WIELD );
	hassecond=get_eq_char( ch, WEAR_SECONDARY )!=NULL;
    }
    else
    {
        wield = get_eq_char( ch, WEAR_SECONDARY );
        hassecond=TRUE;
    }

    if ( dt == TYPE_UNDEFINED )
    {
	dt = TYPE_HIT;
	if ( wield != NULL && wield->item_type == ITEM_WEAPON )
	    dt += wield->value[3];
	else
	    dt += ch->dam_type;
    }

    if (dt < TYPE_HIT)
    	if (wield != NULL)
    	    dam_type = attack_table[wield->value[3]].damage;
    	else
    	    dam_type = attack_table[ch->dam_type].damage;
    else
    	dam_type = attack_table[dt - TYPE_HIT].damage;

    if (dam_type == -1)
	dam_type = DAM_BASH;

    /* get the weapon skill */
    sn = get_weapon_sn(ch,secondary);
    skill = 20 + get_weapon_skill(ch,sn);

    /*
     * Calculate to-hit-armor-class-0 versus armor.
     */
    if ( IS_NPC(ch) )
    {
	thac0_00 = 20;
	thac0_32 = -4;   /* as good as a thief */
	if (STR_IS_SET(ch->strbit_act,ACT_WARRIOR))
	    thac0_32 = -10;
	else if (STR_IS_SET(ch->strbit_act,ACT_THIEF))
	    thac0_32 = -4;
	else if (STR_IS_SET(ch->strbit_act,ACT_CLERIC))
	    thac0_32 = 2;
	else if (STR_IS_SET(ch->strbit_act,ACT_MAGE))
	    thac0_32 = 6;
    }
    else
    {
	thac0_00 = class_table[ch->class].thac0_00;
	thac0_32 = class_table[ch->class].thac0_32;
    }
    thac0  = interpolate( ch->level, thac0_00, thac0_32 );

    if (thac0 < 0)
        thac0 = thac0/2;

    if (thac0 < -5)
        thac0 = -5 + (thac0 + 5) / 2;

    thac0 -= GET_HITROLL(ch) * skill/100;
    thac0 += 5 * (100 - skill) / 100;

    if (dt == gsn_backstab)
	thac0 -= 10 * (100 - get_skill(ch,gsn_backstab));

    /* Apply penelties if caracter uses a secondary weapon */
    if(hassecond)
    {
        /* Fix Joran, was -=, but penalty is a += */
        if(!secondary) thac0+=1;
        else thac0+=2;
    }

    switch(dam_type)
    {
	case(DAM_PIERCE):victim_ac = GET_AC(victim,AC_PIERCE)/10;	break;
	case(DAM_BASH):	 victim_ac = GET_AC(victim,AC_BASH)/10;		break;
	case(DAM_SLASH): victim_ac = GET_AC(victim,AC_SLASH)/10;	break;
	default:	 victim_ac = GET_AC(victim,AC_EXOTIC)/10;	break;
    };

    if (victim_ac < -15)
	victim_ac = (victim_ac + 15) / 5 - 15;

    if ( !can_see( ch, victim ) )
	victim_ac -= 4;

    if ( victim->position < POS_FIGHTING)
	victim_ac += 4;

    if (victim->position < POS_RESTING)
	victim_ac += 6;

    /*
     * The moment of excitement!
     */
    while ( ( diceroll = number_bits( 5 ) ) >= 20 )
	;

    if ( diceroll == 0
    || ( diceroll != 19 && diceroll < thac0 - victim_ac ) )
    {
	/* Miss. */
	damage( ch, victim, 0, dt, dam_type, TRUE, NULL );
	if ( wield && OBJ_HAS_TRIGGER( wield, OTRIG_NOHIT) )
	    op_nohit_trigger(wield,ch,victim,"miss");
	tail_chain( );
	return;
    }

    /*
     * Hit.
     * Calc damage.
     */
    if ( IS_NPC(ch) && wield == NULL)
      dam = dice(ch->damage[DICE_NUMBER],ch->damage[DICE_TYPE]);
    else
    {
	if (sn != -1)
	    check_improve(ch,sn,TRUE,5);
	if ( wield != NULL )
	{
	    dam = dice(wield->value[1],wield->value[2]) * skill/100;

	    if (get_eq_char(ch,WEAR_SHIELD) == NULL)  /* no shield = more */
		dam = dam * 11/10;

	    /* sharpness! */
	    if (IS_WEAPON_STAT(wield,WEAPON_SHARP))
	    {
/* This looks ridiculous
		int percent;

		if ((percent = number_percent()) <= (skill / 8))
		    dam = 2 * dam + (dam * 2 * percent / 100);
*/
		dam = dam + dam/4;
	    }
	}
	else
	    dam = number_range( 1 + 4 * skill/100, 2 * ch->level/3 * skill/100);
    }

    /*
     * Bonuses.
     */
    if ( has_skill_available(ch,gsn_enhanced_damage) )
    {
        if (skillcheck(ch,gsn_enhanced_damage))
        {
            check_improve(ch,gsn_enhanced_damage,TRUE,6);
            dam += 2 * ( dam * diceroll/300);
        }
    }

    if ( !IS_AWAKE(victim) )
	dam *= 2;
     else if (victim->position < POS_FIGHTING)
	dam = dam * 3 / 2;

    if ( dt == gsn_backstab && wield != NULL) {
    	if ( wield->value[0] != WEAPON_DAGGER )
	    dam *= 2 + (ch->level / 20);
	else
	    dam *= 2 + (ch->level / 16);
    }

    dam += GET_DAMROLL(ch) * UMIN(100,skill) /100;

    if ( dam <= 0 )
	dam = 1;

    // this is the only difference with the old one_hit() :
    dam=dam*multiplier/100;

    // Divine Relic / Unholy Relic
    {
	OBJ_DATA *relic=get_eq_char(ch->fighting,WEAR_FLOAT);
	if (relic)
	    if (relic->pIndexData->vnum==OBJ_VNUM_RELIC)
		if ((IS_GOOD(ch) && IS_EVIL(victim)) ||
		    (IS_EVIL(ch) && IS_GOOD(victim)))
		    dam=dam*9/10;
    }

    /*
     * Check for parry, and dodge.
     */
    if ( dt >= TYPE_HIT && ch != victim) {
	if ( check_parry( ch, victim ) ) {
	    if ( wield && OBJ_HAS_TRIGGER( wield, OTRIG_NOHIT) )
		op_nohit_trigger(wield,ch,victim,"parry");
	    return;
	}
	if ( check_dodge( ch, victim ) ) {
	    if ( wield && OBJ_HAS_TRIGGER( wield, OTRIG_NOHIT) )
		op_nohit_trigger(wield,ch,victim,"dodge");
	    return;
	}
	if ( check_shield_block(ch,victim)) {
	    if ( wield && OBJ_HAS_TRIGGER( wield, OTRIG_NOHIT) )
		op_nohit_trigger(wield,ch,victim,"block");
	    return;
	}
    }

    if ( wield && OBJ_HAS_TRIGGER( wield, OTRIG_PREHIT) ) {
	if (!op_prehit_trigger(wield,ch,victim))
	    return;
	if (!IS_VALID(wield)) wield=NULL;
    }

    result = damage( ch, victim, dam, dt, dam_type, TRUE, NULL );

    if (!result) {
	tail_chain( );
	return;
    }
    if ( wield && OBJ_HAS_TRIGGER( wield, OTRIG_HIT) ) {
	op_hit_trigger(wield,ch,victim);
	if (!IS_VALID(wield)) wield=NULL;
    }

    /* I still don't know where to place it so I place it here */
    if (ch->level>15)
	ch->move=UMAX(0,ch->move-number_range(1,1+(ch->level/30)));
    if (victim->level>15)
	victim->move=UMAX(0,victim->move-number_range(1,1+(ch->level/30)));

    /* but do we have a funky weapon? */
    if (wield != NULL)
    {
	int dam;

	if (ch->fighting == victim && IS_WEAPON_STAT(wield,WEAPON_POISON))
	{
	    int level;
	    EFFECT_DATA *poison, ef;

	    if ((poison = effect_find(wield->affected,TO_EFFECTS,gsn_poison)) == NULL)
		level = wield->level;
	    else
		level = poison->level;

	    if (!saves_spell(level / 2,victim,DAM_POISON))
	    {
		send_to_char("You feel poison coursing through your veins.\n\r",
		    victim);
		act("$n is poisoned by the venom on $p.",
		    victim,wield,NULL,TO_ROOM);

		ZEROVAR(&ef,EFFECT_DATA);
    		ef.where     = TO_EFFECTS;
    		ef.type      = gsn_poison;
    		ef.level     = level * 3/4;
    		ef.duration  = level / 2;
    		ef.location  = APPLY_STR;
    		ef.modifier  = -1;
    		ef.bitvector = EFF_POISON;
		ef.casted_by = ch?ch->id:0;
    		effect_join( victim, &ef );
	    }

	    /* weaken the poison if it's temporary */
	    if (poison != NULL)
	    {
	    	poison->level = UMAX(0,poison->level - 2);
	    	poison->duration = UMAX(0,poison->duration - 1);

	    	if (poison->level == 0 || poison->duration == 0)
		    act("The poison on $p has worn off.",ch,wield,NULL,TO_CHAR);
	    }
 	}


    	if (ch->fighting == victim && IS_WEAPON_STAT(wield,WEAPON_VAMPIRIC) &&
	    !STR_IS_SET(victim->strbit_imm_flags,IMM_NEGATIVE))
	{
	    dam = number_range(1, wield->level / 5 + 1);
	    act("$p draws life from $n.",victim,wield,NULL,TO_ROOM);
	    act("You feel $p drawing your life away.",
		victim,wield,NULL,TO_CHAR);
	    if (STR_IS_SET(ch->strbit_comm,COMM_SHOW_EXTRADAM))
	        damage(ch,victim,dam,33 + TYPE_HIT,DAM_NEGATIVE,TRUE, wield);
	    else
	        damage(ch,victim,dam,0,DAM_NEGATIVE,FALSE, NULL);
	    ch->alignment = UMAX(-1000,ch->alignment - 1);

	    if (STR_IS_SET(victim->strbit_vuln_flags,VULN_NEGATIVE)) dam+=dam/2;
	    if (STR_IS_SET(victim->strbit_res_flags,RES_NEGATIVE)) dam-=dam/3;

	    ch->hit += dam/2;
	    char_obj_alignment_check(ch);
	}

	if (ch->fighting == victim && IS_WEAPON_STAT(wield,WEAPON_FLAMING))
	{
	    dam = number_range(1,wield->level / 4 + 1);
	    act("$n is burned by $p.",victim,wield,NULL,TO_ROOM);
	    act("$p sears your flesh.",victim,wield,NULL,TO_CHAR);
	    fire_effect( (void *) victim,wield->level/2,dam,TARGET_CHAR);
	    if (STR_IS_SET(ch->strbit_comm,COMM_SHOW_EXTRADAM))
	        damage(ch,victim,dam,38 + TYPE_HIT,DAM_FIRE,TRUE, wield);
	    else
	        damage(ch,victim,dam,0,DAM_FIRE,FALSE, NULL);
	}

	if (ch->fighting == victim && IS_WEAPON_STAT(wield,WEAPON_FROST))
	{
	    dam = number_range(1,wield->level / 6 + 2);
	    act("$p freezes $n.",victim,wield,NULL,TO_ROOM);
	    act("The cold touch of $p surrounds you with ice.",
		victim,wield,NULL,TO_CHAR);
	    cold_effect(victim,wield->level/2,dam,TARGET_CHAR);
            if (STR_IS_SET(ch->strbit_comm,COMM_SHOW_EXTRADAM))
	        damage(ch,victim,dam,39 + TYPE_HIT,DAM_COLD,TRUE, wield);
            else
	        damage(ch,victim,dam, 0,DAM_COLD,FALSE, NULL);
	}

	if (ch->fighting == victim && IS_WEAPON_STAT(wield,WEAPON_SHOCKING))
	{
	    dam = number_range(1,wield->level/5 + 2);
	    act("$n is struck by lightning from $p.",victim,wield,NULL,TO_ROOM);
	    act("You are shocked by $p.",victim,wield,NULL,TO_CHAR);
	    shock_effect(victim,wield->level/2,dam,TARGET_CHAR);
	    if (STR_IS_SET(ch->strbit_comm,COMM_SHOW_EXTRADAM))
	        damage(ch,victim,dam,36 + TYPE_HIT,DAM_LIGHTNING,TRUE , wield);
            else
	        damage(ch,victim,dam, 0,DAM_LIGHTNING,FALSE, NULL );
	}

	if (ch->fighting == victim && (dam=is_race_poison(wield,victim->race))) {
	    // dam == level of the effect.
	    dam=dam*victim->max_hit/100;
	    if (STR_IS_SET(ch->strbit_comm,COMM_SHOW_EXTRADAM))
	        damage(ch,victim,dam,gsn_poison,DAM_POISON,TRUE , wield);
            else
	        damage(ch,victim,dam, gsn_poison,DAM_POISON,TRUE, NULL );
	}
    }

    tail_chain( );
    return;
}

void event_plr_auto(EVENT_DATA *event) {
    OBJ_DATA *corpse=event->object;
    CHAR_DATA *ch=event->ch;

    if (!IS_VALID(ch)) return;
    if (!IS_VALID(corpse)) return;

    if (corpse->in_room!=ch->in_room) return;

    if (ch->position<POS_FIGHTING) return;
    if (ch->position==POS_FIGHTING) {
	create_event(4,ch,NULL,corpse,event_plr_auto,NULL);	
	return;
    }

    if (corpse->item_type == ITEM_CORPSE_NPC && can_see_obj(ch,corpse)) {
	    OBJ_DATA *coins;

	    if ( STR_IS_SET(ch->strbit_act, PLR_AUTOLOOT) &&
		 corpse && corpse->contains) /* exists and not empty */
	//	do_function(ch,&do_get,"all corpse" );
		get_from_container(ch,corpse,1,"all");

 	    if (STR_IS_SET(ch->strbit_act,PLR_AUTOGOLD) &&
	        corpse && corpse->contains  && /* exists and not empty */
		!STR_IS_SET(ch->strbit_act,PLR_AUTOLOOT))
		if ((coins = get_obj_list(ch,"gcash",corpse->contains))
		     != NULL) {
	      	    //do_function(ch,&do_get,"all.gcash corpse");
		    get_from_container(ch,corpse,1,"gcash");
		}

	    if ( STR_IS_SET(ch->strbit_act, PLR_AUTOSAC) ) {
       	      if ( STR_IS_SET(ch->strbit_act,PLR_AUTOLOOT) && corpse && corpse->contains)
		return;  /* leave if corpse has treasure */
	      else {
		int reward=0;
		if (sacrifice(ch,corpse,&reward,TRUE))
		    award_sacrifice(ch,reward);
            }
	}
    }
}

/*
 * Inflict damage from a hit.
 */
bool damage(CHAR_DATA *ch,CHAR_DATA *victim,int dam,int dt,int dam_type,
	    bool show, OBJ_DATA *obj)
{

    OBJ_DATA *corpse;
    bool immune;
    char buf[MSL];
    float fdam;

    if ( victim->position == POS_DEAD )
	return FALSE;

    /*
     * Stop up any residual loopholes.
     */
    if ( dam > 1200 && dt >= TYPE_HIT)
    {
	bugf( "Damage: %d: more than 1200 points for %s!", dam,NAME(ch));
	dam = 1200;
	if (!IS_IMMORTAL(ch))
	{
	    OBJ_DATA *obj;
	    obj = get_eq_char( ch, WEAR_WIELD );
	    send_to_char("You really shouldn't cheat.\n\r",ch);
	    if (obj != NULL)
	    	extract_obj(obj);
	}

    }

    fdam=dam;
#define dam OH_NO_DAM_USED
//
//    /* damage reduction */
//    if ( dam > 35)
//	dam = (dam - 35)/2 + 35;
//   if ( dam > 80)
//	dam = (dam - 80)/2 + 80;
//



    if ( victim != ch )
    {
	/*
	 * Certain attacks are forbidden.
	 * Most other attacks are returned.
	 */
	if ( is_safe( ch, victim ) )
	    return FALSE;
	check_killer( ch, victim );

	if ( victim->position > POS_STUNNED )
	{
	    if ( victim->fighting == NULL )
	    {
		set_fighting( victim, ch );
		if ( IS_NPC( victim ) && MOB_HAS_TRIGGER( victim, MTRIG_KILL ) )
		    mp_kill_trigger(victim,ch);
		rp_kill_trigger(ch,victim);
		if (!IS_VALID(victim))
		    return FALSE;
	    }
	    if (victim->timer <= 4)
	    	victim->position = POS_FIGHTING;
	}

	if ( victim->position > POS_STUNNED )
	{
	    if ( ch->fighting == NULL )
		set_fighting( ch, victim );

	    /*
	     * If victim is charmed, ch might attack victim's master.
	     taken out by Russ! */
/*
	    if ( IS_NPC(ch)
	    &&   IS_NPC(victim)
	    &&   IS_AFFECTED(victim, EFF_CHARM)
	    &&   victim->master != NULL
	    &&   victim->master->in_room == ch->in_room
	    &&   number_bits( 3 ) == 0 )
	    {
		stop_fighting( ch, FALSE );
		multi_hit( ch, victim->master, TYPE_UNDEFINED );
		return FALSE;
	    }
*/
	}

	/*
	 * More charm stuff.
	 */
	if ( victim->master == ch )
	    stop_follower( victim );
    }

    /*
     * Inviso attacks ... not.
     */
    if ( IS_AFFECTED(ch, EFF_INVISIBLE) )
    {
	effect_strip( ch, gsn_invisibility );
	effect_strip( ch, gsn_mass_invis );
	if( IS_NPC(ch) ) STR_REMOVE_BIT(ch->strbit_affected_by,EFF_INVISIBLE);
	if(!IS_AFFECTED(ch, EFF_INVISIBLE) )
		act( "$n fades into existence.", ch, NULL, NULL, TO_ROOM );
    }

    /*
     * Damage modifiers.
     */

    if ( fdam > 1 && !IS_NPC(victim)
    &&   victim->pcdata->condition[COND_DRUNK]  > 10 )
	fdam = 9 * fdam / 10;

    if ( fdam > 1 && IS_AFFECTED(victim, EFF_SANCTUARY) )
	fdam /= 2;

    if ( fdam > 1 && ((IS_AFFECTED(victim, EFF_PROTECT_EVIL) && IS_EVIL(ch) )
    ||		     (IS_AFFECTED(victim, EFF_PROTECT_GOOD) && IS_GOOD(ch) )))
	fdam -= fdam / 4;

    immune = FALSE;

    switch (check_immune(victim,dam_type))
    {
	case(IS_IMMUNE):
	    immune = TRUE;
	    fdam = 0;
	    break;
	case(IS_RESISTANT):
	    fdam -= fdam/3;
	    break;
	case(IS_VULNERABLE):
	    fdam += fdam/2;
	    break;
    }
#undef dam
    dam=floor(fdam);
    /*
    * Now we are hitting the character...
    * Check for the assassation skill
    */
    if (dam!=0 && dt==gsn_backstab) {
	if (skillcheck2(ch,gsn_assassinate,5) || (dam>=victim->hit))
	{
	    act("You are brutally assassinated by $n.", ch, NULL, victim, TO_VICT);
	    act("You assassinate $N!!", ch, NULL, victim, TO_CHAR);
	    act("$n assasinates $N!!!", ch, NULL, victim, TO_NOTVICT);
	    dam=UMAX(dam,victim->hit);
	    check_improve(ch, gsn_assassinate, TRUE, 1);
	} else
	    check_improve(ch, gsn_assassinate, FALSE, 1);
    }

    if (show) {
        if (obj == NULL)
    	    dam_message( ch, victim, dam, dt, immune );
        else
	    dam_message_obj( ch, victim, obj, dam, dt, immune );
    }


    if (dam == 0)
	return FALSE;

    /*
     * Hurt the victim.
     * Inform the victim of his new state.
     */
    victim->hit -= dam;
    if ( !IS_NPC(victim)
    &&   victim->level >= LEVEL_IMMORTAL
    &&   victim->hit < 1 )
	victim->hit = 1;
    update_pos( victim );

    switch( victim->position )
    {
    case POS_MORTAL:
	act( "$n is mortally wounded, and will die soon, if not aided.",
	    victim, NULL, NULL, TO_ROOM );
	send_to_char(
	    "You are mortally wounded, and will die soon, if not aided.\n\r",
	    victim );
	break;

    case POS_INCAP:
	act( "$n is incapacitated and will slowly die, if not aided.",
	    victim, NULL, NULL, TO_ROOM );
	send_to_char(
	    "You are incapacitated and will slowly die, if not aided.\n\r",
	    victim );
	break;

    case POS_STUNNED:
	act( "$n is stunned, but will probably recover.",
	    victim, NULL, NULL, TO_ROOM );
	send_to_char("You are stunned, but will probably recover.\n\r",
	    victim );
	break;

    case POS_DEAD:
    	/*
	 * Death trigger
	 */
	if ( IS_NPC( victim ) && MOB_HAS_TRIGGER( victim, MTRIG_DEATH) ) {
	    victim->position = POS_STANDING;
	    /* Make the mob alive so he can do something */
	    mp_death_trigger(victim,ch);
	    if (!IS_VALID(victim))
		return FALSE;
	    /* Kill the mob again */
	    victim->position = POS_DEAD;
	}

	rp_death_trigger(victim,ch);

	{
	    CHAR_DATA *vch;
	    for (vch=ch->in_room->people;vch;vch=vch->next_in_room) {
		if (vch==victim) continue;
		if (IS_NPC(vch) && MOB_HAS_TRIGGER( vch, MTRIG_KILLED ))
		    mp_killed_trigger(vch,victim);

		if (IS_AFFECTED(vch,EFF_HALLUCINATION))
		    act( "$N is DEAD!! You hear the studio audience applaud!",
					vch, NULL, victim, TO_CHAR );
		else
		    act( "$N is DEAD!!", vch, NULL, victim, TO_CHAR );
	    }
	    send_to_char( "You have been {WKILLED{x!!\n\r\n\r", victim );
	}
	break;

    default:
	if ( dam > victim->max_hit / 4 )
	    send_to_char( "That really did {rHURT!{x\n\r", victim );
	if ( victim->hit < victim->max_hit / 4 )
	    send_to_char( "You sure are {rBLEEDING!{x\n\r", victim );
	break;
    }

    /*
     * Sleep spells and extremely wounded folks.
     */
    if ( !IS_AWAKE(victim) )
	stop_fighting( victim, FALSE );

    /*
     * Payoff for killing things.
     */
    if ( victim->position == POS_DEAD )
    {
        bool arena=in_safe_kill_room(ch, victim);

	if(!arena) group_gain( ch, victim );

	if (IS_PC(victim))
	    announce( victim, "%s got killed by %s at %s.",
		victim->name,
		IS_NPC(ch)?ch->short_descr:ch->name,
		ch->in_room->name);

        sprintf(buf,
	    "$N [level %d] got toasted by %s [level %d] at %s [room %d]",
	    victim->level,(IS_NPC(ch) ? ch->short_descr : ch->name),
	    ch->level,ch->in_room->name, ch->in_room->vnum);
        if (IS_NPC(victim))
            wiznet(WIZ_MOBDEATHS,0,victim,NULL,"%s",buf);
        else
            wiznet(WIZ_DEATHS,0,victim,NULL,"%s",buf);

	if ( IS_PC(victim) )
	{
	    logf( "[%d] %s [level %d] killed by %s [level %d] at %d",
		GET_DESCRIPTOR(victim),victim->name,victim->level,
		(IS_NPC(ch) ? ch->short_descr : ch->name),
		ch->level,ch->in_room->vnum );

            if(!arena && !STR_IS_SET(victim->strbit_act, PLR_HARDCORE))
            {
	        /*
	         * Dying penalty:
	         * 2/3 way back to previous level.
	         */
	        if ( victim->exp > exp_per_level(victim,victim->pcdata->points)
			       * victim->level )
	        gain_exp( victim, (2 * (exp_per_level(victim,victim->pcdata->points)
			         * victim->level - victim->exp)/3) + 50, TRUE );
	    }
	}

	corpse=raw_kill( victim , ch );
        /* dump the flags */
/* or not
        if (ch != victim && !IS_NPC(ch) && !is_same_clan(ch,victim))
        {
            if (STR_IS_SET(victim->strbit_act,PLR_KILLER))
                STR_REMOVE_BIT(victim->strbit_act,PLR_KILLER);
            else
                STR_REMOVE_BIT(victim->strbit_act,PLR_THIEF);
        }
*/
        /* RT new auto commands */

	if (!arena && !IS_NPC(ch) && corpse)
	    create_event(1,ch,NULL,corpse,event_plr_auto,NULL);

	return TRUE;
    }

    if ( victim == ch )
	return TRUE;

    /*
     * Take care of link dead people.
     */
    if ( !IS_NPC(victim) && victim->desc == NULL )
    {
	if ( number_range( 0, victim->wait ) == 0 )
	{
	    do_function(victim,&do_recall,"");
	    return TRUE;
	}
    }

    /*
     * Wimp out?
     */
    if ( IS_NPC(victim) && dam > 0 && victim->wait < PULSE_VIOLENCE / 2)
    {
	if ( ( STR_IS_SET(victim->strbit_act, ACT_WIMPY) && number_bits( 2 ) == 0
	&&   victim->hit < victim->max_hit / 5)
	||   ( IS_AFFECTED(victim, EFF_CHARM) && victim->master != NULL
	&&     victim->master->in_room != victim->in_room ) )
	    do_function(victim,&do_flee, "" );
    }

    if ( !IS_NPC(victim)
    &&   victim->hit > 0
    &&   victim->hit <= victim->wimpy
    &&   victim->wait < PULSE_VIOLENCE / 2 )
	do_function(victim,&do_flee, "" );

    tail_chain( );
    return TRUE;
}

bool allow_killstealing(CHAR_DATA *ch,CHAR_DATA *victim) {
    bool isfightingPC;
    CHAR_DATA *gch;

//  if (IS_NPC(victim) &&
//	victim->fighting != NULL &&
//	!is_same_group(ch,victim->fighting))
//  {
//      send_to_char("Kill stealing is not permitted.\n\r",ch);
//      return FALSE;
//  }

    // is NPC is attacking something, allow it
    if (IS_NPC(ch) && ch->master==NULL)
	return TRUE;

    // if victim isn't fighting anybody, allow it
    if (victim->fighting==NULL)
	return TRUE;

    // if the current opponent of the victim is in the same group, allow it
    if (is_same_group(ch,victim->fighting))
	return TRUE;

    // pets? no problem
    if (IS_NPC(ch) && ch->master &&
	ch->master->fighting==victim)
	return TRUE;

    // if victim is not fighting a player, allow it
    isfightingPC=FALSE;
    for (gch=ch->in_room->people;gch;gch=gch->next_in_room) {
	if ((gch->fighting==victim || gch->fighting==victim->fighting) &&
	    !IS_NPC(gch)) {
	    isfightingPC=TRUE;
	}
    }
    if (isfightingPC) {
	send_to_char("Kill stealing is not permitted.\n\r",ch);
	return FALSE;
    }

    // is two random mobs are fighting and they both don't have a
    // master, allow it
    if (IS_NPC(victim->fighting) && IS_NPC(victim) &&
	victim->master==NULL && victim->fighting->master==NULL)
	return TRUE;

    send_to_char("Kill stealing is not permitted.\n\r",ch);
    return FALSE;
}

bool is_safe(CHAR_DATA *ch, CHAR_DATA *victim)
{
    if (victim->in_room == NULL || ch->in_room == NULL)
	return TRUE;

    if (victim->fighting == ch || victim == ch)
	return FALSE;

    if (IS_IMMORTAL(ch) && ch->level > LEVEL_IMMORTAL)
	return FALSE;

    /* killing mobiles */
    if (IS_NPC(victim))
    {

	/* safe room? */
	if (STR_IS_SET(victim->in_room->strbit_room_flags,ROOM_SAFE))
	{
	    send_to_char("Not in this room.\n\r",ch);
	    return TRUE;
	}

	if (victim->pIndexData->pShop != NULL)
	{
	    send_to_char("The shopkeeper wouldn't like that.\n\r",ch);
	    return TRUE;
	}

	if (STR_IS_SET(victim->strbit_act,ACT_PEACEFULL)) {
	    act("$N doesn't respond to your provocations.",
		ch,NULL,victim,TO_CHAR);
	    act("You try not to respond to $n's provocations.",
		ch,NULL,victim,TO_VICT);
	    return TRUE;
	}

	/* no killing healers, trainers, etc */
	if (STR_IS_SET(victim->strbit_act,ACT_TRAIN)
	||  STR_IS_SET(victim->strbit_act,ACT_PRACTICE)
	||  STR_IS_SET(victim->strbit_act,ACT_IS_HEALER)
	||  STR_IS_SET(victim->strbit_act,ACT_IS_CHANGER))
	{
	    sprintf_to_char(ch,
		"I don't think %s would approve.\n\r",players_god(ch));
	    return TRUE;
	}

	if (!IS_NPC(ch))
	{
	    /* no pets unless not longer charmed */
	    if (STR_IS_SET(victim->strbit_act,ACT_PET)
	    && IS_AFFECTED(victim,EFF_CHARM)) {
		act("But $N looks so cute and cuddly...",
		    ch,NULL,victim,TO_CHAR);
		return TRUE;
	    }

	    /* no charmed creatures unless owner */
	    if (IS_AFFECTED(victim,EFF_CHARM) && ch != victim->master)
	    {
		send_to_char("You don't own that monster.\n\r",ch);
		return TRUE;
	    }
	}
    }
    /* killing players */
    else
    {
	/* NPC doing the killing */
	if (IS_NPC(ch))
	{
	    /* safe room check */
	    if (STR_IS_SET(victim->in_room->strbit_room_flags,ROOM_SAFE))
	    {
		send_to_char("Not in this room.\n\r",ch);
		return TRUE;
	    }

	    /* charmed mobs and pets cannot attack players while owned */
	    if (IS_AFFECTED(ch,EFF_CHARM) && ch->master != NULL
	    &&  ch->master->fighting != victim)
	    {
		send_to_char("Players are your friends!\n\r",ch);
		return TRUE;
	    }
	}
	/* player doing the killing */
	else
	{
	    bool b=FALSE;

  	    /* safe room? */
	    if (STR_IS_SET(victim->in_room->strbit_room_flags,ROOM_SAFE))
	    {
	    	send_to_char("Not in this room.\n\r",ch);
	    	return TRUE;
	    }

	    if ( !STR_IS_SET(ch->strbit_act, PLR_PKILLING)) {
		send_to_char( "Use 'config pkill' to enable player killing.\n\r", ch);
		return TRUE;
	    }
	    if ( !STR_IS_SET(victim->strbit_act, PLR_KILLER)
	    &&   !STR_IS_SET(victim->strbit_act, PLR_THIEF)
	    &&   !STR_IS_SET(victim->strbit_act, PLR_PKILLING) ) {
		act_new("$N doesn't want to get involved in player killing.",ch,NULL,victim,TO_CHAR,POS_DEAD);
		return TRUE;
	    }

	    if (IS_SET(victim->in_room->area->area_flags,AREA_ARENA))
	        return FALSE;

	    GetRoomProperty(victim->in_room,PROPERTY_BOOL,"is-arena",&b);
	    if (b)
		return FALSE;

	    if (!is_clan(ch))
	    {
		send_to_char("Join a clan if you want to kill players.\n\r",ch);
		return TRUE;
	    }

	    if (STR_IS_SET(victim->strbit_act,PLR_KILLER) || STR_IS_SET(victim->strbit_act,PLR_THIEF))
		return FALSE;

	    if (!is_clan(victim))
	    {
		send_to_char("They aren't in a clan, leave them alone.\n\r",ch);
		return TRUE;
	    }

	    if (!IS_SET(victim->in_room->area->area_flags,AREA_PKZONE))
	    {
	    	send_to_char("Not in this area.\n\r",ch);
	    	return TRUE;
	    }

	    if (ch->level > victim->level + 8)
	    {
		send_to_char("Pick on someone your own size.\n\r",ch);
		return TRUE;
	    }
	}
    }
    return FALSE;
}

bool is_safe_spell(CHAR_DATA *ch, CHAR_DATA *victim, bool area )
{
    if (victim->in_room == NULL || ch->in_room == NULL)
        return TRUE;

    if (victim == ch && area)
	return TRUE;

    if (victim->fighting == ch || victim == ch)
	return FALSE;

    if (IS_IMMORTAL(ch) && ch->level > LEVEL_IMMORTAL && !area)
	return FALSE;

    /* killing mobiles */
    if (IS_NPC(victim))
    {
	/* safe room? */
	if (STR_IS_SET(victim->in_room->strbit_room_flags,ROOM_SAFE))
	    return TRUE;

	if (victim->pIndexData->pShop != NULL)
	    return TRUE;

	/* no killing healers, trainers, etc */
	if (STR_IS_SET(victim->strbit_act,ACT_TRAIN)
	||  STR_IS_SET(victim->strbit_act,ACT_PRACTICE)
	||  STR_IS_SET(victim->strbit_act,ACT_IS_HEALER)
	||  STR_IS_SET(victim->strbit_act,ACT_IS_CHANGER)
	||  STR_IS_SET(victim->strbit_act,ACT_PEACEFULL))
	    return TRUE;

	if (!IS_NPC(ch))
	{
	    /* no pets */
	    if (STR_IS_SET(victim->strbit_act,ACT_PET))
	   	return TRUE;

	    /* no charmed creatures unless owner */
	    if (IS_AFFECTED(victim,EFF_CHARM) && (area || ch != victim->master))
		return TRUE;

	    /* legal kill? -- cannot hit mob fighting non-group member */
	    if (victim->fighting != NULL && !is_same_group(ch,victim->fighting))
		return TRUE;
	}
	else
	{
	    /* area effect spells do not hit other mobs */
	    if (area && !is_same_group(victim,ch->fighting))
		return TRUE;
	}
    }
    /* killing players */
    else
    {
	if (area && IS_IMMORTAL(victim) && victim->level > LEVEL_IMMORTAL)
	    return TRUE;

	/* NPC doing the killing */
	if (IS_NPC(ch))
	{
	    /* charmed mobs and pets cannot attack players while owned */
	    if (IS_AFFECTED(ch,EFF_CHARM) && ch->master != NULL
	    &&  ch->master->fighting != victim)
		return TRUE;

	    /* safe room? */
	    if (STR_IS_SET(victim->in_room->strbit_room_flags,ROOM_SAFE))
		return TRUE;

	    /* legal kill? -- mobs only hit players grouped with opponent*/
	    if (ch->fighting != NULL && !is_same_group(ch->fighting,victim))
		return TRUE;
	}

	/* player doing the killing */
	else
	{
	    bool b=FALSE;

  	    /* safe room? */
	    if (STR_IS_SET(victim->in_room->strbit_room_flags,ROOM_SAFE))
	    	return TRUE;

	    if ( !STR_IS_SET(ch->strbit_act, PLR_PKILLING)) {
		if (!area)
		    send_to_char( "Use 'config pkill' to enable player killing.\n\r", ch);
		return TRUE;
	    }
	    if ( !STR_IS_SET(victim->strbit_act, PLR_KILLER)
	    &&   !STR_IS_SET(victim->strbit_act, PLR_THIEF)
	    &&   !STR_IS_SET(victim->strbit_act, PLR_PKILLING) ) {
		if (!area)
		    act_new("$N doesn't want to get involved in player killing.",ch,NULL,victim,TO_CHAR,POS_DEAD);
		return TRUE;                                            
            }

	    if (IS_SET(victim->in_room->area->area_flags,AREA_ARENA))
	        return FALSE;

	    GetRoomProperty(victim->in_room,PROPERTY_BOOL,"is-arena",&b);
	    if (b)
		return FALSE;

	    if (!is_clan(ch))
		return TRUE;

	    if (STR_IS_SET(victim->strbit_act,PLR_KILLER) || STR_IS_SET(victim->strbit_act,PLR_THIEF))
		return FALSE;

	    if (!is_clan(victim))
		return TRUE;

	    if (!IS_SET(victim->in_room->area->area_flags,AREA_PKZONE))
	    	return TRUE;

	    if (ch->level > victim->level + 2)
		return TRUE;
	}

    }
    return FALSE;
}

/*
 * See if an attack justifies a KILLER flag.
 */
void check_killer( CHAR_DATA *ch, CHAR_DATA *victim ) {
    /* You are never a killer in a arena */
    if (in_safe_kill_room(ch, victim)) return;

    /*
     * Follow charm thread to responsible character.
     * Attacking someone's charmed char is hostile!
     */
    while ( IS_AFFECTED(victim, EFF_CHARM) && victim->master != NULL )
	victim = victim->master;

    /*
     * NPC's are fair game.
     * So are killers and thieves.
     */
    if ( IS_NPC(victim)
    ||   STR_IS_SET(victim->strbit_act, PLR_KILLER)
    ||   STR_IS_SET(victim->strbit_act, PLR_THIEF))
	return;

    /*
     * Charm-o-rama.
     */
    if ( STR_IS_SET(ch->strbit_affected_by, EFF_CHARM) )
    {
	if ( ch->master == NULL )
	{
	    bugf( "Check_killer: %s bad EFF_CHARM",
		IS_NPC(ch) ? ch->short_descr : ch->name );
	    effect_strip( ch, gsn_charm_person );
	    STR_REMOVE_BIT( ch->strbit_affected_by, EFF_CHARM );
	    return;
	}
/*
	send_to_char( "*** You are now a KILLER!! ***\n\r", ch->master );
  	STR_SET_BIT(ch->master->strbit_act, PLR_KILLER);
*/

	stop_follower( ch );
	return;
    }

    /*
     * NPC's are cool of course (as long as not charmed).
     * Hitting yourself is cool too (bleeding).
     * So is being immortal (Alander's idea).
     * And current killers stay as they are.
     */
    if ( IS_NPC(ch)
    ||   ch == victim
    ||   ch->level >= LEVEL_IMMORTAL
    ||   !is_clan(ch)
    ||   STR_IS_SET(ch->strbit_act, PLR_KILLER)
    ||	 ch->fighting  == victim)
	return;

    send_to_char( "*** You are now a KILLER!! ***\n\r", ch );
    STR_SET_BIT(ch->strbit_act, PLR_KILLER);
    wiznet(WIZ_FLAGS,0,ch,NULL,"$N is attempting to murder %s",victim->name);
    save_char_obj( ch );
    return;
}



/*
 * Check for parry.
 */
bool check_parry( CHAR_DATA *ch, CHAR_DATA *victim )
{
    float chance;

    if ( !IS_AWAKE(victim) )
	return FALSE;

    chance = 50;

    if ( get_eq_char( victim, WEAR_WIELD ) == NULL )
    {
	if (IS_NPC(victim))
	    chance = chance/2;
	else
	    return FALSE;
    }

    if (!can_see(ch,victim))
	chance = chance/2;

    chance*=1+ (victim->level-ch->level)*0.01;

    if (!skillcheck2(victim,gsn_parry,chance)) return FALSE;

    act( "You parry $n's attack.",  ch, NULL, victim, TO_VICT    );
    act( "$N parries your attack.", ch, NULL, victim, TO_CHAR    );
    check_improve(victim,gsn_parry,TRUE,6);
    return TRUE;
}

/*
 * Check for shield block.
 */
bool check_shield_block( CHAR_DATA *ch, CHAR_DATA *victim )
{
    float chance;

    if ( !IS_AWAKE(victim) )
        return FALSE;

    if ( get_eq_char( victim, WEAR_SHIELD ) == NULL )
        return FALSE;

    chance=23*(1+ (victim->level-ch->level)*0.01);

    if (!skillcheck2(victim,gsn_shield_block,chance)) return FALSE;

    act( "You block $n's attack with your shield.",  ch, NULL, victim, TO_VICT);
    act( "$N blocks your attack with a shield.", ch, NULL, victim, TO_CHAR    );
    check_improve(victim,gsn_shield_block,TRUE,6);
    return TRUE;
}


/*
 * Check for dodge.
 */
bool check_dodge( CHAR_DATA *ch, CHAR_DATA *victim )
{
    float chance;

    if ( !IS_AWAKE(victim) )
	return FALSE;

    chance = 50;

    if (!can_see(victim,ch))
	chance = chance/2;

    chance*=1.0+ (victim->level-ch->level)*0.01;

    if (!skillcheck2(victim,gsn_dodge,chance))
        return FALSE;

    act( "You dodge $n's attack.", ch, NULL, victim, TO_VICT    );
    act( "$N dodges your attack.", ch, NULL, victim, TO_CHAR    );
    check_improve(victim,gsn_dodge,TRUE,6);
    return TRUE;
}



/*
 * Set position of a victim.
 */
void update_pos( CHAR_DATA *victim )
{
    if ( victim->hit > 0 )
    {
    	if ( victim->position <= POS_STUNNED )
	    victim->position = POS_STANDING;
	if (victim->position==POS_FIGHTING && !victim->fighting)
	    victim->position = POS_STANDING;
	return;
    }

    if ( IS_NPC(victim) && victim->hit < 1 )
    {
	victim->position = POS_DEAD;
	return;
    }

    if ( victim->hit <= -11 )
    {
	victim->position = POS_DEAD;
	return;
    }

         if ( victim->hit <= -6 ) victim->position = POS_MORTAL;
    else if ( victim->hit <= -3 ) victim->position = POS_INCAP;
    else                          victim->position = POS_STUNNED;

    return;
}



/*
 * Start fights.
 */
void set_fighting( CHAR_DATA *ch, CHAR_DATA *victim )
{
    if ( ch->fighting != NULL )
    {
	bugf( "Set_fighting: %s already fighting with %s in room %d",
	    NAME(ch),NAME(victim),ch->in_room->vnum );
	return;
    }

    if ( IS_AFFECTED(ch, EFF_SLEEP) )
	effect_strip( ch, gsn_sleep );

    ch->fighting = victim;
    ch->position = POS_FIGHTING;
    ch->on=NULL;	// get rid of objects you where sleeping/resting on

    return;
}



/*
 * Stop fights. (actually let opponent stop)
 */
void stop_fighting( CHAR_DATA *ch, bool fBoth )
{
    CHAR_DATA *fch;

    for ( fch = char_list; fch != NULL; fch = fch->next )
    {
	if ( fch == ch || ( fBoth && fch->fighting == ch ) )
	{
	    fch->fighting	= NULL;
	    fch->position	= IS_NPC(fch) ? fch->default_pos : POS_STANDING;
	    if (fch->phasedskill_proc) {
		if (IS_SET(fch->phasedskill_flags,PHASED_TAKEAFTERCARE)) {
		    (fch->phasedskill_proc)(fch);
		}
		set_phased_skill(fch,NULL,NULL,0,PHASED_NONE);
	    }
	    update_pos( fch );
	}
    }

    return;
}



/*
 * Make a corpse out of a character.
 */
OBJ_DATA *make_corpse( CHAR_DATA *ch , CHAR_DATA *killer)
{
    char buf[MAX_STRING_LENGTH];
    OBJ_DATA *corpse=NULL;
    OBJ_DATA *obj;
//    OBJ_DATA *obj_next;
    char *name;
    bool b;

    //
    // make a corpse, sometimes not done. corpse=NULL in that case.
    //
    b=FALSE;
    GetCharProperty(ch,PROPERTY_BOOL,"nocorpse",&b);
    if (!b) {
	if ( IS_NPC(ch) ) {
	    name		= ch->short_descr;
	    corpse		= create_object(get_obj_index(OBJ_VNUM_CORPSE_NPC), 0);
	    corpse->timer	= number_range( 3, 6 );
	    if ( ch->silver > 0 || ch->gold >0 )
	    {
		obj_to_obj( create_money( ch->gold, ch->silver ), corpse );
		ch->gold = 0;
		ch->silver = 0;
	    }
	    corpse->cost = ch->level*3;
	} else {
	    name		= ch->name;
	    corpse		= create_object(get_obj_index(OBJ_VNUM_CORPSE_PC), 0);
	    corpse->timer	= number_range( 25, 40 );
	    //STR_REMOVE_BIT(ch->strbit_act,PLR_CANLOOT);
	    if (!STR_IS_SET(ch->strbit_act,PLR_HARDCORE)) {
		free_string(corpse->owner_name);
		corpse->owner_name = str_dup(ch->name);
		corpse->owner_id = ch->id;
	    }
	    if (is_clan(ch)) {
		if (ch->gold > 1 || ch->silver > 1)
		{
		    obj_to_obj(create_money(ch->gold / 2, ch->silver/2), corpse);
		    ch->gold -= ch->gold/2;
		    ch->silver -= ch->silver/2;
		}
	    }

	    corpse->cost = ch->level*3;
	}

	corpse->level = ch->level;

	if (IS_NPC(ch) && ch->pIndexData->vnum==MOB_VNUM_ZOMBIE) {
	    free_string( corpse->short_descr );
	    corpse->short_descr = str_dup( name );

	    free_string( corpse->description );
	    corpse->description = str_dup( ch->description );
	} else {
	    sprintf( buf, corpse->short_descr, name );
	    free_string( corpse->short_descr );
	    corpse->short_descr = str_dup( buf );

	    sprintf( buf, corpse->description, name );
	    free_string( corpse->description );
	    corpse->description = str_dup( buf );
	}

	SetObjectProperty(corpse,PROPERTY_STRING,"race",race_table[ch->race].name);
	SetObjectProperty(corpse,PROPERTY_LONG,"bodyparts",&(ch->parts));
	SetObjectProperty(corpse,PROPERTY_STRING,"bodypartsource",name);
    }

    //
    // get all junk from mob/player into room or into corpse
    //
//    for ( obj = ch->carrying; obj != NULL; obj = obj_next )
    while ((obj=ch->carrying)) {
	bool floating = FALSE;

//	obj_next = obj->next_content;
	if (obj->wear_loc == WEAR_FLOAT)
	    floating = TRUE;
	obj_from_char( obj );
	if (obj->item_type == ITEM_POTION)
	    obj->timer = number_range(500,1000);
	if (obj->item_type == ITEM_SCROLL)
	    obj->timer = number_range(1000,2500);
	if (STR_IS_SET(obj->strbit_extra_flags,ITEM_ROT_DEATH) && !floating)
	{
	    obj->timer = number_range(5,10);
//	    STR_REMOVE_BIT(obj->strbit_extra_flags,ITEM_ROT_DEATH);
	}
	STR_REMOVE_BIT(obj->strbit_extra_flags,ITEM_VIS_DEATH);

	if ( STR_IS_SET( obj->strbit_extra_flags, ITEM_INVENTORY ) )
	    extract_obj( obj );
	else if (floating) {
	    if (IS_OBJ_STAT(obj,ITEM_ROT_DEATH)) /* get rid of it! */ {
		if (obj->contains != NULL) {
		    OBJ_DATA *in, *in_next;

		    act("$p evaporates,scattering its contents.",
			ch,obj,NULL,TO_ROOM);
		    for (in = obj->contains; in != NULL; in = in_next) {
			in_next = in->next_content;
			obj_from_obj(in);
			obj_to_room(in,ch->in_room);
		    }
		} else
		    act("$p evaporates.", ch,obj,NULL,TO_ROOM);
		extract_obj(obj);
	    } else {
		if (IS_OBJ_STAT(obj,ITEM_MELT_DROP)) {
		    act("$p dissolves into smoke.",ch,obj,NULL,TO_ROOM);
		    act("$p dissolves into smoke.",ch,obj,NULL,TO_CHAR);
		    extract_obj(obj);
		} else {
		    act("$p falls to the floor.",ch,obj,NULL,TO_ROOM);
		    obj_to_room(obj,ch->in_room);
		}
	    }
	} else {
	    if (corpse)
		obj_to_obj( obj, corpse );
	    else
		obj_to_room(obj,ch->in_room);
	}
    }

    //
    // some fancy animating thingies
    //
    if (corpse) {
	ROOM_DATA *morgue=get_room_index(ROOM_VNUM_MORGUE);
	OBJ_INDEX_DATA *ibody_bag=get_obj_index(OBJ_VNUM_BODYBAG);
	if (!killer && morgue && ibody_bag) {
            // hmmm, no killer. either drowned or deathtrapped
            OBJ_DATA *body_bag;
            CHAR_DATA *wch;
	    EXTRA_DESCR_DATA *ed;
	    char tag_text[]="The tag says: '%s'\n\r";
	    int length;

            body_bag=create_object(ibody_bag, 0);
	    ed=new_extra_descr();
	    ed->keyword         = str_dup( "tag" );
	    length=strlen(tag_text)+str_len(NAME(ch))+1-2; // -2 because %s is gone afterwards
	    ed->description	= alloc_mem(length);
	    snprintf(ed->description,length,tag_text,NAME(ch));
	    ed->next            = body_bag->extra_descr;
	    body_bag->extra_descr    = ed;
            obj_to_obj(corpse,body_bag);
            obj_to_room(body_bag,morgue);
            for (wch=morgue->people;wch;wch=wch->next_in_room)
                send_to_char("A new corpse has arrived.",wch);   
	} else {
	    obj_to_room( corpse, ch->in_room );

	    if (corpse->in_room->sector_type==SECT_AIR)
		object_drop_air(corpse);
	    else if (corpse->in_room->sector_type==SECT_WATER_NOSWIM ||
		     corpse->in_room->sector_type==SECT_WATER_SWIM ||
		     corpse->in_room->sector_type==SECT_WATER_BELOW )
		object_drop_water(corpse);
	    else if (number_percent()<10)
		object_drop_fall(corpse);
	}
	op_load_trigger(corpse,NULL);
    }
    return corpse;
}


struct body_parts {
    int vnum;
    int part;
    long subparts; // all subparts, no recursive lookup is done
    char *drop_msg;
    char *carve_msg; // text is prefixed with "you cut " and "$n cuts "
    } body_parts[] = {
	{ OBJ_VNUM_HAND,	PART_HANDS,	PART_FINGERS,
	  "$n's hand is sliced off $s arm.",				"a hand off $P."	},
	{ OBJ_VNUM_FOOT,	PART_FEET,	0,
	  "$n's foot is sliced off $s leg.",				"a foot off $P."	},
	{ OBJ_VNUM_FINGER,	PART_FINGERS,	0,
	  "One of $n's fingers is sliced off.",				"a finger off $P."	},
	{ OBJ_VNUM_EAR,		PART_EAR,	0,
	  "One of $n's ears is sliced off.",				"an ear off $P."	},
	{ OBJ_VNUM_EYE,		PART_EYE,	0,
	  "$n's eye is cut out of $s skull.",				"an eye out of $P."	},
	{ OBJ_VNUM_LONG_TONGUE,	PART_LONG_TONGUE, 0,
	  "$n's tongue is sliced off.",					"a tongue out of $P."	},
	{ OBJ_VNUM_EYESTALK,	PART_EYESTALKS,	PART_EYE,
	  "$n's eyestalk is sliced off.",				"an eyestalk off $P."	},
	{ OBJ_VNUM_TENTACLE,	PART_TENTACLES,	0,
	  "One of $n's tentacle is sliced off.",			"a tentacle off $P."	},
	{ OBJ_VNUM_FIN,		PART_FINS,	0,
	  "One of $n's fins is sliced off.",				"a fin off $P."		},
	{ OBJ_VNUM_WING,	PART_WINGS,	0,
	  "One of $n's wings is sliced off.",				"a wing off $P."	},
	{ OBJ_VNUM_TAIL,	PART_TAIL,	0,
	  "$n's tail is sliced off.",					"a tail off $P."	},
	{ OBJ_VNUM_CLAW,	PART_CLAWS,	0,
	  "One of $n's claws is sliced off.",				"a claw off $P."	},
	{ OBJ_VNUM_FANG,	PART_FANGS,	0,
	  "One of $n's fangs is sliced off.",				"a fang out of $P."	},
	{ OBJ_VNUM_HORN,	PART_HORNS,	0,
	  "One of $n's horns is sliced off.",				"a horn off $P."	},
	{ OBJ_VNUM_SCALES,	PART_SCALES,	0,
	  "Some of $n's scales are sliced off.",			"some scales off $P."	},
	{ OBJ_VNUM_TUSK,	PART_TUSKS,	0,
	  "One of $n's tusks is sliced off.",				"a tusk out of $P."	},
	{ OBJ_VNUM_SEVERED_HEAD,PART_HEAD,	PART_EAR|PART_EYE|PART_LONG_TONGUE|PART_EYESTALKS|PART_FANGS|PART_TUSKS|PART_BRAINS,
	  "$n's severed head plops on the ground.",			"a head off $P."	},
	{ OBJ_VNUM_TORN_HEART,	PART_HEART,	0,
	  "$n's heart is torn from $s chest.",				"a heart out of $P."	},
	{ OBJ_VNUM_SLICED_ARM,	PART_ARMS,	PART_HANDS|PART_FINGERS,
	  "$n's arm is sliced from $s dead body.",			"an arm off $P."	},
	{ OBJ_VNUM_SLICED_LEG,	PART_LEGS,	PART_FEET,
	  "$n's leg is sliced from $s dead body.",			"a leg off $P."		},
	{ OBJ_VNUM_GUTS,	PART_GUTS,	0,
	  "$n spills $s guts all over the floor.",			"the guts out of $P."	},
	{ OBJ_VNUM_BRAINS,	PART_BRAINS,	0,
	  "$n's head is shattered, and $s brains splash all over you.",	"the brains out of $P."	},
	{ 0,			0,		0,
	  NULL,								NULL				}
    };

static OBJ_DATA *create_bodypart(int vnum, char *name) {
	OBJ_DATA *obj;
	char buf[MAX_STRING_LENGTH];

	obj		= create_object( get_obj_index(vnum), 0 );
	obj->timer	= number_range( 4, 7 );

	sprintf( buf, obj->short_descr, name );
	free_string( obj->short_descr );
	obj->short_descr = str_dup( buf );

	sprintf( buf, obj->description, name );
	free_string( obj->description );
	obj->description = str_dup( buf );


	return obj;
}

/*
 * Improved Death_cry contributed by Diavolo.
 */
void death_cry( CHAR_DATA *ch, CHAR_DATA *killer )
{
    ROOM_INDEX_DATA *was_in_room;
    char *msg;
    int i,part,door;
    int parts_avail=0;
    long chance;
    OBJ_DATA *weapon;

    // chance of a bodypart dependes on:
    // prim waepon is bladed: x1
    // prim weapon non bladed, sec w. bladed: x0.5
    // weapon skill
    // carve skill
    // range: 0.2 - 0.7

    weapon=get_eq_char(killer,WEAR_WIELD);

    if (weapon &&
	weapon->item_type==ITEM_WEAPON &&
	   (weapon->value[0]==WEAPON_EXOTIC ||
	    weapon->value[0]==WEAPON_SWORD ||
	    weapon->value[0]==WEAPON_DAGGER ||
	    weapon->value[0]==WEAPON_AXE)) {

	chance=get_weapon_skill(killer,get_weapon_sn(killer,FALSE))*get_skill(killer,gsn_carve);
	chance=20+chance/200;

	if (number_percent()<chance) {
	    for (part=0;body_parts[part].drop_msg;part++)
		if (IS_SET(ch->parts,body_parts[part].part))
		    parts_avail++;
	    check_improve(killer,gsn_carve,TRUE,1);
	} else 
	    check_improve(killer,gsn_carve,FALSE,1);
    }

    if (parts_avail==0) {
	msg = "You hear $n's death cry.";
	part=-1;
    } else {
	i=number_range(0,parts_avail-1);
	for (part=0;body_parts[part].drop_msg;part++)
	    if (IS_SET(ch->parts,body_parts[part].part))
		if ((i--)==0) break;

	if (body_parts[part].drop_msg==NULL) {
	    bugf("death_cry(): selected part > amount of parts");
	    part=-1;
	    msg = "$n hits the ground ... DEAD.";
	} else {
	    msg=body_parts[part].drop_msg;
	}
    }

    act( msg, ch, NULL, NULL, TO_ROOM );

    if ( part>=0 )
    {
	OBJ_DATA *obj;
	long subparts,mask;
	int skill_level;
	char *name;

	name=IS_NPC(ch) ? ch->short_descr : ch->name;
	obj=create_bodypart(body_parts[part].vnum,name);

	if (obj->item_type == ITEM_FOOD)
	{
	    if ( IS_AFFECTED(ch, EFF_POISON) )
		obj->value[3]=1;
	    if (IS_SET(ch->form,FORM_POISON))
		obj->value[3] = 1;
	    else if (!IS_SET(ch->form,FORM_EDIBLE))
		obj->item_type = ITEM_TRASH;
	}

	subparts=ch->parts & body_parts[part].subparts;
	ch->parts&=~(body_parts[part].subparts | body_parts[part].part);

	// randomly remove parts from the NPC and OBJ
	mask=0;
	skill_level=get_skill(killer,gsn_carve);
	for (i=0;body_parts[i].drop_msg;i++)
	    if (number_percent()>skill_level) mask|=body_parts[i].part;


	subparts&=~mask;
	ch->parts&=~mask;
	if (subparts) {
	    SetObjectProperty(obj,PROPERTY_LONG,"bodyparts",&subparts);
	    SetObjectProperty(obj,PROPERTY_STRING,"bodypartsource",name);
	}
	SetObjectProperty(obj,PROPERTY_INT,"bodypartlevel",&(ch->level));

	obj_to_room( obj, ch->in_room );
	op_load_trigger(obj,NULL);
    }

    if ( IS_NPC(ch) )
	msg = "You hear something's death cry.";
    else
	msg = "You hear someone's death cry.";

    was_in_room = ch->in_room;
    for ( door = 0; door < DIR_MAX; door++ )
    {
	EXIT_DATA *pexit;

	if ( ( pexit = was_in_room->exit[door] ) != NULL
	&&   pexit->to_room != NULL
	&&   pexit->to_room != was_in_room )
	{
	    ch->in_room = pexit->to_room;
	    act( msg, ch, NULL, NULL, TO_ROOM );
	}
    }
    ch->in_room = was_in_room;

    return;
}

void do_carve( CHAR_DATA *ch, char *argument ) {
    OBJ_DATA *obj;
    char name[MSL]="somebody";
    char buf[MSL];
    long parts;
    int parts_avail=0;
    int i;
    int part;
    OBJ_DATA *weapon;

    if (argument[0]==0) {
	send_to_char("Carve from what?\n\r",ch);
	return;
    }

    obj=get_obj_list(ch,argument,ch->carrying);
    if (obj==NULL)
	obj=get_obj_list(ch,argument,ch->in_room->contents);

    if (obj==NULL) {
	sprintf_to_char(ch,"I see no %s here.\n\r",argument);
	return;
    }

    if (GetObjectProperty(obj,PROPERTY_LONG,"bodyparts",&parts)==0 || parts==0) {
	sprintf_to_char(ch,"%s has no parts to carve.\n\r",obj->short_descr);
	return;
    }
    GetObjectProperty(obj,PROPERTY_STRING,"bodypartsource",name);

    weapon=get_eq_char(ch,WEAR_WIELD);
    if (weapon &&
	weapon->item_type==ITEM_WEAPON &&
	   (weapon->value[0]==WEAPON_EXOTIC ||
	    weapon->value[0]==WEAPON_SWORD ||
	    weapon->value[0]==WEAPON_DAGGER ||
	    weapon->value[0]==WEAPON_AXE)) {
	long chance;

	chance=get_weapon_skill(ch,get_weapon_sn(ch,FALSE))*get_skill(ch,gsn_carve);
	chance=20+chance/200;

	if (number_percent()<chance) {
	    for (part=0;body_parts[part].drop_msg;part++)
		if (IS_SET(parts,body_parts[part].part))
		    parts_avail++;
	    check_improve(ch,gsn_carve,TRUE,1);
	} else 
	    check_improve(ch,gsn_carve,FALSE,1);
    } else {
	send_to_char("You need something sharp to carve.\n\r",ch);
	return;
    }

    if (parts_avail>0) {
	OBJ_DATA *newobj;
	long subparts,mask;
	int skill_level;
	int obj_level;

	i=number_range(0,parts_avail-1);
	for (part=0;body_parts[part].drop_msg;part++)
	    if (IS_SET(parts,body_parts[part].part))
		if ((i--)==0) break;

	newobj=create_bodypart(body_parts[part].vnum,name);
	newobj->level=obj->level;

	if (newobj->item_type == ITEM_FOOD)
	{
	    newobj->value[3]=obj->value[3];
	    if (obj->item_type==ITEM_TRASH)
		newobj->item_type=ITEM_TRASH;
	}

	subparts=parts & body_parts[part].subparts;
	parts&=~(body_parts[part].subparts | body_parts[part].part);

	// randomly remove parts from the NPC and OBJ
	mask=0;
	skill_level=get_skill(ch,gsn_carve);
	for (i=0;body_parts[i].drop_msg;i++)
	    if (number_percent()>skill_level) mask|=body_parts[i].part;

	subparts&=~mask;
	parts&=~mask;
	if (subparts) {
	    SetObjectProperty(newobj,PROPERTY_LONG,"bodyparts",&subparts);
	    SetObjectProperty(newobj,PROPERTY_STRING,"bodypartsource",name);
	}
	SetObjectProperty(obj,PROPERTY_LONG,"bodyparts",&parts);
        if (GetObjectProperty(obj,PROPERTY_INT,"bodypartlevel",&obj_level)) {
	    SetObjectProperty(newobj,PROPERTY_INT,"bodypartlevel",&obj_level);
	}

	sprintf(buf,"You cut %s",body_parts[part].carve_msg);
	act_new(buf,ch,newobj,obj,TO_CHAR,POS_RESTING);

	sprintf(buf,"$n cuts %s",body_parts[part].carve_msg);
	act_new(buf,ch,newobj,obj,TO_ROOM,POS_RESTING);

	obj_to_char( newobj, ch );
	op_load_trigger(newobj,NULL);
    } else {
	long mask;
	int skill_level;

	act_new("You only manage to mangle $p further.",ch,obj,NULL,TO_CHAR,POS_RESTING);
	act_new("$n attempts to carve something out of $p.",ch,obj,NULL,TO_ROOM,POS_RESTING); 

	mask=0;
	skill_level=get_skill(ch,gsn_carve);
	for (i=0;body_parts[i].drop_msg;i++)
	    if (number_percent()>skill_level) mask|=body_parts[i].part;
	parts&=~mask;
	SetObjectProperty(obj,PROPERTY_LONG,"bodyparts",&parts);
    }
}


void update_mobkill_table(MOB_INDEX_DATA *mid) {
    int i,j;

    //
    // the table is filled in from lowest to highest. i.e. #0 is the lowest
    //

    //
    // if it is lower than the first, don't bother
    //
    if (mobkill_table[0] && mid->killed<mobkill_table[0]->killed)
	return;

    //
    // if it is the same as the first, add it
    //
    if (mobkill_table[0] && mid->killed==mobkill_table[0]->killed) {
	int found=-1;
	for (i=0;i<MAX_MOBKILL_SIZE;i++) {
	    if (mobkill_table[i]==mid)
		found=i;
	    if (mobkill_table[i]->killed!=mid->killed)
		break;
	}
	if (found>=0) {
	    if (i==MAX_MOBKILL_SIZE) {
		mobkill_table[found]=mobkill_table[MAX_MOBKILL_SIZE-1];
		mobkill_table[MAX_MOBKILL_SIZE-1]=mid;
	    } else {
		mobkill_table[found]=mobkill_table[i-1];
		mobkill_table[i-1]=mid;
	    }
	} else {
	    if (i==MAX_MOBKILL_SIZE)
		mobkill_table[MAX_MOBKILL_SIZE-1]=mid;
	    else
		mobkill_table[i-1]=mid;
	}
	return;
    }

    for (i=MAX_MOBKILL_SIZE-1;i>=0;i--) {
	//
	// if the list isn't filled in completly, just add it here
	//
	if (mobkill_table[i]==NULL) {
	    mobkill_table[i]=mid;
	    return;
	}

	//
	// the vnum is already in the table once. now swap it with
	// the last in the array with this number
	//
	if (mobkill_table[i]->vnum==mid->vnum) {
	    for (j=i+1;j<MAX_MOBKILL_SIZE;j++)
		if (mobkill_table[i]->killed!=mobkill_table[j]->killed)
		    break;
	    mobkill_table[i]=mobkill_table[j-1];
	    mobkill_table[j-1]=mid;
	    return;
	}
    }
}


/* victim is killed by ch */
OBJ_DATA *raw_kill( CHAR_DATA *victim , CHAR_DATA *killer)
{
    int i;
    bool arena=in_safe_kill_room(killer, victim);
    OBJ_INDEX_DATA *obj_index;
    OBJ_DATA *obj;
    OBJ_DATA *corpse=NULL;

    if (is_affected(victim,gsn_entangle)) {
	obj_index=get_obj_index( OBJ_VNUM_TENDRILS );
	obj = create_object( obj_index, 0 );
	obj_to_room(obj,victim->in_room);
	obj->timer=number_range(3,5);
	effect_strip(victim,gsn_entangle);
	op_load_trigger(obj,NULL);
    }

    if (IS_PC(victim) && STR_IS_SET(victim->strbit_act,PLR_HARDCORE)) {
	if (killer) {
	    char buf[MSL];
	    sprintf(buf,"killed by %s.",NAME(killer));
	    set_title(victim,buf);
	}

	victim->hit=-11;
	update_pos(victim);
	stop_fighting( victim, TRUE );
	return NULL;
    }

    // poor pet has died!
    if (STR_IS_SET(victim->strbit_act,ACT_PET) &&
	victim->master &&
	victim->in_room!=victim->master->in_room) {
	send_to_char("You had a sad feeling for a moment, "
	    "then it passes.\n\r",victim->master);
    }

    if (killer && victim && IS_PC(killer) && IS_NPC(victim))
	STR_SET_BIT(killer->pcdata->killedthatmob,victim->pIndexData->vnum);
    if (IS_PC(victim))
	victim->pcdata->killed++;

    // just make the corpses-to-be visible for everybody.
    // invisible mobs leave invisible corpses? don't know.
    STR_REMOVE_BIT( victim->strbit_affected_by, EFF_SNEAK );
    STR_REMOVE_BIT( victim->strbit_affected_by, EFF_DETECT_HIDDEN );

    stop_fighting( victim, TRUE );
    death_cry( victim,killer );
    if (arena) {
        act("You have been rescued by the divine intervention of $t.",
	    victim,players_god(victim),NULL,TO_CHAR);
        act("$n is rescued by the divine intervention of $t.",
	    victim,players_god(victim),NULL,TO_ROOM);
        if (IS_PC(victim)) {
	    wiznet(WIZ_DEATHS,0,victim,NULL,
		"$N died in arena [room %d]",
		victim->in_room->vnum);
	}
    }
    else corpse=make_corpse( victim , killer );

    if ( IS_NPC(victim) )
    {
	update_mobkill_table(victim->pIndexData);
	victim->pIndexData->killed++;
	extract_char( victim, TRUE );
	return corpse;
    }

    if(arena) {
	int to_room_i;
        ROOM_DATA *to_room=NULL;
        ROOM_DATA *from_room=victim->in_room;
	if (GetRoomProperty(from_room,PROPERTY_INT,"resurrection-room",&to_room_i))
	    to_room=get_room_index(to_room_i);
	if (to_room==NULL && victim->clan)
	    to_room=get_room_index(victim->clan->clan_info->hall);
        if (to_room==NULL)
	    to_room=get_room_index(ROOM_VNUM_TEMPLE);
	ap_leave_trigger(victim,to_room);
        if (victim->in_room != NULL)
	    char_from_room( victim );
	char_to_room(victim,to_room);
	ap_enter_trigger(victim,from_room);
    } else {
	// get rid of the pet (kind of)
	CHAR_DATA *pet=victim->pet;

	if (pet) {
	    char buf1[MSL],buf2[MSL];
	    char *p1,*p2;

	    strcpy(buf1,pet->description);
	    strcpy(buf2,pet->description);
	    if ((p1=strstr(buf1,"belong"))!=NULL) {
		p2=strstr(buf2,"belong");
		p2[6]='e';
		p2[7]='d';
		strcpy(p2+8,p1+6);
		free_string(pet->description);
		pet->description=str_dup(buf2);
	    }
	    stop_follower(pet);
	    victim->pet=NULL;
	}
	// and get rid of the player
	extract_char( victim, FALSE );
    }

    if(!arena)
    {
        while ( victim->affected )
	    effect_remove( victim, victim->affected );
        STR_COPY_STR(victim->strbit_affected_by,
		    race_table[victim->race].strbit_eff,MAX_FLAGS);
        for (i = 0; i < 4; i++)
    	    victim->armor[i]= 100;
    }
    victim->position	= POS_RESTING;
    victim->hit		= UMAX( 1, victim->hit  );
    victim->mana	= UMAX( 1, victim->mana );
    victim->move	= UMAX( 1, victim->move );
//  STR_REMOVE_BIT(victim->strbit_act,PLR_KILLER|PLR_THIEF);
/*  save_char_obj( victim ); we're stable enough to not need this :) */
    return corpse;
}


/* zap an object due to alignment-change */
void zap_char_obj(OBJ_DATA *obj,bool removedfromequipment) {
    CHAR_DATA *ch=obj->carried_by;

    act( "You are zapped by $p.", ch, obj, NULL, TO_CHAR );
    act( "$n is zapped by $p.",   ch, obj, NULL, TO_ROOM );

    obj_from_char( obj );
    if ((IS_OBJ_STAT(obj,ITEM_NODROP) || IS_OBJ_STAT(obj,ITEM_NOREMOVE)) &&
	!removedfromequipment) {
	obj_to_char(obj,ch);
    } else if (IS_OBJ_STAT(obj,ITEM_MELT_DROP)) {
	act("$p dissolves into smoke.",ch,obj,NULL,TO_ROOM);
	act("$p dissolves into smoke.",ch,obj,NULL,TO_CHAR);
	extract_obj(obj);
    } else {
	obj_to_room( obj, ch->in_room );
	if (obj->in_room->sector_type==SECT_AIR)
	    object_drop_air(obj);
	else if (obj->in_room->sector_type==SECT_WATER_NOSWIM ||
		 obj->in_room->sector_type==SECT_WATER_SWIM ||
		 obj->in_room->sector_type==SECT_WATER_BELOW )
	    object_drop_water(obj);
	else if (number_percent()<10)
	    object_drop_fall(obj);
    }
}

void group_gain( CHAR_DATA *ch, CHAR_DATA *victim )
{
    CHAR_DATA *gch;
    CHAR_DATA *questdoer;
    int xp;
    int members;
    int group_levels;

    /*
     * Monsters don't get kill xp's or alignment changes.
     * P-killing doesn't help either.
     * Dying of mortal wounds or poison doesn't give xp to anyone!
     */
    if ( victim == ch )
	return;

    members = 0;
    group_levels = 0;
    for ( gch = ch->in_room->people; gch != NULL; gch = gch->next_in_room )
    {
	if ( is_same_group( gch, ch ) )
        {
	    members++;
	    group_levels += IS_NPC(gch) ? gch->level / 2 : gch->level;
	}
    }

    if ( members == 0 )
    {
	bugf( "Group_gain: members==0 for %s and %s.",NAME(ch),NAME(victim));
	members = 1;
	group_levels = ch->level ;
    }

    for ( gch = ch->in_room->people; gch != NULL; gch = gch->next_in_room )
    {

	if ( !is_same_group( gch, ch ) || IS_NPC(gch))
	    continue;

/*	Taken out, add it back if you want it
	if ( gch->level - lch->level >= 5 )
	{
	    send_to_char( "You are too high for this group.\n\r", gch );
	    continue;
	}

	if ( gch->level - lch->level <= -5 )
	{
	    send_to_char( "You are too low for this group.\n\r", gch );
	    continue;
	}
*/

	xp = xp_compute( gch, victim, group_levels );
	if(IS_NPC(victim) && victim->pIndexData->area->area_flags&AREA_UNFINISHED)
	{
	    sprintf_to_char(gch, "This is an unfinished mob. You would have gained %d xp.\n\r", xp);
	    return;
	}

	sprintf_to_char( gch, "You receive %d experience point%s.\n\r", xp, (xp==1)? "" : "s" );
	gain_exp( gch, xp, TRUE );

	questdoer=gch;
	if (IS_NPC(gch)) questdoer=gch->master;
	if (questdoer!=NULL &&			// not a mob without master
	    STR_IS_SET(questdoer->strbit_act, PLR_QUESTOR) &&
	    IS_NPC(victim) &&			// kill of a mob
	    !IS_NPC(questdoer)) {		// questdoer should be a player

	    int quest_mob;

	    GetCharProperty(questdoer,PROPERTY_INT,"quest_mob",&quest_mob);

	    if (quest_mob == victim->pIndexData->vnum) {
		send_to_char("You have almost completed your QUEST!\n\r",
		    questdoer);
		send_to_char(
		    "Return to the questmaster before your time runs out!\n\r",
		    questdoer);
		quest_mob=0;
		SetCharProperty(questdoer,PROPERTY_INT,
		    "quest_mob",&quest_mob);
	    }
	}

	char_obj_alignment_check(gch);
    }

    return;
}

/*
 * Compute xp for a kill.
 * Also adjust alignment of killer.
 * Edit this function to change xp computations.
 */
int xp_compute( CHAR_DATA *gch, CHAR_DATA *victim, int total_levels )
{
    int xp,base_exp;
    int align,level_range;
    int change;
    int time_per_level;
    int alignment_before=gch->alignment;

    level_range = victim->level - gch->level;

    /* compute the base exp */
    switch (level_range)
    {
 	default : 	base_exp =   0;		break;
	case -9 :	base_exp =   1;		break;
	case -8 :	base_exp =   2;		break;
	case -7 :	base_exp =   5;		break;
	case -6 : 	base_exp =   9;		break;
	case -5 :	base_exp =  11;		break;
	case -4 :	base_exp =  22;		break;
	case -3 :	base_exp =  33;		break;
	case -2 :	base_exp =  50;		break;
	case -1 :	base_exp =  66;		break;
	case  0 :	base_exp =  83;		break;
	case  1 :	base_exp =  99;		break;
	case  2 :	base_exp = 121;		break;
	case  3 :	base_exp = 143;		break;
	case  4 :	base_exp = 165;		break;
    }

    if (level_range > 4)
	base_exp = 160 + 20 * (level_range - 4);

    /* do alignment computations */

    align = victim->alignment - gch->alignment;

    if (STR_IS_SET(victim->strbit_act,ACT_NOALIGN))
    {
	/* no change */
    }

    else if (align > 500) /* monster is more good than slayer */
    {
	change = (align - 500) * base_exp / 500 * gch->level/total_levels;
	change = UMAX(1,change);
        gch->alignment = UMAX(-1000,gch->alignment - change);
    }

    else if (align < -500) /* monster is more evil than slayer */
    {
	change =  ( -1 * align - 500) * base_exp/500 * gch->level/total_levels;
	change = UMAX(1,change);
	gch->alignment = UMIN(1000,gch->alignment + change);
    }

    else /* improve this someday */
    {
	change =  gch->alignment * base_exp/500 * gch->level/total_levels;
	gch->alignment -= change;
    }

    /* calculate exp multiplier */
    if (STR_IS_SET(victim->strbit_act,ACT_NOALIGN))
	xp = base_exp;

    else if (gch->alignment > 500)  /* for goodie two shoes */
    {
	if (victim->alignment < -750)
	    xp = (base_exp *4)/3;

 	else if (victim->alignment < -500)
	    xp = (base_exp * 5)/4;

        else if (victim->alignment > 750)
	    xp = base_exp / 4;

   	else if (victim->alignment > 500)
	    xp = base_exp / 2;

        else if (victim->alignment > 250)
	    xp = (base_exp * 3)/4;

	else
	    xp = base_exp;
    }

    else if (gch->alignment < -500) /* for baddies */
    {
	if (victim->alignment > 750)
	    xp = (base_exp * 5)/4;

  	else if (victim->alignment > 500)
	    xp = (base_exp * 11)/10;

   	else if (victim->alignment < -750)
	    xp = base_exp/2;

	else if (victim->alignment < -500)
	    xp = (base_exp * 3)/4;

	else if (victim->alignment < -250)
	    xp = (base_exp * 9)/10;

	else
	    xp = base_exp;
    }

    else if (gch->alignment > 200)  /* a little good */
    {

	if (victim->alignment < -500)
	    xp = (base_exp * 6)/5;

 	else if (victim->alignment > 750)
	    xp = base_exp/2;

	else if (victim->alignment > 0)
	    xp = (base_exp * 3)/4;

	else
	    xp = base_exp;
    }

    else if (gch->alignment < -200) /* a little bad */
    {
	if (victim->alignment > 500)
	    xp = (base_exp * 6)/5;

	else if (victim->alignment < -750)
	    xp = base_exp/2;

	else if (victim->alignment < 0)
	    xp = (base_exp * 3)/4;

	else
	    xp = base_exp;
    }

    else /* neutral */
    {

	if (victim->alignment > 500 || victim->alignment < -500)
	    xp = (base_exp * 4)/3;

	else if (victim->alignment < 200 && victim->alignment > -200)
	    xp = base_exp/2;

 	else
	    xp = base_exp;
    }

    /* more exp at the low levels */
    if (gch->level < 6)
    	xp = 10 * xp / (gch->level + 4);

    /* less at high */
    if (gch->level > 35 )
	xp =  15 * xp / (gch->level - 25 );

    /* reduce for playing time */

    {
	/* compute quarter-hours per level */
	time_per_level = 4 *
			 (gch->played + (int) (current_time - gch->logon))/3600
			 / gch->level;

	time_per_level = URANGE(2,time_per_level,12);
	if (gch->level < 15)  /* make it a curve */
	    time_per_level = UMAX(time_per_level,(15 - gch->level));
	xp = xp * time_per_level / 10;
/* Was: xp = xp * time_per_level / 12; */
    }

    /* randomize the rewards */
    xp = number_range (xp * 3/4, xp * 5/4);

    /* adjust for grouping */
    xp = xp * gch->level/( UMAX(1,total_levels -1) );

    if (IS_NEUTRAL(gch) && alignment_before<=-350) {
	send_to_char(
	    "You have taken the first step back into the {WLight{x.\n\r",gch);
    } else if (IS_NEUTRAL(gch) && alignment_before>=350) {
	send_to_char("You are now walking the narrow path between {WGood{x "
	    "and {REvil{x don't wander off {Btoo{x far.\n\r",gch);
    } else if (IS_EVIL(gch) && alignment_before>-350) {
	send_to_char("You hear a dark voice in the distance "
	    "'{gWelcome to the {RDark{g side{x'\n\r",gch);
    } else if (IS_GOOD(gch) && alignment_before<350) {
	send_to_char("The {WLight{x side {Bembraces{x you once more.\n\r",gch);
    }

    char_obj_alignment_check(gch);

    return xp;
}


void dam_message( CHAR_DATA *ch, CHAR_DATA *victim,int dam,int dt,bool immune )
{
    char buf1[256], buf2[256], buf3[256];
    char dam_info[32];
    const char *vs;
    const char *vp;
    const char *attack;
    char punct;

    if (ch == NULL || victim == NULL)
	return;

    if (!STR_IS_SET(ch->strbit_comm,COMM_SHOW_PAIN)) {
	     if ( dam ==   0 ) { vs = "miss";	vp = "misses";		}
	else if ( dam <=   4 ) { vs = "scratch";vp = "scratches";	}
	else if ( dam <=   8 ) { vs = "graze";	vp = "grazes";		}
	else if ( dam <=  12 ) { vs = "hit";	vp = "hits";		}
	else if ( dam <=  16 ) { vs = "injure";	vp = "injures";		}
	else if ( dam <=  20 ) { vs = "wound";	vp = "wounds";		}
	else if ( dam <=  24 ) { vs = "maul";   vp = "mauls";		}
	else if ( dam <=  28 ) { vs = "decimate";vp = "decimates";	}
	else if ( dam <=  32 ) { vs = "devastate";vp = "devastates";	}
	else if ( dam <=  36 ) { vs = "maim";	vp = "maims";		}
	else if ( dam <=  40 ) { vs = "MUTILATE";vp = "MUTILATES";	}
	else if ( dam <=  44 ) { vs = "DISEMBOWEL";vp = "DISEMBOWELS";	}
	else if ( dam <=  48 ) { vs = "DISMEMBER";vp = "DISMEMBERS";	}
	else if ( dam <=  52 ) { vs = "MASSACRE";vp = "MASSACRES";	}
	else if ( dam <=  56 ) { vs = "MANGLE";	vp = "MANGLES";		}
	else if ( dam <=  60 ) { vs = "*** DEMOLISH ***";
				 vp = "*** DEMOLISHES ***";		}
	else if ( dam <=  75 ) { vs = "*** DEVASTATE ***";
				 vp = "*** DEVASTATES ***";		}
	else if ( dam <= 100)  { vs = "=== OBLITERATE ===";
				 vp = "=== OBLITERATES ===";		}
	else if ( dam <= 125)  { vs = ">>> ANNIHILATE <<<";
				 vp = ">>> ANNIHILATES <<<";		}
	else if ( dam <= 150)  { vs = "<<< ERADICATE >>>";
				 vp = "<<< ERADICATES >>>";		}
	else                   { vs = "do UNSPEAKABLE things to";
				 vp = "does UNSPEAKABLE things to";	}

	punct   = (dam <= 24) ? '.' : '!';
    } else {
	int pain_percent;

	if (victim->max_hit<=0) {
	    char s[MSL];
	    sprintf(s,"dam_message(): max_hit<=0 for mob %d\n\r",victim->pIndexData->vnum);
	    return;
	}

	pain_percent = 1000 * dam/victim->max_hit;

	     if ( dam  ==   0 )         { vs = "miss";      vp = "misses";    }
	else if ( pain_percent <=   5 ) { vs = "scratch";   vp = "scratches"; }
	else if ( pain_percent <=  10 ) { vs = "graze";     vp = "grazes";    }
	else if ( pain_percent <=  25 ) { vs = "hit";       vp = "hits";      }
	else if ( pain_percent <=  30 ) { vs = "injure";    vp = "injures";   }
	else if ( pain_percent <=  40 ) { vs = "wound";     vp = "wounds";    }
	else if ( pain_percent <=  55 ) { vs = "maul";      vp = "mauls";     }
	else if ( pain_percent <=  65 ) { vs = "decimate";  vp = "decimates"; }
	else if ( pain_percent <=  70 ) { vs = "devastate"; vp = "devastates";}
	else if ( pain_percent <=  80 ) { vs = "maim";      vp = "maims";     }
	else if ( pain_percent <=  90 ) { vs = "MUTILATE";  vp = "MUTILATES"; }
	else if ( pain_percent <= 100 ) { vs = "DISEMBOWEL";vp ="DISEMBOWELS";}
	else if ( pain_percent <= 120 ) { vs = "DISMEMBER"; vp = "DISMEMBERS";}
	else if ( pain_percent <= 140 ) { vs = "MASSACRE";  vp = "MASSACRES"; }
	else if ( pain_percent <= 160 ) { vs = "MANGLE";    vp = "MANGLES";   }
	else if ( pain_percent <= 180 ) { vs = "*** DEMOLISH ***";
					  vp = "*** DEMOLISHES ***";          }
	else if ( pain_percent <= 190 ) { vs = "*** DEVASTATE ***";
					  vp = "*** DEVASTATES ***";          }
	else if ( pain_percent <= 210 ) { vs = "=== OBLITERATE ===";
					  vp = "=== OBLITERATES ===";         }
	else if ( pain_percent <= 230 ) { vs = ">>> ANNIHILATE <<<";
					  vp = ">>> ANNIHILATES <<<";         }
	else if ( pain_percent <= 250 ) { vs = "<<< ERADICATE >>>";
					  vp = "<<< ERADICATES >>>";          }
	else                   		{ vs = "do UNSPEAKABLE things to";
					  vp = "does UNSPEAKABLE things to";  }
	punct   = (pain_percent <= 90) ? '.' : '!';
    }

    if(dam) sprintf(dam_info," [%d]",dam);
    else dam_info[0]='\0';

    if ( dt == TYPE_HIT )
    {
	if (ch  == victim)
	{
	    sprintf( buf1, "%s$n%s %s$melf%c%s{x",
		(dam?C_HITROOM:C_MISROOM),
		(dam?C_HITROOM:C_MISROOM),
		vp,punct,dam_info);
	    sprintf( buf2, "%sYou %s%syourself%c%s{x",
		(dam?C_YOUHIT:C_YOUMIS),
		(dam?C_YOUHIT:C_YOUMIS),
		vs,punct,dam_info);
	}
	else
	{
	    sprintf( buf1, "%s$n %s%s $N%s%c%s{x",
		(dam?C_HITROOM:C_MISROOM),
		(dam?C_HITROOM:C_MISROOM),
		vp,
		(dam?C_HITROOM:C_MISROOM),
		punct,dam_info );
	    sprintf( buf2, "%sYou %s%s $N%s%c%s{x",
		(dam?C_YOUHIT:C_YOUMIS),
		(dam?C_YOUHIT:C_YOUMIS),
		vs,
		(dam?C_YOUHIT:C_YOUMIS),
		punct,dam_info );
	    sprintf( buf3, "%s$n %s%s you%s%c%s{x",
		(dam?C_HITYOU:C_MISYOU),
		(dam?C_HITYOU:C_MISYOU),
		vp,
		(dam?C_HITYOU:C_MISYOU),
		punct,dam_info );
	}
    }
    else
    {
	if ( dt >= 0 && dt < MAX_SKILL )
	    attack	= skill_noun_damage(dt,ch);
	else if ( dt >= TYPE_HIT
	&& dt < TYPE_HIT + MAX_DAMAGE_MESSAGE)
	    attack	= attack_table[dt - TYPE_HIT].noun;
	else
	{
	    bugf( "Dam_message: bad dt %d from %s to %s.",
		dt,NAME(ch),NAME(victim));
	    dt  = TYPE_HIT;
	    attack  = attack_table[0].name;
	}

	if (immune)
	{
	    if (ch == victim)
	    {
		sprintf(buf1,C_MISROOM "$n "C_MISROOM
		    "is unaffected by $s own %s.{x",attack);
		sprintf(buf2,C_YOUMIS "Luckily, you are immune to that.{x");
	    }
	    else
	    {
	    	sprintf(buf1,C_MISROOM "$N "C_MISROOM"is unaffected by $n"
		    C_MISROOM"'s %s!{x",attack);
	    	sprintf(buf2,C_YOUMIS "$N "C_YOUMIS"is unaffected by your %s!{x",attack);
	    	sprintf(buf3,C_MISYOU "$n"C_MISYOU"'s %s is powerless against you.{x",attack);
	    }
	}
	else
	{
	    if (ch == victim)
	    {
		sprintf( buf1, "%s$n%s's %s %s $m%c%s{x",
		    dam?C_HITROOM:C_MISROOM,
		    dam?C_HITROOM:C_MISROOM,
		    attack,vp,
		    punct,dam_info);
		sprintf( buf2, "%sYour %s %s you%c%s{x",
		    dam?C_YOUHIT:C_YOUMIS,
		    attack,
		    vp,punct,dam_info);
	    }
	    else
	    {
	    	sprintf( buf1, "%s$n%s's %s %s $N%s%c%s{x",
		    dam?C_HITROOM:C_MISROOM,
		    dam?C_HITROOM:C_MISROOM,
		    attack, vp,
		    dam?C_HITROOM:C_MISROOM,
		    punct,dam_info );
	    	sprintf( buf2, "%sYour %s %s $N%s%c%s{x",
		    dam?C_YOUHIT:C_YOUMIS,
		    attack, vp,
		    dam?C_YOUHIT:C_YOUMIS,
		    punct,dam_info );
	    	sprintf( buf3, "%s$n%s's %s %s you%c%s{x",
		    dam?C_HITYOU:C_MISYOU,
		    dam?C_HITYOU:C_MISYOU,
		    attack, vp, punct,dam_info );
	    }
	}
    }

    if (ch == victim)
    {
	act(buf1,ch,NULL,NULL,TO_ROOM);
	act(buf2,ch,NULL,NULL,TO_CHAR);
    }
    else
    {
    	act( buf1, ch, NULL, victim, TO_NOTVICT );
    	act( buf2, ch, NULL, victim, TO_CHAR );
    	act( buf3, ch, NULL, victim, TO_VICT );
    }

    return;
}

void dam_message_obj( CHAR_DATA *ch, CHAR_DATA *victim, OBJ_DATA *obj, int dam,int dt,bool immune )
{
    char buf1[256], buf2[256], buf3[256];
    char dam_info[32];
    const char *vs;
    const char *vp;
    const char *attack;
    char punct;

    if (ch == NULL || victim == NULL)
	return;

    if (!STR_IS_SET(ch->strbit_comm,COMM_SHOW_PAIN)) {
	     if ( dam ==   0 ) { vs = "miss";	vp = "misses";		}
	else if ( dam <=   4 ) { vs = "scratch";vp = "scratches";	}
	else if ( dam <=   8 ) { vs = "graze";	vp = "grazes";		}
	else if ( dam <=  12 ) { vs = "hit";	vp = "hits";		}
	else if ( dam <=  16 ) { vs = "injure";	vp = "injures";		}
	else if ( dam <=  20 ) { vs = "wound";	vp = "wounds";		}
	else if ( dam <=  24 ) { vs = "maul";   vp = "mauls";		}
	else if ( dam <=  28 ) { vs = "decimate";vp = "decimates";	}
	else if ( dam <=  32 ) { vs = "devastate";vp = "devastates";	}
	else if ( dam <=  36 ) { vs = "maim";	vp = "maims";		}
	else if ( dam <=  40 ) { vs = "MUTILATE";vp = "MUTILATES";	}
	else if ( dam <=  44 ) { vs = "DISEMBOWEL";vp = "DISEMBOWELS";	}
	else if ( dam <=  48 ) { vs = "DISMEMBER";vp = "DISMEMBERS";	}
	else if ( dam <=  52 ) { vs = "MASSACRE";vp = "MASSACRES";	}
	else if ( dam <=  56 ) { vs = "MANGLE";	vp = "MANGLES";		}
	else if ( dam <=  60 ) { vs = "*** DEMOLISH ***";
				 vp = "*** DEMOLISHES ***";		}
	else if ( dam <=  75 ) { vs = "*** DEVASTATE ***";
				 vp = "*** DEVASTATES ***";		}
	else if ( dam <= 100)  { vs = "=== OBLITERATE ===";
				 vp = "=== OBLITERATES ===";		}
	else if ( dam <= 125)  { vs = ">>> ANNIHILATE <<<";
				 vp = ">>> ANNIHILATES <<<";		}
	else if ( dam <= 150)  { vs = "<<< ERADICATE >>>";
				 vp = "<<< ERADICATES >>>";		}
	else                   { vs = "do UNSPEAKABLE things to";
				 vp = "does UNSPEAKABLE things to";	}

	punct   = (dam <= 24) ? '.' : '!';
    } else {
	int pain_percent;

	if (victim->max_hit<=0) {
	    char s[MSL];
	    sprintf(s,"dam_message(): max_hit<=0 for mob %d\n\r",victim->pIndexData->vnum);
	    return;
	}

	pain_percent = 1000 * dam/victim->max_hit;

	     if ( dam  ==   0 )         { vs = "miss";      vp = "misses";    }
	else if ( pain_percent <=   5 ) { vs = "scratch";   vp = "scratches"; }
	else if ( pain_percent <=  10 ) { vs = "graze";     vp = "grazes";    }
	else if ( pain_percent <=  25 ) { vs = "hit";       vp = "hits";      }
	else if ( pain_percent <=  30 ) { vs = "injure";    vp = "injures";   }
	else if ( pain_percent <=  40 ) { vs = "wound";     vp = "wounds";    }
	else if ( pain_percent <=  55 ) { vs = "maul";      vp = "mauls";     }
	else if ( pain_percent <=  65 ) { vs = "decimate";  vp = "decimates"; }
	else if ( pain_percent <=  70 ) { vs = "devastate"; vp = "devastates";}
	else if ( pain_percent <=  80 ) { vs = "maim";      vp = "maims";     }
	else if ( pain_percent <=  90 ) { vs = "MUTILATE";  vp = "MUTILATES"; }
	else if ( pain_percent <= 100 ) { vs = "DISEMBOWEL";vp ="DISEMBOWELS";}
	else if ( pain_percent <= 120 ) { vs = "DISMEMBER"; vp = "DISMEMBERS";}
	else if ( pain_percent <= 140 ) { vs = "MASSACRE";  vp = "MASSACRES"; }
	else if ( pain_percent <= 160 ) { vs = "MANGLE";    vp = "MANGLES";   }
	else if ( pain_percent <= 180 ) { vs = "*** DEMOLISH ***";
					  vp = "*** DEMOLISHES ***";          }
	else if ( pain_percent <= 190 ) { vs = "*** DEVASTATE ***";
					  vp = "*** DEVASTATES ***";          }
	else if ( pain_percent <= 210 ) { vs = "=== OBLITERATE ===";
					  vp = "=== OBLITERATES ===";         }
	else if ( pain_percent <= 230 ) { vs = ">>> ANNIHILATE <<<";
					  vp = ">>> ANNIHILATES <<<";         }
	else if ( pain_percent <= 250 ) { vs = "<<< ERADICATE >>>";
					  vp = "<<< ERADICATES >>>";          }
	else                   		{ vs = "do UNSPEAKABLE things to";
					  vp = "does UNSPEAKABLE things to";  }
	punct   = (pain_percent <= 90) ? '.' : '!';
    }

    if(dam) sprintf(dam_info," [%d]",dam);
    else dam_info[0]='\0';

    if ( dt == TYPE_HIT )
    {
	if (ch  == victim)
	{
	    sprintf( buf1, "%s$n's $p %s $melf%c%s{x",(dam?C_HITROOM:C_MISROOM),vp,punct,dam_info);
	    sprintf( buf2, "%sYour $p %s yourself%c%s{x",(dam?C_YOUHIT:C_YOUMIS),vs,punct,dam_info);
	}
	else
	{
	    sprintf( buf1, "%s$p %s $N%c%s{x",(dam?C_HITROOM:C_MISROOM),vp, punct,dam_info );
	    sprintf( buf2, "%s$p %s $N%c%s{x",(dam?C_YOUHIT:C_YOUMIS),vs, punct,dam_info );
	    sprintf( buf3, "%s$p %s you%c%s{x",(dam?C_HITYOU:C_MISYOU), vp, punct,dam_info );
	}
    }
    else
    {
	if ( dt >= 0 && dt < MAX_SKILL )
	    attack	= skill_noun_damage(dt,ch);
	else if ( dt >= TYPE_HIT
	&& dt < TYPE_HIT + MAX_DAMAGE_MESSAGE)
	    attack	= attack_table[dt - TYPE_HIT].noun;
	else
	{
	    bugf( "Dam_message: bad dt %d from %s to %s.",
		dt,NAME(ch),NAME(victim));
	    dt  = TYPE_HIT;
	    attack  = attack_table[0].name;
	}

	if (immune)
	{
	    if (ch == victim)
	    {
		sprintf(buf1,C_MISROOM "$n is unaffected by $p's %s.{x",attack);
		sprintf(buf2,C_YOUMIS "Luckily, you are immune to that.{x");
	    }
	    else
	    {
	    	sprintf(buf1,C_MISROOM "$N is unaffected by $p's %s!{x",attack);
	    	sprintf(buf2,C_YOUMIS "$N is unaffected by $p's %s!{x",attack);
	    	sprintf(buf3,C_MISYOU "$p's %s is powerless against you.{x",attack);
	    }
	}
	else
	{
	    if (ch == victim)
	    {
		sprintf( buf1, "%s$p's %s %s $m%c%s{x",dam?C_HITROOM:C_MISROOM,attack,vp,punct,dam_info);
		sprintf( buf2, "%s$p's %s %s you%c%s{x",dam?C_YOUHIT:C_YOUMIS,attack,vp,punct,dam_info);
	    }
	    else
	    {
	    	sprintf( buf1, "%s$p's %s %s $N%c%s{x",dam?C_HITROOM:C_MISROOM, attack, vp, punct,dam_info );
	    	sprintf( buf2, "%s$p's %s %s $N%c%s{x", dam?C_YOUHIT:C_YOUMIS, attack, vp, punct,dam_info );
	    	sprintf( buf3, "%s$p's %s %s you%c%s{x", dam?C_HITYOU:C_MISYOU, attack, vp, punct,dam_info );
	    }
	}
    }

    if (ch == victim)
    {
	act(buf1,ch,obj,NULL,TO_ROOM);
	act(buf2,ch,obj,NULL,TO_CHAR);
    }
    else
    {
    	act( buf1, ch, obj, victim, TO_NOTVICT );
    	act( buf2, ch, obj, victim, TO_CHAR );
    	act( buf3, ch, obj, victim, TO_VICT );
    }

    return;
}
