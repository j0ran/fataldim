//
// $Id: fd_areaprog.c,v 1.19 2006/10/18 19:41:57 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#define NEEDS_TCL
#include "merc.h"
#include "interp.h"
#include "olc.h"

/*
 * Misc thingies
 */
int ap_error(AREA_DATA *area) {
    char *errormsg;

    bugf("AP: TCL error in namespace %s in trigger %s on line %d: %s",
	namespace_area(area),
	progData==NULL?"(null)":progData->trigName,
    interp->errorLineDontUse,
	Tcl_GetStringResult(interp));

    errormsg=str_dup(Tcl_GetVar(interp,"errorInfo",TCL_GLOBAL_ONLY));
    while (errormsg) {
	char *line=strsep(&errormsg,"\n\r");
	logf("AP: errorInfo: %s",line);
	wiznet(WIZ_TCL_DEBUG,0,NULL,NULL,"AP: errorInfo: %s",line);
    }
    free_string(errormsg);

    return TCL_ERROR;
}



/*
 * ---------------------------------------------------------------------
 * Trigger handlers. These are called from various parts of the code
 * when an event is triggered.
 * ---------------------------------------------------------------------
 */

/*
 * general trigger for "normal" character/room interaction
 */

/*
 * In case of a return value (boolean always), a TRUE means
 * "okay, go on" and a FALSE means "this is wrong"
 */
bool ap_general_trigger_bool_all( AREA_DATA *area, int trig_type, CHAR_DATA *ch, ROOM_DATA *room, char *buf) {
    TCLPROG_LIST *prg;
    int boolResult;

// Don't think we need these.
//    if (buf==NULL)
//	Tcl_SetVar(interp,"t","",0);
//    else
//	Tcl_SetVar(interp,"t",buf,0);

    for ( prg = area->aprogs; prg != NULL; prg = prg->next ) {
	if(prg->trig_type==trig_type) {
	    Tcl_Obj *result;

	    if (tcl_start(prg->name,buf,NULL,ch,NULL,NULL,NULL,room,area))
		break;

	    if (prg->trig_phrase && str_cmp("",prg->trig_phrase)) {
		if(tcl_run_ap_code(area,prg->trig_phrase)==TCL_ERROR)
		    goto error;

		result=Tcl_GetObjResult(interp);
		boolResult=FALSE;
		if(Tcl_GetBooleanFromObj(interp,result,&boolResult)==TCL_ERROR)
		    goto error;
	    } else
		boolResult=TRUE;

	    if(boolResult) {
		if (tcl_run_ap_code(area,prg->code)==TCL_ERROR)
		    goto error;
		result=Tcl_GetObjResult(interp);
		boolResult=FALSE;
		if(Tcl_GetBooleanFromObj(interp,result,&boolResult)==TCL_ERROR)
		    goto error;
		if(tcl_stop()) return FALSE;
		return boolResult;
	    }

	    if(tcl_stop()) return FALSE;
	}
    }
    return TRUE;

error:
    ap_error(area);
    if(tcl_stop()) return FALSE;
    return FALSE;
}

void ap_general_trigger_all( AREA_DATA *area, int trig_type, CHAR_DATA *ch, ROOM_DATA *room, char *buf) {
    TCLPROG_LIST *prg;
    int boolResult;

// Don't think we need these.
//    if (buf==NULL)
//	Tcl_SetVar(interp,"t","",0);
//    else
//	Tcl_SetVar(interp,"t",buf,0);

    for ( prg = area->aprogs; prg != NULL; prg = prg->next ) {
	if(prg->trig_type==trig_type) {
	    Tcl_Obj *result;

	    if(tcl_start(prg->name,buf,NULL,ch,NULL,NULL,NULL,room,area)) return;

	    if (prg->trig_phrase && str_cmp("",prg->trig_phrase)) {
		if(tcl_run_ap_code(area,prg->trig_phrase)==TCL_ERROR)
		    goto error;

		result=Tcl_GetObjResult(interp);
		boolResult=FALSE;
		if(Tcl_GetBooleanFromObj(interp,result,&boolResult)==TCL_ERROR)
		    goto error;
	    } else
		boolResult=TRUE;

	    if(boolResult) {
		if (tcl_run_ap_code(area,prg->code)==TCL_ERROR)
		    goto error;
		if(tcl_stop()) return;
		break;
	    }

	    if(tcl_stop()) return;
	}
    }
    return;

error:
    ap_error(area);
    if(tcl_stop()) return;
    return;
}


//
// by making these wrappers it's easier to add new fields to the
// ap_general_trigger() function.
//
// trigger1: area
// trigger2: area + text
//
bool ap_general_trigger1_bool(int trig_type,AREA_DATA *area)
    { return ap_general_trigger_bool_all(area,trig_type,NULL,NULL,NULL); }
bool ap_general_trigger2_bool(int trig_type,AREA_DATA *area,char *txt)
    { return ap_general_trigger_bool_all(area,trig_type,NULL,NULL,txt); }

void ap_general_trigger1(int trig_type,AREA_DATA *area)
    { ap_general_trigger_all(area,trig_type,NULL,NULL,NULL); }
void ap_general_trigger2(int trig_type,AREA_DATA *area,char *txt)
    { ap_general_trigger_all(area,trig_type,NULL,NULL,txt); }
void ap_general_trigger3(int trig_type,AREA_DATA *area,ROOM_DATA *room,CHAR_DATA *ch)
    { ap_general_trigger_all(area,trig_type,ch,room,NULL); }

//
//
//

//
// reset trigger
//
bool ap_prereset_trigger(AREA_DATA *area) {
    return ap_general_trigger1_bool(ATRIG_PRERESET,area);
}
void ap_reset_trigger(AREA_DATA *area) {
    ap_general_trigger1(ATRIG_RESET,area);
}

//
// timed triggers
//
void	ap_hour_trigger(AREA_DATA *area,int hour) {
    char buf[INT_STRING_BUF_SIZE];
    itoa_r(hour,buf,sizeof(buf));
    ap_general_trigger2(ATRIG_HOUR,area,buf);
}
void	ap_delay_trigger(AREA_DATA *area) {
    ap_general_trigger1(ATRIG_DELAY,area);
}
void	ap_timer_trigger(AREA_DATA *area) {
    char buf[LONG_STRING_BUF_SIZE];
    ltoa_r(area->aprog_timer,buf,sizeof(buf));
    ap_general_trigger2(ATRIG_TIMER,area,buf);
}
void	ap_random_trigger(AREA_DATA *area) {
    ap_general_trigger1(ATRIG_RANDOM,area);
}

//
// day / sun triggers
//
void    ap_sunset_trigger(void) {
    AREA_DATA *area;
    for (area=area_first;area!=NULL;area=area->next)
	if (AREA_HAS_TRIGGER(area,ATRIG_SUNSET))
	    ap_general_trigger1(ATRIG_SUNSET,area);
}
void    ap_sunrise_trigger(void) {
    AREA_DATA *area;
    for (area=area_first;area!=NULL;area=area->next)
	if (AREA_HAS_TRIGGER(area,ATRIG_SUNRISE))
	    ap_general_trigger1(ATRIG_SUNRISE,area);
}
void    ap_dayend_trigger(void) {
    AREA_DATA *area;
    for (area=area_first;area!=NULL;area=area->next)
	if (AREA_HAS_TRIGGER(area,ATRIG_DAYEND))
	    ap_general_trigger1(ATRIG_DAYEND,area);
}
void    ap_daystart_trigger(void) {
    AREA_DATA *area;
    for (area=area_first;area!=NULL;area=area->next)
	if (AREA_HAS_TRIGGER(area,ATRIG_DAYSTART))
	    ap_general_trigger1(ATRIG_DAYSTART,area);
}

void    ap_weather_trigger(int old_state, int new_state) {
    char *old_descr,*new_descr;
    char *text;
    AREA_DATA *area;

    old_descr=table_find_value(old_state,weather_sky_table);
    new_descr=table_find_value(new_state,weather_sky_table);
    text=malloc(strlen(old_descr)+strlen(new_descr)+2);

    strcpy(text,new_descr);
    strcat(text," ");                       
    strcat(text,old_descr);
    for (area=area_first;area!=NULL;area=area->next)
	if (AREA_HAS_TRIGGER(area,ATRIG_WEATHER))
	    ap_general_trigger2(ATRIG_WEATHER,area,text);
    free(text);
}

void	ap_leave_trigger(CHAR_DATA *ch, ROOM_DATA *to_room) {
    AREA_DATA *from_area=ch->in_room?ch->in_room->area:NULL;
    AREA_DATA *to_area=NULL;

    if (!IS_PC(ch)) return;

    if (to_room) to_area=to_room->area;

    if (from_area==to_area) return;

    if (from_area && AREA_HAS_TRIGGER(from_area,ATRIG_LEAVE))
	ap_general_trigger3(ATRIG_LEAVE,from_area,ch->in_room,ch);
}

void	ap_enter_trigger(CHAR_DATA *ch, ROOM_DATA *from_room) {
    AREA_DATA *from_area=NULL;
    AREA_DATA *to_area=ch->in_room?ch->in_room->area:NULL;

    if (!IS_PC(ch)) return;

    if (from_room) from_area=from_room->area;

    if (from_area==to_area) return;

    if (to_area && AREA_HAS_TRIGGER(to_area,ATRIG_ENTER))
	ap_general_trigger3(ATRIG_ENTER,to_area,ch->in_room,ch);
}
