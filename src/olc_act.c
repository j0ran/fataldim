/* $Id: olc_act.c,v 1.79 2008/05/11 20:50:47 jodocus Exp $ */
/***************************************************************************
 *  File: olc_act.c                                                        *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 *                                                                         *
 *  This code was freely distributed with the The Isles 1.1 source code,   *
 *  and has been used here for OLC - OLC would not be what it is without   *
 *  all the previous coders who released their source code.                *
 *                                                                         *
 ***************************************************************************/



#include "merc.h"
#include "olc.h"
#include "interp.h"
#include "db.h"

struct olc_mob_recommended olc_mob_recommended[91]={
/* 0 */	{{ 2,3,10},	{40,1,99},	{1,3,3},	10},
/* 1 */ {{ 2,6,10},	{40,2,99},	{1,4,0},	9},
/* 2 */ {{ 2,7,21},	{40,3,99},	{1,5,0},	8},
/* 3 */ {{ 2,6,35},	{40,4,99},	{1,6,0},	7},
/* 4 */ {{ 2,7,46},	{40,5,99},	{1,5,1},	6},
/* 5 */ {{ 2,6,60},	{40,6,99},	{1,6,1},	5},
/* 6 */ {{ 2,7,71},	{40,7,99},	{1,7,1},	4},
/* 7 */ {{ 2,6,85},	{40,8,99},	{1,8,1},	4},
/* 8 */ {{ 2,7,96},	{40,9,99},	{1,7,2},	3},
/* 9 */ {{ 2,6,110},	{40,10,99},	{1,8,2},	2},
/*10 */ {{ 2,7,121},	{40,11,99},	{2,4,2},	1},
/*11 */ {{ 2,8,134},	{40,12,99},	{1,10,2},	1},
/*12 */ {{ 2,10,150},	{40,13,99},	{1,10,3},	0},
/*13 */ {{ 2,10,170},	{40,14,99},	{2,5,3},	-1},
/*14 */ {{ 2,10,190},	{40,15,99},	{1,12,3},	-1},
/*15 */ {{ 3,9,208},	{40,16,99},	{2,6,3},	-2},
/*16 */ {{ 3,9,233},	{40,17,99},	{2,6,4},	-2},
/*17 */ {{ 3,9,258},	{40,18,99},	{3,4,4},	-3},
/*18 */ {{ 3,9,283},	{40,19,99},	{2,7,4},	-3},
/*19 */ {{ 3,9,308},	{40,20,99},	{2,7,5},	-4},
/*20 */ {{ 3,9,333},	{40,21,99},	{2,8,5},	-4},
/*21 */ {{ 4,10,360},	{40,22,99},	{4,4,5},	-5},
/*22 */ {{ 5,10,400},	{40,23,99},	{4,4,6},	-5},
/*23 */ {{ 5,10,450},	{40,24,99},	{3,6,6},	-6},
/*24 */ {{ 5,10,500}, {40,25,99},	{2,10,6},	-6},
/*25 */ {{ 5,10,550},	{40,26,99},	{2,10,7},	-7},
/*26 */ {{ 5,10,600},	{40,27,99},	{3,7,7},	-7},
/*27 */ {{ 5,10,650},	{40,28,99},	{5,4,7},	-8},
/*28 */ {{ 6,12,703},	{40,29,99},	{2,12,8},	-8},
/*29 */ {{ 6,12,778},	{40,30,99},	{2,12,8},	-9},
/*30 */ {{ 6,12,853},	{40,31,99},	{4,6,8},	-9},
/*31 */ {{ 6,12,928},	{40,32,99},	{4,6,9},	-10},
/*32 */ {{10,10,1000},	{40,33,99},	{6,4,9},	-10},
/*33 */ {{10,10,1100},	{40,34,99},	{6,4,10},	-11},
/*34 */ {{10,10,1200},	{40,35,99},	{4,7,10},	-11},
/*35 */ {{10,10,1300},	{40,36,99},	{4,7,11},	-11},
/*36 */ {{10,10,1400},	{40,37,99},	{3,10,11},	-12},
/*37 */ {{10,10,1500},	{40,38,99},	{3,10,12},	-12},
/*38 */ {{10,10,1600},	{40,39,99},	{5,6,12},	-13},
/*39 */ {{15,10,1700},	{40,40,99},	{5,6,13},	-13},
/*40 */ {{15,10,1850},	{40,41,99},	{4,8,13},	-13},
/*41 */ {{25,10,2000},	{40,42,99},	{4,8,14},	-14},
/*42 */ {{25,10,2250},	{40,43,99},	{3,12,14},	-14},
/*43 */ {{25,10,2500},	{40,44,99},	{3,12,15},	-15},
/*44 */ {{25,10,2750},	{40,45,99},	{8,4,15},	-15},
/*45 */ {{25,10,3000},	{40,46,99},	{8,4,16},	-15},
/*46 */ {{25,10,3250},	{40,47,99},	{6,6,16},	-16},
/*47 */ {{25,10,3500},	{40,48,99},	{6,6,17},	-17},
/*48 */ {{25,10,3750},	{40,49,99},	{6,6,18},	-18},
/*49 */ {{50,10,4000},	{40,50,99},	{4,10,18},	-19},
/*50 */ {{50,10,4500},	{40,51,99},	{5,8,19},	-20},
/*51 */ {{50,10,5000},	{40,52,99},	{5,8,20},	-21},
/*52 */ {{50,10,5500},	{40,53,99},	{6,7,20},	-22},
/*53 */ {{50,10,6000},	{40,54,99},	{6,7,21},	-23},
/*54 */ {{50,10,6500},	{40,55,99},	{7,6,22},	-24},
/*55 */ {{50,10,7000},	{40,56,99},	{10,4,23},	-25},
/*56 */ {{50,10,7500},	{40,57,99},	{10,4,24},	-26},
/*57 */ {{50,10,8000},	{40,58,99},	{6,8,24},	-27},
/*58 */ {{50,10,8500},	{40,59,99},	{5,10,25},	-28},
/*59 */ {{50,10,9000},	{40,60,99},	{8,6,26},	-29},
/*60 */ {{50,10,9500},	{40,61,99},	{8,6,28},	-30},
/*61 */ {{50,10,10000},	{40,62,99},	{8,6,29},	-31},
/*62 */ {{50,10,10100},	{40,63,99},	{10,5,29},	-32},
/*63 */ {{50,10,10200},	{40,64,99},	{10,5,30},	-33},
/*64 */ {{50,10,10300},	{40,65,99},	{10,5,31},	-33},
/*65 */ {{50,10,10400},	{40,66,99},	{10,5,32},	-34},
/*66 */ {{50,10,10500}, {40,67,99},	{10,5,33},	-34},
/*67 */ {{50,10,10600},	{40,68,99},	{10,5,34},	-35},
/*68 */ {{50,10,10700},	{40,69,99},	{10,5,35},	-35},
/*69 */ {{50,10,10800},	{40,70,99},	{10,5,36},	-36},
/*70 */ {{50,10,10900},	{40,71,99},	{10,5,37},	-36},
/*71 */ {{50,10,11000},	{40,72,99},	{9,6,38},	-37},
/*72 */ {{50,10,11100},	{40,73,99},	{9,6,39},	-37},
/*73 */ {{50,10,11200},	{40,74,99},	{9,6,40},	-38},
/*74 */ {{50,10,11300},	{40,75,99},	{9,6,41},	-38},
/*75 */ {{50,10,11400},	{40,76,99},	{9,6,42},	-39},
/*76 */ {{50,10,11500},	{40,77,99},	{7,8,41},	-39},
/*77 */ {{50,10,11600},	{40,78,99},	{7,8,42},	-40},
/*78 */ {{50,10,11700},	{40,79,99},	{7,8,43},	-40},
/*79 */ {{50,10,11800},	{40,80,99},	{6,10,43},	-41},
/*80 */ {{50,10,11900},	{40,81,99},	{6,10,44},	-41},
/*81 */ {{50,10,12000},	{40,82,99},	{6,10,45},	-42},
/*82 */ {{50,10,12100},	{40,83,99},	{6,10,46},	-42},
/*83 */ {{50,10,12200},	{40,84,99},	{6,10,47},	-43},
/*84 */ {{50,10,12300},	{40,85,99},	{6,10,48},	-43},
/*85 */ {{50,10,12400},	{40,86,99},	{6,10,49},	-44},
/*86 */ {{50,10,12500},	{40,87,99},	{8,8,50},	-44},
/*87 */ {{50,10,12600},	{40,88,99},	{8,8,51},	-45},
/*88 */ {{50,10,12700},	{40,89,99},	{8,8,52},	-45},
/*89 */ {{50,10,12800},	{40,90,99},	{8,8,53},	-46},
/*90 */ {{50,10,12900}, {40,91,99},	{8,8,54},	-47}
};

struct olc_help_type {
    char *		command;
    const void *	structure;
    const TABLE_TYPE *	table;
    const OPTION_TYPE *	options;
    char *		desc;
};



bool show_version( CHAR_DATA *ch, char *argument )
{
    send_to_char( VERSION, ch );
    send_to_char( "\n\r", ch );
    send_to_char( AUTHOR, ch );
    send_to_char( "\n\r", ch );
    send_to_char( DATE, ch );
    send_to_char( "\n\r", ch );
    send_to_char( CREDITS, ch );
    send_to_char( "\n\r", ch );

    return FALSE;
}

/*
 * This table contains help commands and a brief description of each.
 * ------------------------------------------------------------------
 */

bool dummy_skill_table[1];

const struct olc_help_type help_table[] =
{
    {	"act",		NULL,NULL,act_options,		"Mobile attributes."	},
    {	"effect",	NULL,NULL,effect_options,	"Mobile effects."	},
    {	"effect2",	NULL,NULL,effect2_options,	"More mobile effects."	},
    {	"area",		NULL,NULL,area_options,		"Area attributes."	},
    {	"container",	NULL,NULL,container_options,	"Container status."	},
    {	"damage",	NULL,damagetype_table,NULL,	"Damage type." 		},
    {	"exit",		NULL,NULL,exit_options,		"Exit types."		},
    {	"extra",	NULL,NULL,extra_options,	"Object attributes."	},
    {	"extra2",	NULL,NULL,extra2_options,	"More object attributes."},
    {	"form",		NULL,NULL,form_options,		"Mobile form flags."	},
    {	"furniture",	NULL,NULL,furniture_options,	"Funiture flags."	},
    {	"gate",		NULL,NULL,gate_options,		"Gate flags."		},
    {	"immresvuln",	NULL,NULL,imm_options,		"Mobile imm/res/vuln flags."  },
    {	"liquid",	NULL,liquid_table,NULL,		"Types of liquids."	},
    {	"location",	NULL,NULL,apply_options,	"Object location apply types" },
    {	"offensive",	NULL,NULL,off_options,		"Mobile offencive flags."},
    {	"parts",	NULL,NULL,part_options,		"Mobile parts flags."	},
    {	"positions",	position_table,NULL,NULL,	"Mobile positions"	},
    {	"race",		race_table,NULL,NULL,		"Available races"	},
    {	"room",		NULL,NULL,room_options,		"Room attributes."	},
    {	"sector",	NULL,sector_table,NULL,		"Sector types, terrain."},
    {	"sex",		NULL,sex_table,NULL,		"Sexes."		},
    {	"size",		size_table,NULL,NULL,		"Mobile sizes."		},
    {	"spec",		spec_table,NULL,NULL,		"Available special programs." },
    {	"spells",	dummy_skill_table,NULL,NULL,	"Names of current spells."},
    {	"type",		NULL,NULL,itemtype_options,	"Types of objects."	},
    {	"weapon",	weapon_table,NULL,NULL,		"Weapon types."		},
    {	"weaponflags",	NULL,NULL,weaponflag_options,	"Weapon flags."		},
    {	"wear",		NULL,NULL,wear_options,		"Where to wear object." },
    {	"wear-loc",	NULL,NULL,wear_loc_options,	"Where mobile wears object."},
    {	NULL,		NULL,NULL,NULL,			NULL			}
};


/*****************************************************************************
 Name:		show_flag_cmds
 Purpose:	Displays settable flags and stats.
 Called by:	show_olc_help(olc_act.c).
 ****************************************************************************/
void show_options(CHAR_DATA *ch,const OPTION_TYPE *options) {
    char	buf[MAX_STRING_LENGTH];
    char	buf1[MAX_STRING_LENGTH];
    char 	*s1,*s2;
    int		flag;
    int		col;
    int		wide=0;

    for (flag=0;options[flag].name!=NULL;flag++) {
	if (options[flag].help) {
	    wide=1;
	    break;
	}
    }
    buf1[0]=0;
    col=0;

    if (wide) {
	for (flag=0;options[flag].name!=NULL;flag++) {
	    if (options[flag].settable) {
		sprintf(buf,"%-19.18s",options[flag].name);
		strcat(buf1,buf);
		if (options[flag].help) {
		    strncpy(buf,options[flag].help,MAX_STRING_LENGTH);
		    s1=buf;
		    while ((s2=strsep(&s1,"\n"))) {
			if (s2!=buf) strcat(buf1,"                   ");
			strcat(buf1,s2);
			strcat(buf1,"\n\r");
		    }
		} else 
		    strcat(buf1,"\n\r");
	    }
	}
    } else {
	strcat(buf1,"\n\r");
	for (flag=0;options[flag].name!=NULL;flag++) {
	    if (options[flag].settable) {
		sprintf(buf,"%-19.18s",options[flag].name);
		strcat(buf1,buf);
		if (++col%4==0)
		    strcat(buf1,"\n\r");
	    }
	}

	if (col%4!=0)
	    strcat(buf1,"\n\r");
    }

    send_to_char(buf1,ch);
}

void show_table(CHAR_DATA *ch,const TABLE_TYPE *table) {
    char	buf[MAX_STRING_LENGTH];
    char	buf1[MAX_STRING_LENGTH];
    int		flag;
    int		col;

    buf1[0]=0;
    col=0;
    for (flag=0;table[flag].name!=NULL;flag++) {
	sprintf(buf,"%-19.18s",table[flag].name);
	strcat(buf1,buf);
	if (++col%4==0)
	    strcat(buf1,"\n\r");
    }

    if (col%4!=0)
	strcat(buf1,"\n\r");

    send_to_char(buf1,ch);
}



/*****************************************************************************
 Name:		show_skill_cmds
 Purpose:	Displays all skill functions.
 		Does remove those damn immortal commands from the list.
 		Could be improved by:
 		(1) Adding a check for a particular class.
 		(2) Adding a check for a level range.
 Called by:	show_olc_help(olc_act.c).
 ****************************************************************************/
void show_skill_cmds( CHAR_DATA *ch, int tar )
{
    char buf  [ MAX_STRING_LENGTH ];
    char buf1 [ MAX_STRING_LENGTH*2 ];
    int  sn;
    int  col;

    buf1[0] = '\0';
    col = 0;
    for (sn = 0; sn < MAX_SKILL; sn++)
    {
	if ( !skill_exists(sn) )
	    break;

	if ( !str_cmp( skill_name(sn,ch), "reserved" )
	  || skill_spell_fun(sn,ch) == spell_null
	  || !skill_implemented(sn,ch))
	    continue;

	if ( tar == -1 || skill_target(sn,ch) == tar )
	{
	    sprintf( buf, "%-19.18s", skill_name(sn,ch) );
	    strcat( buf1, buf );
	    if ( ++col % 4 == 0 )
		strcat( buf1, "\n\r" );
	}
    }

    if ( col % 4 != 0 )
	strcat( buf1, "\n\r" );

    send_to_char( buf1, ch );
    return;
}



/*****************************************************************************
 Name:		show_spec_cmds
 Purpose:	Displays settable special functions.
 Called by:	show_olc_help(olc_act.c).
 ****************************************************************************/
void show_spec_cmds( CHAR_DATA *ch )
{
    char buf  [ MAX_STRING_LENGTH ];
    char buf1 [ MAX_STRING_LENGTH ];
    int  spec;
    int  col;

    buf1[0] = '\0';
    col = 0;
    send_to_char( "Preceed special functions with 'spec_'\n\r\n\r", ch );
    for (spec = 0; (*spec_table[spec].function); spec++)
    {
	sprintf( buf, "%-19.18s", &spec_table[spec].name[5] );
	strcat( buf1, buf );
	if ( ++col % 4 == 0 )
	    strcat( buf1, "\n\r" );
    }

    if ( col % 4 != 0 )
	strcat( buf1, "\n\r" );

    send_to_char( buf1, ch );
    return;
}

void show_weapon_table( CHAR_DATA *ch )
{
    char buf  [ MAX_STRING_LENGTH ];
    char buf1 [ MAX_STRING_LENGTH ];
    int  i;
    int  col;

    buf1[0] = '\0';
    col = 0;
    for (i = 0; weapon_table[i].name; i++)
    {
	sprintf( buf, "%-19.18s", weapon_table[i].name );
	strcat( buf1, buf );
	if ( ++col % 4 == 0 )
	    strcat( buf1, "\n\r" );
    }

    if ( col % 4 != 0 )
	strcat( buf1, "\n\r" );

    send_to_char( buf1, ch );
    return;
}

void show_race_table( CHAR_DATA *ch )
{
    char buf  [ MAX_STRING_LENGTH ];
    char buf1 [ MAX_STRING_LENGTH ];
    int  i;
    int  col;

    buf1[0] = '\0';
    col = 0;
    for (i = 0; race_table[i].name; i++)
    {
	sprintf( buf, "%-19.18s", race_table[i].name );
	strcat( buf1, buf );
	if ( ++col % 4 == 0 )
	    strcat( buf1, "\n\r" );
    }

    if ( col % 4 != 0 )
	strcat( buf1, "\n\r" );

    send_to_char( buf1, ch );
    return;
}

void show_position_table( CHAR_DATA *ch )
{
    char buf  [ MAX_STRING_LENGTH ];
    char buf1 [ MAX_STRING_LENGTH ];
    int  i;
    int  col;

    buf1[0] = '\0';
    col = 0;
    for (i = 0; position_table[i].name; i++)
    {
	sprintf( buf, "%-19.18s", position_table[i].name );
	strcat( buf1, buf );
	if ( ++col % 4 == 0 )
	    strcat( buf1, "\n\r" );
    }

    if ( col % 4 != 0 )
	strcat( buf1, "\n\r" );

    send_to_char( buf1, ch );
    return;
}

void show_size_table( CHAR_DATA *ch )
{
    char buf  [ MAX_STRING_LENGTH ];
    char buf1 [ MAX_STRING_LENGTH ];
    int  i;
    int  col;

    buf1[0] = '\0';
    col = 0;
    for (i = 0; size_table[i].name; i++)
    {
	sprintf( buf, "%-19.18s", size_table[i].name );
	strcat( buf1, buf );
	if ( ++col % 4 == 0 )
	    strcat( buf1, "\n\r" );
    }

    if ( col % 4 != 0 )
	strcat( buf1, "\n\r" );

    send_to_char( buf1, ch );
    return;
}



/*****************************************************************************
 Name:		show_olc_help
 Purpose:	Displays help for many tables used in OLC.
 Called by:	olc interpreters.
 ****************************************************************************/
bool show_olc_help( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    char spell[MAX_INPUT_LENGTH];
    int cnt;

    argument = one_argument( argument, arg );
    one_argument( argument, spell );

    //
    // Display syntax.
    //
    if (arg[0]==0) {
	send_to_char( "Syntax:  ? [command]\n\r\n\r", ch );
	send_to_char( "[command]  [description]\n\r", ch );
	for (cnt=0;help_table[cnt].command!=NULL;cnt++) {
	    sprintf_to_char( ch, "%-10.10s -%s\n\r",
	        capitalize(help_table[cnt].command),
		help_table[cnt].desc );
	}
	return FALSE;
    }

    /*
     * Find the command, show changeable data.
     * ---------------------------------------
     */
    for (cnt = 0;help_table[cnt].command!=NULL; cnt++)
    {
        if (arg[0] == help_table[cnt].command[0]
          && !str_prefix( arg, help_table[cnt].command ) ) {

	    //
	    // tables
	    //
	    if ( help_table[cnt].table!=NULL) {
		show_table(ch,help_table[cnt].table);
		return FALSE;
	    };

	    //
	    // options
	    //
	    if ( help_table[cnt].options!=NULL) {
		show_options(ch,help_table[cnt].options);
		return FALSE;
	    };

	    if ( help_table[cnt].structure == spec_table ) {
		show_spec_cmds( ch );
		return FALSE;
	    }
	    if ( help_table[cnt].structure == dummy_skill_table ) {

		if ( spell[0] == '\0' )
		{
		    send_to_char( "Syntax:  ? spells "
		        "[ignore/attack/defend/self/object/all]\n\r", ch );
		    return FALSE;
		}

		if ( !str_prefix( spell, "all" ) )
		    show_skill_cmds( ch, -1 );
		else if ( !str_prefix( spell, "ignore" ) )
		    show_skill_cmds( ch, TAR_IGNORE );
		else if ( !str_prefix( spell, "attack" ) )
		    show_skill_cmds( ch, TAR_CHAR_OFFENSIVE );
		else if ( !str_prefix( spell, "defend" ) )
		    show_skill_cmds( ch, TAR_CHAR_DEFENSIVE );
		else if ( !str_prefix( spell, "self" ) )
		    show_skill_cmds( ch, TAR_CHAR_SELF );
		else if ( !str_prefix( spell, "object" ) )
		    show_skill_cmds( ch, TAR_OBJ_INV );
		else
		    send_to_char( "Syntax:  ? spell "
		        "[ignore/attack/defend/self/object/all]\n\r", ch );

		return FALSE;
	    }
	    if (help_table[cnt].structure==weapon_table) {
		show_weapon_table( ch );
		return FALSE;
	    }
	    if (help_table[cnt].structure==race_table) {
		show_race_table( ch );
		return FALSE;
	    }
	    if (help_table[cnt].structure==position_table) {
		show_position_table( ch );
		return FALSE;
	    }
	    if (help_table[cnt].structure==size_table) {
		show_size_table( ch );
		return FALSE;
	    }
	}
    }

    show_olc_help( ch, "" );
    return FALSE;
}

