/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

#include "merc.h"

//
// table functions
//
int table_find_name(const char *name,const TABLE_TYPE *table) {
    int	i;

    for (i=0;table[i].name!=NULL;i++)
	if (str_cmp(name,table[i].name)==0)
	    return table[i].value;
    return NO_FLAG;
}

char *table_find_value(int value,const TABLE_TYPE *table) {
    int	i;

    for (i=0;table[i].name!=NULL;i++)
	if (table[i].value==value)
	    return table[i].name;
    return "(unknown)";
}

char *table_string(const char *bits,const TABLE_TYPE *table) {
    static char	s[MSL];
    int		i;

    s[0]=0;
    for (i=0;table[i].name!=NULL;i++)
	if (STR_IS_SET(bits,table[i].value)) {
	    if (s[0]!=0)
		strcat(s," ");
	    strcat(s,table[i].name);
	}
    return s;
}

char *table_string_from_long(long bits,const TABLE_TYPE *table) {
    static char	s[MSL];
    int		i;

    s[0]=0;
    for (i=0;table[i].name!=NULL;i++)
	if (IS_SET(bits,table[i].value)) {
	    if (s[0]!=0)
		strcat(s," ");
	    strcat(s,table[i].name);
	}
    return s;
}

char *table_all(const TABLE_TYPE *table) {
    static char	s[MSL];
    int		i;

    s[0]=0;
    for (i=0;table[i].name!=NULL;i++) {
	if (s[0]!=0)
	    strcat(s," ");
	strcat(s,table[i].name);
    }
    return s;
}

//
// options functions
//
int option_find_name(const char *name,const OPTION_TYPE *options, bool settable) {
    int	i;

    for (i=0;options[i].name!=NULL;i++)
	if (str_cmp(name,options[i].name)==0 && 
	    (options[i].settable || !settable) )
	    return options[i].value;
    return NO_FLAG;
}

char *option_find_value(int value,const OPTION_TYPE *options) {
    int	i;

    for (i=0;options[i].name!=NULL;i++)
	if (options[i].value==value)
	    return options[i].name;
    return "(unknown)";
}

char *option_string(const char *bits,const OPTION_TYPE *options) {
    static char	s[MSL];
    int		i;

    s[0]=0;
    for (i=0;options[i].name!=NULL;i++)
	if (STR_IS_SET(bits,options[i].value)) {
	    if (s[0]!=0)
		strcat(s," ");
	    strcat(s,options[i].name);
	}
    return s;
}

char *option_string_from_long(long bits,const OPTION_TYPE *options) {
    static char	s[MSL];
    int		i;

    s[0]=0;
    for (i=0;options[i].name!=NULL;i++)
	if (IS_SET(bits,options[i].value)) {
	    if (s[0]!=0)
		strcat(s," ");
	    strcat(s,options[i].name);
	}
    return s;
}

char *option_all(const OPTION_TYPE *options) {
    static char	s[MSL];
    int		i;

    s[0]=0;
    for (i=0;options[i].name!=NULL;i++) 
	if (options[i].settable) {
	    if (s[0]!=0)
		strcat(s," ");
	    strcat(s,options[i].name);
	}
    return s;
}

//
// smart functions for lookups
//
char *item_type(OBJ_DATA *item) {
    return table_find_value(item->item_type,item_table);
}

char *reboot_state(void) {
    return table_find_value(mud_data.reboot_type,reboot_table);
}

char *effect_loc_name(EFFECT_DATA *effect) {
    return option_find_value(effect->location,apply_options);
}

char *room_sector_type(ROOM_INDEX_DATA *room) {
    return table_find_value(room->sector_type,sector_table);
}

char *room_flags(ROOM_INDEX_DATA *room) {
    return option_string(room->strbit_room_flags,room_options);
}

char *eff_bit_name(EFFECT_DATA *effect) {
    switch (effect->where) {
	case TO_EFFECTS : return option_find_value(effect->bitvector,effect_options);
	case TO_OBJECT  : return option_find_value(effect->bitvector,extra_options);
	case TO_IMMUNE  : return option_find_value(effect->bitvector,imm_options);
	case TO_RESIST  : return option_find_value(effect->bitvector,res_options);
	case TO_VULN    : return option_find_value(effect->bitvector,vuln_options);
	case TO_WEAPON  : return option_find_value(effect->bitvector,weaponflag_options);
	case TO_EFFECTS2: return option_find_value(effect->bitvector,effect2_options);
	case TO_OBJECT2 : return option_find_value(effect->bitvector,extra2_options);
	case TO_ROOM1   : return option_find_value(effect->bitvector,room_options);
	case TO_EXIT1   : return option_find_value(effect->bitvector,exit_options);
	case TO_SKILLS	: return skill_name(effect->bitvector,NULL);
    }
    return "?unknown?";
}

char *effect_bit_name(EFFECT_DATA *effect) {
    return option_find_value(effect->bitvector,effect_options);
}

char *effect2_bit_name(EFFECT_DATA *effect) {
    return option_find_value(effect->bitvector,effect2_options);
}

char *obj_extra_string(OBJ_DATA *obj) {
    return option_string(obj->strbit_extra_flags,extra_options);
}

char *obj_wear_string(OBJ_DATA *obj) {
    return option_string_from_long(obj->wear_flags,wear_options);
}

char *obj_weaponflag_string(OBJ_DATA *obj) {
    return option_string_from_long(obj->value[4],weaponflag_options);
}

char *obj_container_string(OBJ_DATA *obj) {
    return option_string_from_long(obj->value[1],container_options);
}

char *char_act_string(CHAR_DATA *ch) {
    return option_string(ch->strbit_act,act_options);
}

char *char_plr_string(CHAR_DATA *ch) {
    return option_string(ch->strbit_act,plr_options);
}

char *char_comm_string(CHAR_DATA *ch) {
    return option_string(ch->strbit_comm,comm_options);
}

char *char_off_string(CHAR_DATA *ch) {
    return option_string(ch->strbit_off_flags,off_options);
}

char *char_imm_string(CHAR_DATA *ch) {
    return option_string(ch->strbit_imm_flags,imm_options);
}

char *char_res_string(CHAR_DATA *ch) {
    return option_string(ch->strbit_res_flags,res_options);
}

char *char_vuln_string(CHAR_DATA *ch) {
    return option_string(ch->strbit_vuln_flags,vuln_options);
}

char *char_form_string(CHAR_DATA *ch) {
    return option_string_from_long(ch->form,form_options);
}

char *char_part_string(CHAR_DATA *ch) {
    return option_string_from_long(ch->parts,part_options);
}

char *char_affected_string(CHAR_DATA *ch) {
    return option_string(ch->strbit_affected_by,effect_options);
}

char *char_affected2_string(CHAR_DATA *ch) {
    return option_string(ch->strbit_affected_by2,effect2_options);
}

char *area_flags(AREA_DATA *area) {
    return option_string_from_long(area->area_flags,area_options);
}

char *exit_reset_string(EXIT_DATA *exit) {
    return option_string_from_long(exit->rs_flags,exit_options);
}

char *exit_flags(EXIT_DATA *exit) {
    return option_string_from_long(exit->exit_info,exit_options);
}

char *char_damagetype(EXIT_DATA *exit) {
    return option_string_from_long(exit->exit_info,exit_options);
}

//
// OLC related lookups
//
void olc_help_table(CHAR_DATA *ch,char *msg,const TABLE_TYPE *table) {
    if (msg!=NULL)
	sprintf_to_char(ch,"{y%s{x ",msg);
    else
	send_to_char("Try: ",ch);

    sprintf_to_char(ch,"%s\n\r",table_all(table));
}

void olc_help_options(CHAR_DATA *ch,char *msg,const OPTION_TYPE *options) {
    if (msg!=NULL)
	sprintf_to_char(ch,"{y%s{x ",msg);
    else
	send_to_char("Try: ",ch);

    sprintf_to_char(ch,"%s\n\r",option_all(options));
}

///////////////////////////////////////////////////////////////////////////////
//
// old stuff
//

int position_lookup (const char *name) {
   int pos;

   for (pos = 0; position_table[pos].name != NULL; pos++) {
	if (LOWER(name[0]) == LOWER(position_table[pos].name[0])
	&&  !str_prefix(name,position_table[pos].name))
	    return pos;
   }

   return -1;
}

int short_position_lookup (const char *name) {
   int pos;

   for (pos = 0; position_table[pos].name != NULL; pos++) {
	if (LOWER(name[0]) == LOWER(position_table[pos].short_name[0])
	&&  !str_prefix(name,position_table[pos].short_name))
	    return pos;
   }

   return -1;
}

int sex_lookup (const char *name) {
   int sex;

   for (sex = 0; sex_table[sex].name != NULL; sex++) {
	if (LOWER(name[0]) == LOWER(sex_table[sex].name[0])
	&&  !str_prefix(name,sex_table[sex].name))
	    return sex;
   }

   return -1;
}

int size_lookup (const char *name) {
   int size;

   for ( size = 0; size_table[size].name != NULL; size++) {
        if (LOWER(name[0]) == LOWER(size_table[size].name[0])
        &&  !str_prefix( name,size_table[size].name))
            return size;
   }

   return -1;
}

/* returns material number */
int material_lookup (const char *name) {
    return 0;
}

/* returns race number */
int race_lookup (const char *name) {
   int race;

   for ( race = 0; race_table[race].name != NULL; race++) {
	if (LOWER(name[0]) == LOWER(race_table[race].name[0])
	&&  !str_prefix( name,race_table[race].name))
	    return race;
   }

   //
   // rodent is a much better name than a rabbit.
   //
   if (str_cmp(name,"rabbit")==0) return race_lookup("rodent");

   return 0;
}

int liq_lookup (const char *name) {
    int liq;

    for ( liq = 0; liq_table[liq].liq_name != NULL; liq++) {
	if (LOWER(name[0]) == LOWER(liq_table[liq].liq_name[0])
	&& !str_prefix(name,liq_table[liq].liq_name))
	    return liq;
    }

    return -1;
}

int weapon_lookup (const char *name) {
    int type;

    for (type = 0; weapon_table[type].name != NULL; type++) {
	if (LOWER(name[0]) == LOWER(weapon_table[type].name[0])
	&&  !str_prefix(name,weapon_table[type].name))
	    return type;
    }

    return -1;
}

int weapon_type (const char *name) {
    int type;

    for (type = 0; weapon_table[type].name != NULL; type++) {
        if (LOWER(name[0]) == LOWER(weapon_table[type].name[0])
        &&  !str_prefix(name,weapon_table[type].name))
            return weapon_table[type].type;
    }

    return WEAPON_EXOTIC;
}

char *weapon_name( int weapon_type) {
    int type;

    for (type = 0; weapon_table[type].name != NULL; type++)
        if (weapon_type == weapon_table[type].type)
            return weapon_table[type].name;
    return "exotic";
}

int attack_lookup  (const char *name) {
    int att;

    for ( att = 0; attack_table[att].name != NULL; att++) {
	if (LOWER(name[0]) == LOWER(attack_table[att].name[0])
	&&  !str_prefix(name,attack_table[att].name))
	    return att;
    }

    return 0;
}

/* returns a flag for wiznet */
// WARNING! don't misinterpet this. it returns the row number of the array.
long wiznet_lookup (const char *name) {
    int flag;

    for (flag = 0; wiznet_table[flag].name != NULL; flag++) {
	if (LOWER(name[0]) == LOWER(wiznet_table[flag].name[0])
	&& !str_prefix(name,wiznet_table[flag].name))
	    return flag;
    }

    return -1;
}
char *wiznet_name_lookup (int flag) {
    /*
     * return wiznet_table[flag].name;
     *
     * somehow we have to protect against flag being larger than the table
     */
  int i;
  for (i=0;wiznet_table[i].name!=NULL && (flag!=i);i++);
  return wiznet_table[i].name;
}

/* returns class number */
int class_lookup (const char *name) {
   int class;

   for ( class = 0; class < MAX_CLASS; class++) {
        if (LOWER(name[0]) == LOWER(class_table[class].name[0])
        &&  !str_prefix( name,class_table[class].name))
            return class;
   }

   return -1;
}
