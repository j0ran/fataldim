//
// $Id: fd_objprog.h,v 1.42 2008/02/22 19:51:19 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef INCLUDED_FD_OBJPROG_H
#define INCLUDED_FD_OBJPROG_H

int	op_error		(OBJ_DATA *obj);

bool	op_preclose_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
void	op_close_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
bool	op_predrink_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
void	op_drink_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
bool	op_prequaff_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
void	op_quaff_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
bool	op_prerecite_trigger	(OBJ_DATA *obj,CHAR_DATA *ch, OBJ_DATA *otarget, CHAR_DATA *ctarget);
void	op_recite_trigger	(OBJ_DATA *obj,CHAR_DATA *ch, OBJ_DATA *otarget, CHAR_DATA *ctarget);
bool	op_pregive_trigger	(OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *victim);
void	op_give_trigger		(OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *victim);
bool	op_preput_trigger	(OBJ_DATA *obj,CHAR_DATA *ch,OBJ_DATA *container);
void	op_put_trigger		(OBJ_DATA *obj,CHAR_DATA *ch,OBJ_DATA *container);
bool	op_preputin_trigger	(OBJ_DATA *container,CHAR_DATA *ch,OBJ_DATA *obj);
void	op_putin_trigger	(OBJ_DATA *container,CHAR_DATA *ch,OBJ_DATA *obj);
bool	op_preget_trigger	(OBJ_DATA *obj,CHAR_DATA *ch,OBJ_DATA *container);
void	op_get_trigger		(OBJ_DATA *obj,CHAR_DATA *ch,OBJ_DATA *container);
bool	op_pregetout_trigger	(OBJ_DATA *container,CHAR_DATA *ch,OBJ_DATA *obj);
void	op_getout_trigger	(OBJ_DATA *container,CHAR_DATA *ch,OBJ_DATA *obj);
bool	op_predrop_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
void	op_drop_trigger		(OBJ_DATA *obj,CHAR_DATA *ch);
bool	op_preeat_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
void	op_eat_trigger		(OBJ_DATA *obj,CHAR_DATA *ch);
bool	op_presit_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
void	op_sit_trigger		(OBJ_DATA *obj,CHAR_DATA *ch);
bool	op_prestand_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
void	op_stand_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
bool	op_prerest_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
void	op_rest_trigger		(OBJ_DATA *obj,CHAR_DATA *ch);
bool	op_presleep_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
void	op_sleep_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
bool	op_pretrap_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
void	op_trap_trigger		(OBJ_DATA *obj,CHAR_DATA *ch);
bool	op_canlock_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
bool	op_prelock_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
void	op_lock_trigger		(OBJ_DATA *obj,CHAR_DATA *ch);
bool	op_prelookat_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
void	op_lookat_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
bool	op_prelookat_ed_trigger	(OBJ_DATA *obj,CHAR_DATA *ch,char *txt);
void	op_lookat_ed_trigger	(OBJ_DATA *obj,CHAR_DATA *ch,char *txt);
bool	op_prelookin_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
void	op_lookin_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
bool	op_preuse_trigger	(OBJ_DATA *obj,CHAR_DATA *ch,char *txt);
void	op_use_trigger		(OBJ_DATA *obj,CHAR_DATA *ch,char *txt);
bool	op_preopen_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
void	op_open_trigger		(OBJ_DATA *obj,CHAR_DATA *ch);
bool	op_preenter_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
void	op_enter_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
bool	op_preremove_trigger	(OBJ_DATA *obj,CHAR_DATA *ch,int loc);
void	op_remove_trigger	(OBJ_DATA *obj,CHAR_DATA *ch,int loc);
bool	op_canunlock_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
bool	op_preunlock_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
void	op_unlock_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
bool	op_prewear_trigger	(OBJ_DATA *obj,CHAR_DATA *ch,int loc);
void	op_wear_trigger		(OBJ_DATA *obj,CHAR_DATA *ch,int loc);
bool	op_presell_trigger	(OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *keeper);
void	op_sell_trigger		(OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *keeper);
bool	op_prezap_trigger	(OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *victim,OBJ_DATA *target);
void	op_zap_trigger		(OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *victim,OBJ_DATA *target);
bool	op_prehit_trigger	(OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *victim);
void	op_hit_trigger		(OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *victim);
void	op_nohit_trigger	(OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *victim, char *buf);
bool	op_prekey_trigger	(OBJ_DATA *obj,CHAR_DATA *ch,OBJ_DATA *obj2);
void	op_key_trigger		(OBJ_DATA *obj,CHAR_DATA *ch,OBJ_DATA *obj2);
bool	op_prebrandish_trigger	(OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *victim,OBJ_DATA *obj2);
void	op_brandish_trigger	(OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *victim,OBJ_DATA *obj2);
bool	op_playfilter_trigger	(OBJ_DATA *obj,CHAR_DATA *ch,char *song);
bool	op_preplay_trigger	(OBJ_DATA *obj,CHAR_DATA *ch,char *song);
void	op_play_trigger		(OBJ_DATA *obj,CHAR_DATA *ch,char *song);
bool	op_interpret_unknown	(CHAR_DATA *ch,char *string);
bool	op_interpret_preknown	(CHAR_DATA *ch,char *string);
void	op_interpret_postknown	(CHAR_DATA *ch,char *string);
bool	op_preread_trigger	(OBJ_DATA *obj,CHAR_DATA *ch);
void	op_read_trigger		(OBJ_DATA *obj,CHAR_DATA *ch);
void	op_buy_trigger		(OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *shopkeeper);

void	op_delay_trigger	(OBJ_DATA *obj);
void	op_hour_trigger		(OBJ_DATA *obj,int hour);
void	op_timer_trigger	(OBJ_DATA *obj);
void	op_random_trigger	(OBJ_DATA *obj);

bool	op_prespeech_trigger	(CHAR_DATA *ch,char *words);
void	op_speech_trigger	(CHAR_DATA *ch,char *words);

bool	op_preattack_trigger	(CHAR_DATA *ch,CHAR_DATA *victim, char *means);

void	op_load_trigger		(OBJ_DATA *obj,CHAR_DATA *loader);

void	op_sunset_trigger	(void);
void	op_sunrise_trigger	(void);
void	op_dayend_trigger	(void);
void	op_daystart_trigger	(void);

void    op_weather_trigger      (int old_state, int new_state);

#endif	// INCLUDED_FD_OBJPROG_H
