//
// $Id: fd_tcl_area.c,v 1.22 2009/01/05 19:38:33 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#define NEEDS_TCL
#include "merc.h"
#include "olc.h"

#define TCL_AREA_CMD(name) static int name(Tcl_Interp *interp,AREA_DATA *area,int objc,Tcl_Obj *CONST objv[])

TCL_AREA_CMD(AreaCmdNotImpl);
TCL_AREA_CMD(area_name);
TCL_AREA_CMD(area_vnum);
TCL_AREA_CMD(area_lvnum);
TCL_AREA_CMD(area_uvnum);
TCL_AREA_CMD(area_llevel);
TCL_AREA_CMD(area_ulevel);
TCL_AREA_CMD(area_recall);
TCL_AREA_CMD(area_creator);
TCL_AREA_CMD(area_count);
TCL_AREA_CMD(area_playersaround);
TCL_AREA_CMD(area_echo);
TCL_AREA_CMD(area_allchar);
TCL_AREA_CMD(area_allmob);
TCL_AREA_CMD(area_timer);
TCL_AREA_CMD(area_delay);
TCL_AREA_CMD(area_addtrigger);
TCL_AREA_CMD(area_deltrigger);
TCL_AREA_CMD(area_triggers);
TCL_AREA_CMD(area_namespace);
TCL_AREA_CMD(area_variable);
TCL_AREA_CMD(area_flag);

struct tcl_Area_Command_Struct {
    char *cmd;
    int  (*func)(Tcl_Interp *,AREA_DATA *,int,Tcl_Obj *CONST []);
};

static struct tcl_Area_Command_Struct tclAreaCommands[] = {
    { "name",		area_name		},
    { "vnum",		area_vnum		},
    { "lvnum",		area_lvnum		},
    { "uvnum",		area_uvnum		},
    { "llevel",		area_llevel		},
    { "ulevel",		area_ulevel		},
    { "creator",	area_creator		},
    { "recall",		area_recall		},
    { "namespace",	area_namespace		},
    { "variable",	area_variable		},
    { "flag",		area_flag		},

    { "delay",		area_delay		},
    { "timer",		area_timer		},

    { "playersaround",	area_playersaround	},
    { "count",		area_count		},

    { "echo",		area_echo		},

    { "addtrigger",	area_addtrigger		},
    { "deltrigger",	area_deltrigger		},
    { "triggers",	area_triggers		},

    { "allchar",	area_allchar		},
    { "allmob",		area_allmob		},
    { NULL,		AreaCmdNotImpl		}
};

int tclAreaObjCmd (client, interp, objc, objv)
    ClientData client;			/* Not used. */
    Tcl_Interp *interp;			/* Current interpreter. */
    int objc;				/* Number of arguments. */
    Tcl_Obj *CONST objv[];		/* Argument values. */
{
    AREA_DATA *area;
    int index;

    // we know objv[0] is 'area'
    objc--; objv++;

    switch (progType) {
    default:
    case PROGTYPE_NONE:
    case PROGTYPE_CHAR:
	if (progData->mob->in_room) {
	    area=progData->mob->in_room->area;
	} else {
	    area=get_vnum_area(progData->mob->pIndexData->vnum);
	}
	if ((area=tcl_parse_area_switches(area,&objc,&objv))==NULL)
	    return TCL_ERROR;
	break;
    case PROGTYPE_OBJECT: {
	OBJ_DATA *container=progData->obj;
	while (container->in_obj) container=container->in_obj;

	if (container->in_room)
	  area=container->in_room->area;
	else if ((container->carried_by) && (container->carried_by->in_room))
	  area=container->carried_by->in_room->area;
	else {
	    area=get_vnum_area(progData->mob->pIndexData->vnum);
	}
	if ((area=tcl_parse_area_switches(area,&objc,&objv))==NULL)
	    return TCL_ERROR;
	break; }
    case PROGTYPE_ROOM:
	if ((area=tcl_parse_area_switches(progData->room->area,&objc,&objv))==NULL)
	    return TCL_ERROR;
	break;
    case PROGTYPE_AREA:
	if ((area=tcl_parse_area_switches(progData->area,&objc,&objv))==NULL)
	    return TCL_ERROR;
	break;
    }

    if (objc==0) {
	return area_name(interp,area,objc,objv);
    }

    if (Tcl_GetIndexFromObjStruct(interp, objv[0], 
				  (char **)tclAreaCommands, sizeof(struct tcl_Area_Command_Struct), 
				  "area command", 0, &index) != TCL_OK) {
	return TCL_ERROR;
    }

    return (tclAreaCommands[index].func)(interp,area,objc,objv);
}

TCL_AREA_CMD(AreaCmdNotImpl) {
    Tcl_Obj *result;

    result=Tcl_NewStringObj("Area command not implemented yet: ",-1);
    Tcl_AppendObjToObj(result,objv[0]);
    Tcl_SetObjResult(interp,result);

    return TCL_ERROR;
}

TCL_AREA_CMD(area_name) {
    Tcl_SetObjResult(interp,Tcl_NewStringObj(area->name,-1));
    return TCL_OK;
}

// vnum [areaname]
TCL_AREA_CMD(area_vnum) {
    Tcl_Obj *result;
    if (objc<=1) {                   
        result=Tcl_NewIntObj(area->vnum);
    } else {
        AREA_DATA *pArea;

        for (pArea=area_first;pArea!=NULL;pArea=pArea->next)
            if (!str_cmp(pArea->name,Tcl_GetString(objv[1])))
                break;

        if (pArea==NULL)
            result=Tcl_NewIntObj(-1);
        else
            result=Tcl_NewIntObj(pArea->vnum);
    }
    Tcl_SetObjResult(interp,result);
    return TCL_OK;
}

// lvnum
TCL_AREA_CMD(area_lvnum) {
    Tcl_SetObjResult(interp,Tcl_NewIntObj(area->lvnum));
    return TCL_OK;
}

// uvnum
TCL_AREA_CMD(area_uvnum) {
    Tcl_SetObjResult(interp,Tcl_NewIntObj(area->uvnum));
    return TCL_OK;
}

// llevel
TCL_AREA_CMD(area_llevel) {
    Tcl_SetObjResult(interp,Tcl_NewIntObj(area->llevel));
    return TCL_OK;
}

// ulevel
TCL_AREA_CMD(area_ulevel) {
    Tcl_SetObjResult(interp,Tcl_NewIntObj(area->ulevel));
    return TCL_OK;
}

// recall
TCL_AREA_CMD(area_recall) {
    Tcl_SetObjResult(interp,Tcl_NewIntObj(area->recall));
    return TCL_OK;
}

// creator
TCL_AREA_CMD(area_creator) {
    Tcl_SetObjResult(interp,Tcl_NewStringObj(area->creator,-1));
    return TCL_OK;
}

TCL_AREA_CMD(area_namespace) {
    Tcl_SetObjResult(interp,Tcl_NewStringObj(namespace_area(area),-1));
    return TCL_OK;
}

// count [-vnum <vnum>] [-group] [type]
TCL_AREA_CMD(area_count) {
    long vnum=0;
    bool group=FALSE;
    char *arg;
    int index;

    static tclOptionList *countSwitches[]={"-vnum","-group",NULL};
    static tclOptionList *countOptions[]={"all","pc","mob","char",NULL};

    objc--;objv++; // skip the command, ie 'count'

    while(objc) {            
        arg=Tcl_GetString(objv[0]);
        if (arg[0]!='-') break;

        if (Tcl_GetIndexFromObj(interp, objv[0], countSwitches, "switch", 0, &index) != TCL_OK)
            return TCL_ERROR;

        switch(index) {
        case 0: /* -vnum */
            if (objc<2) {                                          
                Tcl_SetObjResult(interp,Tcl_NewStringObj("area count: -vnum switch needs an argument",-1));
                return TCL_ERROR;
            }
            if (Tcl_GetLongFromObj(interp,objv[1],&vnum)!=TCL_OK)
                return TCL_ERROR;
            objv++;objc--;  // Skip argument
            break;
        case 1: /* -group */
            group=TRUE;
            break;
        }

        objv++;objc--;
    }

    if (objc>1) {
        Tcl_WrongNumArgs(interp,0,objv,"area count ...");
        return TCL_ERROR;
    }

    index=1;
    if (objc==1) {
        if (Tcl_GetIndexFromObj(interp, objv[0], countOptions, "option", 0, &index) != TCL_OK)
        return TCL_ERROR;
    }

    if (group) index=4;
    if (vnum!=0) index=3;

    Tcl_SetObjResult(interp,Tcl_NewLongObj(tcl_count_people_area(area,NULL,index,vnum)));
    return TCL_OK;
}

// playersaround
TCL_AREA_CMD(area_playersaround) {
    Tcl_SetObjResult(interp,Tcl_NewIntObj(area->nplayer));
    return TCL_OK;
}

TCL_AREA_CMD(area_echo) {
    Tcl_Obj *concatObj;
    CHAR_DATA *player;

    if (objc<=1) {
        Tcl_WrongNumArgs(interp,0,objv,"area echo text ?text text ...?");
        return TCL_ERROR;
    }

    concatObj=Tcl_ConcatObj(objc-1,objv+1);
    Tcl_IncrRefCount(concatObj);
    Tcl_AppendToObj(concatObj,"\n\r",-1);

    for ( player=player_list;player!=NULL;player=player->next_player) {
        if ( player->in_room &&
             player->in_room->area==area) {   
            if ( IS_IMMORTAL(player) )
                send_to_char( "aecho> ", player );
            send_to_char( Tcl_GetString(concatObj) , player );
        }
    }

    Tcl_DecrRefCount(concatObj);
    return TCL_OK;
}

// allchar [-name]
TCL_AREA_CMD(area_allchar) {
    static tclOptionList *charsSwitches[]={"-name",NULL};
    bool name=FALSE;
    CHAR_DATA *pc;
    char *s;
    Tcl_Obj *result;

    if (objc>1) {
        s=Tcl_GetString(objv[1]);
        if (*s=='-') {
            int sindex;
            if (Tcl_GetIndexFromObj(interp, objv[1], charsSwitches, "switch", 0, &sindex) != TCL_OK)
                return TCL_ERROR;
            if (sindex==0) name=TRUE;
            s=Tcl_GetString(objv[2]);
        }
    }
    if (area->nplayer==0) {
        result=Tcl_NewStringObj("",-1);
        Tcl_SetObjResult(interp,result);
        return TCL_OK;
    }

    result=Tcl_NewListObj(0,NULL);
    for (pc=char_list;pc;pc=pc->next) {
        if (IS_NPC(pc)) continue;
        if (pc->in_room->area!=area) continue;
        if (name) {
            Tcl_ListObjAppendElement(interp,result,Tcl_NewStringObj(pc->name,-1));
        } else {
            Tcl_ListObjAppendElement(interp,result,Tcl_NewIntObj(pc->id));
        }
    }
    Tcl_SetObjResult(interp,result);
    return TCL_OK;
}

/* allmob <charid> <tclscript> */
TCL_AREA_CMD(area_allmob) {
    CHAR_DATA *ch;
    CHAR_DATA *old_ch,*next_ch;
    char *charid;
    int error;

    if (objc!=3) {
	Tcl_WrongNumArgs(interp,0,objv,"area allmob charid tcl-script");
	return TCL_ERROR;
    }

    charid=Tcl_GetString(objv[1]);

    ch=tcl_charid(NULL,charid,NULL);
    old_ch=progData->mob;
    error=TCL_OK;

    while (ch!=NULL) {
	progData->mob=ch;
	next_ch=ch->next;
	if (ch->zone==area) {
	    error=Tcl_EvalObjEx(interp,objv[2],0);
	    if (error!=TCL_OK) break;
	}
	if (!next_ch) break;
	ch=tcl_charid(NULL,charid,next_ch);
    }
    progData->mob=tcl_check_ch(old_ch);

    return error;
}

/* delay [<value>|cancel]*/
TCL_AREA_CMD(area_delay) {
    long delay;

    if (objc>1) {
	if (objc!=2) {
	    Tcl_WrongNumArgs(interp,0,objv,"area delay ?number?");
	    return TCL_ERROR;
	}

	if (Tcl_GetLongFromObj(interp,objv[1],&delay)==TCL_ERROR) {
	    if (!str_cmp(Tcl_GetString(objv[1]),"cancel")) {
		Tcl_ResetResult(interp); // clear the error state
		delay=-1;
	    } else
		return TCL_ERROR;
	}
	area->aprog_delay=delay;
    }
    Tcl_SetObjResult(interp,Tcl_NewLongObj(area->aprog_delay));
    return TCL_OK;
}


/* timer [<value>|cancel]*/
TCL_AREA_CMD(area_timer) {
    long time = 0;

    if (objc>1) {
	if (objc!=2) { /* 1 argument */
	    Tcl_WrongNumArgs(interp,0,objv,"area timer ?number?");
	    return TCL_ERROR;
	}

	if (Tcl_GetLongFromObj(interp,objv[1],&time)==TCL_ERROR) {
	    if (!str_cmp(Tcl_GetString(objv[1]),"cancel")) {
		Tcl_ResetResult(interp); // clear the error state
		time=-1;
	    } else 
		return TCL_ERROR;
	}
	area->aprog_timer=time;
    }
    Tcl_SetObjResult(interp,Tcl_NewLongObj(time));
    return TCL_OK;
}

TCL_AREA_CMD(area_addtrigger) {
    return ap_addtrigger(area,objc,objv);
}

// deltrigger
TCL_AREA_CMD(area_deltrigger) {
    static tclOptionList *deltriggerSwitches[]={"-all","-prefix", NULL};
    int sindex=-1;
    bool delAll,prefix;
    TCLPROG_LIST *trig,*trig_next;
    char *s,l;

    if (objc<=1) {
	Tcl_WrongNumArgs(interp,0,objv,"area deltrigger ?-all? ?-prefix? ?name?");
	return TCL_ERROR;
    }

    s=Tcl_GetString(objv[1]);
    if (*s=='-' && Tcl_GetIndexFromObj(interp, objv[1], deltriggerSwitches, "switch", 0, &sindex) != TCL_OK)
	return TCL_ERROR;

    delAll=sindex==0;
    prefix=sindex==1;

    if ((prefix && objc!=3)
    || (!prefix && objc!=2)) {
	Tcl_WrongNumArgs(interp,0,objv,"area deltrigger ?-all? ?-prefix? ?name?");
	return TCL_ERROR;
    }

    s=Tcl_GetString(objv[objc-1]);
    l=strlen(s);

    for (trig=area->aprogs;trig;trig=trig_next) {
	trig_next=trig->next;
	if (delAll) ap_delonetrigger(area,trig);
	else if (prefix && strncmp(trig->name,s,l)==0) ap_delonetrigger(area,trig);
	else if (strcmp(trig->name,s)==0) ap_delonetrigger(area,trig);
    }
    return TCL_OK;
}

/* triggers */
TCL_AREA_CMD(area_triggers) {
    TCLPROG_LIST *trig;
    Tcl_Obj *result;

    result=Tcl_NewListObj(0,NULL);
    for (trig=area->aprogs;trig;trig=trig->next) {
	Tcl_ListObjAppendElement(interp,result,Tcl_NewStringObj(trig->name,-1));
    }
    Tcl_SetObjResult(interp,result);
    return TCL_OK;
}

TCL_AREA_CMD(area_variable) {
    Tcl_Obj *fullname;

    if (objc<=2) {
	Tcl_WrongNumArgs(interp,0,objv,"area variable <name> <initial value>");
	return TCL_ERROR;
    }

    fullname=Tcl_NewStringObj(namespace_darea(area),-1);
    Tcl_IncrRefCount(fullname);
    Tcl_AppendToObj(fullname,"::",2);
    Tcl_AppendObjToObj(fullname,objv[1]);

    if (Tcl_ObjGetVar2(interp,fullname,NULL,0)==NULL) {
	if (Tcl_ObjSetVar2(interp,fullname,NULL,objv[2],TCL_LEAVE_ERR_MSG)==NULL) {
	    Tcl_DecrRefCount(fullname);
	    return TCL_ERROR;
	}
    }
    if (Tcl_UpVar(interp,"1",Tcl_GetString(fullname),Tcl_GetString(objv[1]),0)!=TCL_OK) {
	Tcl_DecrRefCount(fullname);
	return TCL_ERROR;
    }

    if (!is_exact_name(Tcl_GetString(objv[1]),area->area_vars)) {
	char buf[MSL];

	logf("DEBUG: new area vars: %s + %s",area->area_vars,Tcl_GetString(objv[1]));

	if (area->area_vars[0]!=0) {
	    snprintf(buf,sizeof(buf),"%s %s",area->area_vars,Tcl_GetString(objv[1]));
	    free_string(area->area_vars);
	    area->area_vars=str_dup(buf);
	} else {
	    free_string(area->area_vars);
	    area->area_vars=str_dup(Tcl_GetString(objv[1]));
	}
    }

    Tcl_DecrRefCount(fullname);
    return TCL_OK;
}

TCL_AREA_CMD(area_flag) {
    int  flag;

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"area flag <name>");
	return TCL_ERROR;
    }

    if (Tcl_GetIndexFromObjStruct(interp, objv[1], 
                                  (char **)area_options, sizeof(OPTION_TYPE), 
                                  "area flag", 0, &flag) != TCL_OK) {
        return TCL_ERROR;
    }

    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(IS_SET(area->area_flags,area_options[flag].value)));
    return TCL_OK;
}


