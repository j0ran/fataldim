//
// $Id: fd_log.h,v 1.7 2004/03/04 10:44:17 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

//
// WIZnet flags
//
#define WIZ_NONE		(0)
#define WIZ_ON			(1)
#define WIZ_TICKS		(2)
#define WIZ_LOGINS		(3)
#define WIZ_SITES		(4)
#define WIZ_LINKS		(5)
#define WIZ_DEATHS		(6)
#define WIZ_RESETS		(7)
#define WIZ_MOBDEATHS		(8)
#define WIZ_FLAGS		(9)
#define WIZ_PENALTIES		(10)
#define WIZ_SACCING		(11)
#define WIZ_LEVELS		(12)
//efine WIZ_SECURE		(13)	not in use
#define WIZ_SWITCHES		(14)
#define WIZ_SNOOPS		(15)
#define WIZ_RESTORE		(16)
#define WIZ_LOAD		(17)
#define WIZ_NEWBIE		(18)
#define WIZ_PREFIX		(19)
#define WIZ_SPAM		(20)
#define WIZ_DONATE		(21)
#define WIZ_NOTE		(22)
#define WIZ_ASAVE		(23)
#define WIZ_PLR_LOGS		(24)
#ifdef HAS_HTTP
#define WIZ_HTTP		(25)
#endif
#ifdef HAS_POP3
#define WIZ_POP3		(26)
#endif
#define WIZ_BUGS		(27)
//efine WIZ_INTERMUD		(28)	obsolete, was for intermud
#define WIZ_MEMORY		(29)
#define WIZ_HELP		(30)
//efine WIZ_IMC			(31)	obsolete, was for imc2
//efine WIZ_IMCDEBUG		(32)	obsolete, was for imc2
#ifdef HAS_HTTP
#define WIZ_HTTP_DEBUG		(33)
#endif
#ifdef HAS_POP3
#define WIZ_POP3_DEBUG		(34)
#endif
#define WIZ_QUESTS		(35)
#define WIZ_REBOOT		(36)
//efine WIZ_DEBUGSUMMON		(37)	not in use
#ifdef I3
#define WIZ_I3_ERRORS		(38)
#define WIZ_I3_INFO		(39)
#define WIZ_I3_DEBUG		(40)
#endif
#define WIZ_OOL_DETECTS		(41)
#define WIZ_BAN			(42)
#define WIZ_CMD_LOG		(43)
#define WIZ_TELNET		(44)
#define WIZ_TELNET_DEBUG	(45)
#define WIZ_RESET_DEBUG		(46)
#define WIZ_TCL_DEBUG		(47)

#define WIZNET_FLAGS		20	// max 20x8=160 flags
// keep in mind that there is currently room for 8*20 flags.


#define logf(...) log_stderr(__VA_ARGS__)
void	log_stderr	(const char *,...);
void	bugf		(const char *,...);
void	update_wiznet	(bool add,char *);
void	wiznet		(int flag,int min_level,CHAR_DATA *ch,OBJ_DATA *obj,
			 const char *,...);
