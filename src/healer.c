/* $Id: healer.c,v 1.63 2008/05/01 08:47:33 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,	   *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *									   *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael	   *
 *  Chastain, Michael Quan, and Mitchell Tse.				   *
 *									   *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc	   *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.						   *
 *									   *
 *  Much time and thought has gone into this software and you are	   *
 *  benefitting.  We hope that you share your changes too.  What goes	   *
 *  around, comes around.						   *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

#include "merc.h"
#include "color.h"
#include "interp.h"

typedef struct spells	SPELLS;
struct spells {
    char *	name;
    char *	description;
    int		cost;
    int *	sn;
    SPELL_FUN *	spell;
    int *	diagnose_gsn;
    char *	diagnose_message;
};
const struct spells HEALER_SPELLS[] = {
{"light",		"cure light wounds",	5,
 &gsn_cure_light,	spell_cure_light,
 NULL,			NULL
},
{"serious",		"cure serious wounds",	8,
 &gsn_cure_serious,	spell_cure_serious,
 NULL,			NULL
},
{"critical",		"cure critical wounds",	13,
 &gsn_cure_critical,	spell_cure_critical,
 NULL,			NULL
},
{"heal",		"heal",			25,
 &gsn_heal,		spell_heal,
 NULL,			NULL
},
{"blindness",		"cure blindness",	10,
 &gsn_cure_blindness,	spell_cure_blindness,
 &gsn_blindness,	"You're blind, to give you your sight again"
},
{"disease",		"cure disease",		8,
 &gsn_cure_disease,	spell_cure_disease,
 &gsn_plague,		"You're plagued, to cure it"
},
{"poison",		"cure poison",		13,
 &gsn_cure_poison,	spell_cure_poison,
 &gsn_poison,		"You're poisoned, to cure it"
},
{"curse",		"remove curse",		25,
 &gsn_remove_curse,	spell_remove_curse,
 &gsn_curse,		"You're cursed, to remove it"
},
{"mana",		"restore mana",		5,
 &gsn_restore_mana,	spell_restore_mana,
 NULL,			NULL
},
{"refresh",		"restore movement",	3,
 &gsn_refresh,		spell_refresh,
 NULL,			NULL
},
{"strength",		"restore strength",	18,
 &gsn_restore_strength,	spell_restore_strength,
 &gsn_chill_touch,	"You're cold, to heaten up"
},
{"armor",		"restore armor",	18,
 &gsn_restore_armor,	spell_restore_armor,
 &gsn_faerie_fire,	"You're under influence of faerie fire, to get rid of it"
},
{"massage",		"soothe",		3,
 &gsn_soothe,		spell_soothe,
 NULL,			NULL
},
{"hallucination",	"clarify",		25,
 &gsn_clarify,		spell_clarify,
 &gsn_hallucination,	"You are enjoying life, to clarify your mind"
},
{NULL,			NULL,			0,
 NULL,			NULL,
 NULL,			NULL
},
};

//
// Find a healer in this room
//
CHAR_DATA *healer_find_healer(CHAR_DATA *ch) {
    CHAR_DATA *	healer;
    char 	clan[MSL];

    // check for healer in this room
    for (healer=ch->in_room->people;healer!=NULL;healer=healer->next_in_room)
	if (IS_NPC(healer)
	&&  STR_IS_SET(healer->strbit_act,ACT_IS_HEALER)
	&&  can_see(healer,ch))
	    break;

    if ( healer==NULL ) {
	send_to_char( "You can't do that here.\n\r", ch );
	return NULL;
    }

    if (get_ool_penalty_state(ch)>0) {
	act("$N is not inclined to help you.",ch,NULL,healer,TO_CHAR);
	return NULL;
    }

    if (!can_see(healer,ch)) {
	act("$N is not aware of your presence.",ch,NULL,healer,TO_CHAR);
	return NULL;
    }

    if (healer->position<POS_SLEEPING) {
	act("$N is not in the perfect shape right now.",ch,NULL,healer,TO_CHAR);
	return NULL;
    }

    if (healer->position!=POS_STANDING) {
	act("$N is busy right now.",ch,NULL,healer,TO_CHAR);
	return NULL;
    }

    if (has_event(healer,healer_event)) {
	act("$N is busy right now.",ch,NULL,healer,TO_CHAR);
	return NULL;
    }

    // clan healers
    if (GetCharProperty(healer,PROPERTY_STRING,"clan",clan)) {
	if (ch->clan==NULL || !is_exact_name(ch->clan->clan_info->name,clan)) {
	    act("$n tells you '"C_TELL"I don't help people who are not in my clan!{x'",healer,NULL,ch,TO_VICT);
	    return NULL;
	}
    }

    return healer;
}

//
// Cast one spell
//
int healer_cast_spell(CHAR_DATA *healer,CHAR_DATA *victim,CHAR_DATA *payer,int spellnr) {
    if (!can_afford(payer,healer,100*HEALER_SPELLS[spellnr].cost))
	return 0;
    deduct_cost(payer,0,100*HEALER_SPELLS[spellnr].cost,TRUE);
    say_spell(healer,*(HEALER_SPELLS[spellnr].sn));
    (HEALER_SPELLS[spellnr].spell)(*HEALER_SPELLS[spellnr].sn,healer->level,healer,victim,TARGET_CHAR);
    return 1;
}

//
// Find a spell in the table.
//
int healer_find_spell(int gsn) {
    int	spellnr;

    for (spellnr=0;HEALER_SPELLS[spellnr].name!=NULL;spellnr++) {
	if (*HEALER_SPELLS[spellnr].sn==gsn)
	    return spellnr;
    }
    bugf("healer_find_spell(): couldn't find gsn %d in table.",gsn);
    return 0;
}

void healer_event(EVENT_DATA *event) {
    struct healerdata *data,*newdata;
    int	retval;

    data=(struct healerdata *)event->data;

    if (!IS_VALID(data->healer)) return;
    if (!IS_VALID(data->victim)) return;
    if (!IS_VALID(data->payer)) return;

    if (data->healer->in_room!=data->victim->in_room) {
	act("$n wonders where $s patient is gone...",
	    data->healer,NULL,NULL,TO_ROOM);
	return;
    }

    if (data->healer->in_room!=data->payer->in_room) {
	do_function(data->healer,&do_say,"Hey! Who is going to pay for this?");
	return;
    }

    retval=healer_diagnose(data->healer,data->victim,data->payer,TRUE,TRUE);

    if (retval==0)
	return;

    if (retval==2) {
	act("$n says '"C_SAY"$N, I'm sorry but you can't afford further treatment.{x'",
	    data->healer,NULL,data->payer,TO_ROOM);
	return;
    }

    if (retval==1) {
	newdata=(struct healerdata *)calloc(1,sizeof(struct healerdata));
	newdata->healer=data->healer;
	newdata->victim=data->victim;
	newdata->payer=data->payer;
	event=create_event(PULSE_PER_SECOND,data->healer,
	    NULL,NULL,healer_event,newdata);
    }
}

//
// returns 0 if in perfect condition
// returns 1 if a spell was casted
// returns 2 if out of money
//
int healer_diagnose(CHAR_DATA *healer,CHAR_DATA *victim,CHAR_DATA *payer,bool doheal,bool silent) {
    int		spellnr;
    int		found;
    int		cost,localcost,previous;
    char	s[MSL];

    found=0;
    cost=0;

    // first do the list of spells in the healer-table
    for (spellnr=0;HEALER_SPELLS[spellnr].name!=NULL;spellnr++) {
	if (HEALER_SPELLS[spellnr].diagnose_gsn==NULL)
	    continue;
	if (is_affected(victim,*HEALER_SPELLS[spellnr].diagnose_gsn)) {
	    if (!silent) {
		localcost=HEALER_SPELLS[spellnr].cost;
		cost+=localcost;
		sprintf(s,
		    "%s will cost %d gold",
		    HEALER_SPELLS[spellnr].diagnose_message,
		    localcost);
		do_function(healer,&do_say,s);
	    }
	    if (doheal) {
		if (healer_cast_spell(healer,victim,payer,spellnr)==0)
		    return 2;
		return 1;
	    }
	    found++;
	}
    }

    if (is_affected(victim,gsn_weaken)) {
	spellnr=healer_find_spell(gsn_restore_strength);
	if (!silent) {
	    localcost=HEALER_SPELLS[spellnr].cost;
	    cost+=localcost;
	    sprintf(s,
		"You're weakened, to regain your strength will cost %d gold.",
		localcost);
	    do_function(healer,&do_say,s);
	}
	if (doheal) {
	    if (healer_cast_spell(healer,victim,payer,spellnr)==0)
		return 2;
	    return 1;
	}
	found++;
    }
    if (!IS_NPC(victim) && victim->pcdata->condition[COND_ADRENALINE]>0 ) {
	spellnr=healer_find_spell(gsn_soothe);
	if (!silent) {
	    localcost=HEALER_SPELLS[spellnr].cost;
	    cost+=localcost;
	    sprintf(s,
		"You're troubled from your last fight, to massage you will "
		"be %d gold.",localcost);
	    do_function(healer,&do_say,s);
	}
	if (doheal) {
	    if (healer_cast_spell(healer,victim,payer,spellnr)==0)
		return 2;
	    return 1;
	}
	found++;
    }
    if (victim->hit<victim->max_hit) {
	spellnr=healer_find_spell(gsn_heal);
	if (!silent) {
	    localcost=(1+(victim->max_hit-victim->hit)/100)*
		HEALER_SPELLS[spellnr].cost;
	    cost+=localcost;
	    sprintf(s,"You're hurt, to heal will cost %d gold.",
		localcost);
	    do_function(healer,&do_say,s);
	}
	if (doheal)
	    while (victim->hit<victim->max_hit) {
		previous=victim->hit;
		if (healer_cast_spell(healer,victim,payer,spellnr)==0)
		    return 2;
		if (previous==victim->hit)
		    break;
		return 1;
	    }
	found++;
    }
    if (victim->mana<victim->max_mana) {
	spellnr=healer_find_spell(gsn_restore_mana);
	if (!silent) {
	    localcost=(1+(victim->max_mana-victim->mana)/(12+healer->level/3))*
		HEALER_SPELLS[spellnr].cost;
	    cost+=localcost;
	    sprintf(s,
		"You've lost mana, to fill your manapool will cost %d gold.",
		localcost);
	    do_function(healer,&do_say,s);
	}
	if (doheal)
	    while (victim->mana<victim->max_mana) {
		previous=victim->mana;
		if (healer_cast_spell(healer,victim,payer,spellnr)==0)
		    return 2;
		if (previous==victim->mana)
		    break;
		return 1;
	    }
	found++;
    }
    if (victim->move<victim->max_move) {
	spellnr=healer_find_spell(gsn_refresh);
	if (!silent) {
	    localcost=(1+(victim->max_move-victim->move)/healer->level)*
		HEALER_SPELLS[spellnr].cost;
	    cost+=localcost;
	    sprintf(s,"You're tired, a massage will cost %d gold.",localcost);
	    do_function(healer,&do_say,s);
	}
	if (doheal)
	    while (victim->move<victim->max_move) {
		previous=victim->move;
		if (healer_cast_spell(healer,victim,payer,spellnr)==0)
		    return 2;
		if (previous==victim->move)
		    break;
		return 1;
	    }
	found++;
    }

    if (found==0) {
	sprintf(s,"%s, you are in perfect condition!",NAME(victim));
	do_function(healer,&do_say,s);
	return 0;
    }

    if (found!=1) {
	sprintf(s,"%s, that makes a grand total of %s",
	    NAME(payer),money_string_colour(cost,0,C_SAY));
	do_function(healer,&do_say,s);
	return -1;
    }

    return 0;
}


void do_heal(CHAR_DATA *ch,char *argument) {
    CHAR_DATA *	healer;
    CHAR_DATA *	victim;
    char	arg1[MIL],arg2[MIL];
    char *	request;
    int		spellnr;
    bool	found;

    if ((healer=healer_find_healer(ch))==NULL)
	return;

    argument=one_argument(argument,arg1);
    argument=one_argument(argument,arg2);

    request=arg1;
    if (arg2[0]!=0) {
	if ((victim=get_char_room(ch,arg1))==NULL) {
	    do_function(healer,&do_say,"Sorry, couldn't find him or her here.");
	    return;
	}
	request=arg2;
    } else
	victim=ch;

    // check for dodos healing the healer
    if (healer==victim) {
	act("$N wants to heal $n...",healer,NULL,ch,TO_NOTVICT);
	act("$n exclaims '"C_SAY"I'm feeling fine, thanks for asking $N!{x'",
	    healer,NULL,ch,TO_ROOM);
	return;
    }

    if (request[0]==0) {
	act("$N says '"C_SAY"I offer the following spells{x'",
	    ch,NULL,healer,TO_CHAR);
	for (spellnr=0;HEALER_SPELLS[spellnr].name;spellnr++)
	    sprintf_to_char(ch,"  %-15s: %-25s %2d {ygold{x\n\r",
		HEALER_SPELLS[spellnr].name,
		HEALER_SPELLS[spellnr].description,
		HEALER_SPELLS[spellnr].cost);
	send_to_char(
	    "Type {Wheal <type>{x to be healed.\n\r"
	    "Type {Wdiagnose{x to find out the cost of a complete heal.\n\r"
	    "If you want to heal somebody else, use {Wheal <name> <type>{x\n\r",
	    ch);
	return;
    }

    // insert diagnose here

    if (!str_cmp(request,"all")) {
	char	s[MSL];
	struct healerdata *data;

	sprintf(s,"%s, let's see what's wrong with you.",NAME(victim));
	do_function(healer,&do_say,s);
	data=(struct healerdata *)calloc(1,sizeof(struct healerdata));
	data->healer=healer;
	data->victim=victim;
	data->payer=ch;
	create_event(PULSE_PER_SECOND,healer,NULL,NULL,healer_event,data);
	return;
    }

    found=FALSE;
    for (spellnr=0;HEALER_SPELLS[spellnr].name;spellnr++) {
	if (!str_prefix(request,HEALER_SPELLS[spellnr].name)) {
	    found = TRUE;
	    break;
	}
    }

    if (!found) {
	act("$N says '"C_SAY"Type {Yheal"C_SAY" for a list of spells.{x'",
	    ch,NULL,healer,TO_CHAR);
	return;
    }

    if (!can_afford(ch,healer,100*HEALER_SPELLS[spellnr].cost)) {
	char s[MAX_STRING_LENGTH];
	sprintf(s,"%s, you don't have enough gold for my services.",ch->name);
	do_function(healer,&do_say,s);
	return;
    }

    if (ch!=victim) {
	char	s[MSL];
	sprintf(s,"%s, on behalf of %s.",NAME(victim),NAME(ch));
	do_function(healer,&do_say,s);
    }

    healer_cast_spell(healer,victim,ch,spellnr);
}

void do_diagnose(CHAR_DATA *ch, char *argument) {
    CHAR_DATA *	healer;
    CHAR_DATA *	victim;
    char	s[MSL],arg1[MIL];

    if ((healer=healer_find_healer(ch))==NULL)
	return;

    argument=one_argument(argument,arg1);

    if (arg1[0]!=0) {
	if ((victim=get_char_room(ch,arg1))==NULL) {
	    do_function(healer,&do_say,"Sorry, couldn't find him or her here.");
	    return;
	}
    } else
	victim=ch;

    // check for dodos healing the healer
    if (healer==victim) {
	act("$N wants to heal $n...",healer,NULL,ch,TO_NOTVICT);
	act("$n exclaims '"C_SAY"I'm feeling fine, thanks for asking $N!{x'",
	    healer,NULL,ch,TO_ROOM);
	return;
    }

    if (victim==ch) {
	sprintf(s,"Okay %s, let me have a look at you...",NAME(victim));
	do_function(healer,&do_say,s);
    } else {
	sprintf(s,"Okay %s, %s wants to know what state you're in...",
	    NAME(victim),NAME(ch));
	do_function(healer,&do_say,s);
    }

    healer_diagnose(healer,victim,ch,FALSE,FALSE);
}

// adds n hp's per level of char to the victim
// wis 12: 0.5
// wis 15: 1.1
// wis 18: 1.5
// wis 21: 1.7
// wis 25: 2.0
//
void phase_layonhands(CHAR_DATA *ch) {
    CHAR_DATA *victim;
    int pos=0;
    int wisbonus;

    for (victim=ch->in_room->people;victim;victim=victim->next_in_room)
	if (victim==ch->phasedskill_target)
	    break;
    if (victim==NULL) {
	act("Your concentration is lost, you open your eyes and your target is gone!",ch,NULL,NULL,TO_CHAR);
	act("$n opens $s eyes and looks very confused.",ch,NULL,NULL,TO_ROOM);
	DeleteCharProperty(ch,PROPERTY_INT,"layonhands");
	set_phased_skill(ch,NULL,NULL,0,PHASED_NONE);
	return;
    }

    pos=0;
    GetCharProperty(ch,PROPERTY_INT,"layonhands",&pos);
    if (victim->position!=pos) {
	act("Your concentration is lost, you open your eyes and see your $N has moved!",ch,NULL,victim,TO_CHAR);
	act("$n opens $s eyes and sees he lost contact with you.",
	    ch,NULL,victim,TO_VICT);
	act("$n opens $s eyes and looks very confused.",
	    ch,NULL,victim,TO_NOTVICT);
	set_phased_skill(ch,NULL,NULL,0,PHASED_NONE);
	DeleteCharProperty(ch,PROPERTY_INT,"layonhands");
	return;
    }

    if (ch->phasedskill_step==0) {
	ch->phasedskill_step=1;
	act("You concentrate more...",ch,NULL,NULL,TO_CHAR);
	act("$n concentrates deeper...",ch,NULL,NULL,TO_ROOM);
	return;
    }

	 if (ch->mod_stat[STAT_WIS]<=15) wisbonus= 5;
    else if (ch->mod_stat[STAT_WIS]<=18) wisbonus=11;
    else if (ch->mod_stat[STAT_WIS]<=21) wisbonus=15;
    else if (ch->mod_stat[STAT_WIS]<=24) wisbonus=17;
    else                                 wisbonus=20;

    ch->mana-=ch->level/2;
    if (victim->hit>=victim->max_hit) {
	act("$N doesn't really look better!",ch,NULL,victim,TO_CHAR);
	act("You don't feel much better!",ch,NULL,victim,TO_VICT);
	act("$n is finished and removes $s hands from $N!",ch,NULL,victim,TO_NOTVICT);
    } else {
	victim->hit=UMIN(victim->max_hit,victim->hit+ch->level*wisbonus/10);
	act("$N looks much better!",ch,NULL,victim,TO_CHAR);
	act("You feel much better!",ch,NULL,victim,TO_VICT);
	act("$N looks much better!",ch,NULL,victim,TO_NOTVICT);
    }
    set_phased_skill(ch,NULL,NULL,0,PHASED_NONE);
    DeleteCharProperty(ch,PROPERTY_INT,"layonhands");
}

void do_layonhands(CHAR_DATA *ch,char *argument) {
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    int pos;

    if (ch->position==POS_FIGHTING) {
	send_to_char("Please wait until you're ready with fighting.\n\r",ch);
	return;
    }

    one_argument(argument,arg);

    if (arg[0]==0) {
	send_to_char("Lay your hands on who?\n\r",ch);
	return;
    }
    if ((victim = get_char_room(ch,arg)) == NULL) {
	send_to_char("Nobody with that name is here.\n\r",ch);
	return;
    }
    if (victim->position==POS_FIGHTING) {
	act("Please wait until $E is finished with $s fight.",
	    ch,NULL,victim,TO_CHAR);
	return;
    }

    if (ch==victim) {
	act("You place your hands on your hips, standing very proudly.",
	    ch,NULL,NULL,TO_CHAR);
	act("$n places $s hands on $s hips, standing very proudly.",
	    ch,NULL,NULL,TO_ROOM);
	return;
    }

    if ( !has_skill_available(ch,gsn_lay_on_hands)) {
	act("You place your hands on $N, but nothing happens.",
	    ch,NULL,victim,TO_CHAR);
	act("$n places $s hands on you, what is $e trying to do?",
	    ch,NULL,victim,TO_VICT);
	act("$n places $s hands on $N, who is looking very upset now.",
	    ch,NULL,victim,TO_NOTVICT);
	return;
    }

    if (!skillcheck2(ch,gsn_lay_on_hands,same_alignment(ch,victim)?125:100) || ch->mana<0 ) {
	act("You place your hands on $N, but you can't concentrate enough.",
	    ch,NULL,victim,TO_CHAR);
	act("$n places $s hands on you, but nothing happens.",
	    ch,NULL,victim,TO_VICT);
	act("$n places $s hands on $N, who tries to concentrate but fails.",
	    ch,NULL,victim,TO_NOTVICT);
	ch->mana-=ch->level/4;
	return;
    }

    set_phased_skill(ch,phase_layonhands,victim,0,PHASED_NONE);
    pos=victim->position;
    SetCharProperty(ch,PROPERTY_INT,"layonhands",&pos);

    act("You place your hands on $N and close your eyes.",
	ch,NULL,victim,TO_CHAR);
    act("$n places $s hands on you and closes $s eyes..",
	ch,NULL,victim,TO_VICT);
    act("$n places $s hands on $N and closes $s eyes..",
	ch,NULL,victim,TO_NOTVICT);
}


// Mercenaries spend a great deal of time on or near the battlefield,
// and as a result they know a considerable ammount about Bandaging
// wounds. Any object of material "cloth" can be used as a bandage
// (bandages themselves should also be made available at the general
// store).  The Mercenary bandages the injured party's wounds, resulting
// in a small ammount of healing and increased heal rate. Successful
// bandaging will heal 1 HP per 4 levels of the mercenary, and the
// recipients healrate will be increased by 50% for 10 ticks. A failed
// bandaging attempt will result in no healing, and an infection that
// will reduce healrate by 30% for 10 ticks. Bandaging must be done
// while the recipient is fresh from combat to be effective, ie while
// still affected by adrenaline).
void do_bandage(CHAR_DATA *ch,char *argument) {
    CHAR_DATA *target;
    OBJ_DATA *cloth;
    char arg1[MSL];
    char arg2[MSL];
    EFFECT_DATA ef;

    arg1[0]=arg2[0]=0;
    argument=one_argument(argument,arg1);
    if (argument[0]) {
	argument=one_argument(argument,arg2);
	if (!str_cmp(arg2,"with"))
	    argument=one_argument(argument,arg2);
    }

    if (arg1[0]==0) {
	send_to_char("Apply bandage to who?\n\r",ch);
	return;
    }
    if ((target = get_char_room(ch,arg1)) == NULL) {
	send_to_char("Nobody with that name is here.\n\r",ch);
	return;
    }
    if (target->position==POS_FIGHTING) {
	act("Perhaps you should let $m end the first first.",
	    ch,NULL,target,TO_CHAR);
	return;
    }

    if (arg2[0]==0) {
	if ((cloth=get_eq_char(ch,WEAR_HOLD))==NULL) {
	    if (ch==target)
		send_to_char("Bandage yourself with what?\n\r",ch);
	    else
		act("Bandage $M with what?",ch,NULL,target,TO_CHAR);
	    return;
	}
    } else {
	if ((cloth=get_obj_carry(ch,arg2,ch))==NULL) {
	    send_to_char("You do not have that item.\n\r",ch);
	    return;
	}
	if (cloth->wear_loc!=WEAR_NONE) {
	    send_to_char("You must remove it first.\n\r",ch);
	    return;
	}
    }
    if (cloth->item_type!=ITEM_CLOTHING) {
	act("You can't make a bandage from $p.",ch,cloth,NULL,TO_CHAR);
	return;
    }

    if (target->hit>=target->max_hit) {
	if (target==ch)
	    send_to_char("You aren't hurt!\n\r",ch);
	else
	    act("$E isn't hurt!",ch,NULL,target,TO_CHAR);
	return;
    }

    if (IS_PC(target) && target->pcdata->condition[COND_ADRENALINE]==0) {
	if (target==ch)
	    send_to_char(
		"Your wounds aren't fresh, bandaging them is useless.\n\r",ch);
	else
	    act("It's too long ago that the wounds on $N have been inflicted.",
		ch,NULL,target,TO_CHAR);
	return;
    }

    if (ch==target) {
	act("You use $p to apply a bandage on yourself.",ch,cloth,NULL,TO_CHAR);
	act("$n uses $p to apply a bandage on $mself.",ch,cloth,NULL,TO_ROOM);
    } else {
	act("You use $p to apply a bandage on $N.",ch,cloth,target,TO_CHAR);
	act("$n uses $p to apply a bandage on you.",ch,cloth,target,TO_VICT);
	act("$n uses $p to apply a bandage on $N.",ch,cloth,target,TO_NOTVICT);
    }
    obj_from_char(cloth);
    extract_obj(cloth);


    if (skillcheck(ch,gsn_bandage)) {
	target->hit=UMIN(target->hit+ch->level/4,target->max_hit);
	ZEROVAR(&ef,EFFECT_DATA);
	ef.where	= TO_EFFECTS;
	ef.type		= gsn_bandage;
	ef.level	= ch->level;
	ef.duration	= 10;
	ef.location	= APPLY_NONE;
	ef.modifier	= 50;
	ef.bitvector	= EFF_HEALING;
	ef.casted_by	= ch->id;
	effect_to_char(target,&ef);
	check_improve(ch,gsn_bandage,TRUE,1);
    } else {
	ZEROVAR(&ef,EFFECT_DATA);
	ef.where	= TO_EFFECTS;
	ef.type		= gsn_bandage;
	ef.level	= ch->level;
	ef.duration	= 10;
	ef.location	= APPLY_NONE;
	ef.modifier	= -30;
	ef.bitvector	= EFF_HEALING;
	ef.casted_by	= ch->id;
	effect_to_char(target,&ef);
	check_improve(ch,gsn_bandage,FALSE,1);
	send_to_char("You have your doubts about this bandage...\n\r",ch);
    }
    WAIT_STATE(ch,skill_beats(gsn_bandage,ch));
}
