//
// $Id: fd_fileio.h,v 1.2 2006/04/07 20:54:20 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef INCLUDE_FD_FILEIO_H
#define INCLUDE_FD_FILEIO_H

//
// compare "word" with "literal". If equal, assign "value" to "field"
//
#define KEY( literal, field, value )					\
				if ( !str_cmp( (word), (literal) ) ) {	\
				    (field)	= (value);		\
				    fMatch	= TRUE;			\
				    break;				\
				}

//
// compare "word" with "literal". If equal, assign "value" to
// "field" after freeing the space in use by "field"
//
#define KEYS( literal, field, value )					\
				if ( !str_cmp( (word), (literal) ) ) {	\
				    free_string(field);			\
				    (field)	= (value);		\
				    fMatch	= TRUE;			\
				    break;				\
				}

char	fread_letter		(FILE *fp);
void	funread_letter		(FILE *fp,char c);
long	fread_long		(FILE *fp);
int	fread_number		(FILE *fp);
long	fread_flag		(FILE *fp);
char *	fread_string_temp	(FILE *fp);
char *	fread_string		(FILE *fp);
char *	fread_string_eol	(FILE *fp);
char *	fread_string_eol_temp	(FILE *fp);
void	fread_to_eol		(FILE *fp);
char *	fread_word		(FILE *fp);

#endif	// INCLUDE_FD_FILEIO_H
