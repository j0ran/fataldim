//
// $Id: fd_objprog.c,v 1.65 2008/02/22 19:51:19 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#define NEEDS_TCL
#include "merc.h"
#include "interp.h"
#include "olc.h"

/*
 * Misc thingies
 */
int op_error(OBJ_DATA *obj) {
    char *errormsg;

    bugf("OP: TCL error in namespace %s in trigger %s on line %d: %s",
	namespace_obj(obj),
	progData==NULL?"(null)":progData->trigName,
    interp->errorLineDontUse,
	Tcl_GetStringResult(interp));

    errormsg=str_dup(Tcl_GetVar(interp,"errorInfo",TCL_GLOBAL_ONLY));
    while (errormsg) {
	char *line=strsep(&errormsg,"\n\r");
	logf("OP: errorInfo: %s",line);
	wiznet(WIZ_TCL_DEBUG,0,NULL,NULL,"OP: errorInfo: %s",line);
    }
    free_string(errormsg);
    Tcl_ResetResult(interp);
    return TCL_ERROR;
}


/*
 * ---------------------------------------------------------------------
 * Trigger handlers. These are called from various parts of the code
 * when an event is triggered.
 * ---------------------------------------------------------------------
 */

/*
 * general trigger for "normal" character/object interaction
 */

/*
 * In case of a return value (boolean always), a TRUE means
 * "okay, go on" and a FALSE means "this is wrong"
 */
bool op_general_trigger_bool_all( OBJ_DATA *obj, CHAR_DATA *ch,
				  int trig_type, char *buf,
				  CHAR_DATA *vict,OBJ_DATA *obj2) {
    TCLPROG_LIST *prg;
    int boolResult;

// Don't think we need these.
//    if (buf==NULL)
//	Tcl_SetVar(interp,"t","",0);
//    else
//	Tcl_SetVar(interp,"t",buf,0);

    for ( prg = obj->oprogs; prg != NULL; prg = prg->next ) {
	if(prg->trig_type==trig_type) {
	    Tcl_Obj *result;

	    if(tcl_start(prg->name,buf,NULL,ch,obj,vict,obj2,obj->in_room,NULL))
		break; // if one tcl_start fails they tend to all fail

	    if (prg->trig_phrase && str_cmp("",prg->trig_phrase)) {
		if(tcl_run_op_code(obj,prg->trig_phrase)==TCL_ERROR)
		    goto error;

		result=Tcl_GetObjResult(interp);
		boolResult=FALSE;
		if(Tcl_GetBooleanFromObj(interp,result,&boolResult)==TCL_ERROR)
		    goto error;
	    } else
		boolResult=TRUE;

	    if(boolResult) {
		if (tcl_run_op_code(obj,prg->code)==TCL_ERROR)
		    goto error;
		result=Tcl_GetObjResult(interp);
		boolResult=FALSE;
		if(Tcl_GetBooleanFromObj(interp,result,&boolResult)==TCL_ERROR) goto error;
		if(tcl_stop()) return FALSE;
		return boolResult;
	    }

	    if(tcl_stop()) return FALSE;
	}
    }
    return TRUE;

error:
    op_error(obj);
    if(tcl_stop()) return FALSE;
    return FALSE;
}

void op_general_trigger_all( OBJ_DATA *obj, CHAR_DATA *ch,
			     int trig_type, char *buf,
			     CHAR_DATA *vict,OBJ_DATA *obj2)
{
    TCLPROG_LIST *prg;
    int boolResult;

// Don't think we need these.
//    if (buf==NULL)
//	Tcl_SetVar(interp,"t","",0);
//    else
//	Tcl_SetVar(interp,"t",buf,0);

    for ( prg = obj->oprogs; prg != NULL; prg = prg->next ) {
	if(prg->trig_type==trig_type) {
	    Tcl_Obj *result;

	    if(tcl_start(prg->name,buf,NULL,ch,obj,vict,obj2,obj->in_room,NULL)) return;

	    if (prg->trig_phrase && str_cmp("",prg->trig_phrase)) {
		if(tcl_run_op_code(obj,prg->trig_phrase)==TCL_ERROR)
		    goto error;

		result=Tcl_GetObjResult(interp);
		boolResult=FALSE;
		if(Tcl_GetBooleanFromObj(interp,result,&boolResult)==TCL_ERROR)
		    goto error;
	    } else
		boolResult=TRUE;

	    if(boolResult) {
		if (tcl_run_op_code(obj,prg->code)==TCL_ERROR)
		    goto error;
		if(tcl_stop()) return;
		break;
	    }

	    if(tcl_stop()) return;
	}
    }
    return;

error:
    op_error(obj);
    if(tcl_stop()) return;
    return;
}

//
// by making these wrappers it's easier to add new fields to the
// op_general_trigger() function.
//
//
// trigger1: obj
// trigger2: obj + ch
// trigger3: obj + ch + text
// trigger4: obj + ch + obj2
// trigger5: obj + ch + victim + obj2
// trigger6: obj + ch + victim
// trigger7: obj + ch + victim + text
//
bool op_general_trigger1_bool(int trig_type,OBJ_DATA *obj)
    { return op_general_trigger_bool_all(obj,NULL,trig_type,NULL,NULL,NULL); }
bool op_general_trigger2_bool(int trig_type,OBJ_DATA *obj,CHAR_DATA *ch)
    { return op_general_trigger_bool_all(obj,ch,trig_type,NULL,NULL,NULL); }
bool op_general_trigger3_bool(int trig_type,OBJ_DATA *obj,CHAR_DATA *ch,char *buf)
    { return op_general_trigger_bool_all(obj,ch,trig_type,buf,NULL,NULL); }
bool op_general_trigger4_bool(int trig_type,OBJ_DATA *obj,CHAR_DATA *ch,OBJ_DATA *obj2)
    { return op_general_trigger_bool_all(obj,ch,trig_type,NULL,NULL,obj2); }
bool op_general_trigger5_bool(int trig_type,OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *vict,OBJ_DATA *obj2)
    { return op_general_trigger_bool_all(obj,ch,trig_type,NULL,vict,obj2); }
bool op_general_trigger6_bool(int trig_type,OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *vict)
    { return op_general_trigger_bool_all(obj,ch,trig_type,NULL,vict,NULL); }
bool op_general_trigger7_bool(int trig_type,OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *vict, char *buf)
    { return op_general_trigger_bool_all(obj,ch,trig_type,buf,vict,NULL); }


void op_general_trigger1(int trig_type,OBJ_DATA *obj)
    { op_general_trigger_all(obj,NULL,trig_type,NULL,NULL,NULL); }
void op_general_trigger2(int trig_type,OBJ_DATA *obj,CHAR_DATA *ch)
    { op_general_trigger_all(obj,ch,trig_type,NULL,NULL,NULL); }
void op_general_trigger3(int trig_type,OBJ_DATA *obj,CHAR_DATA *ch,char *buf)
    { op_general_trigger_all(obj,ch,trig_type,buf,NULL,NULL); }
void op_general_trigger4(int trig_type,OBJ_DATA *obj,CHAR_DATA *ch,OBJ_DATA *obj2)
    { op_general_trigger_all(obj,ch,trig_type,NULL,NULL,obj2); }
void op_general_trigger5(int trig_type,OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *vict,OBJ_DATA *obj2)
    { op_general_trigger_all(obj,ch,trig_type,NULL,vict,obj2); }
void op_general_trigger6(int trig_type,OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *vict)
    { op_general_trigger_all(obj,ch,trig_type,NULL,vict,NULL); }
void op_general_trigger7(int trig_type,OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *vict, char *buf)
    { op_general_trigger_all(obj,ch,trig_type,buf,vict,NULL); }


//
// drop trigger
//
bool op_predrop_trigger( OBJ_DATA *obj, CHAR_DATA *ch) {
    return op_general_trigger2_bool(OTRIG_PREDROP,obj,ch);
}
void op_drop_trigger( OBJ_DATA *obj, CHAR_DATA *ch) {
    op_general_trigger2(OTRIG_DROP,obj,ch);
}

//
// open trigger
//
bool op_preopen_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    return op_general_trigger2_bool(OTRIG_PREOPEN, obj,ch);
}
void op_open_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    op_general_trigger2(OTRIG_OPEN, obj,ch);
}

//
// close trigger
//
bool op_preclose_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    return op_general_trigger2_bool(OTRIG_PRECLOSE,obj,ch);
}
void op_close_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    op_general_trigger2(OTRIG_CLOSE, obj,ch);
}

//
// lock trigger
//
bool op_canlock_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    return op_general_trigger2_bool(OTRIG_CANLOCK, obj,ch);
}
bool op_prelock_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    return op_general_trigger2_bool(OTRIG_PRELOCK, obj,ch);
}
void op_lock_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    op_general_trigger2(OTRIG_LOCK, obj,ch);
}

//
// unlock trigger
//
bool op_canunlock_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    return op_general_trigger2_bool(OTRIG_CANUNLOCK, obj,ch);
}
bool op_preunlock_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    return op_general_trigger2_bool(OTRIG_PREUNLOCK, obj,ch);
}
void op_unlock_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    op_general_trigger2(OTRIG_UNLOCK, obj,ch);
}

//
// enter trigger
//
bool op_preenter_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    return op_general_trigger2_bool(OTRIG_PREENTER, obj,ch);
}
void op_enter_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    op_general_trigger2(OTRIG_ENTER, obj,ch);
}

//
// eat trigger
//
bool op_preeat_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    return op_general_trigger2_bool( OTRIG_PREEAT,obj,ch);
}
void op_eat_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    op_general_trigger2(OTRIG_EAT,obj,ch);
}

//
// drink trigger
//
bool op_predrink_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    return op_general_trigger2_bool(OTRIG_PREDRINK, obj,ch);
}
void op_drink_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    op_general_trigger2( OTRIG_DRINK,obj,ch);
}

//
// quaff trigger
//
bool op_prequaff_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    return op_general_trigger2_bool(OTRIG_PREQUAFF, obj,ch);
}
void op_quaff_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    op_general_trigger2( OTRIG_QUAFF,obj,ch);
}

//
// recite trigger
//
bool op_prerecite_trigger(OBJ_DATA *obj,CHAR_DATA *ch, OBJ_DATA *otarget, CHAR_DATA *ctarget) {
    return op_general_trigger5_bool(OTRIG_PRERECITE, obj,ch,ctarget,otarget);
}
void op_recite_trigger(OBJ_DATA *obj,CHAR_DATA *ch, OBJ_DATA *otarget, CHAR_DATA *ctarget) {
    op_general_trigger5( OTRIG_RECITE,obj,ch,ctarget,otarget);
}

//
// lookin trigger
//
bool op_prelookin_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    return op_general_trigger2_bool(OTRIG_PRELOOKIN, obj,ch);
}
void op_lookin_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    op_general_trigger2( OTRIG_LOOKIN,obj,ch);
}

//
// lookat trigger
//
bool op_prelookat_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    return op_general_trigger2_bool(OTRIG_PRELOOKAT,obj,ch);
}
void op_lookat_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    op_general_trigger2(OTRIG_LOOKAT,obj,ch);
}
bool op_prelookat_ed_trigger(OBJ_DATA *obj,CHAR_DATA *ch, char *txt) {
    return op_general_trigger3_bool(OTRIG_PRELOOKAT_ED,obj,ch,txt);
}
void op_lookat_ed_trigger(OBJ_DATA *obj,CHAR_DATA *ch,char *txt) {
    op_general_trigger3(OTRIG_LOOKAT_ED,obj,ch,txt);
}

//
// read trigger
//
bool op_preread_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    return op_general_trigger2_bool(OTRIG_PREREAD,obj,ch);
}
void op_read_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    op_general_trigger2(OTRIG_READ,obj,ch);
}

//
// sit trigger
//
bool op_presit_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    return op_general_trigger2_bool(OTRIG_PRESIT,obj,ch);
}
void op_sit_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    op_general_trigger2(OTRIG_SIT,obj,ch);
}

//
// stand trigger
//
bool op_prestand_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    return op_general_trigger2_bool(OTRIG_PRESTAND,obj,ch);
}
void op_stand_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    op_general_trigger2(OTRIG_STAND,obj,ch);
}

//
// rest trigger
//
bool op_prerest_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    return op_general_trigger2_bool(OTRIG_PREREST,obj,ch);
}
void op_rest_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    op_general_trigger2(OTRIG_REST,obj,ch);
}

//
// sleep trigger
//
bool op_presleep_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    return op_general_trigger2_bool(OTRIG_PRESLEEP,obj,ch);
}
void op_sleep_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    op_general_trigger2(OTRIG_SLEEP,obj,ch);
}

//
// trap trigger
//
bool op_pretrap_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    return op_general_trigger2_bool(OTRIG_PRETRAP,obj,ch);
}
void op_trap_trigger(OBJ_DATA *obj,CHAR_DATA *ch) {
    op_general_trigger2(OTRIG_TRAP,obj,ch);
}

//
// Now the more specialized triggers
//

//
// wear trigger
//
bool op_prewear_trigger(OBJ_DATA *obj,CHAR_DATA *ch,int loc) {
    char buf[100];
    strcpy(buf,option_find_value(loc,wear_loc_options));
    return op_general_trigger3_bool(OTRIG_PREWEAR,obj,ch,buf);
}
void op_wear_trigger(OBJ_DATA *obj,CHAR_DATA *ch,int loc) {
    char buf[100];
    strcpy(buf,option_find_value(loc,wear_loc_options));
    op_general_trigger3(OTRIG_WEAR,obj,ch,buf);
}

//
// remove trigger
//
bool op_preremove_trigger(OBJ_DATA *obj,CHAR_DATA *ch,int loc) {
    char buf[100];
    strcpy(buf,option_find_value(loc,wear_loc_options));
    return op_general_trigger3_bool(OTRIG_PREREMOVE,obj,ch,buf);
}
void op_remove_trigger(OBJ_DATA *obj,CHAR_DATA *ch,int loc) {
    char buf[100];
    strcpy(buf,option_find_value(loc,wear_loc_options));
    op_general_trigger3(OTRIG_REMOVE,obj,ch,buf);
}

//
// put trigger
//
bool op_preput_trigger( OBJ_DATA *obj, CHAR_DATA *ch,OBJ_DATA *container) {
    return op_general_trigger4_bool(OTRIG_PREPUT,obj,ch,container);
}
void op_put_trigger( OBJ_DATA *obj, CHAR_DATA *ch,OBJ_DATA *container) {
    op_general_trigger4(OTRIG_PUT,obj,ch,container);
}

//
// putin trigger
//
bool op_preputin_trigger( OBJ_DATA *container, CHAR_DATA *ch,OBJ_DATA *obj) {
    return op_general_trigger4_bool(OTRIG_PREPUTIN,container,ch,obj);
}
void op_putin_trigger( OBJ_DATA *container, CHAR_DATA *ch,OBJ_DATA *obj) {
    op_general_trigger4(OTRIG_PUTIN,container,ch,obj);
}

//
// use trigger
//
bool op_preuse_trigger(OBJ_DATA *obj,CHAR_DATA *ch,char *txt) {
    return op_general_trigger3_bool(OTRIG_PREUSE,obj,ch,txt);
}
void op_use_trigger(OBJ_DATA *obj,CHAR_DATA *ch,char *txt) {
    op_general_trigger3(OTRIG_USE,obj,ch,txt);
}

//
// give trigger
//
bool op_pregive_trigger( OBJ_DATA *obj, CHAR_DATA *ch,CHAR_DATA *victim) {
    return op_general_trigger6_bool(OTRIG_PREGIVE,obj,ch,victim);
}
void op_give_trigger( OBJ_DATA *obj, CHAR_DATA *ch,CHAR_DATA *victim) {
    op_general_trigger6(OTRIG_GIVE,obj,ch,victim);
}

//
// get trigger
//
bool op_preget_trigger( OBJ_DATA *obj, CHAR_DATA *ch,OBJ_DATA *container) {
    return op_general_trigger4_bool(OTRIG_PREGET,obj,ch,container);
}
void op_get_trigger( OBJ_DATA *obj, CHAR_DATA *ch,OBJ_DATA *container) {
    op_general_trigger4(OTRIG_GET,obj,ch,container);
}

//
// getout trigger
//
bool op_pregetout_trigger( OBJ_DATA *container, CHAR_DATA *ch,OBJ_DATA *obj) {
    return op_general_trigger4_bool(OTRIG_PREGETOUT,container,ch,obj);
}
void op_getout_trigger( OBJ_DATA *container, CHAR_DATA *ch,OBJ_DATA *obj) {
    op_general_trigger4(OTRIG_GETOUT,container,ch,obj);
}

//
// zap trigger
//
bool op_prezap_trigger(OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *victim,OBJ_DATA *target) {
    return op_general_trigger5_bool(OTRIG_PREZAP,obj,ch,victim,target);
}
void op_zap_trigger(OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *victim,OBJ_DATA *target) {
    op_general_trigger5(OTRIG_ZAP,obj,ch,victim,target);
}

//
// sell trigger
//
bool op_presell_trigger(OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *shopkeeper) {
    return op_general_trigger6_bool(OTRIG_PRESELL,obj,ch,shopkeeper);
}
void op_sell_trigger(OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *shopkeeper) {
    op_general_trigger6(OTRIG_SELL,obj,ch,shopkeeper);
}

//
// buy trigger
//
void op_buy_trigger(OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *shopkeeper) {
    op_general_trigger6(OTRIG_BUY,obj,ch,shopkeeper);
}

//
// hit trigger
//
bool op_prehit_trigger(OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *victim) {
    return op_general_trigger6_bool(OTRIG_PREHIT,obj,ch,victim);
}
void op_hit_trigger(OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *victim) {
    op_general_trigger6(OTRIG_HIT,obj,ch,victim);
}
void op_nohit_trigger(OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *victim, char *buf) {
    op_general_trigger7(OTRIG_NOHIT,obj,ch,victim,buf);
}

//
// key trigger
//
bool op_prekey_trigger(OBJ_DATA *obj,CHAR_DATA *ch,OBJ_DATA *obj2) {
    return op_general_trigger4_bool(OTRIG_PREKEY,obj,ch,obj2);
}
void op_key_trigger(OBJ_DATA *obj,CHAR_DATA *ch,OBJ_DATA *obj2) {
    op_general_trigger4(OTRIG_KEY,obj,ch,obj2);
}

//
// brandish trigger
//
bool op_prebrandish_trigger(OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *victim,OBJ_DATA *obj2) {
    return op_general_trigger5_bool(OTRIG_PREBRANDISH,obj,ch,victim,obj2);
}
void op_brandish_trigger(OBJ_DATA *obj,CHAR_DATA *ch,CHAR_DATA *victim,OBJ_DATA *obj2) {
    op_general_trigger5(OTRIG_BRANDISH,obj,ch,victim,obj2);
}

//
// play trigger
//
bool op_playfilter_trigger(OBJ_DATA *obj,CHAR_DATA *ch,char *song) {
    return op_general_trigger3_bool(OTRIG_PLAYFILTER,obj,ch,song);
}
bool op_preplay_trigger(OBJ_DATA *obj,CHAR_DATA *ch,char *song) {
    return op_general_trigger3_bool(OTRIG_PREPLAY,obj,ch,song);
}
void op_play_trigger(OBJ_DATA *obj,CHAR_DATA *ch,char *song) {
    op_general_trigger3(OTRIG_PLAY,obj,ch,song);
}

//
// hour, timer, delay and random triggers
//
void op_hour_trigger(OBJ_DATA *obj,int hour) {
    char buf[INT_STRING_BUF_SIZE];
    itoa_r(hour,buf,sizeof(buf));
    op_general_trigger3(OTRIG_HOUR,obj,NULL,buf);
}
void op_timer_trigger(OBJ_DATA *obj) {
    char buf[LONG_STRING_BUF_SIZE];
    ltoa_r(obj->oprog_timer,buf,sizeof(buf));
    op_general_trigger3(OTRIG_TIMER,obj,NULL,buf);
}
void op_delay_trigger(OBJ_DATA *obj) {
    op_general_trigger1(OTRIG_DELAY,obj);
}
void op_random_trigger(OBJ_DATA *obj) {
    op_general_trigger1(OTRIG_RANDOM,obj);
}

void op_load_trigger(OBJ_DATA *obj,CHAR_DATA *loader) {
    op_general_trigger2(OTRIG_LOAD,obj,loader);
}

//
// something for all objects in the room and carried by player
//
#define MAX_TRIG_LOOP_SIZE 5000
// there is something wrong here. Once every few weeks this codes
// goes in an endless loop.
bool op_allobjects_trigger_bool(int TRIGTYPE,CHAR_DATA *ch,char *string) {
    OBJ_DATA *obj,*obj_next;
    int loop_guard;

    loop_guard=MAX_TRIG_LOOP_SIZE;
    for (obj=ch->in_room->contents;obj!=NULL;obj=obj_next) {
	if (loop_guard--==0) {
	    bugf("LOOPGUARD broke loop of op_allobjects_trigger_bool()");
	    bugf("room: %d",ch->in_room->vnum);
	    bugf("ch: %s",ch->name);
	    break;
	}
	obj_next=obj->next_content;
	if (OBJ_HAS_TRIGGER(obj,TRIGTYPE) &&
	    !op_general_trigger3_bool(TRIGTYPE,obj,ch,string))
	    return FALSE;
    }
    loop_guard=MAX_TRIG_LOOP_SIZE;
    for (obj=ch->carrying;obj!=NULL;obj=obj_next) {
	if (loop_guard--==0) {
	    bugf("LOOPGUARD broke loop of op_allobjects_trigger_bool()");
	    bugf("ch: %s",ch->name);
	    break;
	}
	obj_next=obj->next_content;
	if (OBJ_HAS_TRIGGER(obj,TRIGTYPE) &&
	    !op_general_trigger3_bool(TRIGTYPE,obj,ch,string))
	    return FALSE;
    }
    return TRUE;
}
void op_allobjects_trigger(int TRIGTYPE,CHAR_DATA *ch,char *string) {
    OBJ_DATA *obj,*obj_next;
    int loop_guard;

    loop_guard=MAX_TRIG_LOOP_SIZE;
    for (obj=ch->in_room->contents;obj!=NULL;obj=obj_next) {
	if (loop_guard--==0) {
	    bugf("LOOPGUARD broke loop of op_allobjects_trigger()");
	    bugf("room: %d",ch->in_room->vnum);
	    bugf("ch: %s",ch->name);
	    break;
	}
	obj_next=obj->next_content;
	if (OBJ_HAS_TRIGGER(obj,TRIGTYPE))
	    op_general_trigger3(TRIGTYPE,obj,ch,string);
	if (obj_next && !IS_VALID(obj_next))
	    break;
    }
    loop_guard=MAX_TRIG_LOOP_SIZE;
    for (obj=ch->carrying;obj!=NULL;obj=obj_next) {
	if (loop_guard--==0) {
	    bugf("LOOPGUARD broke loop of op_allobjects_trigger()");
	    bugf("ch: %s",ch->name);
	    break;
	}
	obj_next=obj->next_content;
	if (OBJ_HAS_TRIGGER(obj,TRIGTYPE))
	    op_general_trigger3(TRIGTYPE,obj,ch,string);
	if (obj_next && !IS_VALID(obj_next))
	    break ;
    }
}
bool op_inventoryobjects_trigger6_bool(int TRIGTYPE,CHAR_DATA *ch,CHAR_DATA *victim) {
    OBJ_DATA *obj,*obj_next;
    int loop_guard;

    loop_guard=MAX_TRIG_LOOP_SIZE;
    for (obj=ch->carrying;obj!=NULL;obj=obj_next) {
	if (loop_guard--==0) {
	    bugf("LOOPGUARD broke loop of op_inventoryobjects_trigger6_bool()");
	    bugf("ch: %s",ch->name);
	    break;
	}
	obj_next=obj->next_content;
	if (OBJ_HAS_TRIGGER(obj,TRIGTYPE) &&
	    !op_general_trigger6_bool(TRIGTYPE,obj,ch,victim))
	    return FALSE;
    }
    return TRUE;
}
bool op_inventoryobjects_trigger7_bool(int TRIGTYPE,CHAR_DATA *ch,CHAR_DATA *victim, char *buf) {
    OBJ_DATA *obj,*obj_next;
    int loop_guard;

    loop_guard=MAX_TRIG_LOOP_SIZE;
    for (obj=ch->carrying;obj!=NULL;obj=obj_next) {
	if (loop_guard--==0) {
	    bugf("LOOPGUARD broke loop of op_inventoryobjects_trigger6_bool()");
	    bugf("ch: %s",ch->name);
	    break;
	}
	obj_next=obj->next_content;
	if (OBJ_HAS_TRIGGER(obj,TRIGTYPE) &&
	    !op_general_trigger7_bool(TRIGTYPE,obj,ch,victim,buf))
	    return FALSE;
    }
    return TRUE;
}

//
// interpret trigger
//
bool op_interpret_unknown(CHAR_DATA *ch,char *string) {
    bool b=op_allobjects_trigger_bool(OTRIG_INTERPRET_UNKNOWN,ch,string);
    return !b;
}
bool op_interpret_preknown(CHAR_DATA *ch,char *string) {
    bool b=op_allobjects_trigger_bool(OTRIG_INTERPRET_PREKNOWN,ch,string);
    return !b;
}
void op_interpret_postknown(CHAR_DATA *ch,char *string) {
    op_allobjects_trigger(OTRIG_INTERPRET_POSTKNOWN,ch,string);
}

//
// speech trigger
//
bool op_prespeech_trigger(CHAR_DATA *ch,char *words) {
    bool b=op_allobjects_trigger_bool(OTRIG_PRESPEECH,ch,words);
    return b;
}
void op_speech_trigger(CHAR_DATA *ch,char *words) {
    op_allobjects_trigger(OTRIG_SPEECH,ch,words);
}

//
// attack trigger
//
bool op_preattack_trigger(CHAR_DATA *ch,CHAR_DATA *victim, char *means) {
    return op_inventoryobjects_trigger7_bool(OTRIG_PREATTACK,ch,victim,means);
}

//
// day / sun triggers
//
void	op_doallobjects(int trigger,char *text) {
    OBJ_DATA *obj=object_list;
    OBJ_DATA *obj_next;

    while (obj!=NULL) {
	obj_next=obj->next;

	if (!IS_VALID(obj)) return;	// these things happen sometimes

	if (OBJ_HAS_TRIGGER(obj,trigger))
    	    op_general_trigger_all(obj,NULL,trigger,text,NULL,NULL);

	obj=obj_next;
    }
}
void    op_sunset_trigger(void) {
    op_doallobjects(OTRIG_SUNSET,NULL);
}
void    op_sunrise_trigger(void) {
    op_doallobjects(OTRIG_SUNRISE,NULL);
}
void    op_dayend_trigger(void) {
    op_doallobjects(OTRIG_DAYEND,NULL);
}
void    op_daystart_trigger(void) {
    op_doallobjects(OTRIG_DAYSTART,NULL);
}

void    op_weather_trigger(int old_state, int new_state) {
    char *old_descr,*new_descr;
    char *text;

    old_descr=table_find_value(old_state,weather_sky_table);
    new_descr=table_find_value(new_state,weather_sky_table);
    text=malloc(strlen(old_descr)+strlen(new_descr)+2);

    strcpy(text,new_descr);
    strcat(text," ");                       
    strcat(text,old_descr);
    op_doallobjects(OTRIG_WEATHER,text);
    free(text);
}
