/* $Id: note.c,v 1.100 2007/03/11 11:47:47 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

/*
 * 19971230 EG  Modified note_announce() to also write a message
        to the poster.
 */

#include "merc.h"
#include "interp.h"


/* local procedures */
void	load_thread(int type);
bool	load_note(int type,char *filename);
void	parse_note(CHAR_DATA *ch,char *argument,int type);
bool	hide_note(CHAR_DATA *ch,NOTE_DATA *pnote);
void	note_remove(CHAR_DATA *ch,NOTE_DATA *pnote,int type,bool delete);
void	save_poll(void);


int count_spool(CHAR_DATA *ch, int type) {
    int		count=0;
    NOTE_DATA *	pnote;

    for (pnote=notes_table[type].list; pnote != NULL; pnote = pnote->next)
        if (!hide_note(ch,pnote))
            count++;

    return count;
}

NOTE_DATA* first_unread_note(CHAR_DATA *ch, int type) {
    NOTE_DATA *	pnote;

    for (pnote=notes_table[type].list; pnote != NULL; pnote = pnote->next)
        if (!hide_note(ch,pnote))
            return pnote;
    return NULL;
}

void do_unread(CHAR_DATA *ch, char *argument) {
    int		count;
    int		p=-1;
    bool	found=FALSE;
    int		type;

    if (IS_NPC(ch))
        return;

    if (argument[0]) {
        if (!str_prefix(argument,"list") || !str_prefix(argument,"show")) {
            for (type=0;type<MAX_NOTES;type++) {
                sprintf_to_char(ch,"%s:\n\r",
                                Capitalize(notes_table[type].multiple));
                parse_note(ch,"unread",type);
            }
            return;
        }

        if (!str_prefix(argument,"catchup")) {
            for (type=0;type<MAX_NOTES;type++)
                ch->pcdata->last_notes[type]=current_time;
            send_to_char("No more unread notes.\n\r",ch);

            return;
        }

        if (!str_prefix(argument,"read")) {
            int type_of_oldest=-1;
            time_t time_of_oldest=-1;
            NOTE_DATA *pnote;
            for (type=0;type<MAX_NOTES;type++) {
                if ((pnote=first_unread_note(ch,type))) {
                    if ((time_of_oldest==-1) || (time_of_oldest>pnote->date_stamp)) {
                        type_of_oldest=type;
                        time_of_oldest=pnote->date_stamp;
                    }
                }
            }
            if (type_of_oldest==-1) {
                send_to_char("No unread notes.\n\r",ch);
                return;
            }
            sprintf_to_char(ch,"%s ",notes_table[type_of_oldest].description);
            parse_note(ch,"",type_of_oldest);
            return;
        }

        send_to_char("Syntax: unread [list|show|catchup|read]\n\r",ch);
        return;
    }

    for (type=0;type<MAX_NOTES;type++) {
        if ((count=count_spool(ch,type))>0) {
            found = TRUE;
            sprintf_to_char(ch,"There %s %d new %s waiting.\n\r",
                            count > 1 ? "are" : "is",count,
                            count==1? notes_table[type].single:notes_table[type].multiple,
                            count > 1 ? "s" : "");
        }
    }

    GetCharProperty(ch,PROPERTY_INT,"lastpollid",&p);
    if (mud_data.poll_id != p && mud_data.poll_open == TRUE) {
        found = TRUE;
        send_to_char("There is a poll you haven't voted for yet.\n\r",ch);
    }

    if (!found)
        send_to_char("You have no unread notes.\n\r",ch);
}

void do_note(CHAR_DATA *ch,char *argument) {
    parse_note(ch,argument,NOTE_NOTE);
}

void do_idea(CHAR_DATA *ch,char *argument) {
    parse_note(ch,argument,NOTE_IDEA);
}

void do_penalty(CHAR_DATA *ch,char *argument) {
    parse_note(ch,argument,NOTE_PENALTY);
}

void do_news(CHAR_DATA *ch,char *argument) {
    parse_note(ch,argument,NOTE_NEWS);
}

void do_changes(CHAR_DATA *ch,char *argument) {
    parse_note(ch,argument,NOTE_CHANGES);
}

void do_bug(CHAR_DATA *ch,char *argument) {
    parse_note(ch,argument,NOTE_BUG);
}

void do_typo(CHAR_DATA *ch,char *argument) {
    parse_note(ch,argument,NOTE_TYPO);
}

void do_mcnote(CHAR_DATA *ch,char *argument) {
    parse_note(ch,argument,NOTE_MCNOTE);
}


void load_notes(void) {
    int	type;

    for (type=0;type<MAX_NOTES;type++)
        load_thread(type);
}

void load_thread(int type) {
    int		notesread=0,notesfed=0;
    char	filename[MSL];

    DIR *	dirp;
    struct dirent *dp;

    if ((dirp=opendir(mud_data.notes_dir[type]))!=NULL) {
        while ((dp=readdir(dirp))!=NULL) {
            if (strstr(dp->d_name,".txt")!=NULL) {
                sprintf(filename,"%s/%s",mud_data.notes_dir[type],dp->d_name);
                if (load_note(type,filename))
                    notesread++;
                notesfed++;
            }
        }
        closedir(dirp);
    }
    logf("[0] NOTES: %s: Read %d, skipped %d ",
         notes_table[type].name,
         notesfed,
         notesfed-notesread);
}

bool load_note(int type,char *filename) {
    FILE *	fp;
    NOTE_DATA *	pnotelast=NULL;
    time_t	now=time(NULL);

    if ( ( fp = fopen( filename, "r" ) ) == NULL ) {
        logf("[0] load_thread(): fopen(%s): %s",filename,ERROR);
        bugf("NOTES: Cannot load note from '%s'",filename);
        return FALSE;
    }

    for ( ; ; )
    {
        NOTE_DATA *pnote;
        char letter;

        do
        {
            letter = getc( fp );
            if ( feof(fp) )
            {
                fclose( fp );
                return TRUE;
            }
        }
        while ( isspace(letter) );
        ungetc( letter, fp );

        pnote           = new_note( );

        if ( str_cmp( fread_word( fp ), "sender" ) )
            break;
        pnote->sender   = fread_string( fp );

        if ( str_cmp( fread_word( fp ), "date" ) )
            break;
        pnote->date     = fread_string( fp );

        if ( str_cmp( fread_word( fp ), "stamp" ) )
            break;
        pnote->date_stamp = fread_number(fp);

        if ( str_cmp( fread_word( fp ), "to" ) )
            break;
        pnote->to_list  = fread_string( fp );

        if ( str_cmp( fread_word( fp ), "subject" ) )
            break;
        pnote->subject  = fread_string( fp );

        if ( str_cmp( fread_word( fp ), "text" ) )
            break;
        pnote->text     = fread_string( fp );

        if (notes_table[type].ttl && pnote->date_stamp < now - notes_table[type].ttl) {
            note_remove(NULL,pnote,type,TRUE);
            free_note(pnote);
            fclose(fp);
            return FALSE;
        }

        pnote->type = type;

        if (notes_table[type].list==NULL)
            notes_table[type].list=pnote;
        else {
            NOTE_DATA *last=NULL;
            // ok, lets insert this note at the right place

            pnotelast=notes_table[type].list;
            while (pnotelast && (pnotelast->date_stamp<pnote->date_stamp)) {
                last=pnotelast;
                pnotelast=last->next;
            }
            if (last)
                last->next=pnote;
            else
                notes_table[type].list=pnote;
            pnote->next=pnotelast;
            pnotelast=pnote;
        }
    }

    bugf("load_notes(): bad key word in %s.", mud_data.notes_dir[type]);
    exit( 1 );
}

void write_note(int type,NOTE_DATA *pnote)
{
    FILE *	fp;
    NOTE_DATA *	last;
    char	filename[MSL];

    if (notes_table[type].list==NULL)
        notes_table[type].list=pnote;
    else
    {
        for (last=notes_table[type].list; last->next!=NULL; last=last->next) ;
        if (last->date_stamp>pnote->date_stamp)
            pnote->date_stamp=last->date_stamp+1;
        last->next=pnote;
    }

    fclose(fpReserve);

    sprintf(filename,"%s/%x.txt",mud_data.notes_dir[type],(int)pnote->date_stamp);

    if ( ( fp = fopen(filename, "w" ) ) == NULL ) {
        logf("[0] write_note(): fopen(%s): %s",filename,ERROR);
        bugf("NOTES: Couldn't write to %s",filename);
        fpReserve = fopen( NULL_FILE, "r" );
        return;
    }

    fprintf( fp, "Sender  %s~\n", pnote->sender);
    fprintf( fp, "Date    %s~\n", pnote->date);
    fprintf( fp, "Stamp   %d\n", (int)pnote->date_stamp);
    fprintf( fp, "To      %s~\n", pnote->to_list);
    fprintf( fp, "Subject %s~\n", pnote->subject);
    fprintf( fp, "Text\n%s~\n", pnote->text);
    fclose( fp );
    fpReserve = fopen( NULL_FILE, "r" );
}

#ifdef HAS_HTTP
void html_note(NOTE_DATA *pnote)
{
    char name[512],*prefix;
    FILE *fp;
    char *note;
    int size;
    int l1,l2;  // Vars to remember the lengths

    if(is_exact_name( "all", pnote->to_list ))
        prefix="html/all";
    else if(is_exact_name( "immortal", pnote->to_list ))
        prefix="html/immortal";
    else
        return;  // This not will not be made public

    switch(pnote->type)
    {
    default:
        return;
    case NOTE_NOTE:
        sprintf(name,"%s/%s.html",prefix,NOTE_DIR);
        break;
    case NOTE_IDEA:
        sprintf(name,"%s/%s.html",prefix,IDEA_DIR);
        break;
    case NOTE_PENALTY:
        sprintf(name,"%s/%s.html",prefix,PENALTY_DIR);
        break;
    case NOTE_NEWS:
        sprintf(name,"%s/%s.html",prefix,NEWS_DIR);
        break;
    case NOTE_CHANGES:
        sprintf(name,"%s/%s.html",prefix,CHANGES_DIR);
        break;
    case NOTE_BUG:
        sprintf(name,"%s/%s.html",prefix,BUG_DIR);
        break;
    case NOTE_TYPO:
        sprintf(name,"%s/%s.html",prefix,TYPO_DIR);
        break;
    }

    fclose(fpReserve);
    if ( ( fp = fopen(name, "a" ) ) == NULL ) {
        logf("[0] html_note(): fopen(%s): %s",name,ERROR);
        bugf("NOTES: Couldn't write htmlized note to '%s'",name);
        fpReserve = fopen( NULL_FILE, "r" );
        return;
    }

    fprintf( fp, "<table border=\"1\" cellpadding=\"0\" cellspacing=\"1\" width=\"100%%\" bgcolor=\"#C0C0C0\">\n");
    fprintf( fp, "<tr><td width=\"20%%\"><strong>From</strong></td>");
    fprintf( fp, "<td width=\"100%%\">%s</td></tr>\n",pnote->sender);
    fprintf( fp, "<tr><td><strong>To</strong></td><td>%s</td></tr>\n",pnote->to_list);
    fprintf( fp, "<tr><td><strong>Date</strong></td><td>%s</td></tr>\n",pnote->date);

    l1=ascii_to_html_length(pnote->subject,0);
    l2=ascii_to_html_length(pnote->text,1);

    size=UMAX(l1,l2);
    // An extra 512 bytes, just to be save.. (I shouldn't do this)
    size=512+size;

    note=(char *)calloc(1,size);
    {
        fprintf( fp, "<tr><td><strong>Subject</strong></td><td>%s</td></tr>\n",ascii_to_html(note,pnote->subject,0));
        fprintf( fp, "<td colspan=\"2\">%s</td></tr>\n",ascii_to_html(note,pnote->text,1));
    }
    free(note);

    fprintf( fp, "</table>\n\n<BR>\n\n");

    fclose( fp );
    fpReserve = fopen( NULL_FILE, "r" );
}
#endif


bool is_note_to( CHAR_DATA *ch, NOTE_DATA *pnote )
{
    if ( !str_cmp( ch->name, pnote->sender ) )
        return TRUE;

    if ( is_exact_name( "all", pnote->to_list ) )
        return TRUE;
    /*
 * I could remove these because of job-targets, but
 * lets keep them to make sure these work.
 */
    if ( IS_IMMORTAL(ch) && is_exact_name( "immortal", pnote->to_list ) )
        return TRUE;

    if ( IS_TRUSTED(ch,LEVEL_COUNCIL) && is_exact_name( "council", pnote->to_list ) )
        return TRUE;
    /*
 */
    if (ch->clan && is_exact_name(ch->clan->clan_info->name,pnote->to_list))
        return TRUE;

    if ( is_exact_name( ch->name, pnote->to_list ) )
        return TRUE;

    if ( char_has_jobtarget_in_list( ch, pnote->to_list) )
        return TRUE;

    return FALSE;
}



void note_attach( CHAR_DATA *ch, int type )
{
    NOTE_DATA *pnote;

    if ( ch->pnote != NULL )
        return;

    pnote = new_note();

    pnote->next		= NULL;
    pnote->sender	= str_dup( ch->name );
    pnote->date		= str_dup( "" );
    pnote->to_list	= str_dup( "" );
    pnote->subject	= str_dup( "" );
    pnote->text		= str_dup( "" );
    pnote->type		= type;
    ch->pnote		= pnote;
    return;
}


#ifdef HAS_POP3
void noteref_remove(NOTE_DATA *pnote)
{
    DESCRIPTOR_DATA *d;
    NOTE_REF *noteref;

    for(d=descriptor_list;d;d=d->next)
    {
        if(d->connected==CON_POP3_CONNECTED)
        {
            for(noteref=d->pop3_char->pcdata->noteref;noteref;noteref=noteref->next)
            {
                if(noteref->note==pnote)
                {
                    noteref->note=NULL;
                    noteref->deleted=TRUE;
                    break;
                }
            }
        }
    }
}
#endif

void note_remove( CHAR_DATA *ch, NOTE_DATA *pnote, int type, bool delete) {
    char	to_new[MIL];
    char	to_one[MIL];
    char	filename[MSL];
    NOTE_DATA *	prev;
    char *	to_list;

    if (!delete)
    {
        /* make a new list */
        to_new[0]	= '\0';
        to_list	= pnote->to_list;
        while ( *to_list != '\0' )
        {
            to_list	= one_argument( to_list, to_one );
            if ( to_one[0] != '\0' && str_cmp( ch->name, to_one ) )
            {
                strcat( to_new, " " );
                strcat( to_new, to_one );
            }
        }
        /* Just a simple recipient removal? */
        if ( str_cmp( ch->name, pnote->sender ) && to_new[0] != '\0' )
        {
            free_string( pnote->to_list );
            pnote->to_list = str_dup( to_new + 1 );
            return;
        }
    }

    //
    // Remove note from linked list, only if it is in the game
    //
    if (ch!=NULL) {
        if ( pnote == notes_table[type].list ) {
            notes_table[type].list = pnote->next;
        } else {
            for ( prev = notes_table[type].list; prev != NULL; prev = prev->next ) {
                if ( prev->next == pnote )
                    break;
            }

            if ( prev == NULL ) {
                bugf( "Note_remove: pnote not found, player is %s.",NAME(ch));
                return;
            }
            prev->next = pnote->next;
        }
    }

    sprintf(filename,"%s/%x.txt",mud_data.notes_dir[type],(int)pnote->date_stamp);
    if (unlink(filename)!=0) {
        logf("[0] note_remove(): unlink(%s): %s",filename,ERROR);
        bugf("NOTES: Cannot unlink note from '%s'",filename);
    }

#ifdef HAS_POP3
    noteref_remove(pnote);
#endif
    free_note(pnote);
    return;
}

bool hide_note (CHAR_DATA *ch, NOTE_DATA *pnote)
{
    time_t last_read;

    if (IS_NPC(ch))
        return TRUE;

    last_read = ch->pcdata->last_notes[pnote->type];

    if (pnote->date_stamp <= last_read)
        return TRUE;

    if (!str_cmp(ch->name,pnote->sender))
        return TRUE;

    if (!is_note_to(ch,pnote))
        return TRUE;

    return FALSE;
}

void update_read(CHAR_DATA *ch, NOTE_DATA *pnote) {
    if (IS_NPC(ch))
        return;

    ch->pcdata->last_notes[pnote->type]=
            UMAX(ch->pcdata->last_notes[pnote->type],pnote->date_stamp);
}

void note_announce(char *to,char *subject,int type,CHAR_DATA *ch)
{
    CHAR_DATA *player,*victim;
    NOTE_DATA tmpnote;
    char buf[MAX_STRING_LENGTH];

    tmpnote.to_list=to;
    tmpnote.sender=ch->name;
    sprintf_to_char(ch,
                    "You have posted %s to '%s'.\n\r",notes_table[type].description,to);

    sprintf(buf,"$N has posted %s to '%s' about '%s'.",
            notes_table[type].description,to,subject);
    for ( player = player_list; player != NULL; player = player->next_player )
    {
        victim=STR_IS_SET(player->strbit_act,PLR_SWITCHED)
                ? player->pcdata->switched_into : player;

        /* announce stuff */
        if (STR_IS_SET(victim->strbit_comm,COMM_ANNOUNCE)
                && !STR_IS_SET(victim->strbit_comm,COMM_NOCHANNELS)
                && !STR_IS_SET(victim->strbit_comm,COMM_QUIET)
                && can_see(victim,ch)
                && victim != ch
                && is_note_to(victim,&tmpnote))
        {
            act_new(buf,victim,NULL,ch,TO_CHAR,POS_DEAD);
        }

        /* wiznet stuff */
        if (IS_IMMORTAL(victim)
                && STR_IS_SET(victim->strbit_wiznet,WIZ_ON)
                && STR_IS_SET(victim->strbit_wiznet,WIZ_NOTE)
                && can_see(victim,ch)
                && victim != ch
                && is_note_to(victim,&tmpnote))
        {
            if (STR_IS_SET(victim->strbit_wiznet,WIZ_PREFIX))
                send_to_char("--> ",victim);
            act_new(buf,victim,NULL,ch,TO_CHAR,POS_DEAD);
        }
    }

    return;
}

bool check_receiptent(char *name) {
    CLAN_INFO *clan;

    if (strchr(name,'@'))
        return TRUE;
    if (!str_cmp(name,"all"))
        return TRUE;
    /*
 * even though we have job-targets, lets keep these in here.
 */
    if (!str_cmp(name,"immortal"))
        return TRUE;
    if (!str_cmp(name,"council"))
        return TRUE;
    if (!str_cmp(name,"implementor"))
        return TRUE;
    /*
 */
    if (((clan=get_clan_by_name(name))!=NULL)&&
            !str_cmp(name,clan->name))
        return TRUE;

    if (is_job_note_target(name))
        return TRUE;

    if (player_exist(name))
        return TRUE;

    return FALSE;
}

bool check_receiptents(CHAR_DATA *ch,char *dests) {
    char dest[MAX_INPUT_LENGTH];
    char *arg;

    arg=one_argument(dests,dest);
    while (dest[0]) {
        if (!check_receiptent(dest)) {
            sprintf_to_char(ch,"Recipient '%s' is unknown.\n\r",dest);
            return FALSE;
        }
        arg=one_argument(arg,dest);
    }
    return TRUE;
}

bool warning_about_note(CHAR_DATA *ch,int type) {
    if (ch->pnote==NULL)
        return FALSE;
    if (type==ch->pnote->type)
        return FALSE;

    sprintf_to_char(ch,
                    "You already have %s in progress.\n\r",
                    notes_table[ch->pnote->type].description);
    return TRUE;
}

NOTE_DATA *get_note_by_number(CHAR_DATA *ch,int type,int anum,int *vnum) {
    NOTE_DATA *pnote;

    *vnum=0;
    for ( pnote=notes_table[type].list; pnote != NULL; pnote = pnote->next )
        if ( is_note_to( ch, pnote ) && ( (*vnum)++ == anum ) )
            return pnote;
    return NULL;
}

void parse_note( CHAR_DATA *ch, char *argument, int type ) {
    BUFFER *buffer;
    char buf[MAX_STRING_LENGTH];
    char arg[MAX_INPUT_LENGTH];
    NOTE_DATA *pnote;
    int vnum;
    int anum;

    if (IS_NPC(ch)) return;

    argument = one_argument( argument, arg );
    smash_tilde( argument );

    if ( arg[0] == '\0' || !str_prefix( arg, "read" ) ) {
        if ( argument[0] == '\0' || !str_prefix(argument, "next"))
            /* read next unread note */
        {
            vnum = 0;
            for (pnote=notes_table[type].list;pnote!=NULL;pnote=pnote->next) {
                if (!hide_note(ch,pnote))
                {
                    sprintf_to_char( ch, "[%3d] %s: %s\n\r%s\n\rTo: %s\n\r",
                                     vnum,
                                     pnote->sender,
                                     pnote->subject,
                                     pnote->date,
                                     pnote->to_list);
                    page_to_char( pnote->text, ch );
                    update_read(ch,pnote);
                    return;
                }
                else if (is_note_to(ch,pnote))
                    vnum++;
            }
            sprintf_to_char(ch,
                            "You have no unread %s.\n\r",notes_table[type].multiple);
            return;
        }

        else if ( is_number( argument ) ) {
            anum = atoi( argument );
        } else {
            send_to_char( "Read which number?\n\r", ch );
            return;
        }

        vnum = 0;
        if ((pnote=get_note_by_number(ch,type,anum,&vnum))!=NULL) {
            sprintf_to_char( ch, "[%3d] %s: %s\n\r%s\n\rTo: %s\n\r",
                             vnum - 1,
                             pnote->sender,
                             pnote->subject,
                             pnote->date,
                             pnote->to_list
                             );
            page_to_char( pnote->text, ch );
            update_read(ch,pnote);
            return;
        }

        sprintf_to_char(ch,
                        "There aren't that many %s.\n\r",notes_table[type].multiple);
        return;
    }

    if ( !str_prefix( arg, "list" ) )
    {
        vnum = 0;
        argument=one_argument(argument,buf);
        if (buf[0]==0) {
            for ( pnote=notes_table[type].list; pnote!=NULL; pnote=pnote->next ) {
                if ( is_note_to( ch, pnote ) ) {
                    sprintf_to_char( ch, "[%3d%s] %s: %s\n\r",
                                     vnum, hide_note(ch,pnote) ? " " : "N",
                                     pnote->sender,pnote->subject);
                    vnum++;
                }
            }
        } else
            if (argument[0]==0) {
                sprintf_to_char(ch,"Usage: %s list [from|subject|text <search text>]\n\r",notes_table[type].single);
                return;
            } else if (!str_prefix(buf,"from")) {
                for ( pnote=notes_table[type].list; pnote!=NULL; pnote=pnote->next ) {
                    if ( is_note_to( ch, pnote ) ) {
                        if (!str_cmp(pnote->sender,argument))
                            sprintf_to_char( ch, "[%3d%s] %s: %s\n\r",
                                             vnum, hide_note(ch,pnote) ? " " : "N",
                                             pnote->sender,pnote->subject);
                        vnum++;
                    }
                }
            } else if (!str_prefix(buf,"subject")) {
                for ( pnote=notes_table[type].list; pnote!=NULL; pnote=pnote->next ) {
                    if ( is_note_to( ch, pnote ) ) {
                        if (!str_infix_nocolor(argument,pnote->subject))
                            sprintf_to_char( ch, "[%3d%s] %s: %s\n\r",
                                             vnum, hide_note(ch,pnote) ? " " : "N",
                                             pnote->sender,pnote->subject);
                        vnum++;
                    }
                }
            } else if (!str_prefix(buf,"text")) {
                for ( pnote=notes_table[type].list; pnote!=NULL; pnote=pnote->next ) {
                    if ( is_note_to( ch, pnote ) ) {
                        if (!str_infix_nocolor(argument,pnote->text))
                            sprintf_to_char( ch, "[%3d%s] %s: %s\n\r",
                                             vnum, hide_note(ch,pnote) ? " " : "N",
                                             pnote->sender,pnote->subject);
                        vnum++;
                    }
                }
            } else {
                sprintf_to_char(ch,"Unknown search type.\n\r");
                sprintf_to_char(ch,"Usage: %s list [from|subject|text <search text>]\n\r",notes_table[type].single);
                return;
            }
        if (!vnum)
            sprintf_to_char(ch,
                            "There are no %s for you.\n\r",notes_table[type].multiple);
        return;
    }

    if ( !str_prefix( arg, "unread" ) ) {
        vnum = 0;
        for ( pnote=notes_table[type].list; pnote!=NULL; pnote=pnote->next ) {
            if ( is_note_to( ch, pnote ) ) {
                if (!hide_note(ch,pnote)) {
                    sprintf_to_char( ch, "[%3d%s] %s: %s\n\r",
                                     vnum, hide_note(ch,pnote) ? " " : "N",
                                     pnote->sender,pnote->subject);
                }
                vnum++;
            }
        }
        return;
    }

    if ( !str_prefix( arg, "remove" ) )
    {
        if ( !is_number( argument ) )
        {
            send_to_char( "Note remove which number?\n\r", ch );
            return;
        }

        anum = atoi( argument );
        vnum = 0;
        if ((pnote=get_note_by_number(ch,type,anum,&vnum))!=NULL) {
            note_remove( ch, pnote, type, FALSE );
            send_to_char( "Ok.\n\r", ch );
            return;
        }

        sprintf_to_char(ch,
                        "There aren't that many %s.\n\r",notes_table[type].multiple);
        return;
    }

    if ( !str_prefix( arg, "delete" ) && get_trust(ch) >= MAX_LEVEL3)
    {
        if ( !is_number( argument ) )
        {
            send_to_char( "Note delete which number?\n\r", ch );
            return;
        }

        anum = atoi( argument );
        vnum = 0;
        if ((pnote=get_note_by_number(ch,type,anum,&vnum))!=NULL) {
            note_remove( ch, pnote,type, TRUE );
            send_to_char( "Ok.\n\r", ch );
            return;
        }

        sprintf_to_char(ch,
                        "There aren't that many %s.\n\r",notes_table[type].multiple);
        return;
    }

    if (!str_prefix(arg,"catchup")) {
        ch->pcdata->last_notes[type]=current_time;
        return;
    }

    /* below this point only certain people can edit notes */
    if (notes_table[type].min_level!=0)
        if (!IS_TRUSTED(ch,notes_table[type].min_level)) {
            sprintf_to_char(ch,
                            "You aren't high enough level to write %s.\n\r",
                            notes_table[type].multiple);
            return;
        }

    if (STR_IS_SET(ch->strbit_comm,COMM_NONOTE)) {
        send_to_char("You are not allowed to create any kind of note.\n\r",ch);
        return;
    }

    if (notes_table[type].posting_jobs!=NULL) {
        char *pj,*job,*next;
        bool allowed=FALSE;

        pj=str_dup(notes_table[type].posting_jobs);

        for (next=pj;(job=strsep(&next," "))!=NULL && !allowed;) {
            if (char_has_job(ch,job)) allowed=TRUE;
        }

        free_string(pj);
        if (!allowed) {
            sprintf_to_char(ch,
                            "You aren't allowed to write %s.\n\r",
                            notes_table[type].multiple);
            return;
        }
    }

#ifdef IDEA_BUG_VIA_WEB
    if ((type==NOTE_IDEA) || (type==NOTE_BUG)) {
        send_to_char("Ideas and bugs should be submitted via the website.\n\r",ch);
        return;
    }
#endif

#ifdef later
    if ((type == NOTE_NOTE && !IS_TRUSTED(ch,mud_data.newbie_note))
            || (type == NOTE_IDEA && !IS_TRUSTED(ch,mud_data.newbie_idea))
            || (type == NOTE_BUG  && !IS_TRUSTED(ch,mud_data.newbie_bug ))
            || (type == NOTE_TYPO && !IS_TRUSTED(ch,mud_data.newbie_typo))) {
        sprintf_to_char(ch,"To use these boards, you should be:\n\r");
        sprintf_to_char(ch,"    Note: level %d\n\r",mud_data.newbie_note);
        sprintf_to_char(ch,"    Bug : level %d\n\r",mud_data.newbie_bug);
        sprintf_to_char(ch,"    Idea: level %d\n\r",mud_data.newbie_idea);
        sprintf_to_char(ch,"    Typo: level %d\n\r",mud_data.newbie_typo);
        return;
    }
#endif

    if ( !str_prefix( arg, "subject" ) )
    {
        note_attach( ch,type );
        if (warning_about_note(ch,type))
            return;

        free_string( ch->pnote->subject );
        ch->pnote->subject = str_dup( argument );
        send_to_char( "Ok.\n\r", ch );
        return;
    }

    if ( !str_prefix( arg, "to" ) )
    {
        note_attach( ch,type );
        if (warning_about_note(ch,type))
            return;

        if (!str_cmp(argument,"")) {
            send_to_char("To who do you want to write?\n\r",ch);
            return;
        }
        if (!check_receiptents(ch,argument))
            return;
        free_string( ch->pnote->to_list );
        ch->pnote->to_list = str_dup( argument );
        send_to_char( "Ok.\n\r", ch );
        return;
    }

    if ( !str_prefix( arg, "from" ) && get_trust(ch)>=LEVEL_COUNCIL) {
        note_attach( ch,type );
        if (warning_about_note(ch,type))
            return;

        if (!str_cmp(argument,"")) {
            send_to_char("Who do you want to impersonate?\n\r",ch);
            return;
        }
        free_string( ch->pnote->sender );
        ch->pnote->sender = str_dup( argument );
        send_to_char( "Ok.\n\r", ch );
        return;
    }

    if (!str_prefix( arg, "copy" ) ) {

        if (!is_number(argument)) {
            send_to_char("Syntax: note copy <number>.\n\r",ch);
            return;
        }

        anum=atoi( argument );

        note_attach( ch,type );
        if (warning_about_note(ch,type))
            return;

        vnum=0;
        if ((pnote=get_note_by_number(ch,type,anum,&vnum))!=NULL) {
            buffer = new_buf();
            add_buf(buffer,ch->pnote->text);

            sprintf( buf, "%s: %s\n\r%s\n\rTo: %s\n\r",
                     pnote->sender,
                     pnote->subject,
                     pnote->date,
                     pnote->to_list);
            add_buf(buffer,buf);
            add_buf(buffer,"\n\r");
            add_buf(buffer,pnote->text);
            free_string( ch->pnote->text );
            ch->pnote->text = str_dup( buf_string(buffer) );
            free_buf(buffer);

            send_to_char("Ok.\n\r",ch);

            return;
        }
        send_to_char("No such note.\n\r",ch);
        return;
    }

    if ( !str_prefix( arg, "clear" ) )
    {
        if (warning_about_note(ch,type))
            return;

        if ( ch->pnote != NULL ) {
            sprintf_to_char(ch,
                            "%s cleared.\n\r",
                            notes_table[ch->pnote->type].description);
            free_note(ch->pnote);
            ch->pnote = NULL;
        }

        return;
    }

    if ( !str_prefix( arg, "show" ) )
    {
        if ( ch->pnote == NULL )
        {
            send_to_char( "You have no note in progress.\n\r", ch );
            return;
        }

        if (warning_about_note(ch,type))
            return;

        sprintf_to_char( ch, "%s: %s\n\rTo: %s\n\r",
                         ch->pnote->sender,
                         ch->pnote->subject,
                         ch->pnote->to_list
                         );
        send_to_char( ch->pnote->text, ch );
        return;
    }

    if ( !str_prefix( arg, "post" ) || !str_prefix(arg, "send"))
    {
        char *strtime;

        if ( ch->pnote == NULL )
        {
            send_to_char( "You have no note in progress.\n\r", ch );
            return;
        }

        if (warning_about_note(ch,type))
            return;

        if (!str_cmp(ch->pnote->to_list,""))
        {
            send_to_char(
                        "You need to provide a recipient (name, all, clanname, immortal, council or implementor).\n\r",
                        ch);
            return;
        }

        if (!str_cmp(ch->pnote->subject,""))
        {
            send_to_char("You need to provide a subject.\n\r",ch);
            return;
        }

        ch->pnote->next			= NULL;
        strtime				= ctime( &current_time );
        strtime[strlen(strtime)-1]	= '\0';
        ch->pnote->date			= str_dup( strtime );
        ch->pnote->date_stamp		= current_time;

        note_announce(ch->pnote->to_list,ch->pnote->subject,type,ch);

        write_note(type,ch->pnote);
#ifdef HAS_HTTP
        html_note(ch->pnote);  // Write the note to an html file
#endif

        ch->pnote = NULL;
        return;
    }

    if ( !str_cmp( arg, "edit" ) )
    {
        note_attach( ch,type );
        if (warning_about_note(ch,type))
            return;

        editor_start(ch,&ch->pnote->text);
        return;
    }

    if ( ! str_cmp( arg, "spool" ) )
    {
        if ( ch->pnote == NULL )
        {
            send_to_char("You have no note in progress.\n\r",ch);
            return;
        }

        if (warning_about_note(ch,type))
            return;

        send_to_char("Type of note changes.\n\r",ch);
        ch->pnote->type=type;
        return;
    }

    send_to_char( "You can't do that.\n\r", ch );
    return;
}


//
// Polls
//
#define MAXPOLLS	7
void do_poll(CHAR_DATA *ch, char *argument)
{
    char arg1[MAX_STRING_LENGTH],arg2[MAX_STRING_LENGTH];
    char buf[2*MAX_STRING_LENGTH],buf2[MAX_STRING_LENGTH];
    int i,p=-1,score,k;
    BUFFER *buffer;
    NOTE_DATA *pnote;
    char *strtime;

    if (IS_NPC(ch)) {
        if (ch->master)
            send_to_char("You cheater!\n\r",ch->master);
        return;
    }

    if (argument[0] == '\0' ) {
        char status[14];
        buffer = new_buf();

        if (mud_data.poll_open) {
            char p;
            GetCharProperty(ch,PROPERTY_INT,"lastpollid",&p);
            if (mud_data.poll_id == p) {
                strcpy(status,"already voted");
            } else {
                strcpy(status,"open");
            }
        } else {
            strcpy(status,"closed");
        }


        sprintf(buf,"%s\t[%s]\n\r\n\r", mud_data.poll_question,status);
        add_buf(buffer,buf);
        for (i = 0 ; i < MAXPOLLS ; i++) {
            k = 0;

            if (!mud_data.poll_choiceact[i] )
                continue;

            sprintf(buf,"%d. %s\n\r",i+1,mud_data.poll_choice[i]);
            add_buf(buffer,buf);
            if (mud_data.poll_totalvotes == 0)
                sprintf(buf,"[                    ] 0\n\r\n\r");
            else {
                score =
                        (100*mud_data.poll_teller[i])/mud_data.poll_totalvotes;
                buf2[k] = '[';
                for ( k = 1 ; k < 21 ; k++ ) {
                    if ( (score -= 5) >= 0)
                        buf2[k] = '*';
                    else
                        buf2[k] = ' ';
                }
                buf2[k] = ']';
                buf2[k+1] = '\0';
                sprintf(buf,"%s %d\n\r\n\r",buf2,mud_data.poll_teller[i]);
            }
            add_buf(buffer,buf);
        }
        page_to_char(buf_string(buffer),ch);
        free_buf(buffer);
        return;
    }

    argument = one_argument(argument,arg1);

    switch(which_keyword(arg1,"open","vote","reset","set","question","note","done",NULL)) {
    case -1:
    case 0:
        if (IS_IMMORTAL(ch))
            send_to_char("Invalid argument, valid arguments are : "
                         "set/open/vote/reset/question/note/done.\n\r",ch);
        else
            send_to_char("Invalid argument, valid argument is : vote/done.\n\r",ch);
        return;
    case 1:
        if (!IS_IMMORTAL(ch)) {
            send_to_char("You are a too low level to open/close polls.\n\r",ch);
            return;
        }

        if (mud_data.poll_open == FALSE) {
            send_to_char("The poll is now opened.\n\r",ch);
            mud_data.poll_open = TRUE;
        }
        else {
            send_to_char("The poll is now closed.\n\r",ch);
            mud_data.poll_open = FALSE;
        }
        save_poll();
        return;
    case 2:
        if (ch->level<mud_data.poll_level) {
            sprintf_to_char(ch,"You should be at least level %d before "\
                               "you can take part in the polls.\n\r",
                            mud_data.poll_level);
            return;
        }
        argument = one_argument(argument,arg2);
        i = atoi(arg2);
        i -= 1;

        if (i < 0 || i > 6 || !mud_data.poll_choiceact[i]) {
            send_to_char("Invalid poll number.\n\r",ch);
            return;
        }

        if (mud_data.poll_open == FALSE) {
            send_to_char("The poll is closed.\n\r",ch);
            return;
        }

        GetCharProperty(ch,PROPERTY_INT,"lastpollid",&p);
        if (mud_data.poll_id == p) {
            send_to_char("You have already voted for this poll.\n\r",ch);
            return;
        }

        send_to_char("Thank you for voting!\n\r",ch);
        SetCharProperty(ch,PROPERTY_INT,"lastpollid",&mud_data.poll_id);
        mud_data.poll_teller[i]++;
        mud_data.poll_totalvotes++;
        do_function(ch,&do_poll,"");
        save_poll();
        return;

    case 3:
        if (!IS_IMMORTAL(ch)) {
            send_to_char("You are a too low level to reset polls.\n\r",ch);
            return;
        }
        mud_data.poll_id++;
        strcpy(mud_data.poll_question,"No current Poll");
        mud_data.poll_open = FALSE;
        mud_data.poll_totalvotes = 0;
        for (i = 0 ; i < MAXPOLLS ; i++) {
            mud_data.poll_choiceact[i] = FALSE;
            strcpy(mud_data.poll_choice[i]," ");
            mud_data.poll_teller[i] = 0;
        }
        send_to_char("Poll reseted.\n\r",ch);
        save_poll();
        return;

    case 4:
        if (!IS_IMMORTAL(ch)) {
            send_to_char("You are a too low level to set polls.\n\r",ch);
            return;
        }

        argument = one_argument(argument,arg2);
        i = atoi(arg2);
        i -= 1;

        if (i < 0 || i > 6) {
            send_to_char("Invalid question number.\n\r",ch);
            send_to_char("Syntax : poll set <poll number> <answer>.\n\r",ch);
            return;
        }

        if (argument[0] == '\0') {
            send_to_char("No answer given.\n\r",ch);
            return;
        }

        strcpy(mud_data.poll_choice[i],argument);
        mud_data.poll_choiceact[i] = TRUE;
        send_to_char("Choice set.\n\r",ch);
        save_poll();
        return;

    case 5:
        if (!IS_IMMORTAL(ch)) {
            send_to_char("You are a too low level to set the question.\n\r",ch);
            return;
        }
        if (argument[0] == '\0') {
            send_to_char("No question given.\n\r",ch);
            return;
        }
        strcpy(mud_data.poll_question,argument);
        send_to_char("Question set.\n\r",ch);
        save_poll();
        return;

    case 6:
        if (!IS_IMMORTAL(ch)) {
            send_to_char("You are a too low level to note the poll.\n\r",ch);
            return;
        }
        pnote                       = new_note( );
        strtime			= ctime( &current_time );
        strtime[strlen(strtime)-1]	= '\0';
        pnote->date			= str_dup( strtime );
        pnote->date_stamp		= current_time;
        pnote->to_list              = str_dup( "all" );
        pnote->type                 = NOTE_NOTE;
        pnote->sender               = str_dup( capitalize(ch->name) );
        pnote->valid                = TRUE;
        sprintf(buf,"Poll - %s : %s",pnote->date ,mud_data.poll_question);
        pnote->subject              = str_dup( buf );

        buffer = new_buf();
        sprintf(buf,"%s\n\r\n\r", mud_data.poll_question);
        add_buf(buffer,buf);
        for (i = 0 ; i < MAXPOLLS ; i++) {
            k = 0;

            if (!mud_data.poll_choiceact[i] )
                continue;

            sprintf(buf,"%d. %s\n\r",i+1,mud_data.poll_choice[i]);
            add_buf(buffer,buf);
            if (mud_data.poll_totalvotes == 0)
                sprintf(buf,"[                    ] 0\n\r\n\r");
            else {
                score =
                        (100*mud_data.poll_teller[i])/mud_data.poll_totalvotes;
                buf2[k] = '[';
                for ( k = 1 ; k < 21 ; k++ ) {
                    if ( (score -= 5) >= 0)
                        buf2[k] = '*';
                    else
                        buf2[k] = ' ';
                }
                buf2[k] = ']';
                buf2[k+1] = '\0';
                sprintf(buf,"%s %d\n\r\n\r",buf2,mud_data.poll_teller[i]);
            }
            add_buf(buffer,buf);
        }
        pnote->text = str_dup(buf_string(buffer));
        note_announce(pnote->to_list,pnote->subject,pnote->type,ch);
        write_note(NOTE_NOTE,pnote);
#ifdef HAS_HTTP
        html_note(pnote);
#endif
        free_buf(buffer);
        return;

    case 7:
        GetCharProperty(ch,PROPERTY_INT,"lastpollid",&p);

        if (mud_data.poll_id == p) {
            send_to_char("You have already voted for this poll.\n\r",ch);
            return;
        }

        send_to_char("You rejected your vote!\n\r",ch);
        SetCharProperty(ch,PROPERTY_INT,"lastpollid",&mud_data.poll_id);
        return;
    }
}

void save_poll(void) {
    int i;
    FILE *fp;

    logf("[0] POLL: Saving poll");
    if ( ( fp = fopen(mud_data.poll_file,"w")) == NULL) {
        logf("[0] save_poll(): fopen(%s): %s",mud_data.poll_file,ERROR);
        bugf("POLL: Can't open file '%s'",mud_data.poll_file);
        return;
    }

    fprintf(fp,"%d\n%d\n%d\n",
            mud_data.poll_totalvotes,mud_data.poll_open,mud_data.poll_id);
    fprintf(fp,"%s~\n",mud_data.poll_question);
    for ( i = 0 ; i < MAXPOLLS ; i++ ) {
        fprintf(fp,"%s~\n",mud_data.poll_choice[i]);
        fprintf(fp,"%d\n%d\n",
                mud_data.poll_choiceact[i],mud_data.poll_teller[i]);
    }

    fclose(fp);
    return;
}

void load_poll(void)
{
    int i;
    FILE *fp;

    logf("[0] POLL: Loading poll");
    if ( ( fp = fopen(mud_data.poll_file,"r")) == NULL) {
        logf("[0] load_poll(): fopen(%s): %s",mud_data.poll_file,ERROR);
        bugf("POLL: Can't open file '%s'",mud_data.poll_file);
        return;
    }
    strcpy(mud_data.poll_question,"No Current Poll");
    mud_data.poll_totalvotes=0;

    mud_data.poll_totalvotes = fread_number( fp );
    mud_data.poll_open = fread_number( fp );
    mud_data.poll_id = fread_number( fp );
    strcpy(mud_data.poll_question,fread_string( fp ));

    for ( i = 0 ; i < MAXPOLLS ; i++ ) {
        strcpy(mud_data.poll_choice[i],fread_string( fp ));
        mud_data.poll_choiceact[i] = fread_number( fp );
        mud_data.poll_teller[i] = fread_number( fp );
    }

    fclose(fp);
    return;
}


//
// Notebooks
//
void do_notebook(CHAR_DATA *ch , char *argument)
{
    char arg[MAX_INPUT_LENGTH],arg1[MSL],arg2[MSL];
    char buf[MAX_STRING_LENGTH];
    NOTEBK_DATA *pnotebook,*pnotebook2,*pnotebook1,*pnotebookend;
    CHAR_DATA *victim;
    BUFFER *buffer;
    int number,num,num1,num2;
    int argi;

    if (IS_NPC(ch)) {
        send_to_char("Learn to read first!\n\r",ch);
        return;
    }

    argument = one_argument( argument, arg );

    switch (which_keyword(arg,"list","read","subject","edit","erase",
                          "give","move","nogiveaway","copy",NULL)) {
    case 0: // error
    default:
        send_to_char("Usage: notebook <command>\n\r",ch);
        send_to_char("where command is one of the following:\n\r",ch);
        send_to_char("list, read, subject, edit, erase, give, move,\n\r",ch);
        send_to_char("nogiveaway, copy\n\r",ch);
        return;

    case -1: // default
    case 1: // list
        if ( ch->pcdata->notebook == NULL) {
            send_to_char("Your notebook is still empty.\n\r",ch);
        } else {
            buffer = new_buf();
            number=0;
            for (pnotebook=ch->pcdata->notebook ; pnotebook != NULL ; pnotebook = pnotebook->next) {
                sprintf(buf,"[%3d] %c %s\n\r",++number,
                        IS_SET(pnotebook->flags,NOTEBOOK_NOGIVE)?'*':' ',
                        pnotebook->subject);
                add_buf(buffer,buf);
            }
            page_to_char(buf_string(buffer),ch);
            free_buf(buffer);
        }
        return;

    case 2: // read
        argument = one_argument(argument,arg);
        if (!is_number(arg)) {
            send_to_char("Which page do you want to read?\n\r",ch);
            return;
        }
        argi=atoi(arg);

        number=0;
        for (pnotebook=ch->pcdata->notebook ; pnotebook != NULL ; pnotebook = pnotebook->next) {
            if (++number == argi) {
                buffer = new_buf();
                sprintf(buf,"[%3d] %s\n\r",number,pnotebook->subject);
                add_buf(buffer,buf);
                sprintf(buf,"Page created      : %s\r",ctime(&pnotebook->created));
                add_buf(buffer,buf);
                sprintf(buf,"Page last modified: %s\r",ctime(&pnotebook->modified));
                add_buf(buffer,buf);
                if (IS_SET(pnotebook->flags,NOTEBOOK_NOGIVE)) {
                    sprintf(buf,"Page remark       : do not give away.\n\r");
                    add_buf(buffer,buf);
                }
                add_buf(buffer,pnotebook->text);
                page_to_char(buf_string(buffer),ch);
                free_buf(buffer);
                return;
            }
        }

        sprintf_to_char(ch,"Page %d not found.\n\r",argi);
        return;

    case 3: // subject
        pnotebook=new_notebook();
        pnotebook->created=time(NULL);
        pnotebook->modified=time(NULL);
        pnotebook->next=NULL;
        smash_tilde(argument);
        pnotebook->subject = str_dup(argument);

        // add it to the end
        pnotebookend=ch->pcdata->notebook;
        if (pnotebookend==NULL) {
            ch->pcdata->notebook=pnotebook;
        } else {
            while (pnotebookend->next!=NULL)
                pnotebookend=pnotebookend->next;
            pnotebookend->next=pnotebook;
        }

        return;

    case 4: // edit
        if (!is_number(argument)) {
            send_to_char("Edit which page?\n\r",ch);
            return;
        }
        number=atoi(argument);

        num=0;
        for (pnotebook=ch->pcdata->notebook ; pnotebook!=NULL;pnotebook=pnotebook->next)
            if (++num==number)
                break;
        if (pnotebook==NULL) {
            send_to_char("That page doesn't exist.\n\r",ch);
            return;
        }

        pnotebook->modified=time(NULL);
        editor_start(ch,&pnotebook->text);
        return;

    case 5: // erase
        if (!is_number(argument)) {
            send_to_char("Erase which page?\n\r",ch);
            return;
        }
        number=atoi(argument);

        num=0;
        for (pnotebook=ch->pcdata->notebook ; pnotebook != NULL;pnotebook=pnotebook->next)
            if (++num==number)
                break;
        if (pnotebook==NULL) {
            send_to_char("That page doesn't exist.\n\r",ch);
            return;
        }

        if (pnotebook==ch->pcdata->notebook) {
            pnotebook2=ch->pcdata->notebook;
            ch->pcdata->notebook=pnotebook2->next;
            free_notebook(pnotebook2);
        } else {
            for (pnotebook2=ch->pcdata->notebook ; pnotebook2 != NULL;pnotebook2=pnotebook2->next)
                if (pnotebook2->next==pnotebook)
                    break;
            pnotebook2->next=pnotebook->next;
            free_notebook(pnotebook);
        }
        send_to_char("Page removed.\n\r",ch);
        return;

    case 6: // give
        argument = one_argument(argument,arg);
        if (!is_number(arg)) {
            send_to_char("Give away which page?\n\r",ch);
            return;
        }
        number=atoi(arg);
        num=0;
        for (pnotebook=ch->pcdata->notebook ; pnotebook != NULL;pnotebook=pnotebook->next)
            if (++num==number)
                break;
        if (pnotebook==NULL) {
            send_to_char("That page doesn't exist.\n\r",ch);
            return;
        }

        argument = one_argument(argument,arg);
        if ( ( victim = get_char_room( ch, arg) ) == NULL
             || IS_NPC(victim) ) {
            send_to_char("That person isn't here.\n\r",ch);
            return;
        }

        if (victim==ch) {
            send_to_char("You already have that page.\n\r",ch);
            return;
        }

        if (IS_SET(pnotebook->flags,NOTEBOOK_NOGIVE)) {
            send_to_char("The page has a stamp on it: for your eyes only.\n\r",ch);
            return;
        }

        pnotebook2 = new_notebook();
        pnotebook2->created=time(NULL);
        pnotebook2->modified=time(NULL);

        // add it to the end
        pnotebookend=victim->pcdata->notebook;
        if (pnotebookend==NULL) {
            victim->pcdata->notebook=pnotebook2;
        } else {
            while (pnotebookend->next!=NULL)
                pnotebookend=pnotebookend->next;
            pnotebookend->next=pnotebook2;
        }

        pnotebook2->subject = str_dup(pnotebook->subject);
        pnotebook2->text = str_dup(pnotebook->text);
        sprintf_to_char(victim,
                        "You have gotten a notebook page from %s about: %s.\n\r",
                        capitalize(ch->name),pnotebook2->subject);
        sprintf_to_char(ch,
                        "You give a notebook page to %s about: %s.\n\r",
                        capitalize(victim->name),pnotebook2->subject);
        return;

    case 7: // move
        argument=one_argument(argument,arg1);
        argument=one_argument(argument,arg2);

        if (!is_number(arg1) || !is_number(arg2)) {
            send_to_char("Move which page to which location?\n\r",ch);
            return;
        }

        num1=atoi(arg1);num2=atoi(arg2);

        if (num1==num2) {
            act("You shuffle foolishly in your notebook.",ch,NULL,NULL,TO_CHAR);
            act("$n shuffles in $s notebook, trying to impress you.",ch,NULL,NULL,TO_ROOM);
            return;
        }

        num=0;
        for (pnotebook1=ch->pcdata->notebook ; pnotebook1 != NULL;pnotebook1=pnotebook1->next)
            if (++num==num1)
                break;
        if (pnotebook1==NULL) {
            send_to_char("The first page doesn't exist.\n\r",ch);
            return;
        }
        num=0;
        for (pnotebook2=ch->pcdata->notebook ; pnotebook2 != NULL;pnotebook2=pnotebook2->next)
            if (++num==num2)
                break;
        if (pnotebook2==NULL) {
            send_to_char("The second page doesn't exist.\n\r",ch);
            return;
        }

        // remove the old page first
        if (ch->pcdata->notebook==pnotebook1)
            ch->pcdata->notebook=pnotebook1->next;
        else {
            for (pnotebook=ch->pcdata->notebook;pnotebook;pnotebook=pnotebook->next)
                if (pnotebook->next==pnotebook1)
                    break;
            pnotebook->next=pnotebook1->next;
        }

        // add it after the new one
        pnotebook1->next=pnotebook2->next;
        pnotebook2->next=pnotebook1;
        sprintf_to_char(ch,"You have placed page %d behind %d.\n\r",num1,num2);
        return;

    case 8: // nogiveaway
        argument=one_argument(argument,arg1);
        if (!is_number(arg1)) {
            send_to_char("Mark which page?\n\r",ch);
            return;
        }

        num1=atoi(arg1);
        num=0;
        for (pnotebook1=ch->pcdata->notebook ; pnotebook1 != NULL;pnotebook1=pnotebook1->next)
            if (++num==num1)
                break;
        if (pnotebook1==NULL) {
            sprintf_to_char(ch,"You can't find page %d.\n\r",num1);
            return;
        }

        if (IS_SET(pnotebook1->flags,NOTEBOOK_NOGIVE)) {
            REMOVE_BIT(pnotebook1->flags,NOTEBOOK_NOGIVE);
            sprintf_to_char(ch,"You have removed the stamp on page %d.\n\r",num1);
        } else {
            SET_BIT(pnotebook1->flags,NOTEBOOK_NOGIVE);
            sprintf_to_char(ch,"You have placed a stamp on page %d.\n\r",num1);
        }
        return;

    case 9: // copy <type> <number>
    {
        NOTE_DATA *	pnote;
        int		anum,vnum;
        int		type;


        if (argument[0]==0) {
            send_to_char("Usage: notebook copy <type> <number>\n\r",ch);
            send_to_char("<type> can be note, idea, bug and so on.\n\r",ch);
            return;
        }

        // notes
        argument=one_argument(argument,arg1);
        argument=one_argument(argument,arg2);
        if (arg1[0]==0) {
            send_to_char(
                        "Invalid type, should be note, idea, bug and so on.\n\r",ch);
            return;
        }
        if (!is_number(arg2)) {
            send_to_char("Invalid number.\n\r",ch);
            return;
        }
        anum=atoi(arg2);

        for (type=0;type<MAX_NOTES;type++)
            if (!str_cmp(arg1,notes_table[type].name))
                break;

        if (type==MAX_NOTES) {
            send_to_char(
                        "Invalid type, should be note, idea, bug and so on.\n\r",ch);
            return;
        }

        if ((pnote=get_note_by_number(ch,type,anum,&vnum))==NULL) {
            send_to_char("There are not so many notes.\n\r",ch);
            return;
        }

        // new notebook
        pnotebook=new_notebook();
        pnotebook->created=time(NULL);
        pnotebook->modified=time(NULL);
        pnotebook->next=NULL;
        pnotebook->subject = str_dup(pnote->subject);
        pnotebook->text = str_dup(pnote->text);

        // add it to the end
        pnotebookend=ch->pcdata->notebook;
        if (pnotebookend==NULL) {
            ch->pcdata->notebook=pnotebook;
        } else {
            while (pnotebookend->next!=NULL)
                pnotebookend=pnotebookend->next;
            pnotebookend->next=pnotebook;
        }

        send_to_char("Message copied into your notebook.\n\r",ch);
        return;

    }
    }
}
