//
// $Id: fd_penalty.c,v 1.12 2006/09/19 17:50:53 verlag Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

// stolen from <math.h> because of double declarations.
extern double ceil(double);

#include "merc.h"
#include "interp.h"

void do_pardon( CHAR_DATA *ch,char *argument) {
    char	arg1[MIL];
    char	arg2[MIL];
    CHAR_DATA *	victim;

    argument=one_argument(argument,arg1);
    argument=one_argument(argument,arg2);

    if ( arg1[0]==0 || arg2[0]==0) {
	send_to_char("Syntax: pardon <character> <killer|thief|swear>.\n\r",ch);
	return;
    }

    if ((victim=get_char_world(ch,arg1))==NULL) {
	send_to_char("They aren't here.\n\r",ch);
	return;
    }

    if (IS_NPC(victim)) {
	send_to_char("Not on NPC's.\n\r",ch);
	return;
    }

    switch (which_keyword(arg2,"killer","thief","swear",NULL)) {
	case -1:
	case 0:
	    do_function(ch,&do_pardon,"");
	    return;

	case 1:	// killer
	    if (STR_IS_SET(victim->strbit_act,PLR_KILLER)) {
		STR_REMOVE_BIT( victim->strbit_act, PLR_KILLER );
		send_to_char("Killer flag removed.\n\r",ch);
		send_to_char("You are no longer a KILLER.\n\r",victim);
	    }
	    return;

	case 2:	// thief
	    if (STR_IS_SET(victim->strbit_act,PLR_THIEF)) {
		STR_REMOVE_BIT(victim->strbit_act,PLR_THIEF);
		send_to_char("Thief flag removed.\n\r",ch);
		send_to_char("You are no longer a THIEF.\n\r",victim);
	    }
	    return;

	case 3: // swear
	    if (victim->pcdata->swearing!=0) {
		victim->pcdata->swearing=0;
		send_to_char("Swearing count resetted.\n\r",ch);
		send_to_char("Your swearing count is resetted.\n\r",victim);
		if (STR_IS_SET(victim->strbit_comm, COMM_NOCHANNELS))
		    do_function(ch,&do_nochannels,victim->name);
	    }
	    return;
    }
}


void do_nochannels( CHAR_DATA *ch, char *argument ) {
    char	arg[MIL];
    CHAR_DATA *	victim;

    one_argument( argument, arg );

    if (arg[0]==0) {
	send_to_char("Nochannel whom?\n\r",ch);
	return;
    }

    if ((victim=get_char_world(ch,arg))==NULL) {
	send_to_char("They aren't here.\n\r",ch);
	return;
    }

    if (get_trust(ch)<LEVEL_COUNCIL && get_trust(victim)>=get_trust(ch)) {
	send_to_char("You failed.\n\r",ch);
	return;
    }

    if (STR_IS_SET(victim->strbit_comm,COMM_NOCHANNELS)) {
	STR_REMOVE_BIT(victim->strbit_comm, COMM_NOCHANNELS);
	send_to_char("The Gods have restored your channel privileges.\n\r",
		      victim );
	send_to_char("NOCHANNELS removed.\n\r",ch);
	wiznet(WIZ_PENALTIES,0,ch,NULL,
	    "$N restores channels to %s",victim->name);
    } else {
	STR_SET_BIT(victim->strbit_comm,COMM_NOCHANNELS);
	send_to_char("The gods have revoked your channel privileges.\n\r",
		       victim );
	send_to_char("NOCHANNELS set.\n\r",ch);
	wiznet(WIZ_PENALTIES,0,ch,NULL,
	    "$N revokes %s's channels",victim->name);
    }
}

void do_nonote( CHAR_DATA *ch, char *argument ) {
    char	arg[MIL];
    CHAR_DATA *	victim;

    one_argument( argument, arg );

    if (arg[0]==0) {
	send_to_char("Nonote whom?\n\r",ch);
	return;
    }

    if ((victim=get_char_world(ch,arg))==NULL) {
	send_to_char("They aren't here.\n\r",ch);
	return;
    }

    if (get_trust(ch)<LEVEL_COUNCIL && get_trust(victim)>=get_trust(ch)) {
	send_to_char("You failed.\n\r",ch);
	return;
    }

    if (STR_IS_SET(victim->strbit_comm,COMM_NONOTE)) {
	STR_REMOVE_BIT(victim->strbit_comm, COMM_NONOTE);
	send_to_char("The gods have restored your note privileges.\n\r",
		      victim );
	send_to_char("NONOTE removed.\n\r",ch);
	wiznet(WIZ_PENALTIES,0,ch,NULL,
	    "$N restores notes to %s",victim->name);
    } else {
	STR_SET_BIT(victim->strbit_comm,COMM_NONOTE);
	send_to_char("The gods have revoked your note privileges.\n\r",
		       victim );
	send_to_char("NONOTE set.\n\r",ch);
	wiznet(WIZ_PENALTIES,0,ch,NULL,
	    "$N revokes %s's notes",victim->name);
	if (victim->pnote) {
	    free_note(victim->pnote);
	    victim->pnote=NULL;
	}
    }
}

void do_nocolor(CHAR_DATA *ch,char *argument) {
    char	arg[MIL];
    CHAR_DATA *	victim;

    one_argument( argument, arg );

    if (arg[0]==0) {
	send_to_char("Nocolor whom?",ch);
	return;
    }

    if ((victim=get_char_world(ch,arg))==NULL) {
	send_to_char("They aren't here.\n\r", ch );
	return;
    }

    if (get_trust(ch)<LEVEL_COUNCIL && get_trust(victim)>=get_trust(ch)) {
	send_to_char( "You failed.\n\r", ch );
	return;
    }

    if (STR_IS_SET(victim->strbit_comm,COMM_NOCOLOR)) {
	STR_REMOVE_BIT(victim->strbit_comm,COMM_NOCOLOR);
	send_to_char("The gods have restored your color privileges.\n\r",
		victim);
	send_to_char("NOCOLOR removed.\n\r",ch);
	wiznet(WIZ_PENALTIES,0,ch,NULL,"$N restores colors to %s",victim->name);
    } else {
	STR_SET_BIT(victim->strbit_comm,COMM_NOCOLOR);
	send_to_char("The gods have revoked your color privileges.\n\r",victim);
	send_to_char("NOCOLOR set.\n\r",ch);
	wiznet(WIZ_PENALTIES,0,ch,NULL,"$N revokes %s's colours",victim->name);
    }
}


void do_freeze(CHAR_DATA *ch,char *argument) {
    char	arg[MIL];
    CHAR_DATA *	victim;

    one_argument(argument,arg);

    if (arg[0]==0) {
	send_to_char("Freeze whom?\n\r",ch);
	return;
    }

    if ((victim=get_char_world(ch,arg))==NULL) {
	send_to_char("They aren't here.\n\r",ch);
	return;
    }

    if (IS_NPC(victim)) {
	send_to_char("Not on NPC's.\n\r",ch);
	return;
    }

    if (get_trust(victim)>=get_trust(ch)) {
	send_to_char("You failed.\n\r",ch);
	return;
    }

    if (STR_IS_SET(victim->strbit_act,PLR_FREEZE)) {
	STR_REMOVE_BIT(victim->strbit_act,PLR_FREEZE);
	send_to_char("You can play again.\n\r",victim);
	send_to_char("FREEZE removed.\n\r",ch);
	wiznet(WIZ_PENALTIES,0,ch,NULL,"$N has unfrozen %s.",victim->name);
    } else {
	STR_SET_BIT(victim->strbit_act,PLR_FREEZE);
	send_to_char("You can't do ANYthing!\n\r",victim);
	send_to_char("FREEZE set.\n\r",ch);
	wiznet(WIZ_PENALTIES,0,ch,NULL,
	    "$N puts %s in the deep freeze.",victim->name);
    }

    save_char_obj(victim);
}

void log_char(CHAR_DATA *victim, char *reason, int timeout) {
    int old_timeout=-1;
    if (timeout==-1) {
	DeleteCharProperty(victim,PROPERTY_INT,"log-timeout");
    } else {
	if (!STR_IS_SET(victim->strbit_act,PLR_LOG) ||
	    (GetCharProperty(victim,PROPERTY_INT,"log-timeout",&old_timeout) && 
	     (old_timeout<timeout)))
	    SetCharProperty(victim,PROPERTY_INT,"log-timeout",&timeout);
    }
    if ((timeout==-1) ||
	!STR_IS_SET(victim->strbit_act,PLR_LOG) ||
	((old_timeout!=-1) && (timeout>old_timeout)))
	SetCharProperty(victim,PROPERTY_STRING,"log-reason",reason);
    STR_SET_BIT(victim->strbit_act,PLR_LOG);
}

void do_log(CHAR_DATA *ch,char *argument) {
    char	arg[MIL];
    CHAR_DATA *	victim;
    bool	setOff;

    argument=one_argument(argument,arg);
    setOff=str_cmp(argument,"off")==0;

    if (arg[0]==0) {
	send_to_char("Log whom?\n\r",ch);
	return;
    }

    if (!str_cmp(arg,"allplayers")) {
	if (argument[0]==0) {
	    sprintf_to_char(ch,
		"Log allplayers is currently %s.\n\r",
		mud_data.logAllPlayers?"ON":"OFF");
	    send_to_char("Usage: Log allplayers <reason> to activate\r\n",ch);
	    send_to_char("       Log allplayers off      to deactivate\r\n",ch);
	    return;
	}

	if (setOff==TRUE) {
	    if (mud_data.logAllPlayers==TRUE) {
		mud_data.logAllPlayers=FALSE;
		send_to_char("Log allplayers off.\n\r",ch);
	    } else {
		send_to_char("Log allplayers already off.\n\r",ch);
	    }
	} else {
	    if (mud_data.logAllPlayers==TRUE) {
		send_to_char("Log allplayers already on.\n\r",ch);
	    } else {
		mud_data.logAllPlayers=TRUE;
		send_to_char("Log allplayers on.\n\r",ch);
	    }
	}
	return;
    }

    if (!str_cmp(arg,"allmobs")) {
	if (argument[0]==0) {
	    sprintf_to_char(ch,
		"Log allmobs is currently %s.\n\r",
		mud_data.logAllMobs?"ON":"OFF");
	    send_to_char("Usage: Log allmobs <reason> to activate\r\n",ch);
	    send_to_char("       Log allmobs off      to deactivate\r\n",ch);
	    return;
	}

	if (setOff==TRUE) {
	    if (mud_data.logAllMobs==TRUE) {
		mud_data.logAllMobs=FALSE;
		send_to_char("Log allmobs off.\n\r",ch);
	    } else {
		send_to_char("Log allmobs already off.\n\r",ch);
	    }
	} else {
	    if (mud_data.logAllMobs==TRUE) {
		send_to_char("Log allmobs already on.\n\r",ch);
	    } else {
		mud_data.logAllMobs=TRUE;
		send_to_char("Log allmobs on.\n\r",ch);
	    }
	}
	return;
    }

    if ((victim=get_char_world(ch,arg))==NULL) {
	send_to_char("They aren't here.\n\r",ch);
	return;
    }

    if ( IS_NPC(victim) ) {
	send_to_char("Not on NPC's.\n\r",ch);
	return;
    }

    //
    // No level check, gods can log anyone.
    //
    if (argument[0]==0) {
	char buf[MSL];
	int timeout;
	sprintf_to_char(ch,
	    "Log on %s is currently %s.\n\r",
	    victim->name,
	    STR_IS_SET(victim->strbit_act,PLR_LOG)?"ON":"OFF");
	if (STR_IS_SET(victim->strbit_act,PLR_LOG)) {
	    if (GetCharProperty(victim,PROPERTY_INT,"log-timeout",&timeout))
		sprintf_to_char(ch,"The log will timeout in %d ticks\n\r",
				timeout);
	    if (STR_IS_SET(victim->strbit_act,PLR_LOG) &&
		GetCharProperty(victim,PROPERTY_STRING,"log-reason",buf)) {
		sprintf_to_char(ch,"Logged by %s\r\n",buf);
	    }
	}
	send_to_char("Usage: Log <char> <reason>    to activate\r\n",ch);
	send_to_char("       Log <char> off         to deactivate\r\n",ch);
	return;
    }

    if (setOff) {
	if (STR_IS_SET(victim->strbit_act,PLR_LOG)) {
	    STR_REMOVE_BIT(victim->strbit_act,PLR_LOG);
	    sprintf_to_char(ch,"%s is no longer logged.\n\r",victim->name);
	    DeleteCharProperty(victim,PROPERTY_STRING,"log-reason");
	    DeleteCharProperty(victim,PROPERTY_INT,"log-timeout");
	} else
	    sprintf_to_char(ch,"%s is not logged.\n\r",victim->name);
    } else {
	int timeout;
	if (STR_IS_SET(victim->strbit_act,PLR_LOG) && !GetCharProperty(ch,PROPERTY_INT,"log-timeout",&timeout)) {
	    sprintf_to_char(ch,"%s is already logged.\n\r",victim->name);
	} else {
	    char buf[MSL];
	    sprintf_to_char(ch,"%s is now logged.\n\r",victim->name);
	    sprintf(buf,"%s: %s",NAME(ch),argument);
	    log_char(victim,buf,-1);
	}
    }
}



void do_noemote(CHAR_DATA *ch,char *argument) {
    char	arg[MIL];
    CHAR_DATA *	victim;

    one_argument(argument,arg);

    if (arg[0]==0) {
	send_to_char("Noemote whom?\n\r",ch);
	return;
    }

    if ((victim=get_char_world(ch,arg))==NULL) {
	send_to_char("They aren't here.\n\r",ch);
	return;
    }


    if (get_trust(victim)>=get_trust(ch)) {
	send_to_char("You failed.\n\r",ch);
	return;
    }

    if (STR_IS_SET(victim->strbit_comm,COMM_NOEMOTE)) {
	STR_REMOVE_BIT(victim->strbit_comm, COMM_NOEMOTE);
	send_to_char("You can emote again.\n\r",victim);
	send_to_char("NOEMOTE removed.\n\r",ch);
	wiznet(WIZ_PENALTIES,0,ch,NULL,
	    "$N restores emotes to %s.",victim->name);
    } else {
	STR_SET_BIT(victim->strbit_comm,COMM_NOEMOTE);
	send_to_char("You can't emote!\n\r",victim);
	send_to_char("NOEMOTE set.\n\r",ch);
	wiznet(WIZ_PENALTIES,0,ch,NULL,"$N revokes %s's emotes.",victim->name);
    }
}



void do_noshout(CHAR_DATA *ch,char *argument) {
    char	arg[MIL];
    CHAR_DATA *	victim;

    one_argument(argument,arg);

    if (arg[0]==0) {
	send_to_char("Noshout whom?\n\r",ch);
	return;
    }

    if ((victim=get_char_world(ch,arg))==NULL) {
	send_to_char("They aren't here.\n\r",ch);
	return;
    }

    if (IS_NPC(victim)) {
	send_to_char("Not on NPC's.\n\r",ch);
	return;
    }

    if (get_trust(victim)>=get_trust(ch)) {
	send_to_char("You failed.\n\r",ch);
	return;
    }

    if (STR_IS_SET(victim->strbit_comm,COMM_NOSHOUT)) {
	STR_REMOVE_BIT(victim->strbit_comm,COMM_NOSHOUT);
	send_to_char("You can shout again.\n\r",victim);
	send_to_char("NOSHOUT removed.\n\r",ch);
	wiznet(WIZ_PENALTIES,0,ch,NULL,
	    "$N restores shouts to %s.",victim->name);
    } else {
	STR_SET_BIT(victim->strbit_comm,COMM_NOSHOUT);
	send_to_char("You can't shout!\n\r",victim);
	send_to_char("NOSHOUT set.\n\r",ch);
	wiznet(WIZ_PENALTIES,0,ch,NULL,"$N revokes %s's shouts.",victim->name);
    }
}



void do_notell( CHAR_DATA *ch, char *argument )
{
    char	arg[MIL];
    CHAR_DATA *	victim;

    one_argument(argument,arg);

    if (arg[0]==0) {
	send_to_char("Notell whom?",ch);
	return;
    }

    if ((victim=get_char_world(ch,arg))==NULL) {
	send_to_char("They aren't here.\n\r",ch);
	return;
    }

    if (get_trust(victim)>=get_trust(ch)) {
	send_to_char("You failed.\n\r",ch);
	return;
    }

    if (STR_IS_SET(victim->strbit_comm,COMM_NOTELL)) {
	STR_REMOVE_BIT(victim->strbit_comm,COMM_NOTELL);
	send_to_char("You can tell again.\n\r",victim);
	send_to_char("NOTELL removed.\n\r",ch);
	wiznet(WIZ_PENALTIES,0,ch,NULL,"$N restores tells to %s.",victim->name);
    } else {
	STR_SET_BIT(victim->strbit_comm,COMM_NOTELL);
	send_to_char("You can't tell!\n\r",victim);
	send_to_char("NOTELL set.\n\r",ch);
	wiznet(WIZ_PENALTIES,0,ch,NULL,"$N revokes %s's tells.",victim->name);
    }
}




void do_deny(CHAR_DATA *ch,char *argument) {
    char	arg[MIL];
    CHAR_DATA *	victim;

    one_argument(argument,arg);

    if (arg[0]==0) {
	send_to_char("Deny whom?\n\r",ch);
	return;
    }

    if ((victim=get_char_world(ch,arg))==NULL) {
	send_to_char("They aren't here.\n\r",ch);
	return;
    }

    if (IS_NPC(victim)) {
	send_to_char("Not on NPC's.\n\r",ch);
	return;
    }

    if (get_trust(victim)>=get_trust(ch)) {
	send_to_char("You failed.\n\r",ch);
	return;
    }

    STR_TOGGLE_BIT(victim->strbit_act,PLR_DENY);
    if (STR_IS_SET(victim->strbit_act,PLR_DENY)) {
	send_to_char("You are denied access!\n\r",victim);
	wiznet(WIZ_PENALTIES,0,ch,NULL,"$N denies access to %s",victim->name);
    } else {
	send_to_char("You are allowed access again!\n\r",victim);
	wiznet(WIZ_PENALTIES,0,ch,NULL,"$N allows access to %s",victim->name);
    }
    send_to_char("OK.\n\r",ch);
    save_char_obj(victim);

    if (STR_IS_SET(victim->strbit_act,PLR_DENY)) {
	stop_fighting(victim,TRUE);
	do_function(victim,&do_quit,"");
    }
}

/*
 * ch has just committed an act of OOL
 * after 3 acts, ch will be denied imm-services for a day
 * every additional act will add 1 day.
 */
void ool_penalize(CHAR_DATA *ch) {
  int ool_penalty;

  // does char have ool_pen prop?
    if (GetCharProperty(ch,PROPERTY_INT,"ool_penalty",&ool_penalty)) 
  //   inc by 1 day
	ool_penalty+=24*60*60;
  // else
    else
  //   set prop to now-2days
	ool_penalty=time(NULL)-2*24*60*60;
  //   
    SetCharProperty(ch,PROPERTY_INT,"ool_penalty",&ool_penalty);
    sprintf_to_char(ch,"%s frowns on you.",players_god(ch));
}

/*
 * <0    char has a clean slate
 *  0    char is on probation
 * >0    char has been bad (value == days left until entering probation)
 */
int get_ool_penalty_state(CHAR_DATA *ch) {
    int ool_penalty;
    if (GetCharProperty(ch,PROPERTY_INT,"ool_penalty",&ool_penalty)) {
	ool_penalty-=time(NULL);
	if (ool_penalty<0) return 0;
	return ceil((float)ool_penalty/(24*60*60));
    }
    return -1;
}

/*
 * cantrip clone, but this ine is for OOL
 */
void show_ool_cantrip(CHAR_DATA *ch, CHAR_DATA *victim, int type) {
    int ool_level;

    ool_level=get_ool_penalty_state(victim);
    if (ool_level<0) return;

    if (ool_level==0) {
	act("$e wears a vague mark of OOL.",victim,NULL,ch,type);
	return;
    }
    if (ool_level<=3) {
	act("$e wears a fading mark of OOL.",victim,NULL,ch,type);
	return;
    }
    act("$e wears the mark of OOL.",victim,NULL,ch,type);
}
