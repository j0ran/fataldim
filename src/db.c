/* $Id: db.c,v 1.202 2008/05/11 20:50:47 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

/*
 * 19971227 EG  Modified Capitalize() to capitalize after a non-alpha
        characters [a-zA-Z] instead of a space.
 */

#include "merc.h"
#include "db.h"
#include "olc.h"
#include "recycle.h"

//
// global gsn-list for skills/spells
//
#include "fd_skills_gsn.c"

#if !defined(OLD_RAND)
#if !defined(linux)
long random();
#endif
#ifndef __FreeBSD__
void srandom(unsigned int);
#endif
int getpid();
time_t time(time_t *tloc);
#endif


/* externals for counting purposes */

/*
 * Globals.
 */

MUD_DATA		mud_data;
HELP_DATA *		help_first;
HELP_DATA *		help_last;

SHOP_DATA *		shop_first;
SHOP_DATA *		shop_last;

CHAR_DATA *		char_list;
CHAR_DATA *		player_list;
EVENT_DATA *		event_list;
EVENT_DATA *		event_new;
char *			help_greeting;
NOTE_DATA *		note_list;
OBJ_DATA *		object_list;
TIME_INFO_DATA		time_info;
WEATHER_DATA		weather_info;
FS_ENTRY *		fs_list;

/*
 * Locals.
 */
TCLPROG_CODE *		rprog_index_hash	[MAX_KEY_HASH];
TCLPROG_CODE *		mprog_index_hash	[MAX_KEY_HASH];
TCLPROG_CODE *		oprog_index_hash	[MAX_KEY_HASH];
MOB_INDEX_DATA *	mob_index_hash		[MAX_KEY_HASH];
OBJ_INDEX_DATA *	obj_index_hash		[MAX_KEY_HASH];
ROOM_INDEX_DATA *	room_index_hash		[MAX_KEY_HASH];

AREA_DATA *		area_first;
AREA_DATA *		area_last;

char			str_empty	[1];

int			newmobs = 0;
int			newobjs = 0;

char			ZERO_FLAG[MAX_FLAGS];
char			ONE_FLAG[MAX_FLAGS];


/*
 * Semi-locals.
 */
bool			fBootDb;
FILE *			fpArea;
char			strArea[MAX_INPUT_LENGTH];
static bool		area_header_found;



/*
 * Local booting procedures.
*/
void    init_mm         ( void );
void	load_area	( FILE *fp );
void	load_helps	( FILE *fp );
void 	load_mobiles	( FILE *fp );
void 	load_objects	( FILE *fp );
void	load_resets	( FILE *fp );
void	load_rooms	( FILE *fp , bool newstyle);
void	load_shops	( FILE *fp , int version );
void 	load_socials	( FILE *fp );
void	load_specials	( FILE *fp );
void	load_mobprogs	( FILE *fp );
void	load_objprogs	( FILE *fp );
void	load_roomprogs	( FILE *fp );
void	load_areaprogs	( FILE *fp );
void	load_areaprogram	( FILE *fp );
void	load_area_properties ( FILE *fp );

void	load_notes	( void );
void	load_swearlist	( void );
void	load_jobs	( void );

void	validate_resets	( void );
void	fix_exits	( void );


void	tcl_reset_all_areas(void);
void	reset_area	( AREA_DATA * pArea);

void	update_area	( char *filename );
char   *import_new_area ( void );

/*
 * Non-ROM Loading procedures.
 * Put any new loading function in this section.
 */
void	new_load_area	( FILE *fp );	/* OLC */

/*
 * Big mama top level function.
 */
void boot_db( void )
{

    /*
     * Init some data space stuff.
     */
    {
        new_string_space();
        fBootDb		= TRUE;
        STR_ZERO_BIT(ZERO_FLAG,MAX_FLAGS);
        STR_ONE_BIT(ONE_FLAG,MAX_FLAGS);
    }

    /*
     * Init random number generator.
     */
    {
        init_mm( );
    }

    /*
     * Set time and weather.
     */
    {
        long lhour, lday, lmonth;

        lhour		= (current_time - 902235915)
                / (PULSE_TICK / PULSE_PER_SECOND);
        time_info.hour	= lhour  % 24;
        lday		= lhour  / 24;
        time_info.day	= lday   % 35;
        lmonth		= lday   / 35;
        time_info.month	= lmonth % 17;
        time_info.year	= lmonth / 17;

        if ( time_info.hour <  5 ) weather_info.sunlight = SUN_DARK;
        else if ( time_info.hour <  6 ) weather_info.sunlight = SUN_RISE;
        else if ( time_info.hour < 19 ) weather_info.sunlight = SUN_LIGHT;
        else if ( time_info.hour < 20 ) weather_info.sunlight = SUN_SET;
        else                            weather_info.sunlight = SUN_DARK;

        weather_info.change	= 0;
        weather_info.mmhg	= 960;
        if ( time_info.month >= 7 && time_info.month <=12 )
            weather_info.mmhg += number_range( 1, 50 );
        else
            weather_info.mmhg += number_range( 1, 80 );

        if ( weather_info.mmhg <=  980 ) weather_info.sky = SKY_LIGHTNING;
        else if ( weather_info.mmhg <= 1000 ) weather_info.sky = SKY_RAINING;
        else if ( weather_info.mmhg <= 1020 ) weather_info.sky = SKY_CLOUDY;
        else                                  weather_info.sky = SKY_CLOUDLESS;

    }

    /*
     * Assign gsn's for skills which have them.
     */
    {
        int sn;

        for ( sn = 0; sn < MAX_SKILL; sn++ )
        {
            if ( skill_pgsn(sn,NULL) != NULL )
                (*skill_pgsn(sn,NULL)) = sn;
        }
    }

    load_clans( );	/* Load clans first, needed in rooms (I think) */
    load_properties();
    load_poll();
    init_races();	// initialize bitstrings of them

    /*
     * Read in all the area files.
     */
    {
        FILE *fpList;
        int ListSource=0;
        char filename[MSL];

        if ( ( fpList = fopen( mud_data.area_lst, "r" ) ) == NULL )
        {
            logf("[0] boot_db(): fopen(%s): %s",
                 mud_data.area_lst,ERROR);
            abort();
        }

        for ( ; ; )
        {
            if (ListSource==0) {
                strcpy( strArea, fread_word( fpList ) );
                if ( strArea[0] == '$' ) {
                    fclose( fpList );
                    ListSource=1;
                }
            }
            if (ListSource==1) {
                char *next=import_new_area();
                if (!next) break;
                strcpy(strArea, next);
            }

            if ( strArea[0] == '-' )
            {
                fpArea = stdin;
            }
            else
            {
                if (ListSource==0) update_area(strArea);
                sprintf(filename,"%s/%s",mud_data.area_dir,strArea);
                mud_data.currentareafile=filename;
                mud_data.currentline=1;
                if ( ( fpArea = fopen( filename, "r" ) ) == NULL )
                {
                    bugf("Can't open area file %s.",filename);
                    continue;
                }
                area_header_found=FALSE;
            }

            for ( ; ; )
            {
                char *word;

                if ( fread_letter( fpArea ) != '#' )
                {
                    bugf( "Boot_db: # not found for area %s.", strArea );
                    abort();
                }

                word = fread_word( fpArea );

                if ( word[0] == '$'               )                 break;
                else if ( !str_cmp( word, "AREA"     ) ) load_area    (fpArea);
                else if ( !str_cmp( word, "HELPS"    ) ) load_helps   (fpArea);
                else if ( !str_cmp( word, "MOBILES"  ) ) load_mobiles (fpArea);
                else if ( !str_cmp( word, "OBJECTS"  ) ) load_objects (fpArea);
                else if ( !str_cmp( word, "RESETS"   ) ) load_resets  (fpArea);
                else if ( !str_cmp( word, "ROOMS"    ) ) load_rooms   (fpArea,FALSE);
                else if ( !str_cmp( word, "SHOPS"    ) ) load_shops   (fpArea,0);
                else if ( !str_cmp( word, "SHOPS1"   ) ) load_shops   (fpArea,1);
                else if ( !str_cmp( word, "SOCIALS"  ) ) load_socials (fpArea);
                else if ( !str_cmp( word, "SPECIALS" ) ) load_specials(fpArea);
                else if ( !str_cmp( word, "AREADATA" ) ) new_load_area( fpArea ); /* OLC */
                else if ( !str_cmp( word, "ROOMDATA" ) ) load_rooms( fpArea,TRUE ); /* OLC */
                else if ( !str_cmp( word, "MOBPROGS" ) ) load_mobprogs(fpArea);
                else if ( !str_cmp( word, "OBJPROGS" ) ) load_objprogs(fpArea);
                else if ( !str_cmp( word, "ROOMPROGS" ) )load_roomprogs(fpArea);
                else if ( !str_cmp( word, "AREAPROGS" ) )load_areaprogs(fpArea);
                else if ( !str_cmp( word, "AREAPROG" ) )load_areaprogram(fpArea);
                else if ( !str_cmp( word, "PROPERTIES" ) )load_area_properties(fpArea);
                else
                {
                    bugf("Boot_db: bad section name '%s' for area %s",
                         word,strArea);
                    abort();
                }
            }

            if ( fpArea != stdin )
                fclose( fpArea );
            fpArea = NULL;
        }
    }

    /*
     * Fix up exits.
     * Declare db booting over.
     * Reset all areas once.
     * Load up the songs, notes and ban files.
     */
    {
        fBootDb	= FALSE;
        validate_resets( );
        fix_exits( );
        tcl_reset_all_areas();
        new_load_all_room_obj();
        area_update( );
        load_notes( );
        load_bans();
        load_badnames();
#ifdef HAS_JUKEBOX
        load_songs();
#endif
        load_swearlist();
        load_jobs();
    }

    return;
}

/*
 * Check if there is a new area file available from import
 */
void update_area( char *filename ) {
    // does new file exist
    // yes : MOVE!
    // no  : do nothing

    char	from[MSL];
    char	to[MSL];

    sprintf(from,"%s/%s",mud_data.area_import,filename);
    sprintf(to,"%s/%s",mud_data.area_dir,filename);
    if (rename(from,to)==0)
        logf("[0] AREA import %s (%s -> %s)",filename,from,to);
    else if (errno!=ENOENT)
        logf("[0] update_area(): import failed. Area: %s: %s",
             filename,ERROR);
}

char *import_new_area() {
    static DIR *fd;
    static struct dirent *de;

    if (fd==NULL) {
        if ((fd=opendir(mud_data.area_import))==NULL) {
            logf("[0] import_new_area(): opendir(%s): %s",
                 mud_data.area_import,ERROR);
            return NULL;
        }
    }
    do {
        if ((de=readdir(fd))==NULL) {
            closedir(fd);
            fd=NULL;
            return NULL;
        }
    } while ((de->d_name[0]=='.') || str_suffix(".are",de->d_name));
    logf("[0] import_new_area(): new AREA imported %s",de->d_name);
    update_area(de->d_name);
    return de->d_name;
}

/*
 * Snarf an 'area' header line.
 * This is probably veeeeery obsolete.
 */
void load_area( FILE *fp )
{
    AREA_DATA *pArea;

    pArea		= new_area();
    fread_string(fp); /* file_name */
    fread_string( fp ); /* name */
    free_string(pArea->name);
    pArea->name		= fread_string( fp ); /* credits */
    fread_number(fp); /* min_vnum */
    fread_number(fp); /* max_vnum */
    pArea->vnum		= area_allocated;	/* OLC */
    free_string(pArea->filename);
    pArea->filename	= str_dup( strArea );	/* OLC */

    if ( area_first == NULL )
        area_first = pArea;
    if ( area_last  != NULL )
    {
        area_last->next = pArea;
        REMOVE_BIT(area_last->area_flags, AREA_LOADING);	/* OLC */
    }
    area_last	= pArea;
    pArea->next	= NULL;

    area_header_found=TRUE;
    return;
}

/*
 * OLC
 * Use these macros to load any new area formats that you choose to
 * support on your MUD.  See the new_load_area format below for
 * a short example.
 */
#if defined(KEY)
#undef KEY
#endif

#define KEY( literal, field, value )                \
    if ( !str_cmp( word, literal ) )    \
{                                   \
    field  = value;                 \
    break;                          \
    }

#define SKEY( string, field )                       \
    if ( !str_cmp( word, string ) )     \
{                                   \
    free_string( field );           \
    field = fread_string( fp );     \
    break;                          \
    }



/* OLC
 * Snarf an 'area' header line.   Check this format.  MUCH better.  Add fields
 * too.
 *
 * #AREAFILE
 * Name   { All } Locke    Newbie School~
 * Repop  A teacher pops in the room and says, 'Repop coming!'~
 * Recall 3001
 * End
 */
void new_load_area( FILE *fp )
{
    AREA_DATA *pArea;
    char      *word;

    pArea               = new_area();
    free_string(pArea->filename);
    pArea->filename     = str_dup( strArea );
    pArea->vnum         = area_allocated;

    area_header_found=TRUE;

    for ( ; ; ) {
        word   = feof( fp ) ? "End" : fread_word( fp );

        switch ( UPPER(word[0]) ) {
        case 'B':
            SKEY( "Builders", pArea->builders );
            break;

        case 'C':
            SKEY( "Comment", pArea->comment );
            SKEY( "Creator", pArea->creator );
            if ( !str_cmp( word, "Conquered") ) {
                char *name;
                CLAN_INFO *pClan;

                name=fread_string_temp(fp);
                pClan=get_clan_by_name(name);
                if (pClan)
                    pArea->conquered_by=pClan;
                else
                    bugf("Area conquered by unknown clan ('%s')",name);
            }
            break;

        case 'D':
            SKEY( "Description", pArea->description );
            break;

        case 'E':
            if ( !str_cmp( word, "End" ) ) {
                if ( area_first == NULL )
                    area_first = pArea;
                if ( area_last  != NULL ) {
                    area_last->next = pArea;
                    REMOVE_BIT(area_last->area_flags, AREA_LOADING); // OLC
                }
                area_last   = pArea;
                pArea->next = NULL;

                //
                // disentangle levels, creator and area-name
                //
                if (pArea->version<2) {
                    int llevel,ulevel;
                    char creator[MSL];
                    char name[MSL];

                    if (sscanf(pArea->name,"{{%d--%d}} %s %[^~]",
                               &llevel,&ulevel,creator,name)==4) {
                        free_string(pArea->name);
                        free_string(pArea->creator);
                        pArea->name=str_dup(name);
                        pArea->creator=str_dup(creator);
                        pArea->llevel=llevel;
                        pArea->ulevel=ulevel;
                    } else if (sscanf(pArea->name,"{{%d-%d}} %s %[^~]",
                                      &llevel,&ulevel,creator,name)==4) {
                        free_string(pArea->name);
                        free_string(pArea->creator);
                        pArea->name=str_dup(name);
                        pArea->creator=str_dup(creator);
                        pArea->llevel=llevel;
                        pArea->ulevel=ulevel;
                    } else if (sscanf(pArea->name,"{{ Help }} %s %[^~]",
                                      creator,name)==2) {
                        free_string(pArea->name);
                        free_string(pArea->creator);
                        pArea->name=str_dup(name);
                        pArea->creator=str_dup(creator);
                        pArea->llevel=0;
                        pArea->ulevel=0;
                    } else if (sscanf(pArea->name,"{{ All  }} %s %[^~]",
                                      creator,name)==2) {
                        free_string(pArea->name);
                        free_string(pArea->creator);
                        pArea->name=str_dup(name);
                        pArea->creator=str_dup(creator);
                        pArea->llevel=0;
                        pArea->ulevel=MAX_LEVEL;
                    } else {
                        bugf("new_load_area(): Unable to parse area-line: %s '%s'\n",pArea->filename,pArea->name);
                        abort();
                    }

                }

                return;
            }
            break;
        case 'F':
            if( !str_cmp( word,"Flags" ) ) {
                pArea->area_flags|=fread_number( fp );
                break;
            }
            break;
        case 'L':
            KEY("Login_room",pArea->login_room,fread_number(fp));
            if ( !str_cmp( word, "Level" ) ) {
                pArea->llevel = fread_number( fp );
                pArea->ulevel = fread_number( fp );
            }
            break;
        case 'N':
            SKEY( "Name", pArea->name );
            break;
        case 'P':
            //KEY( "ProgInstance", pArea->aprog_vnum, fread_number( fp ) );
            if ( !str_cmp( word, "ProgInstance") ) {
                int num;
                num=fread_number(fp);
                if (num!=0) pArea->aprog_enabled=TRUE;
            }
            break;
        case 'R':
            KEY( "Recall", pArea->recall, fread_number( fp ) );
            break;
        case 'S':
            KEY( "Security", pArea->security, fread_number( fp ) );
            break;
        case 'U':
            SKEY("Urlprefix",pArea->urlprefix);
        case 'V':
            KEY("Version",pArea->version,fread_number(fp));
            if ( !str_cmp( word, "VNUMs" ) ) {
                pArea->lvnum = fread_number( fp );
                pArea->uvnum = fread_number( fp );
            }
            break;
        }
    }
}


/*
 * Sets vnum range for area using OLC protection features.
 */
void assign_area_vnum( int vnum )
{
    if ( area_last->lvnum == 0 || area_last->uvnum == 0 )
        area_last->lvnum = area_last->uvnum = vnum;
    if ( vnum != URANGE( area_last->lvnum, vnum, area_last->uvnum ) ) {
        if ( vnum < area_last->lvnum )
            area_last->lvnum = vnum;
        else
            area_last->uvnum = vnum;
    }
    return;
}

/*
 * Snarf a help section.
 */
void load_helps( FILE *fp )
{
    HELP_DATA *pHelp;

    for ( ; ; )
    {
        pHelp		= new_help();

        if(area_header_found)
            pHelp->area	= area_last ? area_last : NULL;		/* OLC 1.1b */
        else
            pHelp->area	= NULL;

        pHelp->level	= fread_number( fp );
        pHelp->keyword	= fread_string( fp );
        pHelp->deleted  = FALSE;
        if ( pHelp->keyword[0] == '$' )
            break;
        pHelp->text	= fread_string( fp );

        if ( !str_cmp( pHelp->keyword, "greeting" ) )
            help_greeting = pHelp->text;

        if ( help_first == NULL )
            help_first = pHelp;
        if ( help_last  != NULL )
            help_last->next = pHelp;

        help_last	= pHelp;
        pHelp->next	= NULL;
    }

    return;
}

/*
 * Adds a reset to a room.  OLC
 * Similar to add_reset in olc.c
 */
void reset_to_room( ROOM_INDEX_DATA *pR, RESET_DATA *pReset )
{
    RESET_DATA *pr;

    if ( !pR )
        return;

    pr = pR->reset_last;

    if ( !pr ) {
        pR->reset_first = pReset;
        pR->reset_last  = pReset;
    } else {
        pR->reset_last->next = pReset;
        pR->reset_last       = pReset;
        pR->reset_last->next = NULL;
    }
}

/*
 * Snarf a reset section.
 */
void load_resets( FILE *fp )
{
    RESET_DATA *pReset;
    ROOM_INDEX_DATA *pRoomIndex;
    EXIT_DATA   *pexit;
    int 	iLastRoom = 0;
    int 	iLastObj  = 0;

    if ( area_last == NULL )
    {
        bugf( "Load_resets: no #AREA seen yet.");
        abort();
    }

    for ( ; ; )
    {
        char letter;

        if ( ( letter = fread_letter( fp ) ) == 'S' )
            break;

        if ( letter == '*' )
        {
            fread_to_eol( fp );
            continue;
        }

        pReset		= new_reset();
        pReset->command	= letter;
        if (area_last->version<6)
            /* if_flag */	  fread_number( fp );
        pReset->arg1	= fread_number( fp );
        pReset->arg2	= fread_number( fp );
        pReset->arg3	= (letter == 'G' || letter == 'R')
                ? 0 : fread_number( fp );
        if (area_last->version<6) {
            pReset->arg4	= (letter == 'P' || letter == 'M')
                    ? fread_number(fp) : 0;
            if (letter=='O') {
                if (pReset->arg2==0) pReset->arg2=-1;
                if (pReset->arg4==0) pReset->arg4=1;
            }
        } else {
            pReset->arg4	= (letter == 'P' || letter == 'M' || letter == 'O')
                    ? fread_number(fp) : 0;
        }
        fread_to_eol( fp );

        /* Don't validate now, do it after all area's have been loaded */
        /* Stuff to add reset to the correct room */
        switch(letter) {
        default:
            bugf( "Load_resets: bad command '%c'.", letter );
            abort();
            break;
        case 'M':		// mobile
            if( (pRoomIndex = get_room_index ( pReset->arg3 )) )
            {
                if ((pReset->arg1<pRoomIndex->area->lvnum) ||
                        (pReset->arg1>pRoomIndex->area->uvnum)) {
                    bugf("dangerous reset: room %d, mob %d",
                         pReset->arg3,pReset->arg1);
                }
                reset_to_room( pRoomIndex, pReset);
                iLastRoom = pReset->arg3;
            }
            break;
        case 'O':		// object
            if ( ( pRoomIndex = get_room_index ( pReset->arg3 ) ) )
            {
                if ((pRoomIndex->area->lvnum>pReset->arg1) ||
                        (pRoomIndex->area->uvnum<pReset->arg1)) {
                    bugf("dangerous reset: room %d, object %d",
                         pReset->arg3,pReset->arg1);
                }
                reset_to_room( pRoomIndex, pReset );
                iLastObj = pReset->arg3;
            }
            break;
        case 'P':		// object in object
            if ( ( pRoomIndex = get_room_index ( iLastObj ) ) ) {
                if ((pRoomIndex->area->lvnum>pReset->arg1) ||
                        (pRoomIndex->area->uvnum<pReset->arg1)) {
                    bugf("dangerous reset: room %d, object %d",
                         iLastObj,pReset->arg1);
                }
                reset_to_room( pRoomIndex, pReset );
            }
            break;
        case 'G':		// give object to mobile
        case 'E':		// equip object to mobile
            if ( ( pRoomIndex = get_room_index ( iLastRoom ) ) )
            {
                if ((pRoomIndex->area->lvnum>pReset->arg1) ||
                        (pRoomIndex->area->uvnum<pReset->arg1)) {
                    bugf("dangerous reset: room %d, object %d",
                         iLastRoom,pReset->arg1);
                }
                reset_to_room( pRoomIndex, pReset );
                iLastObj = iLastRoom;
            }
            break;
        case 'D':		// state of door
            pRoomIndex = get_room_index( pReset->arg1 );

            if ( pReset->arg2 < 0
                 ||   pReset->arg2 > 5
                 || !pRoomIndex
                 || ( pexit = pRoomIndex->exit[pReset->arg2] ) == NULL
                 || !IS_SET( pexit->rs_flags, EX_ISDOOR ) )
            {
                bugf( "Load_resets: 'D': exit %d not door.", pReset->arg2 );
                abort();
            }

            switch ( pReset->arg3 )	/* OLC 1.1b */
            {
            default:
                bugf( "Load_resets: 'D': bad 'locks': %d." , pReset->arg3);
            case 0:
                break;
            case 1: SET_BIT( pexit->rs_flags, EX_CLOSED );
                break;
            case 2: SET_BIT( pexit->rs_flags, EX_CLOSED | EX_LOCKED );
                break;
            }
            break;
        case 'R':			// randomize room exits
            if ( pReset->arg2 < 0 || pReset->arg2 > 6 )
            {
                bugf( "Load_resets: 'R': bad exit %d.", pReset->arg2 );
                abort();
            }

            if ( ( pRoomIndex = get_room_index( pReset->arg1 ) ) )
                reset_to_room( pRoomIndex, pReset );

            break;
        }

        pReset->next		= NULL;
    }

    return;
}


void validate_resets(void)
{
    AREA_DATA *pArea;
    OBJ_INDEX_DATA *temp_index;
    ROOM_INDEX_DATA *pRoom;
    RESET_DATA *pReset,*pReset_next,*pReset_last;
    int  vnum;
    bool Okay,oldBoot;


    for ( pArea=area_first; pArea != NULL; pArea = pArea->next)
        for ( vnum = pArea->lvnum; vnum <= pArea->uvnum; vnum++ )
        {
            oldBoot=fBootDb;
            fBootDb=FALSE;
            pRoom = get_room_index(vnum);
            fBootDb=oldBoot;

            pReset_last=NULL;
            if(pRoom) for ( pReset = pRoom->reset_first; pReset != NULL; pReset = pReset_next )
            {
                pReset_next=pReset->next;
                Okay=TRUE;

                /*
         * Validate parameters.
         * We're calling the index functions for the side effect.
         */
                switch ( pReset->command )
                {
                case 'M':
                    if(!(get_mob_index  ( pReset->arg1 )))
                    {
                        Okay=FALSE;
                        bugf("Get_mob_index: bad vnum %d in reset in room %d.",
                             pReset->arg1,pRoom->vnum);
                    }
                    break;
                case 'O':
                    temp_index = get_obj_index  ( pReset->arg1 );
                    if(temp_index) temp_index->reset_num++;
                    else
                    {
                        Okay=FALSE;
                        bugf("Get_obj_index: bad vnum %d in reset in room %d.",
                             pReset->arg1,pRoom->vnum);
                    }
                    break;
                case 'P':
                    temp_index = get_obj_index  ( pReset->arg1 );
                    if(temp_index) temp_index->reset_num++;
                    else
                    {
                        Okay=FALSE;
                        bugf("Get_obj_index: bad vnum %d in reset in room %d.",
                             pReset->arg1,pRoom->vnum);
                    }
                    break;
                case 'G':
                case 'E':
                    temp_index = get_obj_index  ( pReset->arg1 );
                    if(temp_index) temp_index->reset_num++;
                    else
                    {
                        Okay=FALSE;
                        bugf("Get_obj_index: bad vnum %d in reset in room %d.",
                             pReset->arg1,pRoom->vnum);
                    }
                    break;
                case 'D':
                case 'R':
                    break;
                }

                if(!Okay)
                {
                    /* This is a wrong reset. Remove it. */
                    if(pReset_last) pReset_last->next=pReset_next;
                    else pRoom->reset_first=pReset_next;
                    free_reset(pReset);
                }
                else pReset_last=pReset;
            }
        }
}

/*
 * Snarf a room section.
 */
void load_rooms( FILE *fp,bool newstyle )
{
    ROOM_INDEX_DATA *pRoomIndex;

    if ( area_last == NULL )
    {
        bugf("Load_rooms: no #AREA seen yet.");
        abort();
    }

    for ( ; ; )
    {
        int vnum;
        char letter;
        int door;
        int iHash;

        letter				= fread_letter( fp );
        if ( letter != '#' )
        {
            bugf( "Load_rooms: # not found.");
            abort();
        }

        vnum				= fread_number( fp );
        if ( vnum == 0 )
            break;

        fBootDb = FALSE;
        if ( get_room_index( vnum ) != NULL )
        {
            bugf( "Load_rooms: vnum %d duplicated.", vnum );
            abort();
        }
        fBootDb = TRUE;

        pRoomIndex			= new_room_index();
        pRoomIndex->area		= area_last;
        pRoomIndex->vnum		= vnum;
        pRoomIndex->name		= fread_string( fp );
        pRoomIndex->description		= fread_string( fp );

        // update it for the amount of rooms in area.
        area_last->vnum_rooms_in_use++;

        /* Area number */		  fread_number( fp );

        STR_ZERO_BIT(pRoomIndex->strbit_room_flags,MAX_FLAGS);
        if (area_last->version<1) {
            long l;
            int i;

            l=fread_flag(fp);
            for (i=1;i<=32;i++) {
                if (l%2)
                    STR_SET_BIT(pRoomIndex->strbit_room_flags,i);
                l>>=1;
            }
        } else {
            char *s;

            s=fread_string_temp(fp);
            hextostring(s,pRoomIndex->strbit_room_flags,strlen(s));
        }
        pRoomIndex->sector_type		= fread_number( fp );
        pRoomIndex->light		= 0;
        for ( door = 0; door < DIR_MAX; door++ )
            pRoomIndex->exit[door] = NULL;

        /* defaults */
        pRoomIndex->eff_perm = NULL;
        pRoomIndex->eff_temp = NULL;
        pRoomIndex->extra_flags = 0;
        pRoomIndex->rprog_vnum = 0;
        pRoomIndex->pueblo_picture	= str_dup("");

        for ( ; ; )
        {
            letter = fread_letter( fp );

            if ( letter == 'S' )
                break;

            else if ( letter == 'H') { /* healing room */
                int rate;

                rate=fread_number(fp);
                SetRoomProperty(pRoomIndex,PROPERTY_INT,"manarate",&rate);
            }
            else if ( letter == 'L') /* prog instance */
                pRoomIndex->rprog_vnum = fread_number(fp);

            else if ( letter == 'M') { /* mana room */
                int rate;

                rate=fread_number(fp);
                SetRoomProperty(pRoomIndex,PROPERTY_INT,"manarate",&rate);
            }

            else if ( letter == 'N') /* New fields */
            {
                char *word;

                word                    = fread_word(fp);

                if(!str_prefix(word,"level"))
                {
                    pRoomIndex->level = fread_number(fp);
                }
                else
                {
                    bugf("New field: unknown fieldname %s.",word);
                    abort();
                }
            }

            else if ( letter == 'C') /* clan */
            {
                //		if (pRoomIndex->clan)
                //	  	{
                //		    bug("Load_rooms: duplicate clan fields.",0);
                //		    exit(1);
                //		}
                //		pRoomIndex->clan = clan_lookup(fread_string(fp))->vnum;
            }

            else if ( letter == 'D' )
            {
                EXIT_DATA *pexit;
                int locks;

                door = fread_number( fp );
                if ( door < 0 || door > 5 )
                {
                    bugf( "Fread_rooms: vnum %d has bad door number.", vnum );
                    abort();
                }

                pexit			= new_exit();
                pexit->description	= fread_string( fp );
                pexit->keyword		= fread_string( fp );
                locks			= fread_number( fp );
                if(newstyle)
                {
                    pexit->exit_info	= locks;
                    pexit->rs_flags	= locks;		/* OLC */
                }
                else
                {
                    pexit->exit_info	= 0;
                    pexit->rs_flags	= 0;			/* OLC */
                    switch ( locks )
                    {
                    case 1:
                        pexit->rs_flags  = EX_ISDOOR;
                        break;
                    case 2:
                        pexit->rs_flags  = EX_ISDOOR | EX_PICKPROOF;
                        break;
                    case 3:
                        pexit->rs_flags  = EX_ISDOOR | EX_NOPASS;
                        break;
                    case 4:
                        pexit->rs_flags  = EX_ISDOOR|EX_NOPASS|EX_PICKPROOF;
                        break;
                    }
                }
                pexit->key		= fread_number( fp );
                pexit->vnum		= fread_number( fp );

                pRoomIndex->exit[door]	= pexit;
                pRoomIndex->old_exit[door] = pexit;
            }
            else if ( letter == 'E' )
            {
                EXTRA_DESCR_DATA *ed;

                ed			= new_extra_descr();
                ed->keyword		= fread_string( fp );
                ed->description		= fread_string( fp );
                ed->next		= pRoomIndex->extra_descr;
                pRoomIndex->extra_descr	= ed;
            } else if (letter=='Q') {	// pueblo picture
                pRoomIndex->pueblo_picture=fread_string(fp);
            } else if (letter == 'P') {
                char key[MAX_STRING_LENGTH];
                char type[MAX_STRING_LENGTH];
                char value[MAX_STRING_LENGTH];
                int i;
                bool b;
                char c;
                long l;

                if (area_last->version<5) {
                    char *temp;
                    temp=fread_string_eol(fp);
                    temp=one_argument(temp,key);
                    temp=one_argument(temp,type);
                    temp=one_argument(temp,value);
                } else {
                    strcpy(key,fread_string_temp(fp));
                    strcpy(type,fread_string_temp(fp));
                    strcpy(value,fread_string_temp(fp));
                }

                switch (which_keyword(type,"int","bool","string",
                                      "char","long",NULL)) {
                case 1:
                    i=atoi(value);
                    SetRoomProperty(pRoomIndex,PROPERTY_INT,key,&i);
                    break;
                case 2:
                    switch (which_keyword(value,"true","false",NULL)) {
                    case 1:
                        b=TRUE;
                        SetRoomProperty(pRoomIndex,PROPERTY_BOOL,key,&b);
                        break;
                    case 2:
                        b=FALSE;
                        SetRoomProperty(pRoomIndex,PROPERTY_BOOL,key,&b);
                        break;
                    default:
                        ;
                    }
                    break;
                case 3:
                    SetRoomProperty(pRoomIndex,PROPERTY_STRING,key,value);
                    break;
                case 4:
                    c=value[0];
                    SetRoomProperty(pRoomIndex,PROPERTY_CHAR,key,&c);
                    break;
                case 5:
                    l=atol(value);
                    SetRoomProperty(pRoomIndex,PROPERTY_LONG,key,&l);
                    break;
                default:
                    bugf("Property: unknown keyword %s",type);
                }
            }

            else
            {
                bugf( "Load_rooms: vnum %d has flag not 'DES'.", vnum );
                abort();
            }
        }

        iHash			= vnum % MAX_KEY_HASH;
        pRoomIndex->next	= room_index_hash[iHash];
        room_index_hash[iHash]	= pRoomIndex;
        max_vnum_room = max_vnum_room < vnum ? vnum : max_vnum_room; /* OLC */
        assign_area_vnum( vnum );				     /* OLC */
    }

    return;
}


/*
 * Snarf a shop section.
 */
void load_shops( FILE *fp ,int version)
{
    SHOP_DATA *pShop;

    for ( ; ; )
    {
        MOB_INDEX_DATA *pMobIndex;
        int iTrade;

        pShop			= new_shop();
        pShop->keeper		= fread_number( fp );
        if ( pShop->keeper == 0 )
            break;
        for ( iTrade = 0; iTrade < MAX_TRADE; iTrade++ )
            pShop->buy_type[iTrade]	= fread_number( fp );
        pShop->profit_buy	= fread_number( fp );
        pShop->profit_sell	= fread_number( fp );
        pShop->open_hour	= fread_number( fp );
        pShop->close_hour	= fread_number( fp );
        if(version==0) pShop->type = SHOP_NORMAL;
        else pShop->type	= fread_number( fp );
        fread_to_eol( fp );
        pMobIndex		= get_mob_index( pShop->keeper );
        pMobIndex->pShop	= pShop;

        if ( shop_first == NULL )
            shop_first = pShop;
        if ( shop_last  != NULL )
            shop_last->next = pShop;

        shop_last	= pShop;
        pShop->next	= NULL;
    }

    return;
}


/*
 * Snarf spec proc declarations.
 */
void load_specials( FILE *fp )
{
    for ( ; ; )
    {
        MOB_INDEX_DATA *pMobIndex;
        char letter;

        switch ( letter = fread_letter( fp ) )
        {
        default:
            bugf( "Load_specials: letter '%c' not *MS.", letter );
            abort();

        case 'S':
            return;

        case '*':
            break;

        case 'M':
            pMobIndex		= get_mob_index	( fread_number ( fp ) );
            pMobIndex->spec_fun	= spec_lookup	( fread_word   ( fp ) );
            if ( pMobIndex->spec_fun == 0 )
            {
                bugf( "Load_specials: 'M': vnum %d.", pMobIndex->vnum );
                abort();
            }
            break;
        }

        fread_to_eol( fp );
    }
}

/*
 * Load areaporgram section
 */
void load_areaprogram( FILE *fp ) {
    if ( area_last == NULL ) {
        bugf( "load_areaprogram: no #AREA seen yet.");
        abort();
    }

    if (area_last->areaprogram[0]!=0) {
        bugf( "load_areaprogram: area program already loaded.");
        abort();
    }

    area_last->areaprogram=fread_string( fp ); /* name */
}

/*
 * Load areaprogs section
 */
void load_areaprogs( FILE *fp )
{
    TCLPROG_CODE *pAprog;
    char *title;

    if ( area_last == NULL )
    {
        bugf( "Load_areaprogs: no #AREA seen yet.");
        abort();
    }

    for ( ; ; )
    {
        int vnum;
        char letter;

        letter		  = fread_letter( fp );
        if ( letter != '#' )
        {
            bugf("Load_areaprogs: # not found.");
            abort();
        }

        vnum		 = fread_number( fp );
        if ( vnum == 0 )
            break;

        if (vnum!=area_last->vnum) {
            bugf("Load_areaprogs: vnum of program(%d) is different from area vnum(%d) : Adjusting.",vnum,area_last->vnum);
            vnum=area_last->vnum;
        }
        fBootDb = FALSE;
        if ( get_aprog_index( vnum ) != NULL )
        {
            bugf( "Load_areaprogs: vnum %d duplicated.", vnum );
            abort();
        }
        fBootDb = TRUE;

        pAprog		= new_aprog_code_index();
        pAprog->area	= area_last;		/* OLC 1.1b */
        pAprog->vnum  	= vnum;
        pAprog->version	= 0;
        title		= fread_string( fp );	// running after reboot

        if (!strncmp(title,"Title ",6)) {
            pAprog->title	= str_dup(title+6);
            pAprog->code  	= fread_string( fp );
            free_string(title);
        } else {
            pAprog->code	= title;
        }

        area_last->aprog=pAprog;
    }
    return;
}

/*
 * Load objprogs section
 */
void load_objprogs( FILE *fp )
{
    TCLPROG_CODE *pOprog;
    char *title;

    if ( area_last == NULL )
    {
        bugf("Load_objprogs: no #AREA seen yet.");
        abort();
    }

    for ( ; ; )
    {
        int vnum;
        char letter;
        int iHash;

        letter		  = fread_letter( fp );
        if ( letter != '#' )
        {
            bugf("Load_objprogs: # not found.");
            abort();
        }

        vnum		 = fread_number( fp );
        if ( vnum == 0 )
            break;

        fBootDb = FALSE;
        if ( get_oprog_index( vnum ) != NULL )
        {
            bugf( "Load_objprogs: vnum %d duplicated.", vnum );
            abort();
        }
        fBootDb = TRUE;

        pOprog		= new_oprog_code_index();
        pOprog->area	= area_last;		/* OLC 1.1b */
        pOprog->vnum  	= vnum;
        title		= fread_string( fp );

        if (!strncmp(title,"Title ",6)) {
            pOprog->title	= str_dup(title+6);
            pOprog->code  	= fread_string( fp );
            free_string(title);
        } else {
            pOprog->code	= title;
        }

        iHash			= vnum % MAX_KEY_HASH;
        pOprog->next		= oprog_index_hash[iHash];
        oprog_index_hash[iHash]	= pOprog;
        assign_area_vnum( vnum );				   /* OLC */
    }
    return;
}

/*
 * Load roomprogs section
 */
void load_roomprogs( FILE *fp )
{
    TCLPROG_CODE *pRprog;
    char *title;

    if ( area_last == NULL )
    {
        bugf("Load_roomprogs: no #AREA seen yet.");
        abort();
    }

    for ( ; ; )
    {
        int vnum;
        char letter;
        int iHash;

        letter		  = fread_letter( fp );
        if ( letter != '#' )
        {
            bugf("Load_roomprogs: # not found.");
            abort();
        }

        vnum		 = fread_number( fp );
        if ( vnum == 0 )
            break;

        fBootDb = FALSE;
        if ( get_rprog_index( vnum ) != NULL )
        {
            bugf( "Load_roomprogs: vnum %d duplicated.", vnum );
            abort();
        }
        fBootDb = TRUE;

        pRprog		= new_rprog_code_index();
        pRprog->area	= area_last;		/* OLC 1.1b */
        pRprog->vnum  	= vnum;
        pRprog->version	= 0;
        title		= fread_string( fp );	// initialise the first time

        if (!strncmp(title,"Title ",6)) {
            pRprog->title	= str_dup(title+6);
            pRprog->code  	= fread_string( fp );
            free_string(title);
        } else {
            pRprog->code	= title;
        }

        iHash			= vnum % MAX_KEY_HASH;
        pRprog->next		= rprog_index_hash[iHash];
        rprog_index_hash[iHash]	= pRprog;

        assign_area_vnum( vnum );				   /* OLC */
    }
    return;
}

/*
 * Load mobprogs section
 */
void load_mobprogs( FILE *fp )
{
    TCLPROG_CODE *pMprog;
    char *title;

    if ( area_last == NULL )
    {
        bugf("Load_mobprogs: no #AREA seen yet.");
        abort();
    }

    for ( ; ; )
    {
        int vnum;
        char letter;
        int iHash;

        letter		  = fread_letter( fp );
        if ( letter != '#' )
        {
            bugf("Load_mobprogs: # not found.");
            abort();
        }

        vnum		 = fread_number( fp );
        if ( vnum == 0 )
            break;

        fBootDb = FALSE;
        if ( get_mprog_index( vnum ) != NULL )
        {
            bugf( "Load_mobprogs: vnum %d duplicated.", vnum );
            abort();
        }
        fBootDb = TRUE;

        pMprog		= new_mprog_code_index();
        pMprog->area	= area_last;		/* OLC 1.1b */
        pMprog->vnum  	= vnum;
        title		= fread_string( fp );

        if (!strncmp(title,"Title ",6)) {
            pMprog->title	= str_dup(title+6);
            pMprog->code  	= fread_string( fp );
            free_string(title);
        } else {
            pMprog->code	= title;
        }

        iHash			= vnum % MAX_KEY_HASH;
        pMprog->next		= mprog_index_hash[iHash];
        mprog_index_hash[iHash]	= pMprog;

        assign_area_vnum( vnum );				   /* OLC */
    }
    return;
}

void load_area_properties( FILE *fp ) {
    char *word;
    PROPERTY_INDEX_TYPE *pProp;
    char *key,*stype;
    int type;

    word=fread_word(fp);
    while (str_cmp(word,"end")) {
        if (!str_cmp(word,"prop")) {
            key=str_dup(fread_word(fp));
            stype=fread_word(fp);
            type=table_find_name(stype,property_table);

            if (type==NO_FLAG) {
                bugf("%s is not a know property type.",stype);
                abort();
            }
            pProp=new_property_index();
            pProp->key=key;
            pProp->type=type;
            add_property_index_to_list(&area_last->properties,pProp);
            if (!does_property_exist(key,type)) {
                pProp=new_property_index();
                pProp->type=type;
                pProp->key=str_dup(key);
                add_property_index(pProp);
            }


        }
        word=fread_word(fp);
    }
    return;
}

/*
 * Translate all room exits from virtual to real.
 * Has to be done after all rooms are read in.
 * Check for bad reverse exits.
 */
void fix_exits( void )
{
    extern const int rev_dir [];
    ROOM_INDEX_DATA *pRoomIndex;
    ROOM_INDEX_DATA *to_room;
    EXIT_DATA *pexit;
    EXIT_DATA *pexit_rev;
    int iHash;
    int door;

    for ( iHash = 0; iHash < MAX_KEY_HASH; iHash++ )
    {
        for ( pRoomIndex  = room_index_hash[iHash];
              pRoomIndex != NULL;
              pRoomIndex  = pRoomIndex->next )
        {
            bool fexit;

            fexit = FALSE;
            for ( door = 0; door < DIR_MAX; door++ )
            {
                if ( ( pexit = pRoomIndex->exit[door] ) != NULL )
                {
                    if ( pexit->vnum <= 0
                         || get_room_index(pexit->vnum) == NULL)
                    {
                        pexit->to_room = NULL;
                        if (pexit->vnum > 0 && !IS_SET(pexit->rs_flags,EX_NOWARN))
                            bugf("Fix_exits: Wrong vnum %d for exit %d:%c.",
                                 pexit->vnum,pRoomIndex->vnum,*dir_name_short[door]);
                    }
                    else
                    {
                        fexit = TRUE;
                        pexit->to_room = get_room_index( pexit->vnum );
                    }
                }
            }
            if (!fexit)
                STR_SET_BIT(pRoomIndex->strbit_room_flags,ROOM_NO_MOB);
        }
    }

    for ( iHash = 0; iHash < MAX_KEY_HASH; iHash++ )
    {
        for ( pRoomIndex  = room_index_hash[iHash];
              pRoomIndex != NULL;
              pRoomIndex  = pRoomIndex->next )
        {
            for ( door = 0; door < DIR_MAX; door++ )
            {
                if ( ( pexit     = pRoomIndex->exit[door]       ) != NULL
                     &&   ( to_room   = pexit->to_room            ) != NULL
                     &&   ( pexit_rev = to_room->exit[rev_dir[door]] ) != NULL
                     &&   pexit_rev->to_room != pRoomIndex
                     &&   !IS_SET(pexit->rs_flags,EX_NOWARN))
                {
                    bugf( "Fix_exits: %5d:%c -> %5d:%c -> %5d.",
                          pRoomIndex->vnum, *dir_name_short[door],
                          to_room->vnum,    *dir_name_short[rev_dir[door]],
                            (pexit_rev->to_room == NULL)
                            ? 0 : pexit_rev->to_room->vnum );
                }
            }
        }
    }

    return;
}


/*
 * Repopulate areas periodically.
 */
void area_update( void )
{
    AREA_DATA *pArea;

    for ( pArea = area_first; pArea != NULL; pArea = pArea->next )
    {
        if ( ++pArea->age < 3 )
            continue;

        //
        // Check age and reset.
        // Note: Mud School resets every 3 minutes (not 15).
        //
        if ( (!pArea->empty && (pArea->nplayer == 0 || pArea->age >= 15))
             ||    pArea->age >= 31)
        {
            ROOM_INDEX_DATA *pRoomIndex;

            if (AREA_HAS_TRIGGER(pArea,ATRIG_PRERESET))
                if (ap_prereset_trigger(pArea)==FALSE)
                    continue;

            reset_area( pArea );
            wiznet(WIZ_RESETS,0,NULL,NULL,
                   "%s has just been reset.",pArea->name);

            ap_reset_trigger(pArea);

            pArea->age = number_range( 0, 3 );
            pRoomIndex = get_room_index( ROOM_VNUM_SCHOOL );
            if ( pRoomIndex != NULL && pArea == pRoomIndex->area )
                pArea->age = 15 - 2;
            else if (pArea->nplayer == 0)
                pArea->empty = TRUE;
        }
    }

    return;
}


/* OLC
 * Reset one room.  Called by reset_area and olc.
 */
void reset_room( ROOM_INDEX_DATA *pRoom )
{
    RESET_DATA *	pReset;
    CHAR_DATA *		pMob;
    OBJ_DATA *		pObj;
    CHAR_DATA *		LastMob = NULL;
    OBJ_DATA *		LastObj = NULL;
    TCLPROG_LIST *	pList,*pList_next;
    TCLPROG_CODE *	pProg;

    int		iExit;
    int		level = 0;
    bool	last;
    int		resetnumber;

    if ( !pRoom )
        return;

    if (ROOM_HAS_TRIGGER(pRoom,RTRIG_PRERESET))
        if (rp_prereset_trigger(pRoom)==FALSE)
            return;

    pMob	= NULL;
    last	= FALSE;

    for ( iExit = 0;  iExit < MAX_DIR;  iExit++ )
    {
        EXIT_DATA *pExit;
        if ( ( pExit = pRoom->exit[iExit] ) )
        {
            pExit->exit_info = pExit->rs_flags;
            if ( ( pExit->to_room != NULL )
                 && ( ( pExit = pExit->to_room->exit[rev_dir[iExit]] ) ) )
            {
                /* nail the other side */
                pExit->exit_info = pExit->rs_flags;
            }
        }
    }

    /* Initialise its room programs */

    pProg=get_rprog_index(pRoom->rprog_vnum);
    if ((pRoom->rprog_vnum!=pRoom->rprog_running_vnum) ||
            (pProg==NULL?0:pProg->version)!=pRoom->rprog_running_version) {
        tcl_delete_room_instance(pRoom);
        for ( pList = pRoom->rprogs; pList!=NULL;pList=pList_next) {
            pList_next=pList->next;
            free_rprog_list(pList);
        }
        pRoom->rprogs=NULL;
        STR_ZERO_BIT(pRoom->strbit_rprog_triggers,sizeof(pRoom->strbit_rprog_triggers));
        if (pProg!=NULL) {
            tcl_create_room_instance(pRoom);
            pRoom->rprog_delay=0;
            pRoom->rprog_timer=-1;
            pRoom->rprog_target=NULL;
        }
        pRoom->rprog_running_vnum=pRoom->rprog_vnum;
        pRoom->rprog_running_version=pProg==NULL?0:pProg->version;
    }

    resetnumber=0;
    for ( pReset = pRoom->reset_first; pReset != NULL; pReset = pReset->next ) {
        MOB_INDEX_DATA	*pMobIndex;
        OBJ_INDEX_DATA	*pObjIndex;
        OBJ_INDEX_DATA	*pObjToIndex;
        ROOM_INDEX_DATA	*pRoomIndex;
        int count,limit;

        resetnumber++;

        switch ( pReset->command )
        {
        default:
            bugf("Reset_room %d: bad command %c on line %d.",
                 pRoom->vnum,
                 pReset->command,
                 resetnumber);
            break;

        case 'M':
            if ( !( pMobIndex = get_mob_index( pReset->arg1 ) ) )
            {
                bugf("Reset_room %d: 'M': bad vnum %d for room %d on line %d",
                     pRoom->vnum,
                     pReset->arg1,
                     pRoom->vnum,
                     resetnumber);
                continue;
            }

            if ( pMobIndex->count >= pReset->arg2 )
            {
                last = FALSE;
                wiznet(WIZ_RESET_DEBUG,0,NULL,NULL,
                       "Room %d: too many global mobs on line %d",
                       pRoom->vnum,
                       resetnumber);
                break;
            }

            count = 0;
            for (pMob = pRoom->people; pMob != NULL; pMob = pMob->next_in_room)
                if (pMob->pIndexData == pMobIndex)
                {
                    count++;
                    if (count >= pReset->arg4)
                    {
                        last = FALSE;
                        wiznet(WIZ_RESET_DEBUG,0,NULL,NULL,
                               "Room %d: too many local mobs on line %d",
                               pRoom->vnum,
                               resetnumber);
                        break;
                    }
                }

            if (count >= pReset->arg4)
                break;

            if ((STR_IS_SET(pMobIndex->strbit_act,ACT_PC_BLOCKS_RESPAWN) ||
                 IS_SET(pRoom->area->area_flags,AREA_PC_BLOCKS_RESPAWN) ) &&
                    pRoom->area->nplayer > 0) {
                last = FALSE;
                wiznet(WIZ_RESET_DEBUG,0,NULL,NULL,
                       "Room %d: PC present in area on line %d",
                       pRoom->vnum,
                       resetnumber);
                break;
            }

            pMob = create_mobile( pMobIndex );
            pMob->resetted_at=pRoom->vnum;

            /*
         * Pet shop mobiles get ACT_PET set.
         */
        {
            ROOM_INDEX_DATA *pRoomIndexPrev;

            pRoomIndexPrev = get_room_index( pRoom->vnum - 1 );
            if ( pRoomIndexPrev
                 && STR_IS_SET(pRoomIndexPrev->strbit_room_flags,ROOM_PET_SHOP))
                STR_SET_BIT( pMob->strbit_act, ACT_PET);
        }

            pMob->zone = pRoom->area;
            char_to_room( pMob, pRoom );

            LastMob = pMob;
            level  = URANGE( 0, pMob->level - 2, LEVEL_HERO -1);
            last = TRUE;

            mp_load_trigger(pMob,NULL);

            break;

        case 'O':
            if ( !( pObjIndex = get_obj_index( pReset->arg1 ) ) )
            {
                bugf("Reset_room: 'O': bad vnum %d for arg1 for room %d",
                     pReset->arg1,pRoom->vnum);
                continue;
            }

            if ( !( pRoomIndex = get_room_index( pReset->arg3 ) ) )
            {
                bugf("Reset_room: 'O': bad vnum %d for arg3 for room %d",
                     pReset->arg3,pRoom->vnum);
                continue;
            }

            if ( count_obj_list( pObjIndex, pRoom->contents ) >= pReset->arg4 )
            {
                last = FALSE;
                wiznet(WIZ_RESET_DEBUG,0,NULL,NULL,
                       "Room %d: too many local objects on line %d",
                       pRoom->vnum,
                       resetnumber);
                break;
            }
            if ( pReset->arg2!=-1 && pObjIndex->count >= pReset->arg2 )
            {
                last = FALSE;
                wiznet(WIZ_RESET_DEBUG,0,NULL,NULL,
                       "Room %d: too many global objects on line %d",
                       pRoom->vnum,
                       resetnumber);
                break;
            }

            if ((STR_IS_SET(pObjIndex->strbit_extra_flags,ITEM_PC_BLOCKS_RESPAWN) ||
                 IS_SET(pRoom->area->area_flags,AREA_PC_BLOCKS_RESPAWN) ) &&
                    pRoom->area->nplayer > 0) {
                last = FALSE;
                wiznet(WIZ_RESET_DEBUG,0,NULL,NULL,
                       "Room %d: PC present in area on line %d",
                       pRoom->vnum,
                       resetnumber);
                break;
            }

            pObj = create_object( pObjIndex, number_fuzzy( level ) );
            pObj->cost = 0;
            pObj->resetted_at=pRoom->vnum;
            obj_to_room( pObj, pRoom );
            op_load_trigger(pObj,NULL);
            break;

        case 'P':
            if ((pObjIndex=get_obj_index(pReset->arg1))==NULL) {
                bugf("Reset_room: 'P': bad vnum %d for arg1 for room %d",
                     pReset->arg1,pRoom->vnum);
                continue;
            }

            if ((pObjToIndex=get_obj_index(pReset->arg3))==NULL) {
                bugf("Reset_room: 'P': bad vnum %d for arg3 for room %d",
                     pReset->arg3,pRoom->vnum);
                continue;
            }

            if (pReset->arg2 > 50) /* old format */
                limit = 6;
            else if (pReset->arg2 == -1) /* no limit */
                limit = 999;
            else
                limit = pReset->arg2;

            count=0;
            if (pRoom->area->nplayer > 0
                    //	    || (LastObj=get_obj_here_by_objtype(pObjToIndex))==NULL
                    || (LastObj=get_obj_here_by_objtype(pObjToIndex,pRoom->contents))==NULL
                    || (LastObj->in_room==NULL && !last)
                    || ( pObjIndex->count >= limit && number_range(0,4) != 0)
                    || (count = count_obj_list(pObjIndex,LastObj->contains))
                    > pReset->arg4 )
            {
                last = FALSE;
                wiznet(WIZ_RESET_DEBUG,0,NULL,NULL,
                       "Room %d: too many local objects in container on line %d",
                       pRoom->vnum,
                       resetnumber);
                break;
            }

            while (count < pReset->arg4)
            {
                pObj = create_object( pObjIndex, number_fuzzy(LastObj->level) );
                pObj->resetted_at=pRoom->vnum;
                obj_to_obj( pObj, LastObj );
                op_load_trigger(pObj,NULL);
                count++;
                if (pObjIndex->count >= limit)
                    break;
            }

            /*
         * Ensure that the container gets reset.	OLC 1.1b
         */
            if ( LastObj->item_type == ITEM_CONTAINER )
            {
                LastObj->value[1] = LastObj->pIndexData->value[1];
            }
            else
            {
                /* THIS SPACE INTENTIONALLY LEFT BLANK */
            }
            last = TRUE;
            break;

        case 'G':
        case 'E':
            if ( !( pObjIndex = get_obj_index( pReset->arg1 ) ) )
            {
                bugf("Reset_room: 'E' or 'G': bad vnum %d for arg1 for room %d",
                     pReset->arg1,pRoom->vnum);
                continue;
            }

            if ( !last )
                break;

            if ( !LastMob )
            {
                bugf("Reset_room: 'E' or 'G': null mob for vnum %d for room %d",
                     pReset->arg1,pRoom->vnum);
                last = FALSE;
                break;
            }

            if ( LastMob->pIndexData->pShop )	/* Shop-keeper? */
            {
                pObj = create_object( pObjIndex, 0);
                pObj->resetted_at=pRoom->vnum;
                STR_SET_BIT( pObj->strbit_extra_flags, ITEM_INVENTORY );
            }

            else
            {
                if (pReset->arg2 > 50) /* old format */
                    limit = 6;
                else if (pReset->arg2 == -1) /* no limit */
                    limit = 999;
                else
                    limit = pReset->arg2;

                if (pObjIndex->count < limit || number_range(0,4) == 0)
                {
                    pObj=create_object(pObjIndex,UMIN(number_fuzzy(level),
                                                      LEVEL_HERO - 1));
                    pObj->resetted_at=pRoom->vnum;
                }
                else {
                    wiznet(WIZ_RESET_DEBUG,0,NULL,NULL,
                           "Room %d: too many objects for mob on line %d",
                           pRoom->vnum,
                           resetnumber);
                    break;
                }
            }

            obj_to_char( pObj, LastMob );
            if ( pReset->command == 'E' )
                equip_char( LastMob, pObj, pReset->arg3 );
            last = TRUE;
            op_load_trigger(pObj,NULL);
            break;

        case 'D':
            break;

        case 'R':
#ifdef NOTYETDEFROOMRESETS
            if ( !( pRoomIndex = get_room_index( pReset->arg1 ) ) )
            {
                bugf("Reset_room: 'R': bad vnum %d for room %d",
                     pReset->arg1,pRoom->vnum);
                continue;
            }

        {
            EXIT_DATA *pExit;
            int d0;
            int d1;

            for ( d0 = 0; d0 < pReset->arg2 - 1; d0++ )
            {
                d1                   = number_range( d0, pReset->arg2-1 );
                pExit                = pRoomIndex->exit[d0];
                pRoomIndex->exit[d0] = pRoomIndex->exit[d1];
                pRoomIndex->exit[d1] = pExit;
            }
        }
#endif
            break;
        }
    }

    if (ROOM_HAS_TRIGGER(pRoom,RTRIG_RESET))
        rp_reset_trigger(pRoom);

    return;
}

/* OLC
 * Reset one area.
 */
void tcl_reset_area( AREA_DATA *pArea )
{
    TCLPROG_LIST *pList,*pList_next;
    TCLPROG_CODE *pProg;

    pProg=pArea->aprog;
    if ((pArea->aprog_enabled!=pArea->aprog_running) ||
            (pProg==NULL?0:pProg->version)!=pArea->aprog_running_version) {
        tcl_delete_area_instance(pArea);
        for ( pList = pArea->aprogs; pList!=NULL;pList=pList_next) {
            pList_next=pList->next;
            free_aprog_list(pList);
        }
        pArea->aprogs=NULL;
        STR_ZERO_BIT(pArea->strbit_aprog_triggers,sizeof(pArea->strbit_aprog_triggers));
        if (pProg && pArea->aprog_enabled) tcl_create_area_instance(pArea);
        pArea->aprog_running=pArea->aprog_enabled;
        pArea->aprog_running_version=pProg==NULL?0:pProg->version;
    }
}

void tcl_reset_all_areas(void) {
    AREA_DATA *pArea;

    for (pArea=area_first;pArea;pArea=pArea->next)
        tcl_reset_area(pArea);
}

void reset_area( AREA_DATA *pArea )
{
    ROOM_INDEX_DATA *pRoom;
    int  vnum;

    tcl_reset_area(pArea);

    for ( vnum = pArea->lvnum; vnum <= pArea->uvnum; vnum++ )
        if ( ( pRoom = get_room_index(vnum) ) )
            reset_room(pRoom);

    return;
}

/*
 * Create an instance of a mobile.
 */
CHAR_DATA *create_mobile( MOB_INDEX_DATA *pMobIndex )
{
    CHAR_DATA *mob;
    int i;
    EFFECT_DATA ef;

    if ( pMobIndex == NULL )
    {
        bugf( "Create_mobile: NULL pMobIndex.");
        abort();
    }

    mob = new_char();

    mob->pIndexData	= pMobIndex;

    mob->name		= str_dup( pMobIndex->player_name );	/* OLC */
    mob->id		= get_mob_id();
    mob->short_descr	= str_dup( pMobIndex->short_descr );	/* OLC */
    mob->long_descr	= str_dup( pMobIndex->long_descr );	/* OLC */
    mob->description	= str_dup( pMobIndex->description );	/* OLC */
    mob->spec_fun	= pMobIndex->spec_fun;
    mob->prompt		= NULL;
    STR_ZERO_BIT(mob->strbit_mprog_triggers,MPROG_MAX_TRIGGERS);
    mob->mprog_target   = NULL;
    mob->mprog_delay	= -1;
    mob->mprog_timer	= -1;

    if (pMobIndex->wealth == 0)
    {
        mob->silver = 0;
        mob->gold   = 0;
    }
    else
    {
        long wealth;

        wealth = number_range(pMobIndex->wealth/2, 3 * pMobIndex->wealth/2);
        mob->gold = number_range(wealth/200,wealth/100);
        mob->silver = wealth - (mob->gold * 100);
    }

    /* load in new style */
    {
        /* read from prototype */
        mob->group		= pMobIndex->group;
        STR_COPY_STR(mob->strbit_act,pMobIndex->strbit_act,MAX_FLAGS);
        STR_ZERO_BIT(mob->strbit_comm,MAX_FLAGS);
        STR_SET_BIT(mob->strbit_comm,COMM_NOCHANNELS);
        STR_SET_BIT(mob->strbit_comm,COMM_NOSHOUT);
        STR_SET_BIT(mob->strbit_comm,COMM_NOTELL);
        STR_COPY_STR(mob->strbit_affected_by,pMobIndex->strbit_affected_by,MAX_FLAGS);
        STR_COPY_STR(mob->strbit_affected_by2,pMobIndex->strbit_affected_by2,MAX_FLAGS);
        mob->alignment		= pMobIndex->alignment;
        mob->level		= pMobIndex->level;
        mob->hitroll		= pMobIndex->hitroll;
        mob->damroll		= pMobIndex->damage[DICE_BONUS];
        mob->max_hit		= dice(pMobIndex->hit[DICE_NUMBER],
                                   pMobIndex->hit[DICE_TYPE])
                + pMobIndex->hit[DICE_BONUS];
        if (GetCharProperty(mob,PROPERTY_INT,"max_hp",&i)) {
            mob->hit		= UMIN(mob->max_hit,i);
        } else {
            mob->hit		= mob->max_hit;
        }
        mob->max_mana		= dice(pMobIndex->mana[DICE_NUMBER],
                                   pMobIndex->mana[DICE_TYPE])
                + pMobIndex->mana[DICE_BONUS];
        mob->mana		= mob->max_mana;
        mob->max_move		= 100+mob->level*10;
        mob->move		= mob->max_move;
        mob->damage[DICE_NUMBER]= pMobIndex->damage[DICE_NUMBER];
        mob->damage[DICE_TYPE]	= pMobIndex->damage[DICE_TYPE];
        mob->dam_type		= pMobIndex->dam_type;
        if (mob->dam_type == 0)
            switch(number_range(1,3))
            {
            case (1): mob->dam_type = 3;        break;  /* slash */
            case (2): mob->dam_type = 7;        break;  /* pound */
            case (3): mob->dam_type = 11;       break;  /* pierce */
            }
        for (i = 0; i < 4; i++)
            mob->armor[i]	= pMobIndex->ac[i];
        STR_COPY_STR(mob->strbit_off_flags,pMobIndex->strbit_off_flags,MAX_FLAGS);
        STR_COPY_STR(mob->strbit_imm_flags,pMobIndex->strbit_imm_flags,MAX_FLAGS);
        STR_COPY_STR(mob->strbit_res_flags,pMobIndex->strbit_res_flags,MAX_FLAGS);
        STR_COPY_STR(mob->strbit_vuln_flags,pMobIndex->strbit_vuln_flags,MAX_FLAGS);
        mob->start_pos		= pMobIndex->start_pos;
        mob->default_pos	= pMobIndex->default_pos;
        mob->Sex		= pMobIndex->sex;
        if (mob->Sex == SEX_EITHER) /* random sex */
            mob->Sex = number_range(SEX_MALE,SEX_FEMALE);
        mob->race		= pMobIndex->race;
        mob->form		= pMobIndex->form;
        mob->parts		= pMobIndex->parts;
        mob->size		= pMobIndex->size;
        mob->material		= str_dup(pMobIndex->material);

        /* computed on the spot */

        for (i = 0; i < MAX_STATS; i ++)
            mob->perm_stat[i] = UMIN(25,11 + mob->level/4);

        if (STR_IS_SET(mob->strbit_act,ACT_WARRIOR))
        {
            mob->perm_stat[STAT_STR] += 3;
            mob->perm_stat[STAT_INT] -= 1;
            mob->perm_stat[STAT_CON] += 2;
        }

        if (STR_IS_SET(mob->strbit_act,ACT_THIEF))
        {
            mob->perm_stat[STAT_DEX] += 3;
            mob->perm_stat[STAT_INT] += 1;
            mob->perm_stat[STAT_WIS] -= 1;
        }

        if (STR_IS_SET(mob->strbit_act,ACT_CLERIC))
        {
            mob->perm_stat[STAT_WIS] += 3;
            mob->perm_stat[STAT_DEX] -= 1;
            mob->perm_stat[STAT_STR] += 1;
        }

        if (STR_IS_SET(mob->strbit_act,ACT_MAGE))
        {
            mob->perm_stat[STAT_INT] += 3;
            mob->perm_stat[STAT_STR] -= 1;
            mob->perm_stat[STAT_DEX] += 1;
        }

        if (STR_IS_SET(mob->strbit_off_flags,OFF_FAST))
            mob->perm_stat[STAT_DEX] += 2;

        mob->perm_stat[STAT_STR] += mob->size - SIZE_MEDIUM;
        mob->perm_stat[STAT_CON] += (mob->size - SIZE_MEDIUM) / 2;

        /* let's get some spell action */
        if (IS_AFFECTED(mob,EFF_SANCTUARY))
        {
            ZEROVAR(&ef,EFFECT_DATA);
            ef.where	 = TO_EFFECTS;
            ef.type      = gsn_sanctuary;
            ef.level     = mob->level;
            ef.duration  = -1;
            ef.location  = APPLY_NONE;
            ef.modifier  = 0;
            ef.bitvector = EFF_SANCTUARY;
            ef.casted_by = 0;
            effect_to_char( mob, &ef );
        }

        if (IS_AFFECTED(mob,EFF_HASTE))
        {
            ZEROVAR(&ef,EFFECT_DATA);
            ef.where	 = TO_EFFECTS;
            ef.type      = gsn_haste;
            ef.level     = mob->level;
            ef.duration  = -1;
            ef.location  = APPLY_DEX;
            ef.modifier  = 1 + (mob->level >= 18) + (mob->level >= 25) +
                    (mob->level >= 32);
            ef.bitvector = EFF_HASTE;
            ef.casted_by = 0;
            effect_to_char( mob, &ef );
        }

        if (IS_AFFECTED(mob,EFF_PROTECT_EVIL))
        {
            ZEROVAR(&ef,EFFECT_DATA);
            ef.where	 = TO_EFFECTS;
            ef.type	 = gsn_protection_evil;
            ef.level	 = mob->level;
            ef.duration	 = -1;
            ef.location	 = APPLY_SAVES;
            ef.modifier	 = -1;
            ef.bitvector = EFF_PROTECT_EVIL;
            ef.casted_by = 0;
            effect_to_char(mob,&ef);
        }

        if (IS_AFFECTED(mob,EFF_PROTECT_GOOD))
        {
            ZEROVAR(&ef,EFFECT_DATA);
            ef.where	 = TO_EFFECTS;
            ef.type      = gsn_protection_good;
            ef.level     = mob->level;
            ef.duration  = -1;
            ef.location  = APPLY_SAVES;
            ef.modifier  = -1;
            ef.bitvector = EFF_PROTECT_GOOD;
            ef.casted_by = 0;
            effect_to_char(mob,&ef);
        }
    }

    mob->position = mob->start_pos;

    /* Initialise its mob programs */
    tcl_create_char_instance(mob);

    /* link the mob to the world list */
    mob->next		= char_list;
    char_list		= mob;
    pMobIndex->count++;
    return mob;
}

/* duplicate a mobile exactly -- except inventory */
void clone_mobile(CHAR_DATA *parent, CHAR_DATA *clone)
{
    int i;
    EFFECT_DATA *pef;
    PROPERTY *prop;

    if ( parent == NULL || clone == NULL || !IS_NPC(parent))
        return;

    /* start fixing values */
    clone->name 	= str_dup(parent->name);
    clone->version	= parent->version;
    clone->short_descr	= str_dup(parent->short_descr);
    clone->long_descr	= str_dup(parent->long_descr);
    clone->description	= str_dup(parent->description);
    clone->group	= parent->group;
    clone->Sex		= parent->Sex;
    clone->class	= parent->class;
    clone->race		= parent->race;
    clone->level	= parent->level;
    clone->zone		= parent->zone;
    clone->trust	= 0;
    clone->timer	= parent->timer;
    clone->wait		= parent->wait;
    clone->hit		= parent->hit;
    clone->max_hit	= parent->max_hit;
    clone->mana		= parent->mana;
    clone->max_mana	= parent->max_mana;
    clone->move		= parent->move;
    clone->max_move	= parent->max_move;
    clone->gold		= parent->gold;
    clone->silver	= parent->silver;
    clone->exp		= parent->exp;
    STR_COPY_STR(clone->strbit_act,parent->strbit_act,MAX_FLAGS);
    STR_COPY_STR(clone->strbit_comm,parent->strbit_comm,MAX_FLAGS);
    STR_COPY_STR(clone->strbit_imm_flags,parent->strbit_imm_flags,MAX_FLAGS);
    STR_COPY_STR(clone->strbit_res_flags,parent->strbit_res_flags,MAX_FLAGS);
    STR_COPY_STR(clone->strbit_vuln_flags,parent->strbit_vuln_flags,MAX_FLAGS);
    clone->invis_level	= parent->invis_level;
    STR_COPY_STR(clone->strbit_affected_by,parent->strbit_affected_by,MAX_FLAGS);
    STR_COPY_STR(clone->strbit_affected_by2,parent->strbit_affected_by2,MAX_FLAGS);
    clone->position	= parent->position;
    clone->practice	= parent->practice;
    clone->train	= parent->train;
    clone->saving_throw	= parent->saving_throw;
    clone->alignment	= parent->alignment;
    clone->hitroll	= parent->hitroll;
    clone->damroll	= parent->damroll;
    clone->wimpy	= parent->wimpy;
    clone->form		= parent->form;
    clone->parts	= parent->parts;
    clone->size		= parent->size;
    clone->material	= str_dup(parent->material);
    STR_COPY_STR(clone->strbit_off_flags,parent->strbit_off_flags,MAX_FLAGS);
    clone->dam_type	= parent->dam_type;
    clone->start_pos	= parent->start_pos;
    clone->default_pos	= parent->default_pos;
    clone->spec_fun	= parent->spec_fun;

    STR_ZERO_BIT(clone->strbit_mprog_triggers,MPROG_MAX_TRIGGERS);
    clone->mprog_target   = NULL;
    clone->mprog_delay	= -1;
    clone->mprog_timer	= -1;

    for (i = 0; i < 4; i++)
        clone->armor[i]	= parent->armor[i];

    for (i = 0; i < MAX_STATS; i++)
    {
        clone->perm_stat[i]	= parent->perm_stat[i];
        clone->mod_stat[i]	= parent->mod_stat[i];
    }

    for (i = 0; i < 3; i++)
        clone->damage[i]	= parent->damage[i];

    /* now add the effects */
    for (pef = parent->affected; pef != NULL; pef = pef->next)
        effect_to_char(clone,pef);

    /* and the properties */
    for (prop = parent->property; prop ; prop = prop->next)
        if (prop->sValue==NULL||prop->sValue[0]==0)
            SetCharProperty(clone,prop->propIndex->type,
                            prop->propIndex->key,&prop->iValue);
        else
            SetCharProperty(clone,prop->propIndex->type,
                            prop->propIndex->key,prop->sValue);

    /* And lets initialise its mob programs */
    tcl_create_char_instance(clone);
}




/*
 * Create an instance of an object.
 * level isn't used anymore
 */
OBJ_DATA *create_object_data( OBJ_INDEX_DATA *pObjIndex, int level )
{
    EFFECT_DATA *pef;
    OBJ_DATA *obj;

    if ( pObjIndex == NULL )
    {
        bugf( "Create_object: NULL pObjIndex.");
        abort();
    }

    obj = new_obj();

    obj->pIndexData	= pObjIndex;
    obj->in_room	= NULL;
    obj->enchanted	= FALSE;

    obj->level = pObjIndex->level;
    obj->wear_loc	= -1;

    obj->id		= get_obj_id();
    obj->name		= str_dup( pObjIndex->name );		/* OLC */
    obj->short_descr	= str_dup( pObjIndex->short_descr );	/* OLC */
    obj->description	= str_dup( pObjIndex->description );	/* OLC */
    obj->material	= str_dup(pObjIndex->material);
    obj->condition	= pObjIndex->condition;
    obj->item_type	= pObjIndex->item_type;
    STR_COPY_STR(obj->strbit_extra_flags,pObjIndex->strbit_extra_flags,MAX_FLAGS);
    STR_COPY_STR(obj->strbit_extra_flags2,pObjIndex->strbit_extra_flags2,MAX_FLAGS);
    obj->wear_flags	= pObjIndex->wear_flags;
    obj->value[0]	= pObjIndex->value[0];
    obj->value[1]	= pObjIndex->value[1];
    obj->value[2]	= pObjIndex->value[2];
    obj->value[3]	= pObjIndex->value[3];
    obj->value[4]	= pObjIndex->value[4];
    obj->weight		= pObjIndex->weight;
    obj->cost		= pObjIndex->cost;

    STR_ZERO_BIT(obj->strbit_oprog_triggers,OPROG_MAX_TRIGGERS);
    obj->oprog_target   = NULL;
    obj->oprog_delay    = -1;
    obj->oprog_timer    = -1;

    /*
     * Mess with object properties.
     */
    switch ( obj->item_type )
    {
    default:
        bugf( "Create_object: vnum %d bad type.", pObjIndex->vnum );
        break;

    case ITEM_LIGHT:
        if (obj->value[2] == 999)
            obj->value[2] = -1;
        break;

    case ITEM_FURNITURE:
    case ITEM_TRASH:
    case ITEM_CONTAINER:
    case ITEM_DRINK_CON:
    case ITEM_KEY:
    case ITEM_FOOD:
    case ITEM_BOAT:
    case ITEM_CORPSE_NPC:
    case ITEM_CORPSE_PC:
    case ITEM_FOUNTAIN:
    case ITEM_MAP:
    case ITEM_CLOTHING:
    case ITEM_PORTAL:
    case ITEM_PROTECT:
    case ITEM_TREASURE:
    case ITEM_WARP_STONE:
    case ITEM_ROOM_KEY:
    case ITEM_GEM:
    case ITEM_JEWELRY:
    case ITEM_QUEST:
    case ITEM_CLAN:
        break;

#ifdef HAS_JUKEBOX
    case ITEM_JUKEBOX:
    {
        int i;
        for (i = 1; i < 5; i++)
            obj->value[i] = -1;
        break;
    }
#endif

    case ITEM_SCROLL:
        break;

    case ITEM_WAND:
    case ITEM_STAFF:
        break;

    case ITEM_WEAPON:
        break;

    case ITEM_ARMOR:
        break;

    case ITEM_POTION:
    case ITEM_PILL:
        break;

    case ITEM_MONEY:
        break;
    }

    for (pef = pObjIndex->affected; pef != NULL; pef = pef->next)
        if ( pef->location == APPLY_SPELL_EFFECT )
            effect_to_obj(obj,pef);

    obj->next		= object_list;
    object_list		= obj;
    pObjIndex->count++;

    return obj;
}
void create_object_tcl( OBJ_DATA *obj ) {
    /* Initialise its obj programs */
    tcl_create_obj_instance(obj);
}
OBJ_DATA *create_object( OBJ_INDEX_DATA *pObjIndex, int level ) {
    OBJ_DATA *obj;

    obj=create_object_data(pObjIndex,level);
    create_object_tcl(obj);
    return obj;
}

/* duplicate an object exactly -- except contents */
void clone_object(OBJ_DATA *parent, OBJ_DATA *clone)
{
    int i;
    EFFECT_DATA *pef;
    EXTRA_DESCR_DATA *ed,*ed_new;
    PROPERTY *prop;

    if (parent == NULL || clone == NULL)
        return;

    /* start fixing the object */
    clone->name 	= str_dup(parent->name);
    clone->short_descr 	= str_dup(parent->short_descr);
    clone->description	= str_dup(parent->description);
    clone->item_type	= parent->item_type;
    STR_COPY_STR(clone->strbit_extra_flags,parent->strbit_extra_flags,MAX_FLAGS);
    STR_COPY_STR(clone->strbit_extra_flags2,parent->strbit_extra_flags2,MAX_FLAGS);
    clone->wear_flags	= parent->wear_flags;
    clone->weight	= parent->weight;
    clone->cost		= parent->cost;
    clone->level	= parent->level;
    clone->condition	= parent->condition;
    clone->material	= str_dup(parent->material);
    clone->timer	= parent->timer;

    STR_ZERO_BIT(clone->strbit_oprog_triggers,OPROG_MAX_TRIGGERS);
    clone->oprog_target	= NULL;
    clone->oprog_delay	= -1;
    clone->oprog_timer	= -1;

    for (i = 0;  i < 5; i ++)
        clone->value[i]	= parent->value[i];

    /* effects */
    clone->enchanted	= parent->enchanted;

    for (pef = parent->affected; pef != NULL; pef = pef->next)
        effect_to_obj(clone,pef);

    /* extended desc */
    for (ed = parent->extra_descr; ed != NULL; ed = ed->next)
    {
        ed_new                  = new_extra_descr();
        ed_new->keyword    	= str_dup( ed->keyword);
        ed_new->description     = str_dup( ed->description );
        ed_new->next           	= clone->extra_descr;
        clone->extra_descr  	= ed_new;
    }

    /* and the properties */
    for (prop = parent->property; prop ; prop = prop->next)
        if (prop->sValue==NULL||prop->sValue[0]==0)
            SetObjectProperty(clone,prop->propIndex->type,
                              prop->propIndex->key,&prop->iValue);
        else
            SetObjectProperty(clone,prop->propIndex->type,
                              prop->propIndex->key,prop->sValue);

    /* Initialise its obj programs */
    tcl_create_obj_instance(clone);
}



/*
 * Clear a new character.
 */
void clear_char( CHAR_DATA *ch )
{
    static CHAR_DATA ch_zero;
    int i;

    *ch				= ch_zero;
    ch->name			= &str_empty[0];
    ch->short_descr		= &str_empty[0];
    ch->long_descr		= &str_empty[0];
    ch->description		= &str_empty[0];
    ch->prompt                  = &str_empty[0];
    ch->logon			= current_time;
    for (i = 0; i < 4; i++)
        ch->armor[i]		= 100;
    ch->position		= POS_STANDING;
    ch->hit			= 20;
    ch->max_hit			= 20;
    ch->mana			= 100;
    ch->max_mana		= 100;
    ch->move			= 100;
    ch->max_move		= 100;
    ch->on			= NULL;
    for (i = 0; i < MAX_STATS; i ++)
    {
        ch->perm_stat[i] = 13;
        ch->mod_stat[i] = 0;
    }
    return;
}

/*
 * Get an extra description from a list.
 */
EXTRA_DESCR_DATA *get_extra_descr( const char *name, EXTRA_DESCR_DATA *ed )
{
    for ( ; ed != NULL; ed = ed->next ) {
        if ( is_name( (char *) name, ed->keyword ) )
            return ed;
    }
    return NULL;
}



/*
 * Translates mob virtual number to its mob index struct.
 * Hash table lookup.
 */
MOB_INDEX_DATA *get_mob_index( int vnum )
{
    MOB_INDEX_DATA *pMobIndex;

    if (vnum<0 || vnum>MAX_VNUMS) return NULL;

    for ( pMobIndex  = mob_index_hash[vnum % MAX_KEY_HASH];
          pMobIndex != NULL;
          pMobIndex  = pMobIndex->next )
    {
        if ( pMobIndex->vnum == vnum )
            return pMobIndex;
    }

    if ( fBootDb )
    {
        bugf( "Get_mob_index: bad vnum %d.", vnum );
        abort();
    }

    return NULL;
}



/*
 * Translates mob virtual number to its obj index struct.
 * Hash table lookup.
 */
OBJ_INDEX_DATA *get_obj_index( int vnum )
{
    OBJ_INDEX_DATA *pObjIndex;

    if (vnum<0 || vnum>MAX_VNUMS) return NULL;

    for ( pObjIndex  = obj_index_hash[vnum % MAX_KEY_HASH];
          pObjIndex != NULL;
          pObjIndex  = pObjIndex->next )
    {
        if ( pObjIndex->vnum == vnum )
            return pObjIndex;
    }

    if ( fBootDb )
    {
        bugf( "Get_obj_index: bad vnum %d.", vnum );
        abort();
    }

    return NULL;
}



/*
 * Translates mob virtual number to its room index struct.
 * Hash table lookup.
 */
ROOM_INDEX_DATA *get_room_index( int vnum )
{
    ROOM_INDEX_DATA *pRoomIndex;

    if (vnum<0 || vnum>MAX_VNUMS) return NULL;

    for ( pRoomIndex  = room_index_hash[vnum % MAX_KEY_HASH];
          pRoomIndex != NULL;
          pRoomIndex  = pRoomIndex->next )
    {
        if ( pRoomIndex->vnum == vnum )
            return pRoomIndex;
    }

    if ( fBootDb )
    {
        bugf( "Get_room_index: bad vnum %d.", vnum );
        abort();
    }

    return NULL;
}

TCLPROG_CODE *get_mprog_index( int vnum )
{
    TCLPROG_CODE *prg;

    if (vnum<0 || vnum>MAX_VNUMS) return NULL;

    for ( prg  = mprog_index_hash[vnum % MAX_KEY_HASH];
          prg != NULL;
          prg  = prg->next )
    {
        if ( prg->vnum == vnum )
            return prg;
    }

    if ( fBootDb )
    {
        bugf( "get_mprog_index: bad vnum %d.", vnum );
        abort();
    }

    return NULL;
}

TCLPROG_CODE *get_oprog_index( int vnum )
{
    TCLPROG_CODE *prg;

    if (vnum<0 || vnum>MAX_VNUMS) return NULL;

    for ( prg  = oprog_index_hash[vnum % MAX_KEY_HASH];
          prg != NULL;
          prg  = prg->next )
    {
        if ( prg->vnum == vnum )
            return prg;
    }

    if ( fBootDb )
    {
        bugf( "get_oprog_index: bad vnum %d.", vnum );
        abort();
    }

    return NULL;
}

TCLPROG_CODE *get_rprog_index( int vnum )
{
    TCLPROG_CODE *prg;

    if (vnum<0 || vnum>MAX_VNUMS) return NULL;

    for ( prg  = rprog_index_hash[vnum % MAX_KEY_HASH];
          prg != NULL;
          prg  = prg->next )
    {
        if ( prg->vnum == vnum )
            return prg;
    }

    if ( fBootDb )
    {
        bugf( "get_rprog_index: bad vnum %d.", vnum );
        abort();
    }

    return NULL;
}

TCLPROG_CODE *get_aprog_index( int vnum )
{
    AREA_DATA *area;

    if (vnum<0 || vnum>MAX_VNUMS) return NULL;

    area=get_area_data(vnum);

    if (area && area->aprog)
        return area->aprog;

    if ( fBootDb )
    {
        bugf( "get_aprog_index: bad vnum %d.", vnum );
        abort();
    }

    return NULL;
}



void do_areas( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;
    int level=ch->level;
    int counter=0;

    if (argument[0] == '\0' || is_number(argument))
    {
        if (argument[0]=='\0') {
            send_to_char("Areas in your level range are:\n\r",ch);
        } else {
            level=atoi(argument);
            sprintf_to_char(ch,"Areas for level %d:\n\r",level);
        }

        for(pArea=area_first;pArea;pArea=pArea->next) {
            if (IS_SET(pArea->area_flags, AREA_UNFINISHED)) continue;
            if (level>pArea->ulevel) continue;
            if (level<pArea->llevel) continue;
            if (pArea->uvnum==0 || pArea->lvnum==0) continue;

            sprintf_to_char(ch,"%02d-%s%02d %c %-10s %-20s%s",
                            pArea->llevel,
                            pArea->ulevel>99?"":"-",
                            pArea->ulevel,
                            IS_SET(pArea->area_flags, AREA_UNFINISHED)?'*':'-',
                            pArea->creator,
                            pArea->name,counter++%2?"\n\r":"");
        }
        if (counter++%2)
            send_to_char("\n\r",ch);
        return;
    }

    if (!str_prefix(argument,"all")) {
        BUFFER *buf=new_buf();
        char s[MSL];

        for(pArea=area_first;pArea;pArea=pArea->next) {
            sprintf( s, "%02d-%s%02d %c %-10s %-20s%s",
                     pArea->llevel,
                     pArea->ulevel>99?"":"-",
                     pArea->ulevel,
                     IS_SET(pArea->area_flags, AREA_UNFINISHED)?'*':'-',
                     pArea->creator,
                     pArea->name,counter++%2?"\n\r":"");
            add_buf(buf,s);
        }
        if (counter++%2)
            add_buf(buf,"\n\r");
        page_to_char( buf_string(buf),ch);
        free_buf(buf);
        return;
    }

    if (!str_prefix(argument,"explored")) {
        int been,seenmob,seenobject,i,killed,used;
        BUFFER *buf=new_buf();
        char s[MSL];

        sprintf( s, "room - objects     - mobiles     - levels - creator    area name\n\r");
        add_buf( buf,s);
        sprintf( s, "       seen - used - seen - kill\n\r");
        add_buf( buf,s);
        for(pArea=area_first;pArea;pArea=pArea->next) {
            if (IS_SET(pArea->area_flags, AREA_UNFINISHED)) continue;
            if (level>pArea->ulevel) continue;
            if (level<pArea->llevel) continue;
            if (pArea->uvnum==0 || pArea->lvnum==0) continue;

            been=0;
            killed=0;
            seenmob=0;
            seenobject=0;
            used=0;
            for (i=pArea->lvnum;i<=pArea->uvnum;i++)
                if (STR_IS_SET(ch->pcdata->seenthatmob,i))
                    seenmob++;
            for (i=pArea->lvnum;i<=pArea->uvnum;i++)
                if (STR_IS_SET(ch->pcdata->seenthatobject,i))
                    seenobject++;
            for (i=pArea->lvnum;i<=pArea->uvnum;i++)
                if (STR_IS_SET(ch->pcdata->beeninroom,i))
                    been++;
            for (i=pArea->lvnum;i<=pArea->uvnum;i++)
                if (STR_IS_SET(ch->pcdata->usedthatobject,i))
                    used++;
            for (i=pArea->lvnum;i<=pArea->uvnum;i++)
                if (STR_IS_SET(ch->pcdata->killedthatmob,i))
                    killed++;
            sprintf( s, "%3d%% - %3d%% - %3d%% - %3d%% - %3d%% - %02d-%s%02d - %-10s %-20s\n\r",
                     pArea->vnum_rooms_in_use==0?100:
                                                 100*been/pArea->vnum_rooms_in_use,
                     pArea->vnum_objects_in_use==0?100:
                                                   100*seenobject/pArea->vnum_objects_in_use,
                     pArea->vnum_objects_in_use==0?100:
                                                   100*used/pArea->vnum_objects_in_use,
                     pArea->vnum_mobs_in_use==0?100:
                                                100*seenmob/pArea->vnum_mobs_in_use,
                     pArea->vnum_mobs_in_use==0?100:
                                                100*killed/pArea->vnum_mobs_in_use,
                     pArea->llevel,
                     pArea->ulevel>99?"":"-",
                     pArea->ulevel,
                     pArea->creator,
                     pArea->name);
            add_buf( buf,s);
        }
        page_to_char(buf_string(buf),ch);
        free_buf(buf);
        return;
    }

    if (!str_prefix(argument,"explored all")) {
        int been,seenmob,seenobject,i,killed,used;
        BUFFER *buf=new_buf();
        char s[MSL];

        sprintf( s, "room - objects     - mobiles     - levels - creator    area name\n\r");
        add_buf( buf,s);
        sprintf( s, "       seen - used - seen - kill\n\r");
        add_buf( buf,s);
        for(pArea=area_first;pArea;pArea=pArea->next) {
            been=0;
            killed=0;
            seenmob=0;
            seenobject=0;
            used=0;
            for (i=pArea->lvnum;i<=pArea->uvnum;i++)
                if (STR_IS_SET(ch->pcdata->seenthatmob,i))
                    seenmob++;
            for (i=pArea->lvnum;i<=pArea->uvnum;i++)
                if (STR_IS_SET(ch->pcdata->seenthatobject,i))
                    seenobject++;
            for (i=pArea->lvnum;i<=pArea->uvnum;i++)
                if (STR_IS_SET(ch->pcdata->beeninroom,i))
                    been++;
            for (i=pArea->lvnum;i<=pArea->uvnum;i++)
                if (STR_IS_SET(ch->pcdata->usedthatobject,i))
                    used++;
            for (i=pArea->lvnum;i<=pArea->uvnum;i++)
                if (STR_IS_SET(ch->pcdata->killedthatmob,i))
                    killed++;
            sprintf( s, "%3d%% - %3d%% - %3d%% - %3d%% - %3d%% - %02d-%s%02d - %-10s %-20s\n\r",
                     pArea->vnum_rooms_in_use==0?100:
                                                 100*been/pArea->vnum_rooms_in_use,
                     pArea->vnum_objects_in_use==0?100:
                                                   100*seenobject/pArea->vnum_objects_in_use,
                     pArea->vnum_objects_in_use==0?100:
                                                   100*used/pArea->vnum_objects_in_use,
                     pArea->vnum_mobs_in_use==0?100:
                                                100*seenmob/pArea->vnum_mobs_in_use,
                     pArea->vnum_mobs_in_use==0?100:
                                                100*killed/pArea->vnum_mobs_in_use,
                     pArea->llevel,
                     pArea->ulevel>99?"":"-",
                     pArea->ulevel,
                     pArea->creator,
                     pArea->name);
            add_buf( buf,s);
        }
        page_to_char(buf_string(buf),ch);
        free_buf(buf);
        return;
    }

    send_to_char("Syntax: area [all | explored | explored all | <level>]\n\r",ch);
    return;
}


void do_dump( CHAR_DATA *ch, char *argument )
{
    int count,num_pcs,eff_count;
    CHAR_DATA *fch;
    MOB_INDEX_DATA *pMobIndex;
    PC_DATA *pc;
    OBJ_DATA *obj;
    OBJ_INDEX_DATA *pObjIndex;
    ROOM_INDEX_DATA *room;
    EXIT_DATA *exit;
    DESCRIPTOR_DATA *d;
    EFFECT_DATA *af;
    FILE *fp;
    int vnum,nMatch = 0, i;

    /* open file */
    fclose(fpReserve);

    fp=fopen("scrolls.dmp","w");
    fprintf(fp,"vnum,cost,level,spelllevel,numberofspells,spells...\n");
    for (vnum=0;vnum<MAX_VNUMS;vnum++) {
        if ( ( (pObjIndex = get_obj_index(vnum)) != NULL )
             && (pObjIndex->item_type == ITEM_SCROLL) ) {
            fprintf(fp,"%d,",pObjIndex->vnum);
            fprintf(fp,"%d,%d,%d,",
                    pObjIndex->cost,pObjIndex->level,pObjIndex->value[0]);
            count=0;
            for (i=1;i<5;i++)
                count+=pObjIndex->value[i]>0;

            fprintf(fp,"%d,%s,%s,%s,%s\n",
                    count,
                    pObjIndex->value[1]<0?"":skill_name(pObjIndex->value[1],ch),
                    pObjIndex->value[2]<0?"":skill_name(pObjIndex->value[2],ch),
                    pObjIndex->value[3]<0?"":skill_name(pObjIndex->value[3],ch),
                    pObjIndex->value[4]<0?"":skill_name(pObjIndex->value[4],ch));
        }
    }
    fclose(fp);

    fp=fopen("potions.dmp","w");
    fprintf(fp,"vnum,cost,level,spelllevel,numberofspells,spells...\n");
    for (vnum=0;vnum<MAX_VNUMS;vnum++) {
        if ( ( (pObjIndex = get_obj_index(vnum)) != NULL )
             && (pObjIndex->item_type == ITEM_POTION) ) {
            fprintf(fp,"%d,",pObjIndex->vnum);
            fprintf(fp,"%d,%d,%d,",
                    pObjIndex->cost,pObjIndex->level,pObjIndex->value[0]);
            count=0;
            for (i=1;i<5;i++)
                count+=pObjIndex->value[i]>0;

            fprintf(fp,"%d,%s,%s,%s,%s\n",
                    count,
                    pObjIndex->value[1]<0?"":skill_name(pObjIndex->value[1],ch),
                    pObjIndex->value[2]<0?"":skill_name(pObjIndex->value[2],ch),
                    pObjIndex->value[3]<0?"":skill_name(pObjIndex->value[3],ch),
                    pObjIndex->value[4]<0?"":skill_name(pObjIndex->value[4],ch));
        }
    }
    fclose(fp);

    fp = fopen("mem.dmp","w");

    /* report use of data structures */

    num_pcs = 0;
    eff_count = 0;

    /* mobile prototypes */
    fprintf(fp,"MobProt	%4d (%8ld bytes)\n",
            mob_index_allocated, mob_index_allocated * ((long)sizeof(*pMobIndex)));

    /* mobs */
    count = 0;
    for (fch = char_list; fch != NULL; fch = fch->next)
    {
        count++;
        if (fch->pcdata != NULL)
            num_pcs++;
        for (af = fch->affected; af != NULL; af = af->next)
            eff_count++;
    }
    fprintf(fp,"Mobs	%4d (%8ld bytes)\n",count, count * ((long)sizeof(*fch)));

    /* pcdata */
    fprintf(fp,"Pcdata	%4d (%8ld bytes)\n",num_pcs, num_pcs * ((long)sizeof(*pc)));

    /* descriptors */
    count = 0;
    for (d = descriptor_list; d != NULL; d = d->next)
        count++;

    fprintf(fp, "Descs	%4d (%8ld bytes)\n", count, count * ((long)sizeof(*d)));

    /* object prototypes */
    for ( vnum = 0; nMatch < max_vnum_obj; vnum++ )
        if ( ( pObjIndex = get_obj_index( vnum ) ) != NULL )
        {
            for (af = pObjIndex->affected; af != NULL; af = af->next)
                eff_count++;
            nMatch++;
        }

    fprintf(fp,"ObjProt	%4d (%8ld bytes)\n",
            max_vnum_obj, max_vnum_obj * ((long)sizeof(*pObjIndex)));


    /* objects */
    count = 0;
    for (obj = object_list; obj != NULL; obj = obj->next)
    {
        count++;
        for (af = obj->affected; af != NULL; af = af->next)
            eff_count++;
    }

    fprintf(fp,"Objs	%4d (%8ld bytes)\n", count, count * ((long)sizeof(*obj)));

    /* effects */
    fprintf(fp,"Effects	%4d (%8ld bytes)\n", eff_count, eff_count * ((long)sizeof(*af)));

    /* rooms */
    fprintf(fp,"Rooms	%4d (%8ld bytes)\n",
            room_index_allocated, room_index_allocated * ((long)sizeof(*room)));

    /* exits */
    fprintf(fp,"Exits	%4d (%8ld bytes)\n",
            exit_allocated, exit_allocated * ((long)sizeof(*exit)));

    fclose(fp);

    /* start printing out mobile data */
    fp = fopen("mob.dmp","w");

    fprintf(fp,"\nMobile Analysis\n");
    fprintf(fp,  "---------------\n");
    nMatch = 0;
    for (vnum = 0; nMatch < max_vnum_mob; vnum++)
        if ((pMobIndex = get_mob_index(vnum)) != NULL)
        {
            nMatch++;
            fprintf(fp,"#%-4d %3d active %3d killed     %s\n",
                    pMobIndex->vnum,pMobIndex->count,
                    pMobIndex->killed,pMobIndex->short_descr);
        }
    fclose(fp);

    /* start printing out object data */
    fp = fopen("obj.dmp","w");

    fprintf(fp,"\nObject Analysis\n");
    fprintf(fp,  "---------------\n");
    nMatch = 0;
    for (vnum = 0; nMatch < max_vnum_obj; vnum++)
        if ((pObjIndex = get_obj_index(vnum)) != NULL)
        {
            nMatch++;
            fprintf(fp,"#%-4d %3d active %3d reset      %s\n",
                    pObjIndex->vnum,pObjIndex->count,
                    pObjIndex->reset_num,pObjIndex->short_descr);
        }

    /* close file */
    fclose(fp);
    fpReserve = fopen( NULL_FILE, "r" );
}

/*
 * Generate a random door.
 */
int number_door( void )
{
    int door;

    while ( ( door = number_mm() & (8-1) ) >= DIR_MAX)
        ;

    return door;
}

/*
 * This function is here to aid in debugging.
 * If the last expression in a function is another function call,
 *   gcc likes to generate a JMP instead of a CALL.
 * This is called "tail chaining."
 * It hoses the debugger call stack for that call.
 * So I make this the last call in certain critical functions,
 *   where I really need the call stack to be right for debugging!
 *
 * If you don't understand this, then LEAVE IT ALONE.
 * Don't remove any calls to tail_chain anywhere.
 *
 * -- Furey
 */
void tail_chain( void )
{
    return;
}
