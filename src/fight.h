//
// $Id: fight.h,v 1.3 2007/11/25 13:32:49 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef INCLUDED_FIGHT_H
#define INCLUDED_FIGHT_H

// fight2.c
void	rescue			(CHAR_DATA *ch,CHAR_DATA *victim);
void	berserker_rage		(CHAR_DATA *ch);


// fight.c
bool	in_safe_kill_room	(CHAR_DATA *killer,CHAR_DATA *victim);
void	violence_update		(void);
void	check_assist		(CHAR_DATA *ch,CHAR_DATA *victim);
void	multi_hit		(CHAR_DATA *ch,CHAR_DATA *victim,int dt);
int	same_group		(CHAR_DATA *ch1,CHAR_DATA *ch2);
int	same_alignment		(CHAR_DATA *ch1,CHAR_DATA *ch2);
int	same_race		(CHAR_DATA *ch1,CHAR_DATA *ch2);
int	same_mob		(CHAR_DATA *ch1,CHAR_DATA *ch2);
void	mob_hit			(CHAR_DATA *ch,CHAR_DATA *victim,int dt);
void	one_hit			(CHAR_DATA *ch,CHAR_DATA *victim,int dt,
				 bool secondary);
void	one_hit_variable	(CHAR_DATA *ch,CHAR_DATA *victim,int dt,
				 int multiplier,bool secondary);
bool	damage			(CHAR_DATA *ch,CHAR_DATA *victim,int dam,
				 int dt,int dam_type,bool show,OBJ_DATA *obj);
bool	damage_old		(CHAR_DATA *ch,CHAR_DATA *victim ,int dam,
				 int dt,int dam_type,bool show);
bool	allow_killstealing	(CHAR_DATA *ch,CHAR_DATA *victim);
bool	is_safe			(CHAR_DATA *ch,CHAR_DATA *victim);
bool	is_safe_spell		(CHAR_DATA *ch,CHAR_DATA *victim,bool area);
void	check_killer		(CHAR_DATA *ch,CHAR_DATA *victim);
bool	check_parry		(CHAR_DATA *ch,CHAR_DATA *victim);
bool	check_shield_block	(CHAR_DATA *ch,CHAR_DATA *victim);
bool	check_dodge		(CHAR_DATA *ch,CHAR_DATA *victim);
void	update_pos		(CHAR_DATA *victim);
void	set_fighting		(CHAR_DATA *ch,CHAR_DATA *victim);
void	stop_fighting		(CHAR_DATA *ch,bool fBoth);
OBJ_DATA *make_corpse		(CHAR_DATA *ch,CHAR_DATA *killer);
void	death_cry		(CHAR_DATA *ch,CHAR_DATA *killer);
void	update_mobkill_table	(MOB_INDEX_DATA *mid);
OBJ_DATA *raw_kill		(CHAR_DATA *victim,CHAR_DATA *killer);
void	zap_char_obj		(OBJ_DATA *obj,bool removedfromequipment);
void	group_gain		(CHAR_DATA *ch,CHAR_DATA *victim);
int	xp_compute		(CHAR_DATA *gch,CHAR_DATA *victim,
				 int total_levels);
void	dam_message		(CHAR_DATA *ch,CHAR_DATA *victim,int dam,
				 int dt,bool immune);
void	dam_message_obj		(CHAR_DATA *ch,CHAR_DATA *victim,OBJ_DATA *obj,
				 int dam,int dt,bool immune);


#endif	// INCLUDED_FIGHT_H
