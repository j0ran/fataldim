//
// $Id: ban.h,v 1.3 2002/09/02 18:04:26 jodocus Exp $
//


//
// Site ban structure.
//

#define BAN_SUFFIX		(A)
#define BAN_PREFIX		(B)

#define BAN_PERMANENT		(C)
#define BAN_TEMP		(D)
#define BAN_TIMED		(E)

#define BAN_NONEWBIE		(F)
#define BAN_IMMORTALONLY	(G)

struct	ban_data {
    bool	valid;
    BAN_DATA *	next;
    long	ban_flags;
    int		level;
    time_t	ban_wearoff;
    char *	name;
};

void	load_bans		(void);
bool	checkban_siteban	(char *site);
bool	checkban_newbie		(char *site);
bool	checkban_immortalonly	(char *site);
void	ban_update		(void);

//
// Lockout structures
//

struct lockout_data {
    bool		valid;
    LOCKOUT_DATA *	next;
    bool		ipv6;
    HOST_DATA		Host;
    int			attempts;
    time_t		last_attempt;
};

extern	LOCKOUT_DATA *	lockout_list;
bool	check_lockout		(const bool ipv6, const HOST_DATA Host, bool test_only);
void	lockout_update		(void);

//
// Bad names structure.
//

struct badname_data {
    bool		valid;
    BADNAME_DATA *	next;
    char *		name;
};

extern	BADNAME_DATA *	badname_list;
void	load_badnames		(void);
bool	checkbadname		(char *name);
