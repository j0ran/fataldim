//
// $Id: fd_network.c,v 1.43 2009/05/13 18:57:29 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

//
// This part contains all the network-related functions, like opening
// a socket, reading from it, writing to it, closing it and the one
// giant game_loop.
//

#define TELOPTS
#define TELCMDS
#include "merc.h"
#include "fd_mxp.h"
#ifdef I3
#include "intermud3/fd_i3.h"
#endif
#ifdef THREADED_DNS
#include "rdns_cache.h"
#endif


//
// Telnet Options.
//
// ECHO		means that the characters typed in should be printed back
//		on the screen. This is to disable passwords.
// BINARY	allow 8 bit binary characters instead of 7 bits only
// SGA		suppress go ahead. Normally a half-duplex-line is expected
//		and the otherside isn't allowed to send unless it has
//		received a go-ahead. This suppresses the need for sending
//		the go-ahead.
// TTYPE	ask for the terminal-type of the client.
// NAWS		Negotiate About Window Size. The client has to send the
//		size of the screen. Used for the inline pager.
// MCCP		Mud Client Compression Protocol. Use compression for
//		over the socket.
// MSP		Mud Soud Protocol.
// MXP		Mud eXtension Protocol.
//
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Woverflow"
const	char	echo_will	[]={ IAC, WILL,	TELOPT_ECHO, 0 };
const	char	echo_wont	[]={ IAC, WONT,	TELOPT_ECHO, 0 };
const	char	echo_do		[]={ IAC, DO,	TELOPT_ECHO, 0 };
const	char	echo_dont	[]={ IAC, DONT, TELOPT_ECHO, 0 };

const	char	binary_will	[]={ IAC, WILL,	TELOPT_BINARY, 0 };
const	char	binary_wont	[]={ IAC, WONT,	TELOPT_BINARY, 0 };
const	char	binary_do	[]={ IAC, DO,	TELOPT_BINARY, 0 };
const	char	binary_dont	[]={ IAC, DONT, TELOPT_BINARY, 0 };

const	char 	go_ahead_str	[]={ IAC, GA, 0 };
const	char	sga_will	[]={ IAC, WILL,	TELOPT_SGA, 0 };
const	char	sga_wont	[]={ IAC, WONT,	TELOPT_SGA, 0 };
const	char	sga_do		[]={ IAC, DO,	TELOPT_SGA, 0 };
const	char	sga_dont	[]={ IAC, DONT, TELOPT_SGA, 0 };

const	char	tmark_do	[]={ IAC, DO,	TELOPT_TM, 0 };
const	char	tmark_wont	[]={ IAC, WONT,	TELOPT_TM, 0 };

const	char 	ttype_will	[]={ IAC, WILL,	TELOPT_TTYPE, 0 };
const	char 	ttype_wont	[]={ IAC, WONT,	TELOPT_TTYPE, 0 };
const	char 	ttype_do	[]={ IAC, DO,	TELOPT_TTYPE, 0 };
const	char 	ttype_dont	[]={ IAC, DONT,	TELOPT_TTYPE, 0 };
const	char 	ttype_send	[]={ IAC, SB,	TELOPT_TTYPE, TELQUAL_SEND, IAC, SE, 0 };
const	char 	ttype_sb	[]={ IAC, SB,	TELOPT_TTYPE, 0 };

const	char 	naws_will	[]={ IAC, WILL,	TELOPT_NAWS, 0 };
const	char 	naws_wont	[]={ IAC, WONT,	TELOPT_NAWS, 0 };
const	char 	naws_do		[]={ IAC, DO,	TELOPT_NAWS, 0 };
const	char 	naws_dont	[]={ IAC, DONT,	TELOPT_NAWS, 0 };
const	char 	naws_send	[]={ IAC, SB,	TELOPT_NAWS, TELQUAL_SEND, IAC, SE, 0 };
const	char 	naws_sb		[]={ IAC, SB,	TELOPT_NAWS, 0 };

#ifdef HAS_MCCP
const	char	mccp_will	[]={ IAC, WILL,	TELOPT_MCCP, 0 };
const	char	mccp_wont	[]={ IAC, WONT,	TELOPT_MCCP, 0 };
const	char	mccp_do		[]={ IAC, DO,	TELOPT_MCCP, 0 };
const	char	mccp_dont	[]={ IAC, DONT,	TELOPT_MCCP, 0 };
const	char	mccp2_will	[]={ IAC, WILL,	TELOPT_MCCP2, 0 };
const	char	mccp2_wobnt	[]={ IAC, WONT,	TELOPT_MCCP2, 0 };
const	char	mccp2_do	[]={ IAC, DO,	TELOPT_MCCP2, 0 };
const	char	mccp2_dont	[]={ IAC, DONT,	TELOPT_MCCP2, 0 };
#endif
const	char	msp_will	[]={ IAC, WILL,	TELOPT_MSP, 0 };
const	char	msp_wont	[]={ IAC, WONT,	TELOPT_MSP, 0 };
const	char	msp_do		[]={ IAC, DO,	TELOPT_MSP, 0 };
const	char	msp_dont	[]={ IAC, DONT,	TELOPT_MSP, 0 };
const	char	mxp_will	[]={ IAC, WILL,	TELOPT_MXP, 0 };
const	char	mxp_wont	[]={ IAC, WONT,	TELOPT_MXP, 0 };
const	char	mxp_do		[]={ IAC, DO,	TELOPT_MXP, 0 };
const	char	mxp_dont	[]={ IAC, DONT,	TELOPT_MXP, 0 };
#pragma GCC diagnostic pop

//
// Global variables.
//
DESCRIPTOR_DATA *   descriptor_list;	// All open descriptors
DESCRIPTOR_DATA *   d_next;		// Next descriptor in loop
FILE *		    fpReserve;		// Reserved file handle



char *dns_gethostname(DESCRIPTOR_DATA *desc) {
#ifdef THREADED_DNS
#ifdef INET6
    if (desc->ipv6)
        return gethostname_cached(desc->Host,16,FALSE);
    else
        return gethostname_cached(desc->Host,4,FALSE);
#else
    return gethostname_cached(desc->Host,4,FALSE);
#endif
#else
    return desc->Host;
#endif
}


int init_socket(int port,int ipversion) {
    int	x=1;

    if (ipversion==6)
#ifdef INET6
    {
        static struct sockaddr_in6 sa6_zero;
        struct sockaddr_in6 sa6;
        int fd6;

        if ( ( fd6=socket(PF_INET6,SOCK_STREAM,0) ) < 0 ) {
            bugf("init_socket6(): socket: %s",ERROR);
            exit(1);
        }

        if (setsockopt(fd6,SOL_SOCKET,SO_REUSEADDR,(char *)&x,sizeof(x))<0) {
            bugf("init_socket6(): SO_REUSEADDR: %s",ERROR);
            close(fd6);
            exit(1);
        }

        sa6		= sa6_zero;
        sa6.sin6_family = AF_INET6;
        sa6.sin6_port   = htons( port );

        if ( bind( fd6, (struct sockaddr *) &sa6, sizeof(sa6) ) < 0 ) {
            bugf("init_socket6(): bind(%d): %s",port,ERROR);
            close(fd6);
            exit(1);
        }

        if ( listen( fd6, 3 ) < 0 ) {
            bugf("init_socket6(): listen(%d): %s",port,ERROR);
            close(fd6);
            exit(1);
        }

        return fd6;
    }
#else
    {
        bugf("init_socket(): I don't know about IPv6\n");
        exit(1);
    }
#endif

    if (ipversion==4)
#ifndef NO_INET4
    {
        static struct sockaddr_in sa4_zero;
        struct sockaddr_in sa4;
        int fd4;

        if ( ( fd4 = socket( PF_INET, SOCK_STREAM, 0 ) ) < 0 ) {
            bugf("init_socket4(): socket: %s",ERROR);
            exit( 1 );
        }

        if (setsockopt(fd4,SOL_SOCKET,SO_REUSEADDR,(char *)&x,sizeof(x))<0) {
            bugf("init_socket4(): SO_REUSEADDR: %s",ERROR);
            close(fd4);
            exit( 1 );
        }

        sa4	       = sa4_zero;
        sa4.sin_family = AF_INET;
        sa4.sin_port   = htons( port );

        if ( bind( fd4, (struct sockaddr *) &sa4, sizeof(sa4) ) < 0 ) {
            bugf("init_socket4(): bind: %s",ERROR);
            close(fd4);
            exit(1);
        }

        if ( listen( fd4, 3 ) < 0 ) {
            bugf("init_socket4(): listen: %s",ERROR);
            close(fd4);
            exit(1);
        }

        return fd4;
    }
#else
    {
        bugf("init_socket(): I don't know about IPv4\n");
        exit(1);
    }
#endif

    bugf("init_socket(): I don't know about IP version %d\n",ipversion);
    exit(1);
    return 0;	// compiler might complain if we don't use this.
}


char COMMAND_BUFFER[MIL];
char *get_multi_command(DESCRIPTOR_DATA *d,char *argument) {
    char *pcom;

    pcom = COMMAND_BUFFER;
    while ( *argument != 0 ) {
        if ( *argument == ';' ) {
            if ( *++argument != ';' ) {
                strcpy( d->incomm, argument );
                *pcom = 0;
                WAIT_STATE(d->character,PULSE_PER_SECOND);
                return COMMAND_BUFFER;
            }
        }
        *pcom++ = *argument++;
    }
    *pcom=0;
    d->incomm[0]=0;
    return COMMAND_BUFFER;
}


void game_loop_unix( void ) {
    static struct timeval null_time;
    struct timeval last_time;

    // create the watchdog file
    {
        FILE *wd;

        wd=fopen(WATCHDOG_FILE,"w");
        fprintf(wd,"%d",getpid());
        fclose(wd);
    }

    gettimeofday( &last_time, NULL );
    current_time = (time_t) last_time.tv_sec;

    /* Main loop */
    while (!mud_data.reboot_now) {
        fd_set in_set;
        fd_set out_set;
        fd_set exc_set;
        DESCRIPTOR_DATA *d;
        int maxdesc = 0;

        //
        // Poll all active descriptors.
        //
        FD_ZERO( &in_set  );
        FD_ZERO( &out_set );
        FD_ZERO( &exc_set );
#ifndef NO_INET4
        FD_SET( mud_data.control4_mud , &in_set );
#ifdef HAS_HTTP
        FD_SET( mud_data.control4_http, &in_set );
#endif
#ifdef HAS_POP3
        FD_SET( mud_data.control4_pop3, &in_set );
#endif
#endif
#ifdef INET6
        FD_SET( mud_data.control6_mud , &in_set );
#ifdef HAS_HTTP
        FD_SET( mud_data.control6_http, &in_set );
#endif
#ifdef HAS_POP3
        FD_SET( mud_data.control6_pop3, &in_set );
#endif
#endif
#ifndef NO_INET4
        maxdesc=mud_data.control4_mud;
#ifdef HAS_HTTP
        maxdesc	= UMAX(maxdesc,mud_data.control4_http);
#endif
#ifdef HAS_POP3
        maxdesc = UMAX(maxdesc,mud_data.control4_pop3);
#endif
#endif
#ifdef INET6
        maxdesc = UMAX(maxdesc,mud_data.control6_mud);
#ifdef HAS_HTTP
        maxdesc = UMAX(maxdesc,mud_data.control6_http);
#endif
#ifdef HAS_POP3
        maxdesc = UMAX(maxdesc,mud_data.control6_pop3);
#endif
#endif

        for (d=descriptor_list;d!=NULL;d=d->next) {
            maxdesc = UMAX( maxdesc, d->descriptor );
            FD_SET( d->descriptor, &in_set  );
            FD_SET( d->descriptor, &out_set );
            FD_SET( d->descriptor, &exc_set );
        }

        if (select(maxdesc+1, &in_set, &out_set, &exc_set, &null_time )<0) {
            bugf("[0] game_loop(): select-poll: %s",ERROR);
            exit( 1 );
        }

        //
        // New connection?
        //
#ifndef NO_INET4
        if (FD_ISSET( mud_data.control4_mud, &in_set ) )
            init_descriptor( mud_data.control4_mud, DESTYPE_MUD, 4 );
#ifdef HAS_HTTP
        else if (FD_ISSET( mud_data.control4_http, &in_set ) )
            init_descriptor( mud_data.control4_http, DESTYPE_HTTP, 4 );
#endif
#ifdef HAS_POP3
        else if (FD_ISSET( mud_data.control4_pop3, &in_set) )
            init_descriptor( mud_data.control4_pop3, DESTYPE_POP3, 4 );
#endif
#endif
#ifdef INET6
        if (FD_ISSET( mud_data.control6_mud, &in_set) )
            init_descriptor( mud_data.control6_mud, DESTYPE_MUD, 6 );
#ifdef HAS_HTTP
        else if (FD_ISSET( mud_data.control6_http, &in_set) )
            init_descriptor( mud_data.control6_http, DESTYPE_HTTP, 6 );
#endif
#ifdef HAS_POP3
        else if (FD_ISSET( mud_data.control6_pop3, &in_set) )
            init_descriptor( mud_data.control6_pop3, DESTYPE_POP3, 6 );
#endif
#endif

        //
        // Kick out the freaky folks.
        //
        for ( d = descriptor_list; d != NULL; d = d_next )
        {
            d_next = d->next;
            if ( FD_ISSET( d->descriptor, &exc_set ) )
            {
                FD_CLR( d->descriptor, &in_set  );
                FD_CLR( d->descriptor, &out_set );
                if ( d->connected==CON_PLAYING)
                    save_char_obj( d->character );
                d->outtop	= 0;
                logf("[%d] game_loop(): closed link, out-of-band data received",
                     d->descriptor);
                close_socket( d );
            }
        }

        //
        // execute pending events
        //
        {
            EVENT_DATA *event,*event_next;

            for (event=event_list;event!=NULL;event=event_next) {
                event_next=event->next;

                if (!IS_VALID(event)) {
                    bugf("event-dispatcher: got an invalid event, breaking out.");
                    break;
                }

                if (event->ch==NULL) {
                    bugf("event-dispatcher: event->ch is NULL, removing event.");
                    remove_event(event);
                    continue;
                }

                if (--event->delay<0) {
                    (event->fun)(event);
                    remove_event(event);
                }
            }
        }
        // append new events
        {
            EVENT_DATA *event=event_new;

            if (event!=NULL) {
                while (event->next!=NULL)
                    event=event->next;
                event->next=event_list;
                event_list=event_new;
                event_new=NULL;
            }
        }

        //
        // Process input.
        //
        for ( d=descriptor_list; d!=NULL; d=d_next ) {
            d_next	= d->next;
            d->fcommand	= FALSE;

            //
            // try to read the data from the descriptor into the buffer
            // of the descriptor.
            // don't do it if descriptor is in use for logging a mob.
            //
            if (d->connected!=CON_LOGGING) {
                if (FD_ISSET(d->descriptor, &in_set )) {
                    if ( d->character!=NULL )
                        d->character->timer=0;

                    if ( !read_from_descriptor( d ) ) {
                        // read error
                        FD_CLR( d->descriptor, &out_set );
                        if (d->connected==CON_PLAYING)
                            save_char_obj( d->character );
                        d->outtop=0;
                        close_socket(d);
                        continue;
                    }
                }
            }

            // still recovering from a shock?
            if (d->character!=NULL
                    &&  d->character->daze>0)
                --d->character->daze;

            // delay from a previous command
            if (d->character!=NULL
                    &&  d->character->wait>0 ) {
                if (d->character->wait>100) {
                    bugf("wait state too big to make sense for %s: %d",
                         d->character->name,d->character->wait);
                    d->character->wait=100;
                }
                --d->character->wait;
                continue;
            }

            // if we're still busy in a phased-skill, don't do things yet
            if ( d->character!=NULL && d->character->phasedskill_proc!=NULL )
                continue;

            //
            // process incoming commands
            //
            if ((d->incomm[0]!=0) || read_from_buffer(d)) {
                d->fcommand	= TRUE;
                stop_idling(d->character);

                if ( d->showstr_point ) {
                    // in the pager
                    show_string(d,d->incomm);
                    d->incomm[0]=0;
                } else if (d->pStringBegin) {
                    // in the editor
                    editor_addline(d->character,d->incomm);
                    d->incomm[0]=0;
                } else if (d->connected==CON_PLAYING) {
                    // try to find out if it's a command for OLC
                    if (!run_olc_editor(d)) {
                        // if not, execute it
                        char *command2;
                        command2=get_multi_command( d, d->incomm );
                        substitute_alias( d, command2 );
                    } else {
                        d->incomm[0]=0;
                    }
                }
#ifdef HAS_HTTP
                else if (d->connected == CON_HTTP ) {
                    // feed it to the HTTP part
                    run_http_server( d );
                    d->incomm[0]=0;
                }
#endif
#ifdef HAS_POP3
                else if (d->connected >= CON_POP3_GET_USER
                         &&  d->connected <= CON_POP3_CONNECTED) {
                    // or to the POP3 server
                    run_pop3_server( d );
                    d->incomm[0]=0;
                }
#endif
                else {
                    // if the play state is not PLAYING, HTTP or POP3*,
                    // it must be the nanny!
                    nanny( d, d->incomm );
                    d->incomm[0]=0;
                }
            }
        }

#ifdef I3
        I3_loop();
#endif

        //
        // Autonomous game motion.
        //
        update_handler( );

        //
        // Process output to the players.
        //
        for ( d=descriptor_list; d!=NULL; d=d_next ) {
            d_next = d->next;

            if ((d->fcommand || d->outtop > 0 )
                    &&   FD_ISSET(d->descriptor, &out_set) ) {
                if (!process_output(d,TRUE)) {
                    if ( d->connected==CON_PLAYING)
                        save_char_obj(d->character);
                    d->outtop=0;
                    close_socket(d);
                }
            }
        }


        //
        // Watchdog update
        //
        utimes(WATCHDOG_FILE,NULL);

        //
        // Synchronize to a clock.
        // Sleep( last_time + 1/PULSE_PER_SECOND - now ).
        // Careful here of signed versus unsigned arithmetic.
        //
        // Actually what it does is make sure there are not more than
        // four pulses per second.
        //
        {
            struct timeval now_time;
            long secDelta;
            long usecDelta;

            gettimeofday( &now_time, NULL );
            usecDelta	= ((int) last_time.tv_usec) - ((int) now_time.tv_usec)
                    + 1000000 / PULSE_PER_SECOND;
            secDelta	= ((int) last_time.tv_sec ) - ((int) now_time.tv_sec );
            while ( usecDelta < 0 ) {
                usecDelta += 1000000;
                secDelta  -= 1;
            }

            while ( usecDelta >= 1000000 ) {
                usecDelta -= 1000000;
                secDelta  += 1;
            }

            if ( secDelta>0 )
                bugf("[0] Long sync wait: last=%d.%d now=%d.%d delta=%d.%d",
                     last_time.tv_sec,last_time.tv_usec,
                     now_time.tv_sec,now_time.tv_usec,
                     secDelta,usecDelta);

            if ( (secDelta>0 && secDelta<3) || ( secDelta==0 && usecDelta>0 )) {
                struct timeval stall_time;

                stall_time.tv_usec = usecDelta;
                stall_time.tv_sec  = secDelta;
#ifdef NOTDEF
                logf("[0] Idle: %3.5f",100.0*usecDelta*4/1000000);
#endif
                if ( select(0,NULL,NULL,NULL,&stall_time)<0 ) {
                    if (errno!=EINTR) {
                        logf("[0] game_loop(): select-stall: %s",ERROR);
                        exit( 1 );
                    }
                }
            }
        }

        gettimeofday( &last_time, NULL );
        current_time = (time_t) last_time.tv_sec;
    }

    unlink(WATCHDOG_FILE);

    return;
}


void send_options(DESCRIPTOR_DATA *d) {
    write_to_buffer(d,naws_do,0);
    write_to_buffer(d,ttype_do,0);
    write_to_buffer(d,sga_do,0);
    write_to_buffer(d,mxp_wont,0);
    write_to_buffer(d,msp_will,0);
#ifdef HAS_MCCP
    write_to_buffer(d,mccp2_will,0);
    write_to_buffer(d,mccp_will,0);
#endif
}


#ifdef UNIX
void init_descriptor( int control, int destype, int IPversion )
{
    char		buf[MAX_STRING_LENGTH];
    DESCRIPTOR_DATA *	dnew;
    struct sockaddr_in	sock4;
#ifdef INET6
    struct sockaddr_in6 sock6;
#endif
#ifndef THREADED_DNS
    struct hostent *	from;
#endif
    int			desc;
    socklen_t		size;

    if (IPversion==6) {
#ifndef INET6
        bugf("init_descriptor(): got IPv6 request but don't know about it.\n");
        abort();
#endif
    }

    mud_data.connections++;

    if (IPversion==4) {
        size = sizeof(sock4);
        getsockname( control, (struct sockaddr *) &sock4, &size );
        if ( ( desc=accept( control, (struct sockaddr *) &sock4, &size))<0) {
            logf("[0] new_descriptor4(): accept: %s",ERROR);
            return;
        }
    }
#ifdef INET6
    if (IPversion==6) {
        size = sizeof(sock6);
        getsockname( control, (struct sockaddr *) &sock6, &size );
        if ( ( desc = accept( control, (struct sockaddr *) &sock6, &size))<0) {
            logf("[0] new_descriptor6(): accept: %s",ERROR);
            return;
        }
    }
#endif

#if !defined(FNDELAY)
#define FNDELAY O_NDELAY
#endif

    if ( fcntl( desc, F_SETFL, FNDELAY ) == -1 ) {
        logf("[%d] new_descriptor: FNDELAY: %s",desc,ERROR);
        return;
    }

    /*
     * Cons a new descriptor.
     */
    dnew = new_descriptor(destype);
    dnew->descriptor = desc;

#ifdef INET6
    if (IPversion==6) {
        dnew->ipv6=TRUE;
        size = sizeof(sock6);
        if ( getpeername( desc, (struct sockaddr *) &sock6, &size ) < 0 ) {
            logf("[0] new_descriptor6(): getpeername: %s",ERROR);
#ifdef THREADED_DNS
            bzero(dnew->Host,sizeof(dnew->Host));
#else
            free_string(dnew->Host);
            dnew->Host = str_dup("(unknown)");
#endif
        } else {
            /*
         * Would be nice to use inet_ntoa here but it takes a struct arg,
         * which ain't very compatible between gcc and system libraries.
         */
            inet_ntop(AF_INET6,(char *)sock6.sin6_addr.s6_addr,buf,sizeof(buf));
            if(dnew->connected==CON_GET_NAME
        #ifdef HAS_POP3
                    || dnew->connected==CON_POP3_GET_USER
        #endif
        #ifdef HAS_HTTP
                    || dnew->connected==CON_HTTP
        #endif
                    ) {
                logf( "[%d] Sock6.sinaddr: %s", dnew->descriptor,buf);
            }
#ifdef THREADED_DNS
            memcpy(dnew->Host,&sock6.sin6_addr,16);
            gethostname_cached(dnew->Host,16,TRUE);
#else
            if (!mud_data.dnslock)
                from = gethostbyaddr( (char *) &sock6.sin6_addr,
                                      sizeof(sock6.sin6_addr), AF_INET6 );
            else
                from=NULL;
            free_string(dnew->Host);
            dnew->Host = str_dup( from ? from->h_name : buf );
#endif
        }
    }
#endif
    if (IPversion==4) {
        dnew->ipv6=FALSE;
        size = sizeof(sock4);
        if ( getpeername( desc, (struct sockaddr *) &sock4, &size ) < 0 ) {
            logf("[0] new_descriptor4(): getpeername: %s",ERROR);
#ifdef THREADED_DNS
            bzero(dnew->Host,sizeof(dnew->Host));
#else
            free_string(dnew->Host);
            dnew->Host = str_dup("(unknown)");
#endif
        } else {
            /*
         * Would be nice to use inet_ntoa here but it takes a struct arg,
         * which ain't very compatible between gcc and system libraries.
         */
            int addr;

            addr = ntohl( sock4.sin_addr.s_addr );
            sprintf( buf, "%d.%d.%d.%d",
                     ( addr >> 24 ) & 0xFF, ( addr >> 16 ) & 0xFF,
                     ( addr >>  8 ) & 0xFF, ( addr       ) & 0xFF
                     );
            if (dnew->connected==CON_GET_NAME
        #ifdef HAS_POP3
                    ||  dnew->connected==CON_POP3_GET_USER
        #endif
        #ifdef HAS_HTTP
                    ||  dnew->connected==CON_HTTP
        #endif
                    ) {
                logf("[%d] Sock.sinaddr: %s",dnew->descriptor,buf);
            }
#ifdef THREADED_DNS
            memcpy(dnew->Host,&sock4.sin_addr,sizeof(addr));
            gethostname_cached(dnew->Host,4,TRUE);
#else
            if (!mud_data.dnslock)
                from = gethostbyaddr( (char *) &sock4.sin_addr,
                                      sizeof(sock4.sin_addr), AF_INET );
            else
                from=NULL;
            free_string(dnew->Host);
            dnew->Host = str_dup( from ? from->h_name : buf );
#endif
        }
    }

    switch(destype) {
#ifdef HAS_POP3
    case DESTYPE_POP3:
        logf("[%d] POP3 connection from %s",
             dnew->descriptor,dns_gethostname(dnew));
        wiznet(WIZ_POP3,0,NULL,NULL,
               "POP3 connection from %s",dns_gethostname(dnew));
        break;
#endif
#ifdef HAS_HTTP
    case DESTYPE_HTTP:
        logf("[%d] HTTP connection from %s",
             dnew->descriptor,dns_gethostname(dnew));
        wiznet(WIZ_HTTP,0,NULL,NULL,
               "HTTP connection from %s",dns_gethostname(dnew));
        break;
#endif
    default:
        logf("[%d] MUD connection from %s",
             dnew->descriptor,dns_gethostname(dnew));
        wiznet(WIZ_SITES,0,NULL,NULL,
               "MUD connection from %s",dns_gethostname(dnew));
        break;
    }

    if (IPversion==4 && destype==DESTYPE_MUD) {
        int tos=0x10; // LOW DELAY
        setsockopt(dnew->descriptor,IPPROTO_IP,IP_TOS, &tos, sizeof(tos));
    }

    /*
     * Swiftest: I added the following to ban sites.  I don't
     * endorse banning of sites, but Copper has few descriptors now
     * and some people from certain sites keep abusing access by
     * using automated 'autodialers' and leaving connections hanging.
     *
     * Furey: added suffix check by request of Nickel of HiddenWorlds.
     */
    if (check_lockout(dnew->ipv6,dnew->Host,TRUE) ||
            checkban_siteban(dns_gethostname(dnew))) {
#ifdef HAS_HTTP
        if (dnew->connected==CON_HTTP) {
            write_to_real_descriptor( desc,
                                      "<HTML>\n"
                                      "<HEAD><TITLE>Banned</TITLE></HEAD>\n"
                                      "<BODY>\n"
                                      "<H1>Banned</H1>\n"
                                      "<P>Your site has been banned from this mud.</P>\n"
                                      "</BODY>\n"
                                      "</HTML>\n",0);
            mud_data.banhttp_violations++;
        } else
#endif
#ifdef HAS_POP3
            if(dnew->connected==CON_POP3_GET_USER) {
                write_to_real_descriptor( desc,
                                          "-ERR You are banned from this mud.\n",0);
                mud_data.banpop3_violations++;
            } else
#endif
            {
                logf( "[%d] Denying access to %s (site ban).",
                      dnew->descriptor, dns_gethostname(dnew) );
                wiznet(WIZ_LOGINS,0,NULL,NULL,
                       "Denying access to %s (site ban).",
                       dns_gethostname(dnew) );
                write_to_real_descriptor( desc,
                                          "Your site has been banned from this mud.\n\r", 0 );
                mud_data.banmud_violations++;
            }
        close( desc );
        free_descriptor(dnew);
        return;
    }
    /*
     * Init descriptor data.
     */
    dnew->next			= descriptor_list;
    descriptor_list		= dnew;

    /*
     * Send the greeting.
     */
#ifdef HAS_POP3
    if(dnew->connected==CON_POP3_GET_USER)
    {
        extern char *pop3_greeting_msg;
        dnew->fcommand=TRUE;
        write_to_buffer( dnew, pop3_greeting_msg, 0 );
    } else
#endif
#ifdef HAS_HTTP
        if (dnew->connected==CON_HTTP) {
            // do nothing
        } else
#endif
        {
            // mud connection
            extern char * help_greeting;

            send_options(dnew);

            if ( help_greeting[0] == '.' )
                write_to_buffer( dnew, help_greeting+1, 0 );
            else
                write_to_buffer( dnew, help_greeting  , 0 );
            write_to_buffer(dnew,"By what name do you wish to be known? ",0);
            dnew->pueblo=FALSE;
            dnew->mxp=FALSE;
        }

    return;
}
#endif



void close_socket( DESCRIPTOR_DATA *dclose )
{
    CHAR_DATA *ch;

    if ( dclose->outtop > 0 )
        process_output( dclose, FALSE );

    if ( dclose->snoop_by != NULL )
    {
        write_to_buffer( dclose->snoop_by,
                         "Your victim has left the game.\n\r", 0 );
    }

    {
        DESCRIPTOR_DATA *d;

        for ( d = descriptor_list; d != NULL; d = d->next )
        {
            if ( d->snoop_by == dclose )
                d->snoop_by = NULL;
        }
    }

#ifdef I3
    {
        I3_CHANNEL *channel;
        I3_LISTENER *listener;
        for (channel=I3_channel_list;channel;channel=channel->next) {
            if ((listener=find_i3_listener_by_descriptor(channel,dclose))) {
                i3_listen_channel(dclose,channel->local_name,TRUE);
            }
        }
    }
#endif

    if ( ( ch = dclose->character ) != NULL )
    {
#ifdef HAS_MCCP
        if (dclose->mccp || dclose->mccp2) {
            logf("[%d] Closing link to %s [%d/%d] [%d/%3.2f%%]",
                 GET_DESCRIPTOR(ch),
                 ch->name,
                 dclose->bytesreceived,
                 dclose->bytessend,
                 dclose->compressedsend,
                 1.0*100*dclose->compressedsend/dclose->uncompressedsend
                 );
        } else
#endif
        {
            logf("[%d] Closing link to %s [%d/%d]",
                 GET_DESCRIPTOR(ch),
                 ch->name,
                 dclose->bytesreceived,
                 dclose->bytessend
                 );
        }

        /* cut down on wiznet spam when rebooting */
        if ( ( dclose->connected==CON_PLAYING )
             && !mud_data.reboot_now)
        {
            act( "$n has lost $s link.", ch, NULL, NULL, TO_ROOM );
            wiznet(WIZ_LINKS,0,ch,NULL,"Net death has claimed $N.");
            ch->desc = NULL;
            // used to disconnect linkdead imms
            if (IS_IMMORTAL(ch)) ch->timer= 0;
        }
        /* Quit character without saving when losing link while speccing */
        else if(dclose->connected>=CON_GET_SPEC_CLASS
                && dclose->connected<=CON_GEN_SPECGROUPS)
        {
            quit_char(dclose->original ? dclose->original : dclose->character);
            /* Quit char recursivly calls close_socket */
            return;
        } else {
            free_char(dclose->original ? dclose->original :
                                         dclose->character );
        }
    } else {
#ifdef HAS_MCCP
        if (dclose->mccp || dclose->mccp2) {
            logf("[%d] Closing link [%d/%d] [%d/%3.2f%%]",
                 dclose->descriptor,
                 dclose->bytesreceived,
                 dclose->bytessend,
                 dclose->compressedsend,
                 1.0*100*dclose->compressedsend/dclose->uncompressedsend
                 );
        } else
#endif
        {
            logf("[%d] Closing link [%d/%d]",
                 dclose->descriptor,
                 dclose->bytesreceived,
                 dclose->bytessend
                 );
        }
    }

#ifdef HAS_POP3
    if ( ( dclose->pop3_char ) != NULL ) {
        free_char(dclose->pop3_char);
    }
#endif

    if ( d_next == dclose )
        d_next = d_next->next;

    if ( dclose == descriptor_list )
    {
        descriptor_list = descriptor_list->next;
    }
    else
    {
        DESCRIPTOR_DATA *d;

        for ( d = descriptor_list; d && d->next != dclose; d = d->next )
            ;
        if ( d != NULL )
            d->next = dclose->next;
        else
            bugf( "Close_socket: dclose not found.");
    }

#ifdef HAS_MCCP
    if (dclose->mccp || dclose->mccp2) {
        deflateEnd(dclose->out_compress);
        if (dclose->out_compress_buf!=NULL)
            free_mem(dclose->out_compress_buf, COMPRESS_BUF_SIZE);
        if (dclose->out_compress!=NULL)
            free_mem(dclose->out_compress, sizeof(z_stream));
    }
#endif

    close( dclose->descriptor );
    free_descriptor(dclose);
    return;
}

#ifndef O_NONBLOCK
# define O_NONBLOCK O_NDELAY
#endif

void nonblock(int s) {
    int flags;

    flags = fcntl(s, F_GETFL, 0);
    flags |= O_NONBLOCK;
    if (fcntl(s, F_SETFL, flags) < 0) {
        logf("[0] Fatal error executing nonblock (comm.c): %s",ERROR);
        exit(1);
    }
}

bool read_from_descriptor( DESCRIPTOR_DATA *d ) {
#if defined(UNIX)
    for ( ; ; ) {
        int nRead;

        nRead=INBUFSIZE-(d->pinbuf-d->inbuf)-10;
        if (nRead<=0) {
            if ((d->character && IS_IMMORTAL(d->character)) ||
                    (d->original  && IS_IMMORTAL(d->original))) {
                return TRUE;
            }
            logf( "[%d] %s input overflow!",d->descriptor,dns_gethostname(d) );
            write_to_descriptor( d, "\n\r*** PUT A LID ON IT!!! ***\n\r", 0 );
            return FALSE;
        }
        nRead=read(d->descriptor,d->pinbuf,nRead);

        if (nRead>0) {
            d->bytesreceived+=nRead;
            d->pinbuf += nRead;
            d->pinbuf[0]=0;
            if ( '\n' == d->pinbuf[-1] || '\r' == d->pinbuf[-1] )
                break;
        }

        if (errno==EWOULDBLOCK)
            break;
        if (nRead<0) {
            logf("[%d] read_from_descriptor(): %s",d->descriptor,ERROR);
            return FALSE;
        }
        if ( nRead == 0 ) {
            logf( "[%d] Closing connection to %s (aborted)",
                  d->descriptor, dns_gethostname(d) );
            wiznet(WIZ_LOGINS,0,NULL,NULL,
                   "Closing connection to %s (aborted)",
                   dns_gethostname(d) );
            logf( "[%d] EOF encountered on read.",d->descriptor);
            return FALSE;
        }
    }
#endif

    return TRUE;
}



/*
 * Transfer one line from input buffer to input line.
 */
int process_IAC(DESCRIPTOR_DATA *d,char *IACdata,int maxlength) {
    //
    // Interpret As Command
    //
    // recognized are
    // - MXP
    // - MSP
    // - TTYP
    // - NAWS
    // - ECHO
    // - BINARY
    // - NAWS
    // - SGA
#ifdef HAS_MCCP
    // - MCCP
    // - MCCP2
#endif
    // others are ignored
    //

    // MXP
    if (!memcmp(IACdata,mxp_do,strlen(mxp_do))) {
        d->mxp=TRUE;
        if (d->character)
            STR_SET_BIT(d->character->strbit_act,PLR_MXP);
        wiznet(WIZ_TELNET,0,NULL,NULL,"%s is using MXP",dns_gethostname(d));
        STR_SET_BIT(d->telnet_options,TELOPT_MXP);
        return strlen(mxp_do);
    }
    if (!memcmp(IACdata,mxp_dont,strlen(mxp_dont))) {
        d->mxp=FALSE;
        if (d->character)
            STR_REMOVE_BIT(d->character->strbit_act,PLR_MXP);
        STR_REMOVE_BIT(d->telnet_options,TELOPT_MXP);
        return strlen(mxp_dont);
    }

    // MSP
    if (!memcmp(IACdata, msp_do, strlen(msp_do))) {
        STR_SET_BIT(d->telnet_options,TELOPT_MSP);
        return strlen(msp_do);
    }
    if (!memcmp(IACdata, msp_dont, strlen(msp_dont))) {
        STR_REMOVE_BIT(d->telnet_options,TELOPT_MSP);
        return strlen(msp_dont);
    }

    // SGA
    if (!memcmp(IACdata, sga_do, strlen(sga_do))) {
        STR_SET_BIT(d->telnet_options,TELOPT_SGA);
        write_to_descriptor(d,(char *)sga_will,0);
        d->go_ahead=FALSE;
        return strlen(sga_do);
    }
    if (!memcmp(IACdata, sga_dont, strlen(ttype_dont))) {
        STR_REMOVE_BIT(d->telnet_options,TELOPT_SGA);
        d->go_ahead=TRUE;
        return strlen(sga_dont);
    }
    if (!memcmp(IACdata, sga_will, strlen(sga_will))) {
        STR_SET_BIT(d->telnet_options,TELOPT_SGA);
        write_to_descriptor(d,(char *)sga_do,0);
        d->go_ahead=FALSE;
        return strlen(sga_will);
    }
    if (!memcmp(IACdata, sga_wont, strlen(sga_wont))) {
        STR_REMOVE_BIT(d->telnet_options,TELOPT_SGA);
        d->go_ahead=TRUE;
        return strlen(sga_wont);
    }

    // TTYP
    if (!memcmp(IACdata, ttype_do, strlen(ttype_do))) {
        STR_SET_BIT(d->telnet_options,TELOPT_TTYPE);
        return strlen(ttype_do);
    }
    if (!memcmp(IACdata, ttype_dont, strlen(ttype_dont))) {
        STR_REMOVE_BIT(d->telnet_options,TELOPT_TTYPE);
        return strlen(ttype_dont);
    }
    if (!memcmp(IACdata, ttype_will, strlen(ttype_will))) {
        STR_SET_BIT(d->telnet_options,TELOPT_TTYPE);
        write_to_descriptor(d,(char *)ttype_send,0);
        return strlen(ttype_will);
    }
    if (!memcmp(IACdata, ttype_wont, strlen(ttype_wont))) {
        STR_REMOVE_BIT(d->telnet_options,TELOPT_TTYPE);
        return strlen(ttype_wont);
    }
    if (!memcmp(IACdata, ttype_sb, strlen(ttype_sb))) {
        // syntax : IAC SB TTYPE IS ... IAC SE
        // example: IAC SB TTYPE IS x t e r m IAC SE
        char *p;

        if ((p=strchr(IACdata+4,IAC))==NULL)	// this is not good enough yet
            return 0;
        STR_SET_BIT(d->telnet_options,TELOPT_TTYPE);
        free_string(d->terminaltype);
        *p=0;
        d->terminaltype=str_dup(IACdata+4);
        return p-IACdata+2;
    }

    // TM (timing mark)
    if (!memcmp(IACdata, tmark_do, strlen(tmark_do))) {
        write_to_descriptor(d,(char *)tmark_wont,0);
        return strlen(tmark_do);
    }

    // NAWS
    if (!memcmp(IACdata, naws_will, strlen(naws_will))) {
        STR_SET_BIT(d->telnet_options,TELOPT_NAWS);
        write_to_descriptor(d,(char *)naws_send,0);
        return strlen(naws_will);
    }
    if (!memcmp(IACdata, naws_wont, strlen(naws_wont))) {
        STR_REMOVE_BIT(d->telnet_options,TELOPT_NAWS);
        return strlen(naws_wont);
    }
    if (!memcmp(IACdata, naws_sb, strlen(naws_sb))) {
        // syntax : IAC SB NAWS WIDTH[1] WIDTH[0] HEIGHT[1] HEIGHT[0] IAC SE
        // example: IAC SB NAWS 0 80 0 64 IAC SE
        int height;
        char *end_tag=IACdata;

        while ((end_tag=memchr(end_tag+1,IAC,IACdata+maxlength-end_tag))!=NULL &&
               (end_tag[1])!=((char)SE)) ;

        if (end_tag==NULL) return 0;

        if (end_tag!=IACdata+7) {
            bugf("[%d] Illegal IAC-SB-NAWS block.",d->descriptor);
            return end_tag-IACdata+2;
        }

        height=256*((unsigned char)IACdata[5])+(unsigned char)IACdata[6];

        d->lines=height-2;
        STR_SET_BIT(d->telnet_options,TELOPT_NAWS);
        return 9;
    }

    // ECHO
    // ignore them, they're send after the echo_will and echo_wont
    if (!memcmp(IACdata, echo_do, strlen(echo_do))) {
        STR_SET_BIT(d->telnet_options,TELOPT_ECHO);
        return strlen(echo_do);
    }
    if (!memcmp(IACdata, echo_dont, strlen(echo_dont))) {
        STR_REMOVE_BIT(d->telnet_options,TELOPT_ECHO);
        return strlen(echo_dont);
    }

#ifdef HAS_MCCP
    // MCCP
    if (!memcmp(IACdata, mccp_do, strlen(mccp_do))) {
        wiznet(WIZ_TELNET,0,NULL,NULL,"%s is using MCCP",dns_gethostname(d));
        compressStart(d,TELOPT_MCCP);
        d->mccp=TRUE;
        if (d->character)
            STR_SET_BIT(d->character->strbit_act,PLR_MCCP);
        STR_SET_BIT(d->telnet_options,TELOPT_MCCP);
        return strlen(mccp_do);
    }
    if (!memcmp(IACdata, mccp_dont, strlen(mccp_dont))) {
        compressEnd(d);
        d->mccp=FALSE;
        if (d->character)
            STR_REMOVE_BIT(d->character->strbit_act,PLR_MCCP);
        STR_REMOVE_BIT(d->telnet_options,TELOPT_MCCP);
        return strlen(mccp_dont);
    }

    // MCCP2
    if (!memcmp(IACdata, mccp2_do, strlen(mccp2_do))) {
        wiznet(WIZ_TELNET,0,NULL,NULL,"%s is using MCCP2",dns_gethostname(d));
        compressStart(d,TELOPT_MCCP2);
        d->mccp2=TRUE;
        if (d->character)
            STR_SET_BIT(d->character->strbit_act,PLR_MCCP2);
        STR_SET_BIT(d->telnet_options,TELOPT_MCCP2);
        return strlen(mccp2_do);
    }
    if (!memcmp(IACdata, mccp2_dont, strlen(mccp2_dont))) {
        compressEnd(d);
        d->mccp2=FALSE;
        if (d->character)
            STR_REMOVE_BIT(d->character->strbit_act,PLR_MCCP2);
        STR_REMOVE_BIT(d->telnet_options,TELOPT_MCCP2);
        return strlen(mccp2_dont);
    }

#endif
    {
        char command[MSL];
        char option[MSL];

        if (TELCMD_OK((char)IACdata[1]))
            strcpy(command,TELCMD((unsigned char)IACdata[1]));
        else
            sprintf(command,"unknown command (%d)",(unsigned char)IACdata[1]);

        if (TELOPT_OK(IACdata[2]) &&
                IACdata[2]<=NTELOPTS) // bug in arpa/telnet.h
            strcpy(option,TELOPT(IACdata[2]));
        else
            sprintf(option,"unknown option (%d)",(unsigned char)IACdata[2]);

        logf("[%d] Got unsupported IAC: %s, %s",d->descriptor,command,option);
        wiznet(WIZ_TELNET_DEBUG,0,NULL,NULL,
               "Got unsupported IAC: %s, %s for host %s",
               command,option,dns_gethostname(d));

        if (IACdata[1]==((char)DO))	return 3;
        if (IACdata[1]==((char)DONT))	return 3;
        if (IACdata[1]==((char)WILL))	return 3;
        if (IACdata[1]==((char)WONT))	return 3;
        if (IACdata[1]==((char)SE))	return 2;
        if (IACdata[1]==((char)SB)) {
            char *p;

            if ((p=memchr(IACdata+1,SE,maxlength-1))==NULL) return 0;
            return p-IACdata+1;
        }
        return 1;	// skip the IAC and pray
    }
}

//
// Copy data from the socket-input-buffer into the command-input-buffer
//
int read_from_buffer( DESCRIPTOR_DATA *d ) {
    char *pin,*pout;
    char incomm[MIL];

    //
    // Don't copy input from buffer if there is still a command pending
    //
    if ((d->incomm[0])!=0) return 0;

    //
    // If there in nothing in the input-buffer, ignore it
    //
    if (d->pinbuf==d->inbuf) return 0;

    //
    // See if there is an IAC telnet option in the input-buffer
    //
    while (d->inbuf[0]==(signed char)IAC) {
        int length;

        if ((length=process_IAC(d,d->inbuf,d->pinbuf-d->inbuf))==0)
            return 0;	// IAC string was not complete.
        memcpy(d->inbuf,d->inbuf+length,d->pinbuf-d->inbuf-length);
        d->pinbuf-=length;
        memset(d->pinbuf,0,length);
        if (d->pinbuf==d->inbuf) return 0;
    }

    //
    // See if there is a full line in the socket-inputbuffer
    //
    pin=d->inbuf;
    while (pin!=d->pinbuf) {
        if (*pin=='\n') break;
        if (*pin=='\r') break;
        pin++;
    }
    if (pin==d->pinbuf) return 0;

    //
    // Canonical input processing.
    // First copy it into a buffer, only after a full line has been read
    // put in into the descripors buffer.
    //
    pin=d->inbuf;
    pout=incomm;
    while (pin!=d->pinbuf) {
        if (*pin=='\n') break;
        if (*pin=='\r') break;

        if (pout-incomm > MAX_INPUT_LENGTH-4) {
            write_to_descriptor(d,"Line too long.\n\r",0);

            while (pin!=d->pinbuf) {
                if (*pin=='\n') break;
                if (*pin=='\r') break;
                pin++;
            }
            *pout='\n';pout++;
            *pout=0;
            break;
        }

        if (*pin==(char)IAC) {
            int length;

            if ((length=process_IAC(d,pin,d->pinbuf-pin))==0)
                return 0;
            pin+=length;
        }
        else if (*pin=='\b' || *pin==8) {	// backspace
            if (pout!=incomm)
                pout--;
        }
        else if (isprint(*pin)) {
            *pout=*pin;
            pout++;
        }
        pin++;
    }
    /*
    if (pout==incomm) {
    *pout=' ';
    pout++;
    } */
    *pout=0;
    strcpy(d->incomm,incomm);

    //
    // Spammers...
    //
    if ( pout!=d->incomm || d->incomm[0] == '!' ) {
        if ( d->incomm[0] != '!' && strcmp( d->incomm, d->inlast ) ) {
            d->repeat = 0;
        } else {
            if (++d->repeat >= 25
                    &&  d->connected==CON_PLAYING)
            {
                logf( "[%d] %s@%s input spamming: %s",
                      d->descriptor, d->character->name,dns_gethostname(d),
                      d->incomm[0] == '!'?d->inlast:d->incomm );
                wiznet(WIZ_SPAM,get_trust(d->character),d->character,NULL,
                       "Spam spam spam $N spam spam spam spam spam!");
                if (d->incomm[0] == '!')
                    wiznet(WIZ_SPAM,get_trust(d->character),d->character,NULL,
                           "%s",d->inlast);
                else
                    wiznet(WIZ_SPAM,get_trust(d->character),d->character,NULL,
                           "%s",d->incomm);

                d->repeat = 0;
            }
        }
    }

    //
    // Do '!' substitution.
    //
    if (d->incomm[0]=='!')
        strcpy(d->incomm,d->inlast);
    else
        strcpy(d->inlast,d->incomm);

    //
    // get rid of all pending \n/\r's
    //
    while (pin!=d->pinbuf) {
        if (*pin!='\n' && *pin!='\r')
            break;
        pin++;
    }

    //
    // Shift the input buffer.
    //
    pout=d->inbuf;
    while (pin!=d->pinbuf) {
        *pout=*pin;
        pin++;
        pout++;
    }
    d->pinbuf=pout;

    if (d->character && IS_PC(d->character))
        d->character->pcdata->idle=time(NULL);
    d->idle=time(NULL);
    return 1;
}



/*
 * Low level output function.
 */
bool process_output( DESCRIPTOR_DATA *d, bool fPrompt )
{
    /*
     * Bust a prompt.
     */
    if (!mud_data.reboot_now && d->showstr_point)
        write_to_buffer( d,
                         "[Please type (c)ontinue, (r)efresh, (b)ack, (h)elp, (q)uit, or RETURN]:  ",
                         0 );
    else if (fPrompt && !mud_data.reboot_now && d->connected==CON_PLAYING)
    {
        if(d->pStringBegin)
            sprintf_to_char(d->character,"%3d> ",d->currentLinenumber+1);
        else
        {
            CHAR_DATA *ch;
            CHAR_DATA *victim;

            ch = d->character;

            /* battle prompt */
            if ((victim = ch->fighting) != NULL && can_see(ch,victim))
            {
                int percent;
                char wound[100];
                char buf[MAX_STRING_LENGTH];
                char buf2[MAX_STRING_LENGTH];

                if (victim->max_hit > 0)
                    percent = victim->hit * 100 / victim->max_hit;
                else
                    percent = -1;

                if (percent >= 100)
                    sprintf(wound,"is in excellent condition.");
                else if (percent >= 90)
                    sprintf(wound,"has a few scratches.");
                else if (percent >= 75)
                    sprintf(wound,"has some small wounds and bruises.");
                else if (percent >= 50)
                    sprintf(wound,"has quite a few wounds.");
                else if (percent >= 30)
                    sprintf(wound,"has some big nasty wounds and scratches.");
                else if (percent >= 15)
                    sprintf(wound,"looks pretty hurt.");
                else if (percent >= 0)
                    sprintf(wound,"is in awful condition.");
                else
                    sprintf(wound,"is bleeding to death.");

                if (IS_AFFECTED(ch,EFF_HALLUCINATION))
                    victim=hallucination_char();

                sprintf(buf,"%s %s \n\r",
                        IS_NPC(victim) ? victim->short_descr : victim->name,wound);
                buf[0] = UPPER(buf[0]);
                colourbuf(ch,buf2,buf);
                write_to_buffer( d, buf2, 0);
            }

            ch = d->original ? d->original : d->character;
            if (!STR_IS_SET(ch->strbit_comm, COMM_COMPACT) )
                write_to_buffer( d, "\n\r", 2 );

            send_prompt(d->character);
            if (d->go_ahead)
                write_to_buffer(d,go_ahead_str,0);
        }
    }

    /*
     * Short-circuit if nothing to write.
     */
    if ( d->outtop == 0 )
        return TRUE;

    /*
     * Snoop-o-rama.
     */
    if ( d->snoop_by != NULL )
    {
        if (d->character != NULL)
            write_to_buffer( d->snoop_by, d->character->name,0);
        write_to_buffer( d->snoop_by, "> ", 2 );
        write_to_buffer( d->snoop_by, d->outbuf, d->outtop );
    }

    /*
     * OS-dependent output.
     */
    if ( !write_to_descriptor( d, d->outbuf, d->outtop ) )
    {
        d->outtop = 0;
        return FALSE;
    }
    else
    {
        d->outtop = 0;
        return TRUE;
    }
}


/*
 * Append onto an output buffer.
 */
void write_to_buffer( DESCRIPTOR_DATA *d, const char *txt, int length )
{
    if (!d) return;

    /*
     * Find length in case caller didn't.
     */
    if ( length <= 0 )
        length = strlen(txt);

    /*
     * Initial \n\r if needed.
     */
    if ( d->outtop == 0 && !d->fcommand )
    {
        d->outbuf[0]	= '\n';
        d->outbuf[1]	= '\r';
        d->outtop	= 2;
    }

    /*
     * Expand the buffer as needed.
     */
    while ( d->outtop + length >= d->outsize )
    {
        char *outbuf;

        if (d->outsize >= 32000)
        {
            bugf("Buffer overflow. Closing.\n\r");
            close_socket(d);
            return;
        }
        outbuf      = alloc_mem( 2 * d->outsize );
        strncpy( outbuf, d->outbuf, d->outtop );
        free_mem( d->outbuf, d->outsize );
        d->outbuf   = outbuf;
        d->outsize *= 2;
    }

    /*
     * Copy.
     */
    strncpy( d->outbuf + d->outtop, txt, length );	/* OLC */
    d->outtop += length;
    return;
}



/*
 * Lowest level output function.
 * Write a block of text to the file descriptor.
 * If this gives errors on very long blocks (like 'ofind all'),
 *   try lowering the max block size.
 */
bool write_to_real_descriptor( int desc, char *txt, int length )
{
    int iStart;
    int nWrite;
    int nBlock;

    if ( length <= 0 )
        length = strlen(txt);

    for ( iStart = 0; iStart < length; iStart += nWrite )
    {
        nBlock = UMIN( length - iStart, 4096 );
        if ( ( nWrite = write( desc, txt + iStart, nBlock ) ) < 0 ) {
            logf("[%d] write_to_descriptor: %s",desc,ERROR);
            return FALSE;
        }
    }

    return TRUE;
}

bool write_to_descriptor( DESCRIPTOR_DATA *d, char *txt, int length ) {
    d->bytessend+=length;
#ifdef HAS_MCCP
    if (d->out_compress)
        return writeCompressed(d, txt, length);
#endif
    return write_to_real_descriptor(d->descriptor, txt, length);
}


void check_mxp(CHAR_DATA *ch) {
    if (!HAS_MXP(ch)) return;

    send_to_char(
                MXP_SECURE"<!-- Elements to support the zMUD Automapper -->\n\r"
                MXP_SECURE"<!ELEMENT RName '<FONT COLOR=White><B>' FLAG='RoomName'>\n\r"
                MXP_SECURE"<!ELEMENT RDesc FLAG='RoomDesc' OPEN>\n\r"
                MXP_SECURE"<!ELEMENT RNum FLAG='RoomNum'>\n\r"
                MXP_SECURE"<!ELEMENT RExits '<FONT COLOR=Green>' FLAG='RoomExit'>\n\r"
                MXP_SECURE"<!-- The next element is used to define a room exit link that sends -->\n\r"
                MXP_SECURE"<!-- the exit direction to the MUD if the user clicks on it -->\n\r"
                MXP_SECURE"<!ELEMENT Ex '<SEND>'>\n\r"
                MXP_SECURE"<!ELEMENT Chat '<FONT COLOR=Gray>' OPEN>\n\r"
                MXP_SECURE"<!ELEMENT Gossip '<FONT COLOR=Cyan>' OPEN>\n\r"
                MXP_SECURE"<!-- in addition to standard HTML Color specifications, you can use -->\n\r"
                MXP_SECURE"<!-- zMUD color attribute names such as blink -->\n\r"
                MXP_SECURE"<!ELEMENT ImmChan '<FONT COLOR=Red,Blink>'>\n\r"
                MXP_SECURE"<!ELEMENT Auction '<FONT COLOR=Purple>' OPEN>\n\r"
                MXP_SECURE"<!ELEMENT Group '<FONT COLOR=Blue>' OPEN>\n\r"
                MXP_SECURE"<!-- items in inventories -->\n\r"
                MXP_SECURE"<!ELEMENT ItemInContainer '<send \"get &objname; &contname; \" hint=\"get &object; from &container;\">'>\n\r"
                //	MXP_SECURE"<!ELEMENT ItemInContainer '<send =\"&object;\" hint=\"&container;\">'>\n\r"
                //	MXP_SECURE"<!ATTLIST ItemInContainer 'objname contname object container'>\n\r"
                MXP_SECURE"<!ELEMENT Item '<send hint=&col; href=\"buy &col;\">' ATT=col>\n\r"
                MXP_SECURE"<!-- the next elements deal with the MUD prompt -->\n\r"
                MXP_SECURE"<!ELEMENT Prompt FLAG='Prompt'>\n\r"
                MXP_SECURE"<!ELEMENT Hp FLAG='Set hp'>\n\r"
                MXP_SECURE"<!ELEMENT MaxHp FLAG='Set maxhp'>\n\r"
                MXP_SECURE"<!ELEMENT Mana FLAG='Set mana'>\n\r"
                MXP_SECURE"<!ELEMENT MaxMana FLAG='Set maxmana'>\n\r"
                MXP_SECURE"<!ELEMENT Move FLAG='Set move'>\n\r"
                MXP_SECURE"<!ELEMENT MaxMove FLAG='Set maxmove'>\n\r"
                MXP_SECURE"<!-- now the MUD text -->\n\r"
                ,ch);

    //
    //      get x     x
    //     drop   x
    //  examine x x x
    //     look x x x
    //     wear   x
    //   remove     x
    //      eat   x
    //    drink x x
    //     fill   x
    //    quaff   x
    //   donate x
    //      zap     x
    //   recite   x
    // brandish     x
    //          ^ ^ ^ ^
    //  in room | | | |
    //  carried __| | |
    //     worn ____| |
    //  in cont ______|
    //

}


int get_page_length(DESCRIPTOR_DATA *d, CHAR_DATA *ch) {
    if (IS_NPC(ch)) return 0;
    if (ch->pcdata->default_lines>=0) return ch->pcdata->default_lines;
    if (d->lines>10) return d->lines;
    return -ch->pcdata->default_lines;
}

/* OLC, new pager for editing long descriptions. */
/* ========================================================================= */
/* - The heart of the pager.  Thanks to N'Atas-Ha, ThePrincedom for porting  */
/*   this SillyMud code for MERC 2.0 and laying down the groundwork.         */
/* - Thanks to Blackstar, hopper.cs.uiowa.edu 4000 for which the improvements*/
/*   to the pager was modeled from.  - Kahn                                  */
/* - Safer, allows very large pagelen now, and allows to page while switched */
/*   Zavod of jcowan.reslife.okstate.edu 4000.                               */
/* ========================================================================= */

void show_string( DESCRIPTOR_DATA *d, char *input ) {
    char               *start, *end;
    char                arg[MAX_INPUT_LENGTH];
    int                 lines = 0, pagelen;

    // Set the page length
    if (d->original) pagelen=get_page_length(d,d->original);
    else pagelen=get_page_length(d,d->character);

    // Hack, but it should work
    if (pagelen==0) pagelen=1000000;

    // Check for the command entered */
    one_argument( input, arg );

    switch( UPPER( *arg ) ) {
    // Show the next page
    case '\0':
    case 'C': lines = 0;
        break;

        // Scroll back a page
    case 'B': lines = -2 * pagelen;
        break;

        // Help for show page
    case 'H': write_to_buffer( d, "B     - Scroll back one page.\n\r", 0 );
        write_to_buffer( d, "C     - Continue scrolling.\n\r", 0 );
        write_to_buffer( d, "H     - This help menu.\n\r", 0 );
        write_to_buffer( d, "R     - Refresh the current page.\n\r",
                         0 );
        write_to_buffer( d, "Enter - Continue Scrolling.\n\r", 0 );
        return;

        // refresh the current page
    case 'R': lines = -1 - pagelen;
        break;

        // stop viewing
    default:  free_string( d->showstr_head );
        d->showstr_head  = NULL;
        d->showstr_point = NULL;
        return;
    }

    // do any backing up necessary to find the starting point
    if ( lines<0 ) {
        for( start= d->showstr_point; start > d->showstr_head && lines < 0;
             start-- )
            if ( *start == '\r' )
                lines++;
    } else
        start = d->showstr_point;

    // Find the ending point based on the page length
    lines  = 0;

    for ( end= start; *end && lines < pagelen; end++ )
        if ( *end == '\r' )
            lines++;

    d->showstr_point = end;

    if ( end - start )
        write_to_buffer( d, start, end - start );

    // See if this is the end (or near the end) of the string

    for ( ; isspace( *end ); end++ );

    if ( !*end ) {
        free_string( d->showstr_head );
        d->showstr_head  = NULL;
        d->showstr_point = NULL;
    }

    return;
}
