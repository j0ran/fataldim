/* $Id: act_enter.c,v 1.47 2008/01/06 11:14:12 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

#include "merc.h"
#include "interp.h"

/* command procedures needed */
DECLARE_DO_FUN(do_stand		);

/* random room generation procedure */
ROOM_INDEX_DATA  *get_random_room(CHAR_DATA *ch)
{
    ROOM_INDEX_DATA *room;

    for ( ; ; )
    {
        if ((room=get_room_index(number_range(0,MAX_VNUMS)))==NULL)
	    continue;

	// just get a random room in the mud
	if (ch==NULL)
	    break;

	if ( can_see_room(ch,room)
	&&   !room_is_private(room)
	&&   !STR_IS_SET(room->strbit_room_flags, ROOM_PRIVATE)
	&&   !STR_IS_SET(room->strbit_room_flags, ROOM_SOLITARY)
	&&   !STR_IS_SET(room->strbit_room_flags, ROOM_SAFE)
	&&   !IS_SET(room->area->area_flags, AREA_UNFINISHED)
	&&   !(IS_NPC(ch) && 
	       (STR_IS_SET(ch->strbit_act,ACT_AGGRESSIVE) || 
		STR_IS_SET(ch->strbit_act,ACT_TCL_AGGRESSIVE)) &&
	       !STR_IS_SET(room->strbit_room_flags,ROOM_LAW)))
	    break;
    }

    return room;
}


ROOM_INDEX_DATA *get_portal_destination(CHAR_DATA *ch,OBJ_DATA *portal) {
    ROOM_INDEX_DATA *location;
    int	vnum;

    if (portal->level>get_trust(ch)) {
	location=NULL;
    } else if (IS_SET(portal->wear_flags,ITEM_TAKE) &&
	STR_IS_SET(ch->in_room->strbit_room_flags,ROOM_NOMAGIC)) {
	location=NULL;
    } else if (IS_SET(portal->value[2],GATE_RECALL) &&
	GetCharProperty(ch,PROPERTY_INT,"exit_store",&vnum)) {
	portal->value[3] = vnum;
	location = get_room_index(vnum);
    } else if (IS_SET(portal->value[2],GATE_RANDOM)) {
	location = get_random_room(ch);
	portal->value[3] = location->vnum; /* for record keeping :) */
    } else if (portal->value[3] <=0 ) {
	location = get_random_room(ch);
    } else if (IS_SET(portal->value[2],GATE_BUGGY) && (number_percent()<5)) {
	location = get_random_room(ch);
    } else {
	location = get_room_index(portal->value[3]);
    }
    return location;
}


/* RT Enter portals */
/* Enter updated by Joran so it can be used for the enter_tree spell
 * By default aportal should be NULL */
void enter( CHAR_DATA *ch, char *argument,long type, OBJ_DATA *aportal)
{
    ROOM_INDEX_DATA *location;
    ROOM_INDEX_DATA *old_room;
    OBJ_DATA *portal;
    CHAR_DATA *fch, *fch_next;
    char buf[MSL];

    if ( ch->fighting != NULL )
	return;

    /* nifty portal stuff */
    if (aportal==NULL && (argument[0] == '\0')) {
	send_to_char("Nope, can't do it.\n\r",ch);
	return;
    }

    old_room = ch->in_room;

    if(aportal)
	portal=aportal;
    else
	portal = get_obj_list( ch, argument,  ch->in_room->contents );

    if (portal == NULL) {
	send_to_char("You don't see that here.\n\r",ch);
	return;
    }

    if (portal->item_type != ITEM_PORTAL
    ||  (IS_SET(portal->value[1],EX_CLOSED) && !IS_TRUSTED(ch,LEVEL_IMMORTAL)))
    {
	send_to_char("You can't seem to find a way in.\n\r",ch);
	return;
    }

    if (IS_SET(portal->wear_flags,ITEM_TAKE) &&
	STR_IS_SET(ch->in_room->strbit_room_flags,ROOM_NOMAGIC)) {
	send_to_char("It doesn't seem to be working right now.\n\r",ch);
        return;
    }

    if(IS_SET(type,GATE_CLIMB) && !IS_SET(portal->value[2],GATE_CLIMB))
    {
	send_to_char("You can not climb that.\n\r", ch );
	return;
    }

    if(IS_SET(type,GATE_JUMP) && !IS_SET(portal->value[2],GATE_JUMP))
    {
	send_to_char("You can not jump into that.\n\r", ch );
	return;
    }

    if(type==0 && IS_SET(portal->value[2],GATE_CLIMB|GATE_JUMP))
    {
	send_to_char("You can't seem to find a way in.\n\r", ch );
	return;
    }

    if (!IS_TRUSTED(ch,LEVEL_IMMORTAL) && !IS_SET(portal->value[2],GATE_NOCURSE)
    &&  (IS_AFFECTED(ch,EFF_CURSE)
    ||   STR_IS_SET(old_room->strbit_room_flags,ROOM_NO_RECALL)))
    {
	send_to_char("Something prevents you from leaving...\n\r",ch);
	return;
    }

    if (GetObjectProperty(portal,PROPERTY_STRING,"clan",buf) && !IS_IMMORTAL(ch))
	if ((IS_PC(ch) && (ch->clan==NULL || str_cmp(buf,ch->clan->clan_info->name))) ||
	    (IS_NPC(ch) && ((ch->master) && (ch->master->clan==NULL ||
					     str_cmp(buf,ch->master->clan->clan_info->name))))) {
	    sprintf_to_char(ch,
		"%s will only let %s members pass.\n\r",
		portal->short_descr,buf);
	    return;
    }

    location=get_portal_destination(ch,portal);

    if (location == NULL
    ||  location == old_room
    ||  !can_see_room(ch,location)
    ||  (!is_room_owner(ch,location) &&
	 room_is_private(location) &&
	 !is_allowed_in_room(ch,location) &&
	 !IS_TRUSTED(ch,LEVEL_COUNCIL))) {
       act("$p doesn't seem to go anywhere.",ch,portal,NULL,TO_CHAR);
       return;
    }

    if (IS_NPC(ch) && (STR_IS_SET(ch->strbit_act,ACT_AGGRESSIVE) ||
		       STR_IS_SET(ch->strbit_act,ACT_TCL_AGGRESSIVE))
    &&  STR_IS_SET(location->strbit_room_flags,ROOM_LAW))
    {
	send_to_char("Something prevents you from leaving...\n\r",ch);
	return;
    }

    if (OBJ_HAS_TRIGGER(portal,OTRIG_PREENTER) &&
	!op_preenter_trigger(portal,ch)) 
	return;

    if(IS_SET(type,GATE_CLIMB))
    {
	act("$n climbs $p.",ch,portal,NULL,TO_ROOM);
	if (IS_SET(portal->value[2],GATE_NORMAL_EXIT))
	    act("You climb $p.",ch,portal,NULL,TO_CHAR);
	else
	    act("You climb $p and find yourself somewhere else...",
		ch,portal,NULL,TO_CHAR);
    }
    else if(IS_SET(type,GATE_JUMP))
    {
	act("$n jumps into $p.",ch,portal,NULL,TO_ROOM);
	if (IS_SET(portal->value[2],GATE_NORMAL_EXIT))
	    act("You jump into $p.",ch,portal,NULL,TO_CHAR);
	else
	    act("You jump into $p and find yourself somewhere else...",
		ch,portal,NULL,TO_CHAR);
    }
    else
    {
	act("$n steps into $p.",ch,portal,NULL,TO_ROOM);
	if (IS_SET(portal->value[2],GATE_NORMAL_EXIT))
	    act("You enter $p.",ch,portal,NULL,TO_CHAR);
	else
	    act("You walk through $p and find yourself somewhere else...",
		ch,portal,NULL,TO_CHAR);
    }

    if (IS_SET(portal->value[2],GATE_STORE)) {
	SetCharProperty(ch,PROPERTY_INT,"exit_store",&old_room->vnum);
    } else if (IS_SET(portal->value[2],GATE_RECALL)) {
	DeleteCharProperty(ch,PROPERTY_INT,"exit_store");
    }

    ap_leave_trigger(ch,location);
    char_from_room(ch);
    char_to_room(ch, location);
    if (IS_PC(ch))
	STR_SET_BIT(ch->pcdata->usedthatobject,portal->pIndexData->vnum);

    if (IS_SET(portal->value[2],GATE_GOWITH)) /* take the gate along */
    {
	obj_from_room(portal);
	obj_to_room(portal,location);
    }

    if (IS_SET(portal->value[2],GATE_NORMAL_EXIT))
	act("$n has arrived.",ch,portal,NULL,TO_ROOM);
    else
	act("$n has arrived through $p.",ch,portal,NULL,TO_ROOM);

    ap_enter_trigger(ch,old_room);

    if (OBJ_HAS_TRIGGER(portal,OTRIG_ENTER))
	op_enter_trigger(portal,ch); 

    look_room(ch,ch->in_room,TRUE);

    /* charges */
    if (portal->value[0] > 0)
    {
	portal->value[0]--;
	if (portal->value[0] == 0)
	    portal->value[0] = -1;
    }

    /* protect against circular follows */
    if (old_room == location)
	return;

    for ( fch = old_room->people; fch != NULL; fch = fch_next )
    {
	fch_next = fch->next_in_room;

	if (portal == NULL || portal->value[0] == -1)
	/* no following through dead portals */
	    continue;

	if ( fch->master == ch && IS_AFFECTED(fch,EFF_CHARM)
	&&   fch->position < POS_STANDING && fch->position > POS_SLEEPING)
	    do_function(fch,&do_stand,"");

	if ( fch->master == ch && fch->position == POS_STANDING)
	{

	    if (STR_IS_SET(ch->in_room->strbit_room_flags,ROOM_LAW)
	    &&  (IS_NPC(fch) && (STR_IS_SET(fch->strbit_act,ACT_AGGRESSIVE) || 
				 STR_IS_SET(fch->strbit_act,ACT_TCL_AGGRESSIVE))))
	    {
		act("You can't bring $N into the city.",
		    ch,NULL,fch,TO_CHAR);
		act("You aren't allowed in the city.",
		    fch,NULL,NULL,TO_CHAR);
		continue;
	    }

	    act( "You follow $N.", fch, NULL, ch, TO_CHAR );
	    enter(fch,argument,type,aportal);
	}
    }

    if (portal != NULL && portal->value[0] == -1)
    {
	act("$p fades out of existence.",ch,portal,NULL,TO_CHAR);
	if (ch->in_room == old_room)
	    act("$p fades out of existence.",ch,portal,NULL,TO_ROOM);
	else if (old_room->people != NULL)
	{
	    act("$p fades out of existence.",
		old_room->people,portal,NULL,TO_CHAR);
	    act("$p fades out of existence.",
		old_room->people,portal,NULL,TO_ROOM);
	}
	extract_obj(portal);
    }

    /*
     * If someone is following the char, these triggers get activated
     * for the followers before the char, but it's safer this way...
     */
    if ( IS_NPC( ch ) && MOB_HAS_TRIGGER( ch, MTRIG_ENTRY ) )
	mp_entry_trigger(ch);
    if (!IS_VALID(ch))
	return;
    // for historical purposes, should be deleted
    if ( !IS_NPC( ch ) )
	mp_greet_trigger( ch, DIR_DOWN );

    return;

}

void do_enter( CHAR_DATA *ch, char *argument)
{
    enter(ch,argument,0,NULL);
}

void do_jump( CHAR_DATA *ch, char *argument)
{
    enter(ch,argument,GATE_JUMP,NULL);
}

void do_climb( CHAR_DATA *ch, char *argument)
{
    enter(ch,argument,GATE_CLIMB,NULL);
}



void do_welcome(CHAR_DATA *ch, char *argument) {
    ROOM_INDEX_DATA *room;
    char thisname[MAX_STRING_LENGTH];
    char s[MAX_STRING_LENGTH];
    char command[MIL],arg1[MIL];
    char originalargument[MIL];
    bool b,multiple;
    char names[MSL],*p;

    if (IS_NPC(ch)) {
	send_to_char("Oh? And who bought you this room?\n\r",ch);
	return;
    }

    room=ch->in_room;

    if (!is_room_owner(ch,room)) {
	send_to_char("This is not your private room!\n\r",ch);
	return;
    }

    strcpy(originalargument,argument);
    argument=one_argument(argument,command);
    argument=one_argument(argument,arg1);

    switch (which_keyword(command,"all","none","delete","add","remove",NULL)) {
	default:
	    send_to_char("Try: welcome, welcome all, welcome none,\n\r",ch);
	    send_to_char("welcome delete,welcome add name or\n\r",ch);
	    send_to_char("welcome remove name\n\r",ch);
	    return;

	case 1: // all
	    b=TRUE;
	    SetRoomProperty(room,PROPERTY_BOOL,"openhouse",&b);
	    send_to_char("This room is now open for everybody.\n\r",ch);
	    break;

	case 2: // none
	    DeleteRoomProperty(room,PROPERTY_BOOL,"openhouse");
	    if (GetRoomProperty(room,PROPERTY_STRING,"openhouse",s))
		sprintf_to_char(ch,
		    "This room is now closed for everybody, except for %s.\n\r",
		    s);
	    else
		send_to_char("This room is now closed for everybody.\n\r",ch);
	    break;

	case 3: // delete
	    DeleteRoomProperty(room,PROPERTY_STRING,"openhouse");
	    DeleteRoomProperty(room,PROPERTY_BOOL,"openhouse");
	    send_to_char("This room is now closed for everybody, "
		"without any exceptions.\n\r",ch);
	    break;

	case 4: // add
	    if (arg1[0]==0) {
		send_to_char("Who do you want to welcome in this room?\n\r",ch);
		return;
	    }

	    names[0]=0;
	    GetRoomProperty(room,PROPERTY_STRING,"openhouse",names);
	    if (strlen(names)+strlen(arg1)+10>MSL) {
		send_to_char("Too many people are welcome, why don't you throw it open for everybody?\n\r",ch);
		return;
	    }

	    if (is_exact_name(arg1,names)) {
		send_to_char("That person is already welcome.\n\r",ch);
		return;
	    }

	    if (names[0]!=0)
		strcat(names," ");
	    strcat(names,arg1);
	    SetRoomProperty(room,PROPERTY_STRING,"openhouse",names);

	    sprintf_to_char(ch,"%s is now welcome here.\n\r",arg1);

	    break;

	case 5: // remove
	    if (arg1[0]==0) {
		send_to_char("Who is not welcome anymore in this room?\n\r",ch);
		return;
	    }

	    names[0]=0;
	    GetRoomProperty(room,PROPERTY_STRING,"openhouse",names);
	    if (names[0]==0) {
		send_to_char("Nobody was welcome here anyway.\n\r",ch);
		return;
	    }

	    if (!is_exact_name(arg1,names)) {
		send_to_char("That person was not welcome here anyway.\n\r",ch);
		return;
	    }

	    // do some magic with strings :-)
	    strcat(arg1," ");
	    if ((p=str_str(arg1,names))==NULL) {
		p=names;
		if (strlen(names)>strlen(arg1)) {
		    p=names+strlen(names)-strlen(arg1);
		}
		p[0]=0;
	    } else {
		strcpy(p,p+strlen(arg1));
	    }

	    SetRoomProperty(room,PROPERTY_STRING,"openhouse",names);
	    if (names[0]==0)
		DeleteRoomProperty(room,PROPERTY_STRING,"openhouse");
	    sprintf_to_char(ch,"%sis not welcome here anymore.\n\r",arg1);

	    break;

	case -1:
	    b=FALSE;
        GetRoomProperty(room,PROPERTY_BOOL,"openhouse",&b);
        GetRoomProperty(room,PROPERTY_STRING,"openhouse",thisname);

	    if (b)
		send_to_char("Everybody is welcome in this room.\n\r",ch);
	    else
		send_to_char("Nobody is welcome in this room.\n\r",ch);

	    if (GetRoomProperty(room,PROPERTY_STRING,"openhouse",s)) {
		multiple=strchr(s,' ')==NULL?FALSE:TRUE;
		if (b) {
		    sprintf_to_char(ch,
			"Normally, the only person%s allowed %s: %s\n\r",
			multiple?"s":"",
			multiple?"are":"is",
			s);
		} else {
		    sprintf_to_char(ch,
			"%sxception%s made for: %s\n\r",
			multiple?"E":"An e",
			multiple?"s are":" is",
			s);
		}
	    }
	    return;
    }
    SET_BIT(room->area->area_flags,AREA_CHANGED);
}
