//
// $Id: interp.c,v 1.179 2007/11/25 13:32:49 jodocus Exp $ */
//
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

#include "merc.h"
#include "interp.h"
#include "db.h"
#ifdef I3
#include "intermud3/fd_i3.h"
#endif

bool	check_social	( CHAR_DATA *ch, char *command,
			    char *argument );

/* Highest grant/revoke command vnum = 139, remember updating this number.
   when adding immortal command. Remeber to watch the maximum bitstr
   length. It is now 32.

   Note that 60 is reserved for immtalk and is used in act_comm.c/immtalk.

   The last entry in the struct is 'disabled', which is to disable a command.

   Best viewed in widescreen (150 characters wide)
*/

/*
 * Command table.
 */
struct	cmd_type	cmd_table	[] =
{
    /*
     * Common movement commands.
     */
    { "north",		do_north,	POS_SITTING,  LOG_NEVER, COMM_ACTIVE, SHOW_DONT,    0 },
    { "east",		do_east,	POS_SITTING,  LOG_NEVER, COMM_ACTIVE, SHOW_DONT,    0 },
    { "south",		do_south,	POS_SITTING,  LOG_NEVER, COMM_ACTIVE, SHOW_DONT,    0 },
    { "west",		do_west,	POS_SITTING,  LOG_NEVER, COMM_ACTIVE, SHOW_DONT,    0 },
    { "up",		do_up,		POS_STANDING, LOG_NEVER, COMM_ACTIVE, SHOW_DONT,    0 },
    { "down",		do_down,	POS_SITTING,  LOG_NEVER, COMM_ACTIVE, SHOW_DONT,    0 },

    /*
     * Common other commands.
     * Placed here so one and two letter abbreviations work.
     */
    { "at",		do_at,		POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    32 }, // clashes with attack
    { "ban",		do_ban,		POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,     6 }, // clashes with bandage
    { "cast",		do_cast,	POS_FIGHTING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "clan",		do_clan,	POS_SLEEPING, LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "exits",		do_exits,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "get",		do_get,		POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "group",          do_group,       POS_SLEEPING, LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 }, // clashes with groups
    { "inventory",	do_inventory,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "kill",		do_kill,	POS_FIGHTING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "look",		do_look,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "music",          do_music,   	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 }, // clashes with mobkill
    { "order",		do_order,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "rest",		do_rest,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "sit",		do_sit,		POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "sleep",		do_sleep,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 }, // clashes with slaughter
    { "stand",		do_stand,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "tell",		do_tell,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "tag",		do_tag,		POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "unlock",         do_unlock,      POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 }, // clashes with unalias
    { "wield",		do_wear,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },

    /*
     * Informational commands.
     */
    { "effects",	do_effects,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "areas",		do_areas,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "bug",		do_bug,		POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "changes",	do_changes,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "commands",	do_commands,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "compare",	do_compare,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "consider",	do_consider,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
//  { "count",		do_count,	POS_SLEEPING, LOG_NORMAL, COMM_INFORM, SHOW_DO,	    0 }, // obsolete by who
    { "credits",	do_credits,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "effects",	do_effects,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "equipment",	do_equipment,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "examine",	do_examine,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "finger",		do_finger,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
//  { "groups",		do_groups,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 }, // misc. commands
    { "help",		do_help,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "herolist",	do_herolist,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "idea",		do_idea,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "info",           do_groups,      POS_SLEEPING, LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "mcnote",		do_mcnote,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "mobkill",	do_mobkill,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "motd",		do_motd,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "news",		do_news,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "read",		do_read,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "report",		do_report,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "rules",		do_rules,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "score",		do_score2,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "skills",		do_skills,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "socials",	do_socials,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "show",		do_show,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "spells",		do_spells,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "story",		do_story,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "time",		do_time,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "typo",		do_typo,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "weather",	do_weather,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "who",		do_who,		POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
//  { "whois",		do_whois,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 }, // obsolete by who / finger
    { "wizlist",	do_wizlist,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "worth",		do_worth,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },

    /*
     * Configuration commands.
     */
    { "config",		do_config,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "alia",		do_alia,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "alias",		do_alias,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "auto",		do_autolist,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "autoloot",	do_autoloot,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "autoassist",	do_autoassist,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "autogold",	do_autogold,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "autoexit",	do_autoexit,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "autosac",	do_autosac,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "autosplit",	do_autosplit,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "autotitle",	do_autotitle,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "autocompact",	do_autocompact,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "autocombine",	do_autocombine,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "brief",		do_brief,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "channels",       do_channels,    POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "colour",		do_colour,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "color",		do_colour,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "combine",	do_autocombine,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "description",	do_description,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "delet",		do_delet,	POS_DEAD,     LOG_ALWAYS, COMM_INFORM, SHOW_DONT,   0 },
    { "delete",		do_delete,	POS_STANDING, LOG_ALWAYS, COMM_INFORM, SHOW_DO,     0 },
    { "email",		do_email,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "hardcore",	do_hardcore,	POS_DEAD,     LOG_ALWAYS, COMM_INFORM, SHOW_DO,     0 },
    { "mxp",		do_mxp,		POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "nofollow",	do_nofollow,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "noloot",		do_noloot,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "nosummon",	do_nosummon,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "outfit",		do_outfit,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "password",	do_password,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "prompt",		do_prompt,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "pueblo",		do_pueblo,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "puebloclient",   do_puebloclient,POS_DEAD,     LOG_ALWAYS, COMM_INFORM, SHOW_DONT,   0 },
    { "questinfo",	do_questinfo,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "room",		do_room,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "scroll",		do_scroll,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "title",		do_title,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "unalias",	do_unalias,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "welcome",	do_welcome,	POS_RESTING,  LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "wimpy",		do_wimpy,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },

    /*
     * Communication commands.
     */
    { "afk",		do_afk,		POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "answer",		do_answer,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "announce",	do_announce,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "auction",        do_auction,     POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "clantalk",	do_clantalk,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "ct",		do_clantalk,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,   0 },
    { "deaf",		do_deaf,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "emote",		do_emote,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { ".",		do_gossip,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,   0 },
    { "gossip",		do_gossip,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { ",",		do_emote,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,   0 },
    { "grats",		do_grats,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "gtell",		do_gtell,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "herotalk",	do_herotalk,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "ht",		do_herotalk,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,   0 },
//  { "music",          do_music,   	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "mutter",		do_mutter,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "whisper",	do_whisper,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "newbie",		do_newbie,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "note",		do_note,	POS_SLEEPING, LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "notebook",       do_notebook,    POS_SLEEPING, LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "page",		do_page,	POS_SLEEPING, LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "pmote",		do_pmote,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "poll",		do_poll,	POS_RESTING,  LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "pose",		do_pose,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "pray",		do_pray,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "qc",		do_quest,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "questchannel",	do_quest,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "question",	do_question,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "quote",		do_quote,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "quiet",		do_quiet,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "reply",		do_reply,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "replay",		do_replay,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "say",		do_say,		POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "'",		do_say,		POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,   0 },
    { "shout",		do_shout,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "unread",		do_unread,	POS_SLEEPING, LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "yell",		do_yell,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },

    /*
     * Object manipulation commands.
     */
    { "brandish",	do_brandish,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "brew",		do_brew,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "buy",		do_buy,		POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "carve",		do_carve,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "close",		do_close,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "diagnose",	do_diagnose,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "donate",		do_donate,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "drink",		do_drink,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "drop",		do_drop,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "eat",		do_eat,		POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "envenom",	do_envenom,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "fill",		do_fill,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "findtrap",	do_findtrap,	POS_STANDING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "give",		do_give,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "heal",		do_heal,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "install",	do_install,	POS_STANDING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "junk",           do_sacrifice,   POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,   0 },
    { "hold",		do_wear,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "list",		do_list,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "lock",		do_lock,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "nosell",		do_nosell,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "open",		do_open,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "pick",		do_pick,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "pour",		do_pour,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "put",		do_put,		POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "quaff",		do_quaff,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "recite",		do_recite,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "remove",		do_remove,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "removetrap",	do_removetrap,	POS_STANDING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "repair",		do_repair,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "scribe",		do_scribe,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "sell",		do_sell,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "second",		do_second,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "sacrifice",	do_sacrifice,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "take",		do_get,		POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "tap",      	do_sacrifice,   POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,   0 },
//  { "unlock",         do_unlock,      POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "value",		do_value,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "wear",		do_wear,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "zap",		do_zap,		POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },

    /*
     * Combat commands.
     */
    { "attack",		do_attack,	POS_FIGHTING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "backstab",	do_backstab,	POS_FIGHTING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "bash",		do_bash,	POS_FIGHTING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "bs",		do_backstab,	POS_FIGHTING, LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,   0 },
    { "berserk",	do_berserk,	POS_FIGHTING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "dirt",		do_dirt,	POS_FIGHTING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "dirge",		do_war_dirge,	POS_FIGHTING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "disarm",		do_disarm,	POS_FIGHTING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "eyepoke",	do_eyepoke,	POS_FIGHTING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "flee",		do_flee,	POS_FIGHTING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "headbutt",	do_headbutt,	POS_FIGHTING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "hit",		do_kill,	POS_FIGHTING, LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,   0 },
    { "hymn",		do_battle_hymn,	POS_FIGHTING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "hunt",		do_hunt,	POS_STANDING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "kick",		do_kick,	POS_FIGHTING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "murde",		do_murde,	POS_FIGHTING, LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,   0 },
    { "murder",		do_murder,	POS_FIGHTING, LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,     0 },
    { "rescue",		do_rescue,	POS_FIGHTING, LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,   0 },
    { "scare",		do_scare,	POS_STANDING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "slaughter",	do_slaughter,	POS_FIGHTING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "surrender",	do_surrender,	POS_FIGHTING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "trip",		do_trip,	POS_FIGHTING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },

    /*
     * Miscellaneous commands.
     */
    { "abandon",	do_abandon,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "balance",	do_balance,	POS_STANDING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "bandage",	do_bandage,	POS_STANDING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "enter", 		do_enter, 	POS_STANDING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "climb", 		do_climb, 	POS_STANDING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "deposit",	do_deposit,	POS_STANDING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "exile",		do_exile,	POS_STANDING, LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,     0 },
    { "invite",		do_invite,	POS_STANDING, LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,     0 },
    { "follow",		do_follow,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "gain",		do_gain,	POS_STANDING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "go",		do_enter,	POS_STANDING, LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,   0 },
    { "groups",		do_groups,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "hide",		do_hide,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "jump", 		do_jump, 	POS_STANDING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "loner",		do_loner,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "layonhands",	do_layonhands,	POS_SITTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "leader",		do_leader,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "mmmm",		do_mmmm,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,   0 },
    { "paddle",		do_paddle,	POS_SITTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
#ifdef HAS_JUKEBOX
    { "play",		do_play,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
#endif
    { "practice",       do_practice,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "peek",		do_peek,	POS_SITTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "puke",		do_puke,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,   0 },
    { "rank",		do_rank,	POS_STANDING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "qui",		do_qui,		POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,   0 },
    { "quit",		do_quit,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "restart",	do_restart,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,     0 },
    { "recall",		do_recall,	POS_FIGHTING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "/",		do_recall,	POS_FIGHTING, LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,   0 },
    { "recruit",	do_recruit,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "rent",		do_rent,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,   0 },
    { "save",		do_save,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "sharpen",	do_sharpen,	POS_SITTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "shove",		do_shove,	POS_SITTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
//  { "sleep",		do_sleep,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "sneak",		do_sneak,	POS_STANDING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "split",		do_split,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "specialize",	do_specialize,	POS_STANDING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "steal",		do_steal,	POS_STANDING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "stealth",	do_stealth,	POS_SITTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "tagjoin",	do_tagjoin,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "test",		do_test,	POS_SLEEPING, LOG_NORMAL, COMM_INFORM, SHOW_DONT,   0 },
    { "train",		do_train,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "use",		do_use,		POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "visible",	do_visible,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "wake",		do_wake,	POS_SLEEPING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "where",		do_where,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "withdraw",	do_withdraw,	POS_STANDING, LOG_NORMAL, COMM_ACTIVE, SHOW_DO,     0 },
    { "xyzzy",		do_xyzzy,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,   0 },

    /*
     * Immortal commands.
     */
    { "advance",	do_advance,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,     1 },
    { "aexit",		do_aexit,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,    83 },
    { "allow",		do_allow,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,     5 },
    { "audit",		do_audit,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,   124 },
    { "clone",		do_clone,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    58 },
    { "copyove",	do_copyove,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DONT, 108 },
    { "copyover",	do_copyover,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,   108 },
    { "deny",		do_deny,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,     7 },
    { "disconnect",	do_disconnect,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,     8 },
    { "dump",		do_dump,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DONT,   2 },
    { "echo",		do_recho,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    49 },
    { "force",		do_force,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    19 },
    { "freeze",		do_freeze,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    10 },
    { "gecho",		do_gecho,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    35 },
    { "goto",           do_goto,        POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    36 },
    { "grant",		do_grant,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    73 },
    { "guild",		do_guild,	POS_DEAD,     LOG_ALWAYS, COMM_INFORM, SHOW_DO,    75 },
    { "holylight",	do_holylight,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    37 },
    { "immtalk",	do_immtalk,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    60 },
    { ":",		do_immtalk,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,  60 },
    { "imotd",          do_imotd,       POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,    61 },
    { "incognito",	do_incognito,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    38 },
    { "invis",		do_invis,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,  39 },
    { "job",		do_job,		POS_DEAD,     LOG_ALWAYS, COMM_INFORM, SHOW_DO,   132 },
    { "load",		do_load,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    20 },
    { "log",		do_log,		POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    40 },
    { "memory",		do_memory,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,    41 },
    { "mwhere",		do_mwhere,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,    42 },
    { "newlock",	do_newlock,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    21 },
    { "nochannels",	do_nochannels,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    22 },
    { "nocolor",	do_nocolor,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,   125 },
    { "noemote",	do_noemote,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    23 },
    { "noshout",	do_noshout,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    24 },
    { "nonote",		do_nonote,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,   139 },
    { "notell",		do_notell,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    25 },
    { "owhere",		do_owhere,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,    45 },
    { "pardon",		do_pardon,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    27 },
    { "peace",		do_peace,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    46 },
    { "pecho",		do_pecho,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    26 },
    { "penalty",	do_penalty,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,     0 },
    { "poofin",		do_bamfin,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    33 },
    { "poofout",	do_bamfout,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    34 },
    { "prefi",		do_prefi,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,  64 },
    { "prefix",		do_prefix,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    64 },
    { "protect",	do_protect,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    12 },
    { "purge",		do_purge,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    28 },
    { "pwhere",		do_pwhere,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,   130 },
    { "questpoints",	do_questpoints,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    48 },
    { "reboo",		do_reboo,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,  13 },
    { "reboot",		do_reboot,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    13 },
    { "restore",	do_restore,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    29 },
    { "return",         do_return,      POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    50 },
    { "revoke",		do_revoke,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    74 },
    { "rwhere",		do_rwhere,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,    84 },
    { "set",		do_set,		POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    14 },
    { "shutdow",	do_shutdow,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,  15 },
    { "shutdown",	do_shutdown,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    15 },
    { "sla",		do_sla,		POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,  30 },
    { "slay",		do_slay,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    30 },
    { "smit",		do_smit,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    64 },
    { "smite",		do_smite,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    64 },
    { "smote",		do_smote,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    63 },
    { "snoop",		do_snoop,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    51 },
    { "sockets",        do_sockets,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,    17 },
    { "stat",		do_stat,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,    52 },
    { "string",		do_string,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    53 },
    { "swearlist",	do_swearlist,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,    16 },
    { "switch",		do_switch,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    54 },
    { "tagging",	do_tagging,	POS_RESTING,  LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    62 },
    { "transfer",	do_transfer,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    31 },
    { "trust",		do_trust,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,     3 },
    { "violate",	do_violate,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,     4 },
    { "vnum",		do_vnum,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,    56 },
    { "wizhelp",	do_wizhelp,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,    76 },
    { "wizinvis",	do_invis,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    55 },
    { "wizlock",	do_wizlock,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    18 },
    { "wiznet",		do_wiznet,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,    59 },
    { "aecho",		do_aecho,	POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    57 },
/*  { "at",		do_at,		POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    32 }, */
/*  { "goto",		do_goto,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    36 }, */
//  { "teleport",	do_transfer,    POS_DEAD,     LOG_ALWAYS, COMM_ACTIVE, SHOW_DO,    31 },

    //
    // mob/object/room progs
    //
    { "mpdump",		do_mpdump,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,    43 },
    { "mpedit",		do_mpedit,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    69 },
    { "mpstat",		do_mpstat,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,    44 },
    { "mpwhere",	do_mpwhere,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,    47 },
    { "opdump",		do_opdump,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,   120 },
    { "opedit",		do_opedit,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,   123 },
    { "opstat",		do_opstat,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,   121 },
    { "opwhere",	do_opwhere,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,   122 },
    { "rpdump",		do_rpdump,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,   126 },
    { "rpedit",		do_rpedit,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,   129 },
    { "rpstat",		do_rpstat,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,   127 },
    { "rpwhere",	do_rpwhere,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,   128 },
    { "apdump",		do_apdump,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,   135 },
    { "apedit",		do_apedit,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,   136 },
    { "apstat",		do_apstat,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,   137 },
    { "apwhere",	do_apwhere,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,   138 },

    /*
     * OLC 1.1b
     */
    { "aedit",		do_aedit,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    65 },
    { "alist",		do_alist,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    71 },
    { "asave",          do_asave,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    70 },
    { "hedit",		do_hedit,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    82 },
    { "medit",		do_medit,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    68 },
    { "oedit",		do_oedit,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    67 },
    { "olchelp",	do_olchelp,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,   111 },
    { "?",		do_olchelp,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,   111 },
    { "propertylist",	do_propertylist,POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,   109 },
    { "redit",		do_redit,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    66 },
    { "resets",		do_resets,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,    72 },
    { "socialedit",	do_socialedit,	POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,   107 },
    { "resetedit",	do_resetedit,	POS_DEAD,     LOG_NORMAL, COMM_INFORM, SHOW_DO,   133 },

#ifdef I3
    //
    // intermud3 commands
    //
    { "i3",		do_i3,		POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,   114 },
    { "i3emote",	do_i3emote,     POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,   119 },
    { "i3finger",	do_i3finger,    POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,   116 },
    { "i3locate",	do_i3locate,    POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,   117 },
    { "i3tell",		do_i3tell,      POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,   118 },
    { "i3who",		do_i3who,       POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DO,   115 },
#endif

    //
    // End of list.
    //
    { NULL,		NULL,		POS_DEAD,     LOG_NORMAL, COMM_ACTIVE, SHOW_DONT,   0 }
};

static char *filter_pueblo(char *in) {
    char *p;
    while ((p=str_str("</xch_mudtext>",in))) {
	p[0]='[';
	p[13]=']';
    }
    return in;
}

//
// The main entry point for executing commands.
// Can be recursively called from 'at', 'order', 'force'.
//
void interpret( CHAR_DATA *ch, char *argument ) {
    char command[MAX_INPUT_LENGTH];
    char logline[MAX_INPUT_LENGTH];
    char originalcommand[MSL];
    int cmd;
    bool found;
    bool no_interpret_trigger=FALSE;

    if (STR_IS_SET(ch->strbit_comm,COMM_AFK) &&
        STR_IS_SET(ch->strbit_comm,COMM_AFK_BY_IDLE))
	do_function(ch,&do_afk,"");

    //
    // If the first character on the line is a ~, then don't fire off the
    // the interpret-triggers.
    //
    if ((get_trust(ch)>LEVEL_IMMORTAL) && (argument[0]=='~')) {
	no_interpret_trigger=TRUE;
	argument++;
    }

    filter_pueblo(argument);

    /*
     * Strip leading spaces.
     */
    while ( isspace(*argument) )
	argument++;
    if ( argument[0] == 0 )
	return;

    //
    // Grab the command word.
    // Special parsing so ' can be a command,
    //   also no spaces needed after punctuation.
    //
    strcpy( logline, argument );
    if ( !isalpha(argument[0]) && !isdigit(argument[0]) ) {
	command[0] = argument[0];
	command[1] = 0;
	argument++;
	while ( isspace(*argument) )
	    argument++;
    } else {
	argument = one_argument( argument, command );
    }

    //
    // Look for command in command table.
    //
    found = FALSE;
    for ( cmd = 0; cmd_table[cmd].name != NULL; cmd++ ) {
	if ( tolower(command[0]) == cmd_table[cmd].name[0]
	&&   !str_prefix( command, cmd_table[cmd].name )) {
	    int vnum=cmd_table[cmd].vnum;
	    bool fine=FALSE;

	    if (vnum!=0) {

	        if (IS_PC(ch)) {
		    // for players, check the grands
	            fine|=STR_IS_SET(ch->pcdata->strbitgrand,vnum);
		    fine|=is_command_allowed(ch,vnum);
	        } else {
	            if (ch->desc) {
			// if the mob is logged, allow it
			if (ch->desc->connected==CON_LOGGING)
			    fine=TRUE;

			// for switched players NPC's, check the original player
			if (ch->desc->original!=NULL
			&& ch->desc->original->pcdata!=NULL) {
			    fine|=STR_IS_SET(ch->desc->original->pcdata->strbitgrand,vnum);
			    fine|=is_command_allowed(ch,vnum);
			}
		    }
	        }

		if (!fine)
		    continue;
            }
	    //
	    // if the command is explicitly disabled, deny it
	    //
	    if (cmd_table[cmd].disabled==TRUE)
		continue;

	    found = TRUE;
	    break;
	}
    }

    //
    // Log and snoop.
    //
    if ( cmd_table[cmd].log == LOG_NEVER )
	strcpy( logline, "" );
    else {
	if ( ( !IS_NPC(ch) && STR_IS_SET(ch->strbit_act, PLR_LOG) )
	|| ( mud_data.logAllPlayers && IS_PC(ch))
	|| ( mud_data.logAllMobs && IS_NPC(ch))
	||   cmd_table[cmd].log == LOG_ALWAYS )
	{
	    char	s[2*MAX_INPUT_LENGTH],*ps;
	    char	buf[MSL];
	    int	i;

	    ps=s;
	    sprintf( buf, "%sLog %s: %s",
		(cmd_table[cmd].log == LOG_ALWAYS)?"Cmd":"Plr",
		ch->name,
		logline);
	    // Make sure that was is displayed is what is typed
	    for (i=0;buf[i];i++) {
		*ps++=buf[i];
		if (buf[i]=='$')
		    *ps++='$';
		if (buf[i]=='{')
		    *ps++='{';
	    }
	    *ps=0;
	    if (cmd_table[cmd].log == LOG_ALWAYS)
		wiznet(WIZ_CMD_LOG,get_trust(ch),ch,NULL,"%s",s);
	    else
		if ((!IS_NPC(ch) && STR_IS_SET(ch->strbit_act, PLR_LOG) )
		|| ( mud_data.logAllPlayers && IS_PC(ch))
		|| ( mud_data.logAllMobs && IS_NPC(ch)))
		    wiznet(WIZ_PLR_LOGS,get_trust(ch),ch,NULL,"%s",s);
	    logf( "[%d] %s",GET_DESCRIPTOR(ch),buf );
	}
    }

    if ( ch->desc != NULL && ch->desc->snoop_by != NULL ) {
	write_to_buffer( ch->desc->snoop_by, "% ",    2 );
	write_to_buffer( ch->desc->snoop_by, logline, 0 );
	write_to_buffer( ch->desc->snoop_by, "\n\r",  2 );
    }

    //
    // Implement freeze command. After the logging, so we can see
    // what they are doing.
    //
    if ( !IS_NPC(ch) && STR_IS_SET(ch->strbit_act, PLR_FREEZE) ) {
	send_to_char( "You're totally frozen!\n\r", ch );
	return;
    }

    if ( !found ) {
	//
	// Look for command in socials table.
	//
	if ( !check_social( ch, command, argument ) ) {
#ifdef I3
	    if ( !I3_command_hook(ch, command, argument) ) {
#endif
		strcpy(originalcommand,command);
		if (argument[0]) {
		    strcat(originalcommand," ");
		    strcat(originalcommand,argument);
		}
		if (no_interpret_trigger ||
		   (!mp_interpret_unknown(ch,originalcommand) &&
		    !op_interpret_unknown(ch,originalcommand) &&
		    !rp_interpret_unknown(ch,originalcommand))) {
		    send_to_char( "Arglebargle, glop-glyf!?!?\n\r", ch );
		}
#ifdef I3
	    }
#endif
	} else
	    // socials make you come out of hiding, always
	    STR_REMOVE_BIT( ch->strbit_affected_by, EFF_HIDE );
	return;
    }

    // save it for triggers
    strcpy(originalcommand,cmd_table[cmd].name);
    if (argument[0]!=0) {
	strcat(originalcommand," ");
	strcat(originalcommand,argument);
    }

    //
    // No hiding.
    //
    if (cmd_table[cmd].active==COMM_ACTIVE)
	STR_REMOVE_BIT( ch->strbit_affected_by, EFF_HIDE );

    //
    // Character not in position for command?
    //
    if ( ch->position < cmd_table[cmd].position )
    {
	switch( ch->position )
	{
	case POS_DEAD:
	    send_to_char( "Lie still; you are DEAD.\n\r", ch );
	    break;

	case POS_MORTAL:
	case POS_INCAP:
	    send_to_char( "You are hurt far too bad for that.\n\r", ch );
	    break;

	case POS_STUNNED:
	    send_to_char( "You are too stunned to do that.\n\r", ch );
	    break;

	case POS_SLEEPING:
	    send_to_char( "In your dreams, or what?\n\r", ch );
	    break;

	case POS_RESTING:
	    send_to_char( "Nah... You feel too relaxed...\n\r", ch);
	    break;

	case POS_SITTING:
	    send_to_char( "Better stand up first.\n\r",ch);
	    break;

	case POS_FIGHTING:
	    send_to_char( "No way!  You are still fighting!\n\r", ch);
	    break;

	}
	return;
    }

    //
    // Dispatch the command.
    //
    if (!no_interpret_trigger)
	if (mp_interpret_preknown(ch,originalcommand) ||
	    op_interpret_preknown(ch,originalcommand) ||
	    rp_interpret_preknown(ch,originalcommand))
	    return;

    (*cmd_table[cmd].do_fun) ( ch, argument );

    //
    // after calling the function it might happen that the character/mob
    // doesn't exist anymore.
    //
    if (!IS_VALID(ch))
	return;

    if (IS_NPC(ch) || !STR_IS_SET(ch->strbit_act,PLR_QUITTING)) {
	if (!no_interpret_trigger) {
	    mp_interpret_postknown(ch,originalcommand);
	    op_interpret_postknown(ch,originalcommand);
	    rp_interpret_postknown(ch,originalcommand);
	}
    }

    tail_chain( );
    return;
}



//
// function to keep argument safe in all commands -- no static strings
//
void do_function(CHAR_DATA *ch, DO_FUN *do_fun, char *argument) {
    char *command_string;

    // copy the string
    command_string = str_dup(argument);

    // dispatch the command
    (*do_fun) (ch, command_string);

    // free the string
    free_string(command_string);
}

bool check_social( CHAR_DATA *ch, char *command, char *argument ) {
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim,*gch;
    SOCIAL_TYPE *social;
    bool found;
    char originalcommand[MSL];

    found  = FALSE;
    social=social_hash[(unsigned char)command[0]];
    while (social!=NULL) {
	if ( command[0] == social->name[0]
	&&   !str_prefix( command, social->name ) )
	{
	    found = TRUE;
	    break;
	}
	social=social->next;
    }

    if ( !found )
	return FALSE;

    if ( !IS_NPC(ch) && STR_IS_SET(ch->strbit_comm, COMM_NOEMOTE) ) {
	send_to_char( "You are anti-social!\n\r", ch );
	return TRUE;
    }

    switch ( ch->position )
    {
    case POS_DEAD:
	send_to_char( "Lie still; you are DEAD.\n\r", ch );
	return TRUE;

    case POS_INCAP:
    case POS_MORTAL:
	send_to_char( "You are hurt far too bad for that.\n\r", ch );
	return TRUE;

    case POS_STUNNED:
	send_to_char( "You are too stunned to do that.\n\r", ch );
	return TRUE;

    case POS_SLEEPING:
	/*
	 * I just know this is the path to a 12" 'if' statement.  :(
	 * But two players asked for it already!  -- Furey
	 */
	if ( !str_cmp( social->name, "snore" ) )
	    break;
	send_to_char( "In your dreams, or what?\n\r", ch );
	return TRUE;

    }

    // save it for triggers
    strcpy(originalcommand,social->name);
    if (argument[0]) {
	strcat(originalcommand," ");
	strcat(originalcommand,argument);
    }

    if (mp_interpret_preknown(ch,originalcommand) ||
	op_interpret_preknown(ch,originalcommand) ||
	rp_interpret_preknown(ch,originalcommand))
	return TRUE;

    one_argument( argument, arg );
    victim = NULL;

    switch (which_exact_keyword(arg,"all","group",NULL)) {
    case -1:	// social to nobody
	if (rp_presocial_trigger(ch,social->name,NULL)) return TRUE;

	act( social->others_no_arg, ch, NULL, NULL, TO_ROOM    );
	act( social->char_no_arg,   ch, NULL, NULL, TO_CHAR    );

	rp_social_trigger(ch,social->name,NULL);
	break;
    case 2:	// social to everybody in the group
	for ( gch = ch->in_room->people; gch != NULL; gch = gch->next_in_room )
	{
	    if ( is_same_group( gch, ch ) && ch!=gch && can_see(ch,gch)) {
		if (rp_presocial_trigger(ch,social->name,gch) ||
		    (IS_NPC(gch) && mp_presocial_trigger(gch,social->name,ch)))
		    return TRUE;

		act(social->others_found,ch,NULL,gch,TO_NOTVICT );
		act(social->char_found,  ch,NULL,gch,TO_CHAR    );
		act(social->vict_found,  ch,NULL,gch,TO_VICT    );

		rp_social_trigger(ch,social->name,gch);
		if (IS_NPC(gch)) mp_social_trigger(gch,social->name,ch);
	    }
	}
	break;
    case 1:	// social to everybody in the room
	if (!str_cmp(arg,"all")) {
	    for ( gch = ch->in_room->people; gch != NULL; gch = gch->next_in_room ) {
		if (ch!=gch && can_see(ch,gch)) {
		    if (rp_presocial_trigger(ch,social->name,ch) ||
			(IS_NPC(ch) && mp_presocial_trigger(ch,social->name,ch)))
			return TRUE;

		    act(social->others_found,ch,NULL,gch,TO_NOTVICT );
		    act(social->char_found,  ch,NULL,gch,TO_CHAR    );
		    act(social->vict_found,  ch,NULL,gch,TO_VICT    );

		    rp_social_trigger(ch,social->name,gch);
		    if (IS_NPC(gch)) mp_social_trigger(gch,social->name,ch);
		}
	    }
	    break;
	}
	// fallthrough!
    case 0:	// social to one person
	if ( ( victim = get_char_room( ch, arg ) ) == NULL ) {
	    if (social->char_not_found) {
		send_to_char( social->char_not_found, ch);
		send_to_char( "\n\r", ch);
	    } else
		send_to_char( "They aren't here.\n\r", ch );
	} else if ( victim == ch ) {
	    if (rp_presocial_trigger(ch,social->name,ch) ||
		(IS_NPC(ch) && mp_presocial_trigger(ch,social->name,ch)))
		return TRUE;

	    act( social->others_auto,   ch, NULL, victim, TO_ROOM    );
	    act( social->char_auto,     ch, NULL, victim, TO_CHAR    );

	    rp_social_trigger(ch,social->name,NULL);
	    if (IS_NPC(ch)) mp_social_trigger(ch,social->name,ch);
	} else {
	    if (rp_presocial_trigger(ch,social->name,victim) ||
		(IS_NPC(victim) && MOB_HAS_TRIGGER(victim,MTRIG_PRESOCIAL) && mp_presocial_trigger(victim,social->name,ch)))
		return TRUE;

	    act( social->others_found,  ch, NULL, victim, TO_NOTVICT );
	    act( social->char_found,    ch, NULL, victim, TO_CHAR    );
	    act( social->vict_found,    ch, NULL, victim, TO_VICT    );

	    rp_social_trigger(ch,social->name,victim);
	    if (IS_NPC(victim)) mp_social_trigger(victim,social->name,ch);

	    if ( !IS_NPC(ch) && IS_NPC(victim)
	    &&   !IS_AFFECTED(victim, EFF_CHARM)
	    &&   IS_AWAKE(victim)
	    &&   victim->desc == NULL) {
		switch ( number_bits( 4 ) )
		{
		case 0:
		    act("$n sniffs sadly at the way $N is treating $m.",
			victim, NULL, ch, TO_NOTVICT );
/*		    act("You sniff sadly at the way $N is treating you.",
			victim, NULL, ch, TO_CHAR );*/
		    act("$n sniffs sadly at the way you are treating $m.",
			victim, NULL, ch, TO_VICT    );
		    break;

		case 1: case 2: case 3: case 4:
		case 5: case 6: case 7: case 8:
		    act( social->others_found,victim, NULL, ch, TO_NOTVICT );
/*		    act( social->char_found,  victim, NULL, ch, TO_CHAR    );*/
		    act( social->vict_found,  victim, NULL, ch, TO_VICT    );
		    break;

		case 9: case 10: case 11: case 12:
		    act( "$n slaps $N.",  victim, NULL, ch, TO_NOTVICT );
/*		    act( "You slap $N.",  victim, NULL, ch, TO_CHAR    );*/
		    act( "$n slaps you.", victim, NULL, ch, TO_VICT    );
		    break;
		}
	    }
	}
	break;
    }

    if (IS_PC(ch) && !STR_IS_SET(ch->strbit_act,PLR_QUITTING)) {
	mp_interpret_postknown(ch,originalcommand);
	op_interpret_postknown(ch,originalcommand);
	rp_interpret_postknown(ch,originalcommand);
    }

    return TRUE;
}

void do_pueblo(CHAR_DATA *ch,char *argument) {
    send_to_char(argument,ch);
    send_to_char("\n\r",ch);
    interpret(ch,argument);
}

void set_command(CHAR_DATA *ch,char *argument) {
    char	command[MIL];
    char	option[MIL];
    bool	onoff,off;
    bool	logonoff,log;
    int		cmd;

    argument=one_argument(argument,command);
    argument=one_argument(argument,option);

    onoff=FALSE;
    off=FALSE;
    logonoff=FALSE;
    log=TRUE;
    switch (which_keyword(option,"on","off","log","nolog",NULL)) {
    case -1:
    case 1:	// on
	onoff=TRUE;
	off=FALSE;
	break;

    case 2:	// off
	onoff=TRUE;
	off=TRUE;
	break;

    case 3:	// log
	logonoff=TRUE;
	log=TRUE;
	break;

    case 4:	// nolog
	logonoff=TRUE;
	log=FALSE;
	break;

    default:
	send_to_char("Usage: set command <command> <on |  off | [no]log>\n\r",ch);
	return;
    }

    for ( cmd=0; cmd_table[cmd].name!=NULL; cmd++ ) {
	if (command[0]==cmd_table[cmd].name[0]
	&&  !str_prefix(command,cmd_table[cmd].name)) {

	    if (onoff) {
		cmd_table[cmd].disabled=off;
		sprintf_to_char(ch,"'%s' is now %s.\n\r",
		    cmd_table[cmd].name,
		    off?"disabled":"enabled");
		return;
	    }

	    if (logonoff) {
		if (log)
		    cmd_table[cmd].log=LOG_ALWAYS;
		else
		    cmd_table[cmd].log=LOG_NORMAL;
		sprintf_to_char(ch,"'%s' is now %s logged.\n\r",
		    cmd_table[cmd].name,
		    log?"always":"normal");
		return;
	    }
	}
    }
}



/*
 * Given a string like 14.foo, return 14 and 'foo'
 */
int number_argument( char *argument, char *arg )
{
    char *pdot;
    int number;

    for ( pdot = argument; *pdot != '\0'; pdot++ )
    {
	if ( *pdot == '.' )
	{
	    *pdot = '\0';
	    number = atoi( argument );
	    *pdot = '.';
	    strcpy( arg, pdot+1 );
	    return number;
	}
    }

    strcpy( arg, argument );
    return 1;
}

/*
 * Given a string like 14*foo, return 14 and 'foo'
*/
int mult_argument(char *argument, char *arg)
{
    char *pdot;
    int number;

    for ( pdot = argument; *pdot != '\0'; pdot++ )
    {
        if ( *pdot == '*' )
        {
            *pdot = '\0';
            number = atoi( argument );
            *pdot = '*';
            strcpy( arg, pdot+1 );
            return number;
        }
    }

    strcpy( arg, argument );
    return 1;
}



/*
 * Pick off one argument from a string and return the rest.
 * Understands quotes.
 */
char *one_argument( char *argument, char *arg_first )
{
    char cEnd;

    if (argument==NULL || argument[0]==0) {
	arg_first[0]=0;
	return "";
    }

    while ( isspace(*argument) )
	argument++;

    cEnd = ' ';
    if ( *argument == '\'' || *argument == '"' )
	cEnd = *argument++;

    while ( *argument != '\0' )
    {
	if ( *argument == cEnd )
	{
	    argument++;
	    break;
	}
	*arg_first = *argument;
	arg_first++;
	argument++;
    }
    *arg_first = '\0';

    while ( isspace(*argument) )
	argument++;

    return argument;
}


void do_commands( CHAR_DATA *ch, char *argument )
{
    char *Commands[512];		// at most 512 commands
    char buf[MAX_INPUT_LENGTH];
    static BUFFER *buffer=NULL;
    int cmd;
    int top = 0;
    int i, j;

    if (!buffer) {
	logf("[0] Sorting commands-table");
	buffer = new_buf();
	add_buf(buffer,"{cCommand List:{W\n\r");

	for ( cmd = 0; cmd_table[cmd].name!=NULL; cmd++ )
	    if (cmd_table[cmd].vnum==0
	    &&  cmd_table[cmd].show
	    &&  cmd_table[cmd].disabled==FALSE) {
		for ( i = 0; i < top; i++ )
		    if ( strcmp( Commands[i], cmd_table[cmd].name ) > 0 )
			break;
		for ( j = top; j > i; j-- )
		    Commands[j] = Commands[j-1];
		Commands[i] = cmd_table[cmd].name;
		top++;
		if (top==512) {
		    bugf("More than 512 commands, please fix do_commands()");
		    break;
		}
	    }
	for ( i = 0; i < top; i++ ) {
	    sprintf( buf, "{W%-19s", Commands[i]);
	    add_buf(buffer, buf);
	    if ( ( i + 1 )%4 == 0 ) {
		sprintf( buf, "{x\n\r" );
		add_buf(buffer, buf);
	    }
	}
	add_buf(buffer,"\r\n{x");
    }

    page_to_char( buf_string(buffer), ch );

/*  free_buf(buffer);	this buffer is never freed, sorry */

    return;
}


void do_wizhelp( CHAR_DATA *ch, char *argument )
{
    CHAR_DATA *victim;
    char buf[MAX_STRING_LENGTH];
    char marks[]=" x+*";
    int cmd;
    int col;

    if(IS_NPC(ch)) return;

    if (argument[0]) {
        victim=get_char_world(ch,argument);

        if (!victim) {
            sprintf_to_char(ch,"%s not found.\n\r",argument);
            return;
        }

        if (IS_NPC(victim)) {
            send_to_char("mobiles don't have immortal commands.\n\r",ch);
            return;
        }

        if (get_trust(ch)<get_trust(victim)) {
            send_to_char("Curiosity killed the cat.\n\r",ch);
            return;
        }

        sprintf_to_char(ch,"Wizhelp for %s:\n\r",victim->name);
    } else
        victim=ch;

    send_to_char("[x] granted by job  [+] grant  [*] job & grant\n\r",ch);

    col = 0;
    for ( cmd = 0; cmd_table[cmd].name!=NULL; cmd++ )
    {
        int allowed;
        if ( cmd_table[cmd].vnum != 0
        &&   cmd_table[cmd].show ) {
            if (is_command_allowed(victim,cmd_table[cmd].vnum))
                allowed=1;
            else
                allowed=0;
            if (STR_IS_SET(victim->pcdata->strbitgrand,cmd_table[cmd].vnum))
                allowed+=2;
            sprintf( buf, "{y[%c]{x %-12s",
                marks[allowed],
                cmd_table[cmd].name);
            send_to_char( buf, ch );
            if ( ++col % 5 == 0 )
                send_to_char( "\n\r", ch );
        }
    }

    if ( col % 5 != 0 )
        send_to_char( "\n\r", ch );
    return;
}

static struct {
  char *name;
  int vnums[70];
} groups[]={
  { "immortal", { 32,36,76,33,34,37,38,42,45,52,55,59,60,63,64,41,61,80,40,109,111,124,131 } },
  { "builder",  { 43,44,47,65,66,67,68,69,70,71,72,4,20,30,56,28,82,83,84,120,121,122,123,126,127,128,129,130,133,135,136,137,138 } },
  { "questbuilder", { 20,56,30,29,28,46,31,53,62,109 } },
  { "questor", { 20,26,35,49,57,48,54,50,53,58,62 } },
  { "police", { 22,23,24,25,31,10,17,8,51,19,5,6,27,125,134 } },
  { "stringer", { 14,53 } },
#ifdef I3
  { "i3", { 114,115,116,117,118,119 } },
#endif
  { "trustedcoder", { 20,28,43,65,69,71,72,120,123,126,129} },
  { "", { 0 } }
};

/* and now in table-format:
3 a 2 i b q q p s t  (i3, imc2admin, imc2, immortal, builder, questbuilder, questor, police, stringer, trustedcoder)
                     01 advance
                     02 dump
                     03 trust
        x            04 violate
              x      05 allow
              x      06 ban
                     07 deny
              x      08 disconnect
                     09 flag	(unused)
              x      10 freeze
                     11 permban	(unused)
                     12 protect
                     13 reboot
                x    14 set
                     15 shutdown
                     16 swearlist
              x      17 sockets
                     18 wizlock
              x      19 force
        x x x     x  20 load
                     21 newlock
              x      22 nochannels
              x      23 noemote
              x      24 noshout
              x      25 notell
            x        26 pecho
              x      27 pardon
        x x       x  28 purge
          x          29 restore
        x x          30 slay
          x          31 transfer
      x              32 at
      x              33 poofin
      x              34 poofout
            x        35 gecho
      x              36 goto
      x              37 holylight
      x              38 incognito
                     39 invis (is wizinv)
      x              40 log
      x              41 memory
      x              42 mwhere
        x         x  43 mpdump
        x            44 mpstat
      x              45 owhere
          x          46 peace
        x            47 mpwhere
            x        48 questpoints
            x        49 echo
            x        50 return
              x      51 snoop
      x              52 stat
          x x   x    53 string
            x        54 switch
      x              55 wizinvis
        x x          56 vnum
            x        57 aecho
            x        58 clone
      x              59 wiznet
      x              60 immtalk
      x              61 imotd
          x x        62 tagging
      x              63 smote
      x              64 prefix / smite
        x         x  65 aedit
        x            66 redit
        x            67 oedit
        x            68 medit
        x         x  69 mpedit
        x            70 asave
        x         x  71 alist
        x         x  72 hedit
                     73 grant
                     74 revoke
                     75 guild
      x              76 wizhelp
    x                77 rtell
                     78 iconnect (unused)
                     79 ikick    (unused)
      x              80 iimmtalk (unused)
                     81 ivis     (unused)
        x            82 hedit
        x            83 aexit
        x            84 rwhere
    x                85 rwho
    x                86 rwhois
    x                87 rquery
    x                88 rfinger
  x                  89 imc
    x                90 imclist
    x                91 rbeep
    x                92 istats
    x                93 rchannels
    x                94 rinfo
    x                95 rsockets
  x                  96 rconnect
  x                  97 rdisconnect
    x                98 rignore
    x                99 rchanset
    x               100 mailqueue
  x                 101 icommand
  x                 102 isetup
    x               103 ilist
    x               104 ichannels
    x               105 rping
    x               106 rreply
                    107 socialedit
                    108 copyover
  x                 109 propertylist
                    110 propertyedit
      x             111 olcedit
		    112
		    113
x                   114 i3
x                   115 i3who
x                   116 i3finger
x                   117 i3locate
x		    118 i3tell
x		    119 i3emote
        x         x 120 opdump
        x           121 opstat
        x           122 opwhere
        x         x 123 opedit
      x             124 audit
	      x     125 nocolor
        x         x 126 rpdump
        x           127 rpstat
        x           128 rpwhere
        x         x 129 rpedit
	x	    130 pwhere
      x             131 miniban	(unused)
                    132 job
        x         x 133 resetedit
              x     134 timedban (unused)
        x         x 135 apdump
        x           136 apstat
        x           137 apwhere
        x         x 138 apedit
*****/

void show_groups( CHAR_DATA *ch )
{
    int cmd,i;
    char buf[100];
    char strbit[50];

    for(cmd=0;groups[cmd].name[0]!='\0';cmd++)
    {
        sprintf(buf,"%-12s ( ",groups[cmd].name);
        send_to_char(buf,ch);

	STR_ZERO_BIT(strbit,sizeof(strbit));
        strbit[0]='\0';
        for(i=0;groups[cmd].vnums[i];i++) STR_SET_BIT(strbit,groups[cmd].vnums[i]);
        for(i=0;cmd_table[i].name!=NULL;i++)
        {
            if(cmd_table[i].vnum
            && cmd_table[i].show
            && STR_IS_SET(strbit,cmd_table[i].vnum))
            {
                sprintf(buf,"%s ",cmd_table[i].name);
                send_to_char( buf, ch );
            }
        }

        send_to_char(")\n\r",ch);
    }
}

void show_grants( CHAR_DATA *ch, CHAR_DATA *victim )
{
    char grant[MAX_INPUT_LENGTH];
    char buf[100];
    int cmd,i;
    bool found;

    memcpy(grant,victim->pcdata->strbitgrand,
	   sizeof(victim->pcdata->strbitgrand));

    send_to_char("Groups: ", ch);
    found=FALSE;
    /* First show the group this permision falls in. */
    for (cmd=0;groups[cmd].name[0]!='\0';cmd++) {
        for (i=0;groups[cmd].vnums[i];i++) {
            if (!STR_IS_SET(grant,groups[cmd].vnums[i]))
                break;
        }

        if (groups[cmd].vnums[i])
	    continue;

        /* Found a group, display it and remove its bits from out
           grant copy. */
        sprintf(buf,"%s ",groups[cmd].name);
        send_to_char(buf,ch);

        for (i=0;groups[cmd].vnums[i];i++)
            STR_REMOVE_BIT(grant,groups[cmd].vnums[i]);
        found=TRUE;
    }

    if (!found)
	send_to_char("none.", ch );
    send_to_char("\n\r", ch );

    /* All groups where displayed. Now show the unshowed commands. */

    send_to_char("Commands: ",ch );
    found=FALSE;

    for(cmd=0;cmd_table[cmd].name!=NULL;cmd++)
    {
        if(cmd_table[cmd].vnum
        && cmd_table[cmd].show
        && STR_IS_SET(grant,cmd_table[cmd].vnum))
        {
            sprintf(buf,"%s ",cmd_table[cmd].name);
            send_to_char( buf, ch );

            found=TRUE;
        }
    }

    if (!found)
	send_to_char("none.", ch );
    send_to_char("\n\r", ch );

    return;
}

void do_grant( CHAR_DATA *ch, char *argument )
{
    int cmd,i;
    char arg[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;

    argument = one_argument( argument, arg );
    argument = one_argument( argument, arg2 );

    if ( !str_cmp( arg, "groups" ) )
    {
        show_groups( ch );
        return;
    }

    if( arg[0]=='\0' || arg2[0]=='\0')
    {
        send_to_char( "Syntax: grant <char> <command/group>\n\r",ch);
        send_to_char( "        grant <char> job <name>\n\r",ch);
        send_to_char( "        grant <char> all\n\r",ch);
        send_to_char( "        grant <char> show\n\r",ch);
        send_to_char( "        grant groups\n\r", ch);
        return;
    }

    if ( ( victim = get_char_world( ch, arg ) ) == NULL || IS_NPC(victim))
    {
        send_to_char( "They aren't playing.\n\r", ch );
        return;
    }

    /* Search for the command you want to grant. */
    if ( !str_cmp( arg2, "all" ) )
    {
        for (cmd=0; cmd_table[cmd].name!=NULL; cmd++)
            if(cmd_table[cmd].vnum)
        {
            STR_SET_BIT(victim->pcdata->strbitgrand,cmd_table[cmd].vnum);
        }

        send_to_char("All commands granted.\n\r", ch );
        return;
    }

    if ( !str_cmp( arg2, "show" ) )
    {
        show_grants( ch, victim );
	job_show(ch,victim);
        return;
    }

    if ( !str_cmp( arg2, "job" ) ) {
	job_grant(ch,victim,argument);
	return;
    }

    /* Check on groups */
    for ( cmd=0; groups[cmd].name[0] != '\0'; cmd++ )
    {
        if ( arg2[0] == groups[cmd].name[0]
        && !str_cmp( arg2, groups[cmd].name ))
        {
            for(i=0;groups[cmd].vnums[i];i++)
            {
                STR_SET_BIT(victim->pcdata->strbitgrand,groups[cmd].vnums[i]);
            }

            send_to_char("Group granted.\n\r", ch );
            return;
        }
    }

    for ( cmd = 0; cmd_table[cmd].name != NULL; cmd++ )
    {
	if ( arg2[0] == cmd_table[cmd].name[0]
	&&   !str_cmp( arg2, cmd_table[cmd].name ))
	{
	    int vnum=cmd_table[cmd].vnum;

            if(vnum==0)
            {
                send_to_char("That command can not be granted.\n\r",ch);
                return;
            }

            if(STR_IS_SET(victim->pcdata->strbitgrand,vnum))
            {
                send_to_char("That command is already granted.\n\r", ch);
                return;
            }

            STR_SET_BIT(victim->pcdata->strbitgrand,vnum);

            send_to_char("Command granted.\n\r", ch);

            return;
	}
    }

    send_to_char("Command or group not found.\n\r", ch );
}

void do_revoke( CHAR_DATA *ch, char *argument )
{
    int cmd,i;
    char arg[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;

    argument = one_argument( argument, arg );
    argument = one_argument( argument, arg2 );

    if ( !str_cmp( arg, "groups" ) )
    {
        show_groups( ch );
        return;
    }

    if( arg[0]=='\0' || arg2[0]=='\0')
    {
        send_to_char( "Syntax: revoke <char> <command/group>\n\r",ch);
        send_to_char( "        revoke <char> job <jobname>\n\r",ch);
        send_to_char( "        revoke <char> all\n\r",ch);
        send_to_char( "        revoke <char> show\n\r",ch);
        send_to_char( "        revoke groups\n\r", ch);
        return;
    }

    if ( ( victim = get_char_world( ch, arg ) ) == NULL || IS_NPC(victim) )
    {
        send_to_char( "They aren't playing.\n\r", ch );
        return;
    }

    /* Search for the command you want to grant. */
    if ( !str_cmp( arg2, "all" ) )
    {
        for (cmd=0; cmd_table[cmd].name!=NULL; cmd++)
            if(cmd_table[cmd].vnum)
        {
            STR_REMOVE_BIT(victim->pcdata->strbitgrand,cmd_table[cmd].vnum);
        }

        send_to_char("All commands revoked.\n\r", ch );
        return;
    }

    if ( !str_cmp( arg2, "show" ) )
    {
        show_grants( ch, victim );
        return;
    }

    if ( !str_cmp( arg2, "job" ) ) {
	job_revoke(ch,victim,argument);
	return;
    }

    /* Check on groups */
    for ( cmd=0; groups[cmd].name[0] != '\0'; cmd++ )
    {
        if ( arg2[0] == groups[cmd].name[0]
        && !str_cmp( arg2, groups[cmd].name ))
        {
            for(i=0;groups[cmd].vnums[i];i++)
            {
                STR_REMOVE_BIT(victim->pcdata->strbitgrand,groups[cmd].vnums[i]);
            }

            send_to_char("Group revoked.\n\r", ch );
            return;
        }
    }

    for ( cmd = 0; cmd_table[cmd].name != NULL; cmd++ )
    {
	if ( arg2[0] == cmd_table[cmd].name[0]
	&&   !str_cmp( arg2, cmd_table[cmd].name ) )
	{
	    int vnum=cmd_table[cmd].vnum;

            if(vnum==0)
            {
                send_to_char("That command can not be revoked.\n\r",ch);
                return;
            }

            if(!STR_IS_SET(victim->pcdata->strbitgrand,vnum))
            {
                send_to_char("That command is already revoked.\n\r", ch);
                return;
            }

            STR_REMOVE_BIT(victim->pcdata->strbitgrand,vnum);

            send_to_char("Command revoked.\n\r",ch);

            return;
	}
    }

    send_to_char("Command not found.\n\r", ch);
}


int which_keyword(char *keyword, ...) {
    va_list ap;
    int i=1;
    char *arg;
    char k[MAX_STRING_LENGTH];

    if (!keyword || !keyword[0])
	return -1;

    va_start(ap,keyword);
    keyword=one_argument(keyword,k);
    do {
	arg=va_arg(ap, char *);
	if (arg) {
	    if (!str_prefix(k,arg)) {
		va_end(ap);
		return i;
	    }
	}
	i++;
    } while(arg);
    va_end(ap);
    return 0;
}

int which_exact_keyword(char *keyword, ...)
{
    va_list ap;
    int i=1;
    char *arg;
    char k[MAX_STRING_LENGTH];

    if (!keyword || !keyword[0])
	return -1;

    va_start(ap,keyword);
    keyword=one_argument(keyword,k);
    do {
	arg=va_arg(ap, char *);
	if (arg) {
	    if (!str_cmp(k,arg)) {
		va_end(ap);
		return i;
	    }
	}
	i++;
    } while(arg);
    va_end(ap);
    return 0;
}
