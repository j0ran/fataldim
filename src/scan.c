/* $Id: scan.c,v 1.13 2001/09/01 02:33:59 edwin Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

#include "merc.h"

char *const distance[4]=
{
"right here.", "%s from here.", "not far %s.", "off in the distance %s."
};

void scan_list           (ROOM_INDEX_DATA *scan_room, CHAR_DATA *ch,
                               int depth, int door);
void scan_char           (CHAR_DATA *victim, CHAR_DATA *ch,
                               int depth, int door);
void do_scan(CHAR_DATA *ch, char *argument)
{
   extern char *const dir_name[];
   char arg1[MAX_INPUT_LENGTH], buf[MAX_INPUT_LENGTH];
   ROOM_INDEX_DATA *scan_room;
   EXIT_DATA *pExit;
   int door, depth;

   argument = one_argument(argument, arg1);

   if (arg1[0] == '\0')
   {
      act("$n looks all around.", ch, NULL, NULL, TO_ROOM);
      send_to_char("Looking around you see:\n\r", ch);
                scan_list(ch->in_room, ch, 0, -1);

      for (door=0;door<DIR_MAX;door++)
      {
         if ((pExit = ch ->in_room->exit[door]) != NULL)
            scan_list(pExit->to_room, ch, 1, door);
      }
      return;
   }
   else
   {
      door = get_direction(arg1);
      if (door<0)
      {
        send_to_char("Which way do you want to scan?\n\r", ch);
        return;
      }
   }

   act("You peer intently $T.", ch, NULL, dir_name[door], TO_CHAR);
   act("$n peers intently $T.", ch, NULL, dir_name[door], TO_ROOM);
   sprintf(buf, "Looking %s you see:\n\r", dir_name[door]);

   scan_room = ch->in_room;

   for (depth = 1; depth < 4; depth++)
   {
      if (scan_room==NULL)
	break;
      if ((pExit = scan_room->exit[door]) != NULL &&
	 !IS_SET(scan_room->exit[door]->exit_info,EX_CLOSED)) {
         scan_room = pExit->to_room;
         scan_list(pExit->to_room, ch, depth, door);
      }
   }
   return;
}

void scan_list(ROOM_INDEX_DATA *scan_room, CHAR_DATA *ch, int depth,
               int door)
{
   CHAR_DATA *rch;

   if (scan_room == NULL) return;

   /* donno why, but the mud crashed on this once. problem was
      probably with the rev_dir-sutff */
   if ( door != -1 && scan_room->exit[rev_dir[door]] != NULL) {
	if (IS_SET(ch->in_room->exit[door]->exit_info,EX_CLOSED))
	    return;
	if (IS_SET(scan_room->exit[rev_dir[door]]->exit_info,EX_CLOSED))
	    return;
   }

   for (rch=scan_room->people; rch != NULL; rch=rch->next_in_room)
   {
      if (rch == ch) continue;
      if (!IS_NPC(rch) && rch->invis_level > get_trust(ch)) continue;
      if (can_see(ch, rch)) scan_char(rch, ch, depth, door);
   }
   return;
}

void scan_char(CHAR_DATA *victim, CHAR_DATA *ch, int depth, int door)
{
   extern char *const dir_name[];
   extern char *const distance[];
   char buf[MAX_INPUT_LENGTH], buf2[MAX_INPUT_LENGTH];

   buf[0] = '\0';

   strcat(buf, PERS(victim, ch));
   strcat(buf, ", ");
   sprintf(buf2, distance[depth], dir_name[door]);
   strcat(buf, buf2);
   strcat(buf, "\n\r");

   send_to_char(buf, ch);
   return;
}
