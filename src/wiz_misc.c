//
// $Id: wiz_misc.c,v 1.33 2007/07/15 15:18:09 jodocus Exp $ */
//
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

/*
 * 19980101 EG	Modified mstat() to show the number of number of trainings
 * 19980103 EG	Modified the stat()-functions to support color.
 * 19980103 EG	Modified mstat() to show the snoop status
 */

#include "merc.h"
#include "interp.h"
#include "olc.h"
#include "db.h"

void restore_char(CHAR_DATA *ch);

void do_guild( CHAR_DATA *ch, char *argument )
{
    char arg1[MAX_INPUT_LENGTH],arg2[MAX_INPUT_LENGTH];
    char arg3[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    CLAN_INFO *clan;
    RANK_INFO *rank;

    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );
    argument = one_argument( argument, arg3 );

    if ( arg1[0] == '\0' || arg2[0] == '\0' )
    {
        send_to_char( "Syntax: guild <char> <rank> <clan name>\n\r",ch);
        return;
    }
    if ( ( victim = get_char_world( ch, arg1 ) ) == NULL )
    {
        send_to_char( "They aren't playing.\n\r", ch );
        return;
    }
    if (IS_NPC(victim)) {
       send_to_char( "Only players can be a member of a clan.\n\r", ch);
       return;
    }

    rank=NULL;
    if ((rank=get_rank_by_name(arg2))==NULL) {
	send_to_char("Invalid rank, please see the help of 'rank' for the ranks available.\n\r",ch);
	return;
    }

    if (!str_prefix(arg3,"none"))
    {
	send_to_char("They are now clanless.\n\r",ch);
	send_to_char("You are now a member of no clan!\n\r",victim);
	clan_removemember(victim->clan);
	victim->clan=NULL;
	mortalcouncil_update(victim);
	return;
    }

    if ((clan = get_clan_by_name(arg3)) == NULL) {
	send_to_char("No such clan exists.\n\r",ch);
	return;
    }

    if (victim->clan!=NULL) {
	clan_removemember(victim->clan);
	victim->clan=NULL;
    }
    clan_addmember(victim,clan,rank);

    mortalcouncil_update(victim);

    if (clan->independent) {
	act("You are now a $T.",victim,NULL,clan->name,TO_CHAR);
	act("$n is now a $T.",victim,NULL,clan->name,TO_ROOM);
    } else {
	act("You are now a $t of the clan $T.",victim,rank->rank_name,clan->name,TO_CHAR);
	act("$n is now a $t of the clan $T.",victim,rank->rank_name,clan->name,TO_ROOM);
    }
}

/* equips a character */
void do_outfit ( CHAR_DATA *ch, char *argument )
{
    OBJ_DATA *obj;
    int i,sn,vnum;
    bool equiped=FALSE,warning=FALSE;

    if (ch->level > 5 || IS_NPC(ch))
    {
	send_to_char("Find it yourself!\n\r",ch);
	return;
    }

    if ( ( obj = get_eq_char( ch, WEAR_LIGHT ) ) == NULL ) {
	if (!get_obj_carry_by_vnum(ch,OBJ_VNUM_SCHOOL_BANNER)) {
	    obj = create_object( get_obj_index(OBJ_VNUM_SCHOOL_BANNER), 0 );
	    obj->cost = 0;
	    obj_to_char( obj, ch );
	    equip_char( ch, obj, WEAR_LIGHT );
	    equiped=TRUE;
	    op_load_trigger(obj,NULL);
	} else {
	    send_to_char("Use your own light!\n\r",ch);
	    warning=TRUE;
        }
    }

    if ( ( obj = get_eq_char( ch, WEAR_BODY ) ) == NULL ) {
	if (!get_obj_carry_by_vnum(ch,OBJ_VNUM_SCHOOL_VEST)) {
	    obj = create_object( get_obj_index(OBJ_VNUM_SCHOOL_VEST), 0 );
	    obj->cost = 0;
	    obj_to_char( obj, ch );
	    equip_char( ch, obj, WEAR_BODY );
	    equiped=TRUE;
	    op_load_trigger(obj,NULL);
	} else {
	    send_to_char("Use your own vest!\n\r",ch);
	    warning=TRUE;
        }
    }

    /* do the weapon thing */
    if ((obj = get_eq_char(ch,WEAR_WIELD)) == NULL)
    {
    	sn = 0;
    	vnum = OBJ_VNUM_SCHOOL_SWORD; /* just in case! */

    	for (i = 0; weapon_table[i].name != NULL; i++)
    	{
	    if (get_current_skill(ch,sn) <
		get_current_skill(ch,*weapon_table[i].gsn))
	    {
	    	sn = *weapon_table[i].gsn;
	    	vnum = weapon_table[i].vnum;
	    }
    	}

	if (!get_obj_carry_by_vnum(ch,vnum)) {
	    obj = create_object(get_obj_index(vnum),0);
	    obj_to_char(obj,ch);
	    equip_char(ch,obj,WEAR_WIELD);
	    equiped=TRUE;
	    op_load_trigger(obj,NULL);
	} else {
	    send_to_char("Use your own weapon!\n\r",ch);
	    warning=TRUE;
        }
    }

    if (((obj = get_eq_char(ch,WEAR_WIELD)) == NULL
    ||   !IS_WEAPON_STAT(obj,WEAPON_TWO_HANDS))
    &&  (obj = get_eq_char( ch, WEAR_SHIELD ) ) == NULL ) {
        if (!get_obj_carry_by_vnum(ch,OBJ_VNUM_SCHOOL_SHIELD)) {
	    obj = create_object( get_obj_index(OBJ_VNUM_SCHOOL_SHIELD), 0 );
	    obj->cost = 0;
	    obj_to_char( obj, ch );
	    equip_char( ch, obj, WEAR_SHIELD );
	    equiped=TRUE;
	    op_load_trigger(obj,NULL);
	} else {
	    send_to_char("Use your own shield!\n\r",ch);
	    warning=TRUE;
        }
    }

    if (equiped)
	send_to_char("You have been equipped by Fatal.\n\r",ch);
    else if (!warning)
	send_to_char("You were already equipped.\n\r",ch);
}


void do_smote(CHAR_DATA *ch, char *argument )
{
    CHAR_DATA *vch;
    char *letter,*name;
    char last[MAX_INPUT_LENGTH], temp[MAX_STRING_LENGTH];
    int matches = 0;

    if ( !IS_NPC(ch) && STR_IS_SET(ch->strbit_comm, COMM_NOEMOTE) )
    {
        send_to_char( "You can't show your emotions.\n\r", ch );
        return;
    }

    if ( argument[0] == '\0' )
    {
        send_to_char( "Emote what?\n\r", ch );
        return;
    }

    if (strstr(argument,ch->name) == NULL)
    {
	send_to_char("You must include your name in an smote.\n\r",ch);
	return;
    }

    send_to_char(argument,ch);
    send_to_char("\n\r",ch);

    for (vch = ch->in_room->people; vch != NULL; vch = vch->next_in_room)
    {
        if (vch->desc == NULL || vch == ch)
            continue;

        if ((letter = strstr(argument,vch->name)) == NULL)
        {
	    send_to_char(argument,vch);
	    send_to_char("\n\r",vch);
            continue;
        }

        strcpy(temp,argument);
        temp[strlen(argument) - strlen(letter)] = '\0';
        last[0] = '\0';
        name = vch->name;

        for (; *letter != '\0'; letter++)
        {
            if (*letter == '\'' && matches == strlen(vch->name))
            {
                strcat(temp,"r");
                continue;
            }

            if (*letter == 's' && matches == strlen(vch->name))
            {
                matches = 0;
                continue;
            }

            if (matches == strlen(vch->name))
            {
                matches = 0;
            }

            if (*letter == *name)
            {
                matches++;
                name++;
                if (matches == strlen(vch->name))
                {
                    strcat(temp,"you");
                    last[0] = '\0';
                    name = vch->name;
                    continue;
                }
                strncat(last,letter,1);
                continue;
            }

            matches = 0;
            strcat(temp,last);
            strncat(temp,letter,1);
            last[0] = '\0';
            name = vch->name;
        }

	send_to_char(temp,vch);
	send_to_char("\n\r",vch);
    }

    return;
}

void do_bamfin( CHAR_DATA *ch, char *argument )
{
    if ( !IS_NPC(ch) )
    {
	smash_tilde( argument );

	if (argument[0] == '\0')
	{
	    sprintf_to_char(ch,"Your poofin is %s\n\r",ch->pcdata->bamfin);
	    return;
	}

	if ( strstr(argument,ch->name) == NULL)
	{
	    send_to_char("You must include your name.\n\r",ch);
	    return;
	}

	free_string( ch->pcdata->bamfin );
	ch->pcdata->bamfin = str_dup( argument );

	sprintf_to_char(ch,"Your poofin is now %s\n\r",ch->pcdata->bamfin);
    }
    return;
}



void do_bamfout( CHAR_DATA *ch, char *argument )
{
    if ( !IS_NPC(ch) )
    {
        smash_tilde( argument );

        if (argument[0] == '\0')
        {
	    sprintf_to_char(ch,"Your poofout is %s\n\r",ch->pcdata->bamfout);
            return;
        }

        if ( strstr(argument,ch->name) == NULL)
        {
            send_to_char("You must include your name.\n\r",ch);
            return;
        }

        free_string( ch->pcdata->bamfout );
        ch->pcdata->bamfout = str_dup( argument );
	sprintf_to_char(ch,"Your poofout is now %s\n\r",ch->pcdata->bamfout);
    }
    return;
}




void do_disconnect( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    DESCRIPTOR_DATA *d;
    CHAR_DATA *victim;

    one_argument( argument, arg );
    if ( arg[0]==0 ) {
	send_to_char( "Disconnect whom?\n\r", ch );
	return;
    }

    if (is_number(arg))
    {
	int desc;

	desc = atoi(arg);
    	for ( d = descriptor_list; d != NULL; d = d->next ) {
            if ( d->descriptor == desc ) {
            	sprintf_to_char(ch,"You have disconnected socket %d.\n\r",desc);
            	close_socket( d );
            	return;
            }
	}
    }

    if ( ( victim = get_char_world( ch, arg ) ) == NULL ) {
	send_to_char( "They aren't here.\n\r", ch );
	return;
    }

    if ( IS_NPC(victim) ) {
	act("$N is a mob, $E stays!",ch,NULL,victim,TO_CHAR);
	return;
    }

    if ( victim->desc == NULL ) {
	act( "$N doesn't have a descriptor.", ch, NULL, victim, TO_CHAR );
	return;
    }
    close_socket( victim->desc );
    sprintf_to_char(ch,"You have disconnected %s.\n\r",victim->name);

    return;
}






void do_gecho( CHAR_DATA *ch, char *argument )
{
    CHAR_DATA *player;

    if ( argument[0] == '\0' ) {
	send_to_char( "Global echo what?\n\r", ch );
	return;
    }

    for ( player = player_list; player!=NULL; player = player->next_player ) {
	if (get_trust(player) >= get_trust(ch))
	    send_to_char( "global>",player);
	send_to_char( argument, player );
	send_to_char( "{x\n\r",   player );
    }

    return;
}



void do_recho( CHAR_DATA *ch, char *argument )
{
    CHAR_DATA *victim;

    if ( argument[0] == '\0' ) {
	send_to_char( "Room echo what?\n\r", ch );
	return;
    }

    for (victim=ch->in_room->people;victim!=NULL;victim=victim->next_in_room) {
	if (IS_NPC(victim)) continue;

	if (get_trust(victim) >= get_trust(ch)
	&&  STR_IS_SET(victim->strbit_act, PLR_HOLYLIGHT))
	    send_to_char( "room> ",victim);

	send_to_char( argument, victim );
	send_to_char( "{x\n\r", victim );
    }

    return;
}

void do_aecho(CHAR_DATA *ch, char *argument)
{
    CHAR_DATA *player;

    if (argument[0] == '\0') {
	send_to_char("Area echo what?\n\r",ch);
	return;
    }

    for ( player = player_list; player!=NULL; player = player->next_player ) {
	if (player->in_room != NULL && ch->in_room != NULL
	&&  player->in_room->area == ch->in_room->area) {
	    if (get_trust(player) >= get_trust(ch))
		send_to_char("area> ",player);
	    send_to_char(argument,player);
	    send_to_char("{x\n\r",player);
	}
    }
}

void do_pecho( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;

    argument = one_argument(argument, arg);

    if ( argument[0] == '\0' || arg[0] == '\0' ) {
	send_to_char("Personal echo what?\n\r", ch);
	return;
    }

    if  ( (victim = get_char_world(ch, arg) ) == NULL )
    {
	send_to_char("Target not found.\n\r",ch);
	return;
    }

    if (get_trust(victim) >= get_trust(ch) && get_trust(ch) != MAX_LEVEL)
        send_to_char( "personal> ",victim);

    send_to_char(argument,victim);
    send_to_char("{x\n\r",victim);
    send_to_char( "personal> ",ch);
    send_to_char(argument,ch);
    send_to_char("{x\n\r",ch);
}


void do_transfer( CHAR_DATA *ch, char *argument ) {
    char arg1[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    ROOM_INDEX_DATA *location;
    CHAR_DATA *victim;
    CHAR_DATA *player;

    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );

    if ( arg1[0] == '\0' )
    {
	send_to_char( "Transfer whom (and where)?\n\r", ch );
	return;
    }

    if ( !str_cmp( arg1, "all" ) )
    {
	for ( player=player_list; player!=NULL; player=player->next_player ) {
	    if ( player != ch
	    &&   player->in_room != NULL
	    &&   can_see( ch, player ) ) {
		char buf[MAX_STRING_LENGTH];
		sprintf( buf, "%s %s", player->name, arg2 );
		do_function(ch, &do_transfer, buf );
	    }
	}
	return;
    }

    /*
     * Thanks to Grodyn for the optional location parameter.
     */
    if ( arg2[0] == '\0' ) {
	location = ch->in_room;
    } else {
	if ( ( location = find_location( ch, arg2 ) ) == NULL ) {
	    send_to_char( "No such location.\n\r", ch );
	    return;
	}

	if ( !is_room_owner(ch,location) &&
	     room_is_private( location ) &&
	     !is_allowed_in_room(ch,location) &&
	     get_trust(ch) < MAX_LEVEL) {
	    send_to_char( "That room is private right now.\n\r", ch );
	    return;
	}
    }

    if ( ( victim = get_char_world( ch, arg1 ) ) == NULL )
    {
	send_to_char( "They aren't here.\n\r", ch );
	return;
    }

    if ( victim->in_room == NULL )
    {
	send_to_char( "They are in limbo.\n\r", ch );
	return;
    }

    if ( victim->fighting != NULL )
	stop_fighting( victim, TRUE );
    act( "$n disappears in a mushroom cloud.", victim, NULL, NULL, TO_ROOM );
    char_from_room( victim );
    char_to_room( victim, location );
    act( "$n arrives from a puff of smoke.", victim, NULL, NULL, TO_ROOM );
    if ( ch != victim )
	act( "$n has transferred you.", ch, NULL, victim, TO_VICT );
    look_room(victim,victim->in_room,TRUE);
    send_to_char( "Ok.\n\r", ch );
}



void do_at( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    ROOM_INDEX_DATA *location;
    ROOM_INDEX_DATA *original;
    OBJ_DATA *on;
    CHAR_DATA *wch;

    argument = one_argument( argument, arg );

    if ( arg[0] == '\0' || argument[0] == '\0' )
    {
	send_to_char( "At where what?\n\r", ch );
	return;
    }

    if ( ( location = find_location( ch, arg ) ) == NULL )
    {
	send_to_char( "No such location.\n\r", ch );
	return;
    }

    if (!is_room_owner(ch,location) &&
	room_is_private( location ) &&
	!is_allowed_in_room(ch,location) &&
	get_trust(ch) < MAX_LEVEL)
    {
	send_to_char( "That room is private right now.\n\r", ch );
	return;
    }

    original = ch->in_room;
    on = ch->on;
    char_from_room( ch );
    char_to_room( ch, location );
    interpret( ch, argument );

    /*
     * See if 'ch' still exists before continuing!
     * Handles 'at XXXX quit' case.
     */
    for ( wch = char_list; wch != NULL; wch = wch->next )
    {
	if ( wch == ch )
	{
	    char_from_room( ch );
	    char_to_room( ch, original );
	    ch->on = on;
	    break;
	}
    }

    return;
}



void do_goto( CHAR_DATA *ch, char *argument )
{
    ROOM_INDEX_DATA *location;
    CHAR_DATA *rch;
    int count = 0;

    if ( argument[0] == '\0' )
    {
	send_to_char( "Goto where?\n\r", ch );
	return;
    }

    if ( ( location = find_location( ch, argument ) ) == NULL )
    {
	send_to_char( "No such location.\n\r", ch );
	return;
    }

    if (ch->in_room==location) {
	send_to_char("You're already there.\n\r",ch);
	return;
    }

    count = 0;
    for ( rch = location->people; rch != NULL; rch = rch->next_in_room )
        count++;

    if (!is_room_owner(ch,location) &&
	room_is_private(location) &&
	!is_allowed_in_room(ch,location) &&
	(count > 1 || get_trust(ch) < MAX_LEVEL))
    {
	send_to_char( "That room is private right now.\n\r", ch );
	if (is_command_allowed(ch,exact_command_to_vnum("violate"))) {
	    send_to_char("** Violating! **\n\r",ch);
	    do_violate(ch,argument);
	}
	return;
    }

    if ( ch->fighting != NULL )
	stop_fighting( ch, TRUE );

    if (ch->position<POS_STANDING)
	do_function(ch,&do_stand,"");

    for (rch = ch->in_room->people; rch != NULL; rch = rch->next_in_room)
    {
	if (get_trust(rch) >= ch->invis_level)
	{
	    if (ch->pcdata != NULL && ch->pcdata->bamfout[0] != '\0')
		act("$t",ch,ch->pcdata->bamfout,rch,TO_VICT);
	    else
		act("$n leaves in a swirling mist.",ch,NULL,rch,TO_VICT);
	}
    }

    char_from_room( ch );
    char_to_room( ch, location );


    for (rch = ch->in_room->people; rch != NULL; rch = rch->next_in_room)
    {
        if (ch!=rch && get_trust(rch) >= ch->invis_level)
        {
            if (ch->pcdata != NULL && ch->pcdata->bamfin[0] != '\0')
                act("$t",ch,ch->pcdata->bamfin,rch,TO_VICT);
            else
                act("$n appears in a swirling mist.",ch,NULL,rch,TO_VICT);
	    if( IS_AFFECTED2( ch, EFF_CANTRIP ) )
		show_cantrip( rch, ch , TO_VICT );
	    show_ool_cantrip(rch,ch,TO_VICT);
	    if (ch->invis_level)
		sprintf_to_char(rch,"Warning! %s is wizi at level {r%d{x.\n\r",NAME(ch),ch->invis_level);
        }
    }

    look_room(ch,ch->in_room,TRUE);
    return;
}

void do_violate( CHAR_DATA *ch, char *argument )
{
    ROOM_INDEX_DATA *location;
    CHAR_DATA *rch;
    EXIT_DATA *pexit;
    int direction;

    if ( argument[0] == '\0' )
    {
        send_to_char( "Goto where?\n\r", ch );
        return;
    }

    direction=get_direction(argument);
    if (direction!=-1) {
	if ((pexit = ch->in_room->exit[direction] )==NULL) {
	    send_to_char("There is no exit in that direction.\n\r",ch);
	    return;
	}
	location=pexit->to_room;
    } else if ( ( location = find_location( ch, argument ) ) == NULL ) {
        send_to_char( "No such location.\n\r", ch );
        return;
    }

    if ( ch->fighting != NULL )
        stop_fighting( ch, TRUE );

    for (rch = ch->in_room->people; rch != NULL; rch = rch->next_in_room)
    {
        if (get_trust(rch) >= ch->invis_level)
        {
            if (ch->pcdata != NULL && ch->pcdata->bamfout[0] != '\0')
                act("$t",ch,ch->pcdata->bamfout,rch,TO_VICT);
            else
                act("$n leaves in a swirling mist.",ch,NULL,rch,TO_VICT);
        }
    }

    char_from_room( ch );
    char_to_room( ch, location );


    for (rch = ch->in_room->people; rch != NULL; rch = rch->next_in_room)
    {
        if (rch!=ch && get_trust(rch) >= ch->invis_level)
        {
            if (ch->pcdata != NULL && ch->pcdata->bamfin[0] != '\0')
                act("$t",ch,ch->pcdata->bamfin,rch,TO_VICT);
            else
                act("$n appears in a swirling mist.",ch,NULL,rch,TO_VICT);
	    if (ch->invis_level)
		sprintf_to_char(rch,"Warning! %s is wizi at level {r%d{x.\n\r",NAME(ch),ch->invis_level);
        }
    }

    look_room(ch,ch->in_room,TRUE);
    return;
}


///////////////////////////////////////////////////////////////////////////////

void reboot_mud(CHAR_DATA *ch,char *argument,int type) {
    char buf[MAX_STRING_LENGTH];

    if (argument[0]==0) {
	if (mud_data.reboot_type==REBOOT_NONE) {
	    send_to_char("There is nothing in progress.\n\r",ch);
	    return;
	}
	sprintf(buf,"}r%s cancelled{x\n\r",reboot_state());
	send_to_all_char(buf);
	mud_data.reboot_type=REBOOT_NONE;
	mud_data.reboot_timer=0;
	mud_data.reboot_pulse=0;
	strcpy(mud_data.reboot_by,ch->name);
	return;
    }

    if (type==REBOOT_REBOOT)
	send_to_char("Please consider the use of 'copyover'.\n\r",ch);

    if (!str_cmp(argument,"now")) {
	mud_data.reboot_type=type;
	mud_data.reboot_pulse=1;
	mud_data.reboot_timer=1;
	strcpy(mud_data.reboot_by,ch->name);
	// saving is done by reboot_update()

	sprintf(buf,"}r%s by %s.{x\n\r",reboot_state(),ch->name);
	send_to_all_char(buf);

	return;
    }

    if (is_number(argument)) {
        int number=atoi(argument);

        if (number<2) {
            send_to_char("Timer must be more than one minute.\n\r",ch);
            return;
        }

        mud_data.reboot_timer=number*2;
        mud_data.reboot_pulse=PULSE_REBOOT;
	mud_data.reboot_type=type;
	strcpy(mud_data.reboot_by,ch->name);

        sprintf(buf,"}r%s in %d minutes.{x\n\r",reboot_state(),number);
        send_to_all_char(buf);

        return;
    }

    send_to_char("Wrong argument, usage: now or a delay in minutes\n\r",ch);
}


void do_reboo( CHAR_DATA *ch, char *argument ) {
    send_to_char( "If you want to REBOOT, spell it out.\n\r", ch );
}

void do_reboot( CHAR_DATA *ch, char *argument ) {
    reboot_mud(ch,argument,REBOOT_REBOOT);
}

void do_shutdow( CHAR_DATA *ch, char *argument ) {
    send_to_char( "If you want to SHUTDOWN, spell it out.\n\r", ch );
}

void do_shutdown( CHAR_DATA *ch, char *argument ) {
    reboot_mud(ch,argument,REBOOT_SHUTDOWN);
}

void do_copyove( CHAR_DATA *ch, char *argument ) {
    send_to_char( "If you want to COPYOVER, spell it out.\n\r", ch );
}

void do_copyover( CHAR_DATA *ch, char *argument ) {
    reboot_mud(ch,argument,REBOOT_COPYOVER);
}

///////////////////////////////////////////////////////////////////////////////

void do_protect( CHAR_DATA *ch, char *argument)
{
    CHAR_DATA *victim;

    if (argument[0] == '\0')
    {
	send_to_char("Protect whom from snooping?\n\r",ch);
	return;
    }

    if ((victim = get_char_world(ch,argument)) == NULL)
    {
	send_to_char("You can't find them.\n\r",ch);
	return;
    }

    if (STR_IS_SET(victim->strbit_comm,COMM_SNOOP_PROOF))
    {
	act_new("$N is no longer snoop-proof.",ch,NULL,victim,TO_CHAR,POS_DEAD);
	send_to_char("Your snoop-proofing was just removed.\n\r",victim);
	STR_REMOVE_BIT(victim->strbit_comm,COMM_SNOOP_PROOF);
    }
    else
    {
	act_new("$N is now snoop-proof.",ch,NULL,victim,TO_CHAR,POS_DEAD);
	send_to_char("You are now immune to snooping.\n\r",victim);
	STR_SET_BIT(victim->strbit_comm,COMM_SNOOP_PROOF);
    }
}



void do_snoop( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    DESCRIPTOR_DATA *d;

    one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Snoop whom?\n\r", ch );
	return;
    }

    if ( ( victim = get_char_world( ch, arg ) ) == NULL )
    {
	send_to_char( "They aren't here.\n\r", ch );
	return;
    }

    if ( victim->desc == NULL )
    {
	send_to_char( "No descriptor to snoop.\n\r", ch );
	return;
    }

    if ( victim == ch ) {
	send_to_char( "Cancelling all snoops.\n\r", ch );
	wiznet(WIZ_SNOOPS,get_trust(ch),ch,NULL,"$N stops being such a snoop.");
	for ( d = descriptor_list; d != NULL; d = d->next ) {
	    if ( d->snoop_by == ch->desc )
		d->snoop_by = NULL;
	}
	return;
    }

    if ( victim->desc->snoop_by != NULL ) {
	send_to_char( "Busy already.\n\r", ch );
	return;
    }
/*
// this check doesn't make sence
// a snooped char can walk into a private room and will still be snooped.

    if (!is_room_owner(ch,victim->in_room) && ch->in_room != victim->in_room
    &&  room_is_private(victim->in_room) && !IS_TRUSTED(ch,LEVEL_COUNCIL))
    {
        send_to_char("That character is in a private room.\n\r",ch);
        return;
    }
*/
    if ( get_trust( victim ) >= get_trust( ch )
    ||   STR_IS_SET(victim->strbit_comm,COMM_SNOOP_PROOF))
    {
	send_to_char( "You failed.\n\r", ch );
	return;
    }

    victim->desc->snoop_by = ch->desc;
    wiznet(WIZ_SNOOPS,get_trust(ch),ch,NULL,
	"$N starts snooping on %s.",victim->name);
    send_to_char( "Ok.\n\r", ch );
    return;
}



void do_switch( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;

    one_argument( argument, arg );

    if ( arg[0] == '\0' ) {
	send_to_char( "Switch into whom?\n\r", ch );
	return;
    }

    if ( ch->desc == NULL )
	return;

    if ( ch->desc->original != NULL ) {
	send_to_char( "You are already switched.\n\r", ch );
	return;
    }

    if ( ( victim = get_char_world( ch, arg ) ) == NULL ) {
	send_to_char( "They aren't here.\n\r", ch );
	return;
    }

    if ( victim == ch ) {
	send_to_char( "Ok.\n\r", ch );
	return;
    }

    if (!IS_NPC(victim)) {
	send_to_char("You can only switch into mobiles.\n\r",ch);
	return;
    }

    if (!is_room_owner(ch,victim->in_room) && ch->in_room != victim->in_room
    &&  room_is_private(victim->in_room) && !IS_TRUSTED(ch,LEVEL_COUNCIL))
    {
	send_to_char("That character is in a private room.\n\r",ch);
	return;
    }

    if ( victim->desc != NULL ) {
	send_to_char( "Character in use.\n\r", ch );
	return;
    }

    wiznet(WIZ_SWITCHES,get_trust(ch),ch,NULL,
	"SWITCH: $N switches into %s",victim->short_descr);

    ch->desc->character = victim;
    ch->desc->original  = ch;
    ch->pcdata->switched_into = victim;
    victim->desc        = ch->desc;
    ch->desc            = NULL;
    STR_SET_BIT(ch->strbit_act,PLR_SWITCHED);
    /* change communications to match */
    if (ch->prompt != NULL)
        victim->prompt = str_dup(ch->prompt);
    STR_REMOVE_BIT(victim->strbit_comm,COMM_AFK);
    STR_REMOVE_BIT(victim->strbit_comm,COMM_AFK_BY_IDLE);
    send_to_char( "Ok.\n\r", victim );
    return;
}



void do_return( CHAR_DATA *ch, char *argument ) {
    if ( ch->desc == NULL )
	return;

    if ( ch->desc->original == NULL ) {
	send_to_char( "You aren't switched.\n\r", ch );
	return;
    }

    send_to_char("You return to your original body. ",ch);
    if (buf_string(ch->desc->original->pcdata->buffer)[0] != '\0')
	send_to_char("Type 'replay' to see tells.\n\r",ch);
    else
	send_to_char("No tells received.\n\r",ch);

    if (ch->prompt != NULL) {
	free_string(ch->prompt);
	ch->prompt = NULL;
    }

    wiznet(WIZ_SWITCHES,get_trust(ch),ch->desc->original,NULL,
	"SWITCH: $N returns from %s.",ch->short_descr);

    STR_REMOVE_BIT(ch->desc->original->strbit_act,PLR_SWITCHED);
    ch->desc->original->pcdata->switched_into = NULL;;

    ch->desc->original->pcdata->switched_into	= NULL;
    ch->desc->character       = ch->desc->original;
    ch->desc->original        = NULL;
    ch->desc->character->desc = ch->desc;
    ch->desc                  = NULL;
    return;
}

/* for clone, to insure that cloning goes many levels deep */
void recursive_clone(CHAR_DATA *ch, OBJ_DATA *obj, OBJ_DATA *clone)
{
    OBJ_DATA *c_obj, *t_obj;


    for (c_obj = obj->contains; c_obj != NULL; c_obj = c_obj->next_content)
    {
	t_obj = create_object(c_obj->pIndexData,0);
	clone_object(c_obj,t_obj);
	obj_to_obj(t_obj,clone);
	op_load_trigger(t_obj,ch);
	recursive_clone(ch,c_obj,t_obj);
    }
}

/* command that is similar to load */
void do_clone(CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    char *rest;
    int amount=1;
    CHAR_DATA *mob=NULL;
    OBJ_DATA  *obj=NULL;

    rest = one_argument(argument,arg);
    if (is_number(arg)) {
	amount=strtol(arg,NULL,10);
	if (amount<=0 || amount>50) {
	    send_to_char("You can only clone between 1 and 50 items at a time.\n\r",ch);
	    return;
	}
	rest = one_argument(rest,arg);
    }

    switch(which_keyword(arg,"object","mobile","character",NULL)) {
	case -1: /* no keyword at all */
	    send_to_char("Clone what?\n\r",ch);
	    return;
	case  0: /* keyword not in list */
	    mob = get_char_room(ch,arg);
	    obj = get_obj_here(ch,arg);
	    break;
	case  1:
	    obj = get_obj_here(ch,rest);
	    break;
	case  2:
	case  3:
	    mob = get_char_room(ch,rest);
	    break;
    }

    if (mob == NULL && obj == NULL) {
	send_to_char("You don't see that here.\n\r",ch);
	return;
    }

    /* clone an object */
    if (obj != NULL) {
	OBJ_DATA *clone;
	int i;

	for (i=0;i<amount;i++) {
	    clone = create_object(obj->pIndexData,0);
	    clone_object(obj,clone);
	    if (obj->carried_by != NULL)
		obj_to_char(clone,ch);
	    else
		obj_to_room(clone,ch->in_room);
	    recursive_clone(ch,obj,clone);
	    op_load_trigger(clone,ch);
	}

	if (amount>1) {
	    act("$n has created $p $T times.",ch,clone,itoa(amount),TO_ROOM);
	    act("You clone $p $T times.",ch,clone,itoa(amount),TO_CHAR);
	    wiznet(WIZ_LOAD,get_trust(ch),ch,clone,"$N clones $p %d times.",amount);
	} else {
	    act("$n has created $p.",ch,clone,NULL,TO_ROOM);
	    act("You clone $p.",ch,clone,NULL,TO_CHAR);
	    wiznet(WIZ_LOAD,get_trust(ch),ch,clone,"$N clones $p.");
	}
	return;
    } else if (mob != NULL) {
	CHAR_DATA *clone;
	OBJ_DATA *new_obj;

	if (IS_PC(mob))
	{
	    send_to_char("You can only clone mobiles.\n\r",ch);
	    return;
	}

	for (;amount>0;amount--) {
	    clone = create_mobile(mob->pIndexData);
	    clone_mobile(mob,clone);

	    for (obj = mob->carrying; obj != NULL; obj = obj->next_content) {
		new_obj = create_object(obj->pIndexData,0);
		clone_object(obj,new_obj);
		recursive_clone(ch,obj,new_obj);
		obj_to_char(new_obj,clone);
		op_load_trigger(new_obj,ch);
		new_obj->wear_loc = obj->wear_loc;
	    }
	    char_to_room(clone,ch->in_room);
	    mp_load_trigger(clone,ch);
	}
        act("$n has created $N.",ch,NULL,clone,TO_ROOM);
        act("You clone $N.",ch,NULL,clone,TO_CHAR);
	wiznet(WIZ_LOAD,get_trust(ch),ch,NULL,
	    "$N clones %s.",clone->short_descr);
        return;
    }
}

/* RT to replace the two load commands */

void do_load(CHAR_DATA *ch, char *argument )
{
   char arg[MAX_INPUT_LENGTH];

    argument = one_argument(argument,arg);
  switch(which_exact_keyword(arg,"mob","obj","char",NULL))
    {
     case -1:
     case  0:
	send_to_char("Syntax:\n\r",ch);
	send_to_char("  load mob <vnum>\n\r",ch);
	send_to_char("  load obj <vnum> <level>\n\r",ch);
	if(get_trust(ch)>=MAX_LEVEL)
	    send_to_char("  load char <player>\n\r", ch);
	return;
     case  1:
	do_function(ch, &do_mload, argument);
	return;
     case  2:
	do_function(ch, &do_oload, argument);
	return;
     case  3:
	do_function(ch, &do_cload, argument);
	return;
    }
}


void do_mload( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    MOB_INDEX_DATA *pMobIndex;
    CHAR_DATA *victim;

    one_argument( argument, arg );

    if ( arg[0] == '\0' || !is_number(arg) )
    {
	send_to_char( "Syntax: load mob <vnum>.\n\r", ch );
	return;
    }

    if ( ( pMobIndex = get_mob_index( atoi( arg ) ) ) == NULL )
    {
	send_to_char( "No mob has that vnum.\n\r", ch );
	return;
    }

    victim = create_mobile( pMobIndex );
    victim->zone=ch->in_room->area;
    char_to_room( victim, ch->in_room );
    act( "$n has created $N!", ch, NULL, victim, TO_ROOM );
    act( "You have created $N!", ch, NULL, victim, TO_CHAR );
    wiznet(WIZ_LOAD,get_trust(ch),ch,NULL,"$N loads %s.",victim->short_descr);
    mp_load_trigger(victim,ch);
    return;
}

void do_cload( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    bool fOld;
    CHAR_DATA *victim;
    DESCRIPTOR_DATA d;

    if (get_trust(ch)<LEVEL_COUNCIL) {
	send_to_char("You are not allowed to load players.\n\r" , ch);
	return;
    }

    one_argument( argument, arg );

    if ( arg[0] == '\0')
    {
	send_to_char( "Syntax: load char <playername>.\n\r", ch );
	return;
    }

    arg[0] = UPPER(arg[0]);

    /* Only load player if he is not already in the game. */
    for (victim=char_list;victim!=NULL;victim=victim->next)
	if ( !IS_NPC(ch) && !str_cmp( arg, victim->name ) ) break;
    if(victim!=NULL)
    {
	send_to_char("Player is already loaded.\n\r",ch );
	return;
    }

#ifdef THREADED_DNS
    bzero(d.Host,sizeof(d.Host));
    fOld=load_char_obj(&d,arg);
#else
    d.Host=str_dup("");
    fOld=load_char_obj(&d,arg);
    free_string(d.Host);
#endif
    /* Detach descriptor structure */
    victim=d.character;
    victim->desc=NULL;

    if(!fOld)
    {
	free_char(victim);
	send_to_char("Player does not exist.\n\r", ch );
	return;
    }

    // add to player-list
    victim->next_player=player_list;
    player_list=victim;
    update_wiznet(TRUE,victim->strbit_wiznet);

    // add to charlist
    victim->next = char_list;
    char_list	 = victim;

    victim->was_in_room=victim->in_room;
    char_to_room( victim, ch->in_room );
    if (victim->pet)
	char_to_room( victim->pet, ch->in_room );

    act( "$n has summoned $N who seems lifeless!", ch, NULL, victim, TO_ROOM );
    act( "You have summoned $N who seems lifeless!", ch, NULL, victim, TO_CHAR );
    wiznet(WIZ_LOAD,get_trust(ch),ch,NULL,
	"$N loads character %s.",victim->name);
    return;
}

void do_oload( CHAR_DATA *ch, char *argument )
{
    char arg1[MAX_INPUT_LENGTH] ,arg2[MAX_INPUT_LENGTH];
    OBJ_INDEX_DATA *pObjIndex;
    OBJ_DATA *obj;
    int level;

    argument = one_argument( argument, arg1 );
    one_argument( argument, arg2 );

    if ( arg1[0] == '\0' || !is_number(arg1))
    {
	send_to_char( "Syntax: load obj <vnum> <level>.\n\r", ch );
	return;
    }

    level = get_trust(ch); /* default */

    if ( arg2[0] != '\0')  /* load with a level */
    {
	if (!is_number(arg2))
        {
	  send_to_char( "Syntax: oload <vnum> <level>.\n\r", ch );
	  return;
	}
        level = atoi(arg2);
        if (level < 0 || level > get_trust(ch))
	{
	  send_to_char( "Level must be be between 0 and your level.\n\r",ch);
  	  return;
	}
    }

    if ( ( pObjIndex = get_obj_index( atoi( arg1 ) ) ) == NULL )
    {
	send_to_char( "No object has that vnum.\n\r", ch );
	return;
    }

    obj = create_object( pObjIndex, level );
    if ( CAN_WEAR(obj, ITEM_TAKE) )
	obj_to_char( obj, ch );
    else
	obj_to_room( obj, ch->in_room );
    op_load_trigger(obj,ch);

    //
    // Add some comments to it. You never can be too paranoid *sigh*
    //
    sprintf(arg1,"Loaded by %s",ch->name);
    SetObjectProperty(obj,PROPERTY_STRING,"comment",arg1);

    act( "$n has created $p!", ch, obj, NULL, TO_ROOM );
    act( "You have created $p!", ch, obj, NULL, TO_CHAR );
    wiznet(WIZ_LOAD,get_trust(ch),ch,obj,"$N loads $p.");
    return;
}



void do_purge( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    OBJ_DATA *obj;
    DESCRIPTOR_DATA *d;

    one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
	/* 'purge' */
	CHAR_DATA *vnext;
	OBJ_DATA  *obj_next;

	for ( victim = ch->in_room->people; victim != NULL; victim = vnext )
	{
	    vnext = victim->next_in_room;
	    if ( IS_NPC(victim) && !STR_IS_SET(victim->strbit_act,ACT_NOPURGE)
	    &&   victim != ch /* safety precaution */ )
		extract_char( victim, TRUE );
	}

	for ( obj = ch->in_room->contents; obj != NULL; obj = obj_next )
	{
	    obj_next = obj->next_content;
	    if (!IS_OBJ_STAT(obj,ITEM_NOPURGE))
	      extract_obj( obj );
	}

	act( "$n purges the room!", ch, NULL, NULL, TO_ROOM);
	send_to_char( "Ok.\n\r", ch );
	return;
    }

    if ( ( victim = get_char_room( ch, arg ) ) != NULL ) {
	if ( !IS_NPC(victim) ) {

	    if (ch == victim) {
	      send_to_char("Ho ho ho.\n\r",ch);
	      return;
	    }

	    if (get_trust(ch) <= get_trust(victim)) {
	      send_to_char("Maybe that wasn't a good idea...\n\r",ch);
	      sprintf_to_char(victim,"%s tried to purge you!\n\r",ch->name);
	      return;
	    }

	    if ( STR_IS_SET(ch->strbit_act,PLR_SWITCHED) ) {
		sprintf_to_char(ch, "But %s switched!\n\r",NAME(victim));
		return;
	    }

	    act("$n disintegrates $N.",ch,0,victim,TO_NOTVICT);

	    if (victim->level > 1)
		save_char_obj( victim );
	    d = victim->desc;
	    extract_char( victim, TRUE );
	    if ( d != NULL )
	      close_socket( d );

	    return;
	}

	act( "$n purges $N.", ch, NULL, victim, TO_NOTVICT );
	extract_char( victim, TRUE );
	return;
    }

    if ((obj=get_obj_here(ch,arg))!=NULL) {
	
	if (obj->in_room!=ch->in_room) {
	    send_to_char("That object is not in this room.\r\n",ch);
	    return;
	}
	act( "$n purges $P.", ch, NULL, obj, TO_ROOM );
	extract_obj(obj);
	return;

    }

    send_to_char("No char or object by that name here.\n\r",ch);
}



void do_advance( CHAR_DATA *ch, char *argument )
{
    char arg1[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    int level;
    int iLevel;

    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );

    if ( arg1[0] == '\0' || arg2[0] == '\0' || !is_number( arg2 ) )
    {
	send_to_char( "Syntax: advance <char> <level>.\n\r", ch );
	return;
    }

    if ( ( victim = get_char_world( ch, arg1 ) ) == NULL )
    {
	send_to_char( "That player is not here.\n\r", ch);
	return;
    }

    if ( IS_NPC(victim) )
    {
	send_to_char( "Not on NPC's.\n\r", ch );
	return;
    }

    if ( ( level = atoi( arg2 ) ) < 1 || level > MAX_LEVEL )
    {
	sprintf_to_char(ch,"Level must be 1 to %d.\n\r", MAX_LEVEL );
	return;
    }

    if ( level > get_trust( ch ) )
    {
	send_to_char( "Limited to your trust level.\n\r", ch );
	return;
    }

    /*
     * Lower level:
     *   Reset to level 1.
     *   Then raise again.
     *   Currently, an imp can lower another imp.
     *   -- Swiftest
     */
    if ( level <= victim->level )
    {
        int temp_prac;

	send_to_char( "Lowering a player's level!\n\r", ch );
	send_to_char( "**** OOOOHHHHHHHHHH  NNNNOOOO ****\n\r", victim );
	temp_prac = victim->practice;
	victim->level    = 1;
	victim->exp      = exp_per_level(victim,victim->pcdata->points);
	victim->max_hit  = 10;
	victim->max_mana = 100;
	victim->max_move = 100;
	victim->practice = 0;
	victim->hit      = victim->max_hit;
	victim->mana     = victim->max_mana;
	victim->move     = victim->max_move;
	advance_level( victim, TRUE );
	victim->practice = temp_prac;
    }
    else
    {
	send_to_char( "Raising a player's level!\n\r", ch );
	send_to_char( "**** OOOOHHHHHHHHHH  YYYYEEEESSS ****\n\r", victim );
    }

    for ( iLevel = victim->level ; iLevel < level; iLevel++ )
    {
	victim->level += 1;
	advance_level( victim,TRUE);
    }
    sprintf_to_char(victim,"You are now level %d.\n\r",victim->level);
    victim->exp   = exp_per_level(victim,victim->pcdata->points)
		  * UMAX( 1, victim->level );
    victim->trust = 0;
    save_char_obj(victim);
    return;
}



void do_trust( CHAR_DATA *ch, char *argument )
{
    char buf[MAX_STRING_LENGTH];
    char arg1[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    int level;

    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );

    if ( arg1[0] == '\0' || arg2[0] == '\0' || !is_number( arg2 ) )
    {
	send_to_char( "Syntax: trust <char> <level>.\n\r", ch );
	return;
    }

    if ( ( victim = get_char_world( ch, arg1 ) ) == NULL )
    {
	send_to_char( "That player is not here.\n\r", ch);
	return;
    }

    if ( ( level = atoi( arg2 ) ) < 0 || level > MAX_LEVEL )
    {
	sprintf(buf, "Level must be 0 (reset) or 1 to %d.\n\r", MAX_LEVEL );
	send_to_char(buf,ch);
	return;
    }

    if ( level > get_trust( ch ) )
    {
	send_to_char( "Limited to your trust.\n\r", ch );
	return;
    }

    victim->trust = level;
    return;
}



void do_restore( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    CHAR_DATA *vch;

    one_argument( argument, arg );
    if (arg[0] == '\0' || !str_cmp(arg,"room")) {
	/* cure room */

        for (vch = ch->in_room->people; vch != NULL; vch = vch->next_in_room)
        {
            restore_char( vch );
            act("$n has restored you.",ch,NULL,vch,TO_VICT);
        }

	wiznet(WIZ_RESTORE,get_trust(ch),ch,NULL,
	    "$N restored room %d.",ch->in_room->vnum);

        send_to_char("Room restored.\n\r",ch);
        return;

    }

    if ( get_trust(ch) >=  MAX_LEVEL - 1 && !str_cmp(arg,"all")) {
	/* cure all */

        for (victim=player_list; victim!=NULL; victim=victim->next_player) {

	    if (victim == NULL || IS_NPC(victim))
		continue;

            restore_char( victim );
	    if (victim->in_room != NULL)
                act("$n has restored you.",ch,NULL,victim,TO_VICT);
        }
	send_to_char("All active players restored.\n\r",ch);
	return;
    }

    if ( ( victim = get_char_world( ch, arg ) ) == NULL )
    {
	send_to_char( "They aren't here.\n\r", ch );
	return;
    }

    restore_char( victim );
    act( "$n has restored you.", ch, NULL, victim, TO_VICT );
    wiznet(WIZ_RESTORE,get_trust(ch),ch,NULL,
	"$N restored character %s.",
	IS_NPC(victim) ? victim->short_descr : victim->name);
    send_to_char( "Ok.\n\r", ch );
    return;
}

void restore_char(CHAR_DATA *ch)
{
    effect_strip(ch, gsn_plague);
    effect_strip(ch, gsn_poison);
    effect_strip(ch, gsn_blindness);
    effect_strip(ch, gsn_sleep);
    effect_strip(ch, gsn_curse);
    effect_strip(ch, gsn_weaken);
    ch->hit  = ch->max_hit;
    ch->mana = ch->max_mana;
    ch->move = ch->max_move;
    update_pos( ch );
}

void do_peace( CHAR_DATA *ch, char *argument )
{
    CHAR_DATA *rch;

    for ( rch = ch->in_room->people; rch != NULL; rch = rch->next_in_room )
    {
	if ( rch->fighting != NULL )
	    stop_fighting( rch, TRUE );
	if (IS_NPC(rch) && STR_IS_SET(rch->strbit_act,ACT_AGGRESSIVE))
	    STR_REMOVE_BIT(rch->strbit_act,ACT_AGGRESSIVE);
    }

    send_to_char( "Ok.\n\r", ch );
    return;
}


void do_wizlock( CHAR_DATA *ch, char *argument ) {
    send_to_char("Use {gset mud wizlock{x to wizlock the game.\n\r",ch);
    return;
}

void do_newlock( CHAR_DATA *ch, char *argument ) {
    send_to_char("Use {gset mud newlock{x to newlock the game.\n\r",ch);
    return;
}




void do_sockets( CHAR_DATA *ch, char *argument )
{
    char buf[2 * MAX_STRING_LENGTH];
    char buf2[MAX_STRING_LENGTH];
    char arg[MAX_INPUT_LENGTH];
    DESCRIPTOR_DATA *d;
    int count,empty;

    count	= 0;
    empty	= 0;
    buf[0]	= '\0';

#ifndef NO_INET4
    sprintf_to_char(ch,"[ %3d listen               ] IPv4 - mud\n\r",
	mud_data.control4_mud);
#ifdef HAS_HTTP
    sprintf_to_char(ch,"[ %3d listen               ] IPv4 - http\n\r",
	mud_data.control4_http);
#endif
#ifdef HAS_POP3
    sprintf_to_char(ch,"[ %3d listen               ] IPv4 - pop3\n\r",
	mud_data.control4_pop3);
#endif
#endif
#ifdef INET6
    sprintf_to_char(ch,"[ %3d listen               ] IPv6 - mud\n\r",
	mud_data.control6_mud);
#ifdef HAS_HTTP
    sprintf_to_char(ch,"[ %3d listen               ] IPv6 - http\n\r",
	mud_data.control6_http);
#endif
#ifdef HAS_POP3
    sprintf_to_char(ch,"[ %3d listen               ] IPv6 - pop3\n\r",
	mud_data.control6_pop3);
#endif
#endif

    one_argument(argument,arg);
    for ( d = descriptor_list; d != NULL; d = d->next ) {
	if (d->character==NULL) {
	    sprintf( buf+strlen(buf) ,"[ %3d %-20s ] nobody yet@%s\n\r",
		d->descriptor,
		table_find_value(d->connected,socket_table),
		dns_gethostname(d)
		);
	    empty++;
	} else if ( d->character != NULL && can_see( ch, d->character )
		&& (arg[0] == '\0' || is_name(arg,d->character->name)
			   || (d->original && is_name(arg,d->original->name))))
	{
	    sprintf( buf + strlen(buf), "[ %3d %-20s ] %s@%s\n\r",
		d->descriptor,
		table_find_value(d->connected,socket_table),
		d->original  ? d->original->name  :
		d->character ? d->character->name : "(none)",
		dns_gethostname(d)
		);

	    if (d->connected==CON_PLAYING)
		count++;
	    else
		empty++;
	}
    }
    if (count == 0) {
	send_to_char("No one by that name is connected.\n\r",ch);
	return;
    }

    sprintf( buf2, "%d user%s, %d socket%s\n\r",
	count, count == 1 ? "" : "s",
	empty+count, empty+count == 1 ? "" : "s" );
    strcat(buf,buf2);
    page_to_char( buf, ch );
    return;
}



/*
 * Thanks to Grodyn for pointing out bugs in this function.
 */
void do_force( CHAR_DATA *ch, char *argument ) {
    char	buf[MSL];
    char	arg[MIL];
    char	arg2[MIL];
    CHAR_DATA *	player,*player_next,*victim;

    argument = one_argument( argument, arg );

    if ( arg[0]==0 || argument[0]==0) {
	send_to_char( "Force whom to do what?\n\r", ch );
	return;
    }

    one_argument(argument,arg2);

    if ((!str_cmp(arg2,"delete")
    && get_trust(ch)<LEVEL_COUNCIL) || !str_prefix(arg2,"mob")) {
	send_to_char("That will NOT be done.\n\r",ch);
	return;
    }

    sprintf( buf, "$n forces you to '%s'.", argument );

    switch (which_keyword(arg,"all","players","gods",NULL)) {
    case 1:	// all
	if (get_trust(ch) < LEVEL_COUNCIL) {
	    send_to_char("Not at your level!\n\r",ch);
	    return;
	}

	for ( player = player_list; player != NULL; player = player_next ) {
	    player_next = player->next_player;

	    if (get_trust( player ) < get_trust( ch ) ) {
		act( buf, ch, NULL, player, TO_VICT );
		interpret( player, argument );
	    }
	}
	send_to_char( "Ok.\n\r", ch );
	return;

    case 2:	// players
        if (get_trust(ch) < MAX_LEVEL - 2) {
            send_to_char("Not at your level!\n\r",ch);
            return;
        }

        for ( player = player_list; player != NULL; player = player_next ) {
	    player_next = player->next_player;

            if ( get_trust( player ) < get_trust( ch ) &&
		 player->level < LEVEL_HERO) {
                act( buf, ch, NULL, player, TO_VICT );
                interpret( player, argument );
            }
        }
	send_to_char( "Ok.\n\r", ch );
	return;

    case 3:	// gods
        if (get_trust(ch) < MAX_LEVEL - 2) {
            send_to_char("Not at your level!\n\r",ch);
            return;
        }

        for ( player = player_list; player != NULL; player = player_next ) {
            player_next = player->next_player;

            if ( get_trust( player ) < get_trust( ch ) &&
		 player->level >= LEVEL_HERO) {
                act( buf, ch, NULL, player, TO_VICT );
                interpret( player, argument );
            }
        }
	send_to_char( "Ok.\n\r", ch );
	return;

    default:

	if ( ( victim = get_char_world( ch, arg ) ) == NULL ) {
	    send_to_char( "They aren't here.\n\r", ch );
	    return;
	}

	if ( victim == ch ) {
	    send_to_char( "Aye aye, right away!\n\r", ch );
	    return;
	}

    	if (!is_room_owner(ch,victim->in_room)
	&&  ch->in_room != victim->in_room
        &&  room_is_private(victim->in_room) && !IS_TRUSTED(ch,LEVEL_COUNCIL)) {
            send_to_char("That character is in a private room.\n\r",ch);
            return;
        }

	if ( get_trust( victim ) >= get_trust( ch ) ) {
	    send_to_char( "Do it yourself!\n\r", ch );
	    return;
	}

	if ( !IS_NPC(victim) && get_trust(ch) < MAX_LEVEL -3) {
	    send_to_char("Not at your level!\n\r",ch);
	    return;
	}

	act( buf, ch, NULL, victim, TO_VICT );
	interpret( victim, argument );
	send_to_char( "Ok.\n\r", ch );
	return;
    }
}



/*
 * New routines by Dionysos.
 */
void do_invis( CHAR_DATA *ch, char *argument )
{
    int level;
    char arg[MAX_STRING_LENGTH];

    /* RT code for taking a level argument */
    one_argument( argument, arg );

    if ( arg[0] == '\0' )
    /* take the default path */

      if ( ch->invis_level)
      {
	  ch->invis_level = 0;
	  act( "$n slowly fades into existence.", ch, NULL, NULL, TO_ROOM );
	  send_to_char( "You slowly fade back into existence.\n\r", ch );
      }
      else
      {
	  act( "$n slowly fades into thin air.", ch, NULL, NULL, TO_ROOM );
	  send_to_char( "You slowly vanish into thin air.\n\r", ch );
	  ch->invis_level = ch->level;
      }
    else
    /* do the level thing */
    {
      level = atoi(arg);
      if (level < 2 || level > get_trust(ch))
      {
	send_to_char("Invis level must be between 2 and your trust-level.\n\r",ch);
        return;
      }
      else
      {
	  ch->reply = NULL;
          act( "$n slowly fades into thin air.", ch, NULL, NULL, TO_ROOM );
          send_to_char( "You slowly vanish into thin air.\n\r", ch );
          ch->invis_level = level;
      }
    }

    return;
}


void do_incognito( CHAR_DATA *ch, char *argument )
{
    int level;
    char arg[MAX_STRING_LENGTH];

    /* RT code for taking a level argument */
    one_argument( argument, arg );

    if ( arg[0] == '\0' )
    /* take the default path */

      if ( ch->incog_level)
      {
          ch->incog_level = 0;
          act( "$n is no longer cloaked.", ch, NULL, NULL, TO_ROOM );
          send_to_char( "You are no longer cloaked.\n\r", ch );
      }
      else
      {
          ch->incog_level = ch->level;
          act( "$n cloaks $s presence.", ch, NULL, NULL, TO_ROOM );
          send_to_char( "You cloak your presence.\n\r", ch );
      }
    else
    /* do the level thing */
    {
      level = atoi(arg);
      if (level < 2 || level > get_trust(ch))
      {
        send_to_char("Incog level must be between 2 and your trust-level.\n\r",ch);
        return;
      }
      else
      {
          ch->reply = NULL;
          ch->incog_level = level;
          act( "$n cloaks $s presence.", ch, NULL, NULL, TO_ROOM );
          send_to_char( "You cloak your presence.\n\r", ch );
      }
    }

    return;
}



void do_holylight( CHAR_DATA *ch, char *argument ) {
    if ( IS_NPC(ch) )
	return;

    switch(which_keyword(argument,"on","off","better","normal",NULL)) {
	case -1:
		STR_TOGGLE_BIT(ch->strbit_act, PLR_HOLYLIGHT);
		break;
	case  0:
		send_to_char("Syntax: holylight [on|off|normal|better].\n\r",ch);
		return;
	case  1:
		STR_SET_BIT(ch->strbit_act, PLR_HOLYLIGHT);
		STR_REMOVE_BIT(ch->strbit_act, PLR_HOLYLIGHT_PLUSPLUS);
		break;
	case  2:
		STR_REMOVE_BIT(ch->strbit_act, PLR_HOLYLIGHT);
		STR_REMOVE_BIT(ch->strbit_act, PLR_HOLYLIGHT_PLUSPLUS);
		break;
	case  3:
		STR_SET_BIT(ch->strbit_act, PLR_HOLYLIGHT);
		STR_SET_BIT(ch->strbit_act, PLR_HOLYLIGHT_PLUSPLUS);
		break;
	case  4:
		STR_SET_BIT(ch->strbit_act, PLR_HOLYLIGHT);
		STR_REMOVE_BIT(ch->strbit_act, PLR_HOLYLIGHT_PLUSPLUS);
		break;
    }
    sprintf_to_char(ch, "Holy light mode %s.\n\r",
	!STR_IS_SET(ch->strbit_act, PLR_HOLYLIGHT) ? "off" :
	 STR_IS_SET(ch->strbit_act, PLR_HOLYLIGHT_PLUSPLUS) ? "better" : "on");
}

/* prefix command: it will put the string typed on each line typed */

void do_prefi (CHAR_DATA *ch, char *argument)
{
    send_to_char("You cannot abbreviate the prefix command.\r\n",ch);
    return;
}

void do_prefix (CHAR_DATA *ch, char *argument)
{
    if (argument[0] == '\0')
    {
	if (ch->prefix[0] == '\0')
	{
	    send_to_char("You have no prefix to clear.\r\n",ch);
	    return;
	}

	send_to_char("Prefix removed.\r\n",ch);
	free_string(ch->prefix);
	ch->prefix = str_dup("");
	return;
    }

    if (ch->prefix[0] != '\0')
    {
     sprintf_to_char(ch,"Prefix changed to %s.\r\n",argument);
	free_string(ch->prefix);
    }
    else
     sprintf_to_char(ch,"Prefix set to %s.\r\n",argument);
    ch->prefix = str_dup(argument);
}

/* Quest command */
/* Syntax: questpoints <player> <points> */
void do_questpoints (CHAR_DATA *ch, char *argument)
{
    char arg[MAX_INPUT_LENGTH];
    char buf[MAX_STRING_LENGTH];
    CHAR_DATA *victim;
    int points;

    if(argument[0]=='\0')
    {
	send_to_char("Syntax: questpoints <player> <number>\n\r",ch);
	return;
    }

    argument=one_argument(argument,arg);
    victim=get_char_world(ch,arg);
    if(!victim)
    {
        send_to_char("They aren't there.\n\r", ch);
        return;
    }
    if(IS_NPC(victim))
    {
        send_to_char("You can only give quest points to players.\n\r",ch);
        return;
    }

    argument=one_argument(argument,arg);
    if(!is_number(arg))
    {
        send_to_char("You can only add numbers.\n\r", ch);
        return;
    }
    points=atoi(arg);
    if(points<=0)
    {
        send_to_char("No points added.\n\r", ch);
        return;
    }
    if(points>15)
    {
        send_to_char("You may not give more than 15 quest points at a time.\n\r",ch);
        return;
    }
    victim->pcdata->questpoints+=points;
    sprintf(buf,"$N now has %d questpoints (%d added).",
	victim->pcdata->questpoints,points);
    act(buf,ch,NULL,victim,TO_CHAR);
    sprintf(buf,"$n has given you %d questpoints.",points);
    act(buf,ch,NULL,victim,TO_VICT);

    return;
}


/* Immpower! Smite for immortals - if successfully smote, a victim will
   be knocked over, their weapon cast from their hands and their health
   halved. 3 different smite messages according to imm alignment. Based
   on Faramir's smite from Xania */

void do_smit ( CHAR_DATA *ch, char *argument) {
    send_to_char("If you wish to smite someone, then SPELL it out!\n\r",ch);
    return;
}

void do_smite ( CHAR_DATA *ch, char *argument) {
    char      * smitestring;
    CHAR_DATA * victim;
    OBJ_DATA  * obj;

    if ( argument[0] == '\0') {
	send_to_char( "Upon whom do you wish to unleash your power?\n\r", ch);
	return;
    }

    if ( ( victim = get_char_room( ch, argument ) ) == NULL ) {
	send_to_char( "They aren't here.\n\r", ch );
	return;
    }                   /* Not (visibly) present in room! */

    if ( IS_NPC ( ch ) ) {
	send_to_char ("You must take your true form before unleashing your power.\n\r" , ch);
	return;
    }                   /* done whilst switched? No way Jose */

    if (get_trust ( victim ) > get_trust ( ch ))  {
	send_to_char ("You failed.\n\rUmmm...beware of retaliation!\n\r" , ch);
	act( "$n attempted to smite $N!",  ch, NULL, victim, TO_NOTVICT);
	act( "$n attempted to smite you!", ch, NULL, victim, TO_VICT);
	return;
    }
    /*  Immortals cannot smite those with a
	greater trust than themselves */

    smitestring="__NeutralSmite";

    if (ch->alignment >= 300)  {
	smitestring="__GoodSmite" ;
    }                   /* Good Gods */
    if (ch->alignment <= -300)  {
	smitestring="__BadSmite" ;
    }
/* Establish what message will actually
   be sent when the smite takes place.
   Evil Gods have their own evil method of
   incurring their wrath, and so neutral and
   good Gods =)  smitemessage default of 0
   for neutral.
   ok, now the appropriate smite
   string has been determined we must
   send it to the victim, and tell
   the smiter and others in the room. */

    show_help(victim,smitestring);
    act ( "{RThe wrath of the Gods has fallen upon you!\n\rYou are blown helplessly from your feet and are stunned!{w", ch, NULL, victim, TO_VICT );

    if ( ( obj = get_eq_char( victim, WEAR_WIELD ) ) == NULL ) {
	act ("{R$n has been cast down by the power of $N!{w",
	     victim, NULL, ch, TO_NOTVICT);
    } else {
	act ("{R$n has been cast down by the power of $N!\n\rTheir weapon is sent flying!{w", victim, NULL, ch, TO_NOTVICT);
    }

    /* tells others that the victim has
       been disarmed, but not the victim :) */

    sprintf_to_char( ch,
	"You {W>>> {YSMITE{W <<<{w %s with all of your Godly powers!\n\r",
	( victim == ch ) ? "yourself" : victim->name);

    victim->hit=UMAX(victim->hit/2,1);
    victim->move=UMAX(victim->move/2,1);
    victim->mana=UMAX(victim->mana/2,1);
    if (IS_PC(victim))
	victim->pcdata->condition[COND_ADRENALINE]=48;

    victim->position = POS_RESTING;
    /* Knock them into resting
       and disarm them regardless of whether
       they have talon or a noremove weapon */

    if ( ( obj = get_eq_char( victim, WEAR_WIELD ) ) == NULL ) {
	return;
    } else {                       /* can't be disarmed if no weapon */
	obj_from_char( obj );
	obj_to_room( obj, victim->in_room );
	if (IS_NPC(victim) && victim->wait == 0 && can_see_obj(victim,obj))
	    get_obj(victim,obj,NULL);
	else
	    if (obj->in_room->sector_type==SECT_AIR)
		object_drop_air(obj);
	    else if (obj->in_room->sector_type==SECT_WATER_NOSWIM ||
		     obj->in_room->sector_type==SECT_WATER_SWIM ||
		     obj->in_room->sector_type==SECT_WATER_BELOW )
		object_drop_water(obj);
	    else if (number_percent()<10)
		object_drop_fall(obj);

	/* Also disarm 2nd weapon mwahahaahahaa */
	if ((obj=get_eq_char(victim,WEAR_SECONDARY))!=NULL) {
	    obj_from_char( obj );
	    obj_to_room( obj, victim->in_room );
	    if (IS_NPC(victim) && victim->wait == 0 && can_see_obj(victim,obj))
		get_obj(victim,obj,NULL);
	    else
		if (obj->in_room->sector_type==SECT_AIR)
		    object_drop_air(obj);
		else if (number_percent()<10)
		    object_drop_fall(obj);
	}
    }                               /* disarms them, and NPCs will collect
				       their weapon if they can see it.
				       Ta-daa, smite compleeeeeet. Ouch. */
    return;
}


void do_aexit (CHAR_DATA *ch, char *argument) {
    AREA_DATA *area;
    ROOM_INDEX_DATA *room,*newroom;
    int	i,j;
    int vnum;
    BUFFER *output;
    char s[MAX_STRING_LENGTH];

    output=new_buf();

    area=ch->in_room->area;
    for (vnum=area->lvnum;vnum<=area->uvnum;vnum++) {
	if ((room=get_room_index(vnum))) {
	    if (room->area==area) {
		for (i=0;i<DIR_MAX;i++) {
		    if (room->exit[i] && room->exit[i]->to_room &&
		        room->exit[i]->to_room->area!=area) {
			sprintf(s,"From room %d to %d (%s) ",
			    room->vnum,
			    room->exit[i]->to_room->vnum,
			    room->exit[i]->to_room->area->name);
			add_buf(output,s);

			newroom=room->exit[i]->to_room;
			for (j=0;j<DIR_MAX;j++) {
			    if (newroom->exit[j] &&
				newroom->exit[j]->to_room &&
				newroom->exit[j]->to_room->area==area)
				break;
			}
			if (j==6)
			    add_buf(output,"(one way)\n\r");
			else
			    add_buf(output,"(two way)\n\r");
		    }
		}
	    }
	}
    }
    page_to_char(buf_string(output),ch);
    free_buf(output);
}

