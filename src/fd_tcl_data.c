//
// $Id: fd_tcl_data.c,v 1.7 2002/12/29 18:45:49 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#define NEEDS_TCL
#include "merc.h"

FS_ENTRY *fs_getentry(int descriptor) {
    FS_ENTRY *fs;

    fs=fs_list;
    while (fs) {
        if (fs->descriptor==descriptor)
            break;
        fs=fs->next;
    }
    return fs;
}

int fs_open(char *data,char *name) {
    FS_ENTRY *fs;

    fs=new_fsentry();
    fs->next=fs_list;
    fs_list=fs;
    if (fs->next==NULL)
        fs->descriptor=1;
    else
        fs->descriptor=fs->next->descriptor+1;

    fs->filename=str_dup(name);
    fs->data=data;
    fs->offset=0L;
    fs->size=strlen(data);

    return fs->descriptor;
}

char *fs_read(int descriptor) {
    FS_ENTRY *fs;
    char *p,*q;
    static char s[MSL];

    if ((fs=fs_getentry(descriptor))==NULL)
        return NULL;

    if (fs->size==fs->offset)
        return NULL;
    p=q=strchr(fs->data+fs->offset,'\n');
    if (p==NULL)
        p=fs->data+fs->size;
    strncpy(s,fs->data+fs->offset,p-fs->data-fs->offset);
    s[p-fs->data-fs->offset]=0;
    fs->offset+=strlen(s)+2;
    if (q==NULL)
        fs->offset=fs->size;

    return s;
}

void fs_close(int descriptor) {
    FS_ENTRY *fs,*fsnext;

    if ((fs=fs_getentry(descriptor))==NULL)
        return;
    if (fs==fs_list)
        fs_list=fs_list->next;
    else {
        fsnext=fs_list;
        while (fs->next!=fs)
            fsnext=fsnext->next;
        fsnext->next=fs->next;
    }
    free_fsentry(fs);
}

void fs_seek(int descriptor,int offset,int whence) {
    FS_ENTRY *fs;

    if ((fs=fs_getentry(descriptor))==NULL)
        return;
    if (whence==SEEK_SET) {
        if (offset>fs->size)
            fs->offset=fs->size;
        else
            fs->offset=offset;
    }
    if (whence==SEEK_CUR) {
        if (offset+fs->offset>fs->size)
            fs->offset=fs->size;
        else
            fs->offset=offset+fs->offset;
    }
    if (whence==SEEK_END) {
        if (offset>0)
            fs->offset=fs->size;
        else if (offset<-fs->size)
            fs->offset=0;
        else
            fs->offset=fs->size+offset;
    }
}

int fs_tell(int descriptor) {
    FS_ENTRY *fs;

    if ((fs=fs_getentry(descriptor))==NULL)
        return 0;

    return fs->offset;
}

bool fs_eof(int descriptor) {
    FS_ENTRY *fs;

    if ((fs=fs_getentry(descriptor))==NULL)
        return TRUE;

    return fs->size==fs->offset;
}


int tclDataObjCmd(ClientData client, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
    int index;
    int nargs=2;

    static tclOptionList *objOptions[]={
        "open", "read", "close", "tell", "seek", "eof",
        (char *)NULL};

    if (Tcl_GetIndexFromObj(interp, objv[1], objOptions, "option", 0, &index) != TCL_OK)
        return TCL_ERROR;

    switch(index) {
    case 0: // open -fuzzy
        if (objc!=3 && objc!=4) {
            Tcl_WrongNumArgs(interp,0,objv,"data open ?-fuzzy? file");
            return TCL_ERROR;
        }
        nargs=0;
    {
        char *s,*file,*data=NULL;
        HELP_DATA *pHelp;
        bool fuzzy=FALSE;
        int i=2;

        static tclOptionList *searchSwitches[]={"-fuzzy",NULL};

        while(objc-i>0) {
            s=Tcl_GetString(objv[i]);
            if (*s!='-') break;

            if (Tcl_GetIndexFromObj(interp, objv[i], searchSwitches, "option", 0, &index) != TCL_OK)
                return TCL_ERROR;

            if (index==0) fuzzy=TRUE;

            i++;
        }

        if (objc-i!=1) {
            Tcl_WrongNumArgs(interp,0,objv,"data open ?-fuzzy? file");
            return TCL_ERROR;
        }

        file=Tcl_GetString(objv[i]);

        for ( pHelp = help_first; pHelp != NULL; pHelp = pHelp->next ) {
            if ( pHelp->keyword[0]=='*'?is_exact_name(file, pHelp->keyword+1):
                 fuzzy?is_name(file,pHelp->keyword):is_exact_name(file,pHelp->keyword) ) {
                data=pHelp->text;
                break;
            }
        }

        if (!data) {
            Tcl_SetObjResult(interp,Tcl_NewStringObj("data open file: Cannot find requested data-file",-1));
            return TCL_ERROR;
        }
        Tcl_SetObjResult(interp,Tcl_NewIntObj(fs_open(pHelp->text,file)));
        break;
    }
        Tcl_SetObjResult(interp,Tcl_NewIntObj(0));

        break;
    case 1: // read
        if (objc!=3) {
            Tcl_WrongNumArgs(interp,0,objv,"data read descriptor");
            return TCL_ERROR;
        }
        nargs=0;
    {
        int descriptor;
        char *s;

        if (Tcl_GetIntFromObj(interp,objv[2],&descriptor)!=TCL_OK) return TCL_ERROR;
        s=fs_read(descriptor);
        Tcl_SetObjResult(interp,Tcl_NewStringObj(s,-1));
    }
        break;
    case 2: // close
        if (objc!=3) {
            Tcl_WrongNumArgs(interp,0,objv,"data close descriptor");
            return TCL_ERROR;
        }
        nargs=0;
    {
        int descriptor;

        if (Tcl_GetIntFromObj(interp,objv[2],&descriptor)!=TCL_OK) return TCL_ERROR;
        fs_close(descriptor);
    }
        break;
    case 3: // tell
        if (objc!=3) {
            Tcl_WrongNumArgs(interp,0,objv,"data tell descriptor");
            return TCL_ERROR;
        }
        nargs=0;
    {
        int descriptor;

        if (Tcl_GetIntFromObj(interp,objv[2],&descriptor)!=TCL_OK) return TCL_ERROR;
        Tcl_SetObjResult(interp,Tcl_NewIntObj(fs_tell(descriptor)));
    }
        break;
    case 4: // seek
        if (objc!=5) {
            Tcl_WrongNumArgs(interp,0,objv,"data seek descriptor offset whence");
            return TCL_ERROR;
        }
        nargs=0;
    {
        int descriptor,offset;
        char *whence;

        if (Tcl_GetIntFromObj(interp,objv[2],&descriptor)!=TCL_OK) return TCL_ERROR;
        if (Tcl_GetIntFromObj(interp,objv[3],&offset)!=TCL_OK) return TCL_ERROR;
        whence=Tcl_GetString(objv[4]);

        if (str_cmp(whence,"set")==0)
            fs_seek(descriptor,offset,SEEK_SET);
        if (str_cmp(whence,"end")==0)
            fs_seek(descriptor,offset,SEEK_END);
        if (str_cmp(whence,"cur")==0)
            fs_seek(descriptor,offset,SEEK_CUR);
    }
        break;
    case 5: // eof
        if (objc!=3) {
            Tcl_WrongNumArgs(interp,0,objv,"data eof descriptor");
            return TCL_ERROR;
        }
        nargs=0;
    {
        int descriptor;

        if (Tcl_GetIntFromObj(interp,objv[2],&descriptor)!=TCL_OK) return TCL_ERROR;
        Tcl_SetObjResult(interp,Tcl_NewBooleanObj(fs_eof(descriptor)));
    }
        break;

    default:
        Tcl_SetObjResult(interp,Tcl_NewStringObj("data: You should not get this error.",-1));
        return TCL_ERROR;
    }

    if (nargs!=0 && nargs!=objc)
    {
        Tcl_Obj *result;

        result=Tcl_NewStringObj("wrong # args for obj ",-1);
        Tcl_AppendObjToObj(result,objv[0]);
        Tcl_AppendToObj(result,", only ",-1);
        Tcl_AppendObjToObj(result,Tcl_NewIntObj(nargs));
        Tcl_AppendToObj(result," argument(s) allowed.",-1);
        Tcl_SetObjResult(interp,result);
        return TCL_ERROR;
    }

    return TCL_OK;
}
