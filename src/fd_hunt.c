//
// $Id: fd_hunt.c,v 1.25 2006/08/25 09:43:19 jodocus Exp $ */
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"
#include "interp.h"
#include "color.h"

/* When the hunt code has been used too often, reset the hunt codes with
 * this routines.
 */
void huntreset_rooms(void)
{
    ROOM_INDEX_DATA *pRoom;
    int i;

    for (i=0;i<MAX_KEY_HASH;i++) {
	for(pRoom=room_index_hash[i];pRoom;pRoom=pRoom->next)
	    pRoom->hunt=0;
    }
}

int find_path(CHAR_DATA *ch,int from_room,
    int to_room,int max_depth,bool trough_doors,bool in_zone)
{
    static struct {
	char direction;
	ROOM_INDEX_DATA *room;
    } rooms[2][MAX_HUNT_SIZE];
    int roomspos[2];
    static int huntcode=0;
    int curarray=0;
    int i,j,k;
    int depth;
    ROOM_INDEX_DATA *pRoom,*pNextRoom;
    EXIT_DATA *pExit;

    if (max_depth>MAX_HUNT_DEPTH) max_depth=MAX_HUNT_DEPTH;

    if (from_room==to_room) return -1;
    if ((pRoom=get_room_index(from_room))==NULL) return -1;
    if ((pNextRoom=get_room_index(to_room))==NULL) return -1;
    if (in_zone && pRoom->area!=pNextRoom->area) return -1;

    if (++huntcode>1000000) {
	huntreset_rooms();
	huntcode=1;
    }

    /* Mark this room as seen */
    pRoom->hunt=huntcode;

    /* Fill the first array, this code is not in a procedure for speed */
    roomspos[curarray]=0;
    for(i=0;i<DIR_MAX;i++)
    {
	if ((pExit=pRoom->exit[i])
	&& (pNextRoom=pExit->to_room)
	&& pNextRoom->hunt!=huntcode
	&& can_see_room(ch,pNextRoom)
	&& (!in_zone || pRoom->area==pNextRoom->area))
	{
	    /* Check doors */
	    if (IS_SET(pExit->exit_info,EX_CLOSED)) {
		if (IS_SET(pExit->exit_info,EX_SECRET)) continue;
		if ((!IS_AFFECTED(ch,EFF_PASS_DOOR)
		|| IS_SET(pExit->exit_info,EX_NOPASS))
		&& !IS_TRUSTED(ch,LEVEL_IMMORTAL)
		&& !trough_doors) continue;
	    }

	    if (pNextRoom->vnum==to_room)
		return i;

	    rooms[curarray][roomspos[curarray]].direction=i;
	    rooms[curarray][roomspos[curarray]].room=pNextRoom;
	    roomspos[curarray]++;
	    pNextRoom->hunt=huntcode;
	}
    }

    depth=1;
    for(;;) {
	j=1-curarray;
	roomspos[j]=0;
	for (i=0;i<roomspos[curarray];i++) {
	    pRoom=rooms[curarray][i].room;
	    for(k=0;k<MAX_DIR;k++) {
		if ((pExit=pRoom->exit[k])
		&& (pNextRoom=pExit->to_room)
		&& pNextRoom->hunt!=huntcode
		&& can_see_room(ch,pNextRoom)
		&& (!in_zone || pRoom->area==pNextRoom->area)) {
		    /* Check doors */
		    if (IS_SET(pExit->exit_info,EX_CLOSED)) {
			if (IS_SET(pExit->exit_info,EX_SECRET)) continue;
			if ((!IS_AFFECTED(ch,EFF_PASS_DOOR)
			|| IS_SET(pExit->exit_info,EX_NOPASS))
			&& !IS_TRUSTED(ch,LEVEL_IMMORTAL)
			&& !trough_doors) continue;
	    	    }

		    if (pNextRoom->vnum==to_room)
			return rooms[curarray][i].direction;

		    if (roomspos[j]-1>=MAX_HUNT_SIZE) return -1;
		    rooms[j][roomspos[j]].direction=rooms[curarray][i].direction;
		    rooms[j][roomspos[j]].room=pNextRoom;
		    roomspos[j]++;
		    pNextRoom->hunt=huntcode;
		}
	    }
	}

	/* No new rooms found, no path found. */
	if (roomspos[j]==0) return -1;

	curarray=1-curarray; 		  /* Swap arrays */
	if (++depth>max_depth) return -1;  /* Path too long */
    }
}

void do_hunt( CHAR_DATA *ch, char *argument )
{
    char buf[MAX_STRING_LENGTH];
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    int direction;
    bool fArea;

    one_argument( argument, arg );

    if( arg[0] == '\0' ) {
	send_to_char( "Whom are you trying to hunt?\n\r", ch );
	return;
    }

    /* only imps can hunt to different areas */
    fArea = ( get_trust(ch) < MAX_LEVEL );

    if( fArea )
	victim = get_char_area( ch, arg );
    else
	victim = get_char_world( ch, arg );

    if( victim == NULL ) {
	send_to_char("No-one around by that name.\n\r", ch );
	return;
    }

    if ( ch->in_room == victim->in_room ) {
	act( "$N is here!", ch, NULL, victim, TO_CHAR );
	return;
    }

    /*
     * Deduct some movement.
     */
    if ( ch->move > 2 )
	ch->move -= 3;
    else {
	send_to_char( "You're too exhausted to hunt anyone!\n\r", ch );
	return;
    }

    act( "$n carefully sniffs the air.", ch, NULL, NULL, TO_ROOM );
    WAIT_STATE( ch, skill_beats(gsn_hunt,ch) );
    direction=find_path(ch,ch->in_room->vnum,victim->in_room->vnum,
	MAX_HUNT_DEPTH,TRUE,fArea);

    if ( direction < 0 ) {
	act( "You couldn't find a path to $N from here.",
	    ch, NULL, victim, TO_CHAR );
	return;
    }

    if ( direction < 0 || direction >= MAX_DIR ) {
	send_to_char( "Hmm... Something seems to be wrong.\n\r", ch );
	return;
    }

    /*
     * Give a random direction if the player misses the die roll.
     */
    if (!skillcheck(ch,gsn_hunt)) {
	do {
	    direction = number_door();
	} while (( ch->in_room->exit[direction] == NULL )
	      || ( ch->in_room->exit[direction]->to_room == NULL) );
    }

    /*
     * Display the results of the search.
     */
    sprintf( buf, "$N is %s from here.", dir_name[direction] );
    act( buf, ch, NULL, victim, TO_CHAR );
    if ( ch->in_room->exit[direction]->to_room == victim->in_room
	&& number_percent()>4) {	// add some randomness
	act( "$S smell is very strong here.", ch, NULL, victim, TO_CHAR );
    }

    check_improve(ch, gsn_hunt, TRUE,5);
    return;
}

void hunt_victim( CHAR_DATA *ch )
{
    int dir;
    bool found=FALSE;
    int vnum;
    bool inarea;
    CHAR_DATA *hunt_char;
    ROOM_INDEX_DATA *hunt_room;

    if (!IS_VALID(ch)) return;
    if (ch==NULL || !IS_NPC(ch)) return;
    if (ch->hunt_id==0) return;

    if (ch->hunt_type==HUNT_KILL || ch->hunt_type==HUNT_FIND || ch->hunt_type==HUNT_GFIND) {
	/*
	 * Make sure the victim still exists.
	 * Big bug here... You should use the ID... (I think)
	 */
	for (hunt_char = char_list; hunt_char; hunt_char = hunt_char->next ) {
	    if (hunt_char->id==ch->hunt_id) {
		found=TRUE;
		break;
	    }
	}

	if ( !found || !can_see( ch, hunt_char ) ) {
	    do_function(ch,&do_say, "Damn!  My prey is gone!!" );
	    mp_didnotfind_trigger(ch,HUNT_RESULT_NOT_FOUND);
	    ch->hunt_type=HUNT_NONE;
	    return;
	}

	if (ch->in_room == hunt_char->in_room ) {
	    if (ch->hunt_type==HUNT_KILL) {
		act( "$n glares at $N and says, '"C_SAY"Ye shall DIE!{x'",
			ch, NULL, hunt_char, TO_NOTVICT );
		act( "$n glares at you and says, '"C_SAY"Ye shall DIE!{x'",
			ch, NULL, hunt_char, TO_VICT );
		act( "You glare at $N and say, '"C_SAY"Ye shall DIE!{x",
			ch, NULL, hunt_char, TO_CHAR);
		multi_hit( ch, hunt_char, TYPE_UNDEFINED );
	    }
	    mp_walked_trigger(ch);
	    ch->hunt_type = HUNT_NONE;
	    return;
	}

	WAIT_STATE( ch, skill_beats(gsn_hunt,ch) );
	inarea=!(((ch->level >= hunt_char->level+10) &&
		  !STR_IS_SET(ch->strbit_act,ACT_AGGRESSIVE) &&
		  !STR_IS_SET(ch->strbit_act,ACT_TCL_AGGRESSIVE)) ||
		 (ch->hunt_type==HUNT_GFIND) );
	dir = find_path( ch, ch->in_room->vnum, hunt_char->in_room->vnum,
			MAX_HUNT_DEPTH, TRUE, inarea );

	if ( dir < 0 || dir >= MAX_DIR ) {
	    act( "$n says '"C_SAY"Damn!  Lost $M!{x'", ch, NULL, hunt_char, TO_ROOM );
	    mp_didnotfind_trigger(ch,HUNT_RESULT_NO_PATH);
	    ch->hunt_type = HUNT_NONE;
	    return;
	}
    } else if (ch->hunt_type==HUNT_ROOM) {
	if ((hunt_room=get_room_index(ch->hunt_id))==NULL) {
	    mp_didnotfind_trigger(ch,HUNT_RESULT_NOT_FOUND);
	    ch->hunt_type=HUNT_NONE;
	    return;
	}
	if (ch->in_room==hunt_room) {
	    mp_walked_trigger(ch);
	    ch->hunt_type=HUNT_NONE;
	    return;
	}
	dir=find_path(ch,ch->in_room->vnum,hunt_room->vnum,MAX_HUNT_DEPTH,TRUE, FALSE);
    }

    /*
     * Give a random direction if the mob misses the die roll.
     */
    if (number_percent () > 75 ) {       /* @ 25% */
	do {
	    dir = number_door();
	} while( ( ch->in_room->exit[dir] == NULL )
	      || ( ch->in_room->exit[dir]->to_room == NULL ) );
    }

    if( IS_SET( ch->in_room->exit[dir]->exit_info, EX_CLOSED ) ) {
	do_function(ch,&do_open,(char *) dir_name[dir] );
	return;
    }
    if (!IS_VALID(ch))		// exploding doors cause this!
	return;

    vnum=ch->in_room->vnum;
    move_char( ch, dir, FALSE, FALSE );
    if (!IS_VALID(ch))		// deathtraps cause this!
	return;

    if (ch->in_room->vnum==vnum) {
	if (ch->hunt_type==HUNT_KILL || ch->hunt_type==HUNT_FIND || ch->hunt_type==HUNT_GFIND)
	    act("$n says '"C_SAY"Damn! Lost $M!{x'",ch,NULL,hunt_char,TO_ROOM);
	else
	    do_function(ch,&do_say, "I have lost the way." );
	mp_didnotfind_trigger(ch,HUNT_RESULT_NO_PATH);
	ch->hunt_type=HUNT_NONE;
	return;
    }

    return;
}
