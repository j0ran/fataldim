//
// $Id: fd_roomprog.h,v 1.25 2008/05/01 18:47:03 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef INCLUDED_FD_ROOMPROG_H
#define INCLUDED_FD_ROOMPROG_H

int	rp_error		(ROOM_DATA *room);

bool	rp_prelook_trigger	(ROOM_DATA *room,CHAR_DATA *ch);
void	rp_look_trigger		(ROOM_DATA *room,CHAR_DATA *ch);
bool	rp_prelook_ed_trigger	(ROOM_DATA *room,CHAR_DATA *ch,char *txt);
void	rp_look_ed_trigger	(ROOM_DATA *room,CHAR_DATA *ch,char *txt);

void	rp_enter_trigger	(ROOM_DATA *room,CHAR_DATA *ch,int dir);
bool	rp_leave_trigger	(ROOM_DATA *room,CHAR_DATA *ch,int dir);

bool	rp_pretrap_trigger	(ROOM_DATA *room,CHAR_DATA *ch,int dir);
void	rp_trap_trigger		(ROOM_DATA *room,CHAR_DATA *ch,int dir);

bool    rp_prerecall_trigger	(ROOM_DATA *room,CHAR_DATA *ch);
void    rp_recall_trigger	(ROOM_DATA *room,CHAR_DATA *ch);
void    rp_recallto_trigger	(ROOM_DATA *room,CHAR_DATA *ch);

bool	rp_interpret_unknown	(CHAR_DATA *ch,char *string);
bool	rp_interpret_preknown	(CHAR_DATA *ch,char *string);
void	rp_interpret_postknown	(CHAR_DATA *ch,char *string);
bool    rp_presocial_trigger	(CHAR_DATA *ch,char *string,CHAR_DATA *victim);
void    rp_social_trigger	(CHAR_DATA *ch,char *string,CHAR_DATA *victim);

bool	rp_prespeech_trigger	(CHAR_DATA *ch,char *string);
void	rp_speech_trigger	(CHAR_DATA *ch,char *string);

bool	rp_prereset_trigger	(ROOM_DATA *room);
void	rp_reset_trigger	(ROOM_DATA *room);

bool	rp_preattack_trigger	(CHAR_DATA *ch,CHAR_DATA *victim,char *string);
void	rp_kill_trigger		(CHAR_DATA *ch,CHAR_DATA *victim);
void    rp_death_trigger	(CHAR_DATA *ch,CHAR_DATA *killer);

void	rp_delay_trigger	(ROOM_DATA *room);
void	rp_hour_trigger		(ROOM_DATA *room,int hour);
void	rp_timer_trigger	(ROOM_DATA *room);
void	rp_random_trigger	(ROOM_DATA *room);

void	rp_sunset_trigger	(void);
void	rp_sunrise_trigger	(void);
void	rp_dayend_trigger	(void);
void	rp_daystart_trigger	(void);

void    rp_weather_trigger      (int old_state, int new_state);

bool	rp_canlock_trigger	(ROOM_DATA *room,CHAR_DATA *ch,int dir);
bool	rp_prelock_trigger	(ROOM_DATA *room,CHAR_DATA *ch,int dir);
void	rp_lock_trigger		(ROOM_DATA *room,CHAR_DATA *ch,int dir);
bool	rp_canunlock_trigger	(ROOM_DATA *room,CHAR_DATA *ch,int dir);
bool	rp_preunlock_trigger	(ROOM_DATA *room,CHAR_DATA *ch,int dir);
void	rp_unlock_trigger	(ROOM_DATA *room,CHAR_DATA *ch,int dir);

#endif	// INCLUDED_FD_ROOMPROG_H
