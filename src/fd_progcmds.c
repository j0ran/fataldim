//
// $Id: fd_progcmds.c,v 1.20 2004/02/22 10:00:12 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"
#include "interp.h"

extern ROOM_INDEX_DATA *find_location( CHAR_DATA *, char * );


/*
 * Displays MOBprogram triggers of a mobile
 *
 * Syntax: mpstat [name]
 */
void do_mpstat( CHAR_DATA *ch, char *argument )
{
    TCLPROG_LIST  *mprg;
    CHAR_DATA   *victim;
    char	empty_trigger[MPROG_MAX_TRIGGERS];

    if ( argument[0] == '\0' )
    {
	send_to_char( "Mpstat whom?\n\r", ch );
	return;
    }

    if ( ( victim = get_char_world( ch, argument ) ) == NULL )
    {
	send_to_char( "No such creature.\n\r", ch );
	return;
    }

    if ( !IS_NPC( victim ) )
    {
	send_to_char( "That is not a mobile.\n\r", ch);
	return;
    }

    sprintf_to_char(ch,"Mobile  #%-6d [%s]\n\r",
	victim->pIndexData->vnum, victim->short_descr );
    sprintf_to_char(ch,"Program #%-6d\n\r",victim->pIndexData->mprog_vnum);

    sprintf_to_char( ch, "Delay   %-6d  [%s]\n\r"
		  "Timer   %-6ld\n\r",
	victim->mprog_delay,
	victim->mprog_target == NULL
		? "No target" : victim->mprog_target->name,
	victim->mprog_timer );

    STR_ZERO_BIT(empty_trigger,MPROG_MAX_TRIGGERS);
    if ( STR_SAME_STR(victim->strbit_mprog_triggers,empty_trigger,MPROG_MAX_TRIGGERS) )
    {
	send_to_char( "[No triggers set]\n\r", ch);
	return;
    }

    STR_SET_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);
    for ( mprg = victim->mprogs; mprg != NULL;
	 mprg = mprg->next )

    {
	sprintf_to_char( ch, "[%s] Trigger [%-9s] Program [%s] Phrase [%s]\n\r",
	      mprg->name,
	      table_find_value(mprg->trig_type,mobtrigger_table),
	      mprg->code,
	      mprg->trig_phrase );
    }
    STR_REMOVE_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);

    return;

}

/*
 * Displays the source code of a given MOBprogram
 *
 * Syntax: mpdump [vnum]
 */
void do_mpdump( CHAR_DATA *ch, char *argument )
{
    char buf[ MAX_INPUT_LENGTH ];
    TCLPROG_CODE *mprg;
    CHAR_DATA *victim;
    int vnum;

    if (argument[0]==0) {
	send_to_char("Usage: mpdump <vnum> or mpdump <victim>\n\r",ch);
	return;
    }

    one_argument( argument, buf );

    if (is_number(buf))
	vnum=atoi(buf);
    else {
	if ((victim=get_char_world(ch,buf))==NULL) {
	    send_to_char("No such mob known.\n\r",ch);
	    return;
	}
	if (IS_PC(victim)) {
	    send_to_char("That is not a mob\n\r",ch);
	    return;
	}
	vnum=victim->pIndexData->mprog_vnum;
    }

    if ( ( mprg = get_mprog_index( vnum ) ) == NULL ) {
	send_to_char( "No such MOBprogram.\n\r", ch );
	return;
    }

    if (mprg->edit)
	send_to_char("}rNote: program is being edited!{x\n\r",ch);

    STR_SET_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);
    page_to_char( mprg->code, ch );
    STR_REMOVE_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);
}


/*
 * Displays OBJprogram triggers of an object
 *
 * Syntax: opstat [name]
 */
void do_opstat( CHAR_DATA *ch, char *argument )
{
    char        arg[ MAX_STRING_LENGTH  ];
    TCLPROG_LIST  *oprg;
    OBJ_DATA    *obj;
    char	empty_trigger[OPROG_MAX_TRIGGERS];

    one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Opstat what?\n\r", ch );
	return;
    }

    if ( ( obj = get_obj_here( ch, arg ) ) == NULL )
    {
	send_to_char( "No such object.\n\r", ch );
	return;
    }

    sprintf_to_char(ch,"Object  #%-6d [%s]\n\r",
	obj->pIndexData->vnum, obj->short_descr );
    sprintf_to_char(ch,"Program #%-6d\n\r",
	obj->pIndexData->oprog_vnum);

    sprintf_to_char( ch,  "Delay   %-6d  [%s]\n\r"
			  "Timer   %-6ld\n\r",
	obj->oprog_delay,
	obj->oprog_target == NULL
		? "No target" : obj->oprog_target->name,
	obj->oprog_timer );

    STR_ZERO_BIT(empty_trigger,OPROG_MAX_TRIGGERS);
    if ( STR_SAME_STR(obj->strbit_oprog_triggers,empty_trigger,OPROG_MAX_TRIGGERS) )
    {
	send_to_char( "[No triggers set]\n\r", ch);
	return;
    }

    STR_SET_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);
    for ( oprg = obj->oprogs; oprg != NULL; oprg = oprg->next ) {
	sprintf_to_char(ch,"[%s] Trigger [%-9s] Program [%s] Phrase [%s]\n\r",
	      oprg->name,
	      table_find_value(oprg->trig_type,objtrigger_table),
	      oprg->code,
	      oprg->trig_phrase );
    }
    STR_REMOVE_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);

    return;

}


/*
 * Displays the source code of a given OBJprogram
 *
 * Syntax: opdump [vnum]
 */
void do_opdump( CHAR_DATA *ch, char *argument )
{
    char buf[ MAX_INPUT_LENGTH ];
    TCLPROG_CODE *oprg;
    OBJ_DATA *obj;
    int vnum;

    if (argument[0]==0) {
	send_to_char("Usage: opdump <vnum> or opdump <object>\n\r",ch);
	return;
    }

    one_argument( argument, buf );

    if (is_number(buf))
	vnum=atoi(buf);
    else {
	if ((obj=get_obj_world(ch,buf))==NULL) {
	    send_to_char("No such object known.\n\r",ch);
	    return;
	}
	vnum=obj->pIndexData->oprog_vnum;
    }

    if ( ( oprg = get_oprog_index( vnum ) ) == NULL ) {
	send_to_char( "No such objprog.\n\r", ch );
	return;
    }

    if (oprg->edit)
	send_to_char("}rNote: program is being edited!{x\n\r",ch);

    STR_SET_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);
    page_to_char( oprg->code, ch );
    STR_REMOVE_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);
}


/*
 * Displays Roomprogram triggers of a room
 *
 * Syntax: rpstat
 */
void do_rpstat( CHAR_DATA *ch, char *argument )
{
    TCLPROG_LIST  *rprg;
    ROOM_INDEX_DATA *room=ch->in_room;
    char	empty_trigger[RPROG_MAX_TRIGGERS];


    sprintf_to_char(ch,"Room    #%-6d [%s]\n\r",
	room->vnum, room->name );
    sprintf_to_char(ch,"Program #%-6d\n\r",room->rprog_vnum);

    sprintf_to_char( ch,  "Delay   %-6d  [%s]\n\r"
			  "Timer   %-6ld\n\r",
	room->rprog_delay,
	room->rprog_target == NULL
		? "No target" : room->rprog_target->name,
	room->rprog_timer );

    STR_ZERO_BIT(empty_trigger,RPROG_MAX_TRIGGERS);
    if ( STR_SAME_STR(room->strbit_rprog_triggers,empty_trigger,RPROG_MAX_TRIGGERS) )
    {
	send_to_char( "[No triggers set]\n\r", ch);
	return;
    }

    STR_SET_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);
    for ( rprg = room->rprogs; rprg != NULL; rprg = rprg->next ) {
	sprintf_to_char(ch,"[%s] Trigger [%-9s] Program [%s] Phrase [%s]\n\r",
	      rprg->name,
	      table_find_value(rprg->trig_type,roomtrigger_table),
	      rprg->code,
	      rprg->trig_phrase );
    }
    STR_REMOVE_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);

    return;

}


/*
 * Displays the source code of a given roomprogram
 *
 * Syntax: rpdump [vnum]
 */
void do_rpdump( CHAR_DATA *ch, char *argument )
{
    char buf[ MAX_INPUT_LENGTH ];
    TCLPROG_CODE *rprg;
    int vnum;

    if (argument[0]==0) {
	vnum=ch->in_room->vnum;
    } else {
	one_argument( argument, buf );

	vnum=atoi(buf);
    }
    if ( ( rprg = get_rprog_index( vnum ) ) == NULL ) {
	send_to_char( "No such roomprog.\n\r", ch );
	return;
    }

    if (rprg->edit)
	send_to_char("}rNote: program is being edited!{x\n\r",ch);

    STR_SET_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);
    page_to_char( rprg->code, ch );
    STR_REMOVE_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);
}


/*
 * Displays AreaProgram triggers of an area
 *
 * Syntax: rpstat
 */
void do_apstat( CHAR_DATA *ch, char *argument )
{
    TCLPROG_LIST  *aprg;
    AREA_DATA *area=ch->in_room->area;
    char	empty_trigger[APROG_MAX_TRIGGERS];


    sprintf_to_char(ch,"Area    #%-6d [%s]\n\r",
	area->vnum, area->name );
    sprintf_to_char(ch,"Program %s %s\n\r",area->aprog_enabled,(area->aprog_enabled&&area->aprog)?area->aprog->title:"");

    sprintf_to_char( ch,  "Delay   %-6d  [%s]\n\r"
			  "Timer   %-6ld\n\r",
	area->aprog_delay,
	area->aprog_target == NULL
		? "No target" : area->aprog_target->name,
	area->aprog_timer );

    STR_ZERO_BIT(empty_trigger,APROG_MAX_TRIGGERS);
    if ( STR_SAME_STR(area->strbit_aprog_triggers,empty_trigger,APROG_MAX_TRIGGERS) )
    {
	send_to_char( "[No triggers set]\n\r", ch);
	return;
    }

    STR_SET_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);
    for ( aprg = area->aprogs; aprg != NULL; aprg = aprg->next ) {
	sprintf_to_char(ch,"[%s] Trigger [%-9s] Program [%s] Phrase [%s]\n\r",
	      aprg->name,
	      table_find_value(aprg->trig_type,areatrigger_table),
	      aprg->code,
	      aprg->trig_phrase );
    }
    STR_REMOVE_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);

    return;

}


/*
 * Displays the source code of a given areaprogram
 *
 * Syntax: apdump [vnum]
 */
void do_apdump( CHAR_DATA *ch, char *argument )
{
    TCLPROG_CODE *aprg;
    int vnum;

    char buf[ MAX_INPUT_LENGTH ];
    if (argument[0]==0) {
	vnum=ch->in_room->area->vnum;
    } else {
	one_argument( argument, buf );

	vnum=atoi(buf);
    }
    if ( ( aprg = get_aprog_index( vnum ) ) == NULL ) {
	send_to_char( "No such areaprog.\n\r", ch );
	return;
    }

    if (aprg->edit)
	send_to_char("}rNote: program is being edited!{x\n\r",ch);

    STR_SET_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);
    page_to_char( aprg->code, ch );
    STR_REMOVE_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);
}

