//
// $Id: wiz_where.c,v 1.11 2007/03/11 11:47:49 jodocus Exp $ */
//
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

#include "merc.h"
#include "db.h"
#include "olc.h"

//
// This file contains all the xxwhere and vnum commands.
//

//
// VNUM
//
void do_vnum(CHAR_DATA *ch, char *argument)
{
    char arg[MAX_INPUT_LENGTH];

    argument = one_argument(argument,arg);
    switch(which_exact_keyword(arg,"obj","mob","char","skill","spell","area","type",NULL))
    {
        case -1:
	    send_to_char("Syntax:\n\r",ch);
	    send_to_char("  vnum obj [<level>] <name>\n\r",ch);
	    send_to_char("  vnum mob [<level>] <name>\n\r",ch);
	    send_to_char("  vnum skill <skill or spell>\n\r",ch);
	    send_to_char("  vnum type [global] <type>\n\r",ch);
	    send_to_char("  vnum area\n\r",ch);
	    return;
        case 0:
	    vnum_mobile(ch, arg);
	    vnum_object(ch, arg);
	    return;
        case 1:
	    vnum_object(ch, argument);
	    return;
        case 2:
        case 3:
	    vnum_mobile(ch, argument);
	    return;
        case 4:
        case 5:
	    vnum_skill(ch, argument);
	    return;
	case 6:
	    one_argument(argument,arg);
	    if (arg[0]==0)
		vnum_area(ch,0);
	    else
		vnum_area(ch,atoi(arg));
	    return;
	case 7:
	    vnum_type(ch,argument);
	    return;
    }
}


void vnum_mobile( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    MOB_INDEX_DATA *pMobIndex;
    int vnum;
    int nMatch;
    bool fAll;
    bool found;
    int level=0;
    char *tmp;

    tmp=one_argument( argument, arg );
    level=atoi(arg);
    if(level)
    {
	argument=tmp;
	one_argument( argument, arg );
    }
    if ( arg[0] == '\0' )
    {
	send_to_char( "Find whom?\n\r", ch );
	return;
    }

    if(level) fAll=!str_cmp( arg, "all" );
    else fAll	= FALSE;
    found	= FALSE;
    nMatch	= 0;

    /*
     * Yeah, so iterating over all vnum's takes 10,000 loops.
     * Get_mob_index is fast, and I don't feel like threading another link.
     * Do you?
     * -- Furey
     */
    for ( vnum = 0; vnum < max_vnum_mob; vnum++ )
    {
	if ( ( pMobIndex = get_mob_index( vnum ) ) != NULL )
	{
	    nMatch++;
	    if ( ( fAll || is_name( argument, pMobIndex->player_name ) )
	    && (level==0 || pMobIndex->level==level))
	    {
		found = TRUE;
		sprintf_to_char(ch, "[%5d] %s\n\r",
		    pMobIndex->vnum, pMobIndex->short_descr );
	    }
	}
    }

    if ( !found )
	send_to_char( "No mobiles by that name.\n\r", ch );

    return;
}



void vnum_object( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    OBJ_INDEX_DATA *pObjIndex;
    int vnum;
    int nMatch;
    bool fAll;
    bool found;
    int level;
    char *tmp;

    tmp=one_argument( argument, arg );
    level=atoi(arg);
    if(level)
    {
	argument=tmp;
	one_argument( argument, arg );
    }
    if ( arg[0] == '\0' )
    {
	send_to_char( "Find what?\n\r", ch );
	return;
    }

    if(level) fAll=!str_cmp( arg, "all" );
    else fAll	= FALSE;
    found	= FALSE;
    nMatch	= 0;

    /*
     * Yeah, so iterating over all vnum's takes 10,000 loops.
     * Get_obj_index is fast, and I don't feel like threading another link.
     * Do you?
     * -- Furey
     */
    for ( vnum = 0; vnum < max_vnum_obj; vnum++ )
    {
	if ( ( pObjIndex = get_obj_index( vnum ) ) != NULL )
	{
	    nMatch++;
	    if ( ( fAll || is_name( argument, pObjIndex->name ) )
	    && (level==0 || pObjIndex->level==level))
	    {
		found = TRUE;
		sprintf_to_char(ch, "[%5d] %s\n\r",
		    pObjIndex->vnum, pObjIndex->short_descr );
	    }
	}
    }

    if ( !found )
	send_to_char( "No objects by that name.\n\r", ch );

    return;
}


#define VNUM_DISPLAY_MAX 70
void vnum_area(CHAR_DATA *ch,int vnum) {
    int			first,previous;
    OBJ_INDEX_DATA *	oid;
    MOB_INDEX_DATA *	mid;
    ROOM_INDEX_DATA *	rid;
    AREA_DATA *		area;
    TCLPROG_CODE *	tclprog;
    char		s[100],string[100];

    if (vnum==0)
	area=ch->in_room->area;
    else
	if ((area=get_area_data(vnum))==NULL) {
	    send_to_char("Invalid area number.\n\r",ch);
	    return;
	}

    sprintf_to_char(ch,"{yArea     : {x%s\n\r{yAvailable: {x%d {y-{x %d\n\n\r",
			area->name,area->lvnum,area->uvnum);

    first=-1;
    previous=-1;
    strcpy(string,"{yObjects  : {x");
    for (vnum=area->lvnum;vnum<=area->uvnum;vnum++) {
	if ((oid=get_obj_index(vnum))) {
	    if (previous!=vnum-1) {
		strcat(string,itoa(vnum));
		first=vnum;
	    }
	    previous=vnum;
	}
	if (!oid || vnum==area->uvnum) {
	    if (previous==vnum-1 ||	 // first non-existing after an existing
		(vnum==area->uvnum && previous==vnum)) {
		if (previous!=first)
		   sprintf(s,"-%d,",previous);
		else
		   strcpy(s,",");
		strcat(string,s);
		if (strlen(string)>VNUM_DISPLAY_MAX) {
		    send_to_char(string,ch);
		    send_to_char("\n\r           ",ch);
		    string[0]=0;
		}
	    }
	}
    }
    send_to_char(string,ch);
    send_to_char("\n\r",ch);

    strcpy(string,"\r{yMobiles  : {x");
    first=-1;
    previous=-1;
    for (vnum=area->lvnum;vnum<=area->uvnum;vnum++) {
	if ((mid=get_mob_index(vnum))) {
	    if (previous!=vnum-1) {
		strcat(string,itoa(vnum));
		first=vnum;
	    }
	    previous=vnum;
	}
	if (!mid || vnum==area->uvnum) {
	    if (previous==vnum-1 ||	 // first non-existing after an existing
		(vnum==area->uvnum && previous==vnum)) {
		if (previous!=first)
		   sprintf(s,"-%d,",previous);
		else
		   strcpy(s,",");
		strcat(string,s);
		if (strlen(string)>VNUM_DISPLAY_MAX) {
		    send_to_char(string,ch);
		    send_to_char("\n\r           ",ch);
		    string[0]=0;
		}
	    }
	}
    }
    send_to_char(string,ch);
    send_to_char("\n\r",ch);

    previous=-1;
    first=-1;
    strcpy(string,"\r{yRooms    : {x");
    for (vnum=area->lvnum;vnum<=area->uvnum;vnum++) {
	if ((rid=get_room_index(vnum))) {
	    if (previous!=vnum-1) {
		strcat(string,itoa(vnum));
		first=vnum;
	    }
	    previous=vnum;
	}
	if (!rid || vnum==area->uvnum) {
	    if (previous==vnum-1 ||	 // first non-existing after an existing
		(vnum==area->uvnum && previous==vnum)) {
		if (previous!=first)
		   sprintf(s,"-%d,",previous);
		else
		   strcpy(s,",");
		strcat(string,s);
		if (strlen(string)>VNUM_DISPLAY_MAX) {
		    send_to_char(string,ch);
		    send_to_char("\n\r           ",ch);
		    string[0]=0;
		}
	    }
	}
    }
    send_to_char(string,ch);
    send_to_char("\n\r",ch);

    strcpy(string,"\r{yMobprogs : {x");
    first=-1;
    previous=-1;
    for (vnum=area->lvnum;vnum<=area->uvnum;vnum++) {
	if ((tclprog=get_mprog_index(vnum))) {
	    if (previous!=vnum-1) {
		strcat(string,itoa(vnum));
		first=vnum;
	    }
	    previous=vnum;
	}
	if (!tclprog || vnum==area->uvnum) {
	    if (previous==vnum-1 ||	 // first non-existing after an existing
		(vnum==area->uvnum && previous==vnum)) {
		if (previous!=first)
		   sprintf(s,"-%d,",previous);
		else
		   strcpy(s,",");
		strcat(string,s);
		if (strlen(string)>VNUM_DISPLAY_MAX) {
		    send_to_char(string,ch);
		    send_to_char("\n\r           ",ch);
		    string[0]=0;
		}
	    }
	}
    }
    send_to_char(string,ch);
    send_to_char("\n\r",ch);

    strcpy(string,"\r{yObjprogs : {x");
    first=-1;
    previous=-1;
    for (vnum=area->lvnum;vnum<=area->uvnum;vnum++) {
	if ((tclprog=get_oprog_index(vnum))) {
	    if (previous!=vnum-1) {
		strcat(string,itoa(vnum));
		first=vnum;
	    }
	    previous=vnum;
	}
	if (!tclprog || vnum==area->uvnum) {
	    if (previous==vnum-1 ||	 // first non-existing after an existing
		(vnum==area->uvnum && previous==vnum)) {
		if (previous!=first)
		   sprintf(s,"-%d,",previous);
		else
		   strcpy(s,",");
		strcat(string,s);
		if (strlen(string)>VNUM_DISPLAY_MAX) {
		    send_to_char(string,ch);
		    send_to_char("\n\r           ",ch);
		    string[0]=0;
		}
	    }
	}
    }
    send_to_char(string,ch);
    send_to_char("\n\r",ch);

    strcpy(string,"\r{yRoomprogs: {x");
    first=-1;
    previous=-1;
    for (vnum=area->lvnum;vnum<=area->uvnum;vnum++) {
	if ((tclprog=get_rprog_index(vnum))) {
	    if (previous!=vnum-1) {
		strcat(string,itoa(vnum));
		first=vnum;
	    }
	    previous=vnum;
	}
	if (!tclprog || vnum==area->uvnum) {
	    if (previous==vnum-1 ||	 // first non-existing after an existing
		(vnum==area->uvnum && previous==vnum)) {
		if (previous!=first)
		   sprintf(s,"-%d,",previous);
		else
		   strcpy(s,",");
		strcat(string,s);
		if (strlen(string)>VNUM_DISPLAY_MAX) {
		    send_to_char(string,ch);
		    send_to_char("\n\r           ",ch);
		    string[0]=0;
		}
	    }
	}
    }
    send_to_char(string,ch);
    send_to_char("\n\r",ch);

    strcpy(string,"\r{yAreaprogs: {x");
    first=-1;
    previous=-1;
    for (vnum=area->lvnum;vnum<=area->uvnum;vnum++) {
	if ((tclprog=get_aprog_index(vnum))) {
	    if (previous!=vnum-1) {
		strcat(string,itoa(vnum));
		first=vnum;
	    }
	    previous=vnum;
	}
	if (!tclprog || vnum==area->uvnum) {
	    if (previous==vnum-1 ||	 // first non-existing after an existing
		(vnum==area->uvnum && previous==vnum)) {
		if (previous!=first)
		   sprintf(s,"-%d,",previous);
		else
		   strcpy(s,",");
		strcat(string,s);
		if (strlen(string)>VNUM_DISPLAY_MAX) {
		    send_to_char(string,ch);
		    send_to_char("\n\r           ",ch);
		    string[0]=0;
		}
	    }
	}
    }
    send_to_char(string,ch);
    send_to_char("\n\r",ch);
}


void vnum_skill( CHAR_DATA *ch, char *argument ) {
    char	arg[MAX_INPUT_LENGTH];
    int		sn;

    one_argument( argument, arg );
    switch(which_keyword(arg,"all",NULL)) {
     case -1:
	send_to_char("Lookup which skill or spell?\n\r", ch );
        break;
     case  0:
        if ( ( sn = skill_lookup( arg ) ) < 0 ) {
           send_to_char( "No such skill or spell.\n\r", ch );
	    return;
	}
        sprintf_to_char(ch, "Sn: %3d  Skill/spell: '%s'\n\r",
           sn, skill_name(sn,NULL) );
        break;
    case  1:
	for ( sn = 0; sn < MAX_SKILL; sn++ ) {
	    if ( !skill_exists(sn) )
		break;
	    sprintf_to_char(ch, "Sn: %3d  Skill/spell: '%s'\n\r",
		sn, skill_name(sn,NULL) );
	}
        break;
    }
}


void vnum_type(CHAR_DATA *ch,char *argument) {
    OBJ_INDEX_DATA *pObjIndex;
    int item_type=0;
    int vnum,nMatch;
    BUFFER *output;
    char s[MAX_STRING_LENGTH];
    char arg[MAX_STRING_LENGTH];
    bool global=FALSE;

    argument=one_argument(argument,arg);
    if (!str_cmp(arg,"global")) {
	global=TRUE;
	argument=one_argument(argument,arg);
    }

    item_type=table_find_name(arg,item_table);

    if (arg[0]==0 || item_type==NO_FLAG) {
	send_to_char("{yNo such type, usage is \"{Yvnum type [global] <type>{y\", where type is one of these:{x\n\r",ch);
	sprintf_to_char(ch,"%s\n\r",table_all(item_table));
	return;
    }

    output=new_buf();

    if (global) {
	sprintf(s,"Global searching objects of type {y%s{x\n\r",
	    table_find_value(item_type,item_table));
	add_buf(output,s);

	nMatch=0;
	for (vnum=0;vnum<max_vnum_obj;vnum++)
	    if ((pObjIndex=get_obj_index(vnum))!=NULL) {
		nMatch++;

		if (pObjIndex->item_type==item_type) {
		    sprintf(s,"%5d. %s\n\r",
			pObjIndex->vnum,
			pObjIndex->short_descr);
		    add_buf(output,s);
		}
	    }
    } else {
	for (vnum=ch->in_room->area->lvnum;vnum<ch->in_room->area->uvnum;vnum++)
	    if ((pObjIndex=get_obj_index(vnum))!=NULL)
		if (pObjIndex->item_type==item_type) {
		    sprintf(s,"%5d. %s\n\r",
			pObjIndex->vnum,
			pObjIndex->short_descr);
		    add_buf(output,s);
		}
    }

    page_to_char(buf_string(output),ch);
    free_buf(output);
}

//
// Xwhere
//
void do_owhere(CHAR_DATA *ch, char *argument )
{
    char buf[MAX_INPUT_LENGTH];
    BUFFER *buffer;
    OBJ_DATA *obj;
    OBJ_DATA *in_obj;
    CHAR_DATA *carrier;
    ROOM_INDEX_DATA *room;
    bool found;
    bool found_inlist;
    int number = 0, max_found;
    int vnum=0,i;

    char **output;
    int numoutputs=0;
    int *outputcounts;
    int highestcount=0;
    int spacecount=0;

    found = FALSE;
    number = 0;
    max_found = 100;

    if (argument[0]==0) {
	send_to_char("Find what?\n\r",ch);
	return;
    }

    // all the objects without a room
    if (!str_cmp(argument,"nowhere")) {
	buffer = new_buf();
	found=FALSE;
	number=0;
	for ( obj = object_list; obj != NULL; obj = obj->next ) {

	    for ( in_obj=obj; in_obj->in_obj!=NULL; in_obj=in_obj->in_obj )
		;

	    if ( in_obj->carried_by != NULL
	    &&   in_obj->carried_by->in_room != NULL)
		;
	    else if (in_obj->in_room!=NULL )
            	;
	    else {
		found = TRUE;
		number++;
		sprintf(buf,"%3d) %s{x is somewhere\n\r",number,string_trunc(obj->short_descr,30));
		add_buf(buffer,buf);
	    }
	}

	if (found)
	    page_to_char(buf_string(buffer),ch);
	else
	    send_to_char("No objects without rooms found.\n\r",ch);
	free_buf(buffer);
	return;
    }

    output=(char **)calloc(1,sizeof(char *)*max_found);
    outputcounts=(int *)calloc(1,sizeof(int)*max_found);

    vnum=0;
    if (is_number(argument))
	vnum=atoi(argument);

    for ( obj = object_list; obj != NULL; obj = obj->next ) {
        if ( !can_see_obj( ch, obj ))
	    continue;
	if (vnum!=0 && vnum!=obj->pIndexData->vnum && vnum!=obj->id)
	    continue;
	if (vnum==0 && (!is_name( argument, obj->name ) ||
			ch->level < obj->level))
            continue;

        found = TRUE;

        for ( in_obj = obj; in_obj->in_obj != NULL; in_obj = in_obj->in_obj )
	    ;
	carrier=in_obj?in_obj->carried_by:obj->carried_by;
	room=carrier?carrier->in_room:in_obj?in_obj->in_room:obj->in_room;

	if (carrier && !can_see(ch,carrier))
	    continue;

	strcpy(buf,string_trunc(obj->short_descr,40));
	sprintf(buf+strlen(buf),"{x is%s%s{x%s%s%s%s [room %d]",
	    in_obj?in_obj==obj?"":" inside ":"",
	    in_obj?in_obj==obj?"":string_trunc(in_obj->short_descr,40):"",
	    carrier?" carried by ":"",
	    carrier?NAME(carrier):"",
	    (room && !carrier)?" at ":"",
	    (room && !carrier)?room->name:"",
	    room?room->vnum:-1
	);

	found_inlist=FALSE;
	for (i=0;i<numoutputs;i++) {
	    if (!str_cmp(output[i],buf)) {
		outputcounts[i]++;
		highestcount=UMAX(outputcounts[i],highestcount);
		found_inlist=TRUE;
		break;
	    }
	}
	if (found_inlist)
	    continue;

	output[numoutputs]=(char *)calloc(1,strlen(buf)+1);
	strcpy(output[numoutputs],buf);
	outputcounts[numoutputs]=1;
	numoutputs++;

        if (++number >= max_found)
            break;
    }

    if ( !found )
        send_to_char( "Nothing like that in heaven or earth.\n\r", ch );
    else {
	buffer = new_buf();
	spacecount=fd_log10(highestcount);
	for (i=0;i<numoutputs;i++) {
	    if (outputcounts[i]==1)
		sprintf(buf,"%3d) %*s%s\n\r",
		    i+1,(spacecount==0?0:3)+spacecount,"",output[i]);
	    else
		sprintf(buf,"%3d) %*dx %s\n\r",
		    i+1,
		    1+spacecount,
		    outputcounts[i],
		    output[i]);
	    add_buf(buffer,buf);
	    free(output[i]);
	}
        page_to_char(buf_string(buffer),ch);
	free_buf(buffer);
    }

    free(output);
}



void do_rwhere(CHAR_DATA *ch, char *argument ) {
    char buf[MAX_INPUT_LENGTH];
    BUFFER *buffer;
    bool found;
    int i=0;
    ROOM_INDEX_DATA *pRoom;

    found = FALSE;

    if (argument[0] == '\0')
    {
	send_to_char("Find what?\n\r",ch);
	return;
    }

    buffer = new_buf();
    for (i=0;i<MAX_KEY_HASH;i++) {
	for (pRoom=room_index_hash[i];pRoom;pRoom=pRoom->next) {
	    if (pRoom->name[0] && !str_infix_nocolor(argument,pRoom->name)) {
		sprintf(buf,"%5d. %s\n\r",pRoom->vnum,pRoom->name);
		add_buf(buffer,buf);
		found=TRUE;
	    }
	}
    }

    if (!found)
	send_to_char("No room found.\n\r",ch);
    else
        page_to_char(buf_string(buffer),ch);

    free_buf(buffer);
}


void do_mwhere( CHAR_DATA *ch, char *argument )
{
    char buf[MAX_STRING_LENGTH];
    BUFFER *buffer;
    CHAR_DATA *victim;
    bool found;
    int count = 0;
    int vnum=0;

    if ( argument[0] == '\0' )
    {
	/* show non-mobs logged-in or linkdead */
	CHAR_DATA *vch;
	buffer = new_buf();
	for (vch=char_list;vch;vch=vch->next) {
	    if (IS_PC(vch) && vch->in_room && can_see(ch,vch)) {
		count++;
		if (vch->desc) {
		    if (vch->desc->original != NULL)
			sprintf(buf,"%3d) %s (in the body of %s) is in %s [%d]\n\r",
			    count,vch->desc->original->name,
			    vch->short_descr,
			    vch->in_room->name,vch->in_room->vnum);
		    else
			sprintf(buf,"%3d) %s is in %s [%d]\n\r",
			    count, vch->name,vch->in_room->name,
			    vch->in_room->vnum);
		} else {
		    sprintf(buf,"%3d) %s (linkdead) is in %s [%d]\n\r",
			count,vch->name,
			vch->in_room->name,vch->in_room->vnum);
		}
		add_buf(buffer,buf);
	    }
	}
        page_to_char(buf_string(buffer),ch);
	free_buf(buffer);
	return;
    }

    /* all the mobs without a room */
    if (!str_cmp(argument,"nowhere")) {
	buffer = new_buf();
	found=FALSE;
	count=0;
	for ( victim = char_list; victim != NULL; victim = victim->next ) {
	    if (victim->in_room==NULL) {
		found = TRUE;
		count++;
		sprintf( buf, "%3d) [%5d] %-28s %lx\n\r", count,
		    IS_NPC(victim) ? victim->pIndexData->vnum : 0,
		    IS_NPC(victim) ? victim->short_descr : victim->name,
		    (unsigned long)victim);
		add_buf(buffer,buf);
	    }
	}

	if (found)
	    page_to_char(buf_string(buffer),ch);
	else
	    send_to_char("No mobs without rooms found.\n\r",ch);
	free_buf(buffer);
	return;
    }

    if (is_number(argument))
	vnum=atoi(argument);
    found = FALSE;
    buffer = new_buf();
    for ( victim = char_list; victim != NULL; victim = victim->next )
    {
	if ( victim->in_room != NULL
	&&  (is_name( argument, victim->name )
	    || (vnum!=0 && (
		  (victim->pIndexData && victim->pIndexData->vnum==vnum) ||
		  victim->id==vnum)))
	&&   can_see( ch, victim ) )
	{
	    found = TRUE;
	    count++;
	    sprintf( buf, "%3d) [%5d] %-28s [%5d] %s\n\r", count,
		IS_NPC(victim) ? victim->pIndexData->vnum : 0,
		IS_NPC(victim) ? victim->short_descr : victim->name,
		victim->in_room->vnum,
		victim->in_room->name );
	    add_buf(buffer,buf);
	}
    }

    if ( !found )
	act( "You didn't find any $T.", ch, NULL, argument, TO_CHAR );
    else
    	page_to_char(buf_string(buffer),ch);

    free_buf(buffer);

    return;
}


//
// Xpwhere
//
void do_mpwhere(CHAR_DATA *ch, char *argument) {
    int	vnum,vnum_wanted,nMatch=0;
    MOB_INDEX_DATA *pMobIndex;
    bool found=FALSE;

    if (!is_number(argument)) {
	send_to_char("Usage: mpwhere <vnum>\n\r",ch);
	return;
    }

    vnum_wanted=atoi(argument);
    if (vnum_wanted<0) {
	send_to_char("<vnum> should be positive\n\r",ch);
	return;
    }

    for ( vnum=0; vnum<max_vnum_mob; vnum++ ) {
	if ( ( pMobIndex = get_mob_index( vnum ) ) != NULL ) {
	    nMatch++;
	    if (pMobIndex->mprog_vnum==vnum_wanted) {
		if (!found) {
		    found=TRUE;
		    send_to_char("{ymob   name\n\r",ch);
		    send_to_char("------------------------------{x\n\r",ch);
		}
		sprintf_to_char(ch,"%5d %s\n\r",
		    pMobIndex->vnum,pMobIndex->short_descr);
	    }
	}
    }
    if (!found)
	sprintf_to_char(ch,"No mobs with mobprog %d found.\n\r",vnum_wanted);
}


void do_opwhere(CHAR_DATA *ch, char *argument) {
    int	vnum,vnum_wanted,nMatch=0;
    OBJ_INDEX_DATA *pObjIndex;
    bool found=FALSE;

    if (!is_number(argument)) {
	send_to_char("Usage: opwhere <vnum>\n\r",ch);
	return;
    }

    vnum_wanted=atoi(argument);
    if (vnum_wanted<0) {
	send_to_char("<vnum> should be positive\n\r",ch);
	return;
    }

    for ( vnum=0; vnum<max_vnum_obj; vnum++ ) {
	if ( ( pObjIndex = get_obj_index( vnum ) ) != NULL ) {
	    nMatch++;
	    if (pObjIndex->oprog_vnum==vnum_wanted) {
		if (!found) {
		    found=TRUE;
		    send_to_char("{ymob   name\n\r",ch);
		    send_to_char("------------------------------{x\n\r",ch);
		}
		sprintf_to_char(ch,"%5d %s\n\r",
		    pObjIndex->vnum,pObjIndex->short_descr);
	    }
	}
    }
    if (!found)
	sprintf_to_char(ch,"No objects with objprog %d found.\n\r",vnum_wanted);
}

void do_rpwhere(CHAR_DATA *ch, char *argument) {
    int	vnum,vnum_wanted,nMatch=0;
    ROOM_INDEX_DATA *pRoomIndex;
    bool found=FALSE;

    if (!is_number(argument)) {
	send_to_char("Usage: rpwhere <vnum>\n\r",ch);
	return;
    }

    vnum_wanted=atoi(argument);
    if (vnum_wanted<0) {
	send_to_char("<vnum> should be positive\n\r",ch);
	return;
    }

    for ( vnum=0; vnum<max_vnum_room; vnum++ ) {
	if ( ( pRoomIndex = get_room_index( vnum ) ) != NULL ) {
	    nMatch++;
	    if (pRoomIndex->rprog_vnum==vnum_wanted) {
		if (!found) {
		    found=TRUE;
		    send_to_char("{yroom  name\n\r",ch);
		    send_to_char("------------------------------{x\n\r",ch);
		}
		sprintf_to_char(ch,"%5d %s\n\r",
		    pRoomIndex->vnum,pRoomIndex->name);
	    }
	}
    }
    if (!found)
	sprintf_to_char(ch,"No rooms with roomprog %d found.\n\r",vnum_wanted);
}

void do_apwhere(CHAR_DATA *ch, char *argument) {
    send_to_char("Not implemented yet, bugger mavetju about it\n\r",ch);
}

void do_pwhere(CHAR_DATA *ch, char *argument ) {
    char buf[MAX_INPUT_LENGTH];
    BUFFER *buffer;
    bool found;
    TCLPROG_CODE *pCode;
    MOB_INDEX_DATA *pMobIndex;
    OBJ_INDEX_DATA *pObjIndex;
    ROOM_INDEX_DATA *pRoomIndex;
    int vnumsearch=0;
    int vnum,vnumscan,i;

    found = FALSE;

    if (argument[0] == '\0')
    {
        send_to_char("pwhere <text|vnum>\n\r",ch);
        return;
    }
    if ((vnumsearch=is_number(argument))) {
	vnum=atoi(argument);
    } else {
	if (strlen(argument)<3) {
	    send_to_char("Please use at least 3 chars.\n\r",ch);
	    return;
	}
    }

    buffer = new_buf();
    for (i=0;i<MAX_KEY_HASH;i++) {
	pCode=mprog_index_hash[i];
	while (pCode) {
	    if (vnumsearch?(pCode->vnum==vnum):((pCode->title) && !str_infix_nocolor(argument,pCode->title))) {
		sprintf(buf,"mobprog  %5d [%s] %s.\n\r",
		    pCode->vnum,
		    pCode->area->name,
		    pCode->title);
		add_buf(buffer,buf);
		found=TRUE;
		for ( vnumscan=0; vnumscan<max_vnum_mob; vnumscan++ ) {
		    if ( ( pMobIndex = get_mob_index( vnumscan ) ) != NULL ) {
			if (pMobIndex->mprog_vnum==pCode->vnum) {
			    sprintf(buf," - %5d %s\n\r",vnumscan,pMobIndex->short_descr);
			    add_buf(buffer,buf);
			}
		    }
		}
	    }
	    pCode=pCode->next;
	}
    }
    for (i=0;i<MAX_KEY_HASH;i++) {
	pCode=oprog_index_hash[i];
	while (pCode) {
	    if (vnumsearch?(pCode->vnum==vnum):((pCode->title) && !str_infix_nocolor(argument,pCode->title))) {
		sprintf(buf,"objprog  %5d [%s] %s.\n\r",
		    pCode->vnum,
		    pCode->area->name,
		    pCode->title);
		add_buf(buffer,buf);
		found=TRUE;
		for ( vnumscan=0; vnumscan<max_vnum_obj; vnumscan++ ) {
		    if ( ( pObjIndex = get_obj_index( vnumscan ) ) != NULL ) {
			if (pObjIndex->oprog_vnum==pCode->vnum) {
			    sprintf(buf," - %5d %s\n\r",vnumscan,pObjIndex->short_descr);
			    add_buf(buffer,buf);
			}
		    }
		}
	    }
	    pCode=pCode->next;
        }
    }
    for (i=0;i<MAX_KEY_HASH;i++) {
	pCode=rprog_index_hash[i];
	while (pCode) {
	    if (vnumsearch?(pCode->vnum==vnum):((pCode->title) && !str_infix_nocolor(argument,pCode->title))) {
		sprintf(buf,"roomprog %5d [%s] %s.\n\r",
		    pCode->vnum,
		    pCode->area->name,
		    pCode->title);
		add_buf(buffer,buf);
		found=TRUE;
		for ( vnumscan=0; vnumscan<max_vnum_room; vnumscan++ ) {
		    if ( ( pRoomIndex = get_room_index( vnumscan ) ) != NULL ) {
			if (pRoomIndex->rprog_vnum==pCode->vnum) {
			    sprintf(buf," - %5d %s\n\r",vnumscan,pRoomIndex->name);
			    add_buf(buffer,buf);
			}
		    }
		}
	    }
	    pCode=pCode->next;
	}
    }

    if (!found)
        send_to_char("No program found.\n\r",ch);
    else
        page_to_char(buf_string(buffer),ch);

    free_buf(buffer);
}

