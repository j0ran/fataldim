//
// $Id: fd_data_tables.c,v 1.12 2008/03/16 10:11:23 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"

//
// reboot types table
//
const TABLE_TYPE reboot_table[]={
    {	"none",			REBOOT_NONE			},
    {	"reboot",		REBOOT_REBOOT			},
    {	"shutdown",		REBOOT_SHUTDOWN			},
    {	"copyover",		REBOOT_COPYOVER			}
};

//
// socket state types table
//
const TABLE_TYPE socket_table[]={
    {	"playing",		CON_PLAYING			},
    {	"get name",		CON_GET_NAME			},
    {	"get old password",	CON_GET_OLD_PASSWORD		},
    {	"confirm new name",	CON_CONFIRM_NEW_NAME		},
    {	"get new password",	CON_GET_NEW_PASSWORD		},
    {	"confirm new password",	CON_CONFIRM_NEW_PASSWORD	},
    {	"get new race",		CON_GET_NEW_RACE		},
    {	"get new sex",		CON_GET_NEW_SEX			},
    {	"get new class",	CON_GET_NEW_CLASS		},
    {	"get alignment",	CON_GET_ALIGNMENT		},
    {	"default choice",	CON_DEFAULT_CHOICE		},
    {	"gen group",		CON_GEN_GROUPS			},
    {	"pick weapon",		CON_PICK_WEAPON			},
    {	"read imotd",		CON_READ_IMOTD			},
    {	"read motd",		CON_READ_MOTD			},
    {	"break connect",	CON_BREAK_CONNECT		},
    {	"get email",		CON_GET_EMAIL			},
    {	"get ansi",		CON_GET_ANSI			},
    {	"get spec class",	CON_GET_SPEC_CLASS		},
    {	"specialize choice",	CON_SPECIALIZE_CHOICE		},
    {	"gen specgroups",	CON_GEN_SPECGROUPS		},
    {	"screen creation",	CON_SCREEN_CREATION		},
    {	"screen customize",	CON_SCREEN_CUSTOMIZE		},
    {	"screen specialize",	CON_SCREEN_SPECIALIZE		},
    {	"read newbieinfo",	CON_READ_NEWBIEINFO		},
#ifdef HAS_HTTP
    {	"http",			CON_HTTP			},
#endif
#ifdef HAS_POP3
    {	"pop3 get user",	CON_POP3_GET_USER		},
    {	"pop3 get pass",	CON_POP3_GET_PASS		},
    {	"pop3 connected",	CON_POP3_CONNECTED		},
#endif
#ifdef HAS_HTTP
    {	"http close",		CON_HTTP_CLOSE			},
#endif
    {	"copyover recover",	CON_COPYOVER_RECOVER		},
#ifdef HAS_ALTS
    {	"choose alt",		CON_CHOOSE_ALT			},
#endif
    {	"logging",		CON_LOGGING			},
    {	NULL,			0				}
};

//
// item type table
//
const TABLE_TYPE item_table[]={
    {	"light",		ITEM_LIGHT			},
    {	"scroll",		ITEM_SCROLL			},
    {	"wand",			ITEM_WAND			},
    {	"staff",		ITEM_STAFF			},
    {	"weapon",		ITEM_WEAPON			},
    {	"treasure",		ITEM_TREASURE			},
    {	"armor",		ITEM_ARMOR			},
    {	"potion",		ITEM_POTION			},
    {	"clothing",		ITEM_CLOTHING			},
    {	"furniture",		ITEM_FURNITURE			},
    {	"trash",		ITEM_TRASH			},
    {	"container",		ITEM_CONTAINER			},
    {	"drink",		ITEM_DRINK_CON			},
    {	"key",			ITEM_KEY			},
    {	"food",			ITEM_FOOD			},
    {	"money",		ITEM_MONEY			},
    {	"boat",			ITEM_BOAT			},
    {	"npc_corpse",		ITEM_CORPSE_NPC			},
    {	"pc_corpse",		ITEM_CORPSE_PC			},
    {	"fountain",		ITEM_FOUNTAIN			},
    {	"pill",			ITEM_PILL			},
    {	"protect",		ITEM_PROTECT			},
    {	"map",			ITEM_MAP			},
    {	"portal",		ITEM_PORTAL			},
    {	"warp_stone",		ITEM_WARP_STONE			},
    {	"room_key",		ITEM_ROOM_KEY			},
    {	"gem",			ITEM_GEM			},
    {	"jewelry",		ITEM_JEWELRY			},
#ifdef HAS_JUKEBOX
    {	"jukebox",		ITEM_JUKEBOX			},
#endif
    {	"quest",		ITEM_QUEST			},
    {	"clan",			ITEM_CLAN			},
    {	NULL,			0				}
};

//
// property name table
//
const TABLE_TYPE property_table[]={
    {	"undef",		PROPERTY_UNDEF			},
    {	"int",			PROPERTY_INT			},
    {	"long",			PROPERTY_LONG			},
    {	"bool",			PROPERTY_BOOL			},
    {	"string",		PROPERTY_STRING			},
    {	"char",			PROPERTY_CHAR			},
    {	NULL,			0,				}
};

//
// telnet options table
//
const TABLE_TYPE telnet_table[]={
#ifdef HAS_MCCP
    {	"mccp",			TELOPT_MCCP,			},
    {	"mccp2",		TELOPT_MCCP2,			},
#endif
    {	"mxp",			TELOPT_MXP,			},
    {	"msp",			TELOPT_MSP,			},
    {   "termtype",		TELOPT_TTYPE,			},
    {	"naws",			TELOPT_NAWS,			},
    {	"echo",			TELOPT_ECHO,			},
    {	"binary",		TELOPT_BINARY,			},
    {	"supress_go_ahead",	TELOPT_SGA,			},
    {	NULL,			0,				}
};


//
// sector types table
//
const TABLE_TYPE sector_table[]={
    {	"inside",		SECT_INSIDE,			},
    {	"city",			SECT_CITY,			},
    {	"field",		SECT_FIELD,			},
    {	"forest",		SECT_FOREST,			},
    {	"hills",		SECT_HILLS,			},
    {	"mountain",		SECT_MOUNTAIN,			},
    {	"water_swim",		SECT_WATER_SWIM,		},
    {	"water_noswim",		SECT_WATER_NOSWIM,		},
    {	"water_below",		SECT_WATER_BELOW,		},
    {	"air",			SECT_AIR,			},
    {	"desert",		SECT_DESERT,			},
    {	NULL,			0,				}
};

//
// sex types tables
//
const TABLE_TYPE sex_table[]={
   {	"none",			SEX_NEUTRAL			},
   {	"male",			SEX_MALE			},
   {	"female",		SEX_FEMALE			},
   {	"either",		SEX_EITHER			},
   {	NULL,			0				}
};


//
// liquid types table
//
const TABLE_TYPE liquid_table[]={
    {	"water",		0				},
    {	"beer",			1				},
    {	"red wine",		2				},
    {	"ale",			3				},
    {	"dark ale",		4				},
    {	"whisky",		5				},
    {	"lemonade",		6				},
    {	"firebreather",		7				},
    {	"local specialty",	8				},
    {	"slime mold juice",	9				},
    {	"milk",			10				},
    {	"tea",			11				},
    {	"coffee",		12				},
    {	"blood",		13				},
    {	"salt water",		14				},
    {	"coke",			15				},
    {	"root beer",		16				},
    {	"elvish wine",		17				},
    {	"white wine",		18				},
    {	"champagne",		19				},
    {	"mead",			20				},
    {	"rose wine",		21				},
    {	"benedictine wine",	22				},
    {	"vodka",		23				},
    {	"cranberry juice",	24				},
    {	"orange juice",		25				},
    {	"absinthe",		26				},
    {	"brandy",		27				},
    {	"aquavit",		28				},
    {	"schnapps",		29				},
    {	"icewine",		30				},
    {	"amontillado",		31				},
    {	"sherry",		32				},
    {	"framboise",		33				},
    {	"rum",			34				},
    {	"cordial",		35				},
    {	"tomato juice",		36				},
    {	"strawberry kiwi juice",37				},
    {	"alcohol-free beer",	38				},
    {	"soup",			39				},
    {	"sake",			40				},
    {	"liquid blood",		41				},
    {	"milk-n-honey",		42				},
    {	NULL,			0				}
};

//
// weapon type table
//
const TABLE_TYPE damagetype_table[]={
    {	"hit",			0				},
    {	"slice", 		1				},
    {   "stab",			2				},
    {	"slash",		3				},
    {	"whip",			4				},
    {   "claw",			5				},
    {	"blast",		6				},
    {   "pound",		7				},
    {	"crush",		8				},
    {   "grep",			9				},
    {	"bite",			10				},
    {   "pierce",		11				},
    {   "suction",		12				},
    {	"beating",		13				},
    {   "digestion",		14				},
    {	"charge",		15				},
    { 	"slap",			16				},
    {	"punch",		17				},
    {	"wrath",		18				},
    {	"magic",		19				},
    {   "divine power",		20				},
    {	"cleave",		21				},
    {	"scratch",		22				},
    {   "peck pierce",		23				},
    {   "peck bash",		24				},
    {   "chop",			25				},
    {   "sting",		26				},
    {   "smash",		27				},
    {   "shocking bite",	28				},
    {	"flaming bite",		29				},
    {	"freezing bite",	30				},
    {	"acidic bite", 		31				},
    {	"chomp",		32				},
    {  	"life drain",		33				},
    {   "thrust",		34				},
    {   "slime",		35				},
    {	"shock",		36				},
    {   "thwack",		37				},
    {   "flame",		38				},
    {   "chill",		39				},
    {   "smear",		40				},
    {   "crack",		41				},
    {   "song",			42				},
    {	NULL,			0				}
};

const TABLE_TYPE effect_where_table[]={
    {	"effects",		TO_EFFECTS	},
    {	"object",		TO_OBJECT	},
    {	"immune",		TO_IMMUNE	},
    {	"resist",		TO_RESIST	},
    {	"vuln",			TO_VULN		},
    {	"weapon",		TO_WEAPON	},
    {	"effects2",		TO_EFFECTS2	},
    {	"object2",		TO_OBJECT2	},
    {	"room1",		TO_ROOM1	},
    {	"room2",		TO_ROOM2	},
    {	"exit1",		TO_EXIT1	},
    {	"skill/spell",		TO_SKILLS	},
    {	"skill",		TO_SKILLS	}, /* extra to help builders */
    {	"spell",		TO_SKILLS	}, /* extra to help builders */
    {	NULL,			0		}
};

const TABLE_TYPE shoptype_table[] = {
    {   "normal",               SHOP_NORMAL	},
    {   "quest",                SHOP_QUEST	},
    {   NULL,                   0		} 
};

const TABLE_TYPE weather_sun_table[] = {
    {   "dark",		SUN_DARK	},
    {   "sunrise",	SUN_RISE	},
    {   "light",	SUN_LIGHT	},
    {   "sunset",	SUN_SET		},
    {	NULL,		0		}
};

const TABLE_TYPE weather_sky_table[] = {
    {   "cloudless",	SKY_CLOUDLESS	},
    {   "cloudy",	SKY_CLOUDY	},
    {   "raining",	SKY_RAINING	},
    {   "lightning",	SKY_LIGHTNING	},
    {	NULL,		0		}
};

const TABLE_TYPE questconsumption_table[] =
{
    {   "eat",                  QUESTCONS_EAT	},
    {   "drink",                QUESTCONS_DRINK	},
    {   NULL,                   0		}
};

const TABLE_TYPE questeffects_table[] =
{
    {   "level",                QUESTEFF_LEVEL		},
    {   "practices",            QUESTEFF_PRACTICES	},
    {   "trains",               QUESTEFF_TRAINS		},
    {   "exp",                  QUESTEFF_EXP		},
    {   "loner",                QUESTEFF_LONER		},
    {   "questpoints",          QUESTEFF_QUESTPOINTS	},
    {   NULL,                   0			}
};
