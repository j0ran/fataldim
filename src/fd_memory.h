//
// $Id: fd_memory.h,v 1.8 2002/08/09 20:33:52 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef INCLUDED_FD_MEMORY_H
#define INCLUDED_FD_MEMORY_H

//
// MAX_STRING is the size of the block allocated for permanent strings.
// If the mud aborts because of lack of memory, increase this value by
// 65536 (64Kb).
//
// 2002/08/09: MAX_STRING can be overruled by a value loaded by fd_conf.c:init_config()
#define	MAX_STRING	4915200		// 4800K
#define	MAX_PERM_BLOCK	131072		//  128K
#define	MAX_MEM_LIST	11

void	get_memory_overview	(BUFFER *buf);

void	new_string_space	(void);
char *	allocate_perm_string	(const char *string);
void *	alloc_perm		(int sMem);
void *	alloc_mem		(int sMem);
void	free_mem		(void *cMem,int sMem);
char *	str_dup			(const char *str);
void	free_string		(char *pstr);

#endif	// INCLUDED_FD_MEMORY_H
