//
// $Id: healer.h,v 1.1 2001/08/27 01:05:26 edwin Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef INCLUDED_HEALER_H
#define INCLUDED_HEALER_H

struct healerdata {
    CHAR_DATA *healer;
    CHAR_DATA *victim;
    CHAR_DATA *payer;
};

int		healer_cast_spell	(CHAR_DATA *healer,CHAR_DATA *victim,
					 CHAR_DATA *payer,int spellnr);
int		healer_diagnose		(CHAR_DATA *healer,CHAR_DATA *victim,
					 CHAR_DATA *payer,bool doheal,
					 bool silent);
int		healer_find_spell	(int gsn);
int		healer_cast_spell	(CHAR_DATA *healer,CHAR_DATA *victim,
					 CHAR_DATA *payer,int spellnr);
CHAR_DATA *	healer_find_healer	(CHAR_DATA *ch);
void		healer_event		(EVENT_DATA *event);

#endif	// INCLUDED_HEALER_H
