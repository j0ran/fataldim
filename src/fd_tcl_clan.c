//
// $Id: fd_tcl_clan.c,v 1.9 2002/12/25 15:25:29 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#define NEEDS_TCL
#include "merc.h"

#define TCL_CLAN_CMD(name) static int name(Tcl_Interp *interp,CLAN_INFO *clan,int objc,Tcl_Obj *CONST objv[])

TCL_CLAN_CMD(ClanCmdNotImpl);
TCL_CLAN_CMD(clan_name);
TCL_CLAN_CMD(clan_exists);
TCL_CLAN_CMD(clan_hall);
TCL_CLAN_CMD(clan_recall);
TCL_CLAN_CMD(clan_bankaccount);
TCL_CLAN_CMD(clan_members);

struct tcl_Clan_Command_Struct {
    char *cmd;
    int  (*func)(Tcl_Interp *,CLAN_INFO *,int,Tcl_Obj *CONST []);
};

static struct tcl_Clan_Command_Struct tclClanCommands[] = {
	{ "name", 		clan_name		},
	{ "exists", 		clan_exists		},
	{ "hall", 		clan_hall		},
	{ "recall", 		clan_recall		},
	{ "bankaccount", 	clan_bankaccount	},
	{ "members",		clan_members	 	},
	{ NULL,			ClanCmdNotImpl		}
};

int tclClanObjCmd (client, interp, objc, objv)
    ClientData client;			/* Not used. */
    Tcl_Interp *interp;			/* Current interpreter. */
    int objc;				/* Number of arguments. */
    Tcl_Obj *CONST objv[];		/* Argument values. */
{
    int index;
    CHAR_DATA *ch;
    CLAN_INFO *clan;

    // we know objv[0] is 'clan'          
    objc--; objv++;

    if (!(ch=tcl_parse_char_switches(progData->ch,&objc,&objv))) return TCL_ERROR;

    if (ch->clan==NULL)
	clan=NULL;
    else
	clan=ch->clan->clan_info;

    if (Tcl_GetIndexFromObjStruct(interp, objv[0], 
				  (char **)tclClanCommands, sizeof(struct tcl_Clan_Command_Struct), 
				  "clan command", 0, &index) != TCL_OK) {
	return TCL_ERROR;
    }

    return (tclClanCommands[index].func)(interp,clan,objc,objv);
}

TCL_CLAN_CMD(ClanCmdNotImpl) {
    Tcl_Obj *result;

    result=Tcl_NewStringObj("Clan command not implemented yet: ",-1);
    Tcl_AppendObjToObj(result,objv[0]);
    Tcl_SetObjResult(interp,result);

    return TCL_ERROR;
}
TCL_CLAN_CMD(clan_exists) {
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(clan!=NULL));
    return TCL_OK;
}

// name
TCL_CLAN_CMD(clan_name) {
    Tcl_SetObjResult(interp,Tcl_NewStringObj(clan?clan->name:"none",-1));
    return TCL_OK;
}

// hall
TCL_CLAN_CMD(clan_hall) {
    Tcl_SetObjResult(interp,Tcl_NewIntObj(clan?clan->hall:0));
    return TCL_OK;
}

// recall
TCL_CLAN_CMD(clan_recall) {
    Tcl_SetObjResult(interp,Tcl_NewIntObj(clan?clan->recall:0));
    return TCL_OK;
}

// bankaccount
TCL_CLAN_CMD(clan_bankaccount) {
    Tcl_SetObjResult(interp,Tcl_NewIntObj(clan?clan->coins:0));
    return TCL_OK;
}

// members
TCL_CLAN_CMD(clan_members) {
    CLANMEMBER_TYPE *member;
    RANK_INFO *rankwanted;
    int membertype=-1;
    Tcl_Obj *result;

    static tclOptionList *objRanks[]={
	"normal", "elite", "master", "knight", "leader",
	(char *)NULL};

    if (objc==2)
	if (Tcl_GetIndexFromObj(interp, objv[1], objRanks, "rank", 0, &membertype) != TCL_OK)
	    return TCL_ERROR;

    if (clan==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("",-1));
	return TCL_OK;
    }
    result=Tcl_NewListObj(0,NULL);

    if (membertype==-1) {
	for (member=clan->members;member!=NULL;member=member->next_member)
		Tcl_ListObjAppendElement(interp,result,Tcl_NewStringObj(member->playername,-1));
    } else {
	rankwanted=get_rank_by_name((char *)objRanks[membertype]);
	for (member=clan->members;member!=NULL;member=member->next_member)
	    if (member->rank_info==rankwanted)
		Tcl_ListObjAppendElement(interp,result,Tcl_NewStringObj(member->playername,-1));
    }
    Tcl_SetObjResult(interp,result);
    return TCL_OK;
}
