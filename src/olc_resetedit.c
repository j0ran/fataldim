//
// $Id: olc_resetedit.c,v 1.8 2007/10/12 19:46:32 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"
#include "olc.h"
#include "interp.h"
#include "db.h"

//
// RESET EDITOR
//

MOB_INDEX_DATA *display_reset( CHAR_DATA *ch,RESET_DATA *pReset,MOB_INDEX_DATA *pMob,int line)
{
    OBJ_INDEX_DATA  *pObj;
    MOB_INDEX_DATA  *pMobIndex;
    OBJ_INDEX_DATA  *pObjIndex;
    OBJ_INDEX_DATA  *pObjToIndex;
    ROOM_INDEX_DATA *pRoomIndex;
    char 		final [ MAX_STRING_LENGTH ];
    char                buf [ MAX_STRING_LENGTH ];

    final[0] = '\0';
    if (line)
        sprintf( final, "[%2d] ", line );
    else
        send_to_char (
                    " Loads    Description       Location         Vnum   Max(L/G)  Description"
                    "\n\r"
                    "======== ============= =================== ======== ========= ==========="
                    "\n\r", ch );

    switch ( pReset->command )
    {
    default:
        sprintf( buf, "Bad reset command: %c.", pReset->command );
        strcat( final, buf );
        break;

    case 'M':
        if ( !( pMobIndex = get_mob_index( pReset->arg1 ) ) )
        {
            sprintf( buf, "Load Mobile - Bad Mob %d\n\r", pReset->arg1 );
            strcat( final, buf );
            break;
        }

        if ( !( pRoomIndex = get_room_index( pReset->arg3 ) ) )
        {
            sprintf( buf, "Load Mobile - Bad Room %d\n\r", pReset->arg3 );
            strcat( final, buf );
            break;
        }

        pMob = pMobIndex;
        sprintf( buf, "M[%5d] %-13.13s {xin room             R[%5d] [%3d/%3d] %-15.15s {x\n\r",
                 pReset->arg1, pMob->short_descr,pReset->arg3,
                 pReset->arg4,pReset->arg2, pRoomIndex->name );
        strcat( final, buf );

        /*
     * Check for pet shop.
     * -------------------
     */
    {
        ROOM_INDEX_DATA *pRoomIndexPrev;

        pRoomIndexPrev = get_room_index( pRoomIndex->vnum - 1 );
        if ( pRoomIndexPrev
             && STR_IS_SET(pRoomIndexPrev->strbit_room_flags,ROOM_PET_SHOP ))
            final[5] = 'P';
    }

        break;

    case 'O':
        if ( !( pObjIndex = get_obj_index( pReset->arg1 ) ) )
        {
            sprintf( buf, "Load Object - Bad Object %d\n\r",
                     pReset->arg1 );
            strcat( final, buf );
            break;
        }

        pObj       = pObjIndex;

        if ( !( pRoomIndex = get_room_index( pReset->arg3 ) ) )
        {
            sprintf( buf, "Load Object - Bad Room %d\n\r", pReset->arg3 );
            strcat( final, buf );
            break;
        }

        sprintf( buf, "O[%5d] %-13.13s {xin room             "
                      "R[%5d] [%3d/%3d] %-15.15s {x\n\r",
                 pReset->arg1, pObj->short_descr,
                 pReset->arg3,pReset->arg4,pReset->arg2, pRoomIndex->name );
        strcat( final, buf );

        break;

    case 'P':
        if ( !( pObjIndex = get_obj_index( pReset->arg1 ) ) )
        {
            sprintf( buf, "Put Object - Bad Object %d\n\r",
                     pReset->arg1 );
            strcat( final, buf );
            break;
        }

        pObj       = pObjIndex;

        if ( !( pObjToIndex = get_obj_index( pReset->arg3 ) ) )
        {
            sprintf( buf, "Put Object - Bad To Object %d\n\r",
                     pReset->arg3 );
            strcat( final, buf );
            break;
        }

        sprintf( buf,
                 "O[%5d] %-13.13s {xinside              O[%5d] [%3d/%3d] %-15.15s {x\n\r",
                 pReset->arg1,
                 pObj->short_descr,
                 pReset->arg3,
                 pReset->arg4,
                 pReset->arg2,
                 pObjToIndex->short_descr );
        strcat( final, buf );

        break;

    case 'G':
    case 'E':
        if ( !( pObjIndex = get_obj_index( pReset->arg1 ) ) )
        {
            sprintf( buf, "Give/Equip Object - Bad Object %d\n\r",
                     pReset->arg1 );
            strcat( final, buf );
            break;
        }

        pObj       = pObjIndex;

        if ( !pMob )
        {
            sprintf( buf, "Give/Equip Object - No Previous Mobile\n\r" );
            strcat( final, buf );
            break;
        }

        sprintf( buf,
                 "O[%5d] %-13.13s {x%-19.19s {xM[%5d] [    %3d] %-15.15s {x\n\r",
                 pReset->arg1,
                 pObj->short_descr,
                 (pReset->command == 'G') ?
                     option_find_value(WEAR_NONE,wear_loc_options)
                   : option_find_value(pReset->arg3,wear_loc_options),
                 pMob->vnum,
                 pReset->arg2,
                 pMob->short_descr );

        strcat( final, buf );

        break;

    case 'R':
        if ( !( pRoomIndex = get_room_index( pReset->arg1 ) ) )
        {
            sprintf( buf, "Randomize Exits - Bad Room %d\n\r",
                     pReset->arg1 );
            strcat( final, buf );
            break;
        }

        sprintf( buf, "R[%5d] Exits are randomized in %s\n\r",
                 pReset->arg1, pRoomIndex->name );
        strcat( final, buf );

        break;
    }

    send_to_char( final, ch );

    return pMob;
}

void display_resets( CHAR_DATA *ch )
{
    ROOM_INDEX_DATA     *pRoom;
    RESET_DATA          *pReset;
    MOB_INDEX_DATA      *pMob=NULL;
    int                 iReset = 0;

    EDIT_ROOM(ch, pRoom);

    send_to_char (
                " No.  Loads    Description       Location         Vnum   Max(L/G)  Description"
                "\n\r"
                "==== ======== ============= =================== ======== ========= ==========="
                "\n\r", ch );

    for ( pReset = pRoom->reset_first; pReset; pReset = pReset->next )
        pMob=display_reset(ch,pReset,pMob,++iReset);

    return;
}

bool	resetedit_show(CHAR_DATA *ch,char *argument) {
    if ( ch->in_room->reset_first ) {
        send_to_char(
                    "Resets: M = mobile, R = room, O = object, "
                    "P = pet, S = shopkeeper\n\r", ch );
        display_resets( ch );
    } else
        send_to_char( "No resets in this room.\n\r", ch );

    return TRUE;
}


bool	resetedit_create(CHAR_DATA *ch,char *argument) {
    if (argument[0]=='\0') {
        send_to_char("ResetEdit: Usage: create <#> ...\n\r",ch);
        return FALSE;
    }

    return TRUE;
}

//
// general low level reset functions
//
RESET_DATA *reset_find_nth(ROOM_INDEX_DATA *room,int i) {
    RESET_DATA *reset;

    if (room->reset_first==NULL)
        return NULL;

    if (i<0)
        return NULL;

    if (i==0)
        return room->reset_first;

    reset=room->reset_first;
    while (reset!=NULL) {
        if (i==0)
            break;
        reset=reset->next;
        i--;
    }

    return reset;
}

void reset_from_room(ROOM_INDEX_DATA *room,RESET_DATA *reset) {
    RESET_DATA *preset;

    if (room==NULL) {
        bugf("reset_from_room(): room=NULL");
        return;
    }
    if (reset==NULL) {
        bugf("reset_from_room(): reset=NULL");
        return;
    }

    // if the reset is the first in the room, do the quick work
    if (room->reset_first==reset) {
        room->reset_first=reset->next;
        if (room->reset_last==reset)
            room->reset_last=NULL;
        reset->next=NULL;
        return;
    }

    // find the reset before the one to be removed
    preset=room->reset_first;
    while (preset!=NULL) {
        if (preset->next==reset)
            break;
        preset=preset->next;
    }

    if (preset==NULL) {
        bugf("reset_from_room(): reset not in room");
        return;
    }

    preset->next=reset->next;
    reset->next=NULL;
    if (room->reset_last==reset)
        room->reset_last=preset;
}

void reset_to_room_after(ROOM_INDEX_DATA *room,RESET_DATA *reset,RESET_DATA *afterthis) {
    if (room==NULL) {
        bugf("reset_to_room_after(): room=NULL");
        return;
    }
    if (reset==NULL) {
        bugf("reset_to_room_after(): reset=NULL");
        return;
    }

    // put it in front of the row
    if (afterthis==NULL) {
        reset->next=room->reset_first;
        room->reset_first=reset;
        if (room->reset_last==NULL)
            room->reset_last=reset;
        return;
    }

    // or add it after the given reset
    reset->next=afterthis->next;
    afterthis->next=reset;
    if (room->reset_last==afterthis)
        room->reset_last=reset;
}

bool	resetedit_swap(CHAR_DATA *ch,char *argument) {
    char arg[MIL];
    int i1,i2;
    RESET_DATA *prereset1,*prereset2;
    RESET_DATA *reset1,*reset2;
    ROOM_INDEX_DATA *proom;

    EDIT_RESET(ch,proom);

    if (argument[0]=='\0') {
        send_to_char("ResetEdit: Usage: swap <#> <#>\n\r",ch);
        return FALSE;
    }

    if (proom->reset_first==NULL) {
        send_to_char("ResetEdit: There are no resets in this room.\n\r",ch);
        return FALSE;
    }

    argument=one_argument(argument,arg);
    if (!is_number(arg)) {
        resetedit_swap(ch,"");
        return FALSE;
    }
    i1=atoi(arg);

    argument=one_argument(argument,arg);
    if (!is_number(arg)) {
        resetedit_swap(ch,"");
        return FALSE;
    }
    i2=atoi(arg);

    if (i1==i2) {
        send_to_char("ResetEdit: Ok, if that's what you want...\n\r",ch);
        return FALSE;
    }

    // find the entries in the list
    reset1=reset_find_nth(proom,i1);
    reset2=reset_find_nth(proom,i2);

    if (reset1==NULL || reset2==NULL) {
        send_to_char("ResetEdit: Not that much resets in this room!\n\r",ch);
        return FALSE;
    }

    // find the previous resets
    prereset1=reset_find_nth(proom,i1-1);
    prereset2=reset_find_nth(proom,i2-1);

    // disconnect the resets from the room
    reset_from_room(proom,reset1);
    reset_from_room(proom,reset2);

    // switch them
    reset_to_room_after(proom,reset1,prereset2);
    reset_to_room_after(proom,reset2,prereset1);

    sprintf_to_char(ch,"ResetEdit: Swapped resets %d and %d.\n\r",i1,i2);

    return TRUE;
}

bool	resetedit_move(CHAR_DATA *ch,char *argument) {
    char arg[MIL];
    int i1,i2,i;
    RESET_DATA *prereset1;
    RESET_DATA *reset1,*reset2,*lastreset,*reset;
    ROOM_INDEX_DATA *proom;

    EDIT_RESET(ch,proom);

    if (argument[0]== '\0') {
        send_to_char("ResetEdit: Usage: move <from#> <to#>\n\r",ch);
        return FALSE;
    }

    if (proom->reset_first==NULL) {
        send_to_char("ResetEdit: There are no resets in this room.\n\r",ch);
        return FALSE;
    }

    argument=one_argument(argument,arg);
    if (!is_number(arg)) {
        resetedit_move(ch,"");
        return FALSE;
    }
    i1=atoi(arg);

    argument=one_argument(argument,arg);
    if (!is_number(arg)) {
        resetedit_move(ch,"");
        return FALSE;
    }
    i2=atoi(arg);

    // find the entries in the list
    i=1;
    reset1=NULL;
    reset=proom->reset_first;
    while (reset!=NULL) {
        if (i==i1)
            reset1=reset;
        if (i==i2)
            reset2=reset;
        lastreset=reset;
        reset=reset->next;
        i++;
    }

    if (reset1==NULL) {
        sprintf_to_char(ch,"ResetEdit: only %d resets in this room!\n\r",i-1);
        return FALSE;
    }

    if (reset1==reset2 || ( reset2==NULL && reset1==lastreset ) ) {
        send_to_char("Ok.\n\r",ch);
        return TRUE;
    }

    // find the previous entries in the list and disconnect it.
    prereset1=NULL;
    if (reset1!=proom->reset_first) {
        prereset1=proom->reset_first;
        while (prereset1->next!=reset1)
            prereset1=prereset1->next;
    }

    if (prereset1==NULL)
        proom->reset_first=reset1->next;
    else
        prereset1->next=reset1->next;
    reset1->next=NULL;

    // find the place to move the reset to and do it.
    if (reset2==proom->reset_first) {
        reset1->next=proom->reset_first;
        proom->reset_first=reset1;
    } else if (reset2==NULL) {
        lastreset->next=reset1;
    } else {
        reset=proom->reset_first;
        while (reset->next!=reset2)
            reset=reset->next;
        reset1->next=reset->next;
        reset->next=reset1;
    }

    // fix reset_last
    reset1=proom->reset_first;
    while (reset1->next!=NULL)
        reset1=reset1->next;
    proom->reset_last=reset1;

    sprintf_to_char(ch,"ResetEdit: Moved reset %d to %d.\n\r",i1,i2);

    return TRUE;
}

bool	resetedit_change(CHAR_DATA *ch,char *argument) {
    if (argument[0] == '\0') {
        send_to_char("ResetEdit: Usage: change <#> ...\n\r",ch);
        return FALSE;
    }

    return TRUE;
}

bool	resetedit_insert(CHAR_DATA *ch,char *argument) {
    if (argument[0] == '\0') {
        send_to_char("ResetEdit: Usage: insert <#> ...\n\r",ch);
        return FALSE;
    }

    return TRUE;
}

bool	resetedit_delete(CHAR_DATA *ch,char *argument) {
    char arg[MIL];
    ROOM_INDEX_DATA *pRoom=ch->in_room;
    RESET_DATA *pReset;
    int insert_loc;

    argument=one_argument(argument,arg);
    insert_loc=atoi(arg);

    if (arg[0]==0) {
        send_to_char("ResetEdit: Usage: delete <#>\n\r",ch);
        return FALSE;
    }

    if ( !pRoom->reset_first ) {
        send_to_char( "ResetEdit: No resets in this room.\n\r", ch );
        return FALSE;
    }

    if ( insert_loc-1 <= 0 ) {
        pReset = pRoom->reset_first;
        pRoom->reset_first = pRoom->reset_first->next;
        if ( !pRoom->reset_first )
            pRoom->reset_last = NULL;
    } else {
        int iReset = 0;
        RESET_DATA *prev = NULL;

        for ( pReset = pRoom->reset_first;
              pReset!=NULL;
              pReset = pReset->next ) {
            if ( ++iReset == insert_loc )
                break;
            prev = pReset;
        }

        if ( !pReset ) {
            send_to_char( "ResetEdit: Reset not found.\n\r", ch );
            return FALSE;
        }

        if ( prev )
            prev->next = prev->next->next;
        else
            pRoom->reset_first = pRoom->reset_first->next;

        for ( pRoom->reset_last = pRoom->reset_first;
              pRoom->reset_last->next;
              pRoom->reset_last = pRoom->reset_last->next );
    }

    free_reset( pReset );
    SET_BIT(pRoom->area->area_flags,AREA_CHANGED);
    sprintf_to_char(ch, "ResetEdit: Reset %d deleted.\n\r", insert_loc );
    return TRUE;
}
