// $Id: olc_medit.c,v 1.15 2006/08/25 09:43:20 jodocus Exp $
/***************************************************************************
 *  File: olc.c                                                            *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 *                                                                         *
 *  This code was freely distributed with the The Isles 1.1 source code,   *
 *  and has been used here for OLC - OLC would not be what it is without   *
 *  all the previous coders who released their source code.                *
 *                                                                         *
 ***************************************************************************/

#include "merc.h"
#include "olc.h"
#include "interp.h"
#include "db.h"

/*
 * Mobile Editor Functions.
 */

char *mob_flag_string( const OPTION_TYPE *options,long setflags,long raceflags,long clearflags) {
    static char buf[MAX_STRING_LENGTH];
    char *s;

    raceflags=raceflags&(~clearflags);

    buf[0]='\0';
    strcat(buf,"[");
    if (setflags) strcat(buf,option_string_from_long(setflags,options));
    for (s=buf;*s;s++) *s=toupper(*s);
    if (raceflags) {
	if (setflags) strcat(buf," ");
	strcat(buf,option_string_from_long(raceflags,options));
    }
    strcat(buf,"]");
    if (strlen(buf)==2) return "[none]";
    if (clearflags) {
	strcat(buf," not [");
	strcat(buf,option_string_from_long(clearflags,options));
	strcat(buf,"]");
    }

    return buf;
}

char *mob_string_flag_string( const OPTION_TYPE *options,char *p_setflags,const char *p_raceflags,char *p_clearflags) {
    static char buf[MAX_STRING_LENGTH];
    char setflags[MAX_FLAGS];
    char raceflags[MAX_FLAGS];
    char clearflags[MAX_FLAGS];
    char nclearflags[MAX_FLAGS];
    char *s;

    STR_COPY_STR(setflags,p_setflags,MAX_FLAGS);
    STR_COPY_STR(raceflags,p_raceflags,MAX_FLAGS);
    STR_COPY_STR(clearflags,p_clearflags,MAX_FLAGS);
    STR_COPY_STR(nclearflags,p_clearflags,MAX_FLAGS);

    STR_NOT_STR(nclearflags,MAX_FLAGS);
    STR_AND_STR(raceflags,nclearflags,MAX_FLAGS);

    buf[0]='\0';
    strcat(buf,"[");
    if (!STR_SAME_STR(setflags,ZERO_FLAG,MAX_FLAGS))
	strcat(buf,option_string(setflags,options));
    for (s=buf;*s;s++) *s=toupper(*s);
    if (!STR_SAME_STR(raceflags,ZERO_FLAG,MAX_FLAGS)) {
	if (!STR_SAME_STR(setflags,ZERO_FLAG,MAX_FLAGS)) strcat(buf," ");
	strcat(buf,option_string(raceflags,options));
    }
    strcat(buf,"]");
    if (strlen(buf)==2) return "[none]";
    if (!STR_SAME_STR(clearflags,ZERO_FLAG,MAX_FLAGS)) {
	strcat(buf," not [");
	strcat(buf,option_string(clearflags,options));
	strcat(buf,"]");
    }

    return buf;
}

long mob_flag_toggle(char *arg,const OPTION_TYPE *options,long *setflags,long raceflags,long *clearflags)
{
    char word[MAX_INPUT_LENGTH];
    long bit;

    for (; ;)
    {
        arg = one_argument( arg, word );

        if ( word[0] == '\0' )
	    break;

        if ( ( bit = option_find_name( word, options,FALSE ) ) != NO_FLAG )
        {
	    if(IS_SET(raceflags,bit)) TOGGLE_BIT(*clearflags,bit);
	    else TOGGLE_BIT(*setflags,bit);
        }
    }

    return ( *setflags | raceflags ) & ~(*clearflags);
}

char *mob_flag_toggle_string(char *arg,const OPTION_TYPE *options,char *setflags,const char *raceflags,char *clearflags,char *flagstobealtered)
{
    char word[MAX_INPUT_LENGTH];
    long bit;

    for (; ;)
    {
        arg = one_argument( arg, word );

        if ( word[0] == '\0' )
	    break;

        if ( ( bit = option_find_name( word, options,FALSE ) ) != NO_FLAG )
        {
	    if (STR_IS_SET(raceflags,bit))
		STR_TOGGLE_BIT(clearflags,bit);
	    else
		STR_TOGGLE_BIT(setflags,bit);
        }
    }

    STR_COPY_STR(flagstobealtered,setflags,MAX_FLAGS);
    STR_OR_STR(flagstobealtered,raceflags,MAX_FLAGS);
    STR_COPY_STR(word,clearflags,MAX_FLAGS);
    STR_NOT_STR(word,MAX_FLAGS);
    STR_AND_STR(flagstobealtered,word,MAX_FLAGS);
    return flagstobealtered;
}

#ifdef NEEDTHISLATER
void fix_mprog_flag( MOB_INDEX_DATA *pMob)
{
    TCLPROG_LIST *pList;

    pMob->mprog_flags=0;

    for(pList=pMob->mprogs;pList;pList=pList->next)
	pMob->mprog_flags|=pList->trig_type;
}
#endif

bool medit_show( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;
    char tmp1[MAX_STRING_LENGTH];

    EDIT_MOB(ch, pMob);

    sprintf_to_char(ch,
	"{yName:{x        [%s] %s\n\r{yArea:{x        [%5d] [v%d] %s\n\r",
	pMob->player_name,
	pMob->deleted?"{Rdeleted{x":"",
	!pMob->area ? -1        : pMob->area->vnum,
	!pMob->area ? 0		: pMob->area->version,
	!pMob->area ? "No Area" : pMob->area->name );

    sprintf_to_char(ch,
    "{yVnum:{x        [%5d]                   {yCount:{x            [%d]\n\r"
    "{ySex:{x         [%-6s]                  {yRace:{x             [%s]\n\r",
	pMob->vnum,pMob->count,
	table_find_value(pMob->sex,sex_table),
	race_table[pMob->race].name );

    sprintf_to_char(ch,
	"{yStart Pos:{x   [%-5s]                   {yDefault Pos:{x      [%-5s]\n\r",
	position_table[pMob->start_pos].short_name,
	position_table[pMob->default_pos].short_name );

    sprintf_to_char(ch,
	"{yLevel:{x       [%2d]                      {yAlign:{x            [%4d]\n\r",
	pMob->level,       pMob->alignment );

    sprintf_to_char( ch,
	"{yWealth:{x      [%6d]                  {yMaterial:{x         [%s]\n\r",
	(int)pMob->wealth,pMob->material );

    sprintf_to_char( ch,
	"{yHitroll:{x     [%2d]                      {yDamage type:{x      [%s]\n\r"
	"{ySize:{x        [%10s]              {yGroup:{x            [%d]\n\r"
	"{yDice:{x        {yHit:{x [%dd%d+%d]   {yMana:{x [%dd%d+%d]   {yDamage:{x [%dd%d+%d]\n\r",
	pMob->hitroll,
	table_find_value(pMob->dam_type,damagetype_table),
	size_table[pMob->size].name,
	pMob->group,
	pMob->hit[0],pMob->hit[1],pMob->hit[2],
	pMob->mana[0],pMob->mana[1],pMob->mana[2],
	pMob->damage[0],pMob->damage[1],pMob->damage[2] );

    sprintf_to_char( ch,
	"{yAC:{x          {yPierce:{x [%2d]   {yBash:{x [%2d]   {ySlash:{x [%2d]   {yExotic:{x [%2d]\n\r",
	pMob->ac[AC_PIERCE],pMob->ac[AC_BASH],
	pMob->ac[AC_SLASH],pMob->ac[AC_EXOTIC] );

    sprintf_to_char( ch, "{yAct:{x         %s\n\r",
	mob_string_flag_string( act_options,
	    pMob->strbit_act_read,
	    race_table[pMob->race].strbit_act,
	    pMob->strbit_act_saved ) );

    sprintf_to_char( ch, "{yOffensive:{x   %s\n\r",
	mob_string_flag_string( off_options,
	    pMob->strbit_off_read,
	    race_table[pMob->race].strbit_off,
	    pMob->strbit_off_saved ) );
    sprintf_to_char( ch, "{yImmune:{x      %s\n\r",
	mob_string_flag_string( imm_options,
	    pMob->strbit_imm_read,
	    race_table[pMob->race].strbit_imm,
	    pMob->strbit_imm_saved ) );

    sprintf_to_char( ch, "{yResistance:{x  %s\n\r",
	mob_string_flag_string( res_options,
	    pMob->strbit_res_read,
	    race_table[pMob->race].strbit_res,
	    pMob->strbit_res_saved ) );
    sprintf_to_char( ch, "{yVulnrable:{x   %s\n\r",
	mob_string_flag_string( vuln_options,
	    pMob->strbit_vuln_read,
	    race_table[pMob->race].strbit_vuln,
	    pMob->strbit_vuln_saved ) );

    sprintf_to_char( ch, "{yAffected by:{x %s",
	mob_string_flag_string( effect_options,
	    pMob->strbit_affected_read,
	    race_table[pMob->race].strbit_eff,
	    pMob->strbit_affected_saved ) );
    sprintf_to_char( ch, " %s\n\r",
	mob_string_flag_string( effect2_options,
	    pMob->strbit_affected_read2,
	    race_table[pMob->race].strbit_eff2,
	    pMob->strbit_affected_saved2 ) );

    sprintf_to_char( ch, "{yForm:{x        %s\n\r",
	mob_flag_string( form_options,
	    pMob->form_mem[0],
	    race_table[pMob->race].form,
	    pMob->form_mem[1] ) );
    sprintf_to_char( ch, "{yParts:{x       %s\n\r",
	mob_flag_string( part_options,
	    pMob->parts_mem[0],
	    race_table[pMob->race].parts,
	    pMob->parts_mem[1] ) );

    if ( pMob->spec_fun ) {
	sprintf_to_char( ch, "{ySpec fun:{x    [%s]\n\r",  spec_string( pMob->spec_fun ) );
    }

    if ( pMob->pueblo_picture[0] ) {
	sprintf_to_char( ch, "{yPicture:{x     %s\n\r",
	    url(tmp1,pMob->pueblo_picture,pMob->area,TRUE));
    }

    if ( pMob->extra_descr )
    {
        EXTRA_DESCR_DATA *ed;

        send_to_char( "{yEx desc kwd:{x ", ch );

        for ( ed = pMob->extra_descr; ed; ed = ed->next )
        {
            send_to_char( "[", ch );
            send_to_char( ed->keyword, ch );
            send_to_char( "]", ch );
        }

        send_to_char( "\n\r", ch );
    }

    sprintf_to_char( ch, "{yShort descr:{x %s\n\r{yLong descr:{x\n\r%s",
	pMob->short_descr,
	pMob->long_descr );

    sprintf_to_char( ch, "{yDescription:{x\n\r%s", pMob->description );

    if (pMob->mprog_vnum!=0) {
	TCLPROG_CODE *mProg=get_mprog_index(pMob->mprog_vnum);

	if (mProg) {
	    sprintf_to_char(ch,
		"{yProg instance:{x [%d] %s\n\r",
		pMob->mprog_vnum,mProg->title);
	}
    }
    else send_to_char("{yProg instance:{x none\n\r",ch);

    if ( pMob->pShop )
    {
	SHOP_DATA *pShop;
	int iTrade;

	pShop = pMob->pShop;

	sprintf_to_char( ch,
	    "Shop data for [%5d]:\n\r"
	    "  Markup for purchaser: %d%%\n\r"
	    "  Markdown for seller:  %d%%\n\r",
	    pShop->keeper, pShop->profit_buy, pShop->profit_sell );
	sprintf_to_char( ch,
	    "  Hours: %d to %d.\n\r"
	    "  Class: %s\n\r",
	    pShop->open_hour, pShop->close_hour,
	    table_find_value(pShop->type,shoptype_table) );

	for ( iTrade = 0; iTrade < MAX_TRADE; iTrade++ )
	{
	    if ( pShop->buy_type[iTrade] != 0 )
	    {
		if ( iTrade == 0 ) {
		    send_to_char( "  Number Trades Type\n\r", ch );
		    send_to_char( "  ------ -----------\n\r", ch );
		}
		sprintf_to_char( ch, "  [%4d] %s\n\r", iTrade,
		    table_find_value( pShop->buy_type[iTrade], item_table ) );
	    }
	}
    }

    show_properties(ch,pMob->property,"perm");

    return FALSE;
}



bool medit_create( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;
    AREA_DATA *pArea;
    int  value;
    int  iHash;

    if (argument[0]==0) {
	for (value=ch->in_room->area->lvnum;
	    value<=ch->in_room->area->uvnum;value++)
	    if ( !get_mob_index( value ) )
		break;
	if (value>ch->in_room->area->uvnum) {
	    send_to_char("No free vnums found in this area.\n\r",ch);
	    return FALSE;
	}
    } else
	value = atoi( argument );

    /* OLC 1.1b */
    if ( value <= 0 || value >= MAX_VNUMS )
    {
	char output[MAX_STRING_LENGTH];

	sprintf( output, "Syntax:  create [0 < vnum < %d]\n\r", MAX_VNUMS );
	send_to_char( output, ch );
	return FALSE;
    }

    pArea = get_vnum_area( value );

    if ( !pArea )
    {
	send_to_char( "MEdit:  That vnum is not assigned to an area.\n\r", ch );
	return FALSE;
    }

    if ( !IS_BUILDER( ch, pArea ) )
    {
	send_to_char( "MEdit:  Vnum in an area you cannot build in.\n\r", ch );
	return FALSE;
    }

    if ( get_mob_index( value ) )
    {
	send_to_char( "MEdit:  Mobile vnum already exists.\n\r", ch );
	return FALSE;
    }

    pMob			= new_mob_index();
    pMob->vnum			= value;
    pMob->area			= pArea;
    pArea->vnum_mobs_in_use++;

    if ( value > max_vnum_mob )
	max_vnum_mob = value;

    if (ch->desc->editor == ED_MOBPROG) {
	TCLPROG_CODE *pCode2;

	pCode2=(TCLPROG_CODE *)ch->desc->pEdit;
	pCode2->edit=FALSE;
    }

    STR_ZERO_BIT(pMob->strbit_act,MAX_FLAGS);
    STR_SET_BIT(pMob->strbit_act,ACT_IS_NPC);
    iHash			= value % MAX_KEY_HASH;
    pMob->next			= mob_index_hash[iHash];
    mob_index_hash[iHash]	= pMob;
    ch->desc->pEdit		= (void *)pMob;

    send_to_char( "Mobile Created.\n\r", ch );
    return TRUE;
}



#define COPYSTRING(mob1,mob2,field)	{ free_string(mob1->field);	      \
					  mob1->field=str_dup(mob2->field);   \
					}
bool medit_clone(CHAR_DATA *ch, char *argument) {
    MOB_INDEX_DATA *pOriginal,*pThis;
    EXTRA_DESCR_DATA *pEd,*pEdNew;
    PROPERTY *prop;
    int vnum=atoi(argument);
    int i;

    if ((pOriginal=get_mob_index(vnum))==NULL) {
	send_to_char("vnum mob not found.\n\r",ch);
	return FALSE;
    }

    EDIT_MOB(ch, pThis);

    COPYSTRING(pThis,pOriginal,player_name);
    COPYSTRING(pThis,pOriginal,short_descr);
    COPYSTRING(pThis,pOriginal,long_descr);
    COPYSTRING(pThis,pOriginal,description);
    COPYSTRING(pThis,pOriginal,material);
    COPYSTRING(pThis,pOriginal,pueblo_picture);

    while( pThis->extra_descr )
    {
        pEd=pThis->extra_descr->next;
        free_extra_descr(pThis->extra_descr);
        pThis->extra_descr=pEd;
    }

    for(pEd=pOriginal->extra_descr;pEd;pEd=pEd->next)
    {
        pEdNew=new_extra_descr();
        pEdNew->keyword=str_dup(pEd->keyword);
        pEdNew->description=str_dup(pEd->description);
        pEdNew->next=pThis->extra_descr;
        pThis->extra_descr=pEdNew;
    }

    memcpy(pThis->strbit_affected_by,pOriginal->strbit_affected_by,MAX_FLAGS);
    memcpy(pThis->strbit_affected_by2,pOriginal->strbit_affected_by2,MAX_FLAGS);
    memcpy(pThis->strbit_affected_read,pOriginal->strbit_affected_read,MAX_FLAGS);
    memcpy(pThis->strbit_affected_read2,pOriginal->strbit_affected_read2,MAX_FLAGS);
    memcpy(pThis->strbit_affected_saved,pOriginal->strbit_affected_saved,MAX_FLAGS);
    memcpy(pThis->strbit_affected_saved2,pOriginal->strbit_affected_saved2,MAX_FLAGS);
    memcpy(pThis->strbit_off_flags,pOriginal->strbit_off_flags,MAX_FLAGS);
    memcpy(pThis->strbit_imm_flags,pOriginal->strbit_imm_flags,MAX_FLAGS);
    memcpy(pThis->strbit_res_flags,pOriginal->strbit_res_flags,MAX_FLAGS);
    memcpy(pThis->strbit_vuln_flags,pOriginal->strbit_vuln_flags,MAX_FLAGS);
    memcpy(pThis->strbit_off_read,pOriginal->strbit_off_read,MAX_FLAGS);
    memcpy(pThis->strbit_imm_read,pOriginal->strbit_imm_read,MAX_FLAGS);
    memcpy(pThis->strbit_res_read,pOriginal->strbit_res_read,MAX_FLAGS);
    memcpy(pThis->strbit_vuln_read,pOriginal->strbit_vuln_read,MAX_FLAGS);
    memcpy(pThis->strbit_off_saved,pOriginal->strbit_off_saved,MAX_FLAGS);
    memcpy(pThis->strbit_imm_saved,pOriginal->strbit_imm_saved,MAX_FLAGS);
    memcpy(pThis->strbit_res_saved,pOriginal->strbit_res_saved,MAX_FLAGS);
    memcpy(pThis->strbit_vuln_saved,pOriginal->strbit_vuln_saved,MAX_FLAGS);

    memcpy(pThis->strbit_act,pOriginal->strbit_act,MAX_FLAGS);
    memcpy(pThis->strbit_act_read,pOriginal->strbit_act_read,MAX_FLAGS);
    memcpy(pThis->strbit_act_saved,pOriginal->strbit_act_saved,MAX_FLAGS);

    pThis->group		= pOriginal->group;
    pThis->alignment		= pOriginal->alignment;
    pThis->level		= pOriginal->level;
    pThis->hitroll		= pOriginal->hitroll;
    pThis->dam_type		= pOriginal->dam_type;
    pThis->start_pos		= pOriginal->start_pos;
    pThis->default_pos		= pOriginal->default_pos;
    pThis->sex			= pOriginal->sex;
    pThis->race			= pOriginal->race;
    pThis->wealth		= pOriginal->wealth;
    pThis->form			= pOriginal->form;
    pThis->parts		= pOriginal->parts;
    pThis->size			= pOriginal->size;
    pThis->mprog_vnum		= pOriginal->mprog_vnum;

    for (i=0;i<2;i++) {
	pThis->form_mem[i]	= pOriginal->form_mem[i];
	pThis->parts_mem[i]	= pOriginal->parts_mem[i];
    }
    for (i=0;i<3;i++) {
	pThis->hit[i]		= pOriginal->hit[i];
	pThis->mana[i]		= pOriginal->mana[i];
	pThis->damage[i]	= pOriginal->damage[i];
    }
    for (i=0;i<4;i++)
	pThis->ac[i]		= pOriginal->ac[i];

    pThis->spec_fun		= pOriginal->spec_fun;

    /* and the properties */
    for (prop = pOriginal->property; prop ; prop = prop->next)
        if (prop->sValue==NULL||prop->sValue[0]==0)
            SetDCharProperty(pThis,prop->propIndex->type,
                                prop->propIndex->key,&prop->iValue);
        else
            SetDCharProperty(pThis,prop->propIndex->type,
                                prop->propIndex->key,prop->sValue);

    // to do:
    //
    if (pThis->pShop) {
	extract_shop(pThis->pShop);
	pThis->pShop=NULL;
    }
    if (pOriginal->pShop) {
	pThis->pShop		= new_shop();
	pThis->pShop->keeper	= pThis->vnum;
	pThis->pShop->profit_buy= pOriginal->pShop->profit_buy;
	pThis->pShop->profit_sell= pOriginal->pShop->profit_sell;
	pThis->pShop->open_hour	= pOriginal->pShop->open_hour;
	pThis->pShop->close_hour= pOriginal->pShop->close_hour;
	pThis->pShop->type	= pOriginal->pShop->type;
	for (i=0;i<MAX_TRADE;i++)
	    pThis->pShop->buy_type[i] = pOriginal->pShop->buy_type[i];
	advertise_shop(pThis->pShop);
    }

    send_to_char("Mob cloned.\n\r",ch);

    return TRUE;
}

bool medit_ed( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;
    EXTRA_DESCR_DATA *ed;
    char command[MAX_INPUT_LENGTH];
    char keyword[MAX_INPUT_LENGTH];

    EDIT_MOB(ch, pMob);

    argument = one_argument( argument, command );
    one_argument( argument, keyword );
    smash_case(keyword);

    if ( command[0] == '\0' )
    {
        send_to_char( "Syntax:  ed add [keyword]\n\r", ch );
        send_to_char( "         ed delete [keyword]\n\r", ch );
        send_to_char( "         ed edit [keyword]\n\r", ch );
        send_to_char( "         ed format [keyword]\n\r", ch );
        send_to_char( "         ed show [keyword]\n\r", ch );
        return FALSE;
    }

    if ( !str_cmp( command, "add" ) )
    {
        if ( keyword[0] == '\0' )
        {
            send_to_char( "Syntax:  ed add [keyword]\n\r", ch );
            return FALSE;
        }

        ed                  =   new_extra_descr();
        ed->keyword         =   str_dup( keyword );
        ed->next            =   pMob->extra_descr;
        pMob->extra_descr   =   ed;

        editor_start( ch, &ed->description );

        return TRUE;
    }

    if ( !str_cmp( command, "edit" ) )
    {
        if ( keyword[0] == '\0' )
        {
            send_to_char( "Syntax:  ed edit [keyword]\n\r", ch );
            return FALSE;
        }

        for ( ed = pMob->extra_descr; ed; ed = ed->next )
        {
            if ( is_name( keyword, ed->keyword ) )
                break;
        }

        if ( !ed )
        {
            send_to_char( "OEdit:  Extra description keyword not found.\n\r", ch );
            return FALSE;
        }

        editor_start( ch, &ed->description );

        return TRUE;
    }

    if ( !str_cmp( command, "delete" ) )
    {
        EXTRA_DESCR_DATA *ped = NULL;

        if ( keyword[0] == '\0' )
        {
            send_to_char( "Syntax:  ed delete [keyword]\n\r", ch );
            return FALSE;
        }

        for ( ed = pMob->extra_descr; ed; ed = ed->next )
        {
            if ( is_name( keyword, ed->keyword ) )
                break;
            ped = ed;
        }

        if ( !ed )
        {
            send_to_char( "MEdit:  Extra description keyword not found.\n\r", ch );
            return FALSE;
        }

        if ( !ped )
            pMob->extra_descr = ed->next;
        else
            ped->next = ed->next;

        free_extra_descr( ed );

        send_to_char( "Extra description deleted.\n\r", ch );
        return TRUE;
    }


    if ( !str_cmp( command, "format" ) )
    {
        if ( keyword[0] == '\0' )
        {
            send_to_char( "Syntax:  ed format [keyword]\n\r", ch );
            return FALSE;
        }

        for ( ed = pMob->extra_descr; ed; ed = ed->next )
        {
            if ( is_name( keyword, ed->keyword ) )
                break;
        }

        if ( !ed )
        {
                send_to_char( "MEdit:  Extra description keyword not found.\n\r", ch );
                return FALSE;
        }

        /* OLC 1.1b */
        if ( strlen(ed->description) >= (MAX_STRING_LENGTH - 4) )
        {
            send_to_char( "String too long to be formatted.\n\r", ch );
            return FALSE;
        }

        ed->description = format_string( ed->description );

        send_to_char( "Extra description formatted.\n\r", ch );
        return TRUE;
    }

    if(!str_cmp( command, "show" ) )
    {
        if ( keyword[0] == '\0' )
        {
            send_to_char( "Syntax:  ed format [keyword]\n\r", ch );
            return FALSE;
        }

        for ( ed = pMob->extra_descr; ed; ed = ed->next )
        {
            if ( is_name( keyword, ed->keyword ) )
                break;
        }

        if ( !ed )
        {
                send_to_char( "OEdit:  Extra description keyword not found.\n\r", ch );
                return FALSE;
        }

        send_to_char( ed->description, ch );

        return TRUE;
    }

    oedit_ed( ch, "" );
    return FALSE;
}

bool medit_spec( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;
    int i;

    EDIT_MOB(ch, pMob);

    if ( argument[0] == '\0' )
    {
	send_to_char( "Syntax:  spec [special function]\n\r", ch );
	send_to_char("{yAvailable special functions: {x",ch);
	for (i=0;spec_table[i].name[0];i++) {
	    send_to_char(spec_table[i].name,ch);
	    send_to_char(", ",ch);
	}
	send_to_char("\n\r",ch);

	return FALSE;
    }


    if ( !str_cmp( argument, "none" ) )
    {
        pMob->spec_fun = NULL;

        send_to_char( "Spec removed.\n\r", ch);
        return TRUE;
    }

    if ( spec_lookup( argument ) )
    {
	pMob->spec_fun = spec_lookup( argument );
	send_to_char( "Spec set.\n\r", ch);
	return TRUE;
    }

    send_to_char( "MEdit: No such special function.\n\r", ch );
    send_to_char("{yAvailable special functions: {x",ch);
    for (i=0;spec_table[i].name[0];i++) {
	send_to_char(spec_table[i].name,ch);
	send_to_char(", ",ch);
    }
    send_to_char("\n\r",ch);
    return FALSE;
}



bool medit_align( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;

    EDIT_MOB(ch, pMob);

    if ( argument[0] == '\0' || !is_number( argument ) )
    {
	send_to_char( "Syntax:  alignment [number]\n\r", ch );
	return FALSE;
    }

    pMob->alignment = atoi( argument );

    send_to_char( "Alignment set.\n\r", ch);
    return TRUE;
}



bool medit_level( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;

    EDIT_MOB(ch, pMob);

    if ( argument[0] == '\0' || !is_number( argument ) )
    {
	send_to_char( "Syntax:  level [number]\n\r", ch );
	return FALSE;
    }

    pMob->level = atoi( argument );

    if(pMob->level>=0 && pMob->level<=90)
    {
	pMob->hit[0]=olc_mob_recommended[pMob->level].hit[0];
	pMob->hit[1]=olc_mob_recommended[pMob->level].hit[1];
	pMob->hit[2]=olc_mob_recommended[pMob->level].hit[2];
	pMob->mana[0]=olc_mob_recommended[pMob->level].mana[0];
	pMob->mana[1]=olc_mob_recommended[pMob->level].mana[1];
	pMob->mana[2]=olc_mob_recommended[pMob->level].mana[2];
	pMob->damage[0]=olc_mob_recommended[pMob->level].damage[0];
	pMob->damage[1]=olc_mob_recommended[pMob->level].damage[1];
	pMob->damage[2]=olc_mob_recommended[pMob->level].damage[2];
	pMob->ac[0]=olc_mob_recommended[pMob->level].ac*10;
	pMob->ac[1]=olc_mob_recommended[pMob->level].ac*10;
	pMob->ac[2]=olc_mob_recommended[pMob->level].ac*10;
	pMob->ac[3]=(((olc_mob_recommended[pMob->level].ac-10)/3)+10)*10;

        send_to_char( "Level set and recommended values filled in.\n\r", ch);
        return TRUE;
    }

    send_to_char( "Level set no recommemded values are filled in.\n\r", ch);
    return TRUE;
}

bool medit_proginstance( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;
    int vnum;

    EDIT_MOB(ch, pMob);

    if ( argument[0] == '\0' || !is_number( argument ) )
    {
	send_to_char( "Syntax:  proginstance [vnum]\n\r", ch );
	return FALSE;
    }

    vnum = atoi( argument );

    if(vnum==0)
    {
      pMob->mprog_vnum=0;
      send_to_char("Mobprog removed.\n\r", ch);
      return TRUE;
    }

    if(!get_mprog_index(vnum)) {
      sprintf_to_char(ch,"Mob program with vnum %d does not exist.\n\r",vnum);
      return FALSE;
    }

    pMob->mprog_vnum=vnum;

    send_to_char( "Proginstance set.\n\r", ch);
    return TRUE;
}


bool medit_desc( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;

    EDIT_MOB(ch, pMob);

    if ( argument[0] == '\0' )
    {
	editor_start( ch, &pMob->description );
	return TRUE;
    }

    send_to_char( "Syntax:  desc    - line edit\n\r", ch );
    return FALSE;
}




bool medit_long( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;

    EDIT_MOB(ch, pMob);

    if (argument[0]==0) {
	send_to_char("Syntax:  long <string>\n\r",ch);
	return FALSE;
    }

    strcat( argument, "\n\r" );
    free_string(pMob->long_descr);
    pMob->long_descr=str_dup(argument);
    pMob->long_descr[0]=UPPER(pMob->long_descr[0]);

    send_to_char("Long description set.\n\r",ch);
    return TRUE;
}



bool medit_short( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;

    EDIT_MOB(ch, pMob);

    if ( argument[0] == '\0' )
    {
	send_to_char( "Syntax:  short [string]\n\r", ch );
	return FALSE;
    }

    free_string( pMob->short_descr );
    pMob->short_descr = str_dup( argument );

    send_to_char( "Short description set.\n\r", ch);
    return TRUE;
}



bool medit_name( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;

    EDIT_MOB(ch, pMob);

    if ( argument[0] == '\0' )
    {
	send_to_char( "Syntax:  name [string]\n\r", ch );
	return FALSE;
    }

    /* Check if there is a player with one of the mob names. */
    {
        char arg1[MAX_INPUT_LENGTH],*ap;
        char arg2[MAX_INPUT_LENGTH];

        strcpy(arg1,argument);
        ap=arg1;
        fclose(fpReserve);

        while(*ap)
        {
            ap=one_argument(ap,arg2);
	    if (player_exist(arg2)) {
                send_to_char("You have included a PC name in your mobname.\n\r",ch);
                return FALSE;
	    }
        }
    }

    free_string( pMob->player_name );
    pMob->player_name = str_dup( argument );

    send_to_char( "Name set.\n\r", ch);
    return TRUE;
}




bool medit_shop( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;
    char command[MAX_INPUT_LENGTH];
    char arg1[MAX_INPUT_LENGTH];

    argument = one_argument( argument, command );
    argument = one_argument( argument, arg1 );

    EDIT_MOB(ch, pMob);

    if ( command[0] == '\0' )
    {
	send_to_char( "Syntax:  shop hours [#opening] [#closing]\n\r", ch );
	send_to_char( "         shop profit [#buying%] [#selling%]\n\r", ch );
	send_to_char( "         shop type [#0-4] [item type]\n\r", ch );
	send_to_char( "         shop delete [#0-4]\n\r", ch );
	send_to_char( "         shop class [shop class]\n\r",ch);
	send_to_char( "         shop remove\n\r",ch);
	return FALSE;
    }


    if ( !str_cmp( command, "hours" ) )
    {
	if ( arg1[0] == '\0' || !is_number( arg1 )
	|| argument[0] == '\0' || !is_number( argument ) )
	{
	    send_to_char( "Syntax:  shop hours [#opening] [#closing]\n\r", ch );
	    return FALSE;
	}

	if ( !pMob->pShop )
	{
	    pMob->pShop         = new_shop();
	    pMob->pShop->keeper = pMob->vnum;
	    advertise_shop(pMob->pShop);
	}

	pMob->pShop->open_hour = atoi( arg1 );
	pMob->pShop->close_hour = atoi( argument );

	send_to_char( "Shop hours set.\n\r", ch);
	return TRUE;
    }


    if ( !str_cmp( command, "profit" ) )
    {
	if ( arg1[0] == '\0' || !is_number( arg1 )
	|| argument[0] == '\0' || !is_number( argument ) )
	{
	    send_to_char( "Syntax:  shop profit [#buying%] [#selling%]\n\r", ch );
	    return FALSE;
	}

	if ( !pMob->pShop )
	{
	    pMob->pShop         = new_shop();
	    pMob->pShop->keeper = pMob->vnum;
	    advertise_shop(pMob->pShop);
	}

	pMob->pShop->profit_buy     = atoi( arg1 );
	pMob->pShop->profit_sell    = atoi( argument );

	send_to_char( "Shop profit set.\n\r", ch);
	return TRUE;
    }


    if ( !str_cmp( command, "type" ) )
    {
	char buf[MAX_INPUT_LENGTH];
	int value;

	if ( arg1[0] == 0 || !is_number( arg1 )
	|| argument[0] == 0 ) {
	    send_to_char( "Syntax:  shop type [#0-4] [item type]\n\r", ch );
	    olc_help_table(ch,"Item type:",item_table);
	    return FALSE;
	}

	if ( atoi( arg1 ) >= MAX_TRADE ) {
	    sprintf( buf, "REdit:  May sell %d items max.\n\r", MAX_TRADE );
	    send_to_char( buf, ch );
	    return FALSE;
	}

	if ( ( value = table_find_name( argument,item_table) ) == NO_FLAG ) {
	    send_to_char( "REdit:  That type of item is not known.\n\r", ch );
	    olc_help_table(ch,"Item type:",item_table);
	    return FALSE;
	}

	if ( pMob->pShop==NULL ) {
	    pMob->pShop         = new_shop();
	    pMob->pShop->keeper = pMob->vnum;
	    advertise_shop(pMob->pShop);
	}

	pMob->pShop->buy_type[atoi( arg1 )] = value;

	send_to_char( "Shop type set.\n\r", ch);
	return TRUE;
    }

    if ( !str_cmp( command, "class" ) )
    {
	int value;

	if(arg1[0]=='\0')
	{
	    send_to_char( "Syntax:  shop class [shop class]\n\r", ch );
	    olc_help_table(ch,"Shop class:",shoptype_table);
	    return FALSE;
	}

	if ( ( value = table_find_name( arg1,shoptype_table) ) == NO_FLAG ) {
	    send_to_char( "REdit:  That type of shop class is not known.\n\r", ch );
	    olc_help_table(ch,"Shop class:",shoptype_table);
	    return FALSE;
	}

	if ( pMob->pShop==NULL ) {
	    pMob->pShop         = new_shop();
	    pMob->pShop->keeper = pMob->vnum;
	    advertise_shop(pMob->pShop);
	}

	pMob->pShop->type=value;

	send_to_char( "Shop class set.\n\r", ch);
	return TRUE;
    }


    if ( !str_cmp( command, "remove" ) )
    {
	if ( !pMob->pShop )
	{
	    send_to_char( "REdit:  Non-existant shop.\n\r", ch );
	    return FALSE;
	}

	extract_shop( pMob->pShop );
	pMob->pShop=NULL;

	send_to_char( "Shop removed.\n\r", ch);
	return TRUE;
    }

    if ( !str_cmp( command, "delete" ) )
    {
        int value;
        char buf[100];

	value = atoi( arg1 );

	if ( arg1[0] == '\0' || !is_number(arg1))
	{
	    send_to_char( "Syntax:  shop delete [#0-4]\n\r", ch );
	    return FALSE;
	}

	if ( !pMob->pShop )
	{
	    send_to_char( "REdit:  Non-existant shop.\n\r", ch );
	    return FALSE;
	}

	if ( value >= MAX_TRADE )
	{
	    sprintf( buf, "REdit:  May sell %d items max.\n\r", MAX_TRADE );
	    send_to_char( buf, ch );
	    return FALSE;
	}

	pMob->pShop->buy_type[value] = 0;

	send_to_char( "Shop type deleted.\n\r", ch);
	return TRUE;
    }


    medit_shop( ch, "" );
    return FALSE;
}

bool medit_group( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;

    EDIT_MOB(ch, pMob);

    if ( argument[0] == '\0' || !is_number( argument ) )
    {
	send_to_char( "Syntax:  group [number]\n\r", ch );
	return FALSE;
    }

    pMob->group = atoi( argument );

    send_to_char( "Group set.\n\r", ch);
    return TRUE;
}

bool medit_race( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;
    char s[MAX_FLAGS];

    EDIT_MOB(ch, pMob);

    if ( argument[0] == '\0' )
    {
	send_to_char( "Syntax:  race [race name]\n\r", ch );
	return FALSE;
    }

    pMob->race = race_lookup( argument );

    /* Now change the flags */
    STR_COPY_STR(pMob->strbit_act,pMob->strbit_act_read,MAX_FLAGS);
    STR_OR_STR(pMob->strbit_act,race_table[pMob->race].strbit_act,MAX_FLAGS);
    STR_COPY_STR(s,race_table[pMob->race].strbit_act,MAX_FLAGS);
    STR_NOT_STR(s,MAX_FLAGS);
    STR_AND_STR(pMob->strbit_act_read,s,MAX_FLAGS);
    STR_ZERO_BIT(pMob->strbit_act_saved,MAX_FLAGS);
//  pMob->act=pMob->act_mem[0] | race_table[pMob->race].act;
//  pMob->act_mem[0]=pMob->act_mem[0]&(~race_table[pMob->race].act);
//  pMob->act_mem[1]=0;

    STR_COPY_STR(pMob->strbit_affected_by,pMob->strbit_affected_read,MAX_FLAGS);
    STR_OR_STR(pMob->strbit_affected_by,race_table[pMob->race].strbit_eff,MAX_FLAGS);
    STR_COPY_STR(s,race_table[pMob->race].strbit_eff,MAX_FLAGS);
    STR_NOT_STR(s,MAX_FLAGS);
    STR_AND_STR(pMob->strbit_affected_read,s,MAX_FLAGS);
    STR_ZERO_BIT(pMob->strbit_affected_saved,MAX_FLAGS);
//  pMob->affected_by=pMob->affected_mem[0] | race_table[pMob->race].eff;
//  pMob->affected_mem[0]=pMob->affected_mem[0]&(~race_table[pMob->race].eff);
//  pMob->affected_mem[1]=0;

    STR_COPY_STR(pMob->strbit_affected_by2,pMob->strbit_affected_read2,MAX_FLAGS);
    STR_OR_STR(pMob->strbit_affected_by2,race_table[pMob->race].strbit_eff2,MAX_FLAGS);
    STR_COPY_STR(s,race_table[pMob->race].strbit_eff2,MAX_FLAGS);
    STR_NOT_STR(s,MAX_FLAGS);
    STR_AND_STR(pMob->strbit_affected_read2,s,MAX_FLAGS);
    STR_ZERO_BIT(pMob->strbit_affected_saved,MAX_FLAGS);
//  pMob->affected_by2=pMob->affected2_mem[0] | race_table[pMob->race].eff2;
//  pMob->affected2_mem[0]=pMob->affected2_mem[0]&(~race_table[pMob->race].eff2);
//  pMob->affected2_mem[1]=0;

    STR_COPY_STR(pMob->strbit_off_flags,pMob->strbit_off_read,MAX_FLAGS);
    STR_OR_STR(pMob->strbit_off_flags,race_table[pMob->race].strbit_off,MAX_FLAGS);
    STR_COPY_STR(s,race_table[pMob->race].strbit_off,MAX_FLAGS);
    STR_NOT_STR(s,MAX_FLAGS);
    STR_AND_STR(pMob->strbit_off_read,s,MAX_FLAGS);
    STR_ZERO_BIT(pMob->strbit_off_saved,MAX_FLAGS);
//  pMob->off_flags=pMob->off_mem[0] | race_table[pMob->race].off;
//  pMob->off_mem[0]=pMob->off_mem[0]&(~race_table[pMob->race].off);
//  pMob->off_mem[1]=0;

    STR_COPY_STR(pMob->strbit_imm_flags,pMob->strbit_imm_read,MAX_FLAGS);
    STR_OR_STR(pMob->strbit_imm_flags,race_table[pMob->race].strbit_imm,MAX_FLAGS);
    STR_COPY_STR(s,race_table[pMob->race].strbit_imm,MAX_FLAGS);
    STR_NOT_STR(s,MAX_FLAGS);
    STR_AND_STR(pMob->strbit_imm_read,s,MAX_FLAGS);
    STR_ZERO_BIT(pMob->strbit_imm_saved,MAX_FLAGS);
//  pMob->imm_flags=pMob->imm_mem[0] | race_table[pMob->race].imm;
//  pMob->imm_mem[0]=pMob->imm_mem[0]&(~race_table[pMob->race].imm);
//  pMob->imm_mem[1]=0;

    STR_COPY_STR(pMob->strbit_res_flags,pMob->strbit_res_read,MAX_FLAGS);
    STR_OR_STR(pMob->strbit_res_flags,race_table[pMob->race].strbit_res,MAX_FLAGS);
    STR_COPY_STR(s,race_table[pMob->race].strbit_res,MAX_FLAGS);
    STR_NOT_STR(s,MAX_FLAGS);
    STR_AND_STR(pMob->strbit_res_read,s,MAX_FLAGS);
    STR_ZERO_BIT(pMob->strbit_res_saved,MAX_FLAGS);
//  pMob->res_flags=pMob->res_mem[0] | race_table[pMob->race].res;
//  pMob->res_mem[0]=pMob->res_mem[0]&(~race_table[pMob->race].res);
//  pMob->res_mem[1]=0;

    STR_COPY_STR(pMob->strbit_vuln_flags,pMob->strbit_vuln_read,MAX_FLAGS);
    STR_OR_STR(pMob->strbit_vuln_flags,race_table[pMob->race].strbit_vuln,MAX_FLAGS);
    STR_COPY_STR(s,race_table[pMob->race].strbit_vuln,MAX_FLAGS);
    STR_NOT_STR(s,MAX_FLAGS);
    STR_AND_STR(pMob->strbit_vuln_read,s,MAX_FLAGS);
    STR_ZERO_BIT(pMob->strbit_vuln_saved,MAX_FLAGS);
//  pMob->vuln_flags=pMob->vuln_mem[0] | race_table[pMob->race].vuln;
//  pMob->vuln_mem[0]=pMob->vuln_mem[0]&(~race_table[pMob->race].vuln);
//  pMob->vuln_mem[1]=0;

    pMob->form=pMob->form_mem[0] | race_table[pMob->race].form;
    pMob->form_mem[0]=pMob->form_mem[0]&(~race_table[pMob->race].form);
    pMob->form_mem[1]=0;

    pMob->parts=pMob->parts_mem[0] | race_table[pMob->race].parts;
    pMob->parts_mem[0]=pMob->parts_mem[0]&(~race_table[pMob->race].parts);
    pMob->parts_mem[1]=0;

    if(pMob->race==0) send_to_char("Unique race set.\n\r", ch );
    else send_to_char( "Race set.\n\r", ch);

    return TRUE;
}

bool medit_position( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;
    char arg1[MAX_INPUT_LENGTH];
    int pos;

    EDIT_MOB(ch, pMob);

    argument = one_argument( argument, arg1 );

    if( !str_cmp( "start", arg1 ) )
    {
	pos=position_lookup(argument);

	if(pos==-1)
	{
	    send_to_char("MEdit: Unknown position.\n\r", ch );
	    return FALSE;
	}

	if(pos!=POS_SLEEPING && pos!=POS_RESTING
	&& pos!=POS_SITTING && pos!=POS_STANDING)
	{
	    send_to_char( "Unallowed position.\n\r", ch );
	    return FALSE;
	}

	pMob->start_pos=pos;
	send_to_char( "Start position set.\n\r", ch );

	return TRUE;
    }

    if( !str_cmp( "default", arg1 ) )
    {
	pos=position_lookup(argument);

	if(pos==-1)
	{
	    send_to_char("MEdit: Unknown position.\n\r", ch );
	    return FALSE;
	}

	if(pos!=POS_SLEEPING && pos!=POS_RESTING
	&& pos!=POS_SITTING && pos!=POS_STANDING)
	{
	    send_to_char( "Unallowed position.\n\r", ch );
	    return FALSE;
	}

	pMob->default_pos=pos;
	send_to_char( "Default position set.\n\r", ch );

	return TRUE;
    }

    if( position_lookup(arg1)==-1 || position_lookup(argument)==-1)
    {
	send_to_char( "Syntax: position start [position]\n\r", ch );
	send_to_char( "Syntax: position default [position]\n\r", ch );
	send_to_char( "Syntax: position [startpos] [defaultpos]\n\r", ch );
	return FALSE;
    }

    pos=position_lookup(arg1);
    if(pos!=POS_SLEEPING && pos!=POS_RESTING
    && pos!=POS_SITTING && pos!=POS_STANDING)
    {
	send_to_char( "Unallowed start position.\n\r", ch );
	return FALSE;
    }

    pos=position_lookup(argument);
    if(pos!=POS_SLEEPING && pos!=POS_RESTING
    && pos!=POS_SITTING && pos!=POS_STANDING)
    {
	send_to_char( "Unallowed default position.\n\r", ch );
	return FALSE;
    }

    pMob->start_pos=position_lookup(arg1);
    pMob->default_pos=position_lookup(argument);

    send_to_char( "Positions set.\n\r", ch );

    return TRUE;
}

bool medit_wealth( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;

    EDIT_MOB(ch, pMob);

    if ( argument[0] == '\0' || !is_number( argument ) )
    {
	send_to_char( "Syntax:  wealth [silver]\n\r", ch );
	return FALSE;
    }

    pMob->wealth = atoi( argument );

    send_to_char( "Wealth set.\n\r", ch);
    return TRUE;
}

bool medit_material( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;

    EDIT_MOB(ch, pMob);

    if ( argument[0] == '\0')
    {
	send_to_char( "Syntax:  material [string]\n\r", ch );
	return FALSE;
    }

    free_string(pMob->material);
    pMob->material = str_dup( argument );

    send_to_char( "Material set.\n\r", ch);
    return TRUE;
}

bool medit_hitroll( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;

    EDIT_MOB(ch, pMob);

    if ( argument[0] == '\0' || !is_number( argument ) )
    {
	send_to_char( "Syntax:  hitroll [number]\n\r", ch );
	return FALSE;
    }

    pMob->hitroll = atoi( argument );

    send_to_char( "Hitroll set.\n\r", ch);
    return TRUE;
}

bool medit_damagetype( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;
    int i;

    EDIT_MOB(ch, pMob);

    if ( argument[0] == '\0' )
    {
	send_to_char( "{ySyntax: {xdamagetype [damagetype]\n\r", ch );
	send_to_char("{yWhere damage-type is:{x\n\r",ch);
	for (i=0;attack_table[i].name;i++) {
	    if (i)
		send_to_char(", ",ch);
	    send_to_char(attack_table[i].name,ch);
	}
	send_to_char("\n\r",ch);

	return FALSE;
    }

    pMob->dam_type = attack_lookup( argument );

    if(pMob->dam_type==0) {
	send_to_char("Default damage type set.\n\r", ch );
	send_to_char("Use '{Wdamage{x' without options to see the possible damage-types.\n\r",ch);
	return TRUE;
    } else {
	send_to_char( "Damage type set.\n\r", ch);
	return TRUE;
    }

}

bool medit_ac( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;
    char arg1[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    char arg3[MAX_INPUT_LENGTH];

    EDIT_MOB(ch, pMob);

    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );
    argument = one_argument( argument, arg3 );

    if(arg2[0]=='\0' || !is_number(arg2)) goto showhelp;

    if( !str_cmp( "pierce", arg1 ) )
    {
	pMob->ac[AC_PIERCE]=atoi(arg2);
	send_to_char( "Armor vs. pierce set.\n\r", ch );
	return TRUE;
    }

    if( !str_cmp( "bash", arg1 ) )
    {
	pMob->ac[AC_BASH]=atoi(arg2);
	send_to_char( "Armor vs. bash set.\n\r", ch );
	return TRUE;
    }

    if( !str_cmp( "slash", arg1 ) )
    {
	pMob->ac[AC_SLASH]=atoi(arg2);
	send_to_char( "Armor vs. slash set.\n\r", ch );
	return TRUE;
    }

    if( !str_cmp( "exotic", arg1 ) )
    {
	pMob->ac[AC_EXOTIC]=atoi(arg2);
	send_to_char( "Armor vs. exotic set.\n\r", ch );
	return TRUE;
    }

    if(arg1[0]!='\0' && is_number(arg1) &&
       arg3[0]!='\0' && is_number(arg3) &&
       argument[0]!='\0' && is_number(argument))
    {
	pMob->ac[AC_PIERCE]=atoi(arg1);
	pMob->ac[AC_BASH]=atoi(arg2);
	pMob->ac[AC_SLASH]=atoi(arg3);
	pMob->ac[AC_EXOTIC]=atoi(argument);

	send_to_char( "Armor set.\n\r", ch );
	return TRUE;
    }

showhelp:
    send_to_char( "Syntax:  ac pierce [number]\n\r", ch );
    send_to_char( "Syntax:  ac bash [number]\n\r", ch );
    send_to_char( "Syntax:  ac slash [number]\n\r", ch );
    send_to_char( "Syntax:  ac exotic [number]\n\r", ch );
    send_to_char( "Syntax:  ac [pierce] [bash] [slash] [exotic]\n\r", ch );

    return FALSE;
}

bool medit_dice( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;
    char arg1[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    int dice[3];

    EDIT_MOB(ch, pMob);

    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );

    if(arg2[0]=='\0' || sscanf(arg2,"%dd%d+%d",&dice[0],&dice[1],&dice[2])!=3)
	goto showhelp;

    if( !str_cmp( "hit", arg1 ) )
    {
	pMob->hit[0]=dice[0];
	pMob->hit[1]=dice[1];
	pMob->hit[2]=dice[2];

	send_to_char( "Hit dice set.\n\r", ch );
	return TRUE;
    }

    if( !str_cmp( "damage", arg1 ) )
    {
	pMob->damage[0]=dice[0];
	pMob->damage[1]=dice[1];
	pMob->damage[2]=dice[2];

	send_to_char( "Hit dice set.\n\r", ch );
	return TRUE;
    }

    if( !str_cmp( "mana", arg1 ) )
    {
	pMob->mana[0]=dice[0];
	pMob->mana[1]=dice[1];
	pMob->mana[2]=dice[2];

	send_to_char( "Mana dice set.\n\r", ch );
	return TRUE;
    }

    if(arg1[0]!='\0' && sscanf(arg1,"%dd%d+%d",&dice[0],&dice[1],&dice[2])==3
    && argument[0]!='\0' && sscanf(argument,"%dd%d+%d",&dice[0],&dice[1],&dice[2])==3)
    {
	sscanf(arg1,"%dd%d+%d",&dice[0],&dice[1],&dice[2]);
	pMob->hit[0]=dice[0];
	pMob->hit[1]=dice[1];
	pMob->hit[2]=dice[2];
	sscanf(arg2,"%dd%d+%d",&dice[0],&dice[1],&dice[2]);
	pMob->mana[0]=dice[0];
	pMob->mana[1]=dice[1];
	pMob->mana[2]=dice[2];
	sscanf(argument,"%dd%d+%d",&dice[0],&dice[1],&dice[2]);
	pMob->damage[0]=dice[0];
	pMob->damage[1]=dice[1];
	pMob->damage[2]=dice[2];

	send_to_char( "Dices set.\n\r", ch );
	return TRUE;
    }


showhelp:
    send_to_char( "Syntax:  dice hit [?d?+?]\n\r", ch );
    send_to_char( "Syntax:  dice mana [?d?+?]\n\r", ch );
    send_to_char( "Syntax:  dice damage [?d?+?]\n\r", ch );
    send_to_char( "Syntax:  dice [?d?+?] [?d?+?] [?d?+?]\n\r", ch );

    return FALSE;
}

bool medit_imm( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;

    EDIT_MOB(ch, pMob);

    if ( argument[0] == '\0' )
    {
	send_to_char( "Syntax:  imm [immune flags]\n\r", ch );
	return FALSE;
    }

    if ( option_find_name( argument,imm_options,FALSE) != NO_FLAG ) {
	mob_flag_toggle_string(
	    argument,
	    imm_options,
	    pMob->strbit_imm_read,
	    race_table[pMob->race].strbit_imm,
	    pMob->strbit_imm_saved,
	    pMob->strbit_imm_flags);

        send_to_char( "Immune flag toggled.\n\r", ch);
        return TRUE;
    }

    send_to_char( "MEdit: Unkown immune flag.\n\r", ch );
    olc_help_options(ch,NULL,imm_options);
    return FALSE;
}

bool medit_res( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;

    EDIT_MOB(ch, pMob);

    if ( argument[0] == '\0' )
    {
	send_to_char( "Syntax:  res [resistance flags]\n\r", ch );
	return FALSE;
    }

    if ( option_find_name( argument,res_options,FALSE) != NO_FLAG )
    {
	mob_flag_toggle_string(
	    argument,
	    res_options,
	    pMob->strbit_res_read,
	    race_table[pMob->race].strbit_res,
	    pMob->strbit_res_saved,
	    pMob->strbit_res_flags);

        send_to_char( "Resistance flag toggled.\n\r", ch);
        return TRUE;
    }

    send_to_char( "MEdit: Unkown resistance flag.\n\r", ch );
    olc_help_options(ch,NULL,res_options);
    return FALSE;
}

bool medit_vuln( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;

    EDIT_MOB(ch, pMob);

    if ( argument[0] == 0 ) {
	send_to_char( "Syntax:  vuln [vulnrable flags]\n\r", ch );
	return FALSE;
    }

    if ( option_find_name( argument, vuln_options,FALSE ) != NO_FLAG ) {
	mob_flag_toggle_string(
	    argument,
	    vuln_options,
	    pMob->strbit_vuln_read,
	    race_table[pMob->race].strbit_vuln,
	    pMob->strbit_vuln_saved,
	    pMob->strbit_vuln_flags);

        send_to_char( "Vulnrable flag toggled.\n\r", ch);
        return TRUE;
    }

    send_to_char( "MEdit: Unkown vulnrable flag.\n\r", ch );
    olc_help_options(ch,NULL,vuln_options);
    return FALSE;
}

bool medit_offensive( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;

    EDIT_MOB(ch, pMob);

    if ( argument[0] == 0 ) {
	send_to_char( "Syntax:  offensive [offensive flags]\n\r", ch );
	return FALSE;
    }

    if ( option_find_name( argument, off_options,FALSE) != NO_FLAG ) {
	mob_flag_toggle_string(
	    argument,
	    off_options,
	    pMob->strbit_off_read,
	    race_table[pMob->race].strbit_off,
	    pMob->strbit_off_saved,
	    pMob->strbit_off_flags);

        send_to_char( "Offensive flag toggled.\n\r", ch);
        return TRUE;
    }

    send_to_char( "MEdit: Unkown offensive flag.\n\r", ch );
    olc_help_options(ch,NULL,off_options);
    return FALSE;
}

bool medit_size( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;
    int size;

    EDIT_MOB(ch, pMob);

    if ( argument[0] == '\0' )
    {
	send_to_char( "Syntax:  size [size]\n\r", ch );
	return FALSE;
    }

    size = size_lookup( argument );

    if(size==-1)
    {
      send_to_char( "Invalid size value.", ch );
      return FALSE;
    }

    pMob->size = size;
    send_to_char( "Size set.\n\r", ch);

    return TRUE;
}

bool medit_form( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;

    EDIT_MOB(ch, pMob);

    if ( argument[0] == '\0' ) {
	send_to_char( "Syntax:  form [form flags]\n\r", ch );
	return FALSE;
    }

    if ( option_find_name( argument,form_options,FALSE) != NO_FLAG ) {
	pMob->form=mob_flag_toggle(argument,
	    form_options,
	    &pMob->form_mem[0],
	    race_table[pMob->race].form,
	    &pMob->form_mem[1] );

        send_to_char( "Form flag toggled.\n\r", ch);
        return TRUE;
    }

    send_to_char( "MEdit: Unkown form flag.\n\r", ch );
    olc_help_options(ch,NULL,form_options);
    return FALSE;
}

bool medit_delete( CHAR_DATA *ch, char *argument) {
    MOB_INDEX_DATA *pMob;

    EDIT_MOB(ch, pMob);

    pMob->deleted=!pMob->deleted;

    sprintf_to_char(ch,"Mob is %s deleted.\n\r",
	pMob->deleted?"now":"not anymore");
    return TRUE;
}

bool medit_picture(CHAR_DATA *ch, char *argument) {
    MOB_INDEX_DATA *pMob;
    char s[MSL],*p;
    int i;

    EDIT_MOB(ch, pMob);
    free_string(pMob->pueblo_picture);

    p=argument;
    i=0;
    while (*p) {
	switch (*p) {
	    case ' ': s[i++]='%';s[i++]='2';s[i++]='0';break;
	    case '~': s[i++]='%';s[i++]='7';s[i++]='e';break;
	    default : s[i++]=*p;
	}
	p++;
    }
    s[i]=0;
    pMob->pueblo_picture=str_dup(s);
    return TRUE;
}

bool medit_property( CHAR_DATA *ch, char *argument) {
    MOB_INDEX_DATA *pMob;
    char stype[MAX_STRING_LENGTH];
    char key[MAX_STRING_LENGTH];
    char *svalue;
    long l;
    int  i;
    bool b,delete;
    char c;

    EDIT_MOB(ch, pMob);

    argument=one_argument(argument,key);
    argument=one_argument(argument,stype);
    svalue=argument;

    delete=str_cmp(svalue,"delete")?FALSE:TRUE;

    if (svalue==NULL||svalue[0]==0) {
        send_to_char("Usage: property <key> <type> <value>\n\r",ch);
        send_to_char("  Where <type> is \"bool\", \"int\", \"long\", \"char\","
                     "\"string\n\r",ch);
        return FALSE;
    }

    if (!does_property_exist_s(key,stype)) {
	send_to_char("Unknown property, see `{Wpropertylist{x` for overview.\n\r",ch);
	return FALSE;
    }

    switch (which_keyword(stype,"bool","int","long","char","string",NULL)) {
        case 1:
	    if (delete)
		DeleteDCharProperty(pMob,PROPERTY_BOOL,key);
	    else {
		switch (which_keyword(svalue,"true","false",NULL)) {
		    case 1:
			b=TRUE;
			break;
		    case 2:
			b=FALSE;
			break;
		    default:
			medit_property(ch,"");
			return FALSE;
		}
		SetDCharProperty(pMob,PROPERTY_BOOL,key,&b);
	    }
	    break;
	case 2:
	    if (delete)
		DeleteDCharProperty(pMob,PROPERTY_INT,key);
	    else {
		i=atoi(svalue);
		SetDCharProperty(pMob,PROPERTY_INT,key,&i);
	    }
            break;
        case 3:
	    if (delete)
		DeleteDCharProperty(pMob,PROPERTY_LONG,key);
	    else {
		l=atol(svalue);
		SetDCharProperty(pMob,PROPERTY_LONG,key,&l);
	    }
            break;
        case 4:
	    if (delete)
		DeleteDCharProperty(pMob,PROPERTY_CHAR,key);
	    else {
		c=svalue[0];
		SetDCharProperty(pMob,PROPERTY_CHAR,key,&c);
	    }
            break;
        case 5:
	    if (delete)
		DeleteDCharProperty(pMob,PROPERTY_STRING,key);
	    else
		SetDCharProperty(pMob,PROPERTY_STRING,key,svalue);
            break;
        default:
            medit_property(ch,"");
            return FALSE;
    }

    send_to_char("Done.\n\r",ch);

    return TRUE;
}
