//
// $Id: recycle.c,v 1.93 2006/10/18 19:41:57 jodocus Exp $ */
//
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

//
// All routines which have to do with recycling of memory: allocation, freeing
//

#include "merc.h"
#include "db.h"

///////////////////////////////////////////////////////////////////////////////
//
// Layout of functions with regard to new_() and free_() in here:
//
// //
// // comment
// //
// NOTE_DATA *	type_free=NULL;
// int		type_allocated=0;
// int		type_inuse=0;
//
// TYPE *new_type(void) {
//     TYPE *type;
//
//     if (type_free==NULL) {
//         type=alloc_perm(sizeof(TYPE));
//         type_allocated++;
//     } else {
//         type=type_free;
//         type_free=type_free->next;
//     }
//
//     type_inuse++;
//     ZEROVAR(type,TYPE);
//     VALIDATE(type);
//
//     // initialize type here whenever 0 isn't good enough.
//     // note that char *'s always should be initialized to str_dup("");
//
//     return type;
// }
//
// void free_type(TYPE *type) {
//     if (!IS_VALID(type))
//         return;
//
//     // destroy type
//
//     type_inuse--;
//     INVALIDATE(type);
//
//     type->next=type_free;
//     type_free=type;
// }
//
///////////////////////////////////////////////////////////////////////////////


/* stuff for setting ids */
long	last_pc_id;
long	last_mob_id;
long	last_obj_id;

long get_pc_id(void)
{
    int val;

    val = (current_time <= last_pc_id) ? last_pc_id + 1 : current_time;
    last_pc_id = val;
    return val;
}

long get_mob_id(void)
{
    last_mob_id++;
    return last_mob_id;
}

long get_obj_id(void)
{
    last_obj_id++;
    return last_obj_id;
}


//
// NOTES
//
NOTE_DATA *	note_free=NULL;
int		note_allocated=0;
int		note_inuse=0;

NOTE_DATA *new_note()
{
    NOTE_DATA *note;

    if (note_free == NULL) {
	note = alloc_perm(sizeof(NOTE_DATA));
	note_allocated++;
    } else {
	note = note_free;
	note_free = note_free->next;
    }

    note_inuse++;
    ZEROVAR(note,NOTE_DATA);
    VALIDATE(note);

    note->text		= str_dup("");
    note->subject	= str_dup("");
    note->to_list	= str_dup("");
    note->date		= str_dup("");
    note->sender	= str_dup("");

    return note;
}

void free_note(NOTE_DATA *note)
{
    if (!IS_VALID(note))
	return;

    free_string( note->text    );
    free_string( note->subject );
    free_string( note->to_list );
    free_string( note->date    );
    free_string( note->sender  );

    note_inuse--;
    INVALIDATE(note);

    note->next = note_free;
    note_free   = note;
}


#ifdef HAS_POP3
//
// NOTE_REF
//
NOTE_REF *	note_ref_free=NULL;
int		note_ref_allocated=0;
int		note_ref_inuse=0;

NOTE_REF *new_note_ref()
{
    NOTE_REF *noteref;

    if (note_ref_free == NULL) {
	noteref = alloc_perm(sizeof(NOTE_REF));
	note_ref_allocated++;
    } else {
	noteref = note_ref_free;
	note_ref_free = note_ref_free->next;
    }

    note_ref_inuse++;
    ZEROVAR(noteref,NOTE_REF);
    VALIDATE(noteref);

    return noteref;
}

void free_note_ref(NOTE_REF *noteref)
{
    if (!IS_VALID(noteref))
	return;

    note_ref_inuse--;
    INVALIDATE(noteref);

    noteref->next = note_ref_free;
    note_ref_free = noteref;
}
#endif


//
// BAN_DATA
//
BAN_DATA *	ban_free=NULL;
int		ban_allocated=0;
int		ban_inuse=0;

BAN_DATA *new_ban(void)
{
    BAN_DATA *ban;

    if (ban_free == NULL) {
	ban = alloc_perm(sizeof(BAN_DATA));
	ban_allocated++;
    } else {
	ban = ban_free;
	ban_free = ban_free->next;
    }

    ban_inuse++;
    ZEROVAR(ban,BAN_DATA);
    VALIDATE(ban);

    ban->name=str_dup("");

    return ban;
}

void free_ban(BAN_DATA *ban)
{
    if (!IS_VALID(ban))
	return;

    free_string(ban->name);

    ban_inuse--;
    INVALIDATE(ban);

    ban->next = ban_free;
    ban_free = ban;
}


//
// LOCKOUT_DATA
//
LOCKOUT_DATA *	lockout_free=NULL;
int		lockout_allocated=0;
int		lockout_inuse=0;

LOCKOUT_DATA *new_lockout(void)
{
    LOCKOUT_DATA *lockout;

    if (lockout_free == NULL) {
	lockout = alloc_perm(sizeof(LOCKOUT_DATA));
	lockout_allocated++;
    } else {
	lockout = lockout_free;
	lockout_free = lockout_free->next;
    }

    lockout_inuse++;
    ZEROVAR(lockout,LOCKOUT_DATA);
    VALIDATE(lockout);

    return lockout;
}

void free_lockout(LOCKOUT_DATA *lockout)
{
    if (!IS_VALID(lockout))
	return;

    lockout_inuse--;
    INVALIDATE(lockout);

    lockout->next = lockout_free;
    lockout_free = lockout;
}

//
// PROPERTY_INDEX
//
PROPERTY_INDEX_TYPE *	property_index_free=NULL;
int			property_index_allocated=0;
int			property_index_inuse=0;

PROPERTY_INDEX_TYPE *new_property_index(void) {
    PROPERTY_INDEX_TYPE *pProp;

    if (property_index_free==NULL) {
	pProp=alloc_perm(sizeof(PROPERTY_INDEX_TYPE));
	property_index_allocated++;
    } else {
	pProp=property_index_free;
	property_index_free=property_index_free->next;
    }

    property_index_inuse++;
    ZEROVAR(pProp,PROPERTY_INDEX_TYPE);
    VALIDATE(pProp);

    pProp->key=str_dup("");

    return pProp;
}

void free_property_index(PROPERTY_INDEX_TYPE *pProp) {
    if (!IS_VALID(pProp))
	return;

    free_string(pProp->key);

    property_index_inuse--;
    INVALIDATE(pProp);

    pProp->next=property_index_free;
    property_index_free=pProp;
}


//
// PROPERTY
//
PROPERTY *	property_free=NULL;
int		property_allocated=0;
int		property_inuse=0;

PROPERTY *new_property(void) {
    PROPERTY *property;

    if (property_free == NULL) {
	property = alloc_perm(sizeof(PROPERTY));
	property_allocated++;
    } else {
	property = property_free;
	property_free = property_free->next;
    }

    property_inuse++;
    ZEROVAR(property,PROPERTY);
    VALIDATE(property);

    property->sValue=str_dup("");

    return property;
}

void free_property(PROPERTY *property) {
    if (!IS_VALID(property))
	return;

    free_string(property->sValue);

    property_inuse--;
    INVALIDATE(property);

    property->next = property_free;
    property_free = property;
}


//
// DESCRIPTOR_DATA
//
DESCRIPTOR_DATA *	descriptor_free=NULL;
int			descriptor_allocated=0;
int			descriptor_inuse=0;

DESCRIPTOR_DATA *new_descriptor(int destype)
{
    DESCRIPTOR_DATA *d;

    if (descriptor_free == NULL) {
	d = alloc_perm(sizeof(DESCRIPTOR_DATA));
	descriptor_allocated++;
    } else {
	d = descriptor_free;
	descriptor_free = descriptor_free->next;
    }

    descriptor_inuse++;
    ZEROVAR(d,DESCRIPTOR_DATA);
    VALIDATE(d);

    switch(destype) {
#ifdef HAS_POP3
        case DESTYPE_POP3: d->connected=CON_POP3_GET_USER;break;
#endif
#ifdef HAS_HTTP
        case DESTYPE_HTTP: d->connected=CON_HTTP;break;
#endif
        default:           d->connected=CON_GET_NAME;break;
    }

    d->outsize	= 2000;
    d->outbuf	= alloc_mem( d->outsize );
    d->idle	= time(NULL);
    d->pinbuf	= d->inbuf;
    d->go_ahead	= TRUE;

#ifndef THREADED_DNS
    d->Host=str_dup("");
#endif
    d->showstr_head=str_dup("");
    d->terminaltype=str_dup("");

    return d;
}

void free_descriptor(DESCRIPTOR_DATA *d)
{
    if (!IS_VALID(d))
	return;

#ifndef THREADED_DNS
    free_string(d->Host);
#endif
    free_string(d->showstr_head);
    free_mem(d->outbuf, d->outsize);
    free_string(d->terminaltype);

    descriptor_inuse--;
    INVALIDATE(d);

    d->next = descriptor_free;
    descriptor_free = d;
}


//
// GEN_DATA
//
GEN_DATA *	gen_data_free=NULL;
int		gen_data_allocated=0;
int		gen_data_inuse=0;

GEN_DATA *new_gen_data(void)
{
    GEN_DATA *gen;

    if (gen_data_free == NULL) {
	gen = alloc_perm(sizeof(GEN_DATA));
	gen_data_allocated++;
    } else {
	gen = gen_data_free;
	gen_data_free = gen_data_free->next;
    }

    gen_data_inuse++;
    ZEROVAR(gen,GEN_DATA);
    VALIDATE(gen);

    return gen;
}

void free_gen_data(GEN_DATA *gen)
{
    if (!IS_VALID(gen))
	return;

    gen_data_inuse--;
    INVALIDATE(gen);

    gen->next = gen_data_free;
    gen_data_free = gen;
}


//
// EXTRA_DESCR_DATA
//
EXTRA_DESCR_DATA *	extra_descr_free=NULL;
int			extra_descr_allocated=0;
int			extra_descr_inuse=0;

EXTRA_DESCR_DATA *new_extra_descr(void) {
    EXTRA_DESCR_DATA *ed;

    if (extra_descr_free == NULL) {
	ed = alloc_perm(sizeof(EXTRA_DESCR_DATA));
        extra_descr_allocated++;
    } else {
	ed = extra_descr_free;
	extra_descr_free = extra_descr_free->next;
    }

    extra_descr_inuse++;
    ZEROVAR(ed,EXTRA_DESCR_DATA);
    VALIDATE(ed);

    ed->keyword=str_dup("");
    ed->description=str_dup("");

    return ed;
}

void free_extra_descr(EXTRA_DESCR_DATA *ed)
{
    if (!IS_VALID(ed))
	return;

    free_string(ed->keyword);
    free_string(ed->description);

    extra_descr_inuse--;
    INVALIDATE(ed);

    ed->next = extra_descr_free;
    extra_descr_free = ed;
}


//
// EFFECT_DATA
//
EFFECT_DATA *	effect_free=NULL;
int		effect_allocated=0;
int		effect_inuse=0;

EFFECT_DATA *new_effect(void)
{
    EFFECT_DATA *ef;

    if (effect_free == NULL) {
	ef = alloc_perm(sizeof(EFFECT_DATA));
	effect_allocated++;
    } else {
	ef = effect_free;
	effect_free = effect_free->next;
    }

    effect_inuse++;
    ZEROVAR(ef,EFFECT_DATA);
    VALIDATE(ef);

    return ef;
}

void free_effect(EFFECT_DATA *ef)
{
    if (!IS_VALID(ef))
	return;

    effect_inuse--;
    INVALIDATE(ef);

    ef->next = effect_free;
    effect_free = ef;
}


//
// OBJ_DATA
//
OBJ_DATA *	obj_free=NULL;
int		obj_allocated=0;
int		obj_inuse=0;

OBJ_DATA *new_obj(void)
{
    OBJ_DATA *obj;

    if (obj_free == NULL) {
	obj = alloc_perm(sizeof(OBJ_DATA));
	obj_allocated++;
    } else {
	obj = obj_free;
	obj_free = obj_free->next;
    }

    obj_inuse++;
    ZEROVAR(obj,OBJ_DATA);
    VALIDATE(obj);

    obj->oprog_timer	= -1;

    obj->name		= str_dup("");
    obj->description	= str_dup("");
    obj->short_descr	= str_dup("");
    obj->owner_name	= str_dup("");
    obj->owner_id	= -1;
    obj->material	= str_dup("");

    return obj;
}

void free_obj(OBJ_DATA *obj)
{
    EFFECT_DATA *pef, *pef_next;
    EXTRA_DESCR_DATA *ed, *ed_next;
    PROPERTY *prop,*prop_next;

    if (!IS_VALID(obj))
	return;

    for (pef = obj->affected; pef != NULL; pef = pef_next) {
	pef_next = pef->next;
	free_effect(pef);
    }
    obj->affected = NULL;

    for (ed = obj->extra_descr; ed != NULL; ed = ed_next ) {
	ed_next = ed->next;
	free_extra_descr(ed);
     }
     obj->extra_descr = NULL;

    free_string( obj->name        );
    free_string( obj->description );
    free_string( obj->short_descr );
    free_string( obj->owner_name  );
    free_string( obj->material    );

    prop=obj->property;
    while (prop) {
	prop_next=prop->next;
	free_property(prop);
	prop=prop_next;
    }

    obj_inuse--;
    INVALIDATE(obj);

    obj->next   = obj_free;
    obj_free    = obj;
}


//
// CHAR_DATA
//
CHAR_DATA *	char_free=NULL;
int		char_allocated=0;
int		char_inuse=0;

CHAR_DATA *new_char (void)
{
    CHAR_DATA *ch;
    int i;

    if (char_free == NULL) {
	ch = alloc_perm(sizeof(CHAR_DATA));
	char_allocated++;
    } else {
	ch = char_free;
	char_free = char_free->next;
    }

    char_inuse++;
    ZEROVAR(ch,CHAR_DATA);
    VALIDATE(ch);

    ch->logon                   = current_time;
    for (i = 0; i < 4; i++)
        ch->armor[i]            = 100;
    ch->position                = POS_STANDING;
    ch->hit                     = 20;
    ch->max_hit                 = 20;
    ch->mana                    = 100;
    ch->max_mana                = 100;
    ch->move                    = 100;
    ch->max_move                = 100;
    for (i = 0; i < MAX_STATS; i ++) {
        ch->perm_stat[i] = 13;
        ch->mod_stat[i] = 0;
    }

    ch->mprog_timer	= -1;

    ch->name		= str_dup("");
    ch->short_descr	= str_dup("");
    ch->long_descr	= str_dup("");
    ch->description	= str_dup("");
    ch->prompt		= str_dup("");
    ch->prefix		= str_dup("");
    ch->material	= str_dup("");

    return ch;
}

void free_char (CHAR_DATA *ch)
{
    OBJ_DATA *obj;
    OBJ_DATA *obj_next;
    EFFECT_DATA *pef;
    EFFECT_DATA *pef_next;
    MEM_DATA *mem;
    PROPERTY *prop,*prop_next;

    if (!IS_VALID(ch))
	return;

    for (obj = ch->carrying; obj != NULL; obj = obj_next) {
	obj_next = obj->next_content;
	extract_obj(obj);
    }

    for (pef = ch->affected; pef != NULL; pef = pef_next) {
	pef_next = pef->next;
	effect_remove(ch,pef);
    }

    while (ch->memory) {
	mem=ch->memory;
	ch->memory=mem->next;
	free_mobmem(mem);
    }

    if (ch->clan && ch->clan->player==ch)
	    ch->clan->player=NULL;

    free_string(ch->name	);
    free_string(ch->short_descr	);
    free_string(ch->long_descr	);
    free_string(ch->description	);
    free_string(ch->prompt	);
    free_string(ch->prefix	);
    free_string(ch->material	);
    free_note  (ch->pnote	);

    free_pcdata(ch->pcdata	);

    prop=ch->property;
    while (prop) {
	prop_next=prop->next;
	free_property(prop);
	prop=prop_next;
    }

    char_inuse--;
    INVALIDATE(ch);

    ch->next = char_free;
    char_free  = ch;
}


//
// PC_DATA
//
PC_DATA *	pcdata_free=NULL;
int		pcdata_allocated=0;
int		pcdata_inuse=0;

PC_DATA *new_pcdata(void)
{
    PC_DATA *pcdata;

    if (pcdata_free == NULL) {
	pcdata = alloc_perm(sizeof(PC_DATA));
	pcdata_allocated++;
    } else {
	pcdata = pcdata_free;
	pcdata_free = pcdata_free->next;
    }

    pcdata_inuse++;
    ZEROVAR(pcdata,PC_DATA);
    VALIDATE(pcdata);

    pcdata->buffer	= new_buf();
    pcdata->default_lines= PAGELEN;

    pcdata->pwd		= str_dup("");
    pcdata->bamfin	= str_dup("");
    pcdata->bamfout	= str_dup("");
    pcdata->title	= str_dup("");
    pcdata->afk_text	= str_dup("");
    pcdata->whoname	= str_dup("");
    pcdata->pueblo_picture= str_dup("");
#ifdef HAS_ALTS
    pcdata->alts	= str_dup("");
    pcdata->uberalt	= str_dup("");
#endif

    return pcdata;
}

void free_pcdata(PC_DATA *pcdata)
{
#ifdef HAS_POP3
    NOTE_REF *noteref;
#endif
    NOTEBK_DATA *notebook,*notebook_next;
    ALIAS_TYPE *alias,*alias_next;

    if (!IS_VALID(pcdata))
	return;

    free_string(pcdata->pwd	);
    free_string(pcdata->bamfin	);
    free_string(pcdata->bamfout	);
    free_string(pcdata->title	);
    free_string(pcdata->afk_text);
    free_string(pcdata->whoname	);
    free_string(pcdata->pueblo_picture);
#ifdef HAS_ALTS
    free_string(pcdata->alts	);
    free_string(pcdata->uberalt	);
#endif
    free_buf(pcdata->buffer	);

    for (alias=pcdata->aliases;alias;alias=alias_next) {
	alias_next=alias->next;
	free_alias(alias);
    }

    for (notebook=pcdata->notebook;notebook;notebook=notebook_next) {
	notebook_next=notebook->next;
	free_notebook(notebook);
    }

#ifdef HAS_POP3
    while(pcdata->noteref) {
        noteref=pcdata->noteref;
        pcdata->noteref=noteref->next;
        free_note_ref(noteref);
    }
#endif

    pcdata_inuse--;
    INVALIDATE(pcdata);

    pcdata->next = pcdata_free;
    pcdata_free = pcdata;
}


//
// BUFFER
//
BUFFER *	buffer_free=NULL;
int		buffer_allocated=0;
int		buffer_inuse=0;

const int buf_size[MAX_BUF_LIST] = {
    16,32,64,128,256,1024,2048,4096,8192,16384
};

/* local procedure for finding the next acceptable size */
/* -1 indicates out-of-boundary error */
int get_size (int val) {
    int i;

    for (i = 0; i < MAX_BUF_LIST; i++)
	if (buf_size[i] >= val)
	{
	    return buf_size[i];
	}

    return -1;
}

BUFFER *new_buf() {
    BUFFER *buffer;

    if (buffer_free == NULL) {
	buffer = alloc_perm(sizeof(BUFFER));
	buffer_allocated++;
    } else {
	buffer = buffer_free;
	buffer_free = buffer_free->next;
    }

    buffer_inuse++;
    ZEROVAR(buffer,BUFFER);
    VALIDATE(buffer);

    buffer->state	= BUFFER_SAFE;
    buffer->size	= get_size(BASE_BUF);

    buffer->string	= alloc_mem(buffer->size);
    buffer->string[0]	= '\0';

    return buffer;
}

BUFFER *new_buf_size(int size)
{
    BUFFER *buffer;

    if (buffer_free == NULL) {
        buffer = alloc_perm(sizeof(BUFFER));
	buffer_allocated++;
    } else {
        buffer = buffer_free;
        buffer_free = buffer_free->next;
    }

    buffer_inuse++;
    ZEROVAR(buffer,BUFFER);
    VALIDATE(buffer);

    buffer->state       = BUFFER_SAFE;
    buffer->size        = get_size(size);
    if (buffer->size == -1) {
        bugf("new_buf: buffer size %d too large.",size);
        abort();
    }
    buffer->string      = alloc_mem(buffer->size);
    buffer->string[0]   = '\0';

    return buffer;
}

void free_buf(BUFFER *buffer)
{
    if (!IS_VALID(buffer))
	return;

    free_mem(buffer->string,buffer->size);

    buffer_inuse--;
    INVALIDATE(buffer);

    // this should not be set to zero!
    buffer->state  = BUFFER_FREED;

    buffer->next  = buffer_free;
    buffer_free      = buffer;
}

bool add_buf(BUFFER *buffer, char *string) {
    int len;
    char *oldstr;
    int oldsize;

    oldstr = buffer->string;
    oldsize = buffer->size;

    if (buffer->state == BUFFER_OVERFLOW) // don't waste time on bad strings!
	return FALSE;

    len = strlen(buffer->string) + strlen(string) + 1;

    while (len >= buffer->size) /* increase the buffer size */
    {
	buffer->size 	= get_size(buffer->size + 1);
	{
	    if (buffer->size == -1) /* overflow */
	    {
		buffer->size = oldsize;
		buffer->state = BUFFER_OVERFLOW;
		bugf("add_buf: buffer overflow past size %d",buffer->size);
		return FALSE;
	    }
  	}
    }

    if (buffer->size != oldsize)
    {
	buffer->string	= alloc_mem(buffer->size);

	strcpy(buffer->string,oldstr);
	free_mem(oldstr,oldsize);
    }

    strcat(buffer->string,string);
    return TRUE;
}

void clear_buf(BUFFER *buffer) {
    buffer->string[0] = '\0';
    buffer->state     = BUFFER_SAFE;
}

char *buf_string(BUFFER *buffer) {
    return buffer->string;
}


//
// RESET_DATA
//
RESET_DATA *	reset_free=NULL;
int		reset_allocated=0;
int		reset_inuse=0;

RESET_DATA *new_reset( void )
{
    RESET_DATA *pReset;

    if ( reset_free==NULL ) {
	pReset		= alloc_perm( sizeof(RESET_DATA) );
	reset_allocated++;
    } else {
	pReset		= reset_free;
	reset_free	= reset_free->next;
    }

    reset_inuse++;
    ZEROVAR(pReset,RESET_DATA);
    VALIDATE(pReset);

    pReset->command	= 'X';

    return pReset;
}

void free_reset( RESET_DATA *pReset )
{
    if (!IS_VALID(pReset))
	return;

    reset_inuse--;
    INVALIDATE(pReset);

    pReset->next            = reset_free;
    reset_free              = pReset;
}


//
// AREA_DATA
//
AREA_DATA *	area_free=NULL;
int		area_allocated=0;
int		area_inuse=0;

AREA_DATA *new_area( void ) {
    AREA_DATA *pArea;

    if ( area_free==NULL ) {
	pArea		= alloc_perm( sizeof(AREA_DATA) );
	area_allocated++;
    } else {
	pArea		= area_free;
	area_free	= area_free->next;
    }

    area_inuse++;
    ZEROVAR(pArea,AREA_DATA);
    VALIDATE(pArea);

    pArea->recall	= ROOM_VNUM_TEMPLE;
    pArea->area_flags	= AREA_LOADING;
    pArea->security	= 1;
    pArea->age		= 15;			// force repopulate
    pArea->vnum		= area_allocated;		/* OLC 1.1b */

    pArea->aprog_timer	= -1;

    pArea->filename	= str_dup("nofile");
    pArea->name		= str_dup("");
    pArea->builders	= str_dup("");
    pArea->creator	= str_dup("");
    pArea->urlprefix	= str_dup("");
    pArea->comment	= str_dup("");
    pArea->areaprogram	= str_dup("");
    pArea->area_vars	= str_dup("");
    pArea->description	= str_dup("\n\r");

    return pArea;
}

void free_area( AREA_DATA *pArea ) {
    if (!IS_VALID(pArea))
	return;

    free_string( pArea->name		);
    free_string( pArea->filename	);
    free_string( pArea->builders	);
    free_string( pArea->creator		);
    free_string( pArea->urlprefix	);
    free_string( pArea->comment		);
    free_string( pArea->description	);
    free_string( pArea->areaprogram	);
    free_string( pArea->area_vars	);

    area_inuse--;
    INVALIDATE(pArea);

    pArea->next		=   area_free->next;
    area_free		=   pArea;
}


//
// EXIT_DATA
//
EXIT_DATA *	exit_free=NULL;
int		exit_allocated=0;
int		exit_inuse=0;

EXIT_DATA *new_exit( void ) {
    EXIT_DATA *pExit;

    if ( exit_free==NULL ) {
        pExit           =   alloc_perm( sizeof(EXIT_DATA) );
        exit_allocated++;
    } else {
        pExit           =   exit_free;
        exit_free       =   exit_free->next;
    }

    exit_inuse++;
    ZEROVAR(pExit,EXIT_DATA);
    VALIDATE(pExit);

    pExit->key          =   -1;
    pExit->keyword	= str_dup("");
    pExit->description	= str_dup("");

    return pExit;
}

void free_exit( EXIT_DATA *pExit )
{
    if (!IS_VALID(pExit))
	return;

    free_string( pExit->keyword		);
    free_string( pExit->description	);

    exit_inuse--;
    INVALIDATE(pExit);

    pExit->next         =   exit_free;
    exit_free           =   pExit;
}


//
// ROOM_INDEX_DATA
//
ROOM_INDEX_DATA	*	room_index_free=NULL;
int			room_index_allocated=0;
int			room_index_inuse=0;

ROOM_INDEX_DATA *new_room_index( void ) {
    ROOM_INDEX_DATA *pRoom;

    if ( room_index_free==NULL ) {
        pRoom           =   alloc_perm( sizeof(ROOM_INDEX_DATA) );
        room_index_allocated++;
    } else {
        pRoom           =   room_index_free;
        room_index_free =   room_index_free->next;
    }

    room_index_inuse++;
    ZEROVAR(pRoom,ROOM_INDEX_DATA);
    VALIDATE(pRoom);

    pRoom->rprog_timer		= -1;

    pRoom->name			= str_dup("");
    pRoom->description		= str_dup("");
    pRoom->pueblo_picture	= str_dup("");

    return pRoom;
}

void free_room_index( ROOM_INDEX_DATA *pRoom ) {
    int door;
    EXTRA_DESCR_DATA *pExtra;
    RESET_DATA *pReset;

    if (!IS_VALID(pRoom))
	return;

    free_string( pRoom->name		);
    free_string( pRoom->description	);
    free_string( pRoom->pueblo_picture	);

    for ( door = 0; door < MAX_DIR; door++ )
        if ( pRoom->exit[door] )
            free_exit( pRoom->exit[door] );

    for ( pExtra = pRoom->extra_descr; pExtra; pExtra = pExtra->next )
        free_extra_descr( pExtra );

    for ( pReset = pRoom->reset_first; pReset; pReset = pReset->next )
        free_reset( pReset );

    room_index_inuse--;
    INVALIDATE(pRoom);

    pRoom->next     =   room_index_free;
    room_index_free =   pRoom;
}


//
// SHOP_DATA
//
SHOP_DATA *	shop_free=NULL;
int		shop_allocated=0;
int		shop_inuse=0;

SHOP_DATA *new_shop( void ) {
    SHOP_DATA *pShop;

    if ( shop_free==NULL ) {
        pShop           =   alloc_perm( sizeof(SHOP_DATA) );
        shop_allocated++;
    } else {
        pShop           =   shop_free;
        shop_free       =   shop_free->next;
    }

    shop_inuse++;
    ZEROVAR(pShop,SHOP_DATA);
    VALIDATE(pShop);

    pShop->profit_buy   =   100;
    pShop->profit_sell  =   100;
    pShop->open_hour    =   0;
    pShop->close_hour   =   23;
    pShop->type		=   SHOP_NORMAL;

    return pShop;
}

void free_shop( SHOP_DATA *pShop ) {
    if (!IS_VALID(pShop))
	return;

    shop_inuse--;
    INVALIDATE(pShop);

    pShop->next = shop_free;
    shop_free   = pShop;
}


//
// OBJ_INDEX_DATA
//
OBJ_INDEX_DATA *	obj_index_free;
int			obj_index_allocated=0;
int			obj_index_inuse=0;

OBJ_INDEX_DATA *new_obj_index( void ) {
    OBJ_INDEX_DATA *pObj;

    if ( obj_index_free==NULL ) {
        pObj           =   alloc_perm( sizeof(OBJ_INDEX_DATA) );
        obj_index_allocated++;
    } else {
        pObj            =   obj_index_free;
        obj_index_free  =   obj_index_free->next;
    }

    obj_index_inuse++;
    ZEROVAR(pObj,OBJ_INDEX_DATA);
    VALIDATE(pObj);

    pObj->name          =   str_dup("");
    pObj->short_descr   =   str_dup("");
    pObj->description   =   str_dup("");
    pObj->material	=   str_dup("");
    pObj->pueblo_picture=   str_dup("");
    pObj->item_type     =   ITEM_TRASH;
    pObj->condition	=   100;


    return pObj;
}

void free_obj_index( OBJ_INDEX_DATA *pObj ) {
    EXTRA_DESCR_DATA *pExtra;
    EFFECT_DATA *pef;

    if (!IS_VALID(pObj))
	return;

    free_string( pObj->name		);
    free_string( pObj->short_descr	);
    free_string( pObj->description	);
    free_string( pObj->pueblo_picture	);
    free_string( pObj->material		);

    for ( pef = pObj->affected; pef; pef = pef->next )
        free_effect( pef );

    for ( pExtra = pObj->extra_descr; pExtra; pExtra = pExtra->next )
        free_extra_descr( pExtra );

    obj_index_inuse--;
    INVALIDATE(pObj);

    pObj->next              = obj_index_free;
    obj_index_free          = pObj;
    obj_index_inuse--;
}


//
// MOB_INDEX_DATA
//
MOB_INDEX_DATA *	mob_index_free=NULL;
int			mob_index_allocated=0;
int			mob_index_inuse=0;

MOB_INDEX_DATA *new_mob_index( void ) {
    MOB_INDEX_DATA *pMob;

    if ( mob_index_free==NULL ) {
        pMob           =   alloc_perm( sizeof(MOB_INDEX_DATA) );
        mob_index_allocated++;
    } else {
        pMob            =   mob_index_free;
        mob_index_free  =   mob_index_free->next;
    }

    mob_index_inuse++;
    ZEROVAR(pMob,MOB_INDEX_DATA);
    VALIDATE(pMob);

    pMob->player_name   =   str_dup( "" );
    pMob->short_descr   =   str_dup( "" );
    pMob->long_descr    =   str_dup( "" );
    pMob->description   =   str_dup( "" );
    pMob->pueblo_picture=   str_dup( "" );
    pMob->material	=   str_dup( "" );

    STR_SET_BIT(pMob->strbit_act,ACT_IS_NPC);
    STR_SET_BIT(pMob->strbit_act_read,ACT_IS_NPC);

    pMob->start_pos	=   POS_STANDING;
    pMob->default_pos	=   POS_STANDING;

    return pMob;
}

void free_mob_index( MOB_INDEX_DATA *pMob ) {
    if (!IS_VALID(pMob))
	return;

    free_string( pMob->player_name	);
    free_string( pMob->short_descr	);
    free_string( pMob->long_descr	);
    free_string( pMob->description	);
    free_string( pMob->pueblo_picture	);
    free_string( pMob->material		);

    free_shop( pMob->pShop );

    mob_index_inuse--;
    INVALIDATE(pMob);

    pMob->next              = mob_index_free;
    mob_index_free          = pMob;
}


//
// MOB_PROGS
//
TCLPROG_CODE *	mprog_code_index_free=NULL;
int		mprog_code_index_allocated=0;
int		mprog_code_index_inuse=0;

TCLPROG_CODE *new_mprog_code_index(void) {
    TCLPROG_CODE *pCode;

    if ( mprog_code_index_free==NULL ) {
        pCode           	=   alloc_perm( sizeof(TCLPROG_CODE) );
	mprog_code_index_allocated++;
    } else {
        pCode           	=   mprog_code_index_free;
        mprog_code_index_free  	=   mprog_code_index_free->next;
    }

    mprog_code_index_inuse++;
    ZEROVAR(pCode,TCLPROG_CODE);
    VALIDATE(pCode);

    pCode->code		= str_dup("");
    pCode->title	= str_dup("");

    return pCode;
}

void free_mprog_code_index( TCLPROG_CODE *pCode ) {
    if (!IS_VALID(pCode))
	return;

    free_string( pCode->code	);
    free_string( pCode->title	);

    mprog_code_index_inuse--;
    INVALIDATE(pCode);

    pCode->next             = mprog_code_index_free;
    mprog_code_index_free   = pCode;
}

TCLPROG_LIST *	mprog_list_free=NULL;
int		mprog_list_allocated=0;
int		mprog_list_inuse=0;

TCLPROG_LIST *new_mprog_list(void) {
    TCLPROG_LIST *list;

    if (mprog_list_free == NULL) {
	list = alloc_perm(sizeof(TCLPROG_LIST));
	mprog_list_allocated++;
    } else {
	list = mprog_list_free;
	mprog_list_free = mprog_list_free->next;
    }

    mprog_list_inuse++;
    ZEROVAR(list,TCLPROG_LIST);
    VALIDATE(list);

    list->name		= str_dup("");
    list->trig_phrase	= str_dup("");
    list->code		= str_dup("");

    return list;
}

void free_mprog_list(TCLPROG_LIST *list) {
    if (!IS_VALID(list))
	return;

    free_string(list->name		);
    free_string(list->trig_phrase	);
    free_string(list->code		);

    mprog_list_inuse--;
    INVALIDATE(list);

    list->next = mprog_list_free;
    mprog_list_free = list;
}


//
// AREA_PROGS
//
TCLPROG_CODE *	aprog_code_index_free=NULL;
int		aprog_code_index_allocated=0;
int		aprog_code_index_inuse=0;

TCLPROG_CODE *new_aprog_code_index(void) {
    TCLPROG_CODE *pCode;

    if ( aprog_code_index_free==NULL ) {
        pCode           	=   alloc_perm( sizeof(TCLPROG_CODE) );
	aprog_code_index_allocated++;
    } else {
        pCode           	=   aprog_code_index_free;
        aprog_code_index_free  	=   aprog_code_index_free->next;
    }

    aprog_code_index_inuse++;
    ZEROVAR(pCode,TCLPROG_CODE);
    VALIDATE(pCode);

    pCode->code		= str_dup("");
    pCode->title	= str_dup("");

    return pCode;
}

void free_aprog_code_index( TCLPROG_CODE *pCode ) {
    if (!IS_VALID(pCode))
	return;

    free_string( pCode->code	);
    free_string( pCode->title	);

    aprog_code_index_inuse--;
    INVALIDATE(pCode);

    pCode->next             = aprog_code_index_free;
    aprog_code_index_free   = pCode;
}

TCLPROG_LIST *	aprog_list_free=NULL;
int		aprog_list_allocated=0;
int		aprog_list_inuse=0;

TCLPROG_LIST *new_aprog_list(void) {
    TCLPROG_LIST *list;

    if (aprog_list_free == NULL) {
	list = alloc_perm(sizeof(TCLPROG_LIST));
	aprog_list_allocated++;
    } else {
	list = aprog_list_free;
	aprog_list_free = aprog_list_free->next;
    }

    aprog_list_inuse++;
    ZEROVAR(list,TCLPROG_LIST);
    VALIDATE(list);

    list->name		= str_dup("");
    list->trig_phrase	= str_dup("");
    list->code		= str_dup("");

    return list;
}

void free_aprog_list(TCLPROG_LIST *list) {
    if (!IS_VALID(list))
	return;

    free_string(list->name		);
    free_string(list->trig_phrase	);
    free_string(list->code		);

    aprog_list_inuse--;
    INVALIDATE(list);

    list->next = aprog_list_free;
    aprog_list_free = list;
}


//
// OBJECT_PROGS
//
TCLPROG_CODE *	oprog_code_index_free=NULL;
int		oprog_code_index_allocated=0;
int		oprog_code_index_inuse=0;

TCLPROG_CODE *new_oprog_code_index(void) {
    TCLPROG_CODE *pCode;

    if ( oprog_code_index_free==NULL ) {
        pCode           	=   alloc_perm( sizeof(TCLPROG_CODE) );
	oprog_code_index_allocated++;
    } else {
        pCode           	=   oprog_code_index_free;
        oprog_code_index_free  	=   oprog_code_index_free->next;
    }

    oprog_code_index_inuse++;
    ZEROVAR(pCode,TCLPROG_CODE);
    VALIDATE(pCode);

    pCode->code		= str_dup("");
    pCode->title	= str_dup("");

    return pCode;
}

void free_oprog_code_index( TCLPROG_CODE *pCode ) {
    if (!IS_VALID(pCode))
	return;

    free_string( pCode->code	);
    free_string( pCode->title	);

    oprog_code_index_inuse--;
    INVALIDATE(pCode);

    pCode->next             = oprog_code_index_free;
    oprog_code_index_free   = pCode;
}

TCLPROG_LIST *	oprog_list_free=NULL;
int		oprog_list_allocated=0;
int		oprog_list_inuse=0;

TCLPROG_LIST *new_oprog_list(void) {
    TCLPROG_LIST *list;

    if (oprog_list_free == NULL) {
	list = alloc_perm(sizeof(TCLPROG_LIST));
	oprog_list_allocated++;
    } else {
	list = oprog_list_free;
	oprog_list_free = oprog_list_free->next;
    }

    list->name		= str_dup("");
    list->trig_phrase	= str_dup("");
    list->code		= str_dup("");

    oprog_list_inuse++;
    ZEROVAR(list,TCLPROG_LIST);
    VALIDATE(list);

    return list;
}

void free_oprog_list(TCLPROG_LIST *list) {
    if (!IS_VALID(list))
	return;

    free_string(list->name		);
    free_string(list->trig_phrase	);
    free_string(list->code		);

    oprog_list_inuse--;
    INVALIDATE(list);

    list->next = oprog_list_free;
    oprog_list_free = list;
}

//
// ROOM_PROGS
//
TCLPROG_CODE *	rprog_code_index_free=NULL;
int		rprog_code_index_allocated=0;
int		rprog_code_index_inuse=0;

TCLPROG_CODE *new_rprog_code_index(void) {
    TCLPROG_CODE *pCode;

    if ( rprog_code_index_free==NULL ) {
        pCode           	=   alloc_perm( sizeof(TCLPROG_CODE) );
	rprog_code_index_allocated++;
    } else {
        pCode           	=   rprog_code_index_free;
        rprog_code_index_free  	=   rprog_code_index_free->next;
    }

    rprog_code_index_inuse++;
    ZEROVAR(pCode,TCLPROG_CODE);
    VALIDATE(pCode);

    pCode->code		= str_dup("");
    pCode->title	= str_dup("");

    return pCode;
}

void free_rprog_code_index( TCLPROG_CODE *pCode ) {
    if (!IS_VALID(pCode))
	return;

    free_string( pCode->code	);
    free_string( pCode->title	);

    rprog_code_index_inuse--;
    INVALIDATE(pCode);

    pCode->next             = rprog_code_index_free;
    rprog_code_index_free   = pCode;
}

TCLPROG_LIST *	rprog_list_free=NULL;
int		rprog_list_allocated=0;
int		rprog_list_inuse=0;

TCLPROG_LIST *new_rprog_list(void) {
    TCLPROG_LIST *list;

    if (rprog_list_free == NULL) {
	list = alloc_perm(sizeof(TCLPROG_LIST));
	rprog_list_allocated++;
    } else {
	list = rprog_list_free;
	rprog_list_free = rprog_list_free->next;
    }

    rprog_list_inuse++;
    ZEROVAR(list,TCLPROG_LIST);
    VALIDATE(list);

    list->name		= str_dup("");
    list->trig_phrase	= str_dup("");
    list->code		= str_dup("");

    return list;
}

void free_rprog_list(TCLPROG_LIST *list) {
    if (!IS_VALID(list))
	return;

    free_string(list->name		);
    free_string(list->trig_phrase	);
    free_string(list->code		);

    rprog_list_inuse--;
    INVALIDATE(list);

    list->next = rprog_list_free;
    rprog_list_free = list;
}

//
// RANK_INFO
//
RANK_INFO *	clanrank_free=NULL;
int		clanrank_allocated=0;
int		clanrank_inuse=0;

RANK_INFO *new_clanrank( void ) {
    RANK_INFO *pRank;

    if ( clanrank_free==NULL ) {
        pRank		= alloc_perm( sizeof(RANK_INFO) );
        clanrank_allocated++;
    } else {
        pRank		= clanrank_free;
        clanrank_free	= clanrank_free->next;
    }

    clanrank_inuse++;
    ZEROVAR(pRank,RANK_INFO);
    VALIDATE(pRank);

    pRank->rank_name=str_dup("");
    pRank->who_name=str_dup("");

    return pRank;
}

void free_clanrank( RANK_INFO *pRank ) {
    if (!IS_VALID(pRank))
	return;

    free_string(pRank->rank_name);
    free_string(pRank->who_name	);

    clanrank_inuse--;
    INVALIDATE(pRank);

    pRank->next = clanrank_free;
    clanrank_free   = pRank;
}

//
// CLAN_INFO
//
CLAN_INFO *	clan_free=NULL;
int		clan_allocated=0;
int		clan_inuse=0;

CLAN_INFO *new_clan( void ) {
    CLAN_INFO *pClan;

    if ( clan_free==NULL ) {
        pClan		= alloc_perm( sizeof(CLAN_INFO) );
        clan_allocated++;
    } else {
        pClan		= clan_free;
        clan_free	= clan_free->next;
    }

    clan_inuse++;
    ZEROVAR(pClan,CLAN_INFO);
    VALIDATE(pClan);

    pClan->hall		= 1;
    pClan->recall	= 1;
    pClan->independent	= TRUE;

    pClan->name		= str_dup("");
    pClan->who_name	= str_dup("");
    pClan->description	= str_dup("");
    pClan->url		= str_dup("");

    return pClan;
}

void free_clan( CLAN_INFO *pClan ) {
    if (!IS_VALID(pClan))
	return;

    free_string(pClan->name		);
    free_string(pClan->who_name		);
    free_string(pClan->description	);
    free_string(pClan->url		);

    clan_inuse--;
    INVALIDATE(pClan);

    pClan->next = clan_free;
    clan_free   = pClan;
}

//
// CLANMEMBER_TYPE
//
CLANMEMBER_TYPE *	clanmember_free=NULL;
int			clanmember_allocated=0;
int			clanmember_inuse=0;

CLANMEMBER_TYPE *new_clanmember( void ) {
    CLANMEMBER_TYPE *pMember;

    if ( clanmember_free==NULL ) {
        pMember           =   alloc_perm( sizeof(CLANMEMBER_TYPE) );
        clanmember_allocated++;
    } else {
        pMember         =   clanmember_free;
        clanmember_free =   clanmember_free->next;
    }

    clanmember_inuse++;
    ZEROVAR(pMember,CLANMEMBER_TYPE);
    VALIDATE(pMember);

    pMember->lastonline	= time(NULL);
    pMember->playername	= str_dup("");

    return pMember;
}

void free_clanmember( CLANMEMBER_TYPE *pMember ) {
    if (!IS_VALID(pMember))
	return;

    free_string(pMember->playername);

    clanmember_inuse--;
    INVALIDATE(pMember);

    pMember->next = clanmember_free;
    clanmember_free   = pMember;
}

//
// MEM_DATA
//
MEM_DATA *	mobmem_free=NULL;
int		mobmem_allocated=0;
int		mobmem_inuse=0;

MEM_DATA *new_mobmem( void ) {
    MEM_DATA *pMem;

    if ( mobmem_free==NULL ) {
        pMem           =   alloc_perm( sizeof(MEM_DATA) );
        mobmem_allocated++;
    } else {
        pMem		=   mobmem_free;
        mobmem_free	=   mobmem_free->next;
    }

    mobmem_inuse++;
    ZEROVAR(pMem,MEM_DATA);
    VALIDATE(pMem);

    return pMem;
}

void free_mobmem( MEM_DATA *pMem ) {
    if (!IS_VALID(pMem))
	return;

    mobmem_inuse--;
    INVALIDATE(pMem);

    pMem->next = mobmem_free;
    mobmem_free   = pMem;
}

//
// SOCIAL_TYPE
//
SOCIAL_TYPE *	social_free=NULL;
int		social_allocated=0;
int		social_inuse=0;

SOCIAL_TYPE *new_social(void) {
    SOCIAL_TYPE *pSocial;

    if ( social_free==NULL ) {
        pSocial=alloc_perm( sizeof(SOCIAL_TYPE) );
        social_allocated++;
    } else {
        pSocial		= social_free;
        social_free	= social_free->next;
    }

    ZEROVAR(pSocial,SOCIAL_TYPE);
    VALIDATE(pSocial);

    pSocial->name		= str_dup("");
    pSocial->char_no_arg	= str_dup("");
    pSocial->others_no_arg	= str_dup("");
    pSocial->char_found		= str_dup("");
    pSocial->others_found	= str_dup("");
    pSocial->vict_found		= str_dup("");
    pSocial->char_not_found	= str_dup("");
    pSocial->char_auto		= str_dup("");
    pSocial->others_auto	= str_dup("");

    return pSocial;
}

void add_social(SOCIAL_TYPE *pSocial ) {
    SOCIAL_TYPE *list;
    SOCIAL_TYPE **last;

    social_inuse++;

    last=&social_hash[(int)(pSocial->name[0])];
    list=*last;

    while (list && strcmp(list->name,pSocial->name)<=0) {
	last=&list->next;
	list=*last;
    }

    *last=pSocial;
    pSocial->next=list;

}

void remove_social(SOCIAL_TYPE *pSocial) {
    SOCIAL_TYPE *list;
    SOCIAL_TYPE **last;
    int hash,hash_offset;

    hash=(int)(pSocial->name[0]);

    for (hash_offset=0;hash_offset<256;hash_offset++) {
	last=&social_hash[(hash+hash_offset)&255];
	list=*last;

	while (list && list!=pSocial) {
	    last=&list->next;
	    list=*last;
	}
	if (list==pSocial) break;
    }

    if (!list) return; // social was never in the list to begin with

    social_inuse--;

    *last=list->next;
    list->next=NULL;
}

void free_social( SOCIAL_TYPE *pSocial ) {
    if (!IS_VALID(pSocial))
	return;

    free_string(pSocial->name		);
    free_string(pSocial->char_no_arg	);
    free_string(pSocial->others_no_arg	);
    free_string(pSocial->char_found	);
    free_string(pSocial->others_found	);
    free_string(pSocial->vict_found	);
    free_string(pSocial->char_not_found	);
    free_string(pSocial->char_auto	);
    free_string(pSocial->others_auto	);

    social_inuse--;
    INVALIDATE(pSocial);

    pSocial->next = social_free;
    social_free   = pSocial;
}


//
// ALIAS_TYPE
//
ALIAS_TYPE *	alias_free=NULL;
int		alias_allocated=0;
int		alias_inuse=0;

ALIAS_TYPE *new_alias(void) {
    ALIAS_TYPE *pAlias;

    if (alias_free==NULL) {
        pAlias=alloc_perm(sizeof(ALIAS_TYPE));
	alias_allocated++;
    } else {
	pAlias=alias_free;
	alias_free=alias_free->next;
    }

    alias_inuse++;
    ZEROVAR(pAlias,ALIAS_TYPE);
    VALIDATE(pAlias);

    pAlias->alias	= str_dup("");
    pAlias->command	= str_dup("");

    return pAlias;
}

void free_alias(ALIAS_TYPE *pAlias) {
    if (!IS_VALID(pAlias))
	return;

    free_string(pAlias->alias	);
    free_string(pAlias->command	);

    alias_inuse--;
    INVALIDATE(pAlias);

    pAlias->next=alias_free;
    alias_free=pAlias;
}


//
// NOTEBK_DATA
//
NOTEBK_DATA *	notebook_free=NULL;
int		notebook_allocated=0;
int		notebook_inuse=0;

NOTEBK_DATA *new_notebook() {
    NOTEBK_DATA *notebook;

    if (notebook_free == NULL ) {
        notebook=alloc_perm(sizeof(NOTEBK_DATA));
	notebook_allocated++;
    } else {
	notebook=notebook_free;
	notebook_free=notebook_free->next;
    }

    notebook_inuse++;
    ZEROVAR(notebook,NOTEBK_DATA);
    VALIDATE(notebook);

    notebook->text	= str_dup("");
    notebook->subject	= str_dup("");

    return notebook;
}

void free_notebook(NOTEBK_DATA *notebook) {
    if (!IS_VALID(notebook))
	return;

    free_string( notebook->text    );
    free_string( notebook->subject );

    notebook_inuse--;
    INVALIDATE(notebook);

    notebook->next	= notebook_free;
    notebook_free	= notebook;
}


//
// FS_ENTRY
//
FS_ENTRY *	fsentry_free=NULL;
int		fsentry_allocated=0;
int		fsentry_inuse=0;

FS_ENTRY *new_fsentry(void) {
    FS_ENTRY *fsentry;

    if (fsentry_free==NULL) {
	fsentry=alloc_perm(sizeof(FS_ENTRY));
	fsentry_allocated++;
    } else {
	fsentry=fsentry_free;
	fsentry_free=fsentry_free->next;
    }

    fsentry_inuse++;
    ZEROVAR(fsentry,FS_ENTRY);
    VALIDATE(fsentry);

    fsentry->filename=str_dup("");

    return fsentry;
}

void free_fsentry (FS_ENTRY *fsentry) {
    free_string(fsentry->filename);
    // note: don't free the data please :-)

    fsentry_inuse--;
    INVALIDATE(fsentry);

    fsentry->next=fsentry_free;
    fsentry_free=fsentry;
}


//
// event procedures
//
EVENT_DATA *	event_free=NULL;
int		event_allocated=0;
int		event_inuse=0;

EVENT_DATA *new_event(void) {
    EVENT_DATA *event;

    if (event_free==NULL) {
	event=alloc_perm(sizeof(EVENT_DATA));
	event_allocated++;
    } else {
	event=event_free;
	event_free=event_free->next;
    }

    event_inuse++;
    ZEROVAR(event,EVENT_DATA);
    VALIDATE(event);

    return event;
}

void free_event (EVENT_DATA *event) {
    if (!IS_VALID(event))
	return;

    if (event->data!=NULL)
	free(event->data);

    event_inuse--;
    INVALIDATE(event);

    event->next=event_free;
    event_free=event;
}

bool has_event(CHAR_DATA *ch,EVENT_FUN *fun) {
    EVENT_DATA *event;

    event=event_list;
    while (event!=NULL) {
	if (event->ch==ch && event->fun==fun)
		return TRUE;
	event=event->next;
    }
    return FALSE;
}

EVENT_DATA *create_event(int delay,CHAR_DATA *ch,CHAR_DATA *victim,OBJ_DATA *obj,EVENT_FUN *fun,void *data) {
    EVENT_DATA *event=new_event();

    event->delay=delay;
    event->ch=ch;
    event->victim=victim;
    event->object=obj;
    event->data=data;
    event->fun=fun;

    event->next=event_new;
    event_new=event;

    return event;
}

void remove_event(EVENT_DATA *oldevent) {
    EVENT_DATA *event;

    if (oldevent==NULL) {
	bugf("remove_event(): oldevent=NULL");
	return;
    }

    if (oldevent==event_list) {
	event_list=event_list->next;
    } else {
	event=event_list;
	while (event->next!=oldevent)
	    event=event->next;
	event->next=oldevent->next;
    }
    free_event(oldevent);
}

//
// help recycling
//
HELP_DATA *	help_free=NULL;
int		help_allocated=0;
int		help_inuse=0;

HELP_DATA *new_help(void) {
    HELP_DATA *pHelp;

    if (help_free==NULL) {
	pHelp			= alloc_perm(sizeof(HELP_DATA));
	help_allocated++;
    } else {
	pHelp=help_free;
	help_free=help_free->next;
    }

    help_inuse++;
    ZEROVAR(pHelp,HELP_DATA);
    VALIDATE(pHelp);

    pHelp->level	= -1;
    pHelp->keyword	= str_dup("");
    pHelp->text		= str_dup("");

    return pHelp;
}

void free_help(HELP_DATA *pHelp) {
    if (!IS_VALID(pHelp))
	return;

    free_string(pHelp->keyword	);
    free_string(pHelp->text	);

    help_inuse--;
    INVALIDATE(pHelp);

    pHelp->next=help_free;
    help_free=pHelp;
}

//
// dream recycling
//
DREAM_TYPE *	dream_free=NULL;
int		dream_allocated=0;
int		dream_inuse=0;

DREAM_TYPE *new_dream(void) {
    DREAM_TYPE *pDream;

    if (dream_free==NULL) {
	pDream			= alloc_perm(sizeof(DREAM_TYPE));
	dream_allocated++;
    } else {
	pDream=dream_free;
	dream_free=dream_free->next;
    }

    dream_inuse++;
    ZEROVAR(pDream,DREAM_TYPE);
    VALIDATE(pDream);

    pDream->string	= str_dup("");

    return pDream;
}

void free_dream(DREAM_TYPE *pDream) {
    if (!IS_VALID(pDream))
	return;

    free_string(pDream->string	);

    dream_inuse--;
    INVALIDATE(pDream);

    pDream->next=dream_free;
    dream_free=pDream;
}

//
// dream recycling
//
BADNAME_DATA *	badname_free=NULL;
int		badname_allocated=0;
int		badname_inuse=0;

BADNAME_DATA *new_badname(void) {
    BADNAME_DATA *pBadname;

    if (badname_free==NULL) {
	pBadname			= alloc_perm(sizeof(BADNAME_DATA));
	badname_allocated++;
    } else {
	pBadname=badname_free;
	badname_free=badname_free->next;
    }

    badname_inuse++;
    ZEROVAR(pBadname,BADNAME_DATA);
    VALIDATE(pBadname);

    pBadname->name	= str_dup("");

    return pBadname;
}

void free_badname(BADNAME_DATA *pBadname) {
    if (!IS_VALID(pBadname))
	return;

    free_string(pBadname->name	);

    badname_inuse--;
    INVALIDATE(pBadname);

    pBadname->next=badname_free;
    badname_free=pBadname;
}
