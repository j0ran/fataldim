/* $Id: olc.c,v 1.77 2008/06/06 18:42:48 jodocus Exp $ */
/***************************************************************************
 *  File: olc.c	                                                    *
 *	                                                                 *
 *  Much time and thought has gone into this software and you are	  *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.	                                          *
 *	                                                                 *
 *  This code was freely distributed with the The Isles 1.1 source code,   *
 *  and has been used here for OLC - OLC would not be what it is without   *
 *  all the previous coders who released their source code.	        *
 *	                                                                 *
 ***************************************************************************/

#include "merc.h"
#include "olc.h"
#include "db.h"


/* Executed from comm.c.  Minimizes compiling when changes are made. */
// if it returns TRUE the command was processed by the OLC interpreter
bool run_olc_editor( DESCRIPTOR_DATA *d ) {
    switch ( d->editor ) {
    case ED_AREA:
	return aedit( d->character, d->incomm );
	break;
    case ED_ROOM:
	return redit( d->character, d->incomm );
	break;
    case ED_OBJECT:
	return oedit( d->character, d->incomm );
	break;
    case ED_MOBILE:
	return medit( d->character, d->incomm );
	break;
    case ED_OBJPROG:
	return mpedit( d->character, d->incomm );
	break;
    case ED_MOBPROG:
	return mpedit( d->character, d->incomm );
	break;
    case ED_ROOMPROG:
	return rpedit( d->character, d->incomm );
	break;
    case ED_HELP:
    	return hedit( d->character, d->incomm );
    	break;
    case ED_SOCIAL:
    	return socialedit( d->character, d->incomm );
    	break;
    case ED_RESET:
    	return resetedit( d->character, d->incomm );
    	break;
    case ED_AREAPROG:
    	return apedit( d->character, d->incomm );
    	break;
    default:
	return FALSE;
    }
}



char *olc_ed_name( CHAR_DATA *ch )
{
    static char buf[16];

    buf[0] = '\0';
    switch (ch->desc->editor)
    {
    case ED_AREA:
	strcpy( buf, "AEdit" );
	break;
    case ED_ROOM:
	strcpy( buf, "REdit" );
	break;
    case ED_OBJECT:
	strcpy( buf, "OEdit" );
	break;
    case ED_MOBILE:
	strcpy( buf, "MEdit" );
	break;
    case ED_OBJPROG:
	strcpy( buf, "OPEdit" );
	break;
    case ED_ROOMPROG:
	strcpy( buf, "RPEdit" );
	break;
    case ED_MOBPROG:
	strcpy( buf, "MPEdit" );
	break;
    case ED_HELP:
    	strcpy( buf, "HEdit" );
    	break;
    case ED_SOCIAL:
    	strcpy( buf, "SocialEdit" );
    	break;
    case ED_RESET:
    	strcpy( buf, "ResetEdit" );
    	break;
    case ED_AREAPROG:
    	strcpy( buf, "APEdit" );
    	break;
    default:
	strcpy( buf, "" );
	break;
    }
    return buf;
}



char *olc_ed_vnum( CHAR_DATA *ch )
{
    AREA_DATA *pArea;
    ROOM_INDEX_DATA *pRoom;
    OBJ_INDEX_DATA *pObj;
    MOB_INDEX_DATA *pMob;
    TCLPROG_CODE *pProg;
    HELP_DATA *pHelp;
    static char buf[INT_STRING_BUF_SIZE];

    buf[0] = '\0';
    switch ( ch->desc->editor )
    {
    case ED_AREA:
	EDIT_AREA(ch,pArea);
	itoa_r(pArea ? pArea->vnum : 0,buf,sizeof(buf));
	break;
    case ED_ROOM:
	EDIT_ROOM(ch,pRoom);
	itoa_r(pRoom ? pRoom->vnum : 0,buf,sizeof(buf));
	break;
    case ED_OBJECT:
	EDIT_OBJ(ch,pObj);
	itoa_r(pObj ? pObj->vnum : 0,buf,sizeof(buf));
	break;
    case ED_MOBILE:
	EDIT_MOB(ch,pMob);
	itoa_r(pMob ? pMob->vnum : 0,buf,sizeof(buf));
	break;
    case ED_OBJPROG:
    case ED_ROOMPROG:
    case ED_MOBPROG:
    case ED_AREAPROG:
	EDIT_TCLPROG(ch,pProg);
	itoa_r(pProg ? pProg->vnum : 0,buf,sizeof(buf));
	break;
    case ED_RESET:
	sprintf( buf, " " );
	break;
    case ED_SOCIAL:
	sprintf( buf, " " );
	break;
    case ED_HELP:
	EDIT_HELP(ch,pHelp);
    	strncpy(buf,pHelp->keyword,9);
    	buf[9]='\0';
    	break;
    default:
	sprintf( buf, " " );
	break;
    }

    return buf;
}



/*****************************************************************************
 Name:		show_olc_cmds
 Purpose:	Format up the commands from given table.
 Called by:	show_commands(olc_act.c).
 ****************************************************************************/
void show_olc_cmds( CHAR_DATA *ch, const struct olc_cmd_type *olc_table )
{
    char buf  [ MAX_STRING_LENGTH ];
    char buf1 [ MAX_STRING_LENGTH ];
    int  cmd;
    int  col;

    buf1[0] = '\0';
    col = 0;
    for (cmd = 0; olc_table[cmd].name[0] != '\0'; cmd++)
    {
	sprintf( buf, "%-15.15s", olc_table[cmd].name );
	strcat( buf1, buf );
	if ( ++col % 5 == 0 )
	    strcat( buf1, "\n\r" );
    }

    if ( col % 5 != 0 )
	strcat( buf1, "\n\r" );

    send_to_char( buf1, ch );
    return;
}



/*****************************************************************************
 Name:		show_commands
 Purpose:	Display all olc commands.
 Called by:	olc interpreters.
 ****************************************************************************/
bool show_commands( CHAR_DATA *ch, char *argument )
{
    switch (ch->desc->editor)
    {
	case ED_AREA:
	    show_olc_cmds( ch, aedit_table );
	    break;
	case ED_ROOM:
	    show_olc_cmds( ch, redit_table );
	    break;
	case ED_OBJECT:
	    show_olc_cmds( ch, oedit_table );
	    break;
	case ED_MOBILE:
	    show_olc_cmds( ch, medit_table );
	    break;
	case ED_MOBPROG:
	    show_olc_cmds( ch, mpedit_table );
	    break;
	case ED_ROOMPROG:
	    show_olc_cmds( ch, rpedit_table );
	    break;
	case ED_OBJPROG:
	    show_olc_cmds( ch, opedit_table );
	    break;
	case ED_AREAPROG:
	    show_olc_cmds( ch, apedit_table );
	    break;
	case ED_HELP:
	    show_olc_cmds( ch, hedit_table );
	    break;
	case ED_SOCIAL:
	    show_olc_cmds( ch, socialedit_table );
	    break;
	case ED_RESET:
	    show_olc_cmds( ch, resetedit_table );
	    break;
	default:
	    send_to_char("In which editor are you anyway?\n\r",ch);
	    break;
    }

    return FALSE;
}

bool show_freevnum( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea=NULL;
    int vroom,vmob,vobj,vmobprog,vobjprog,vroomprog,vareaprog;

    switch ( ch->desc->editor )
    {
    case ED_AREA:
	pArea = (AREA_DATA *)ch->desc->pEdit;
	break;
    case ED_ROOM:
	pArea = ch->in_room->area;
	break;
    case ED_OBJECT:
	pArea = ((OBJ_INDEX_DATA *)ch->desc->pEdit)->area;
	break;
    case ED_MOBILE:
	pArea = ((MOB_INDEX_DATA *)ch->desc->pEdit)->area;
	break;
    case ED_OBJPROG:
    case ED_MOBPROG:
    case ED_ROOMPROG:
    case ED_AREAPROG:
	pArea = ((TCLPROG_CODE *)ch->desc->pEdit)->area;
	break;
    case ED_HELP:
    	pArea = ((HELP_DATA *)ch->desc->pEdit)->area;
    	break;
    case ED_SOCIAL:
    case ED_RESET:
    default:
	send_to_char("OLC:  You are not editing in an area.\n\r", ch);
	return FALSE;
    }

    for(vroom=pArea->lvnum;vroom<=pArea->uvnum; vroom++)
	if(!get_room_index(vroom)) break;
    for(vmob=pArea->lvnum;vmob<=pArea->uvnum; vmob++)
	if(!get_mob_index(vmob)) break;
    for(vobj=pArea->lvnum;vobj<=pArea->uvnum; vobj++)
	if(!get_obj_index(vobj)) break;
    for(vmobprog=pArea->lvnum;vmobprog<=pArea->uvnum; vmobprog++)
	if(!get_mprog_index(vmobprog)) break;
    for(vobjprog=pArea->lvnum;vobjprog<=pArea->uvnum; vobjprog++)
	if(!get_oprog_index(vobjprog)) break;
    for(vroomprog=pArea->lvnum;vroomprog<=pArea->uvnum; vroomprog++)
	if(!get_rprog_index(vroomprog)) break;
    for(vareaprog=pArea->lvnum;vareaprog<=pArea->uvnum; vareaprog++)
	if(!get_aprog_index(vareaprog)) break;

    sprintf_to_char(ch,
	"First free room:    [%d]\n\r"
	"First free mob:     [%d]\n\r"
	"First free obj:     [%d]\n\r"
	"First free mobprog: [%d]\n\r"
	"First free objprog: [%d]\n\r"
	"First free roomprog: [%d]\n\r"
	"First free areaprog: [%d]\n\r",
	vroom,vmob,vobj,vmobprog,vobjprog,vroomprog,vareaprog );

    return FALSE;
}


/*****************************************************************************
 *	                   Interpreter Tables.                             *
 *****************************************************************************/
const struct olc_cmd_type aedit_table[] =
{
/*  {   command		function		}, */

    {   "age",		aedit_age		},
    {   "builders",	aedit_builder		},
    {   "commands",	show_commands		},
    {   "create",	aedit_create		},
    {   "builders",	aedit_builder		},
    {   "creator",	aedit_creator		},
    {   "filename",	aedit_file		},
    {   "name",		aedit_name		},
    {   "recall",	aedit_recall		},
    {   "login-room",	aedit_login_room	},
    {	"reset",	aedit_reset		},
    {	"urlprefix",	aedit_urlprefix		},
    {   "security",	aedit_security		},
    {	"show",		aedit_show		},
    {   "level",	aedit_level		},
    {   "vnum",		aedit_vnum		},
    {   "lvnum",	aedit_lvnum		},
    {   "uvnum",	aedit_uvnum		},
    {	"objlevel",	aedit_objlevel		},
    {	"delete",	aedit_remove		},
    {   "unfinished",	aedit_unfinished	},
    {   "purge",	aedit_purge		},
    {   "comment",	aedit_comment		},
    {   "description",	aedit_description	},
    {	"aprog",	aedit_aprog		},
    {	"proginstance",	aedit_proginstance	},
    {	"program",	aedit_program		},
    {	"property",	aedit_property		},

    {	"mlist",	redit_mlist		},
    {   "rlist",	redit_rlist		},
    {	"olist",	redit_olist		},
    {	"hlist",	hedit_hlist		},

    {   "?",		show_olc_help		},
    {   "version",	show_version		},
    {	"free",		show_freevnum,		},

    {	"",		0,			}
};



const struct olc_cmd_type redit_table[] =
{
/*  {   command		function		}, */

    {   "north",	redit_north		},
    {   "south",	redit_south		},
    {   "east",		redit_east		},
    {   "west",		redit_west		},
    {   "up",		redit_up		},
    {   "down",		redit_down		},
    {   "walk",		redit_move		},

    {   "commands",	show_commands		},
    {   "create",	redit_create		},
    {   "clone",	redit_clone		},
    {   "desc",		redit_desc		},
    {   "ed",		redit_ed		},
    {   "format",	redit_format		},
    {   "name",		redit_name		},
    {	"show",		redit_show		},
    {	"level",	redit_level		},
    {	"picture",	redit_picture		},

    {	"mlist",	redit_mlist		},
    {   "rlist",	redit_rlist		},
    {	"olist",	redit_olist		},
    {	"hlist",	hedit_hlist		},

    {   "?",		show_olc_help		},
    {   "version",	show_version		},
    {	"free",		show_freevnum,		},
    {	"delete",	redit_delete		},
    {	"property",	redit_property		},
    {	"proginstance",	redit_proginstance	},

    {	"",		0,			}
};



const struct olc_cmd_type oedit_table[] =
{
/*  {   command		function		}, */

    {   "addeffect",	oedit_addeffect		},
    {   "commands",	show_commands		},
    {   "cost",		oedit_cost		},
    {   "create",	oedit_create		},
    {   "clone",	oedit_clone		},
    {   "deleffect",	oedit_deleffect		},
    {   "ed",		oedit_ed		},
    {   "long",		oedit_long		},
    {   "name",		oedit_name		},
    {   "short",	oedit_short		},
    {	"show",		oedit_show		},
    {   "v0",		oedit_value0		},
    {   "v1",		oedit_value1		},
    {   "v2",		oedit_value2		},
    {   "v3",		oedit_value3		},
    {	"v4",		oedit_value4		},
    {   "weight",	oedit_weight		},
    {	"material",	oedit_material		},
    {	"level",	oedit_level		},
    {	"condition",	oedit_condition		},
    {	"anticlass",	oedit_anticlass		},
    {	"antirace",	oedit_antirace		},
    {	"classonly",	oedit_classonly		},
    {	"raceonly",	oedit_raceonly		},
    {	"racepoison",	oedit_racepoison	},
    {	"picture",	oedit_picture		},
    {   "proginstance",	oedit_proginstance	},

    {	"mlist",	redit_mlist		},
    {   "rlist",	redit_rlist		},
    {	"olist",	redit_olist		},
    {	"hlist",	hedit_hlist		},

    {   "?",		show_olc_help		},
    {   "version",	show_version		},
    {	"free",		show_freevnum,		},
    {	"delete",	oedit_delete		},
    {	"property",	oedit_property		},

    {	"",		0,			}
};



const struct olc_cmd_type medit_table[] =
{
/*  {   command		function		}, */

    {   "alignment",	medit_align		},
    {   "commands",	show_commands		},
    {   "create",	medit_create		},
    {   "desc",		medit_desc		},
    {   "ed",		medit_ed		},
    {   "level",	medit_level		},
    {   "long",		medit_long		},
    {   "name",		medit_name		},
    {   "shop",		medit_shop		},
    {   "short",	medit_short		},
    {	"show",		medit_show		},
    {   "spec",		medit_spec		},
    {   "group",	medit_group		},
    {	"race",		medit_race		},
    {	"size",		medit_size		},
    {	"position",	medit_position		},
    {   "proginstance",	medit_proginstance	},
    {	"wealth",	medit_wealth		},
    {	"material",	medit_material		},
    {	"hitroll",	medit_hitroll		},
    {	"damagetype",	medit_damagetype	},
    {	"ac",		medit_ac		},
    {	"dice",		medit_dice		},
    {	"imm",		medit_imm		},
    {	"res",		medit_res		},
    {	"vuln",		medit_vuln		},
    {	"offensive",	medit_offensive		},
    {	"form",		medit_form		},
    {	"picture",	medit_picture		},
    {   "clone",	medit_clone		},

    {	"mlist",	redit_mlist		},
    {   "rlist",	redit_rlist		},
    {	"olist",	redit_olist		},
    {	"hlist",	hedit_hlist		},

    {   "?",		show_olc_help		},
    {   "version",	show_version		},
    {	"free",		show_freevnum,		},
    {	"delete",	medit_delete		},
    {	"property",	medit_property		},

    {	"",		0,			}
};

const struct olc_cmd_type opedit_table[] =
{
/*  {   command		function		}, */

    {   "clear",	opedit_clear		},
    {   "commands",	show_commands		},
    {   "append",	opedit_append		},
    {	"delete",	opedit_delete		},
    {	"title",	opedit_title		},

    {	"mlist",	redit_mlist		},
    {   "rlist",	redit_rlist		},
    {	"olist",	redit_olist		},
    {	"hlist",	hedit_hlist		},


    {   "?",		show_olc_help		},
    {   "version",	show_version		},
    {	"free",		show_freevnum,		},

    {	"",		0,			}
};

const struct olc_cmd_type rpedit_table[] =
{
/*  {   command		function		}, */

    {   "clear",	rpedit_clear		},
    {   "commands",	show_commands		},
    {   "append",	rpedit_append		},
    {	"delete",	rpedit_delete		},
    {	"title",	rpedit_title		},

    {	"mlist",	redit_mlist		},
    {   "rlist",	redit_rlist		},
    {	"olist",	redit_olist		},
    {	"hlist",	hedit_hlist		},


    {   "?",		show_olc_help		},
    {   "version",	show_version		},
    {	"free",		show_freevnum,		},

    {	"",		0,			}
};

const struct olc_cmd_type mpedit_table[] =
{
/*  {   command		function		}, */

    {   "clear",	mpedit_clear		},
    {   "commands",	show_commands		},
    {   "append",	mpedit_append		},
    {	"delete",	mpedit_delete		},
    {	"title",	mpedit_title		},

    {	"mlist",	redit_mlist		},
    {   "rlist",	redit_rlist		},
    {	"olist",	redit_olist		},
    {	"hlist",	hedit_hlist		},


    {   "?",		show_olc_help		},
    {   "version",	show_version		},
    {	"free",		show_freevnum,		},

    {	"",		0,			}
};

const struct olc_cmd_type apedit_table[] =
{
/*  {   command		function		}, */

    {   "clear",	apedit_clear		},
    {   "commands",	show_commands		},
    {   "append",	apedit_append		},
    {	"delete",	apedit_delete		},
    {	"title",	apedit_title		},

    {	"mlist",	redit_mlist		},
    {   "rlist",	redit_rlist		},
    {	"olist",	redit_olist		},
    {	"hlist",	hedit_hlist		},


    {   "?",		show_olc_help		},
    {   "version",	show_version		},
    {	"free",		show_freevnum,		},

    {	"",		0,			}
};


const struct olc_cmd_type hedit_table[] =
{
/*  {   command		function		}, */

    {   "clear",	hedit_clear		},
    {   "commands",	show_commands		},
    {   "append",	hedit_append		},
    {	"keyword",	hedit_keyword		},
    {	"level",	hedit_level		},
    {	"delete",	hedit_delete		},

    {	"mlist",	redit_mlist		},
    {   "rlist",	redit_rlist		},
    {	"olist",	redit_olist		},
    {	"hlist",	hedit_hlist		},

    {   "?",		show_olc_help		},
    {   "version",	show_version		},
    {	"free",		show_freevnum,		},

    {	"",		0,			}
};

const struct olc_cmd_type socialedit_table[] =
{
/*  {	command		function		}, */
    {   "commands",	show_commands		},
    {	"name",		socialedit_name		},
    {	"delete",	socialedit_delete	},
    {   "cna",		socialedit_cna		},
    {   "ona",		socialedit_ona		},
    {   "cf",		socialedit_cf		},
    {   "of",		socialedit_of		},
    {   "vf",		socialedit_vf		},
    {   "cnf",		socialedit_cnf		},
    {   "ca",		socialedit_ca		},
    {   "oa",		socialedit_oa		},

    {	"",		0			}
};


const struct olc_cmd_type resetedit_table[] =
{
/*  {	command		function		}, */
    {   "commands",	show_commands		},
    {	"create",	resetedit_create	},
    {	"delete",	resetedit_delete	},
    {	"insert",	resetedit_insert	},
    {	"swap",		resetedit_swap		},
    {	"change",	resetedit_change	},
    {	"move",		resetedit_move		},

    {	"",		0			}
};


/*****************************************************************************
 *	                  End Interpreter Tables.                          *
 *****************************************************************************/



/*****************************************************************************
 Name:		get_area_data
 Purpose:	Returns pointer to area with given vnum.
 Called by:	do_aedit(olc.c).
 ****************************************************************************/
AREA_DATA *get_area_data( int vnum )
{
    AREA_DATA *pArea;

    for (pArea = area_first; pArea; pArea = pArea->next )
    {
	if (pArea->vnum == vnum)
	    return pArea;
    }

    return 0;
}

HELP_DATA *get_help_data( AREA_DATA *pArea,char *keyword)
{
    HELP_DATA *pHelp;
    HELP_DATA *pointer=(HELP_DATA *)strtol(keyword,NULL,16);

    for(pHelp=help_first;pHelp;pHelp=pHelp->next )
    {
    	if(pHelp->area==pArea && pHelp->deleted==FALSE
    	&& (pointer==pHelp || is_name(keyword,pHelp->keyword)))
    	    return pHelp;
    }

    return NULL;
}

AREA_DATA *get_help_area(CHAR_DATA *ch)
{
    switch(ch->desc->editor) {
      case ED_HELP:
	return ((HELP_DATA *)ch->desc->pEdit)->area;
      case ED_AREA:
	return (AREA_DATA *)ch->desc->pEdit;
      default:
	return ch->in_room->area;
    }
}


/*****************************************************************************
 Name:		edit_done
 Purpose:	Resets builder information on completion.
 Called by:	aedit, redit, oedit, medit(olc.c) and the do_?edit
 ****************************************************************************/
bool edit_done( CHAR_DATA *ch )
{
    if(ch->desc->editor == ED_MOBPROG ||
       ch->desc->editor == ED_OBJPROG ||
       ch->desc->editor == ED_ROOMPROG ||
       ch->desc->editor == ED_AREAPROG )
    {
	TCLPROG_CODE *pCode;

	pCode=(TCLPROG_CODE *)ch->desc->pEdit;
	pCode->edit=FALSE;
	pCode->version++;
    }

    if (ch->desc->editor == ED_AREA) {
	AREA_DATA *pArea;
	pArea=(AREA_DATA *)ch->desc->pEdit;
	pArea->areaprogram_edit=FALSE;
    }

    if (ch->desc->editor==ED_SOCIAL)
	save_socials();

    ch->desc->pEdit = NULL;
    ch->desc->editor = 0;
    return FALSE;
}



/*****************************************************************************
 *	                      Interpreters.                                *
 *****************************************************************************/


/* Area Interpreter, called by do_aedit. */
bool aedit( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;
    char command[MAX_INPUT_LENGTH];
    char arg[MAX_INPUT_LENGTH];
    int  cmd;
    int  value;

    EDIT_AREA(ch, pArea);
    smash_tilde( argument );
    strcpy( arg, argument );
    argument = one_argument( argument, command );

    if ( !IS_BUILDER( ch, pArea ) )
	send_to_char( "AEdit:  Insufficient security to modify area.\n\r", ch );

    if ( command[0] == '\0' )
    {
	aedit_show( ch, argument );
	return TRUE;
    }

    if ( !str_cmp(command, "done") )
    {
	edit_done( ch );
	return TRUE;
    }

    if ( !IS_BUILDER( ch, pArea ) )
	return TRUE;

    /* Search Table and Dispatch Command. */
    for ( cmd = 0; *aedit_table[cmd].name; cmd++ )
    {
	if ( !str_prefix( command, aedit_table[cmd].name ) )
	{
	    if ( (*aedit_table[cmd].olc_fun) ( ch, argument ) )
		SET_BIT( pArea->area_flags, AREA_CHANGED );
	    return TRUE;
	}
    }

    /* Take care of flags. */
    if ((value=option_find_name(arg,area_options,TRUE))!=NO_FLAG) {
	TOGGLE_BIT(pArea->area_flags, value);

	SET_BIT( pArea->area_flags, AREA_CHANGED );
	send_to_char( "Area flag toggled.\n\r", ch );
	return TRUE;
    }

    return FALSE;
}



/* Room Interpreter, called by do_redit. */
bool redit( CHAR_DATA *ch, char *argument )
{
    ROOM_INDEX_DATA *pRoom;
    AREA_DATA *pArea;
    char arg[MAX_STRING_LENGTH];
    char command[MAX_INPUT_LENGTH];
    int  cmd;
    int  value;

    EDIT_ROOM(ch, pRoom);
    pArea = pRoom->area;

    smash_tilde( argument );
    strcpy( arg, argument );
    argument = one_argument( argument, command );

    if ( !IS_BUILDER( ch, pArea ) )
	send_to_char( "REdit:  Insufficient security to modify room.\n\r", ch );

    if ( command[0] == '\0' )
    {
	redit_show( ch, argument );
	return TRUE;
    }

    if ( !str_cmp(command, "done") )
    {
	edit_done( ch );
	return TRUE;
    }

    if ( !IS_BUILDER( ch, pArea ) )
	return TRUE;

    /* Search Table and Dispatch Command. */
    for ( cmd = 0; *redit_table[cmd].name; cmd++ )
    {
	if ( !str_prefix( command, redit_table[cmd].name ) )
	{
	    if ( (*redit_table[cmd].olc_fun) ( ch, argument ) )
		SET_BIT( pArea->area_flags, AREA_CHANGED );
	    return TRUE;
	}
    }

    /* Take care of flags. */
    if ((value=option_find_name(arg,room_options,TRUE))!=NO_FLAG) {
	STR_TOGGLE_BIT(pRoom->strbit_room_flags, value);

	SET_BIT( pArea->area_flags, AREA_CHANGED );
	send_to_char( "Room flag toggled.\n\r", ch );
	return TRUE;
    }

    if ((value=table_find_name(arg,sector_table))!=NO_FLAG) {
	pRoom->sector_type  = value;

	SET_BIT( pArea->area_flags, AREA_CHANGED );
	send_to_char( "Sector type set.\n\r", ch );
	return TRUE;
    }

    return FALSE;
}



/* Object Interpreter, called by do_oedit. */
bool oedit( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;
    OBJ_INDEX_DATA *pObj;
    char arg[MAX_STRING_LENGTH];
    char command[MAX_INPUT_LENGTH];
    int  cmd;
    int  value;

    smash_tilde( argument );
    strcpy( arg, argument );
    argument = one_argument( argument, command );

    EDIT_OBJ(ch, pObj);
    pArea = pObj->area;

    if ( !IS_BUILDER( ch, pArea ) )
	send_to_char( "OEdit: Insufficient security to modify area.\n\r", ch );

    if ( command[0] == '\0' )
    {
	oedit_show( ch, argument );
	return TRUE;
    }

    if ( !str_cmp(command, "done") )
    {
	edit_done( ch );
	return TRUE;
    }

    if ( !IS_BUILDER( ch, pArea ) )
	return FALSE;

    /* Search Table and Dispatch Command. */
    for ( cmd = 0; *oedit_table[cmd].name; cmd++ )
    {
	if ( !str_prefix( command, oedit_table[cmd].name ) )
	{
	    if ( (*oedit_table[cmd].olc_fun) ( ch, argument ) )
		SET_BIT( pArea->area_flags, AREA_CHANGED );
	    return TRUE;
	}
    }

    /* Take care of flags. */
    if ((value=table_find_name(arg,item_table))!=NO_FLAG ) {
	pObj->item_type = value;

	SET_BIT( pArea->area_flags, AREA_CHANGED );
	send_to_char( "Type set.\n\r", ch);

	/*
	 * Clear the values.
	 */
	pObj->value[0] = 0;
	pObj->value[1] = 0;
	pObj->value[2] = 0;
	pObj->value[3] = 0;
	pObj->value[4] = 0;

	switch(value) {
	case ITEM_WAND:
	case ITEM_STAFF:
	    pObj->value[3]=-1;
	    break;
	case ITEM_SCROLL:
	case ITEM_POTION:
	case ITEM_PILL:
	    pObj->value[1]=-1;
	    pObj->value[2]=-1;
	    pObj->value[3]=-1;
	    pObj->value[4]=-1;
	    break;
	case ITEM_CONTAINER:
	    pObj->value[4]=100;
	    break;
	case ITEM_FURNITURE:
	    pObj->value[3]=100;
	    pObj->value[4]=100;
	    break;
	case ITEM_CLAN:
	    send_to_char("Use the clan-property to assign it to a clan!\n\r",ch);
	    break;
	}

	return TRUE;
    }

    if ( ( value = option_find_name(arg,extra_options,TRUE) ) != NO_FLAG )
    {
	STR_TOGGLE_BIT(pObj->strbit_extra_flags, value);

	SET_BIT( pArea->area_flags, AREA_CHANGED );
	send_to_char( "Extra flag toggled.\n\r", ch);
	return TRUE;
    }

    if ( ( value = option_find_name( arg,wear_options,TRUE) ) != NO_FLAG )
    {
	TOGGLE_BIT(pObj->wear_flags, value);

	SET_BIT( pArea->area_flags, AREA_CHANGED );
	send_to_char( "Wear flag toggled.\n\r", ch);
	return TRUE;
    }

    return FALSE;
}



/* Mobile Interpreter, called by do_medit. */
bool medit( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;
    MOB_INDEX_DATA *pMob;
    char command[MAX_INPUT_LENGTH];
    char arg[MAX_STRING_LENGTH];
    int  cmd;
    int  value;

    smash_tilde( argument );
    strcpy( arg, argument );
    argument = one_argument( argument, command );

    EDIT_MOB(ch, pMob);
    pArea = pMob->area;

    if ( !IS_BUILDER( ch, pArea ) )
	send_to_char( "MEdit: Insufficient security to modify area.\n\r", ch );

    if ( command[0] == '\0' )
    {
	medit_show( ch, argument );
	return TRUE;
    }

    if ( !str_cmp(command, "done") )
    {
	edit_done( ch );
	return TRUE;
    }

    if ( !IS_BUILDER( ch, pArea ) )
	return FALSE;

    /* Search Table and Dispatch Command. */
    for ( cmd = 0; *medit_table[cmd].name; cmd++ )
    {
	if ( !str_prefix( command, medit_table[cmd].name ) )
	{
	    if ( (*medit_table[cmd].olc_fun) ( ch, argument ) )
		SET_BIT( pArea->area_flags, AREA_CHANGED );
	    return TRUE;
	}
    }

    /* Take care of flags. */
    if ( ( value = table_find_name( arg, sex_table ) ) != NO_FLAG )
    {
	pMob->sex = value;

	SET_BIT( pArea->area_flags, AREA_CHANGED );
	send_to_char( "Sex set.\n\r", ch);
	return TRUE;
    }


    if ( option_find_name( arg, act_options,TRUE ) != NO_FLAG ) {
	mob_flag_toggle_string(
	    arg,
	    act_options,
	    pMob->strbit_act_read,
	    race_table[pMob->race].strbit_act,
	    pMob->strbit_act_saved,
	    pMob->strbit_act);

	SET_BIT( pArea->area_flags, AREA_CHANGED );
	send_to_char( "Act flag toggled.\n\r", ch);
	return TRUE;
    }

    if ( option_find_name( arg, effect_options,TRUE ) != NO_FLAG ) {
	mob_flag_toggle_string(
	    arg,
	    effect_options,
	    pMob->strbit_affected_read,
	    race_table[pMob->race].strbit_eff,
	    pMob->strbit_affected_saved,
	    pMob->strbit_affected_by);

	SET_BIT( pArea->area_flags, AREA_CHANGED );
	send_to_char( "Effect flag toggled.\n\r", ch);
	return TRUE;
    }

    if ( option_find_name( arg, effect2_options,TRUE ) != NO_FLAG )
    {
	mob_flag_toggle_string(
	    arg,
	    effect2_options,
	    pMob->strbit_affected_read2,
	    race_table[pMob->race].strbit_eff2,
	    pMob->strbit_affected_saved2,
	    pMob->strbit_affected_by2);

	SET_BIT( pArea->area_flags, AREA_CHANGED );
	send_to_char( "Effect2 flag toggled.\n\r", ch);
	return TRUE;
    }

    if ( option_find_name( arg, off_options,TRUE ) != NO_FLAG )
    {
	mob_flag_toggle_string(
	    arg,
	    off_options,
	    pMob->strbit_off_read,
	    race_table[pMob->race].strbit_off,
	    pMob->strbit_off_saved,
	    pMob->strbit_off_flags);

	SET_BIT( pArea->area_flags, AREA_CHANGED );
	send_to_char( "Offensive flag toggled.\n\r", ch);
	return TRUE;
    }

    if ( option_find_name( arg, form_options,TRUE ) != NO_FLAG )
    {
	pMob->form=mob_flag_toggle(arg,
	    form_options,
	    &pMob->form_mem[0],
	    race_table[pMob->race].form,
	    &pMob->form_mem[1] );

	SET_BIT( pArea->area_flags, AREA_CHANGED );
	send_to_char( "Form flag toggled.\n\r", ch);
	return TRUE;
    }

    if ( option_find_name( arg, part_options,TRUE ) != NO_FLAG )
    {
	pMob->parts=mob_flag_toggle(arg,
	    part_options,
	    &pMob->parts_mem[0],
	    race_table[pMob->race].parts,
	    &pMob->parts_mem[1] );

	SET_BIT( pArea->area_flags, AREA_CHANGED );
	send_to_char( "Parts flag toggled.\n\r", ch);
	return TRUE;
    }

    return FALSE;
}

/* Mobile Interpreter, called by do_mpedit. */
bool mpedit( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;
    TCLPROG_CODE *pCode;
    char command[MAX_INPUT_LENGTH];
    char arg[MAX_STRING_LENGTH];
    int  cmd;

    smash_tilde( argument );
    strcpy( arg, argument );
    argument = one_argument( argument, command );

    EDIT_TCLPROG(ch, pCode);
    pArea = pCode->area;

    if ( !IS_BUILDER( ch, pArea ) )
	send_to_char( "MPEdit: Insufficient security to modify area.\n\r", ch );

    if ( command[0] == '\0' )
    {
	mpedit_show( ch, argument );
	return TRUE;
    }

    if ( !str_cmp(command, "done") )
    {
	edit_done( ch );
	return TRUE;
    }

    if ( !IS_BUILDER( ch, pArea ) )
	return FALSE;

    /* Search Table and Dispatch Command. */
    for ( cmd = 0; *mpedit_table[cmd].name; cmd++ )
    {
	if ( !str_prefix( command, mpedit_table[cmd].name ) )
	{
	    if ( (*mpedit_table[cmd].olc_fun) ( ch, argument ) )
		SET_BIT( pArea->area_flags, AREA_CHANGED );
	    return TRUE;
	}
    }

    return FALSE;
}

/* Object Interpreter, called by do_opedit. */
bool opedit( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;
    TCLPROG_CODE *pCode;
    char command[MAX_INPUT_LENGTH];
    char arg[MAX_STRING_LENGTH];
    int  cmd;

    smash_tilde( argument );
    strcpy( arg, argument );
    argument = one_argument( argument, command );

    EDIT_TCLPROG(ch, pCode);
    pArea = pCode->area;

    if ( !IS_BUILDER( ch, pArea ) )
	send_to_char( "OPEdit: Insufficient security to modify area.\n\r", ch );

    if ( command[0] == '\0' )
    {
	opedit_show( ch, argument );
	return TRUE;
    }

    if ( !str_cmp(command, "done") )
    {
	edit_done( ch );
	return TRUE;
    }

    if ( !IS_BUILDER( ch, pArea ) )
	return FALSE;

    /* Search Table and Dispatch Command. */
    for ( cmd = 0; *opedit_table[cmd].name; cmd++ )
    {
	if ( !str_prefix( command, opedit_table[cmd].name ) )
	{
	    if ( (*opedit_table[cmd].olc_fun) ( ch, argument ) )
		SET_BIT( pArea->area_flags, AREA_CHANGED );
	    return TRUE;
	}
    }

    return FALSE;
}

/* Room Interpreter, called by do_rpedit. */
bool rpedit( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;
    TCLPROG_CODE *pCode;
    char command[MAX_INPUT_LENGTH];
    char arg[MAX_STRING_LENGTH];
    int  cmd;

    smash_tilde( argument );
    strcpy( arg, argument );
    argument = one_argument( argument, command );

    EDIT_TCLPROG(ch, pCode);
    pArea = pCode->area;

    if ( !IS_BUILDER( ch, pArea ) )
	send_to_char( "RPEdit: Insufficient security to modify area.\n\r", ch );

    if ( command[0] == '\0' )
    {
	rpedit_show( ch, argument );
	return TRUE;
    }

    if ( !str_cmp(command, "done") )
    {
	edit_done( ch );
	return TRUE;
    }

    if ( !IS_BUILDER( ch, pArea ) )
	return FALSE;

    /* Search Table and Dispatch Command. */
    for ( cmd = 0; *rpedit_table[cmd].name; cmd++ )
    {
	if ( !str_prefix( command, rpedit_table[cmd].name ) )
	{
	    if ( (*rpedit_table[cmd].olc_fun) ( ch, argument ) )
		SET_BIT( pArea->area_flags, AREA_CHANGED );
	    return TRUE;
	}
    }

    return FALSE;
}


/* Area Interpreter, called by do_apedit. */
bool apedit( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;
    TCLPROG_CODE *pCode;
    char command[MAX_INPUT_LENGTH];
    char arg[MAX_STRING_LENGTH];
    int  cmd;

    smash_tilde( argument );
    strcpy( arg, argument );
    argument = one_argument( argument, command );

    EDIT_TCLPROG(ch, pCode);
    pArea = pCode->area;

    if ( !IS_BUILDER( ch, pArea ) )
	send_to_char( "APEdit: Insufficient security to modify area.\n\r", ch );

    if ( command[0] == '\0' )
    {
	apedit_show( ch, argument );
	return TRUE;
    }

    if ( !str_cmp(command, "done") )
    {
	edit_done( ch );
	return TRUE;
    }

    if ( !IS_BUILDER( ch, pArea ) )
	return FALSE;

    /* Search Table and Dispatch Command. */
    for ( cmd = 0; *apedit_table[cmd].name; cmd++ )
    {
	if ( !str_prefix( command, apedit_table[cmd].name ) )
	{
	    if ( (*apedit_table[cmd].olc_fun) ( ch, argument ) )
		SET_BIT( pArea->area_flags, AREA_CHANGED );
	    return TRUE;
	}
    }

    return FALSE;
}



/* Help editor Interpreter, called by do_hedit. */
bool hedit( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;
    char command[MAX_INPUT_LENGTH];
    char arg[MAX_STRING_LENGTH];
    int  cmd;

    smash_tilde( argument );
    strcpy( arg, argument );
    argument = one_argument( argument, command );

    pArea = get_help_area(ch);

    if ( !IS_BUILDER( ch, pArea ) )
	send_to_char( "HEdit: Insufficient security to modify help in this area.\n\r", ch );

    if ( command[0] == '\0' ) {
	hedit_show( ch, argument );
	return TRUE;
    }

    if ( !str_cmp(command, "done") ) {
	edit_done( ch );
	return TRUE;
    }

    if ( !IS_BUILDER( ch, pArea ) )
	return FALSE;

    /* Search Table and Dispatch Command. */
    for ( cmd = 0; *hedit_table[cmd].name; cmd++ )
    {
	if ( !str_prefix( command, hedit_table[cmd].name ) )
	{
	    if ( (*hedit_table[cmd].olc_fun) ( ch, argument ) )
		SET_BIT( pArea->area_flags, AREA_CHANGED );
	    return TRUE;
	}
    }

    return FALSE;
}


/* Social editor Interpreter, called by run_olc_cmd. */
bool socialedit( CHAR_DATA *ch, char *argument )
{
    char command[MAX_INPUT_LENGTH];
    char arg[MAX_STRING_LENGTH];
    int  cmd;

    smash_tilde( argument );
    strcpy( arg, argument );
    argument = one_argument( argument, command );

    if ( command[0] == '\0' )
    {
	socialedit_show( ch, argument );
	return TRUE;
    }

    if ( !str_cmp(command, "done") )
    {
	edit_done( ch );
	return TRUE;
    }

    /* Search Table and Dispatch Command. */
    for ( cmd = 0; *socialedit_table[cmd].name; cmd++ )
    {
	if ( !str_prefix( command, socialedit_table[cmd].name ) )
	{
	    if ( (*socialedit_table[cmd].olc_fun) ( ch, argument ) )
		mud_data.socials_changed=TRUE;
	    return TRUE;
	}
    }

    return FALSE;
}


/* Reset Interpreter, called by do_resetedit. */
bool resetedit( CHAR_DATA *ch, char *argument ) {
    char command[MAX_INPUT_LENGTH];
    char arg[MAX_STRING_LENGTH];
    int  cmd;
    ROOM_INDEX_DATA *pRoom;

    EDIT_ROOM(ch, pRoom);

    smash_tilde( argument );
    strcpy( arg, argument );
    argument = one_argument( argument, command );

    if ( command[0] == '\0' )
    {
	resetedit_show( ch, argument );
	return TRUE;
    }

    if ( !str_cmp(command, "done") )
    {
	edit_done( ch );
	return TRUE;
    }

    if ( !IS_BUILDER( ch, ch->in_room->area ) )
    {
	send_to_char( "Resets: Invalid security for editing this room.\n\r",ch);
	return TRUE;
    }

    /* Search Table and Dispatch Command. */
    for ( cmd = 0; *resetedit_table[cmd].name; cmd++ )
    {
	if ( !str_prefix( command, resetedit_table[cmd].name ) )
	{
	    if ( (*resetedit_table[cmd].olc_fun) ( ch, argument ) )
		SET_BIT( pRoom->area->area_flags, AREA_CHANGED );
	    return TRUE;
	}
    }

    return FALSE;
}



void do_aedit( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;
    char command[MAX_INPUT_LENGTH];

    argument = one_argument( argument, command );
    pArea = ch->in_room->area;

    if ( command[0] == 'r' && !str_prefix( command, "reset" ) )
    {
	if ( ch->desc->editor == ED_AREA )
	    reset_area( (AREA_DATA *)ch->desc->pEdit );
	else
	    reset_area( pArea );
	send_to_char( "Area reset.\n\r", ch );
	return;
    }

    if ( command[0] == 'c' && !str_prefix( command, "create" ) )
    {
	if ( aedit_create( ch, argument ) )
	{
	    ch->desc->editor = ED_AREA;
	    pArea = (AREA_DATA *)ch->desc->pEdit;
	    SET_BIT( pArea->area_flags, AREA_CHANGED );
	    aedit_show( ch, "" );
	}
	return;
    }

    if ( is_number( command ) )
    {
	if ( !( pArea = get_area_data( atoi(command) ) ) )
	{
	    send_to_char( "No such area vnum exists.\n\r", ch );
	    return;
	}
    }

    if (IS_SET(pArea->area_flags,AREA_PROTECTED))
	send_to_char("{RYou are changing a protected area.{x\r\n"
		     "{RChanges will be undone if you don't contact the head builder.{x\r\n",ch);

    /*
     * Builder defaults to editing current area.
     */
    edit_done( ch );
    ch->desc->pEdit = (void *)pArea;
    ch->desc->editor = ED_AREA;
    aedit_show( ch, "" );
    return;
}



/* Entry point for editing room_index_data. */
void do_redit( CHAR_DATA *ch, char *argument )
{
    ROOM_INDEX_DATA *pRoom;
    char command[MAX_INPUT_LENGTH];

    argument = one_argument( argument, command );
    pRoom = ch->in_room;

    if ( command[0] == 'r' && !str_prefix( command, "reset" ) )
    {
	reset_room( pRoom );
	send_to_char( "Room reset.\n\r", ch );
	return;
    }

    if ( command[0] == 'c' && !str_prefix( command, "create" ) )
    {
	if ( redit_create( ch, argument ) )
	{
	    char_from_room( ch );
	    char_to_room( ch, ch->desc->pEdit );
	    SET_BIT( pRoom->area->area_flags, AREA_CHANGED );
    	}
    }

    if (IS_SET(pRoom->area->area_flags,AREA_PROTECTED))
	send_to_char("{RYou are changing a protected area.{x\r\n"
		     "{RChanges will be undone if you don't contact the head builder.{x\r\n",ch);

    /*
     * Builder defaults to editing current room.
     */
    edit_done( ch );
    ch->desc->editor = ED_ROOM;
    redit_show( ch, "" );
    return;
}



/* Entry point for editing resets in roomsj. */
void do_resetedit( CHAR_DATA *ch, char *argument )
{
    ROOM_INDEX_DATA *pRoom;
    char command[MAX_INPUT_LENGTH];

    argument = one_argument( argument, command );
    pRoom = ch->in_room;

    if (IS_SET(pRoom->area->area_flags,AREA_PROTECTED))
	send_to_char("{RYou are changing a protected area.{x\r\n"
		     "{RChanges will be undone if you don't contact the head builder.{x\r\n",ch);

    /*
     * Builder defaults to editing current room.
     */
    edit_done( ch );
    ch->desc->editor = ED_RESET;
    resetedit_show( ch, "" );
    return;
}



/* Entry point for editing obj_index_data. */
void do_oedit( CHAR_DATA *ch, char *argument )
{
    OBJ_INDEX_DATA *pObj;
    AREA_DATA *pArea;
    char command[MAX_INPUT_LENGTH];

    argument = one_argument( argument, command );

    if ( is_number( command ) )
    {
	if ( !( pObj = get_obj_index( atoi( command ) ) ) )
	{
	    send_to_char( "OEdit:  That vnum does not exist.\n\r", ch );
	    return;
	}
	if (pObj->level > get_trust(ch)) {
	    send_to_char("OEdit: Access to object denied.\n\r",ch);
	    return;
	}

	if (IS_SET(pObj->area->area_flags,AREA_PROTECTED))
	    send_to_char("{RYou are changing a protected area.{x\r\n"
			 "{RChanges will be undone if you don't contact the head builder.{x\r\n",ch);

	edit_done( ch );
	ch->desc->pEdit = (void *)pObj;
	ch->desc->editor = ED_OBJECT;
	oedit_show( ch, "" );
	return;
    }

    if ( command[0] == 'c' && !str_prefix( command, "create" ) )
    {
	if ( oedit_create( ch, argument ) )
	{
	    pArea = get_vnum_area( atoi( argument ) );

	    if (IS_SET(pArea->area_flags,AREA_PROTECTED))
		send_to_char("{RYou are changing a protected area.{x\r\n"
			     "{RChanges will be undone if you don't contact the head builder.{x\r\n",ch);

	    SET_BIT( pArea->area_flags, AREA_CHANGED );
	    ch->desc->editor = ED_OBJECT;
	    oedit_show( ch, "" );
	}
	return;
    }

    send_to_char( "OEdit:  There is no default object to edit.\n\r", ch );
    return;
}



/* Entry point for editing mob_index_data. */
void do_medit( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA *pMob;
    AREA_DATA *pArea;
    char command[MAX_INPUT_LENGTH];

    argument = one_argument( argument, command );

    if ( is_number( command ) )
    {
	if ( !( pMob = get_mob_index( atoi( command ) ) ))
	{
	    send_to_char( "MEdit:  That vnum does not exist.\n\r", ch );
	    return;
	}

	edit_done( ch );

	if (IS_SET(pMob->area->area_flags,AREA_PROTECTED))
	    send_to_char("{RYou are changing a protected area.{x\r\n"
			 "{RChanges will be undone if you don't contact the head builder.{x\r\n",ch);

	ch->desc->pEdit = (void *)pMob;
	ch->desc->editor = ED_MOBILE;
	medit_show( ch, "" );
	return;
    }

    if ( command[0] == 'c' && !str_prefix( command, "create" ) )
    {
	if ( medit_create( ch, argument ) )
	{
	    pArea = get_vnum_area( atoi( argument ) );
	    SET_BIT( pArea->area_flags, AREA_CHANGED );
	    if (IS_SET(pArea->area_flags,AREA_PROTECTED))
		send_to_char("{RYou are changing a protected area.{x\r\n"
			     "{RChanges will be undone if you don't contact the head builder.{x\r\n",ch);

	    ch->desc->editor = ED_MOBILE;
	    medit_show( ch, "" );
	}
	return;
    }

    send_to_char( "MEdit:  There is no default mobile to edit.\n\r", ch );
    return;
}

/* Entry point for editing mprog_code */
void do_mpedit( CHAR_DATA *ch, char *argument )
{
    TCLPROG_CODE *pCode;
    AREA_DATA *pArea;
    char command[MAX_INPUT_LENGTH];

    argument = one_argument( argument, command );

    if ( is_number( command ) )
    {
	if ( !( pCode = get_mprog_index( atoi( command ) ) ) )
	{
	    send_to_char( "MPEdit:  That vnum does not exist.\n\r", ch );
	    return;
	}

	edit_done( ch );

	if (IS_SET(pCode->area->area_flags,AREA_PROTECTED))
	    send_to_char("{RYou are changing a protected area.{x\r\n"
			 "{RChanges will be undone if you don't contact the head builder.{x\r\n",ch);

	ch->desc->pEdit = (void *)pCode;
	pCode->edit=TRUE;
	ch->desc->editor = ED_MOBPROG;
	mpedit_show( ch, "" );
	return;
    }

    if ( command[0] == 'c' && !str_prefix( command, "create" ) )
    {
	if ( mpedit_create( ch, argument ) )
	{
	    pArea = get_vnum_area( atoi( argument ) );
	    SET_BIT( pArea->area_flags, AREA_CHANGED );

	    if (IS_SET(pArea->area_flags,AREA_PROTECTED))
		send_to_char("{RYou are changing a protected area.{x\r\n"
			     "{RChanges will be undone if you don't contact the head builder.{x\r\n",ch);

	    ch->desc->editor = ED_MOBPROG;
	    mpedit_show( ch, "" );
	}
	return;
    }

    send_to_char( "MPEdit:  There is no default object to edit.\n\r", ch );
    return;
}

/* Entry point for editing oprog_code */
void do_opedit( CHAR_DATA *ch, char *argument )
{
    TCLPROG_CODE *pCode;
    AREA_DATA *pArea;
    char command[MAX_INPUT_LENGTH];

    argument = one_argument( argument, command );

    if ( is_number( command ) )
    {
	if ( !( pCode = get_oprog_index( atoi( command ) ) ) )
	{
	    send_to_char( "OPEdit:  That vnum does not exist.\n\r", ch );
	    return;
	}

	edit_done( ch );

	if (IS_SET(pCode->area->area_flags,AREA_PROTECTED))
	    send_to_char("{RYou are changing a protected area.{x\r\n"
			 "{RChanges will be undone if you don't contact the head builder.{x\r\n",ch);

	ch->desc->pEdit = (void *)pCode;
	pCode->edit=TRUE;
	ch->desc->editor = ED_OBJPROG;
	opedit_show( ch, "" );
	return;
    }

    if ( command[0] == 'c' && !str_prefix( command, "create" ) )
    {
	if ( opedit_create( ch, argument ) )
	{
	    pArea = get_vnum_area( atoi( argument ) );
	    SET_BIT( pArea->area_flags, AREA_CHANGED );

	    if (IS_SET(pArea->area_flags,AREA_PROTECTED))
		send_to_char("{RYou are changing a protected area.{x\r\n"
			     "{RChanges will be undone if you don't contact the head builder.{x\r\n",ch);

	    ch->desc->editor = ED_OBJPROG;
	    opedit_show( ch, "" );
	}
	return;
    }

    send_to_char( "OPEdit:  There is no default object to edit.\n\r", ch );
    return;
}

/* Entry point for editing rprog_code */
void do_rpedit( CHAR_DATA *ch, char *argument )
{
    TCLPROG_CODE *pCode;
    AREA_DATA *pArea;
    char command[MAX_INPUT_LENGTH];
    int vnum;

    argument = one_argument( argument, command );

    if ( is_number(command) || command[0]==0 )
    {
	if (command[0]==0) {
	    vnum=ch->in_room->rprog_vnum;
	} else {
	    vnum=atoi(command);
	}
	if ( !( pCode = get_rprog_index( vnum))) {
	    send_to_char( "RPEdit:  That vnum does not exist.\n\r", ch );
	    return;
	}

	edit_done( ch );

	if (IS_SET(pCode->area->area_flags,AREA_PROTECTED))
	    send_to_char("{RYou are changing a protected area.{x\r\n"
			 "{RChanges will be undone if you don't contact the head builder.{x\r\n",ch);

	ch->desc->pEdit = (void *)pCode;
	pCode->edit=TRUE;
	ch->desc->editor = ED_ROOMPROG;
	rpedit_show( ch, "" );
	return;
    }

    if ( command[0] == 'c' && !str_prefix( command, "create" ) )
    {
	if ( rpedit_create( ch, argument ) )
	{
	    pArea = get_vnum_area( atoi( argument ) );
	    SET_BIT( pArea->area_flags, AREA_CHANGED );

	    if (IS_SET(pArea->area_flags,AREA_PROTECTED))
		send_to_char("{RYou are changing a protected area.{x\r\n"
			     "{RChanges will be undone if you don't contact the head builder.{x\r\n",ch);

	    ch->desc->editor = ED_ROOMPROG;
	    rpedit_show( ch, "" );
	}
	return;
    }

    send_to_char( "RPEdit:  There is no default object to edit.\n\r", ch );
    return;
}

/* Entry point for editing aprog_code */
void do_apedit( CHAR_DATA *ch, char *argument )
{
    TCLPROG_CODE *pCode;
    AREA_DATA *pArea;
    char command[MAX_INPUT_LENGTH];
    int vnum;

    argument = one_argument( argument, command );

    if ( is_number(command) || command[0]==0 )
    {
	if (command[0]==0) {
	    vnum=ch->in_room->area->vnum;
	} else {
	    vnum=atoi(command);
	}
	if ( !( pCode = get_aprog_index( vnum))) {
	    send_to_char( "APEdit:  That vnum does not exist.\n\r", ch );
	    return;
	}

	edit_done( ch );

	if (IS_SET(pCode->area->area_flags,AREA_PROTECTED))
	    send_to_char("{RYou are changing a protected area.{x\r\n"
			 "{RChanges will be undone if you don't contact the head builder.{x\r\n",ch);

	ch->desc->pEdit = (void *)pCode;
	pCode->edit=TRUE;
	ch->desc->editor = ED_AREAPROG;
	apedit_show( ch, "" );
	return;
    }

    if ( command[0] == 'c' && !str_prefix( command, "create" ) )
    {
	if ( apedit_create( ch, argument ) )
	{
	    pArea = get_vnum_area( atoi( argument ) );
	    SET_BIT( pArea->area_flags, AREA_CHANGED );

	    if (IS_SET(pArea->area_flags,AREA_PROTECTED))
		send_to_char("{RYou are changing a protected area.{x\r\n"
			     "{RChanges will be undone if you don't contact the head builder.{x\r\n",ch);

	    ch->desc->editor = ED_AREAPROG;
	    apedit_show( ch, "" );
	}
	return;
    }

    send_to_char( "APEdit:  There is no default object to edit.\n\r", ch );
    return;
}


/* Entry point for editing help-files */
void do_hedit( CHAR_DATA *ch, char *argument )
{
    HELP_DATA *pHelp;
    AREA_DATA *pArea;
    char command[MAX_INPUT_LENGTH];

    argument = one_argument( argument, command );

    pArea=get_help_area(ch);

    if ( command[0] == 'c' && !str_prefix( command, "create" ) )
    {
	if ( hedit_create( ch, argument ) )
	{
	    SET_BIT( pArea->area_flags, AREA_CHANGED );

	    if (IS_SET(pArea->area_flags,AREA_PROTECTED))
		send_to_char("{RYou are changing a protected area.{x\r\n"
			     "{RChanges will be undone if you don't contact the head builder.{x\r\n",ch);

	    ch->desc->editor = ED_HELP;
	    hedit_show( ch, "" );
	}
	return;
    }

    if(!(pHelp=get_help_data( pArea, command)))
    {
    	send_to_char("HEdit: Keywords not found.\n\r",ch);
    	return;
    }

    edit_done( ch );

    if (IS_SET(pArea->area_flags,AREA_PROTECTED))
	send_to_char("{RYou are changing a protected area.{x\r\n"
		     "{RChanges will be undone if you don't contact the head builder.{x\r\n",ch);

    ch->desc->pEdit = (void *)pHelp;
    ch->desc->editor=ED_HELP;
    hedit_show( ch, "" );

    return;
}


/* Entry point for editing socials */
void do_socialedit( CHAR_DATA *ch, char *argument )
{
    char command[MAX_INPUT_LENGTH];
    SOCIAL_TYPE *pSocial;

    argument = one_argument( argument, command );

    if (command[0]==0) {
	send_to_char("SocialEdit: try with a social to edit.\n\r",ch);
	return;
    }

    if ( command[0] == 'c' && !str_prefix( command, "create" ) )
    {
	if ( socialedit_create( ch, argument ) )
	{
	    ch->desc->editor = ED_SOCIAL;
	    ch->desc->pEdit = social_hash[(int)argument[0]];
	    socialedit_show( ch, "" );
	}
	return;
    }

    pSocial=social_hash[(int)command[0]];
    while (pSocial) {
	if (!str_cmp(command,pSocial->name))
	    break;
	pSocial=pSocial->next;
    }

    if (pSocial==NULL) {
	send_to_char("SocialEdit: Keyword not found.\n\r",ch);
	return;
    }

    edit_done( ch );
    ch->desc->pEdit = (void *)pSocial;
    ch->desc->editor=ED_SOCIAL;
    socialedit_show( ch, "" );

    return;
}


/*****************************************************************************
 Name:		add_reset
 Purpose:	Inserts a new reset in the given index slot.
 Called by:	do_resets(olc.c).
 ****************************************************************************/
void add_reset( ROOM_INDEX_DATA *room, RESET_DATA *pReset, int index )
{
    RESET_DATA *reset;
    int iReset = 0;

    if ( !room->reset_first )
    {
	room->reset_first	= pReset;
	room->reset_last	= pReset;
	pReset->next		= NULL;
	return;
    }

    index--;

    if ( index == 0 )	/* First slot (1) selected. */
    {
	pReset->next = room->reset_first;
	room->reset_first = pReset;
	return;
    }

    /*
     * If negative slot( <= 0 selected) then this will find the last.
     */
    for ( reset = room->reset_first; reset->next; reset = reset->next )
    {
	if ( ++iReset == index )
	    break;
    }

    pReset->next	= reset->next;
    reset->next		= pReset;
    if ( !pReset->next )
	room->reset_last = pReset;
    return;
}



void do_resets( CHAR_DATA *ch, char *argument )
{
    char arg1[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    char arg3[MAX_INPUT_LENGTH];
    char arg4[MAX_INPUT_LENGTH];
    char arg5[MAX_INPUT_LENGTH];
    char arg6[MAX_INPUT_LENGTH];
    RESET_DATA *pReset = NULL;

    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );
    argument = one_argument( argument, arg3 );
    argument = one_argument( argument, arg4 );
    argument = one_argument( argument, arg5 );
    argument = one_argument( argument, arg6 );

    /*
     * Display resets in current room.
     * -------------------------------
     */
    if (arg1[0]==0) {
	if ( ch->in_room->reset_first ) {
	    send_to_char(
		"Resets: M = mobile, R = room, O = object, "
		"P = pet, S = shopkeeper\n\r", ch );
	    display_resets( ch );
	    return;
	}
	else
	    send_to_char( "No resets in this room.\n\r", ch );
    }

    /* just reset this room */
    if (!str_cmp(arg1,"room")) {
	ROOM_INDEX_DATA *pRoom = ch->in_room;
	ROOM_INDEX_DATA *ChRoom=NULL;
	int with_debug=0;
	char *arg;

	arg=arg2;
	if (is_number(arg2)) {
	    int vnum;

	    vnum=atoi(arg2);
	    if (!vnum || !(pRoom=get_room_index(vnum))) {
		send_to_char("No room found to reset\n\r",ch);
		return;
	    }
	    ChRoom=ch->in_room;
	    arg=arg3;
	}
	if (*arg) {
	    if (!str_prefix("debug",arg)) {
		if (is_channel_allowed(ch,WIZ_RESET_DEBUG) &&
		    !STR_IS_SET(ch->strbit_wiznet,WIZ_RESET_DEBUG))
		    with_debug=1;
	    }
	}

	wiznet(WIZ_RESETS,0,ch,NULL,
	    "%s [%d] has been reset by $N.\n\r",
	    pRoom->name,pRoom->vnum);

	if (ChRoom) char_from_room(ch);
	if (with_debug) {
	    STR_TOGGLE_BIT(ch->strbit_wiznet,WIZ_RESET_DEBUG);
	    update_wiznet(TRUE,ch->strbit_wiznet);
	}
	reset_room(pRoom);
	if (with_debug) {
	    STR_TOGGLE_BIT(ch->strbit_wiznet,WIZ_RESET_DEBUG);
	    update_wiznet(FALSE,NULL);
	}
	if (ChRoom) char_to_room(ch,ChRoom);
	send_to_char("Room reset.\n\r",ch);
	return;
    }

    /*
     * Take index number and search for commands.
     * ------------------------------------------
     */
    if ( is_number( arg1 ) )
    {
	ROOM_INDEX_DATA *pRoom = ch->in_room;

	if (IS_SET(pRoom->area->area_flags,AREA_PROTECTED))
	    send_to_char("{RYou are changing a protected area.{x\r\n"
			 "{RChanges will be undone if you don't contact the head builder.{x\r\n",ch);

	/*
	 * Delete a reset.
	 * ---------------
	 */
	if ( !str_cmp( arg2, "delete" ) )
	{
	    int insert_loc = atoi( arg1 );

	    if ( !ch->in_room->reset_first )
	    {
	        send_to_char( "No resets in this area.\n\r", ch );
	        return;
	    }

	    if ( insert_loc-1 <= 0 )
	    {
	        pReset = pRoom->reset_first;
	        pRoom->reset_first = pRoom->reset_first->next;
	        if ( !pRoom->reset_first )
	            pRoom->reset_last = NULL;
	    }
	    else
	    {
	        int iReset = 0;
	        RESET_DATA *prev = NULL;

	        for ( pReset = pRoom->reset_first;
	          pReset;
	          pReset = pReset->next )
	        {
	            if ( ++iReset == insert_loc )
	                break;
	            prev = pReset;
	        }

	        if ( !pReset )
	        {
	            send_to_char( "Reset not found.\n\r", ch );
	            return;
	        }

	        if ( prev )
	            prev->next = prev->next->next;
	        else
	            pRoom->reset_first = pRoom->reset_first->next;

	        for ( pRoom->reset_last = pRoom->reset_first;
	          pRoom->reset_last->next;
	          pRoom->reset_last = pRoom->reset_last->next );
	    }

	    free_reset( pReset );
	    SET_BIT(pRoom->area->area_flags,AREA_CHANGED);
	    send_to_char( "Reset deleted.\n\r", ch );

	}
	else
	/*
	 * Add a reset.
	 * ------------
	 */
	if ( (!str_cmp( arg2, "mob" ) && is_number( arg3 ))
	  || (!str_cmp( arg2, "obj" ) && is_number( arg3 )) )
	{
	    /*
	     * Check for Mobile reset.
	     * -----------------------
	     */
	    if ( !str_cmp( arg2, "mob" ) )
	    {
		pReset = new_reset();
		pReset->command = 'M';
		pReset->arg1    = atoi( arg3 );
		pReset->arg2    = is_number( arg5 ) ? atoi( arg5 ) : -1; /* Max global # */
		pReset->arg3    = ch->in_room->vnum;
		pReset->arg4	= is_number( arg4 ) ? atoi( arg4 ) : 1; /* Max local # */
	    }
	    else
	    /*
	     * Check for Object reset.
	     * -----------------------
	     */
	    if ( !str_cmp( arg2, "obj" ) )
	    {
		pReset = new_reset();
		pReset->arg1    = atoi( arg3 );
		/*
		 * Inside another object.
		 * ----------------------
		 */
		if ( !str_prefix( arg4, "inside" ) )
		{
		    pReset->command = 'P';
		    pReset->arg2    = is_number( argument ) ? atoi( argument ) : -1;
		    pReset->arg3    = atoi( arg5 );
		    pReset->arg4    = is_number( arg6 ) ? atoi( arg6 ) : 1;
		}
		else
		/*
		 * Inside the room.
		 * ----------------
		 */
		if ( !str_cmp( arg4, "room" ) )
		{
		    pReset->command = 'O';
		    pReset->arg2    = is_number( arg6 ) ? atoi( arg6 ) : -1; /* Max global # */
		    pReset->arg3    = ch->in_room->vnum;
		    pReset->arg4    = is_number( arg5 ) ? atoi( arg5 ) : 1; /* Max local # */
		}
		else
		/*
		 * Into a Mobile's inventory.
		 * --------------------------
		 */
		{
		    int loc;
		    if ( (loc=option_find_name( arg4, wear_loc_options,FALSE )) == NO_FLAG )
		    {
			send_to_char( "Resets: '? wear-loc'\n\r", ch );
			return;
		    }
		    pReset->arg2 = is_number(arg5) ? atoi(arg5) : -1;
		    pReset->arg3 = loc;
		    pReset->arg4 = 0;
		    if ( pReset->arg3 == WEAR_NONE )
			pReset->command = 'G';
		    else
			pReset->command = 'E';
		}
	    }

	    add_reset( ch->in_room, pReset, atoi( arg1 ) );
	    SET_BIT(pRoom->area->area_flags,AREA_CHANGED);
	    send_to_char( "Reset added.\n\r", ch );
	} else {
	    send_to_char( "Syntax: RESET <number> OBJ <vnum> <wear_loc> [<global max #>]\n\r", ch );
	    send_to_char( "	RESET <number> OBJ <vnum> in <vnum> [<# copies> [<global max #>]]\n\r", ch );
	    send_to_char( "	RESET <number> OBJ <vnum> room [<local max #> [<global max #>]]\n\r", ch );
	    send_to_char( "	RESET <number> MOB <vnum> [<local max #> [<global max #>]]\n\r", ch );
	    send_to_char( "	RESET <number> DELETE\n\r", ch );
	}
    }

    return;
}



/*****************************************************************************
 Name:		do_alist
 Purpose:	Normal command to list areas and display area information.
 Called by:	interpreter(interp.c)
 ****************************************************************************/
void do_alist( CHAR_DATA *ch, char *argument )
{
    char buf    [ MAX_STRING_LENGTH ];
    AREA_DATA *pArea;
    BUFFER *buffer;

    if (IS_NPC(ch))
	return;

    buffer=new_buf();

    sprintf( buf, "[%3s]  [%-27s] (%-5s-%5s) [%-10s] %3s [%-10s]\n\r",
       "Num", "Area Name", "lvnum", "uvnum", "Filename", "Sec", "Builders" );
    add_buf(buffer,buf);

    for ( pArea = area_first; pArea; pArea = pArea->next )
    {
	sprintf( buf, "[%3d] %c%-10s %-18s (%-5d-%5d) %-12.12s [%d] [%-10.10s]\n\r",
	     pArea->vnum,
	     IS_SET(pArea->area_flags, AREA_UNFINISHED)?'*':' ',
	     pArea->creator,
	     pArea->name,
	     pArea->lvnum,
	     pArea->uvnum,
	     pArea->filename,
	     pArea->security,
	     pArea->builders );
	     add_buf( buffer, buf );
    }

    add_buf(buffer,"\n\r* = unfinished\n\r");
    page_to_char( buf_string(buffer), ch );
    free_buf(buffer);
    return;
}
