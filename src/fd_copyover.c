//
// $Id: fd_copyover.c,v 1.14 2007/03/11 11:43:57 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"
#include "rdns_cache.h"
#include "interp.h"
#include "intermud3/fd_i3.h"

//
//  Copyover - Original idea: Fusion of MUD++
//

//
// Does the actual copyover:
// - save important data into a file.
// - try to start the mud again.
//
void copyover_execute(void) {
    FILE *fp;
    DESCRIPTOR_DATA *d, *d_next;
    char buf[MSL];

    if (( fp = fopen (mud_data.copyover_file, "w"))==NULL) {
        bugf("copyover: %s not writeable, aborted.\n\r",mud_data.copyover_file);
        logf("[0] copyover_execute(): Could not write to copyover file: %s", mud_data.copyover_file);
        return;
    }

    /* exec - descriptors are inherited */
#ifndef NO_INET4
    fprintf(fp, "mud4 %d\n", mud_data.control4_mud);
#ifdef HAS_HTTP
    fprintf(fp, "http4 %d\n", mud_data.control4_http);
#endif
#ifdef HAS_POP3
    fprintf(fp, "pop34 %d\n", mud_data.control4_pop3);
#endif
#endif

#ifdef INET6
    fprintf(fp, "mud6 %d\n", mud_data.control6_mud);
#ifdef HAS_HTTP
    fprintf(fp, "http6 %d\n", mud_data.control6_http);
#endif
#ifdef HAS_POP3
    fprintf(fp, "pop36 %d\n", mud_data.control6_pop3);
#endif
#endif
    fprintf(fp,"\n");

    sprintf(buf,"\n\r%s utters the words, 'earthquake' and the world trembles and shakes.\n\rYou freeze for a few seconds and hope everything will stop moving...\n\r\n\r",mud_data.reboot_by);

    /* For each playing descriptor, save its state */
    for (d = descriptor_list; d!=NULL ; d = d_next) {
        CHAR_DATA *och;

        d_next = d->next;

        if (!d->character || d->connected!=CON_PLAYING) { // drop logging on
            write_to_descriptor (d, "\n\rSorry, we're rebooting. Come back in a few minutes.\n\r",0);
#ifdef HAS_MCCP
            if (d->mccp || d->mccp2)
                compressEnd(d);
#endif

            close_socket (d); // throw'em out
            continue;
        }

        och=d->character;
        if (STR_IS_SET(och->strbit_act,PLR_SWITCHED))
            och=d->original;

#ifdef THREADED_DNS
#ifdef INET6
        if (d->ipv6)
            fprintf(fp,
                    "%d %s 6 %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x\n",
                    d->descriptor, och->name,
                    d->Host[0], d->Host[1], d->Host[2], d->Host[3],
                    d->Host[4], d->Host[5], d->Host[6], d->Host[7],
                    d->Host[8], d->Host[9], d->Host[10],d->Host[11],
                    d->Host[12],d->Host[13],d->Host[14],d->Host[15]);
        else
#endif
            fprintf (fp, "%d %s 4 %d.%d.%d.%d\n",
                     d->descriptor, och->name,
                     d->Host[0],d->Host[1],d->Host[2],d->Host[3]);
#else	// THREADED_DNS
        fprintf (fp, "%d %s %s\n",
                 d->descriptor, och->name, d->Host);
#endif	// THREADED_DNS

        save_char_obj (och);
        write_to_descriptor (d, buf, 0);
        if ( d->outtop > 0 )
            process_output( d, FALSE );
#ifdef HAS_MCCP
        if (d->mccp || d->mccp2)
            compressEnd(d);
#endif
    }

    fprintf (fp, "-1\n");
    fclose (fp);

    /* Close reserve and other always-open files and release other resources */
    fclose (fpReserve);

#ifdef I3
    // get rid of I3
    I3_shutdown(60);
#endif

#ifdef THREADED_DNS
    rdns_cache_destroy();
#endif

    chdir(mud_data.startup_dir);
    execv(mud_data.exe_file,mud_data.argv);

    /* Failed - sucessful exec will not return */
    logf("[0] copyover_execute(): execl(%s): %s",mud_data.exe_file,ERROR);
    chdir(mud_data.work_dir);
#ifdef THREADED_DNS
    rdns_cache_init();
#endif

    bugf("Copyover FAILED!");
    logf("[0] Copyover FAILED!");

    /* Here you might want to reopen fpReserve */
    fpReserve = fopen (NULL_FILE, "r");
}

//
// Recover from a copyover - load mud-data
//
void copyover_recover_mud(void) {
    FILE *fp;
    char buf[MSL],arg1[MSL],arg2[MSL],*p;

    logf("[0] Copyover recovery for mud initiated");

    if ((fp=fopen(mud_data.copyover_file,"r"))==NULL) {
        logf("[0] copyover_recover_players(): fopen(%s): %s",
             mud_data.copyover_file,ERROR);
        exit (1);
    }

    while (!feof(fp)) {
        fgets(buf,sizeof(buf),fp);

        // only read the first half of the file
        if (buf[0]=='\n')
            break;

        p=buf;
        p=one_argument(p,arg1);
        p=one_argument(p,arg2);
        switch (which_keyword(arg1,
                              "mud4","http4","pop34",
                              "mud6","http6","pop36",NULL)) {
        case -1:
        case 0:
        default:
            logf("[0] copyover_recover_mud(): unknown keyword '%s'",arg1);
            break;
#ifndef NO_INET4
        case 1:
            mud_data.control4_mud=atoi(arg2);
            break;
#ifdef HAS_HTTP
        case 2:
            mud_data.control4_http=atoi(arg2);
            break;
#endif
#ifdef HAS_POP3
        case 3:
            mud_data.control4_pop3=atoi(arg2);
            break;
#endif
#endif
#ifdef INET6
        case 4:
            mud_data.control6_mud=atoi(arg2);
            break;
#ifdef HAS_HTTP
        case 5:
            mud_data.control6_http=atoi(arg2);
            break;
#endif
#ifdef HAS_POP3
        case 6:
            mud_data.control6_pop3=atoi(arg2);
            break;
#endif
#endif
        }
    }
    fclose(fp);
}

//
// Recover from a copyover - load players
//
void copyover_recover_players(void) {
    DESCRIPTOR_DATA *d;
    CHAR_DATA *last_player;
    FILE *fp;
    char name[100];
    char buf[MIL];
#ifdef THREADED_DNS
    unsigned int h0,h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15;
#else
    char host[MSL];
#endif
    int desc;
    bool fOld;
#ifdef THREADED_DNS
    int type;
#endif

    logf("[0] Copyover recovery for players initiated");

    if ((fp=fopen(mud_data.copyover_file,"r"))==NULL) {
        logf("[0] copyover_recover_players(): fopen(%s): %s",
             mud_data.copyover_file,ERROR);
        exit (1);
    }

    //
    // skip first part of the file, that's for the mud.
    //
    while (!feof(fp)) {
        fgets(buf,sizeof(buf),fp);
        if (buf[0]=='\n')
            break;
    }

    // In case something crashes - doesn't prevent reading
    unlink (mud_data.copyover_file);

    for (;;) {
#ifdef THREADED_DNS
        fscanf (fp, "%d %s %d",&desc, name,&type);
#else
        fscanf (fp, "%d %s %s\n", &desc, name, host);
#endif
        if (desc == -1)
            break;

        /* Write something, and check if it goes error-free */
        if (!write_to_real_descriptor(desc,"\n\rThe world seems stable now...\n\r",0)) {
            close (desc); /* nope */

            while (fgetc(fp)!='\n') ;
            continue;
        }

        d = new_descriptor(DESTYPE_MUD);
        d->descriptor = desc;

#ifdef THREADED_DNS
        if (type==4) {
            d->ipv6=FALSE;
            fscanf(fp,"%u.%u.%u.%u\n", &h0,&h1,&h2,&h3);
            d->Host[0]=h0;
            d->Host[1]=h1;
            d->Host[2]=h2;
            d->Host[3]=h3;
        } else {
            d->ipv6=TRUE;
            fscanf(fp,"%x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x\n",
                   &h0,&h1,&h2,&h3,&h4,&h5,&h6,&h7,
                   &h8,&h9,&h10,&h11,&h12,&h13,&h14,&h15);
            d->Host[0]=h0;
            d->Host[1]=h1;
            d->Host[2]=h2;
            d->Host[3]=h3;
            d->Host[4]=h4;
            d->Host[5]=h5;
            d->Host[6]=h6;
            d->Host[7]=h7;
            d->Host[8]=h8;
            d->Host[9]=h9;
            d->Host[10]=h10;
            d->Host[11]=h11;
            d->Host[12]=h12;
            d->Host[13]=h13;
            d->Host[14]=h14;
            d->Host[15]=h15;
        }
#else
        free_string(d->Host);
        d->Host=str_dup(host);
#endif
        d->next = descriptor_list;
        descriptor_list = d;
        d->connected = CON_COPYOVER_RECOVER;
        // at least close_socket frees the char

        /* Now, find the pfile */
        fOld = load_char_obj (d, name);

        if (!fOld) { /* Player file not found?! */
            write_to_real_descriptor (desc,
                                      "\n\rSomehow, your character was lost "
                                      "in the copyover. Sorry.\n\r", 0);
            close_socket (d);
            continue;
        }

        // Just In Case
        if (!d->character->in_room)
            d->character->in_room = get_room_index (ROOM_VNUM_TEMPLE);

        // Insert in the char_list
        d->character->next = char_list;
        char_list = d->character;

        // Insert in the player_list
        // ... getting sick and tired of players complaining about the wholist being reversed on CO.
        //     so lets do it the hard way.
        if (player_list==NULL) {
            d->character->next_player = player_list;
            player_list = d->character;
        } else {
            for (last_player=player_list;last_player->next_player;last_player=last_player->next_player) ;
            d->character->next_player=NULL;
            last_player->next_player=d->character;
        }
        update_wiznet(TRUE,d->character->strbit_wiznet);

        char_to_room (d->character, d->character->in_room);
        look_room(d->character,d->character->in_room,TRUE);
        act ("$n materializes!", d->character, NULL, NULL, TO_ROOM);
        d->connected = CON_PLAYING;

        if (d->character->pet != NULL) {
            char_to_room(d->character->pet,d->character->in_room);
            act("$n materializes!.",d->character->pet,NULL,NULL,TO_ROOM);
        }

        init_char_descriptor(d,d->character);
        send_options(d);
    }
    fclose (fp);
}
