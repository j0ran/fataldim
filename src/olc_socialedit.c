//
// $Id: olc_socialedit.c,v 1.6 2006/01/29 10:57:20 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"
#include "olc.h"
#include "interp.h"
#include "db.h"

//
// social-editor
//
bool socialedit_show( CHAR_DATA *ch, char *argument )
{
    char buf[MAX_STRING_LENGTH];
    SOCIAL_TYPE *social;

    EDIT_SOCIAL(ch, social);

    sprintf( buf, "{y    Name           : {x%s {r%s{x\n\r"
		  "{ycna Char no arg    : {x%s\n\r"
		  "{yona Others no arg  : {x%s\n\r"
		  "{ycf  Char found     : {x%s\n\r"
		  "{yof  Others found   : {x%s\n\r"
		  "{yvf  Victim found   : {x%s\n\r"
		  "{ycnf Char not found : {x%s\n\r"
		  "{yca  Char auto      : {x%s\n\r"
		  "{yoa  Others auto    : {x%s\n\r",
	social->name,
	social->deleted?"*deleted*":"",
	social->char_no_arg,
	social->others_no_arg,
	social->char_found,
	social->others_found,
	social->vict_found,
	social->char_not_found,
	social->char_auto,
	social->others_auto);
    send_to_char( buf, ch );

    return TRUE;
}

bool socialedit_create(CHAR_DATA *ch, char *argument) {
    SOCIAL_TYPE *pSocial;
    char s[MAX_STRING_LENGTH];

    one_argument(argument,s);
    if (!s[0]) {
	send_to_char("Usage: socialedit create <social>\n\r",ch);
	return FALSE;
    }

    // first check if the social already exists
    pSocial=social_hash[(int)s[0]];
    while (pSocial) {
	if (!str_cmp(pSocial->name,s)) {
	    send_to_char("Social already exists.\n\r",ch);
	    return FALSE;
	}
	pSocial=pSocial->next;
    }

    pSocial=new_social();
    free_string(pSocial->name);
    pSocial->name=str_dup(argument);
    add_social(pSocial);
    return TRUE;
}

bool socialedit_name(CHAR_DATA *ch, char *argument) {
    SOCIAL_TYPE *social;

    EDIT_SOCIAL(ch, social);
    free_string(social->name);
    social->name=str_dup(argument);
    send_to_char("Name set.\n\r",ch);

    // relocate to sorted
    remove_social(social);
    add_social(social);

    return TRUE;
}

bool socialedit_delete(CHAR_DATA *ch, char *argument) {
    SOCIAL_TYPE *social;

    EDIT_SOCIAL(ch, social);
    social->deleted=!social->deleted;;
    sprintf_to_char(ch,"Social %s.\n\r",social->deleted?"deleted":"recovered");

    return TRUE;
}

bool  socialedit_cna(CHAR_DATA *ch, char *argument) {
    SOCIAL_TYPE *social;

    EDIT_SOCIAL(ch, social);
    free_string(social->char_no_arg);
    social->char_no_arg=str_dup(argument);
    send_to_char("Char No Args set.\n\r",ch);

    return TRUE;
}

bool  socialedit_ona(CHAR_DATA *ch, char *argument) {
    SOCIAL_TYPE *social;

    EDIT_SOCIAL(ch, social);
    free_string(social->others_no_arg);
    social->others_no_arg=str_dup(argument);
    send_to_char("Others No Args set.\n\r",ch);

    return TRUE;
}

bool  socialedit_cf(CHAR_DATA *ch, char *argument) {
    SOCIAL_TYPE *social;

    EDIT_SOCIAL(ch, social);
    free_string(social->char_found);
    social->char_found=str_dup(argument);
    send_to_char("Char Found set.\n\r",ch);

    return TRUE;
}

bool  socialedit_of(CHAR_DATA *ch, char *argument) {
    SOCIAL_TYPE *social;

    EDIT_SOCIAL(ch, social);
    free_string(social->others_found);
    social->others_found=str_dup(argument);
    send_to_char("Others Found set.\n\r",ch);

    return TRUE;
}

bool  socialedit_vf(CHAR_DATA *ch, char *argument) {
    SOCIAL_TYPE *social;

    EDIT_SOCIAL(ch, social);
    free_string(social->vict_found);
    social->vict_found=str_dup(argument);
    send_to_char("Victim Found set.\n\r",ch);

    return TRUE;
}

bool  socialedit_cnf(CHAR_DATA *ch, char *argument) {
    SOCIAL_TYPE *social;

    EDIT_SOCIAL(ch, social);
    free_string(social->char_not_found);
    social->char_not_found=str_dup(argument);
    send_to_char("Char Not Found set.\n\r",ch);

    return TRUE;
}

bool  socialedit_ca(CHAR_DATA *ch, char *argument) {
    SOCIAL_TYPE *social;

    EDIT_SOCIAL(ch, social);
    free_string(social->char_auto);
    social->char_auto=str_dup(argument);
    send_to_char("Char Auto.\n\r",ch);

    return TRUE;
}

bool  socialedit_oa(CHAR_DATA *ch, char *argument) {
    SOCIAL_TYPE *social;

    EDIT_SOCIAL(ch, social);
    free_string(social->others_auto);
    social->others_auto=str_dup(argument);
    send_to_char("Others Auto.\n\r",ch);

    return TRUE;
}

