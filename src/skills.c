/* $Id: skills.c,v 1.51 2009/04/22 19:55:35 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,	   *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *									   *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael	   *
 *  Chastain, Michael Quan, and Mitchell Tse.				   *
 *									   *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc	   *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.						   *
 *									   *
 *  Much time and thought has gone into this software and you are	   *
 *  benefitting.  We hope that you share your changes too.  What goes	   *
 *  around, comes around.						   *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

#include "merc.h"
#include "interp.h"
#include "color.h"

/****************************************************************************
* New skill function by Joran Jessurun (Fatal Dimensions)                   *
****************************************************************************/


bool skill_exists(int gsn)
{
  if (gsn<0 || gsn>=MAX_SKILL)
      return FALSE;
  return skill_table[gsn].name!=NULL;
}

char *skill_name(int gsn,CHAR_DATA *ch)
{
  // Funtion may not use ch
  if (gsn<0 || gsn>=MAX_SKILL) gsn=0;
  return skill_table[gsn].name;
}

bool skill_implemented(int gsn,CHAR_DATA *ch)
{
  // Function may note use ch
  if (gsn<0 || gsn>=MAX_SKILL) gsn=0;
  return skill_table[gsn].implemented;
}

int skill_level(int gsn,CHAR_DATA *ch)
{
  if (gsn<0 || gsn>=MAX_SKILL) gsn=0;
  if(!ch) return 92;
  return skill_table[gsn].Skill_level[ch->class][PC_RACE(ch)];
}

int skill_rating(int gsn,CHAR_DATA *ch)
{
  if (gsn<0 || gsn>=MAX_SKILL) gsn=0;
  if(!ch) return 1;
  return skill_table[gsn].Rating[ch->class][PC_RACE(ch)];
}

int skill_adept(int gsn,CHAR_DATA *ch)
{
  if (gsn<0 || gsn>=MAX_SKILL) gsn=0;
  if(!ch) return 75;
  return skill_table[gsn].skill_adept[ch->class][PC_RACE(ch)];
}

SPELL_FUN *skill_spell_fun(int gsn,CHAR_DATA *ch)
{
  if (gsn<0 || gsn>=MAX_SKILL) gsn=0;
  return skill_table[gsn].spell_fun;
}

WEAROFF_FUN *skill_wearoff_fun(int gsn,CHAR_DATA *ch)
{
  if (gsn<0 || gsn>=MAX_SKILL) gsn=0;
  return skill_table[gsn].wearoff_fun;
}

int skill_target(int gsn,CHAR_DATA *ch)
{
  if (gsn<0 || gsn>=MAX_SKILL) gsn=0;
  return skill_table[gsn].target;
}

int skill_minimum_position(int gsn,CHAR_DATA *ch)
{
  if (gsn<0 || gsn>=MAX_SKILL) gsn=0;
  return skill_table[gsn].minimum_position;
}

int *skill_pgsn(int gsn,CHAR_DATA *ch)
{
  // Function may not use ch
  if (gsn<0 || gsn>=MAX_SKILL) gsn=0;
  return skill_table[gsn].pgsn;
}

int skill_min_mana(int gsn,CHAR_DATA *ch)
{
  if (gsn<0 || gsn>=MAX_SKILL) gsn=0;
  if(!ch) return 100;
  return skill_table[gsn].Min_mana[ch->class][PC_RACE(ch)];
}

int skill_beats(int gsn,CHAR_DATA *ch)
{
  if (gsn<0 || gsn>=MAX_SKILL) gsn=0;
  return skill_table[gsn].beats;
}

char *skill_noun_damage(int gsn,CHAR_DATA *ch)
{
  if (gsn<0 || gsn>=MAX_SKILL) gsn=0;
  return skill_table[gsn].noun_damage;
}

char *skill_msg_off(int gsn,CHAR_DATA *ch)
{
  if (gsn<0 || gsn>=MAX_SKILL) gsn=0;
  if (skill_table[gsn].msg_off[0]==0)
    return NULL;
  else
    return skill_table[gsn].msg_off;
}

char *skill_msg_obj(int gsn,CHAR_DATA *ch)
{
  if (gsn<0 || gsn>=MAX_SKILL) gsn=0;
  if (skill_table[gsn].msg_obj[0]==0)
    return NULL;
  else
    return skill_table[gsn].msg_obj;
}

char *skill_msg_off_room(int gsn,CHAR_DATA *ch)
{
  if (gsn<0 || gsn>=MAX_SKILL) gsn=0;
  if (skill_table[gsn].msg_off_room[0]==0)
    return NULL;
  else
    return skill_table[gsn].msg_off_room;
}

/*

This function is broken currently (and not used anywhere)		EG

SKILLTABLE_TYPE *skill_skilltype(int gsn,CHAR_DATA *ch)
{
  static SKILLTABLE_TYPE skill;

  skill.name=skill_name(gsn,ch);
  skill.implemented=skill_implemented(gsn,ch);
  skill.skill_level=skill_level(gsn,ch);
  skill.rating=skill_rating(gsn,ch);
  skill.skill_adept=skill_adept(gsn,ch);
  skill.spell_fun=skill_spell_fun(gsn,ch);
  skill.wearoff_fun=skill_wearoff_fun(gsn,ch);
  skill.target=skill_target(gsn,ch);
  skill.minimum_position=skill_minimum_position(gsn,ch);
  skill.pgsn=skill_pgsn(gsn,ch);
  skill.min_mana=skill_min_mana(gsn,ch);
  skill.beats=skill_beats(gsn,ch);
  skill.noun_damage=skill_noun_damage(gsn,ch);
  skill.msg_off=skill_msg_off(gsn,ch);
  skill.msg_obj=skill_msg_obj(gsn,ch);
  skill.msg_off_room=skill_msg_off_room(gsn,ch);

  return &skill;
} */


bool has_gained_skill(CHAR_DATA *ch, int sn) {

    if (sn==-1) return TRUE; /* shorthand for level based skills */

    if (sn<-1 || sn >= MAX_SKILL) {
        bugf("Bad sn %d in has_gained_skill() for %s.",sn,NAME(ch));
	return FALSE;
    }

    if (!skill_implemented(sn,ch)) return FALSE;

    if (IS_NPC(ch)) return TRUE;

    return ch->pcdata->skill[sn].learned>0;
}

bool has_skill_available(CHAR_DATA *ch, int sn) {
    int skill;
    EFFECT_DATA *aff;

    if (sn==-1) return TRUE; /* shorthand for level based skills */

    if (sn<-1 || sn >= MAX_SKILL) {
        bugf("Bad sn %d in has_skill_available() for %s.",sn,NAME(ch));
	return FALSE;
    }

    if (!skill_implemented(sn,ch)) return FALSE;

    if (IS_NPC(ch)) return TRUE;

    if (ch->level < skill_level(sn,ch)) return FALSE;

    skill=ch->pcdata->skill[sn].learned;
    for (aff=ch->affected;aff;aff=aff->next) 
	if (aff->where==TO_SKILLS &&
	    aff->bitvector==sn)
	    skill+=aff->modifier;

    return skill>0;
}

// return the max % the skill has ever been
int get_peak_skill(CHAR_DATA *ch, int sn) {

    if (IS_NPC(ch) || sn==-1)
	return get_current_skill(ch,sn);

    if (!has_skill_available(ch,sn)) return 0;

    return ch->pcdata->skill[sn].learned;
}

// returns the skill % including forgetting bonus
int get_current_skill(CHAR_DATA *ch, int sn) {
    int skill;

    if (!has_skill_available(ch,sn)) return 0;

    if (sn==-1) {

	skill = ch->level * 5 / 2;

    } else if (!IS_NPC(ch)) {
	EFFECT_DATA *aff;

	skill = ch->pcdata->skill[sn].learned-ch->pcdata->skill[sn].forgotten;
	for (aff=ch->affected;aff;aff=aff->next) 
	    if (aff->where==TO_SKILLS &&
		aff->bitvector==sn)
		skill+=aff->modifier;

	skill=UMIN(100,skill);
	skill=UMAX(1,skill);

    } else { /* mobiles */

        if (skill_spell_fun(sn,ch) != spell_null)
            skill = 40 + 2 * ch->level;

        else if (sn == gsn_sneak || sn == gsn_hide)
            skill = ch->level * 2 + 20;

        else if ((sn == gsn_dodge && STR_IS_SET(ch->strbit_off_flags,OFF_DODGE))
        ||       (sn == gsn_parry && STR_IS_SET(ch->strbit_off_flags,OFF_PARRY)))
            skill = ch->level * 2;

        else if (sn == gsn_shield_block)
            skill = 10 + 2 * ch->level;

        else if (sn == gsn_second_attack
        && (STR_IS_SET(ch->strbit_act,ACT_WARRIOR) || STR_IS_SET(ch->strbit_act,ACT_THIEF)))
            skill = 10 + 3 * ch->level;

        else if (sn == gsn_third_attack && STR_IS_SET(ch->strbit_act,ACT_WARRIOR))
            skill = 4 * ch->level - 40;

        else if (sn == gsn_hand_to_hand)
            skill = 40 + 2 * ch->level;

        else if (sn == gsn_trip && STR_IS_SET(ch->strbit_off_flags,OFF_TRIP))
            skill = 10 + 3 * ch->level;

        else if (sn == gsn_tail && STR_IS_SET(ch->strbit_off_flags,OFF_TAIL))
            skill = 10 + 3 * ch->level;

        else if (sn == gsn_bash && STR_IS_SET(ch->strbit_off_flags,OFF_BASH))
            skill = 10 + 3 * ch->level;

        else if (sn == gsn_dirt && STR_IS_SET(ch->strbit_off_flags,OFF_KICK_DIRT))
            skill = 10 + 3 * ch->level;

        else if (sn == gsn_disarm
             &&  (STR_IS_SET(ch->strbit_off_flags,OFF_DISARM)
             ||   STR_IS_SET(ch->strbit_act,ACT_WARRIOR)
             ||   STR_IS_SET(ch->strbit_act,ACT_THIEF)))
            skill = 20 + 3 * ch->level;

        else if (sn == gsn_berserk && STR_IS_SET(ch->strbit_off_flags,OFF_BERSERK))
            skill = 3 * ch->level;

        else if (sn == gsn_kick)
            skill = 10 + 3 * ch->level;

        else if (sn == gsn_backstab && STR_IS_SET(ch->strbit_act,ACT_THIEF))
            skill = 20 + 2 * ch->level;

        else if (sn == gsn_rescue)
            skill = 40 + ch->level;

        else if (sn == gsn_recall)
            skill = 40 + ch->level;

        else if (sn == gsn_sword
        ||  sn == gsn_dagger
        ||  sn == gsn_spear
        ||  sn == gsn_mace
        ||  sn == gsn_axe
        ||  sn == gsn_flail
        ||  sn == gsn_whip
        ||  sn == gsn_polearm)
            skill = 40 + 5 * ch->level / 2;

        else if (sn == gsn_hunt)
	    skill = 25;

	else if (sn == gsn_scare)
	    skill = 100;

	else if (sn == gsn_wands)
	    skill = 25;

        else
           skill = 0;
    }
    return skill;
}

/* for returning skill information */
int get_skill(CHAR_DATA *ch, int sn)
{
    int skill;

    skill=get_current_skill(ch,sn);

    if (skill==0) return 0;

    if (ch->daze > 0)
    {
        if (skill_spell_fun(sn,ch) != spell_null)
            skill /= 2;
        else
            skill = 2 * skill / 3;
    }

    if ( !IS_NPC(ch) && ch->pcdata->condition[COND_DRUNK]  > 10 )
        skill = 9 * skill / 10;

    return URANGE(0,skill,100);
}

bool skillcheck(CHAR_DATA *ch, int gsn) {
    return skillcheck2(ch,gsn,100);
}

bool skillcheck2(CHAR_DATA *ch, int gsn, int basechance) {
    int chance;

    if (IS_PC(ch))
      ch->pcdata->skill[gsn].counter=0; // reset decay counter

    chance=basechance*get_skill(ch,gsn)*0.01;
    if (chance<100 && chance<number_percent()) return FALSE;
 
    return TRUE;
}

// Decay interval functie: ceil(rating*250*100/currentskill)

void update_skill_decay(CHAR_DATA *ch) {
    int sn;

    for (sn=0;sn<MAX_SKILL;sn++) {
	if (!has_skill_available(ch,sn))
	    continue;

	if ((++ch->pcdata->skill[sn].counter)<UMAX(1,skill_rating(sn,ch))*250*100/UMAX(1,get_current_skill(ch,sn)))
	    continue;

        if ((0.5+URANGE(1,ch->level,100)*0.5/100)*ch->pcdata->skill[sn].learned
	    <
	    get_current_skill(ch,sn)) { 
	    ch->pcdata->skill[sn].forgotten++;
	}

	ch->pcdata->skill[sn].counter=0;
    }
}

/****************************************************************************
* New group function by Joran Jessurun (Fatal Dimensions)                   *
****************************************************************************/

bool group_exists(int gn)
{
  return group_table[gn].name!=NULL;
}

bool group_spell_exists(int gn,CHAR_DATA *ch,int sn)
{
  return group_table[gn].spells[sn]!=NULL;
}

char *group_name(int gn,CHAR_DATA *ch)
{
  return group_table[gn].name;
}

int group_rating(int gn,CHAR_DATA *ch)
{
  if(!ch) return -1;
  return group_table[gn].Rating[ch->class][PC_RACE(ch)];
}

char *group_spell(int gn,CHAR_DATA *ch,int sn)
{
  return group_table[gn].spells[sn];
}

/***************************************************************************/


static int get_min_group_level(CHAR_DATA *ch,int gn)
{
  int i,level,tlevel;
  int n;

  level=MAX_LEVEL;
  for(i=0;i<MAX_IN_GROUP;i++)
  {
    if(!group_spell_exists(gn,ch,i)) break;

    n=skill_lookup(group_spell(gn,ch,i));
    if(n!=-1)
    {
      tlevel=skill_level(n,ch);
      level=UMIN(level,tlevel);
      continue;
    }
    n=group_lookup(group_spell(gn,ch,i));
    if(n!=-1)
    {
      tlevel=get_min_group_level(ch,n);
      level=UMIN(level,tlevel);
      continue;
    }
  }

  return level;
}


CHAR_DATA *find_npc(CHAR_DATA *ch, int bit)
{
  CHAR_DATA *npc;

  for(npc=ch->in_room->people; npc!=NULL ; npc = npc->next_in_room)
     if (IS_NPC(npc) && STR_IS_SET(npc->strbit_act,bit))
        break;

  if (npc!=NULL && !can_see(ch,npc))
     npc=NULL;

  return npc;
}

void do_practice( CHAR_DATA *ch, char *argument )
{
    int sn;
    char skillname[MSL];

    if ( IS_NPC(ch) ) /* mobs dont practice */
	return;

    one_argument(argument,skillname);

    if ( skillname[0] == '\0' ) {
	int col=0;
	BUFFER *buffer;
	char s[MAX_STRING_LENGTH];

	buffer = new_buf();

	for ( sn = 0; sn < MAX_SKILL; sn++ ) {
	    if (skill_name(sn,ch) == NULL )
		break;
	    if (ch->level < skill_level(sn,ch) ||
		!skill_implemented(sn,ch) ||
		!has_gained_skill(ch,sn))  /* skill is not known */
		continue;

	    sprintf(s,"%-20s %3d%% (%3d%%)  ",
		      skill_name(sn,ch),
		      get_peak_skill(ch,sn),
		      get_current_skill(ch,sn));
	    add_buf(buffer,s);
	    if ( ++col % 2 == 0 )
	    add_buf(buffer,"\n\r");
	}

	if ( col % 2 != 0 )
	    add_buf(buffer,"\n\r");
	sprintf(s,"You have %d practice sessions left.\n\r", ch->practice );
	add_buf(buffer,s);
	page_to_char(buf_string(buffer),ch);
	free_buf(buffer);
    } else {
	CHAR_DATA *mob;
	char *n;
	int adept;

	if (!IS_AWAKE(ch)) {
	    send_to_char("In your dreams, or what?\n\r", ch );
	    return;
	}

	sn=find_spell(ch,skillname);

	mob=find_npc(ch,ACT_PRACTICE);
	if (mob==NULL) {
	    if (sn>=0 &&
		has_gained_skill(ch,sn)) {		// skill already known
		sprintf_to_char(ch,
		    "%s is now at %d percent.\n\r",
		    skill_name(sn,ch),get_current_skill(ch,sn));
		if (ch->pcdata->skill[sn].forgotten>0) 
		    sprintf_to_char(ch,"It once was at %d percent.\n\r",
			get_peak_skill(ch,sn));
	    } else
		send_to_char("You can't do that here.\n\r",ch);
	    return;
	}

	if ( ch->practice <= 0 ) {
	    send_to_char( "You have no practice sessions left.\n\r", ch );
	    return;
	}

	if ((sn<0)
	||   ch->level < skill_level(sn,ch)
	||   !has_gained_skill(ch,sn)                  /* skill is not known */
	||   skill_rating(sn,ch)==0
	||   !skill_implemented(sn,ch)) {
	    send_to_char( "You can't practice that.\n\r", ch );
	    return;
	}

	n=skill_name(sn,ch);
	adept = skill_adept(sn,ch);

	if ( get_current_skill(ch,sn) >= adept ) {
	    sprintf_to_char(ch, "You are already learned at %s.\n\r",n);
	} else {
	    int bonus;
	    ch->practice--;
	    bonus=2*int_app[get_curr_stat(ch,STAT_INT)].learn / skill_rating(sn,ch);

	    if (ch->pcdata->skill[sn].forgotten>0) {
		if (ch->pcdata->skill[sn].forgotten>bonus) {
		    ch->pcdata->skill[sn].forgotten-=bonus;bonus=0;
		} else {
		    bonus-=ch->pcdata->skill[sn].forgotten;
		    ch->pcdata->skill[sn].forgotten=0;
		}
	    } else {
		bonus/=2;
		ch->pcdata->skill[sn].learned += bonus;
		if (ch->pcdata->skill[sn].learned > adept)
		    ch->pcdata->skill[sn].learned = adept;
	    }

	    ch->pcdata->skill[sn].counter=0;

	    if ( ch->pcdata->skill[sn].learned < adept ) {
		act( "You practice $T.",ch, NULL, n, TO_CHAR );
		act( "$n practices $T.",ch, NULL, n, TO_ROOM );
	    } else {
		act( "You are now learned at $T.",ch, NULL, n, TO_CHAR );
		act( "$n is now learned at $T.",ch, NULL, n, TO_ROOM );
	    }
	    sprintf_to_char(ch,
		"%s is now at %d percent, %d practice%s left.\n\r",
		n,get_current_skill(ch,sn),ch->practice,ch->practice==1?"":"s");
	}
    }
}


static void do_gain_list(CHAR_DATA *ch)
{
    int sn,gn,col;
    col = 0;

    send_to_char(
   "(level/cp) group          (level/cp) group          (level/cp) group\n\r"
   "------------------------- ------------------------- -------------------------\n\r",
       ch);

    for (gn = 0; gn < MAX_GROUP; gn++)
    {
	if (!group_exists(gn)) break;
	if (!ch->pcdata->group_known[gn]
	&& may_choose_group(ch,gn))
	{
	    sprintf_to_char(ch,"(%2d/%2d) %-17s ",
	      get_min_group_level(ch,gn),
	      group_rating(gn,ch),
	      group_name(gn,ch));
	    if (++col % 3 == 0) send_to_char("\n\r",ch);
	}
    }
    if ( col % 3 != 0 ) send_to_char( "\n\r", ch );

    col = 0;


    send_to_char(
      "\n\r"
      "(level/cp) skill          (level/cp) skill          (level/cp) skill\n\r"
      "------------------------- ------------------------- -------------------------\n\r",ch);

    for (sn = 0; sn < MAX_SKILL; sn++)
    {
	if(!skill_exists(sn)) break;

	if(!has_gained_skill(ch,sn)
	&& may_choose_skill(ch,sn)){
	    if (skill_implemented(sn,ch))
		sprintf_to_char(ch,"(%2d/%2d) %-17s ",
		    skill_level(sn,ch),
		    skill_rating(sn,ch),
		    skill_name(sn,ch));
	    else
		sprintf_to_char(ch,"( N/A ) %-17s ",
		    skill_name(sn,ch));
	    if (++col % 3 == 0) send_to_char("\n\r",ch);
	}
    }
    if ( col % 3 != 0 ) send_to_char( "\n\r", ch );
    send_to_char("\n\r",ch);
    sprintf_to_char(ch,
	"You have %d trainings- and %d practices sessions left.\n\r",
	ch->train,ch->practice);
}


static void do_gain_convert(CHAR_DATA *ch, CHAR_DATA *trainer)
{
    if (ch->practice < 10) {
	act("$N tells you '"C_TELL"You are not yet ready.{x'",
	    ch,NULL,trainer,TO_CHAR);
    } else {
	act("$N helps you apply your practice to training",
	    ch,NULL,trainer,TO_CHAR);
	ch->practice -= 10;
	ch->train +=1 ;
    }
}


static void do_gain_practice(CHAR_DATA *ch, CHAR_DATA *trainer)
{
    if (ch->train < 1) {
	act("$N tells you '"C_TELL"You are not yet ready.{x'",
	    ch,NULL,trainer,TO_CHAR);
    } else {
	act("$N helps you apply your training to practices.",
	    ch,NULL,trainer,TO_CHAR);
	ch->practice += 10;
	ch->train -=1 ;
    }
}

static void do_gain_points(CHAR_DATA *ch, CHAR_DATA *trainer)
{
    if (ch->train < 2) {
	act("$N tells you '"C_TELL"You are not yet ready.{x'",
	    ch,NULL,trainer,TO_CHAR);
	return;
    }

    if (ch->pcdata->points <= 40) {
	act("$N tells you '"C_TELL"There would be no point in that.{x'",
	    ch,NULL,trainer,TO_CHAR);
	return;
    }

    act("$N trains you, and you feel more at ease with your skills.",
	ch,NULL,trainer,TO_CHAR);
    ch->train -= 2;
    ch->pcdata->points -= 1;
    ch->exp = exp_per_level(ch,ch->pcdata->points) * ch->level;
}

static void do_gain_undo(CHAR_DATA *ch, CHAR_DATA *trainer)
{
    char property[MAX_INPUT_LENGTH];
    int sn,gn,i,gnc,snc;
    bool inothergr = FALSE;

    if (!GetCharProperty(ch,PROPERTY_STRING,"lastgained",property)) {
        act("$N tells you '"C_TELL"First gain something to undo.{x'", ch,NULL,trainer,TO_CHAR);
	return;
    }

    if ( (sn = skill_lookup(property)) != -1) {
        if (ch->pcdata->skill[sn].learned == 1)
	    ch->pcdata->skill[sn].learned = 0;
	else if (ch->pcdata->skill[sn].learned > 1) {
            act("$N tells you '"C_TELL"You are too advanced to forget the skill.{x'", ch,NULL,trainer,TO_CHAR);
	    DeleteCharProperty(ch,PROPERTY_STRING,"lastgained");
	    return;
	} else {
	    send_to_char("For a strange reason you don't know the skill set to undo, please report this as a bug.\n\r",ch);
	    return;
	}
	ch->train += skill_rating(sn,ch);
        act("$N tells you '"C_TELL"You have forgotten the skill.{x'", ch,NULL,trainer,TO_CHAR);
	DeleteCharProperty(ch,PROPERTY_STRING,"lastgained");
	return;
    }

    if ( (gn = group_lookup(property)) != -1) {
        if (ch->pcdata->group_known[gn] == 0) {
	    send_to_char("For a strange reason you don't know the group set to undo, please report this as a bug.\n\r",ch);
	    return;
	} else {
	    for (i=0 ; i<MAX_IN_GROUP ; i++) {
                if (!group_spell_exists(gn,ch,i)) break;
                sn = skill_lookup(group_spell(gn,ch,i));
	        for ( gnc = 0 ; gnc < MAX_GROUP ; gnc++) {
	            if (!group_exists(gnc) || gnc == gn || ch->pcdata->group_known[gnc] == 0)
	                continue;
		    for (snc = 0 ; snc < MAX_IN_GROUP ; snc++) {
		        if (!group_spell_exists(gnc,ch,snc)) break;
		        if (sn == skill_lookup(group_spell(gnc,ch,snc)))
		            inothergr = TRUE;
		    }
	        }
	        if ( inothergr == FALSE && ch->pcdata->skill[sn].learned > 1 ) {
	            act("$N tells you '"C_TELL"You are too advanced to forget the group.{x'", ch,NULL,trainer,TO_CHAR);
	            DeleteCharProperty(ch,PROPERTY_STRING,"lastgained");
	            return;
	        }
	        inothergr = FALSE;
	    }
	    for (i=0 ; i<MAX_IN_GROUP ; i++) {
                if (!group_spell_exists(gn,ch,i)) break;
	        sn = skill_lookup(group_spell(gn,ch,i));
	        for ( gnc = 0 ; gnc < MAX_GROUP ; gnc++) {
	            if (!group_exists(gnc) || gnc == gn || ch->pcdata->group_known[gnc] == 0)
	                continue;
		    for (snc = 0 ; snc < MAX_IN_GROUP ; snc++) {
		        if (!group_spell_exists(gnc,ch,snc)) break;
		        if (sn == skill_lookup(group_spell(gnc,ch,snc)))
		            inothergr = TRUE;
		    }
	        }
	        if ( inothergr == FALSE )
	            ch->pcdata->skill[sn].learned = 0;
	        inothergr = FALSE;
       	    }
	    ch->train += group_rating(gn,ch);
	    DeleteCharProperty(ch,PROPERTY_STRING,"lastgained");
	    act("$N tells you '"C_TELL"You have forgotten the group.{x'", ch,NULL,trainer,TO_CHAR);
	    ch->pcdata->group_known[gn] = 0;
	    return;
      }

    }
}

/* used to get new skills */
void do_gain(CHAR_DATA *ch, char *argument)
{
  char arg[MAX_STRING_LENGTH];
  CHAR_DATA *trainer;
  int gn,sn;

  if (IS_NPC(ch)) return;

  if (!IS_AWAKE(ch))
  {
    send_to_char("In your dreams, or what?\n\r", ch );
    return;
  }

  trainer=find_npc(ch,ACT_GAIN);
  if (trainer==NULL)
  {
    send_to_char("You can't do that here, try your guildmaster.\n\r",ch);
    return;
  }

  one_argument(argument,arg);
  switch(which_keyword(arg,"list","convert","practice","points","undo",NULL))
  {
     case -1:  /* no args */
        do_function(trainer,&do_say,"Pardon me? Maybe you should try '{Yhelp gain"C_SAY"'.");
        return;
     case  1:  /* list */
        do_gain_list(ch);
        return;
     case  2:  /* convert */
        do_gain_convert(ch,trainer);
        return;
     case  3: /* practice */
        do_gain_practice(ch,trainer);
     	return;
     case  4: /* points */
        do_gain_points(ch,trainer);
        return;
     case  5: /* undo */
        do_gain_undo(ch,trainer);
	return;
     case  0: /* all other words */
       gn = group_lookup(argument);
       if (gn > -1)
       {
	 if (ch->pcdata->group_known[gn])
	 {
	    act("$N tells you '"C_TELL"You already know that group!{x'",
		ch,NULL,trainer,TO_CHAR);
	    return;
	 }

	 if (!may_choose_group(ch,gn))
	 {
	    act("$N tells you '"C_TELL"That group is beyond your powers.{x'",
		ch,NULL,trainer,TO_CHAR);
	    return;
	 }

	 if (ch->train < group_rating(gn,ch))
	 {
	    act("$N tells you '"C_TELL"You are not yet ready for that group.{x'",
		ch,NULL,trainer,TO_CHAR);
	    return;
	 }

	 /* add the group */
	 gn_add(ch,gn);
	 act("$N trains you in the art of $t.",
	    ch,group_name(gn,ch),trainer,TO_CHAR);
	 ch->train -= group_rating(gn,ch);
	 SetCharProperty(ch,PROPERTY_STRING,"lastgained",group_name(gn,ch));
	 sprintf_to_char(ch,"You have %d trainings left.\n\r",ch->train);
	 return;
       }

       sn = skill_lookup(argument);
       if (sn > -1)
       {
	 if (skill_spell_fun(sn,ch) != spell_null)
	 {
	    act("$N tells you '"C_TELL"You must learn the full group. Use the 'info' command for more info.{x'",
		ch,NULL,trainer,TO_CHAR);
	    return;
	 }
         if (has_gained_skill(ch,sn))
         {
            act("$N tells you '"C_TELL"You already know that skill!{x'",
                ch,NULL,trainer,TO_CHAR);
            return;
         }
         if (!may_choose_skill(ch,sn))
         {
            act("$N tells you '"C_TELL"That skill is beyond your powers.{x'",
                ch,NULL,trainer,TO_CHAR);
            return;
         }
         if (ch->train < skill_rating(sn,ch))
         {
            act("$N tells you '"C_TELL"You are not yet ready for that skill.{x'",
                ch,NULL,trainer,TO_CHAR);
            return;
         }

         if(!skill_implemented(sn,ch))
         {
            act("$N tells you '"C_TELL"Good luck with that unimplemented skill.{x'",
              ch,NULL,trainer,TO_CHAR);
         }

         /* add the skill */
	 ch->pcdata->skill[sn].learned = 1;
         act("$N trains you in the art of $t.",
            ch,skill_name(sn,ch),trainer,TO_CHAR);
         ch->train -= skill_rating(sn,ch);
	 sprintf_to_char(ch,"You have %d trainings left.\n\r",ch->train);
         SetCharProperty(ch,PROPERTY_STRING,"lastgained",skill_name(sn,ch));
         return;
      }
        act("$N tells you '"C_TELL"I do not understand...{x'",
	    ch,NULL,trainer,TO_CHAR);
        return;
  }
}



/* RT spells and skills show the players spells (or skills) */

void do_spells(CHAR_DATA *ch, char *argument)
{
    BUFFER *buffer;
    char arg[MAX_INPUT_LENGTH];
    char spell_list[MAX_LEVEL][MAX_STRING_LENGTH];
    char spell_columns[MAX_LEVEL];
    int sn, level, min_lev = 1, max_lev = MAX_LEVEL, mana;
    bool fAll = FALSE, found = FALSE;
    char buf[MAX_STRING_LENGTH];
    CHAR_DATA *victim;

    if (IS_NPC(ch))
      return;

    if (IS_IMMORTAL(ch))
	max_lev=MAX_LEVEL;
    else
	max_lev=LEVEL_HERO;

    if (argument[0]) {
        if (get_trust(ch)<LEVEL_IMMORTAL)
            victim=ch;
        else {
            char arg[MSL];

            victim=get_char_world(ch,argument);
            if (!victim) {
                victim=ch;
            } else {
                if (IS_NPC(victim)) {
                    send_to_char("That's an NPC.\n\r",ch);
                    return;
                }
                if (get_trust(ch)<get_trust(victim)) {
                    sprintf_to_char(ch,"You're level %d, victim is"
                        "level %d, so bad luck.\n\r",
                        get_trust(ch),get_trust(victim));
                    return;
                }
                argument=one_argument(argument,arg);
            }
        }
    } else
        victim=ch;

    if (argument[0] != '\0')
    {
	fAll = TRUE;

	if (str_prefix(argument,"all"))
	{
	    argument = one_argument(argument,arg);
	    if (!is_number(arg))
	    {
		send_to_char("Arguments must be numerical or all.\n\r",ch);
		return;
	    }
	    max_lev = atoi(arg);

	    if (max_lev < 1 || max_lev > LEVEL_HERO)
	    {
		sprintf(buf,"Levels must be between 1 and %d.\n\r",LEVEL_HERO);
		send_to_char(buf,ch);
		return;
	    }

	    if (argument[0] != '\0')
	    {
		argument = one_argument(argument,arg);
		if (!is_number(arg))
		{
		    send_to_char("Arguments must be numerical or all.\n\r",ch);
		    return;
		}
		min_lev = max_lev;
		max_lev = atoi(arg);

		if (max_lev < 1 || max_lev > LEVEL_HERO)
		{
		    sprintf_to_char(ch,"Levels must be between 1 and %d.\n\r",LEVEL_HERO);
		    return;
		}

		if (min_lev > max_lev)
		{
		    send_to_char("That would be silly.\n\r",ch);
		    return;
		}
	    }
	}
    }


    /* initialize data */
    for (level = 0; level < MAX_LEVEL ; level++)
    {
        spell_columns[level] = 0;
        spell_list[level][0] = '\0';
    }

    for (sn = 0; sn < MAX_SKILL; sn++)
    {
        if (!skill_exists(sn))
	    break;

	if ((level = skill_level(sn,victim)) < MAX_LEVEL
	&&  (fAll || level <= victim->level)
	&&  level >= min_lev && level <= max_lev
	&&  skill_spell_fun(sn,victim) != spell_null
	&&  victim->pcdata->skill[sn].learned>0)
        {
	    found = TRUE;
	    level = skill_level(sn,victim);
	    if (ch->level < level)
	    	sprintf(buf,"%-18s        n/a          ", skill_name(sn,victim));
	    else
	    {
		mana = UMAX(skill_min_mana(sn,victim),
		    100/(2 + ch->level - level));
	        sprintf(buf,"%-18s  %3d%% (%3d%%) %3dma  ",
			skill_name(sn,victim),get_peak_skill(victim,sn),get_current_skill(victim,sn),mana);
	    }

	    if (spell_list[level][0] == '\0')
          	sprintf(spell_list[level],"\n\r%2d: %s",level,buf);
	    else /* append */
	    {
          	if ( ++spell_columns[level] % 2 == 0)
		    strcat(spell_list[level],"\n\r    ");
          	strcat(spell_list[level],buf);
	    }
	}
    }

    /* return results */

    if (!found)
    {
      	send_to_char("No spells found.\n\r",ch);
      	return;
    }

    buffer = new_buf();
    for (level = 0; level < MAX_LEVEL ; level++)
      	if (spell_list[level][0] != '\0')
	    add_buf(buffer,spell_list[level]);
    add_buf(buffer,"\n\r");
    page_to_char(buf_string(buffer),ch);
    free_buf(buffer);
}

void do_skills(CHAR_DATA *ch, char *argument)
{
    BUFFER *buffer;
    CHAR_DATA *victim;
    char arg[MAX_INPUT_LENGTH];
    char skill_list[MAX_LEVEL][MAX_STRING_LENGTH];
    char skill_columns[MAX_LEVEL];
    int sn, level, min_lev = 1, max_lev = MAX_LEVEL;
    bool fAll = FALSE, found = FALSE;
    char buf[MAX_STRING_LENGTH];

    if (IS_NPC(ch))
      return;

    if (IS_IMMORTAL(ch))
	max_lev=MAX_LEVEL;
    else
	max_lev=LEVEL_HERO;

    if (argument[0]) {
	if (get_trust(ch)<LEVEL_IMMORTAL)
	    victim=ch;
	else {
	    char arg[MSL];

	    victim=get_char_world(ch,argument);
	    if (!victim) {
		victim=ch;
	    } else {
		if (IS_NPC(victim)) {
		    send_to_char("That's an NPC.\n\r",ch);
		    return;
		}
		if (get_trust(ch)<get_trust(victim)) {
		    sprintf_to_char(ch,"You're level %d, victim is"
			"level %d, so bad luck.\n\r",
			get_trust(ch),get_trust(victim));
		    return;
		}
		argument=one_argument(argument,arg);
	    }
	}
    } else
	victim=ch;

    if (argument[0] != '\0')
    {
	fAll = TRUE;

	if (str_prefix(argument,"all"))
	{
	    argument = one_argument(argument,arg);
	    if (!is_number(arg))
	    {
		send_to_char("Arguments must be numerical or all.\n\r",ch);
		return;
	    }
	    max_lev = atoi(arg);

	    if (max_lev < 1 || max_lev > LEVEL_HERO)
	    {
		sprintf_to_char(ch,"Levels must be between 1 and %d.\n\r",LEVEL_HERO);
		return;
	    }

	    if (argument[0] != '\0')
	    {
		argument = one_argument(argument,arg);
		if (!is_number(arg))
		{
		    send_to_char("Arguments must be numerical or all.\n\r",ch);
		    return;
		}
		min_lev = max_lev;
		max_lev = atoi(arg);

		if (max_lev < 1 || max_lev > LEVEL_HERO)
		{
		    sprintf_to_char(ch,
			"Levels must be between 1 and %d.\n\r",LEVEL_HERO);
		    return;
		}

		if (min_lev > max_lev)
		{
		    send_to_char("That would be silly.\n\r",ch);
		    return;
		}
	    }
	}
    }


    /* initialize data */
    for (level = 0; level < MAX_LEVEL ; level++)
    {
        skill_columns[level] = 0;
        skill_list[level][0] = '\0';
    }

    for (sn = 0; sn < MAX_SKILL; sn++)
    {
        if (!skill_exists(sn) )
	    break;

	if ((level = skill_level(sn,victim)) < MAX_LEVEL
	&&  (fAll || level <= victim->level)
	&&  level >= min_lev && level <= max_lev
	&&  skill_spell_fun(sn,victim) == spell_null
	&&  victim->pcdata->skill[sn].learned>0)
        {
	    found = TRUE;
	    level = skill_level(sn,victim);
	    if (victim->level < level)
	    	sprintf(buf,"%-18s     n/a     ", skill_name(sn,victim));
	    else if(!skill_implemented(sn,victim))
	    	sprintf(buf,"%-17s* %3d%% (%3d%%) ", skill_name(sn,victim),
		    get_peak_skill(ch,sn),get_current_skill(ch,sn));
	    else
	    	sprintf(buf,"%-18s %3d%% (%3d%%) ",skill_name(sn,victim),
		    get_peak_skill(victim,sn),get_current_skill(victim,sn));

	    if (skill_list[level][0] == '\0')
          	sprintf(skill_list[level],"\n\rLevel %2d: %s",level,buf);
	    else /* append */
	    {
          	if ( ++skill_columns[level] % 2 == 0)
		    strcat(skill_list[level],"\n\r          ");
          	strcat(skill_list[level],buf);
	    }
	}
    }

    /* return results */

    if (!found)
    {
      	send_to_char("No skills found.\n\r",ch);
      	return;
    }

    buffer = new_buf();
    for (level = 0; level < MAX_LEVEL; level++)
      	if (skill_list[level][0] != '\0')
	    add_buf(buffer,skill_list[level]);
    add_buf(buffer,"\n\r");
    page_to_char(buf_string(buffer),ch);
    free_buf(buffer);
}

int exp_per_level(CHAR_DATA *ch, int points)
{
    int expl,inc;
    int class_mult;

    if (IS_NPC(ch)) return 1000;

    expl = 1000;
    inc = 500;

    if(pc_race_table[ch->race].class_mult[ch->class]==0) class_mult=1000;
    else class_mult=pc_race_table[ch->race].class_mult[ch->class];

    if (points < 40) return (1000 * class_mult)/100;

    /* processing */
    points -= 40;

    while (points > 9)
    {
	expl += inc;
        points -= 10;
        if (points > 9)
	{
	    expl += inc;
	    inc *= 2;
	    points -= 10;
	}
    }

    expl += points * inc / 10;

    return (expl * class_mult)/100;
}

/* shows all groups, or the sub-members of a group, the info command */
void do_groups(CHAR_DATA *ch, char *argument)
{
    int gn,sn,col,i;

    if (IS_NPC(ch)) return;

    col = 0;

    if (argument[0] == '\0')
    {   /* show all groups */

	send_to_char(
	   "All chosen groups:\n\r"
	   "(level/cp) group          (level/cp) group          (level/cp) group\n\r"
	   "------------------------- ------------------------- -------------------------\n\r",
	   ch);

	for (gn = 0; gn < MAX_GROUP; gn++)
        {
	    if (!group_exists(gn)) break;
	    if (ch->pcdata->group_known[gn]
	    || (ch->gen_data && ( ch->gen_data->group_chosen[gn]
	    || ch->gen_data->auto_group[gn])))
	    {
		sprintf_to_char(ch,"(%2d/%2d) %-17s ",
		    get_min_group_level(ch,gn),
		    group_rating(gn,ch),
		    group_name(gn,ch));
		if (++col % 3 == 0)
		    send_to_char("\n\r",ch);
	    }
        }
        if ( col % 3 != 0 )
            send_to_char( "\n\r", ch );

        if(!ch->gen_data)
	    sprintf_to_char(ch,"\n\rCreation points: %d\n\r",ch->pcdata->points);
	return;
     }

     if (!str_cmp(argument,"all"))    /* show all groups */
     {
	send_to_char(
	   "All groups:\n\r"
	   "(level/cp) group          (level/cp) group          (level/cp) group\n\r"
	   "------------------------- ------------------------- -------------------------\n\r",
	   ch);

        for (gn = 0; gn < MAX_GROUP; gn++)
        {
            if (!group_exists(gn)) break;
	    sprintf_to_char(ch,"(%2d/%2d) %-17s ",
		get_min_group_level(ch,gn),
		group_rating(gn,ch),
		group_name(gn,ch));
	    if (++col % 3 == 0)
		send_to_char("\n\r",ch);
        }
        if ( col % 3 != 0 ) send_to_char( "\n\r", ch );
	return;
     }


     /* show the sub-members of a group */
     gn = group_lookup(argument);
     if (gn == -1)
     {
	send_to_char("No group of that name exist.\n\r",ch);
        send_to_char("Type 'groups all' or 'info all' for a full listing.\n\r",ch);
	return;
     }

     sprintf_to_char(ch,
	 "Skills and groups that are in group '%s':\n\r"
	 "%s"
	 "(level/cp) skill/spell    (level/cp) skill/spell    (level/cp) skill/spell\n\r"
	 "------------------------- ------------------------- -------------------------\n\r",
	 group_name(gn,ch),
	 group_rating(gn,ch)==-1?"{RRemember, you can't gain this group.{x\n\r":"");

     for (sn = 0; sn < MAX_IN_GROUP; sn++)
     {
	if (!group_spell_exists(gn,ch,sn)) break;

        i=skill_lookup(group_spell(gn,ch,sn));
        if(i>=0) {
	    sprintf_to_char(ch,"(%2d/%2d) %-17s ",
		skill_level(i,ch),
		skill_rating(i,ch),
		skill_name(i,ch));
        }
        else
        {
	    i=group_lookup(group_spell(gn,ch,sn));
	    if(i<0)
		sprintf_to_char(ch,"Error: %-18s",group_spell(gn,ch,sn));
	    else {
		sprintf_to_char(ch,"(%2d/%2d) Grp %-13s ",
		    get_min_group_level(ch,i),
		    group_rating(i,ch),
		    group_name(i,ch));
	    }
        }

       if (++col % 3 == 0) send_to_char("\n\r",ch);
    }
    if ( col % 3 != 0 ) send_to_char( "\n\r", ch );
}

/* checks for skill improvement */
void check_improve( CHAR_DATA *ch, int sn, bool success, int multiplier )
{
    int chance;

    if (IS_NPC(ch))
	return;

    if (ch->level < skill_level(sn,ch)
    ||  skill_rating(sn,ch) == 0
    ||  !has_skill_available(ch,sn)
    ||  get_current_skill(ch,sn)>=100
    ||  !skill_implemented(sn,ch))
	return;  /* skill is not known */

    /* check to see if the character has a chance to learn */
    chance = 10 * int_app[get_curr_stat(ch,STAT_INT)].learn;
    chance /= (		multiplier
		*	skill_rating(sn,ch)
		*	4);
    chance += ch->level;
    if (ch->pcdata->skill[sn].forgotten>0) chance=chance/2;

    if (number_range(1,1000) > chance)
	return;

    /* now that the character has a CHANCE to learn, see if they really have */

    if (success)
    {
	chance = URANGE(5,100 - get_current_skill(ch,sn), 95);
	if (ch->pcdata->skill[sn].forgotten>0) chance=UMIN(100,chance*5);
	if (number_percent() < chance)
	{
	    if (ch->pcdata->skill[sn].forgotten>0) {
		sprintf_to_char(ch,"You regain some skill at %s!\n\r",skill_name(sn,ch));
	        ch->pcdata->skill[sn].forgotten--;
	    } else {
		sprintf_to_char(ch,"You have become better at %s!\n\r",skill_name(sn,ch));
	        ch->pcdata->skill[sn].learned++;
		gain_exp(ch,2 * skill_rating(sn,ch),TRUE);
	    }
	}
    }

    else
    {
	chance = URANGE(5,get_current_skill(ch,sn)/2,30);
	if (ch->pcdata->skill[sn].forgotten>0) chance=chance*5;
	if (number_percent() < chance)
	{
	    if (ch->pcdata->skill[sn].forgotten>0) {
		sprintf_to_char(ch,"Your mistakes remind you of how %s is supposed to work.\n\r",skill_name(sn,ch));
		ch->pcdata->skill[sn].forgotten-=number_range(1,3);
		if (ch->pcdata->skill[sn].forgotten<0)
		    ch->pcdata->skill[sn].forgotten=0;
	    } else {
		sprintf_to_char(ch,"You learn from your mistakes, and your %s skill improves.\n\r",skill_name(sn,ch));
		ch->pcdata->skill[sn].learned += number_range(1,3);
		ch->pcdata->skill[sn].learned = UMIN(ch->pcdata->skill[sn].learned,100);
		gain_exp(ch,2 * skill_rating(sn,ch),TRUE);
	    }
	}
    }
}

/* returns a group index number given the name */
int group_lookup( const char *name )
{
    int gn;

  if (!name) return -1;   /* Check for valid name */
  for ( gn = 0; (gn<MAX_GROUP) && (group_exists(gn)) ; gn++ )
    {
        if ( LOWER(name[0]) == LOWER(group_name(gn,NULL)[0])
        &&   !str_prefix( name, group_name(gn,NULL) ) )
            return gn;
    }
    return -1;
}

/* recursively adds a group given its number -- uses group_add */
void gn_add( CHAR_DATA *ch, int gn)
{
    int i;

    ch->pcdata->group_known[gn] = TRUE;
    for ( i = 0; (i<MAX_IN_GROUP) && (group_spell(gn,ch,i)); i++)
      group_add(ch,group_spell(gn,ch,i),FALSE);
}

void gendata_clear(CHAR_DATA *ch)
{
  int i;

  for(i=0;i<MAX_SKILL;i++)
  {
    ch->gen_data->skill_chosen[i]=FALSE;
    ch->gen_data->auto_skill[i]=FALSE;
  }
  for(i=0;i<MAX_GROUP;i++)
  {
    ch->gen_data->group_chosen[i]=FALSE;
    ch->gen_data->auto_group[i]=FALSE;
  }
  ch->gen_data->points_chosen=0;
}

void gendata_group_add( CHAR_DATA *ch,const char *name,bool deduct)
{
  int sn,gn,i;

  if(IS_NPC(ch) || !ch->gen_data) return;

  /* Check Skill */
  sn=skill_lookup(name);
  if(sn!=-1)
  {
    /* If we are in auto mode and skill was chosen, uncheck it */
    if(!deduct && ch->gen_data->skill_chosen[sn])
    {
      ch->gen_data->skill_chosen[sn]=FALSE;
      ch->gen_data->points_chosen-=skill_rating(sn,ch);
    }

    if(ch->pcdata->skill[sn].learned<=0
    && !ch->gen_data->skill_chosen[sn]
    && !ch->gen_data->auto_skill[sn])
    {
      if(!deduct) ch->gen_data->auto_skill[sn]=TRUE;
      else ch->gen_data->skill_chosen[sn]=TRUE;
      if(deduct)
        ch->gen_data->points_chosen+=skill_rating(sn,ch);
    }
    return;
  }

  /* Check Group */
  gn=group_lookup(name);
  if(gn!=-1)
  {
    /* If we are in auto mode and group is chosen, uncheck it */
    if(!deduct && ch->gen_data->group_chosen[gn])
    {
      ch->gen_data->group_chosen[gn]=FALSE;
      ch->gen_data->points_chosen-=group_rating(gn,ch);
    }

    if(ch->pcdata->group_known[gn]<=0
    && !ch->gen_data->group_chosen[gn]
    && !ch->gen_data->auto_group[gn])
    {
      if(!deduct) ch->gen_data->auto_group[gn]=TRUE;
      else ch->gen_data->group_chosen[gn]=TRUE;
      for(i=0;i<MAX_IN_GROUP;i++)
      {
        if(!group_spell_exists(gn,ch,i)) break;
        gendata_group_add(ch,group_spell(gn,ch,i),FALSE);
      }
      if(deduct)
        ch->gen_data->points_chosen += group_rating(gn,ch);
    }
    return;
  }
}

void gendata_group_remove(CHAR_DATA *ch,const char *name)
{
  int sn,gn,i,j;

  if(IS_NPC(ch) || !ch->gen_data) return;

  /* Check Skill */
  sn=skill_lookup(name);
  if(sn!=-1)
  {
    if(ch->gen_data->skill_chosen[sn])
      ch->gen_data->points_chosen-=skill_rating(sn,ch);
    ch->gen_data->skill_chosen[sn]=FALSE;
    ch->gen_data->auto_skill[sn]=FALSE;
    return;
  }

  /* Check Group */
  gn=group_lookup(name);
  if(gn!=-1)
  {
    if(ch->gen_data->group_chosen[gn])
      ch->gen_data->points_chosen -= group_rating(gn,ch);
    ch->gen_data->group_chosen[gn]=FALSE;
    ch->gen_data->auto_group[gn]=FALSE;
    for(i=0;i<MAX_IN_GROUP;i++)
    {
      if(!group_spell_exists(gn,ch,i)) break;
      gendata_group_remove(ch,group_spell(gn,ch,i));
    }
    /* Now auto add all chosen groups */
    for(i=0;i<MAX_GROUP;i++)
    {
      if(!group_exists(i)) break;
      if(ch->gen_data->group_chosen[i])
      {
        for(j=0;j<MAX_IN_GROUP;j++)
        {
          if(!group_spell_exists(i,ch,j)) break;
          gendata_group_add(ch,group_spell(i,ch,j),FALSE);
        }
      }
    }
    return;
  }
}

void gendata_to_pcdata(CHAR_DATA *ch)
{
  int i;

  for(i=0;i<MAX_GROUP;i++)
    if(!ch->pcdata->group_known[i])
  {
    ch->pcdata->group_known[i]=ch->gen_data->group_chosen[i] ||
                               ch->gen_data->auto_group[i];
  }
  for(i=0;i<MAX_SKILL;i++)
    if(!ch->pcdata->skill[i].learned
    && (ch->gen_data->skill_chosen[i] || ch->gen_data->auto_skill[i]))
  {
    ch->pcdata->skill[i].learned=1;
  }
  ch->pcdata->points+=ch->gen_data->points_chosen;
}

/* use for processing a skill or group for addition  */
void group_add( CHAR_DATA *ch, const char *name, bool deduct)
{
    int sn,gn,i;

    if (IS_NPC(ch)) /* NPCs do not have skills */
	return;

    sn = skill_lookup(name);

    if (sn != -1)
    {
	if (ch->pcdata->skill[sn].learned == 0) /* i.e. not known */
	{
	    ch->pcdata->skill[sn].learned = 1;
	    if (deduct)
	   	ch->pcdata->points += skill_rating(sn,ch);
	}
	return;
    }

    /* now check groups */

    gn = group_lookup(name);

    if (gn != -1)
    {
	if (ch->pcdata->group_known[gn] == FALSE)
	{
	    ch->pcdata->group_known[gn] = TRUE;
	    if (deduct)
		ch->pcdata->points += group_rating(gn,ch);
	}

        for ( i = 0; (i<MAX_IN_GROUP) && (group_spell_exists(gn,ch,i)); i++)
          group_add(ch,group_spell(gn,ch,i),FALSE);
    }
}

/* Specialize command */
void do_specialize( CHAR_DATA *ch, char *argument )
{
  CHAR_DATA *mob;

  if ( IS_NPC(ch) ) /* mobs dont specialize */
	return;

  mob=find_npc(ch,ACT_PRACTICE);
  if (mob==NULL)
  {
    send_to_char("You can't do that here.\n\r",ch);
    return;
  }

#ifdef notdef

  if(class_table[ch->class].spec_level==MAX_LEVEL)
  {
    if(class_table[ch->class].spec_off>=0)
      send_to_char("You can't specialize anymore.\n\r",ch);
    else
      send_to_char("You will never be able to specialize.\n\r",ch);
    return;
  }

  if(class_table[ch->class].spec_level>ch->level)
  {
    sprintf_to_char(ch,"You can't specialize until you are level %d.\n\r",
      class_table[ch->class].spec_level);
    return;
  }

  {
    char buf[MAX_STRING_LENGTH];
    int iClass;
    bool first;
    ROOM_INDEX_DATA *was_in_room;

    act("$n goes to another plane of existence to specialize.",ch,NULL,NULL,TO_ROOM);

    save_char_obj(ch);	/* Save character before the spec */
    was_in_room=ch->in_room;
    char_from_room(ch);
    ch->was_in_room=was_in_room;
    strcpy(buf,"Hi pupil, choose a specialisation.\n\r");
    strcat(buf,"Specialisations [");
    for(iClass=0,first=TRUE;iClass<MAX_CLASS;iClass++)
    {
      if(class_table[iClass].spec_off!=ch->class) continue;
      if(!first) strcat(buf," ");
      strcat(buf,class_table[iClass].name);
      first=FALSE;
    }
    strcat(buf,"]: ");
    send_to_char(buf,ch);
    ch->desc->connected=CON_GET_SPEC_CLASS;
  }
#else
  #pragma message("Specialization code needs to be rewritten here.")
  send_to_char("Specialization code is not operational yet.\n\r",ch);
#endif

  return;
}

/* Calculate the number of points spend for race and class choice, also
   called the base points */
int base_points(CHAR_DATA *ch)
{
  int points,iClass;

  points=pc_race_table[ch->race].points;
  iClass=ch->class;
  while(iClass>=0)
  {
    points+=class_table[iClass].points;
    iClass=class_table[iClass].spec_off;
  }

  return points;
}


bool may_choose_group(CHAR_DATA *ch,int gn)
{
  int level;

  level=class_table[ch->class].spec_level-1;
  level=UMIN(level,LEVEL_HERO);

  if(group_rating(gn,ch)>0
  && get_min_group_level(ch,gn)<=level)
    return TRUE;
  else
    return FALSE;
}

bool may_choose_skill(CHAR_DATA *ch,int sn)
{
  int level;

  level=class_table[ch->class].spec_level-1;
  level=UMIN(level,LEVEL_HERO);

  if(skill_spell_fun(sn,ch)==spell_null
  && skill_rating(sn,ch)>0
  && skill_level(sn,ch)<=level)
    return TRUE;
  else
    return FALSE;
}

