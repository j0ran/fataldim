//
// $Id: fd_swear.c,v 1.25 2009/04/22 18:25:38 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"
#include "db.h"

static char	swear_list[MSL];
static char	swear_overview[MSL];
static char *	swear_reply[MAX_SWEARREPLY][2];
static char *	swear_short_descr;
int		top_swearreply;

// This routine may only be called once
void load_swearlist( void )
{
    FILE *	fp;
    int		count;
    char *	p,s[2];
    char	buf[MIL],new[MIL];

    if ((fp = fopen(mud_data.swear_file,"r")) == NULL)
    {
	bugf("load_swearlist(): Couldn't open swear file '%s': %s",
	    mud_data.swear_file,ERROR);
	fclose(fp);
	return;
    }

    swear_short_descr=fread_string(fp);	// Name of the swear mob.
    count=fread_number(fp);
    if (count<0 || count>MAX_SWEARREPLY) {
	bugf("Too much swear reply's (%d).",count);
	fclose(fp);
	return;
    }

    for (top_swearreply=0;top_swearreply<count;top_swearreply++) {
	swear_reply[top_swearreply][0]=fread_string(fp);
	swear_reply[top_swearreply][1]=fread_string(fp);
    }

    //
    // The swearwords itself are stored in swear_overview, but they are
    // never used (except in the swearlist-command).
    // Each swearword is is now a regular expression
    // ([\{\}].)* is search for a colour-code, they are placed between
    // letters so that lines which are tricky are still caught.
    //
    swear_overview[0]=0;
    strcpy(swear_list,"(");
    while (!feof(fp)) {
	strcpy(buf,fread_word(fp));
	if (buf[0]=='#') break;

	if (swear_overview[0]!=0) strcat(swear_overview,", ");;
	strcat(swear_overview,buf);

	new[0]=0;
	for (p=buf;*p!=0;p++) {
	    strcat(new,"([\\{\\}].)*");
	    s[0]=*p;
	    s[1]=0;
	    strcat(new,s);
	}
	if (strlen(swear_list)>2)
	    strcat(swear_list,"|");
	strcat(swear_list,new);
    }
    strcat(swear_list,")");
    logf("[0] Swear words: %s",swear_overview);

    fclose(fp);

    return;
}

void do_swearlist( CHAR_DATA *ch, char *argument ) {
    sprintf_to_char(ch,"Words that are not allowed to use:\n\r%s\n\r",
	swear_overview);
    STR_SET_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);
    sprintf_to_char(ch,"Words that are not allowed to use:\n\r%s\n\r",
	swear_list);
    STR_REMOVE_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);
}


char *check_swearing(char *sentence) {
    static char buffer[MAX_INPUT_LENGTH];
    int		j,length;
    char *	s;
    bool	found=FALSE;
    regex_t	preg;
    regmatch_t	mtch;
    int		retval;

    regcomp(&preg,swear_list,REG_EXTENDED|REG_ICASE);
    retval=regexec(&preg,sentence,1,&mtch,0);
    if (retval==0) {
      strncpy(buffer,sentence,MAX_INPUT_LENGTH);
    }
    while (retval==0) {
	s=buffer+mtch.rm_so;
	length=mtch.rm_eo-mtch.rm_so;
	for (j=0;j<length;j++)
	    s[j]='-';
	retval=regexec(&preg,buffer,1,&mtch,REG_NOTBOL);
	found=TRUE;
    }
    regfree(&preg);
    return found?buffer:NULL;
}

void complain_swearing(CHAR_DATA *ch,bool personal) {
    int line;

    if (IS_NPC(ch)) return;
    if (top_swearreply==0) return;

    if (ch->pcdata->swearing>=top_swearreply)
	line=top_swearreply-1;
    else
	line=ch->pcdata->swearing;

    if (top_swearreply>4 && ch->pcdata->swearing>=top_swearreply-4) {
	STR_SET_BIT(ch->strbit_comm, COMM_NOCHANNELS);
	logf("[%d] nochanneling %s due to swearing",
	    GET_DESCRIPTOR(ch),ch->name);
	if (STR_IS_SET(ch->strbit_act,PLR_ORDERED) && ch->master) {
	    STR_SET_BIT(ch->master->strbit_comm, COMM_NOCHANNELS);
	    logf("[%d] nochanneling %s due to swearing, after all (s)he is the master",
		GET_DESCRIPTOR(ch->master),ch->master->name);
	}
    }

    if (personal) {
	//
	// the remember is a dirty hack because act()'s don't get shown
	// to people sleeping.
	//
	int oldpos=ch->position;
	if (ch->position<POS_RESTING)
	    ch->position=POS_STANDING;
	act(swear_reply[line][1],ch,swear_short_descr,ch,TO_CHAR);
	ch->position=oldpos;
    } else {
	CHAR_DATA *player,*victim;
	for (player=player_list;player!=NULL;player=player->next_player ) {

	victim=STR_IS_SET(player->strbit_act,PLR_SWITCHED)
			? player->pcdata->switched_into : player;

	act(swear_reply[line][0],victim,swear_short_descr,ch,TO_CHAR);
	}
    }

    ch->pcdata->swearing++;
}
