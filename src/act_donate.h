//
// $Id: act_donate.h,v 1.3 2002/08/25 10:01:42 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef INCLUDED_ACT_DONATE_H
#define INCLUDED_ACT_DONATE_H

char *	players_god		(CHAR_DATA *ch);
bool	sacrifice		(CHAR_DATA *ch,OBJ_DATA *obj,int *reward,
				 bool mShow);
void    award_sacrifice 	(CHAR_DATA *ch,int silver);
bool	donate_into_room	(CHAR_DATA *ch,OBJ_DATA *obj,
				 ROOM_INDEX_DATA *room,int *reward,bool mShow);
bool	donate_into_obj		(CHAR_DATA *ch,OBJ_DATA *obj,
				 OBJ_DATA *target_obj,int *reward,bool mShow);
extern  bool donate_force_find_targets;

#endif	// INCLUDED_ACT_DONATE_H
