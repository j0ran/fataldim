//
// $Id: fd_tcl_core.c,v 1.49 2008/03/06 22:06:12 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#define NEEDS_TCL
#include "merc.h"
#include "interp.h"
#include "color.h"
#include "olc.h"

/* current_namespace contains the namespace identifier of the current mob/obj area/room or obj */
char current_namespace[80]="";

/* mobprog type contains the type of the currently running mobprog */

Tcl_Interp *Master_interp;  /* The globally used tcl interpeter */
Tcl_Interp *interp;  /* The globally used tcl safe interpeter */

/* Data managed by tcl_start, if structure is updated, please check the
 * functions tcl_extracted_obj and tcl_extracted_char
 */
#define TCL_MAX_STACK 2
#define TCL_PROGMAX_STACK 20

struct _progData *progData;
static struct _progData progDataStack[TCL_MAX_STACK];
static int progDataStackPos=0;

int progType=PROGTYPE_NONE;
static int progTypeStackPos=0;
static int progTypeStack[TCL_PROGMAX_STACK]={PROGTYPE_NONE};

#if TCL_MAJOR_VERSION>8
#define TCL_USE_LIMITS
#elif TCL_MAJOR_VERSION==8
#if TCL_MINOR_VERSION>=5
#define TCL_USE_LIMITS
#endif
#endif

#ifdef TCL_USE_LIMITS
Tcl_Time tcl_time_limit = { 0, 250000 };
#else
#warning: not using tcl resource limits
#endif

char * tcl_AddSlashes(const char *src) {

    static char *buf=NULL;
    static int  bufsize;

    char *spos;
    char *dpos;

    if ((!buf) || (bufsize<(strlen(src)*2+1))) {
        if (buf) free(buf);
        bufsize=strlen(src)*2+1;
        buf=(char *)malloc(bufsize);
        logf("Allocate new tcl_AddSlashes buffer. (size=%d)",bufsize,buf);
    }

    spos=(char *)src;
    dpos=buf;

    while (*spos!=0) {
        switch (*spos) {
        case '"':
        case '{':
        case '}':
        case '\\': {
            *(dpos++)='\\';
        }
        }
        *(dpos++)=*(spos++);
    }
    *(dpos++)=0;
    return buf;
}

void tcl_progTypePush(int type) {
    if (progTypeStackPos==TCL_PROGMAX_STACK) {
        bugf("Serious tclprog error: progType stack overflow");
        exit(0);
    }
    progTypeStack[progTypeStackPos++]=type;
    progType=type;
}
void tcl_progTypePop(void) {
    if (progTypeStackPos==0) {
        bugf("Serious tclprog error: progType stack underflow");
        exit(0);
    }
    progTypeStackPos--;
    if (progTypeStackPos>0)
        progType=progTypeStack[progTypeStackPos-1];
    else
        progType=PROGTYPE_NONE;
}


bool tcl_start(char *name,char *trig,CHAR_DATA *mob,CHAR_DATA *ch,OBJ_DATA *obj,CHAR_DATA *ch2,OBJ_DATA *obj2,ROOM_INDEX_DATA *room, AREA_DATA *area) {
    if (progDataStackPos>=TCL_MAX_STACK) {
#if TCL_MAX_STACK>1
        bugf("Serious tclprog error: stack overflow. "
             "There are %d simultanious triggers executing, max is %d.",
             progDataStackPos,TCL_MAX_STACK);
        bugf("name: %s",name==NULL?"null":name);
        bugf("trig: %s",trig==NULL?"null":trig);
        bugf("mob: %d",
             mob==NULL?0:mob->pIndexData==NULL?0:mob->pIndexData->vnum,
             mob==NULL?"null":mob->short_descr);
        bugf("ch: %d %s",
             ch==NULL?0:ch->pIndexData==NULL?0:ch->pIndexData->vnum,
             ch==NULL?"null":ch->name);
        bugf("obj: %d",
             obj==NULL?0:obj->pIndexData==NULL?0:obj->pIndexData->vnum,
             obj==NULL?"null":obj->short_descr);
        bugf("ch2: %d",
             ch2==NULL?0:ch2->pIndexData==NULL?0:ch2->pIndexData->vnum,
             ch2==NULL?"null":ch2->name);
        bugf("obj2: %d",
             obj2==NULL?0:obj2->pIndexData==NULL?0:obj2->pIndexData->vnum,
             obj2==NULL?"null":obj2->short_descr);
        bugf("room: %d %s",
             room==NULL?0:room->vnum,
             room==NULL?"":room->name);
        bugf("area: %d %s",
             area==NULL?0:area->vnum,
             area==NULL?"":area->name);
#endif
        return TRUE;
    }
    progData=&progDataStack[progDataStackPos];
    progDataStackPos++;

    progData->mob=mob;
    progData->ch=ch;
    progData->obj=obj;
    progData->ch2=ch2;
    progData->room=room;
    progData->area=area;
    progData->obj2=obj2;
    progData->lastCh=NULL;
    progData->lastObj=NULL;
    progData->lastRoom=NULL;
    progData->lastArea=NULL;
    progData->text1=NULL;
    progData->text2=NULL;
    progData->trigValue=trig;
    progData->trigName=name;

    return FALSE;
}


bool tcl_stop(void) {
    if (progDataStackPos<=0) {
        bugf("Serious mobprog error: stack underflow. "
             "Calls to tcl_start and tcl_stop (in the C source) are not balanced",0);
        return TRUE;
    }

    progData->mob=NULL;
    progData->ch=NULL;
    progData->obj=NULL;
    progData->ch2=NULL;
    progData->obj2=NULL;
    progData->room=NULL;
    progData->area=NULL;
    progData->lastCh=NULL;
    progData->lastObj=NULL;
    progData->lastRoom=NULL;
    progData->lastArea=NULL;
    progData->text1=NULL;
    progData->text2=NULL;
    progData->trigValue=NULL;
    progData->trigName=NULL;

    progDataStackPos--;
    if (progDataStackPos==0) progData=NULL;
    else progData=&progDataStack[progDataStackPos-1];

    return FALSE;
}

/*
 * Namespace support routines
 */
char *namespace_darea(AREA_DATA *area) {
    static char namespace[80];  /* 80 should be more then enough. */
    sprintf(namespace,"::area%d",area->vnum);
    return namespace;
}

char *namespace_dobj(OBJ_INDEX_DATA *dobj) {
    char *namespace=namespace_darea(dobj->area);
    sprintf(namespace+strlen(namespace),"::obj%d",dobj->vnum);
    return namespace;
}

char *namespace_dmob(MOB_INDEX_DATA *dmob) {
    char *namespace=namespace_darea(dmob->area);
    sprintf(namespace+strlen(namespace),"::mob%d",dmob->vnum);
    return namespace;
}

char *namespace_char(CHAR_DATA *ch) {
    if (IS_NPC(ch)) {
        char *namespace=namespace_dmob(ch->pIndexData);
        sprintf(namespace+strlen(namespace),"::id%d",(int)ch->id);
        return namespace;
    } else {
        static char namespace[40];  /* should be enough for player namespaces */
        sprintf(namespace,"char%d",(int)ch->id);
        return namespace;
    }
}

char *namespace_obj(OBJ_DATA *obj) {
    char *namespace=namespace_dobj(obj->pIndexData);
    sprintf(namespace+strlen(namespace),"::id%d",(int)obj->id);
    return namespace;
}

char *namespace_room(ROOM_DATA *room) {
    char *namespace=namespace_darea(room->area);
    sprintf(namespace+strlen(namespace),"::room%d",room->vnum);
    return namespace;
}

char *namespace_area(AREA_DATA *area) {
    char *namespace=namespace_darea(area);
    sprintf(namespace+strlen(namespace),"::area%d",area->vnum);
    return namespace;
}



/*
 * Creates the namespace and parses the initial script
 */
void tcl_create_char_instance(CHAR_DATA *ch) {
    if (IS_PC(ch)) return;

    if (ch->pIndexData->mprog_vnum!=0) {
        TCLPROG_CODE *prg;
        char fullcode[MSL*2];

        if ((prg=get_mprog_index(ch->pIndexData->mprog_vnum))
                && !prg->edit
                && !ch->pIndexData->area->areaprogram_edit) {
            if (tcl_start(NULL,NULL,ch,NULL,NULL,NULL,NULL,NULL,NULL)) return;
            strcpy(fullcode,ch->pIndexData->area->areaprogram);
            strcat(fullcode,prg->code);
            if (tcl_run_mp_code(ch,fullcode)==TCL_ERROR)
                mp_error(ch);
            if (tcl_stop()) return;
        }
    }
}

void tcl_create_obj_instance(OBJ_DATA *obj) {
    if (obj->pIndexData->oprog_vnum!=0) {
        TCLPROG_CODE *prg;
        char fullcode[MSL*2];

        if ((prg=get_oprog_index(obj->pIndexData->oprog_vnum))
                && !prg->edit
                && !obj->pIndexData->area->areaprogram_edit) {
            if (tcl_start(NULL,NULL,NULL,NULL,obj,NULL,NULL,NULL,NULL)) return;
            strcpy(fullcode,obj->pIndexData->area->areaprogram);
            strcat(fullcode,prg->code);
            if (tcl_run_op_code(obj,fullcode)==TCL_ERROR)
                op_error(obj);
            if (tcl_stop()) return;
        }
    }
}

void tcl_create_room_instance(ROOM_DATA *room) {
    if (room->rprog_vnum!=0) {
        TCLPROG_CODE *prg;
        char fullcode[MSL*2];

        if ((prg=get_rprog_index(room->rprog_vnum))
                && !prg->edit
                && !room->area->areaprogram_edit) {
            if (tcl_start(NULL,NULL,NULL,NULL,NULL,NULL,NULL,room,NULL)) return;
            strcpy(fullcode,room->area->areaprogram);
            strcat(fullcode,prg->code);
            if (tcl_run_rp_code(room,fullcode)==TCL_ERROR)
                rp_error(room);
            if (tcl_stop()) return;
        }
    }
}

void tcl_create_area_instance(AREA_DATA *area) {
    if (area->aprog_enabled && area->aprog) {
        TCLPROG_CODE *prg;
        char fullcode[MSL*2];

        if ((prg=area->aprog)
                && !prg->edit
                && !area->areaprogram_edit) {
            if (tcl_start(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,area)) return;
            strcpy(fullcode,area->areaprogram);
            strcat(fullcode,prg->code);
            if (tcl_run_ap_code(area,fullcode)==TCL_ERROR)
                ap_error(area);
            if (tcl_stop()) return;
        }
    }
}



/*
 * Deletes the namespace
 */
void tcl_delete_room_instance(ROOM_DATA *room) {
    char buf[1000];

    /* delete the namespace if it exists */
    sprintf(buf,"namespace delete %s;",namespace_room(room));
    Tcl_Eval(interp,buf);
}

void tcl_extracted_room(ROOM_DATA *room) {
    int i;

    for (i=0;i<progDataStackPos;i++) {
        if (room==progDataStack[i].room) progDataStack[i].room=NULL;
    }
}

void tcl_delete_area_instance(AREA_DATA *area) {
    char buf[1000];

    /* delete the namespace if it exists */
    sprintf(buf,"namespace delete %s;",namespace_area(area));
    Tcl_Eval(interp,buf);
}

void tcl_extracted_area(AREA_DATA *area) {
    int i;

    for (i=0;i<progDataStackPos;i++) {
        if (area==progDataStack[i].area) progDataStack[i].area=NULL;
    }
}

void tcl_delete_obj_instance(OBJ_DATA *obj) {
    char buf[1000];

    /* delete the namespace if it exists */
    sprintf(buf,"namespace delete %s;",namespace_obj(obj));
    Tcl_Eval(interp,buf);
}

void tcl_extracted_obj(OBJ_DATA *obj) {
    int i;

    for (i=0;i<progDataStackPos;i++) {
        if (obj==progDataStack[i].obj) progDataStack[i].obj=NULL;
        if (obj==progDataStack[i].obj2) progDataStack[i].obj2=NULL;
        if (obj==progDataStack[i].lastObj) progDataStack[i].lastObj=NULL;
    }
}

void tcl_delete_char_instance(CHAR_DATA *ch) {
    char buf[1000];

    if (IS_PC(ch)) return;

    /* delete the namespace if it exists */
    sprintf(buf,"namespace delete %s;",namespace_char(ch));
    Tcl_Eval(interp,buf);
}

void tcl_extracted_char(CHAR_DATA *ch) {
    int i;

    for (i=0;i<progDataStackPos;i++) {
        if (ch==progDataStack[i].mob) progDataStack[i].mob=NULL;
        if (ch==progDataStack[i].ch) progDataStack[i].ch=NULL;
        if (ch==progDataStack[i].ch2) progDataStack[i].ch2=NULL;
        if (ch==progDataStack[i].lastCh) progDataStack[i].lastCh=NULL;
        if (ch==progDataStack[i].lastRoomCh) progDataStack[i].lastRoomCh=NULL;
    }
}



/*
 * --------------------------------------------------------------------
 * TCL Support routines.
 *
 * --------------------------------------------------------------------
 */

/*
 * Get a random PC in the room (for $r parameter)
 * type: 1=pc, 2=mob, 3=char
 */
CHAR_DATA *tcl_get_random_char(ROOM_DATA *room, CHAR_DATA *mob, int type )
{
    CHAR_DATA *vch, *victim = NULL;
    int now = 0, highest = 0;
    for ( vch = (mob?(mob->in_room):room)->people; vch; vch = vch->next_in_room ) {
        if ( mob!=vch
             &&   (type==3 || (type==1 && IS_PC(vch)) || (type==2 && IS_NPC(vch)))
             &&   (!mob || can_see( mob, vch ))
             &&   ( now = number_percent() ) > highest )
        {
            victim = vch;
            highest = now;
        }
    }
    return victim;
}

/* args values:
 * 0 - not used
 * 1 - string
 * 2 - char
 * 4 - obj
 * 8 - door
 * 1024 - error
 */
bool tcl_parse_act(char *format,int *arg1,int *arg2)
{
    int state;

    *arg1=0;
    *arg2=0;

    state=0;
    while(*format) {
        switch(state) {
        case 0:
            if (*format=='{' || *format=='}') state=2;
            else if (*format=='$') state=1;
            break;
        case 1:
            switch(*format) {
            case 't': *arg1|=1;break;
            case 'T': *arg2|=1;break;
            case 'n':
            case 'e':
            case 'm':
            case 's': break; /* If the char itself, not used for arg1 or arg2 */
            case 'N':
            case 'E':
            case 'M':
            case 'S': *arg2|=2;break;
            case 'p': *arg1|=4;break;
            case 'P': *arg2|=4;break;
            case 'd': *arg2|=8;break;
            default: *arg1=1024;*arg2=1024;return FALSE;
            }
            state=0;
            break;
        case 2:
            state=0;
            break;
        }
        format++;
    }

    if (*arg1!=0 && *arg1!=1 && *arg1!=2 && *arg1!=4 && *arg1!=8) *arg1=1024;
    if (*arg2!=0 && *arg2!=1 && *arg2!=2 && *arg2!=4 && *arg2!=8) *arg2=1024;

    if (*arg1==1024 || *arg2==1024) return FALSE;
    else return TRUE;
}

OBJ_DATA *tcl_check_obj(const OBJ_DATA *cobj)
{
    OBJ_DATA *obj;

    for (obj=object_list;obj;obj=obj->next)
        if (obj==cobj) return obj;

    return NULL;
}

CHAR_DATA *tcl_check_ch(const CHAR_DATA *cch)
{
    CHAR_DATA *ch;

    for (ch=char_list;ch;ch=ch->next)
        if (ch==cch) return ch;

    return NULL;
}

void fixObjTriggerFlags(OBJ_DATA *obj) {
    TCLPROG_LIST *oprg;

    STR_ZERO_BIT(obj->strbit_oprog_triggers,OPROG_MAX_TRIGGERS);
    for (oprg=obj->oprogs;oprg;oprg=oprg->next)
        STR_SET_BIT(obj->strbit_oprog_triggers,oprg->trig_type);
}

void fixRoomTriggerFlags(ROOM_DATA *room) {
    TCLPROG_LIST *rprg;

    STR_ZERO_BIT(room->strbit_rprog_triggers,RPROG_MAX_TRIGGERS);
    for (rprg=room->rprogs;rprg;rprg=rprg->next)
        STR_SET_BIT(room->strbit_rprog_triggers,rprg->trig_type);
}

void fixAreaTriggerFlags(AREA_DATA *area) {
    TCLPROG_LIST *aprg;

    STR_ZERO_BIT(area->strbit_aprog_triggers,APROG_MAX_TRIGGERS);
    for (aprg=area->aprogs;aprg;aprg=aprg->next)
        STR_SET_BIT(area->strbit_aprog_triggers,aprg->trig_type);
}

void fixMobTriggerFlags(CHAR_DATA *ch)
{
    TCLPROG_LIST *mprg;

    STR_ZERO_BIT(ch->strbit_mprog_triggers,MPROG_MAX_TRIGGERS);
    for (mprg=ch->mprogs;mprg;mprg=mprg->next)
        STR_SET_BIT(ch->strbit_mprog_triggers,mprg->trig_type);
}

// String id will not be changed
CHAR_DATA *tcl_charid(ROOM_DATA *room,char *id,CHAR_DATA *start_pos)
{
    char *colon,*dot,*s;
    bool searchGlobal=FALSE;
    bool mobOnly=FALSE;
    bool pcOnly=FALSE;
    bool useVNum=FALSE;
    CHAR_DATA *result;
    int idValue;
    int count,number;

    // get slot
    if (id[0]=='#') {
        number=atoi(id+1);
        if (number<0 || number>=PROGDATA_SLOTS)
            return NULL;
        return progData->char_store[number];
    }

    if (!room) searchGlobal=TRUE;

    // Handle options
    if ((colon=strchr(id,':'))) {
        for (s=id;*s!=':';s++) {
            switch(*s) {
            case 'g': searchGlobal=TRUE;break;
            case 'm': mobOnly=TRUE;break;
            case 'p': pcOnly=TRUE;break;
            case 'v': useVNum=TRUE;break;
            }
        }
    }

    // Skip options
    if (colon) s=colon+1;else s=id;

    // Handle 2.grum structures
    if ((dot=strchr(s,'.'))) {
        number=atoi(s);
        if (number==0) return NULL;
        s=dot+1;
    }
    else number=1;

    // check if we search for an id or a name
    if (isdigit(*s))
    {
        idValue=atoi(s);
        if (idValue==0) return NULL;
    } else {
        idValue=0;
        if (*s=='\0') return NULL;
    }

    // Initialise the search
    if (start_pos) result=start_pos;
    else {
        if (searchGlobal) result=char_list;
        else result=room->people;
    }
    count=1;

    while(result) {
        if ((mobOnly || useVNum) && IS_PC(result)) goto next;
        if (pcOnly && IS_NPC(result)) goto next;
        if (idValue) {
            if (useVNum && result->pIndexData->vnum==idValue && number==count++) break;
            else if (!useVNum && result->id==idValue) break;
        }
        else if (is_name(s,result->name) && number==count++) break;

next:
        if (searchGlobal) result=result->next;else result=result->next_in_room;
    }

    return result;
}

CHAR_DATA *tcl_charid_char(CHAR_DATA *ch,char *id,CHAR_DATA *start_pos) {
    ROOM_DATA *room=NULL;
    if (ch) room=ch->in_room;
    return tcl_charid(room,id,start_pos);
}

CHAR_DATA *tcl_charid_obj(OBJ_DATA *obj,char *id,CHAR_DATA *start_pos) {
    ROOM_DATA *room=NULL;

    if (!obj) return NULL;
    while (obj->in_obj) obj=obj->in_obj;

    if (obj->carried_by) room=obj->carried_by->in_room;
    if (obj->in_room)    room=obj->in_room;

    return tcl_charid(room,id,start_pos);
}

OBJ_DATA *tcl_get_obj_carry( CHAR_DATA *ch, char *argument, CHAR_DATA *viewer )
{
    char arg[MAX_INPUT_LENGTH];
    OBJ_DATA *obj;
    int number;
    int count;

    number = number_argument( argument, arg );
    count  = 0;
    for ( obj = ch->carrying; obj != NULL; obj = obj->next_content )
    {
        if ( obj->wear_loc == WEAR_NONE
             &&   (can_see_obj( viewer, obj ) )
             &&   is_name( arg, obj->name ) )
        {
            if ( ++count == number )
                return obj;
        }
    }

    return NULL;
}


// String id will not be changed
ROOM_INDEX_DATA *tcl_location(char *id)
{
    char *colon,*dot,*s;
    bool mobOnly=FALSE;
    bool pcOnly=FALSE;
    bool mobOrPc=FALSE;
    bool objSearch=FALSE;
    ROOM_INDEX_DATA *result;
    OBJ_DATA *obj;
    CHAR_DATA *ch;
    int idValue;
    int count,number;

    // Handle options
    if ((colon=strchr(id,':'))) {
        for (s=id;*s!=':';s++) {
            switch(*s) {
            case 'c': mobOrPc=TRUE;break;
            case 'm': mobOnly=TRUE;break;
            case 'p': pcOnly=TRUE;break;
            case 'o': objSearch=TRUE;break;
            }
        }
    }

    // Skip options
    if (colon) s=colon+1;else s=id;

    // Handle 2.grum structures
    if ((dot=strchr(s,'.'))) {
        number=atoi(s);
        if (number==0) return NULL;
        s=dot+1;
    }
    else number=1;

    // check if we search for an id or a name
    if (isdigit(*s)) {
        idValue=atoi(s);
        if (idValue==0) return NULL;
    } else {
        idValue=0;
        if (*s=='\0') return NULL;
    }

    if (idValue!=0 && !mobOrPc && !mobOnly && !objSearch) {
        result=get_room_index(idValue);
        return result;
    }

    count=1;

    if (objSearch) {
        for (obj=object_list;obj;obj=obj->next) {
            if (idValue && obj->id==idValue) break;
            if (!idValue && is_name(s,obj->name) && number==count++ && obj->in_room) break;
        }

        if (obj && obj->in_room) return obj->in_room;
        else return NULL;
    } else // char search
    {
        for (ch=char_list;ch;ch=ch->next) {
            if (mobOnly && IS_PC(ch)) continue;
            if (pcOnly && IS_NPC(ch)) continue;
            if (idValue && ch->id==idValue) break;
            if (!idValue && is_name(s,ch->name) && number==count++ && ch->in_room) break;
        }

        if (ch && ch->in_room) return ch->in_room;
        else return NULL;
    }

    return NULL;
}


// String id will not be changed
OBJ_DATA *tcl_objid(OBJ_DATA *obj_list,char *id,bool isworldlist) {
    char *dot,*s;
    int number,count;
    int idValue;
    int objType=NO_FLAG;
    OBJ_DATA *result;
    char *colon;
    bool useVNum=FALSE;
    bool searchType=FALSE;

    // get slot
    if (id[0]=='#') {
        number=atoi(id+1);
        if (number<0 || number>=PROGDATA_SLOTS)
            return NULL;
        return progData->obj_store[number];
    }

    // Handle options
    if ((colon=strchr(id,':'))) {
        for (s=id;*s!=':';s++) {
            switch(*s) {
            case 'v': useVNum=TRUE;break;
            case 't': searchType=TRUE;break;
            }
        }
    }

    // Skip options
    if (colon) s=colon+1;else s=id;

    // Handle 2.grum structures
    if ((dot=strchr(s,'.'))) {
        number=atoi(s);
        if (number==0) return NULL;
        s=dot+1;
    } else
        number=1;

    // check if we search for an id or a name
    if (isdigit(*s)) {
        idValue=atoi(s);
        if (idValue==0) return NULL;
    } else {
        idValue=0;
        if (*s=='\0') return NULL;
    }

    if (useVNum) searchType=FALSE;

    if (searchType) {
        if ((objType=table_find_name(s,item_table))==NO_FLAG)
            return NULL;
    }

    count=1;
    if (isworldlist && obj_list==NULL)
        result=object_list;
    else
        result=obj_list;
    while (result) {
        if (searchType && result->item_type==objType)
            break;
        else
            if (idValue) {
                if (useVNum && result->pIndexData->vnum==idValue && number==count++)
                    break;
                else if (!useVNum && result->id==idValue)
                    break;
            } else
                if (is_name(s,result->name) && number==count++) break;

        // next:
        if (isworldlist)
            result=result->next;
        else
            result=result->next_content;
    }

    return result;
}

// Returns true if object and id matches
bool tcl_isobjid(OBJ_DATA *obj,char *id) {
    OBJ_DATA *old;
    bool result;

    old=obj->next_content;
    obj->next_content=NULL;
    result=tcl_objid(obj,id,FALSE)!=NULL;
    obj->next_content=old;

    return result;
}

// Returns true if object and id matches
bool tcl_ischarid(CHAR_DATA *ch,char *id) {
    CHAR_DATA *old;
    bool result;

    old=ch->next;
    ch->next=NULL;
    result=tcl_charid(NULL,id,ch)!=NULL;
    ch->next=old;

    return result;
}


OBJ_DATA *tcl_objid_here(CHAR_DATA *ch,char *arg) {
    OBJ_DATA *obj=NULL;

    if (ch->in_room) obj=tcl_objid(ch->in_room->contents,arg,FALSE);
    if (obj) return obj;

    obj=tcl_objid(ch->carrying,arg,FALSE);
    if (obj && can_see_obj(ch,obj)) return obj;

    return NULL;
}

/* Pre: tcl_init, tcl_start
 */
int tcl_act(CHAR_DATA *ch,int objc,Tcl_Obj *CONST objv[]) {
    static tclOptionList *acttoOptions[]={"toroom","tonotvict","tovict","tochar","toall",NULL};
    int to,i;
    char *format;
    int minpos;
    int carg1,carg2;
    void *arg1,*arg2;

    if (objc<4) {
        Tcl_WrongNumArgs(interp,0,objv,"act to format ?arg1? arg2 ?minpos?");
        return TCL_ERROR;
    }

    if (Tcl_GetIndexFromObj(interp,objv[1],acttoOptions,"to",0,&to)!=TCL_OK) {
        //resultPtr already contains a much better error.
        //Tcl_SetObjResult(interp,Tcl_NewStringObj("Couldn't convert index from obj.",-1));
        return TCL_ERROR;
    }

    format=Tcl_GetString(objv[2]);

    if (tcl_parse_act(format,&carg1,&carg2)==FALSE) {
        Tcl_SetObjResult(interp,Tcl_NewStringObj("invalid combination or variable in act format string",-1));
        return TCL_ERROR;
    }

    i=1; /* to count number of arguments, arg2 isn't optional */
    if (carg1) i++;

    if (objc<3+i) {
        Tcl_WrongNumArgs(interp,0,objv,"act to format ?arg1? arg2 ?minpos?");
        return TCL_ERROR;
    }

    i=3;  /* index to get argument from */

    arg1=NULL;
    switch (carg1) {
    case 1: /* string */
        arg1=(void *)Tcl_GetString(objv[i]);
        break;
    case 4: /* obj */
    {
        OBJ_DATA *obj;
        if (!(obj=tcl_objid(NULL,Tcl_GetString(objv[i]),TRUE))) {
            Tcl_SetObjResult(interp,Tcl_NewStringObj("act: can't find objid",-1));
            return TCL_ERROR;
        }
        arg1=(void *)obj;
        break;
    }
    }

    if (carg1) i++;

    // We have parsed up to: act to format arg1

    if (objc-i>2 || objc-i<1) {
        Tcl_WrongNumArgs(interp,0,objv,"act to format ?arg1? arg2 ?pos?");
        return TCL_ERROR;
    }

    arg2=NULL;
    if (to==TO_VICT || to==TO_NOTVICT) {
        if (carg2!=0 && carg2!=2) {
            Tcl_SetObjResult(interp,Tcl_NewStringObj("act: with a tovict act the secound argument must be a char",-1));
            return TCL_ERROR;
        }
        carg2=2; // Set to char if not alreay
    }

    switch(carg2) {
    case 1: /* string */
        arg2=(void *)Tcl_GetString(objv[i]);
        break;
    case 2: /* char */
    {
        CHAR_DATA *target;
        if (!(target=tcl_charid_char(ch,Tcl_GetString(objv[i]),NULL))) {
            Tcl_SetObjResult(interp,Tcl_NewStringObj("act: can't find charid",-1));
            return TCL_ERROR;
        }
        arg2=(void *)target;
        break;
    }
    case 4: /* obj */
    {
        OBJ_DATA *obj;
        if (!(obj=tcl_objid(NULL,Tcl_GetString(objv[i]),TRUE))) {
            Tcl_SetObjResult(interp,Tcl_NewStringObj("act: can't find objid",-1));
            return TCL_ERROR;
        }
        arg2=(void *)obj;
        break;
    }
    case 8: /* door */
        arg2=(void *)Tcl_GetString(objv[i]);
        break;
    }

    i++; // Next argument

    minpos=POS_RESTING;
    if (objc-i>0) {
        // min position
        if ((minpos=short_position_lookup(Tcl_GetString(objv[i]))==-1)) {
            Tcl_SetObjResult(interp,Tcl_NewStringObj("act: unknown position parameter",-1));
            return TCL_ERROR;
        }
    }

    act_new(format,ch,arg1,arg2,to,minpos);

    return TCL_OK;
}

/*
 * How many other players / mobs are there in the room
 * iFlag: 0: all, 1: players, 2: mobiles 3: mobs w/ same vnum 4: same group
 */
int tcl_count_people_room( ROOM_INDEX_DATA *room, CHAR_DATA *mob, int iFlag, int vnum )
{
    CHAR_DATA *vch;
    int count;
    for ( count = 0, vch = room->people; vch; vch = vch->next_in_room )
        if ( mob != vch
             &&   (iFlag == 0
                   || (iFlag == 1 && !IS_NPC( vch ))
                   || (iFlag == 2 && IS_NPC( vch ))
                   || (iFlag == 3 && IS_NPC( vch )
                       && vnum == vch->pIndexData->vnum )
                   || (iFlag == 4 && (mob==NULL || is_same_group( mob, vch ))) )
             && (mob==NULL || can_see( mob, vch )) )
            count++;
    return ( count );
}

/*
 * How many other players / mobs are there in the area
 * iFlag: 0: all, 1: players, 2: mobiles 3: mobs w/ same vnum 4: same group
 */
int tcl_count_people_area( AREA_DATA *area, CHAR_DATA *mob, int iFlag, int vnum )
{
    ROOM_INDEX_DATA *room;
    int count=0;
    int vnumroom;

    for (vnumroom=area->lvnum;vnumroom<area->uvnum;vnumroom++)
        if ((room=get_room_index(vnumroom))!=NULL)
            count+=tcl_count_people_room(room,mob,iFlag,vnum);
    return count;
}



// Pre: tcl_start must have been called.
// Returns the character that should execute the command
// Returns NULL if error (no mp_error done yet)
// This function should be called before any other parsing has been done
CHAR_DATA *tcl_parse_char_switches(CHAR_DATA *defch,int *objc,Tcl_Obj *CONST **objv)
{
    Tcl_Obj *CONST *argv=*objv;
    int argc=*objc;
    char *arg;
    int index;
    CHAR_DATA *result=defch;
    int setslot=-1;
    int getslot;

    static tclOptionList *charSwitches[]={
        "-mob","-char","-target","-last","-second","-find","-set","-get",(char *)NULL};

    while (argc!=0) {
        arg=Tcl_GetString(argv[0]);
        if (*arg=='-') {

            if (Tcl_GetIndexFromObj(interp, argv[0], charSwitches, "switch", 0, &index) != TCL_OK) {
                result=NULL;
                goto done;
            }

            argc--;
            argv++;

            switch(index) {
            case 0: /* -mob */
                result=progData->mob;
                break;
            case 1: /* -char */
                result=progData->ch;
                if (!result) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("-char switch used while no char available.",-1));
                    goto done;
                }
                break;
            case 2: /* -target */
                if (progType==PROGTYPE_CHAR
                        && (result=progData->mob->mprog_target)!=NULL)
                    goto done;
                if (progType==PROGTYPE_OBJECT
                        && (result=progData->obj->oprog_target)!=NULL)
                    goto done;
                if (progType==PROGTYPE_ROOM
                        && (result=progData->room->rprog_target)!=NULL)
                    goto done;
                if (progType==PROGTYPE_AREA
                        && (result=progData->area->aprog_target)!=NULL)
                    goto done;
                Tcl_SetObjResult(interp,Tcl_NewStringObj("-target switch used while no target available.",-1));
                goto done;
                break;
            case 3: /* -last */
                result=progData->lastCh;
                if (!result) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("-last switch used while no valid last char available.",-1));
                    goto done;
                }
                break;
            case 4: /* -second */
                result=progData->ch2;
                if (!result) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("-second switch used while no valid second char available.",-1));
                    goto done;
                }
                break;
            case 5: /* -find <charid> */
                if (argc==0) {
                    Tcl_WrongNumArgs(interp,0,argv,"-find charid");
                    result=NULL;
                    goto done;
                }
                if (progData->mob) result=tcl_charid_char(progData->mob,Tcl_GetString(argv[0]),NULL);
                else if (progData->obj) result=tcl_charid_obj(progData->obj,Tcl_GetString(argv[0]),NULL);
                else if (progData->room) result=tcl_charid(progData->room,Tcl_GetString(argv[0]),NULL);
                else result=tcl_charid(NULL,Tcl_GetString(argv[0]),NULL);
                argc--;argv++;
                if (!result) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("-find charid switch used but no char found.",-1));
                    goto done;
                }
                break;
            case 6: /* -set <0-4> */
                if (argc==0) {
                    Tcl_WrongNumArgs(interp,0,argv,"-set <0-4>");
                    result=NULL;
                    goto done;
                }
                if (Tcl_GetIntFromObj(interp,argv[0],&setslot)!=TCL_OK) {
                    result=NULL;
                    goto done;
                }
                argc--;argv++;
                if (setslot<0 || setslot>=PROGDATA_SLOTS) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("char -set: index out of bounds.",-1));
                    result=NULL;
                    setslot=-1;
                    goto done;
                }
                break;
            case 7: /* -get <0-4> */
                if (argc==0) {
                    Tcl_WrongNumArgs(interp,0,argv,"-get <0-4>");
                    result=NULL;
                    goto done;
                }
                if (Tcl_GetIntFromObj(interp,argv[0],&getslot)!=TCL_OK) {
                    result=NULL;
                    goto done;
                }
                argc--;argv++;
                if (getslot<0 || getslot>=PROGDATA_SLOTS) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("char -get: index out of bounds.",-1));
                    result=NULL;
                    goto done;
                }
                result=progData->char_store[getslot];
                break;
            default:
                Tcl_SetObjResult(interp,Tcl_NewStringObj("char switches: you should not get this error.",-1));
                result=NULL;
                goto done;
            }
        } else
            break;
    }

    if (result==NULL && defch==NULL)
        Tcl_SetObjResult(interp,Tcl_NewStringObj("can't find char, no default present",-1));

done:
    *objc=argc;
    *objv=argv;
    progData->lastCh=result;
    if (setslot!=-1) progData->char_store[setslot]=result;
    return result;
}

OBJ_DATA *tcl_parse_obj_switches(OBJ_DATA *defobj,int *objc,Tcl_Obj *CONST **objv) {
    Tcl_Obj *CONST *argv=*objv;
    int argc=*objc;
    char *arg;
    int index;
    OBJ_DATA *result=defobj;
    int getslot;
    int setslot=-1;

    static tclOptionList *objSwitches[]={
        "-obj","-last","-second","-find","-set","-get",(char *)NULL};

    while (argc!=0) {
        arg=Tcl_GetString(argv[0]);
        if (*arg=='-') {

            if (Tcl_GetIndexFromObj(interp, argv[0], objSwitches, "switch", 0, &index) != TCL_OK)
                result=NULL;
            argc--;
            argv++;

            switch (index) {
            case 0: /* -obj */
                result=progData->obj;
                break;
            case 1: /* -last */
                result=progData->lastObj;
                if (!result) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("-last switch used while no valid last object available.",-1));
                    goto done;
                }
                break;
            case 2: /* -second */
                result=progData->obj2;
                if (!result) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("-second switch used while no valid second object available.",-1));
                    goto done;
                }
                break;
            case 3: /* -find <objid> */
                if (argc==0) {
                    Tcl_WrongNumArgs(interp,0,argv,"-find objid");
                    result=NULL;
                    goto done;
                }
                result=tcl_objid(NULL,Tcl_GetString(argv[0]),TRUE);
                argc--;argv++;
                if (!result) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("-find objid switch used but no obj found.",-1));
                    goto done;
                }
                break;
            case 4: /* -set <0-4> */
                if (argc==0) {
                    Tcl_WrongNumArgs(interp,0,argv,"-set <0-4>");
                    result=NULL;
                    goto done;
                }
                if (Tcl_GetIntFromObj(interp,argv[0],&setslot)!=TCL_OK) {
                    result=NULL;
                    goto done;
                }
                argc--;argv++;
                if (setslot<0 || setslot>4) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("char -set: index out of bounds.",-1));
                    setslot=-1;
                    result=NULL;
                    goto done;
                }
                break;
            case 5: /* -get <0-4> */
                if (argc==0) {
                    Tcl_WrongNumArgs(interp,0,argv,"-get <0-4>");
                    result=NULL;
                    goto done;
                }
                if (Tcl_GetIntFromObj(interp,argv[0],&getslot)!=TCL_OK) {
                    result=NULL;
                    goto done;
                }
                argc--;argv++;
                if (getslot<0 || getslot>4) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("char -get: index out of bounds.",-1));
                    result=NULL;
                    goto done;
                }
                result=progData->obj_store[getslot];
                break;
            default:
                Tcl_SetObjResult(interp,Tcl_NewStringObj("obj switches: you should not get this error.",-1));
                result=NULL;
                goto done;
            }
        } else
            break;
    }


done:
    *objc=argc;
    *objv=argv;
    progData->lastObj=result;
    if (setslot!=-1) progData->obj_store[setslot]=result;
    return result;
}


ROOM_INDEX_DATA *tcl_parse_room_switches(ROOM_INDEX_DATA *thisroom,CHAR_DATA **defch,int *objc,Tcl_Obj *CONST **objv)
{
    Tcl_Obj *CONST *argv=*objv;
    int argc=*objc;
    char *arg;
    int index;
    CHAR_DATA *resultCh=*defch;
    ROOM_INDEX_DATA *resultRoom=NULL;

    static tclOptionList *roomSwitches[]={
        "-mob","-char","-target","-last","-second","-find","-vnum",
        "-object",NULL};

    while (argc!=0) {
        arg=Tcl_GetString(argv[0]);
        if (*arg=='-') {

            if (Tcl_GetIndexFromObj(interp, argv[0],roomSwitches, "switch", 0, &index) != TCL_OK) {
                resultCh=NULL;
                goto done;
            }

            argc--;
            argv++;

            switch(index) {
            case 0: /* -mob */
                resultCh=progData->mob;
                break;
            case 1: /* -char */
                resultCh=progData->ch;
                if (!resultCh) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("-char switch used while no char available.",-1));
                    goto done;
                }
                break;
            case 2: /* -target */
                resultCh=progData->mob->mprog_target;
                if (!resultCh) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("-target switch used while no target available.",-1));
                    goto done;
                }
                break;
            case 3: /* -last */
                resultCh=progData->lastRoomCh;
                resultRoom=progData->lastRoom;
                if (!resultRoom) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("-last switch used while no valid last room available.",-1));
                    goto done;
                }
                break;
            case 4: /* -second */
                resultCh=progData->ch2;
                if (!resultCh) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("-second switch used while no valid second char available.",-1));
                    goto done;
                }
                break;
            case 5: /* -find <charid> */
                if (argc==0) {
                    Tcl_WrongNumArgs(interp,0,argv,"-find charid");
                    resultCh=NULL;
                    goto done;
                }
                resultCh=tcl_charid_char(progData->mob,Tcl_GetString(argv[0]),NULL);
                argc--;argv++;
                if (!resultCh) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("-find charid switch used but no char found.",-1));
                    goto done;
                }
                break;
            case 6: /* -vnum <vnum> */
                if (argc==0) {
                    Tcl_WrongNumArgs(interp,0,argv,"-vnum roomvnum");
                    resultCh=NULL;
                    goto done;
                }

            {
                long vnum;
                if (Tcl_GetLongFromObj(interp,argv[0],&vnum)!=TCL_OK) {
                    argc--;argv++;
                    goto done;
                }

                argc--;argv++;

                resultRoom=get_room_index(vnum);
                if (!resultRoom) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("room with vnum not found",-1));
                    goto done;
                }
            }
                break;
            case 7: // -object <objid>
                if (argc==0) {
                    Tcl_WrongNumArgs(interp,0,argv,"-object objectid");
                    resultCh=NULL;
                    goto done;
                }

            {
                OBJ_DATA *obj;

                obj=tcl_objid(NULL,Tcl_GetString(argv[0]),TRUE);
                if (!obj) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("room with object not found",-1));
                    argc--;argv++;
                    goto done;
                }
                argc--;argv++;
                resultRoom=obj->in_room;
            }
                break;

            default:
                Tcl_SetObjResult(interp,Tcl_NewStringObj("room switches: you should not get this error.",-1));
                resultCh=NULL;
                goto done;
            }
        } else
            break;
    }

    if (resultCh==NULL && resultRoom==NULL) {
        if (thisroom!=NULL) {
            resultRoom=thisroom;
        } else {
            Tcl_SetObjResult(interp,Tcl_NewStringObj("no char found for room",-1));
            resultRoom=NULL;
            goto done;
        }
    }

    if (resultRoom==NULL) resultRoom=resultCh->in_room;

    if (resultRoom==NULL) {
        Tcl_SetObjResult(interp,Tcl_NewStringObj("char found, but he/she is not in a room.",-1));
        goto done;
    }

done:
    *objc=argc;
    *objv=argv;
    progData->lastRoom=resultRoom;
    progData->lastRoomCh=resultCh;
    *defch=resultCh;
    return resultRoom;
}

AREA_DATA *tcl_parse_area_switches(AREA_DATA *this_area,int *objc,Tcl_Obj *CONST **objv)
{
    Tcl_Obj *CONST *argv=*objv;
    int argc=*objc;
    char *arg;
    int index;
    CHAR_DATA *ch=NULL;
    AREA_DATA *result=NULL;

    static tclOptionList *areaSwitches[]={
        "-mob","-char","-target","-last","-second","-find","-vnum","-rvnum","-ovnum","-mvnum",NULL};

    if (progData->mob && progData->mob->in_room) result=progData->mob->in_room->area;

    while(argc!=0) {
        arg=Tcl_GetString(argv[0]);
        if (*arg=='-') {
            if (Tcl_GetIndexFromObj(interp, argv[0],areaSwitches, "switch", 0, &index) != TCL_OK) {
                result=NULL;
                goto done;
            }

            argc--;
            argv++;

            switch (index) {
            case 0: /* -mob */
                result=NULL;
                ch=progData->mob;
                if (!ch) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("-mob switch used while no ob available.",-1));
                    goto done;
                }
                break;
            case 1: /* -char */
                result=NULL;
                ch=progData->ch;
                if (!ch) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("-char switch used while no char available.",-1));
                    goto done;
                }
                break;
            case 2: /* -target */
                result=NULL;
                ch=progData->mob->mprog_target;
                if (!ch) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("-target switch used while no target available.",-1));
                    goto done;
                }
                break;
            case 3: /* -last */
                result=progData->lastArea;
                if (!result) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("-last switch used while no valid last area available.",-1));
                    goto done;
                }
                break;
            case 4: /* -second */
                result=NULL;
                ch=progData->ch2;
                if (!ch) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("-second switch used while no valid second char available.",-1));
                    goto done;
                }
                break;
            case 5: /* -find <charid> */
                result=NULL;
                if (argc==0) {
                    Tcl_WrongNumArgs(interp,0,argv,"-find charid");
                    ch=NULL;
                    goto done;
                }
                ch=tcl_charid_char(progData->mob,Tcl_GetString(argv[0]),NULL);
                argc--;argv++;
                if (!ch) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("-find charid switch used but no char found.",-1));
                    goto done;
                }
                break;
            case 6: /* -vnum <vnum> */
                if (argc==0) {
                    Tcl_WrongNumArgs(interp,0,argv,"-vnum area");
                    ch=NULL;
                    goto done;
                }

            {
                long vnum;

                if (Tcl_GetLongFromObj(interp,argv[0],&vnum)!=TCL_OK) {
                    argc--;argv++;
                    goto done;
                }

                argc--;argv++;

                result=get_area_data(vnum);
                if (!result) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("area with vnum not found",-1));
                    goto done;
                }
            }
                break;
                /* these three do exactly the same thing, but should be kept for tcl-code cleanness. */
            case 7: /* -rvnum */
            case 8: /* -ovnum */
            case 9: /* -mvnum */
                if (argc==0) {
                    Tcl_WrongNumArgs(interp,1,argv-1,"vnum");
                    ch=NULL;
                    goto done;
                }

            {
                long vnum;

                if (Tcl_GetLongFromObj(interp,argv[0],&vnum)!=TCL_OK) {
                    argc--;argv++;
                    goto done;
                }

                argc--;argv++;

                result=get_vnum_area(vnum);
                if (!result) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("area containing vnum not found",-1));
                    goto done;
                }
            }
                break;
            default:
                Tcl_SetObjResult(interp,Tcl_NewStringObj("area switches: you should not get this error.",-1));
                result=NULL;
                goto done;
            }
        } else
            break;
    }

    if (result==NULL && ch && ch->in_room) result=ch->in_room->area;

    if (result==NULL && this_area!=NULL) result=this_area;

    if (result==NULL) {
        Tcl_SetObjResult(interp,Tcl_NewStringObj("no area found",-1));
        goto done;
    }

done:
    *objc=argc;
    *objv=argv;
    progData->lastArea=result;
    return result;
}

#ifdef TCL_USE_LIMITS
void tcl_start_timer(Tcl_Time *limit) {
    struct timeval  now;
    Tcl_Time	    endtime;

    if (limit==NULL)
        limit=&tcl_time_limit;

    gettimeofday(&now,NULL);
    endtime.usec=now.tv_usec+limit->usec;
    endtime.sec=now.tv_sec+limit->sec;
    while (endtime.usec>1000000) {
        endtime.usec-=1000000;
        endtime.sec++;
    }
    Tcl_LimitTypeSet(interp,TCL_LIMIT_TIME);
    Tcl_LimitSetTime(interp,&endtime);
}

void tcl_stop_timer(void) {
    Tcl_LimitTypeReset(interp,TCL_LIMIT_TIME);
}

Tcl_CmdInfo Tcl_Orig_TimeObjCmd;

int tclFDtime(ClientData dummy, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[]) {
    int res;
    Tcl_Time time_limit = { 5, 0 };

    // grant timed command plenty of time to execute.
    tcl_start_timer(&time_limit);
    res=Tcl_Orig_TimeObjCmd.objProc(Tcl_Orig_TimeObjCmd.objClientData,interp,objc,objv);
    tcl_start_timer(NULL);

    return res;
}

#else
#define tcl_start_timer(limit)
#define tcl_stop_timer(limit)
#endif

/*
 *Pre: tcl_start must been run before using this code.
 */
int tcl_run_mp_code(CHAR_DATA *ch,char *code)
{
    char buf[256];
    Tcl_Obj *obj;
    int error;

    /* Create namespace and run the code */
    strcpy(current_namespace,namespace_char(ch));
    sprintf(buf,"namespace eval %s {",current_namespace);

    obj=Tcl_NewStringObj(buf,-1);
    Tcl_IncrRefCount(obj);
    Tcl_AppendStringsToObj(obj,code,"}",NULL);

    tcl_progTypePush(PROGTYPE_CHAR);

    tcl_start_timer(NULL);
    error=Tcl_EvalObjEx(interp,obj,TCL_EVAL_DIRECT);
    tcl_stop_timer();

    Tcl_DecrRefCount(obj); /* Delete the object */
    *current_namespace='\0';
    tcl_progTypePop();

    return error;
}


/*
 * Pre: tcl_start must been run before using this code.
 */
int tcl_run_op_code(OBJ_DATA *object,char *code) {
    char buf[256];
    Tcl_Obj *obj;
    int error;

    /* Create namespace and run the code */
    strcpy(current_namespace,namespace_obj(object));
    sprintf(buf,"namespace eval %s {",current_namespace);

    obj=Tcl_NewStringObj(buf,-1);
    Tcl_IncrRefCount(obj);
    Tcl_AppendStringsToObj(obj,code,"}",NULL);

    tcl_progTypePush(PROGTYPE_OBJECT);

    tcl_start_timer(NULL);
    error=Tcl_EvalObjEx(interp,obj,TCL_EVAL_DIRECT);
    tcl_stop_timer();

    Tcl_DecrRefCount(obj); /* Delete the object */
    *current_namespace='\0';
    tcl_progTypePop();

    return error;
}

/*
 * Pre: tcl_start must been run before using this code.
 */
int tcl_run_rp_code(ROOM_DATA *room,char *code) {
    char buf[256];
    Tcl_Obj *obj;
    int error;

    /* Create namespace and run the code */
    strcpy(current_namespace,namespace_room(room));
    sprintf(buf,"namespace eval %s {",current_namespace);

    obj=Tcl_NewStringObj(buf,-1);
    Tcl_IncrRefCount(obj);
    Tcl_AppendStringsToObj(obj,code,"}",NULL);

    tcl_progTypePush(PROGTYPE_ROOM);

    tcl_start_timer(NULL);
    error=Tcl_EvalObjEx(interp,obj,TCL_EVAL_DIRECT);
    tcl_stop_timer();

    Tcl_DecrRefCount(obj); /* Delete the object */
    *current_namespace='\0';
    tcl_progTypePop();

    return error;
}

/*
 * Pre: tcl_start must been run before using this code.
 */
int tcl_run_ap_code(AREA_DATA *area,char *code) {
    char buf[256];
    Tcl_Obj *obj;
    int error;

    /* Create namespace and run the code */
    strcpy(current_namespace,namespace_area(area));
    sprintf(buf,"namespace eval %s {",current_namespace);

    obj=Tcl_NewStringObj(buf,-1);
    Tcl_IncrRefCount(obj);
    Tcl_AppendStringsToObj(obj,code,"}",NULL);

    tcl_progTypePush(PROGTYPE_AREA);

    tcl_start_timer(NULL);
    error=Tcl_EvalObjEx(interp,obj,TCL_EVAL_DIRECT);
    tcl_stop_timer();

    Tcl_DecrRefCount(obj); /* Delete the object */
    *current_namespace='\0';
    tcl_progTypePop();

    return error;
}

char *tcl_firstname(CHAR_DATA *ch) {
    static char fname[MAX_INPUT_LENGTH];

    if (IS_NPC(ch)) one_argument(ch->name,fname);
    else strcpy(fname,ch->name);

    return fname;
}

char *tcl_firstobjname(OBJ_DATA *obj) {
    static char fname[MAX_INPUT_LENGTH];

    one_argument(obj->name,fname);

    return fname;
}


// Returns true if word is a substring of tring s considering
// word boundaries
bool tcl_findword(char *word,char *str)
{
    char *w,*s;  	// word work pointers
    int state;		// state of the search process

    w=word;s=str;
    state=0;

    while(state!=3 && state!=4) {
        switch(state) {
        case 0:
            if (*s=='\0' || *w=='\0') state=4;
            else if (toupper(*s)==toupper(*w)) { w++;state=1; }
            else if (isalnum(*s)) { state=2; }
            break;
        case 1:
            if (*w=='\0' && !isalnum(*s)) state=3;
            else if (*s=='\0') state=4;
            else if (toupper(*w)==toupper(*s)) w++;
            else if (!isalnum(*s)) { w=word;state=0; }
            else { w=word;state=2; }
            break;
        case 2:
            if (*s=='\0') state=4;
            else if (!isalnum(*s)) state=0;
            break;
        }
        s++;
    }

    return state==3;
}


int tcl_remove(CHAR_DATA *ch,int objc,Tcl_Obj *CONST objv[])
{
    int i=1;
    bool delAll=FALSE;
    char *s;
    int index;
    OBJ_DATA *obj;
    static tclOptionList *removeSwitches[]={"-all",NULL};

    while (objc-i>0) {
        s=Tcl_GetString(objv[i]);
        if (*s!='-') break;

        if (Tcl_GetIndexFromObj(interp, objv[i], removeSwitches, "switch", 0, &index) != TCL_OK)
            return TCL_ERROR;

        if (index==0) delAll=TRUE;

        i++;
    }

    if (objc-i>1) {
        Tcl_WrongNumArgs(interp,0,objv,"char/mob remove ?-all? ?objid?");
        return TCL_ERROR;
    }

    if (objc-i>0) {
        s=Tcl_GetString(objv[i]);

        obj=tcl_objid(ch->carrying,s,FALSE);
        while (obj) {
            if (obj->wear_loc!=WEAR_NONE) unequip_char(ch,obj);
            obj_from_char(obj);
            extract_obj(obj);

            if (delAll) obj=tcl_objid(ch->carrying,s,FALSE);
            else obj=NULL;
        }
    } else if (delAll) { // Just delete all objects
        obj=ch->carrying;
        while (obj) {
            if (obj->wear_loc!=WEAR_NONE) unequip_char(ch,obj);
            obj_from_char(obj);
            extract_obj(obj);

            obj=ch->carrying;
        }
    }

    return TCL_OK;
}


/*
 * --------------------------------------------------------------------
 * TCL Commands. The following commands will be added to TCL for use
 * in the mob programs.
 * --------------------------------------------------------------------
 */

void mp_delonetrigger(CHAR_DATA *ch,TCLPROG_LIST *trig)
{
    if (!ch || !ch->mprogs) return;

    if (ch->mprogs==trig) {
        ch->mprogs=trig->next;
        free_mprog_list(trig);
    } else {
        TCLPROG_LIST *t,*l;

        for (t=ch->mprogs->next,l=ch->mprogs;t;l=t,t=t->next) {
            if (trig==t) {
                l->next=t->next;
                free_mprog_list(trig);
                break;
            }
        }
    }
    fixMobTriggerFlags(ch);
}

void op_delonetrigger(OBJ_DATA *obj,TCLPROG_LIST *trig)
{
    if (!obj || !obj->oprogs) return;

    if (obj->oprogs==trig) {
        obj->oprogs=trig->next;
        free_oprog_list(trig);
    } else {
        TCLPROG_LIST *t,*l;

        for (t=obj->oprogs->next,l=obj->oprogs;t;l=t,t=t->next) {
            if (trig==t) {
                l->next=t->next;
                free_oprog_list(trig);
                break;
            }
        }
    }
    fixObjTriggerFlags(obj);
}

void rp_delonetrigger(ROOM_DATA *room,TCLPROG_LIST *trig)
{
    if (!room || !room->rprogs) return;

    if (room->rprogs==trig) {
        room->rprogs=trig->next;
        free_rprog_list(trig);
    } else {
        TCLPROG_LIST *t,*l;

        for (t=room->rprogs->next,l=room->rprogs;t;l=t,t=t->next) {
            if (trig==t) {
                l->next=t->next;
                free_rprog_list(trig);
                break;
            }
        }
    }
    fixRoomTriggerFlags(room);
}

void ap_delonetrigger(AREA_DATA *area,TCLPROG_LIST *trig)
{
    if (!area || !area->aprogs) return;

    if (area->aprogs==trig) {
        area->aprogs=trig->next;
        free_aprog_list(trig);
    } else {
        TCLPROG_LIST *t,*l;

        for (t=area->aprogs->next,l=area->aprogs;t;l=t,t=t->next) {
            if (trig==t) {
                l->next=t->next;
                free_aprog_list(trig);
                break;
            }
        }
    }
    fixAreaTriggerFlags(area);
}

int mp_addtrigger(CHAR_DATA *ch,int objc,Tcl_Obj *CONST objv[])
{
    TCLPROG_LIST *trig=NULL,*item;
    CHAR_DATA *mob=ch;

    if (mob==NULL) {
        Tcl_SetObjResult(interp,Tcl_NewStringObj("No char found for addtrigger.",-1));
        goto error;
    }

    if (objc!=5 && objc!=4) {
        Tcl_WrongNumArgs(interp,1,objv,"name type prog ?trigexpr?");
        goto error;
    }

    trig=new_mprog_list();
    trig->name=str_dup(Tcl_GetString(objv[1]));
    trig->trig_type=table_find_name(Tcl_GetString(objv[2]),mobtrigger_table);
    trig->code=str_dup(Tcl_GetString(objv[3]));
    if (objc==5)
        trig->trig_phrase=str_dup(Tcl_GetString(objv[4]));

    /* Does the trigger exist? */
    if (trig->trig_type==NO_FLAG) {
        Tcl_Obj *result;
        result=Tcl_NewStringObj("",0);
        Tcl_AppendStringsToObj(result,"invalid trigger \"", Tcl_GetString(objv[2]), "\"",NULL);
        Tcl_SetObjResult(interp,result);
        goto error;
    }

    /* Set the result to the trigger name */
    Tcl_SetObjResult(interp,Tcl_NewStringObj(trig->name,-1));

    /* Add the trigger to the end of the list (order is importand) */
    if (mob->mprogs==NULL) {
        mob->mprogs=trig;
    } else {
        for (item=mob->mprogs;item->next;item=item->next);
        item->next=trig;
    }
    fixMobTriggerFlags(mob);

    return TCL_OK;

error:
    if (trig) free_mprog_list(trig);
    return TCL_ERROR;
}

int op_addtrigger(OBJ_DATA *obj,int objc,Tcl_Obj *CONST objv[]) {
    TCLPROG_LIST *trig=NULL,*item;

    if (obj==NULL) {
        Tcl_SetObjResult(interp,Tcl_NewStringObj("No object found for addtrigger.",-1));
        goto error;
    }

    if (objc!=5 && objc!=4) {
        Tcl_WrongNumArgs(interp,1,objv,"name type prog ?trigexpr?");
        goto error;
    }

    trig=new_oprog_list();
    trig->name=str_dup(Tcl_GetString(objv[1]));
    trig->trig_type=table_find_name(Tcl_GetString(objv[2]),objtrigger_table);
    trig->code=str_dup(Tcl_GetString(objv[3]));
    if (objc==5)
        trig->trig_phrase=str_dup(Tcl_GetString(objv[4]));

    /* Does the trigger exist? */
    if (trig->trig_type==NO_FLAG) {
        Tcl_Obj *result;
        result=Tcl_NewStringObj("",0);
        Tcl_AppendStringsToObj(result,"invalid trigger \"", Tcl_GetString(objv[2]), "\"",NULL);
        Tcl_SetObjResult(interp,result);
        goto error;
    }

    /* Set the result to the trigger name */
    Tcl_SetObjResult(interp,Tcl_NewStringObj(trig->name,-1));

    /* Add the trigger to the end of the list (order is importand) */
    if (obj->oprogs==NULL) {
        obj->oprogs=trig;
    } else {
        for (item=obj->oprogs;item->next;item=item->next)
            ;
        item->next=trig;
    }
    fixObjTriggerFlags(obj);

    return TCL_OK;

error:
    if (trig) free_oprog_list(trig);
    return TCL_ERROR;
}

int rp_addtrigger(ROOM_DATA *room,int objc,Tcl_Obj *CONST objv[]) {
    TCLPROG_LIST *trig=NULL,*item;

    if (room==NULL) {
        Tcl_SetObjResult(interp,Tcl_NewStringObj("No room found for roomtrigger.",-1));
        goto error;
    }

    if (objc!=5 && objc!=4) {
        Tcl_WrongNumArgs(interp,1,objv,"name type prog ?trigexpr?");
        goto error;
    }

    trig=new_rprog_list();
    trig->name=str_dup(Tcl_GetString(objv[1]));
    trig->trig_type=table_find_name(Tcl_GetString(objv[2]),roomtrigger_table);
    trig->code=str_dup(Tcl_GetString(objv[3]));
    if (objc==5)
        trig->trig_phrase=str_dup(Tcl_GetString(objv[4]));

    /* Does the trigger exist? */
    if (trig->trig_type==NO_FLAG) {
        Tcl_Obj *result;
        result=Tcl_NewStringObj("",0);
        Tcl_AppendStringsToObj(result,"invalid trigger \"", Tcl_GetString(objv[2]), "\"",NULL);
        Tcl_SetObjResult(interp,result);
        goto error;
    }

    /* Set the result to the trigger name */
    Tcl_SetObjResult(interp,Tcl_NewStringObj(trig->name,-1));

    /* Add the trigger to the end of the list (order is importand) */
    if (room->rprogs==NULL) {
        room->rprogs=trig;
    } else {
        for (item=room->rprogs;item->next;item=item->next)
            ;
        item->next=trig;
    }
    fixRoomTriggerFlags(room);

    return TCL_OK;

error:
    if (trig) free_rprog_list(trig);
    return TCL_ERROR;
}

int ap_addtrigger(AREA_DATA *area,int objc,Tcl_Obj *CONST objv[]) {
    TCLPROG_LIST *trig=NULL,*item;

    if (area==NULL) {
        Tcl_SetObjResult(interp,Tcl_NewStringObj("No area found for areatrigger.",-1));
        goto error;
    }

    if (objc!=5 && objc!=4) {
        Tcl_WrongNumArgs(interp,1,objv,"name type prog ?trigexpr?");
        goto error;
    }

    trig=new_aprog_list();
    trig->name=str_dup(Tcl_GetString(objv[1]));
    trig->trig_type=table_find_name(Tcl_GetString(objv[2]),areatrigger_table);
    trig->code=str_dup(Tcl_GetString(objv[3]));
    if (objc==5)
        trig->trig_phrase=str_dup(Tcl_GetString(objv[4]));

    /* Does the trigger exist? */
    if (trig->trig_type==NO_FLAG) {
        Tcl_Obj *result;
        result=Tcl_NewStringObj("",0);
        Tcl_AppendStringsToObj(result,"invalid trigger \"", Tcl_GetString(objv[2]), "\"",NULL);
        Tcl_SetObjResult(interp,result);
        goto error;
    }

    /* Set the result to the trigger name */
    Tcl_SetObjResult(interp,Tcl_NewStringObj(trig->name,-1));

    /* Add the trigger to the end of the list (order is importand) */
    if (area->aprogs==NULL) {
        area->aprogs=trig;
    } else {
        for (item=area->aprogs;item->next;item=item->next)
            ;
        item->next=trig;
    }
    fixAreaTriggerFlags(area);

    return TCL_OK;

error:
    if (trig) free_aprog_list(trig);
    return TCL_ERROR;
}


int tclAddMobTriggerObjCmd_depricated (client, interp, objc, objv)
ClientData client;			/* Not used. */
Tcl_Interp *interp;			/* Current interpreter. */
int objc;				/* Number of arguments. */
Tcl_Obj *CONST objv[];		/* Argument values. */
{
bugf("TCL: %s: addtrigger is depricated, please use mobtrigger.",current_namespace);
return mp_addtrigger(progData->mob,objc,objv);
}

int tclAddMobTriggerObjCmd (client, interp, objc, objv)
ClientData client;			/* Not used. */
Tcl_Interp *interp;			/* Current interpreter. */
int objc;				/* Number of arguments. */
Tcl_Obj *CONST objv[];		/* Argument values. */
{
return mp_addtrigger(progData->mob,objc,objv);
}

int tclAddObjTriggerObjCmd (client, interp, objc, objv)
ClientData client;                  /* Not used. */
Tcl_Interp *interp;                 /* Current interpreter. */
int objc;                           /* Number of arguments. */
Tcl_Obj *CONST objv[];              /* Argument values. */
{
return op_addtrigger(progData->obj,objc,objv);
}

int tclAddRoomTriggerObjCmd (client, interp, objc, objv)
ClientData client;                  /* Not used. */
Tcl_Interp *interp;                 /* Current interpreter. */
int objc;                           /* Number of arguments. */
Tcl_Obj *CONST objv[];              /* Argument values. */
{
return rp_addtrigger(progData->room,objc,objv);
}

int tclAddAreaTriggerObjCmd (client, interp, objc, objv)
ClientData client;                  /* Not used. */
Tcl_Interp *interp;                 /* Current interpreter. */
int objc;                           /* Number of arguments. */
Tcl_Obj *CONST objv[];              /* Argument values. */
{
return ap_addtrigger(progData->area,objc,objv);
}


/*
 * Initialize the TCL interpeter for use with the mob programs
 */
void tcl_init(void)
{
    Master_interp=Tcl_CreateInterp();
    if (!Master_interp) {
        bugf("Error creating Tcl interpeter.");
        exit(1);
    }
    Tcl_Init(Master_interp);
    interp=Tcl_CreateSlave(Master_interp,"proginterp",1);
    if (!interp) {
        bugf("Error creating safe Tcl interpeter.");
        exit(1);
    }

    Tcl_DeleteCommand(interp,"after");

    Tcl_CreateObjCommand(interp,"addtrigger",tclAddMobTriggerObjCmd_depricated,0,NULL);
    Tcl_CreateObjCommand(interp,"mobtrigger",tclAddMobTriggerObjCmd,0,NULL);
    Tcl_CreateObjCommand(interp,"objtrigger",tclAddObjTriggerObjCmd,0,NULL);
    Tcl_CreateObjCommand(interp,"roomtrigger",tclAddRoomTriggerObjCmd,0,NULL);
    Tcl_CreateObjCommand(interp,"areatrigger",tclAddAreaTriggerObjCmd,0,NULL);
    Tcl_CreateObjCommand(interp,"!",tclInterpretObjCmd,0,NULL);
    Tcl_CreateObjCommand(interp,"char",tclCharObjCmd,0,NULL);
    Tcl_CreateObjCommand(interp,"mob",tclMobObjCmd,0,NULL);
    Tcl_CreateObjCommand(interp,"trig",tclTrigObjCmd,0,NULL);
    Tcl_CreateObjCommand(interp,"room",tclRoomObjCmd,0,NULL);
    Tcl_CreateObjCommand(interp,"area",tclAreaObjCmd,0,NULL);
    Tcl_CreateObjCommand(interp,"mud",tclMudObjCmd,0,NULL);
    Tcl_CreateObjCommand(interp,"obj",tclObjObjCmd,0,NULL);
    Tcl_CreateObjCommand(interp,"data",tclDataObjCmd,0,NULL);
    Tcl_CreateObjCommand(interp,"random",tclRandomObjCmd,0,NULL);
    Tcl_CreateObjCommand(interp,"strformat",tclFormatObjCmd,0,NULL);
    Tcl_CreateObjCommand(interp,"clan",tclClanObjCmd,0,NULL);
    Tcl_CreateObjCommand(interp,"directionname",tclDirectionNameObjCmd,0,NULL);
    Tcl_CreateObjCommand(interp,"mud::progtype",tclProgTypeObjCmd,0,NULL);
    Tcl_CreateObjCommand(interp,"debug",tclDebugObjCmd,0,NULL);
    Tcl_CreateObjCommand(interp,"strchangecost",tclStringChangeCost,0,NULL);

#ifdef TCL_USE_LIMITS
    Tcl_GetCommandInfo(interp,"time",&Tcl_Orig_TimeObjCmd);
    Tcl_CreateObjCommand(interp,"time",tclFDtime,0,NULL);
#endif

    {
        char romtcl[MSL];

        sprintf(romtcl,"%s/rom.tcl",mud_data.config_dir);

        if (Tcl_EvalFile(interp,romtcl)==TCL_ERROR) {
            bugf("%s",Tcl_GetStringResult(interp));
            exit(1);
        }
    }

    progDataStackPos=0;
    progData=NULL;
}


Tcl_Obj *Effect_to_ListObj(EFFECT_DATA *eff) {
    Tcl_Obj *result;
    const OPTION_TYPE *options=NULL;
    result=Tcl_NewListObj(0,NULL);

    //where
    Tcl_ListObjAppendElement(interp,result,Tcl_NewStringObj(table_find_value(eff->where,effect_where_table),-1));
    //type
    Tcl_ListObjAppendElement(interp,result,Tcl_NewStringObj(skill_name(eff->type,NULL),-1));
    //level
    Tcl_ListObjAppendElement(interp,result,Tcl_NewIntObj(eff->level));
    //duration
    Tcl_ListObjAppendElement(interp,result,Tcl_NewIntObj(eff->duration));
    //location
    Tcl_ListObjAppendElement(interp,result,Tcl_NewStringObj(option_find_value(eff->location,apply_options),-1));
    //mod
    Tcl_ListObjAppendElement(interp,result,Tcl_NewIntObj(eff->modifier));
    //vector
    switch (eff->where) {
    case TO_SKILLS      : Tcl_ListObjAppendElement(interp,result,Tcl_NewStringObj(skill_name(eff->bitvector,NULL),-1));
        return result;
    case TO_EFFECTS     : options=effect_options;       break;
    case TO_OBJECT      : options=extra_options;        break;
    case TO_IMMUNE      : options=imm_options;          break;
    case TO_RESIST      : options=res_options;          break;
    case TO_VULN        : options=vuln_options;         break;
    case TO_WEAPON      : options=weaponflag_options;   break;
    case TO_EFFECTS2    : options=effect2_options;      break;
    case TO_OBJECT2     : options=extra2_options;       break;
    }
    if (options)
        Tcl_ListObjAppendElement(interp,result,Tcl_NewStringObj(option_find_value(eff->bitvector,options),-1));
    return result;
}
