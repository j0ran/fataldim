//
// $Id: olc_hedit.c,v 1.8 2008/05/11 20:50:47 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"
#include "olc.h"
#include "interp.h"
#include "db.h"

bool hedit_show( CHAR_DATA *ch, char *argument )
{
    HELP_DATA *pHelp;

    EDIT_HELP(ch, pHelp);

    sprintf_to_char(ch, "{yVnum:    {x [%8p]\n\r"
			"{yArea:    {x [%5d] %s\n\r"
			"{yLevel:   {x [%d] %s\n\r"
			"{yKeywords:{x %s\n\r"
			"{yText:    {x\n\r",
	pHelp,
	!pHelp->area ? -1 : pHelp->area->vnum,
	!pHelp->area ? "No Area" : pHelp->area->name,
	pHelp->level, pHelp->deleted?"{Rdeleted{x":"",
	pHelp->keyword );

    if(pHelp->text[0]!='\0')
	send_to_char( pHelp->text, ch );
    else
	send_to_char( "Empty help.\n\r", ch );

    return FALSE;
}

bool hedit_create( CHAR_DATA *ch, char *argument )
{
    HELP_DATA *pHelp;
    AREA_DATA *pArea;
    extern HELP_DATA *help_last;

    /* OLC 1.1b */
    if ( argument[0] == '\0')
    {
	send_to_char( "Syntax: create [keywords]\n\r",ch );
	return FALSE;
    }

    pArea = get_help_area( ch );

    if ( !IS_BUILDER( ch, pArea ) )
    {
	send_to_char( "MPEdit:  You are editing in an area you cannot build in.\n\r", ch );
	return FALSE;
    }

    /* This way we can't remove the helps.. Need to add more code for that */
    pHelp		= new_help();
    pHelp->area			= pArea;
    pHelp->keyword		= str_dup(argument);

    if ( help_first == NULL ) help_first = pHelp;
    if ( help_last != NULL ) help_last->next = pHelp;

    help_last                   = pHelp;
    pHelp->next			= NULL;

    ch->desc->pEdit		= (void *)pHelp;

    send_to_char( "Help Created.\n\r", ch );

    return TRUE;
}


bool hedit_append( CHAR_DATA *ch, char *argument )
{
    HELP_DATA *pHelp;

    EDIT_HELP(ch, pHelp);

    if ( argument[0] == '\0' )
    {
	editor_start( ch, &pHelp->text );
	return TRUE;
    }

    send_to_char( "Syntax:  append\n\r", ch );
    return FALSE;
}

bool hedit_delete( CHAR_DATA *ch, char *argument ) {
    HELP_DATA *pHelp;

    EDIT_HELP(ch, pHelp);

    pHelp->deleted=!pHelp->deleted;
    sprintf_to_char(ch,"This entry is %s deleted.\n\r",
	pHelp->deleted?"now":"not any more");

   return TRUE;
}

bool hedit_clear( CHAR_DATA *ch, char *argument )
{
    HELP_DATA *pHelp;

    EDIT_HELP(ch, pHelp);

    if ( argument[0] == '\0' )
    {
	free_string( pHelp->text );
	pHelp->text = &str_empty[0];

	return TRUE;
    }

    send_to_char( "Syntax:  clear\n\r", ch );
    return FALSE;
}

bool hedit_keyword( CHAR_DATA *ch, char *argument )
{
    HELP_DATA *pHelp;

    EDIT_HELP(ch, pHelp);

    if ( argument[0] == '\0' )
    {
	send_to_char("An empty keyword is not allowed.\n\r",ch);
	return FALSE;
    }

    if(get_help_data(pHelp->area,argument))
    {
    	send_to_char("Duplicate keywords in one area are not allowed.\n\r",ch);
    	return FALSE;
    }

    free_string( pHelp->keyword );
    pHelp->keyword=str_dup(argument);

    send_to_char( "Keyword set.\n\r", ch );
    return TRUE;
}

bool hedit_hlist( CHAR_DATA *ch, char *argument )
{
    HELP_DATA		*pHelp;
    AREA_DATA		*pArea;
    char		buf  [ MAX_STRING_LENGTH   ];
    char		buf1 [ MAX_STRING_LENGTH*2 ];
    char		arg  [ MAX_INPUT_LENGTH    ];
    bool fAll, found;
    int  col = 0;

    one_argument( argument, arg );
    if ( arg[0] == '\0' )
    {
	send_to_char( "Syntax:  hlist <all/prefix>\n\r", ch );
	return FALSE;
    }

    pArea = get_help_area(ch);

    buf1[0] = '\0';
    fAll    = !str_cmp( arg, "all" );
    found   = FALSE;

    for ( pHelp = help_first; pHelp ; pHelp=pHelp->next )
    {
	if ( pHelp->area==pArea && (fAll || is_name( arg, pHelp->keyword )) )
	{
	    found = TRUE;
	    sprintf( buf, "[%8p] %-15.14s",
            (void*)pHelp, pHelp->keyword );
	    strcat( buf1, buf );
	    if ( ++col % 3 == 0 ) strcat( buf1, "\n\r" );
	}
    }

    if ( !found )
    {
	send_to_char( "No help found in this area.\n\r", ch);
	return FALSE;
    }

    if ( col % 3 != 0 )
	strcat( buf1, "\n\r" );

    send_to_char( buf1, ch );
    return FALSE;
}

bool hedit_level( CHAR_DATA *ch, char *argument )
{
    HELP_DATA *pHelp;

    EDIT_HELP(ch, pHelp);

    if ( argument[0] == '\0' || !is_number( argument ) )
    {
	send_to_char( "Syntax:  levelh [number]\n\r", ch );
	return FALSE;
    }

    pHelp->level = atoi( argument );

    send_to_char( "Level set.\n\r", ch);
    return TRUE;
}
