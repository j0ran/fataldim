//
// $Id: fd_screen.c,v 1.17 2003/04/11 18:35:48 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"
#include "fd_screen.h"

#define SCREEN_COLUMNS 79
#define SCREEN_LINES 20

static char screen_text[SCREEN_LINES][SCREEN_COLUMNS];
static char screen_color[SCREEN_LINES][SCREEN_COLUMNS];

void screen_clear(void) {
    memset(screen_text,' ',SCREEN_LINES*SCREEN_COLUMNS*sizeof(char));
    memset(screen_color,'x',SCREEN_LINES*SCREEN_COLUMNS*sizeof(char));
}

void screen_print(int x,int y,char color,int len,bool pad,char *text)
{
  /* Are the coordinates in range */
  if(x<0 || x>=SCREEN_COLUMNS) return;
  if(y<0 || y>=SCREEN_LINES) return;
  /* Is there a len given? If not calculate it */
  if(len<=0) len=strlen(text);
  /* Calculate the number of chars that can be printed */
  len=UMIN(len,SCREEN_COLUMNS-x);
  if(len<=0) return;

  /* Now the printing starts */
  while(len--)
  {
    if(*text) screen_text[y][x]=*(text++);
    else if(pad) screen_text[y][x]=' ';
    screen_color[y][x]=color;
    x++;
  }
}

void screen_to_char(CHAR_DATA *ch)
{
  int x,y,pos;
  char line[MAX_STRING_LENGTH];
  char buf[2*MAX_STRING_LENGTH];
  char color;
  /* Go line by line */

  if(STR_IS_SET(ch->strbit_act,PLR_COLOUR)) send_to_char("\033[2J\033[0;0H",ch);

  color='x';
  buf[0]='\0';
  for(y=0;y<SCREEN_LINES;y++)
  {
    pos=0;
    for(x=0;x<SCREEN_COLUMNS;x++)
    {
      if(screen_color[y][x]!=color && screen_text[y][x]!=' ')
      {
        line[pos++]='{';
        line[pos++]=screen_color[y][x];
        color=screen_color[y][x];
      }
      line[pos++]=screen_text[y][x];
      if(screen_text[y][x]=='{') line[pos++]='{';
    }

    /* Search back to first non space and put a \0 there */
    line[pos]=' ';
    while(pos)
    {
      if(line[pos]!=' ') break;
      pos--;
    }
    if(line[pos]!=' ') pos++;
    line[pos++]='\n';
    line[pos++]='\r';
    line[pos]='\0';

    strcat(buf,line);
  }

  strcat(buf,"{x");
  send_to_char(buf,ch);
}


void screen_specialize(CHAR_DATA *ch, char *message)
{
  int i;

  if(IS_NPC(ch)) return;

  screen_clear();
  screen_print(0,0,'x',0,FALSE," __________  ___________ ____________");
  screen_print(0,1,'x',0,FALSE,"/ Creation \\/ Customize / Specialize \\");
  screen_print(26,1,'w',0,FALSE,"Specialize");
  screen_print(0,2,'x',0,FALSE,"------------------------              -----------------------------------------");
  screen_print(0,3,'y',0,FALSE,"Character:");
  screen_print(11,3,'x',14,FALSE,ch->name);
  i=ch->pcdata->points;
  if(ch->gen_data) i+=ch->gen_data->points_chosen;
  screen_print(26,3,'y',0,FALSE,"Creation Points:");
  screen_print(43,3,'x',9,FALSE,itoa(i));
  screen_print(53,3,'y',0,FALSE,"XP per level:");
  screen_print(67,3,'x',0,FALSE,itoa(exp_per_level(ch,i)));

  screen_print(5,9,'y',0,FALSE,"This screen is not functional yet.");

  screen_to_char(ch);
  if(!message) message="";
  sprintf_to_char(ch,"Commands: creation, customize, done, help.\n\r{y%s{xEnter Command: ",message);
}

void screen_customize( CHAR_DATA *ch,char *message )
{
  char s[100];

  int i,j,k;
  int page,pages;

  if(ch->gen_data) page=ch->gen_data->page;
  else page=1;

  if(IS_NPC(ch)) return;

  screen_clear();
  screen_print(0,0,'x',0,FALSE," __________ __________  ____________");
  screen_print(0,1,'x',0,FALSE,"/ Creation / Customize \\ Specialize \\");
  screen_print(13,1,'w',0,FALSE,"Customize");
  screen_print(0,2,'x',0,FALSE,"-----------             ------------------------------------------------------");
  screen_print(0,3,'y',0,FALSE,"Character:");
  screen_print(11,3,'x',14,FALSE,ch->name);
  i=ch->pcdata->points;
  if(ch->gen_data) i+=ch->gen_data->points_chosen;
  screen_print(26,3,'y',0,FALSE,"Creation Points:");
  screen_print(43,3,'x',9,FALSE,itoa(i));
  screen_print(53,3,'y',0,FALSE,"XP per level:");
  screen_print(67,3,'x',0,FALSE,itoa(exp_per_level(ch,i)));

  /* Determine the number of pages */

  for(i=0,j=0;i<MAX_GROUP;i++)
  {
    if(!group_exists(i)) break;
    if(!may_choose_group(ch,i)) continue;
    j++;
  }
  pages=((j-1)/11)+1;

  for(i=0,j=0;i<MAX_SKILL;i++)
  {
    if(!skill_exists(i)) break;
    if(!may_choose_skill(ch,i)) continue;
    j++;
  }
  j=((j-1)/22)+1;
  pages=UMAX(pages,j);
  if(ch->gen_data) ch->gen_data->pages=pages;

  /* Print selected page */

  screen_print(7,5,'y',0,FALSE,"Groups");
  for(i=0,j=0,k=0;i<MAX_GROUP;i++)
  {
    if(!group_exists(i)) break;
    if(!may_choose_group(ch,i)) continue;

    if((k/11)+1==page)
    {
      sprintf(s,"[ ] %2d %s",
        group_rating(i,ch),
        group_name(i,ch));
      screen_print(0,7+j,'x',25,FALSE,s);
      if(ch->pcdata->group_known[i])
        screen_print(1,7+j,'g',1,FALSE,"*");
      else if(ch->gen_data)
      {
        if(ch->gen_data->group_chosen[i])
          screen_print(1,7+j,'g',1,FALSE,"X");
        else if(ch->gen_data->auto_group[i])
          screen_print(1,7+j,'g',1,FALSE,"x");
      }
      j++;
    }
    k++;
  }

  screen_print(33,5,'y',0,FALSE,"Skills");
  screen_print(59,5,'y',0,FALSE,"Skills");
  for(i=0,j=0,k=0;i<MAX_SKILL;i++)
  {
    if(!skill_exists(i)) break;
    if(!may_choose_skill(ch,i)) continue;

    if((k/22)+1==page)
    {
      sprintf(s,"[ ] %2d %s",
        skill_rating(i,ch),
        skill_name(i,ch));
      screen_print(26*((j%2)+1),7+(j/2),'x',25,FALSE,s);
      if(has_gained_skill(ch,i))
        screen_print(26*((j%2)+1)+1,7+(j/2),'g',1,FALSE,"*");
      else if(ch->gen_data)
      {
        if(ch->gen_data->skill_chosen[i])
          screen_print(26*((j%2)+1)+1,7+(j/2),'g',1,FALSE,"X");
        else if(ch->gen_data->auto_skill[i])
          screen_print(26*((j%2)+1)+1,7+(j/2),'g',1,FALSE,"x");
      }
      j++;
    }
    k++;
  }

  if(pages>page)
  {
    screen_print(7,18,'b',0,FALSE,"** down **");
    screen_print(33,18,'b',0,FALSE,"** down **");
    screen_print(59,18,'b',0,FALSE,"** down **");
  }

  if(page!=1)
  {
    screen_print(7,6,'b',0,FALSE,"*** up ***");
    screen_print(33,6,'b',0,FALSE,"*** up ***");
    screen_print(59,6,'b',0,FALSE,"*** up ***");
  }

  screen_to_char(ch);
  if(!message) message="";
  sprintf_to_char(ch,"Commands: creation, specialize, up, down, reset, add, drop, info, done, help.\n\r{y%s{xEnter Command: ",message);
}

void screen_creation( CHAR_DATA *ch, char *message )
{
  char s[100];

  int i,j;
  int base_class;

  if(IS_NPC(ch)) return;

  screen_clear();
  screen_print(0,0,'x',0,FALSE," __________ ___________  ____________");
  screen_print(0,1,'x',0,FALSE,"/ Creation \\ Customize \\/ Specialize \\");
  screen_print(2,1,'w',0,FALSE,"Creation");
  screen_print(0,2,'x',0,FALSE,"            -------------------------------------------------------------------");
  screen_print(0,3,'y',0,FALSE,"Character:");
  screen_print(11,3,'x',14,FALSE,ch->name);
  screen_print(26,3,'y',0,FALSE,"Creation Points:");
  i=ch->pcdata->points;
  if(ch->gen_data) i+=ch->gen_data->points_chosen;
  screen_print(43,3,'x',9,FALSE,itoa(i));
  screen_print(53,3,'y',0,FALSE,"XP per level:");
  screen_print(67,3,'x',0,FALSE,itoa(exp_per_level(ch,i)));

  screen_print(5,5,'y',0,FALSE,"Race");

  for(i=1,j=0;i<MAX_PC_RACE;i++)
  {
    sprintf(s,"[ ] %s",pc_race_table[i].name);
    screen_print(1,6+j,'x',0,FALSE,s);
    if(i==ch->race) screen_print(2,6+j,'g',1,FALSE,"X");
    j++;
  }

  base_class=ch->class;
  while(class_table[base_class].spec_off>=0)
    base_class=class_table[base_class].spec_off;

  screen_print(19,5,'y',0,FALSE,"Base Class");

  for(i=0,j=0;i<MAX_CLASS;i++)
  {
    if(class_table[i].spec_off>=0) continue;
    sprintf(s,"[ ] %s",class_table[i].name);
    screen_print(15,6+j,'x',0,FALSE,s);
    if(i==base_class) screen_print(16,6+j,'g',1,FALSE,"X");
    j++;
  }

  screen_print(35,5,'y',0,FALSE,"Sex");
  screen_print(31,6,'x',0,FALSE,"[ ] male");
  screen_print(31,7,'x',0,FALSE,"[ ] female");
  screen_print(32,5+show_sex(ch),'g',1,FALSE,"X");

  screen_print(35,9,'y',0,FALSE,"Alignment");
  screen_print(31,10,'x',0,FALSE,"[ ] good");
  screen_print(31,11,'x',0,FALSE,"[ ] neutral");
  screen_print(31,12,'x',0,FALSE,"[ ] evil");
  if(ch->alignment<=-350) i=2; else if(ch->alignment>=350) i=0; else i=1;
  screen_print(32,10+i,'g',1,FALSE,"X");

  screen_print(51,5,'y',0,FALSE,"Weapons");
  for(i=0,j=0;weapon_table[i].name;i++)
  {
    if(skill_rating(*weapon_table[i].gsn,ch)<=0)
      continue;

    sprintf(s,"[ ] %s",weapon_table[i].name);
    screen_print(47,6+j,'x',0,FALSE,s);
    if(ch->gen_data && ch->gen_data->weapon==i)
      screen_print(48,6+j,'g',1,FALSE,"X");
    j++;
  }

  screen_print(68,5,'y',0,FALSE,"Options");
  screen_print(64,6,'x',0,FALSE,"[ ] Ansi Color");
  if(STR_IS_SET(ch->strbit_act,PLR_COLOUR)) screen_print(65,6,'g',1,FALSE,"X");

  screen_to_char(ch);
  if(!message) message="";
  sprintf_to_char(ch,"Commands: customize, specialize, human, elf, ..., done, help.\n\r{y%s{xEnter Command: ",message);
}


void do_test( CHAR_DATA *ch, char *argument )
{
  screen_customize( ch,NULL);
}

