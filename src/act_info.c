/* $Id: act_info.c,v 1.257 2008/04/15 17:43:18 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

/*
 * 19971214 EG Modified do_consider() to work without arguments when fighting
 * 19980106 EG	Modified do_time() to support multiple timezones
 * 19980106 EG	Modified do_score() to prevent crashing it on mobiles
 * 19980316 EG	Modified lore() to stop waiting for imm's
 * 19980316 EG  Modified look() to refuse to be looked in containers
        with level higher than players.
 * 19980318 EG	Modified do_consider() to look at the health of the victim
 * 19980404 EG	Modified show_char_to_char_1() to display mv-points
 */

#include "merc.h"
#include "interp.h"
#include "color.h"
#include "fd_screen.h"
#include "time.h"
#include "db.h"
#include "fd_mxp.h"

char *	const	where_name	[] =
{
    "<used as light>     ",
    "<worn on finger>    ",
    "<worn on finger>    ",
    "<worn around neck>  ",
    "<worn around neck>  ",
    "<worn on body>      ",
    "<worn on head>      ",
    "<worn on legs>      ",
    "<worn on feet>      ",
    "<worn on hands>     ",
    "<worn on arms>      ",
    "<worn as shield>    ",
    "<worn about body>   ",
    "<worn about waist>  ",
    "<worn around wrist> ",
    "<worn around wrist> ",
    "<wielded>           ",
    "<held>              ",
    "<floating nearby>   ",
    "<secondary weapon>  "
};


/* for keeping track of the player count */
int max_on = 0;

/*
 * Local functions.
 */
char *	format_obj_to_char	(OBJ_DATA *obj, CHAR_DATA *ch, bool fShort);
void	show_list_to_char	(OBJ_DATA *list, CHAR_DATA *ch,
                             char *prefix, bool fShort, bool fShowNothing);
void	show_char_to_char_0	(CHAR_DATA *victim, CHAR_DATA *ch);
void	show_char_to_char_1	(CHAR_DATA *victim, CHAR_DATA *ch);
void	show_char_to_char	(CHAR_DATA *list, CHAR_DATA *ch);

void    lore( CHAR_DATA *, OBJ_DATA *, bool );

int	compare			(CHAR_DATA *, char *,OBJ_DATA *);



char *format_obj_to_char( OBJ_DATA *obj, CHAR_DATA *ch, bool fShort )
{
    static char buf[MAX_STRING_LENGTH];

    buf[0] = '\0';

    if (IS_AFFECTED(ch,EFF_HALLUCINATION))
        obj=hallucination_obj();

    if ((fShort && (obj->short_descr == NULL || obj->short_descr[0] == '\0'))
            ||  (obj->description == NULL || obj->description[0] == '\0'))
        return buf;

    if (IS_PC(ch) && STR_IS_SET(ch->strbit_act,PLR_HOLYLIGHT_PLUSPLUS))
        sprintf(buf,"[%d] ",obj->pIndexData->vnum);

    if ( STR_IS_SET(ch->strbit_act,PLR_HOLYLIGHT)
         && IS_OBJ_STAT(obj, ITEM_NOSHOW ))   strcat( buf, "(NoShow) "        );
    if ( STR_IS_SET(ch->strbit_act,PLR_HOLYLIGHT)
         && IS_OBJ_STAT(obj, ITEM_VIS_DEATH )) strcat( buf, "(VisDeath) "    );
    if ( IS_OBJ_STAT(obj, ITEM_INVIS)     )   strcat( buf, "(Invis) "         );
    if ( IS_AFFECTED(ch, EFF_DETECT_EVIL)
         && IS_OBJ_STAT(obj, ITEM_EVIL)   )   strcat( buf, "({rRed Aura{x) "  );
    if ((IS_OBJ_STAT(obj,ITEM_NODROP) || IS_OBJ_STAT(obj,ITEM_NOREMOVE))
            &&  IS_AFFECTED(ch, EFF_DETECT_CURSE))    strcat(buf,"({wCursed{x) "      );
    if (IS_AFFECTED(ch, EFF_DETECT_GOOD)
            &&  IS_OBJ_STAT(obj,ITEM_BLESS))	      strcat(buf,"({bBlue Aura{x) "   );
    if ( IS_AFFECTED(ch, EFF_DETECT_MAGIC)
         && IS_OBJ_STAT(obj, ITEM_MAGIC)  )   strcat( buf, "({wMagical{x) "   );
    if ( IS_OBJ_STAT(obj, ITEM_GLOW)      )   strcat( buf, "({yGlowing{x) "   );
    if ( IS_OBJ_STAT(obj, ITEM_HUM)       )   strcat( buf, "({gHumming{x) "   );
    if ( IS_OBJ_STAT(obj, ITEM_INSTALLED) )   strcat( buf, "({CClan Banner{x) "   );

    if (HAS_MXP(ch)) {
        char commands[MSL],hints[MSL];
        char s[MSL],objname[MSL],*ps;

        //
        //      get x     x
        //     drop   x
        //  examine x x x
        //     look x x x
        //     wear   x
        //   remove     x
        //      eat   x
        //    drink x x
        //     fill   x
        //    quaff   x
        //   donate x
        //      zap     x
        //   recite   x
        // brandish     x
        //          ^ ^ ^ ^
        //  in room | | | |
        //  carried __| | |
        //     worn ____| |
        //  in cont ______|
        //

        commands[0]=0;
        hints[0]=0;

        strcpy(objname,obj->name);
        if ((ps=strchr(objname,' '))!=NULL) ps[0]=0;

        //
        // In this room
        //
        if (obj->carried_by==NULL && obj->in_obj==NULL ) {
            if (obj->item_type==ITEM_FOUNTAIN) {
                sprintf(s,"drink %s|",objname);strcat(commands,s);
                sprintf(s,"drink from %s|",obj->short_descr);strcat(hints,s);
            }

            if (IS_SET(obj->wear_flags,ITEM_TAKE)) {
                sprintf(s,"get %s|",objname);strcat(commands,s);
                sprintf(s,"get %s|",obj->short_descr);strcat(hints,s);
            }

            sprintf(s,"donate %s|",objname);strcat(commands,s);
            sprintf(s,"donate %s|",obj->short_descr);strcat(hints,s);

            sprintf(s,"examine %s|",objname);strcat(commands,s);
            sprintf(s,"examine %s|",obj->short_descr);strcat(hints,s);

            sprintf(s,"look %s",objname);strcat(commands,s);
            sprintf(s,"look at %s",obj->short_descr);strcat(hints,s);
        }

        //
        // Carried by player, inside container
        //
        if (obj->carried_by==NULL && obj->in_obj!=NULL) {
            char t[MSL];
            strcpy(t,obj->in_obj->name);
            if ((ps=strchr(t,' '))!=NULL) ps[0]=0;

            sprintf(s,"get %s %s",objname,t);strcat(commands,s);
            sprintf(s,"get %s from %s",obj->short_descr,obj->in_obj->short_descr);strcat(hints,s);
            //	    sprintf(buf,MXP_SECURE"<Item col=\"red\">blaat duh</Item><ItemInContainer objname=\"%s\" contname=\"%s\" container=\"%s\" object=\"%s\">%s</ItemInContainer>\n\r",objname,t,obj->short_descr,obj->in_obj->short_descr,obj->short_descr);
            //	    return buf;
        }

        //
        // Carried by player, but not worn
        //
        if (obj->carried_by!=NULL && obj->wear_loc==WEAR_NONE && obj->in_obj==NULL) {
            if (obj->item_type==ITEM_DRINK_CON) {
                sprintf(s,"drink %s|",objname);strcat(commands,s);
                sprintf(s,"drink from %s|",obj->short_descr);strcat(hints,s);
                sprintf(s,"fill %s|",objname);strcat(commands,s);
                sprintf(s,"fill %s|",obj->short_descr);strcat(hints,s);
            }

            if (obj->item_type==ITEM_FOOD ||
                    obj->item_type==ITEM_PILL) {
                sprintf(s,"eat %s|",objname);strcat(commands,s);
                sprintf(s,"eat %s|",obj->short_descr);strcat(hints,s);
            }

            if (obj->item_type==ITEM_SCROLL) {
                sprintf(s,"recite %s|",objname);strcat(commands,s);
                sprintf(s,"recite %s|",obj->short_descr);strcat(hints,s);
            }

            if (obj->item_type==ITEM_POTION) {
                sprintf(s,"quaff %s|",objname);strcat(commands,s);
                sprintf(s,"quaff %s|",obj->short_descr);strcat(hints,s);
            }

            if (obj->item_type==ITEM_CONTAINER) {
                sprintf(s,"look in %s|",objname);strcat(commands,s);
                sprintf(s,"look in %s|",obj->short_descr);strcat(hints,s);
            }

            sprintf(s,"drop %s|",objname);strcat(commands,s);
            sprintf(s,"drop %s|",obj->short_descr);strcat(hints,s);

            sprintf(s,"examine %s|",objname);strcat(commands,s);
            sprintf(s,"examine %s|",obj->short_descr);strcat(hints,s);

            sprintf(s,"look %s|",objname);strcat(commands,s);
            sprintf(s,"look at %s|",obj->short_descr);strcat(hints,s);

            sprintf(s,"wear %s",objname);strcat(commands,s);
            sprintf(s,"wear %s",obj->short_descr);strcat(hints,s);
        }

        //
        // Carried by player, worn
        //
        if (obj->carried_by!=NULL && obj->wear_loc!=WEAR_NONE) {
            if (obj->item_type==ITEM_WAND) {
                sprintf(s,"zap %s|",objname);strcat(commands,s);
                sprintf(s,"zap %s|",obj->short_descr);strcat(hints,s);
            }

            if (obj->item_type==ITEM_STAFF) {
                sprintf(s,"brandish %s|",objname);strcat(commands,s);
                sprintf(s,"brandish %s|",obj->short_descr);strcat(hints,s);
            }

            if (obj->item_type==ITEM_WAND) {
                sprintf(s,"zap %s|",objname);strcat(commands,s);
                sprintf(s,"zap %s|",obj->short_descr);strcat(hints,s);
            }

            sprintf(s,"examine %s|",objname);strcat(commands,s);
            sprintf(s,"examine %s|",obj->short_descr);strcat(hints,s);

            sprintf(s,"look %s|",objname);strcat(commands,s);
            sprintf(s,"look at %s|",obj->short_descr);strcat(hints,s);

            sprintf(s,"remove %s",objname);strcat(commands,s);
            sprintf(s,"remove %s",obj->short_descr);strcat(hints,s);
        }

        strcat(buf,MXP"<send \"");
        strcat(buf,commands);
        strcat(buf,"\" hint=\"");
        strcat(buf,hints);
        strcat(buf,"\">");
        if ( fShort )
            strcat( buf, obj->short_descr );
        else
            strcat( buf, obj->description );
        strcat(buf,"</send>");
    } else {
        if ( fShort )
            strcat( buf, obj->short_descr );
        else
            strcat( buf, obj->description );
    }

    return buf;
}



/*
 * Show a list to a character.
 * Can coalesce duplicated items.
 */
void show_list_to_char( OBJ_DATA *list, CHAR_DATA *ch, char *prefix, bool fShort, bool fShowNothing )
{
    char buf[MAX_STRING_LENGTH];
    BUFFER *output;
    char **prgpstrShow;
    int *prgnShow;
    char *pstrShow;
    OBJ_DATA *obj,*realobj;
    int nShow;
    int iShow;
    int count;
    bool fCombine;

    if ( ch->desc == NULL )
        return;

    /*
     * Alloc space for output lines.
     */
    output = new_buf();

    count = 0;
    for ( obj = list; obj != NULL; obj = obj->next_content )
        count++;
    prgpstrShow	= alloc_mem( count * sizeof(char *) );
    prgnShow    = alloc_mem( count * sizeof(int)    );
    nShow	= 0;

    /*
     * Format the list of objects.
     */
    for ( realobj = list; realobj != NULL; realobj = realobj->next_content )
    {
        if (IS_PC(ch))
            STR_SET_BIT(ch->pcdata->seenthatobject,realobj->pIndexData->vnum);

        if (IS_AFFECTED(ch,EFF_HALLUCINATION))
            obj=hallucination_obj();
        else
            obj=realobj;

        if (prefix!=NULL && !is_name(prefix,obj->name))
            continue;

        if ( obj->wear_loc == WEAR_NONE && can_see_obj( ch, obj )
             && (STR_IS_SET(ch->strbit_act,PLR_HOLYLIGHT) || !IS_OBJ_STAT(obj,ITEM_NOSHOW)))
        {
            pstrShow = format_obj_to_char( obj, ch, fShort );

            fCombine = FALSE;

            if ( IS_NPC(ch) || STR_IS_SET(ch->strbit_comm, COMM_COMBINE) )
            {
                /*
         * Look for duplicates, case sensitive.
         * Matches tend to be near end so run loop backwords.
         */
                for ( iShow = nShow - 1; iShow >= 0; iShow-- )
                {
                    if ( !strcmp( prgpstrShow[iShow], pstrShow ) )
                    {
                        prgnShow[iShow]++;
                        fCombine = TRUE;
                        break;
                    }
                }
            }

            /*
         * Couldn't combine, or didn't want to.
         */
            if ( !fCombine )
            {
                prgpstrShow [nShow] = str_dup( pstrShow );
                prgnShow    [nShow] = 1;
                nShow++;
            }
        }
    }

    /*
     * Output the formatted list.
     */
    for ( iShow = 0; iShow < nShow; iShow++ )
    {
        if (prgpstrShow[iShow][0] == '\0')
        {
            free_string(prgpstrShow[iShow]);
            continue;
        }

        if ( IS_NPC(ch) || STR_IS_SET(ch->strbit_comm, COMM_COMBINE) )
        {
            if ( prgnShow[iShow] != 1 )
            {
                sprintf( buf, "(%2d) ", prgnShow[iShow] );
                add_buf(output,buf);
            }
            else
            {
                add_buf(output,"     ");
            }
        }
        add_buf(output,prgpstrShow[iShow]);
        add_buf(output,"\n\r");
        free_string( prgpstrShow[iShow] );
    }

    if ( fShowNothing && nShow == 0 )
    {
        if ( IS_NPC(ch) || STR_IS_SET(ch->strbit_comm, COMM_COMBINE) )
            send_to_char( "     ", ch );
        send_to_char( "Nothing.\n\r", ch );
    }
    page_to_char(buf_string(output),ch);

    /*
     * Clean up.
     */
    free_buf(output);
    free_mem( prgpstrShow, count * sizeof(char *) );
    free_mem( prgnShow,    count * sizeof(int)    );

    return;
}



void show_char_to_char_0( CHAR_DATA *victim, CHAR_DATA *ch )
{
    char buf[MAX_STRING_LENGTH],message[MAX_STRING_LENGTH];
    char stime[100],pueblo[MSL];
    time_t ttime;

    ttime=time(NULL)-10*60;

    if (IS_PC(ch) && IS_NPC(victim))
        STR_SET_BIT(ch->pcdata->seenthatmob,victim->pIndexData->vnum);

    if (IS_AFFECTED(ch,EFF_HALLUCINATION))
        victim=hallucination_char();

    buf[0] = '\0';

    if (IS_PC(ch) && IS_NPC(victim) &&
            STR_IS_SET(ch->strbit_act,PLR_HOLYLIGHT_PLUSPLUS))
        sprintf(buf,"[%d] ",victim->pIndexData->vnum);


    if (IS_PC(victim) && STR_IS_SET(victim->strbit_comm,COMM_AFK))	strcat(buf,"{b[AFK]{x ");
    if (IS_AFFECTED(victim, EFF_INVISIBLE))	strcat(buf,"(Invis) ");
    // tricky, don't show this to normal players
    if (!IS_AFFECTED(ch,EFF_HALLUCINATION) &&
            IS_AFFECTED2(victim, EFF_IMPINVISIBLE)) strcat(buf,"(ImpInvis) ");
    if (victim->invis_level >= LEVEL_HERO)	strcat(buf,"({wWizi{x) ");
    if (IS_AFFECTED(victim, EFF_HIDE))		strcat(buf,"({wHide{x) ");
    if (IS_AFFECTED(victim, EFF_CHARM))		strcat(buf,"({wCharmed{x) ");
    if (IS_AFFECTED(victim, EFF_PASS_DOOR))	strcat(buf,"({wTranslucent{x) ");
    if (IS_AFFECTED(victim, EFF_INVITED))	strcat(buf,"({gInvited{x) ");
    if (IS_AFFECTED(victim, EFF_CURSE)
            &&  IS_AFFECTED(ch, EFF_DETECT_CURSE))	strcat(buf,"({wCursed{x) ");
    if ( IS_AFFECTED(victim, EFF_FAERIE_FIRE))	strcat(buf,"({mPink Aura{x) ");
    if ( IS_EVIL(victim)
         &&   IS_AFFECTED(ch, EFF_DETECT_EVIL))	strcat(buf,"({rRed Eyes{x) ");
    if ( IS_GOOD(victim)
         &&   IS_AFFECTED(ch, EFF_DETECT_GOOD))	strcat(buf,"({yGolden Aura{x) ");
    if ( IS_AFFECTED(victim, EFF_SANCTUARY))	strcat(buf,"({wWhite Aura{x) ");
    if (IS_NPC(victim) && STR_IS_SET(ch->strbit_act,PLR_QUESTOR)) {
        int quest_mob=0;

        GetCharProperty(ch,PROPERTY_INT,"quest_mob",&quest_mob);
        if (quest_mob>0 && victim->pIndexData->vnum==quest_mob)
            strcat( buf, "[TARGET] ");
    }

    pueblo[0]=0;
    if (HAS_PUEBLO(ch)) {
        if (IS_NPC(victim) && victim->pIndexData->pueblo_picture[0]) {
            sprintf(pueblo,
                    "</xch_mudtext><a xch_cmd=\"pueblo look %s\">"
                    "<img src=\""URLBASE"/pics/limbo/photo.gif\">"
                                        " </a><xch_mudtext>",
                    victim->name);
        }
        if (IS_PC(victim) && victim->pcdata->pueblo_picture[0]) {
            sprintf(pueblo,
                    "</xch_mudtext><a xch_cmd=\"pueblo look %s\">"
                    "<img src=\""URLBASE"/pics/limbo/photo.gif\">"
                                        " </a>"
                                        "<xch_mudtext>",
                    victim->name);
        }
    }
    if (HAS_MXP(ch)) {
#ifdef MXP_IMAGE
        if (IS_NPC(victim) && victim->pIndexData->pueblo_picture[0]) {
            sprintf(pueblo,MXP"<send \"look %s\">"
                              "<image url=\""URLBASE"/pics/limbo/photo.gif\">"
                                                    " </a></send>",
                    victim->name);
        }
        if (IS_PC(victim) && victim->pcdata->pueblo_picture[0]) {
            sprintf(pueblo,
                    MXP"<send \"look %s\">"
                       "<image url=\""URLBASE"/pics/limbo/photo.gif\">"
                                             " </a></send>",
                    victim->name);
        }
#endif
        if (IS_NPC(victim))
            sprintf(pueblo,
                    MXP_SECURE"<send \"look %s|cons %s|kill %s\" hint=\"|Look at %s|Consider %s|Kill %s\">[x]</SEND> ",
                    victim->name,victim->name,victim->name,
                    NAME(victim),NAME(victim),NAME(victim)
                    );
    }

    if (!IS_NPC(victim) && STR_IS_SET(victim->strbit_act,PLR_KILLER))
        strcat(buf,"({rKILLER{x) ");
    if (!IS_NPC(victim) && STR_IS_SET(victim->strbit_act,PLR_THIEF ))
        strcat(buf,"({rTHIEF{x) ");

    strcat( buf, pueblo);
    if (victim->position==victim->default_pos && victim->long_descr[0]!='\0' ) {
        strcat( buf, victim->long_descr );
        send_to_char( buf, ch );
        if( IS_AFFECTED2( victim, EFF_CANTRIP ) )
            show_cantrip( ch , victim, TO_VICT);
        show_ool_cantrip(ch,victim,TO_VICT);
        return;
    }

    strcat( buf, PERS( victim, ch ) );
    if ( !IS_NPC(victim) && !STR_IS_SET(ch->strbit_comm, COMM_BRIEF)
         &&   victim->position == POS_STANDING && victim->on==NULL) {
        strcat( buf, victim->pcdata->title );
        strcat( buf, "{x");
    }

    switch ( victim->position )
    {
    case POS_DEAD:     strcat( buf, " is DEAD!!" );              break;
    case POS_MORTAL:   strcat( buf, " is mortally wounded." );   break;
    case POS_INCAP:    strcat( buf, " is incapacitated." );      break;
    case POS_STUNNED:  strcat( buf, " is lying here stunned." ); break;
    case POS_SLEEPING:
        stime[0]=0;
        if (!IS_NPC(victim)&&victim->pcdata->idle<ttime) strcpy(stime,"still ");
        if (victim->on != NULL) {
            if (IS_SET(victim->on->value[2],SLEEP_AT)) {
                sprintf(message," is %ssleeping at %s.",
                        stime,victim->on->short_descr);
                strcat(buf,message);
            } else if (IS_SET(victim->on->value[2],SLEEP_ON)) {
                sprintf(message," is %ssleeping on %s.",
                        stime,victim->on->short_descr);
                strcat(buf,message);
            } else {
                sprintf(message, " is %ssleeping in %s.",
                        stime,victim->on->short_descr);
                strcat(buf,message);
            }
        } else {
            sprintf(message, " is %ssleeping here.",stime);
            strcat(buf,message);
        }
        break;
    case POS_RESTING:
        stime[0]=0;
        if (!IS_NPC(victim)&&victim->pcdata->idle<ttime) strcpy(stime,"still ");
        if (victim->on != NULL) {
            if (IS_SET(victim->on->value[2],REST_AT)) {
                sprintf(message," is %sresting at %s.",
                        stime,victim->on->short_descr);
                strcat(buf,message);
            } else if (IS_SET(victim->on->value[2],REST_ON)) {
                sprintf(message," is %sresting on %s.",
                        stime,victim->on->short_descr);
                strcat(buf,message);
            } else {
                sprintf(message, " is %sresting in %s.",
                        stime,victim->on->short_descr);
                strcat(buf,message);
            }
        } else {
            sprintf(message, " is %sresting here.",stime);
            strcat(buf,message);
        }
        break;
    case POS_SITTING:
        stime[0]=0;
        if (!IS_NPC(victim)&&victim->pcdata->idle<ttime) strcpy(stime,"still ");
        if (victim->on != NULL) {
            if (IS_SET(victim->on->value[2],SIT_AT)) {
                sprintf(message," is %ssitting at %s.",
                        stime,victim->on->short_descr);
                strcat(buf,message);
            } else if (IS_SET(victim->on->value[2],SIT_ON)) {
                sprintf(message," is %ssitting on %s.",
                        stime,victim->on->short_descr);
                strcat(buf,message);
            } else {
                sprintf(message, " is %ssitting in %s.",
                        stime,victim->on->short_descr);
                strcat(buf,message);
            }
        } else {
            sprintf(message, " is %ssitting here.",stime);
            strcat(buf,message);
        }
        break;
    case POS_STANDING:
        stime[0]=0;
        if (!IS_NPC(victim)&&victim->pcdata->idle<ttime) strcpy(stime,"still ");
        if (victim->on != NULL) {
            if (IS_SET(victim->on->value[2],STAND_AT)) {
                sprintf(message," is %sstanding at %s.",
                        stime,victim->on->short_descr);
                strcat(buf,message);
            } else if (IS_SET(victim->on->value[2],STAND_ON)) {
                sprintf(message," is %sstanding on %s.",
                        stime,victim->on->short_descr);
                strcat(buf,message);
            } else {
                sprintf(message," is %sstanding in %s.",
                        stime,victim->on->short_descr);
                strcat(buf,message);
            }
        } else {
            sprintf(message, " is %shere.",stime);
            strcat(buf,message);
        }
        break;
    case POS_FIGHTING:
        strcat( buf, " is here, fighting " );
        if ( victim->fighting == NULL )
            strcat( buf, "thin air??" );
        else if ( victim->fighting == ch )
            strcat( buf, "YOU!" );
        else if ( victim->in_room == victim->fighting->in_room ) {
            strcat( buf, PERS( victim->fighting, ch ) );
            strcat( buf, "." );
        } else
            strcat( buf, "someone who left??" );
        break;
    }

    strcat( buf, "\n\r" );
    buf[0] = UPPER(buf[0]);
    send_to_char( buf, ch );

    // linkdead
    if (IS_PC(victim) && victim->desc==NULL)
        act("$N has an absent look in $S eyes.",ch,NULL,victim,TO_CHAR);

    // cantripped
    if( IS_AFFECTED2( victim, EFF_CANTRIP ) )
        show_cantrip( ch , victim, TO_VICT);
    show_ool_cantrip(ch,victim,TO_VICT);

    // plagued
    if (is_affected(victim,gsn_plague) || IS_AFFECTED(victim,EFF_PLAGUE))
        act("$N has black stains over $S face.",ch,NULL,victim,TO_CHAR);

    // hallucinating
    if (is_affected(victim,gsn_hallucination) ||
            IS_AFFECTED(victim,EFF_HALLUCINATION))
        act("$N moves $S head to the rhythm of the music.",
            ch,NULL,victim,TO_CHAR);

    return;
}


void show_char_to_char_1( CHAR_DATA *victim, CHAR_DATA *ch )
{
    char buf[MAX_STRING_LENGTH];
    OBJ_DATA *obj;
    int iWear;
    int percent;
    bool found;

    if ( can_see( victim, ch ) )
    {
        if (ch == victim)
            act( "$n looks at $mself.",ch,NULL,NULL,TO_ROOM);
        else
        {
            act( "$n looks at you.", ch, NULL, victim, TO_VICT    );
            act( "$n looks at $N.",  ch, NULL, victim, TO_NOTVICT );
        }
    }

    if (IS_PC(ch) && IS_NPC(victim))
        STR_SET_BIT(ch->pcdata->seenthatmob,victim->pIndexData->vnum);

    if (IS_AFFECTED(ch,EFF_HALLUCINATION))
        victim=hallucination_char();

    // display picture of mob/char
    if (HAS_PUEBLO(ch)) {
        if (IS_NPC(victim)) {
            if (victim->pIndexData->pueblo_picture[0]) {
                sprintf_to_char(ch,"</xch_mudtext>"
                                   "<img src=\"%s\" align=\"right\"><xch_mudtext>",
                                url(buf,victim->pIndexData->pueblo_picture,
                                    victim->pIndexData->area,FALSE));
            }
        } else {
            if (victim->pcdata->pueblo_picture[0]) {
                sprintf_to_char(ch,"</xch_mudtext>"
                                   "<img src=\"%s\" align=\"right\"><xch_mudtext>",
                                victim->pcdata->pueblo_picture);
            }
        }
    }
#ifdef MXP_IMAGE
    if (HAS_MXP(ch)) {
        if (IS_NPC(victim)) {
            if (victim->pIndexData->pueblo_picture[0]) {
                sprintf_to_char(ch,
                                MXP"<image url=\"%s\" align=\"right\">",
                                url(buf,victim->pIndexData->pueblo_picture,
                                    victim->pIndexData->area,FALSE));
            }
        } else {
            if (victim->pcdata->pueblo_picture[0]) {
                sprintf_to_char(ch,
                                MXP"<image url=\"%s\" align=\"right\">",
                                victim->pcdata->pueblo_picture);
            }
        }
    }
#endif

    if ( victim->description[0] != '\0' )
    {
        send_to_char( victim->description, ch );
    }
    else
    {
        act( "You see nothing special about $M.", ch, NULL, victim, TO_CHAR );
    }

    if ( victim->max_hit > 0 )
        percent = ( 100 * victim->hit ) / victim->max_hit;
    else
        percent = -1;

    strcpy( buf, PERS(victim, ch) );

    if (percent >= 100)
        strcat( buf, " is in excellent condition.\n\r");
    else if (percent >= 90)
        strcat( buf, " has a few scratches.\n\r");
    else if (percent >= 75)
        strcat( buf," has some small wounds and bruises.\n\r");
    else if (percent >=  50)
        strcat( buf, " has quite a few wounds.\n\r");
    else if (percent >= 30)
        strcat( buf, " has some big nasty wounds and scratches.\n\r");
    else if (percent >= 15)
        strcat ( buf, " looks pretty hurt.\n\r");
    else if (percent >= 0 )
        strcat (buf, " is in awful condition.\n\r");
    else if (victim->position>POS_DEAD)
        strcat(buf, " is bleeding to death.\n\r");
    else
        strcat(buf, " is dead.\n\r");

    buf[0] = UPPER(buf[0]);
    send_to_char( buf, ch );

    if (victim->position>POS_DEAD) {
        /* Look at his movement-status. Thanks to Benedict for it */
        if (victim->max_move > 0 )
            percent = ( 100 * victim->move ) / victim->max_move;
        else
            percent=-1;

        strcpy( buf, PERS(victim, ch) );
        if (percent >= 50) {
            if (IS_AFFECTED(victim,EFF_FLYING))
                strcat( buf, " can fly the whole day.\n\r");
            else
                strcat( buf, " can walk the whole day.\n\r");
        }
        else if (percent >= 25)
            strcat( buf, " looks well-travelled.\n\r");
        else if (percent >= 10)
            strcat( buf, " looks weary.\n\r");
        else if (percent > 0)
            strcat( buf, " needs a holiday.\n\r");
        else
            strcat( buf, " is very tired.\n\r");
        buf[0] = UPPER(buf[0]);
        send_to_char( buf, ch );
    }


    if( IS_AFFECTED2( victim, EFF_CANTRIP ) )
        show_cantrip( ch , victim, TO_VICT);
    show_ool_cantrip(ch,victim,TO_VICT);

    found = FALSE;
    for ( iWear = 0; iWear < MAX_WEAR; iWear++ )
    {
        if ( ( obj = get_eq_char( victim, iWear ) ) != NULL
             &&   can_see_obj( ch, obj ) && !IS_OBJ_STAT(obj,ITEM_NOSHOW))
        {
            if ( !found )
            {
                send_to_char( "\n\r", ch );
                act( "$N is using:", ch, NULL, victim, TO_CHAR );
                found = TRUE;
            }
            send_to_char( where_name[iWear], ch );
            send_to_char( format_obj_to_char( obj, ch, TRUE ), ch );
            send_to_char( "\n\r", ch );
        }
    }

    // the peeking in the inventory has been removed and placed into the
    // "peek" command do be able to disable it with look.

    return;
}



void show_char_to_char( CHAR_DATA *list, CHAR_DATA *ch )
{
    CHAR_DATA *rch;

    for ( rch = list; rch != NULL; rch = rch->next_in_room ) {
        if ( rch == ch )
            continue;

        if ( get_trust(ch) < rch->invis_level)
            continue;

        if ( can_see( ch, rch ) ) {
            show_char_to_char_0( rch, ch );
        } else if ( room_is_dark( ch->in_room )
                    &&        IS_AFFECTED(ch, EFF_INFRARED ) ) {
            send_to_char( "You see glowing red eyes watching YOU!\n\r", ch );
        }
    }

    return;
}



// returns TRUE if character can see
bool check_blind( CHAR_DATA *ch,bool show_message )
{

    if (!IS_NPC(ch) && STR_IS_SET(ch->strbit_act,PLR_HOLYLIGHT))
        return TRUE;

    if ( IS_AFFECTED(ch, EFF_BLIND) ) {
        if (show_message)
            send_to_char( "You can't see a thing!\n\r", ch );
        return FALSE;
    }

    if ( is_affected(ch, gsn_blindness )) {
        if (show_message)
            send_to_char( "You can't see a thing!\n\r", ch );
        return FALSE;
    }

    return TRUE;
}


void do_socials(CHAR_DATA *ch, char *argument) {
    static BUFFER *buffer=NULL;
    int top = 0;
    int j,k;
    SOCIAL_TYPE *social;

    if (!buffer || mud_data.socials_changed) {
        char *Commands[1024];		// at most 1024 socials
        char buf[MAX_INPUT_LENGTH];
        int  i;

        if (buffer)
            free_buf(buffer);
        logf("[0] Sorting social-table");
        buffer = new_buf();
        add_buf(buffer,"{cSocials List:{W\n\r");

        for (k=0;k<MAX_SOCIAL_HASH;k++) {
            social=social_hash[k];
            while (social) {
                for ( i = 0; i < top; i++ )
                    if ( strcmp( Commands[i], social->name ) > 0 )
                        break;
                for ( j = top; j > i; j-- )
                    Commands[j] = Commands[j-1];
                Commands[i] = social->name;
                social=social->next;
                top++;
                if (top==1024) {
                    bugf("Too many socials (1024 at most), please fix do_socials().");
                    break;		// silent break for user.
                }
            }
            if (top==1024)
                break;
        }
        for ( i = 0; i < top; i++ ) {
            sprintf( buf, "{W%-19s", Commands[i]);
            add_buf(buffer, buf);
            if ( ( i + 1 )%4 == 0 ) {
                sprintf( buf, "{x\n\r" );
                add_buf(buffer, buf);
            }
        }
        add_buf(buffer,"\r\n{x");
        mud_data.socials_changed=FALSE;
    }

    page_to_char( buf_string(buffer), ch );

    /*  free_buf(buffer);	This buffer is not freed here.
            Perhaps if the socials are edited, but not here. */
}

/* RT Commands to replace news, motd, imotd, etc from ROM */

void do_motd(CHAR_DATA *ch, char *argument) {
    show_help(ch,"motd");
    send_to_char("\n\r",ch);
}

void do_imotd(CHAR_DATA *ch, char *argument) {
    show_help(ch,"imotd");
    send_to_char("\n\r",ch);
}

void do_rules(CHAR_DATA *ch, char *argument) {
    show_help(ch,"rules");
}

void do_story(CHAR_DATA *ch, char *argument) {
    show_help(ch,"story");
}

void do_herolist(CHAR_DATA *ch, char *argument) {
    show_help(ch,"herolist");
}

void do_wizlist(CHAR_DATA *ch, char *argument) {
    show_help(ch,"wizlist");
}

void do_credits(CHAR_DATA *ch, char *argument) {
    show_help(ch,"diku");
}


void do_nofollow(CHAR_DATA *ch, char *argument)
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;

    if (IS_NPC(ch))
        return;

    /* nofollow <target> */
    if (argument[0]) {
        one_argument(argument,arg);
        if ((victim=get_char_room(ch,arg))==NULL) {
            send_to_char( "They aren't here.\n\r", ch );
            return;
        }

        if (victim->master!=ch) {
            act("You wish $N was following you!",ch,NULL,victim,TO_CHAR);
            return;
        }
        stop_follower(victim);
        act("$n sniffs sadly at the way you're treating $m.",
            victim,NULL,ch,TO_VICT);
        act("You sniff sadly at the way $N is treating you.",
            victim,NULL,ch,TO_CHAR);
        act("$n is forced to stop following $N.",victim,NULL,ch,TO_NOTVICT);
        act("$n sniffs sadly at the way $N is treating $m.",
            victim,NULL,ch,TO_NOTVICT);

        return;
    }

    if (STR_IS_SET(ch->strbit_act,PLR_NOFOLLOW)) {
        send_to_char("You now accept followers.\n\r",ch);
        STR_REMOVE_BIT(ch->strbit_act,PLR_NOFOLLOW);
    } else {
        send_to_char("You no longer accept followers.\n\r",ch);
        STR_SET_BIT(ch->strbit_act,PLR_NOFOLLOW);
        die_follower( ch );
    }
}

//
// compass code
//
void compass_outputline(CHAR_DATA *ch,char *p,int *linelength,int *currentline,char **compasslines) {
    if (*linelength+strlen(p)>60) {
        send_to_char("\n\r",ch);
        if (*currentline<5)
            send_to_char(compasslines[(*currentline)++],ch);
        else
            send_to_char(compasslines[5],ch);
        *linelength=0;
    }
    send_to_char(" ",ch);
    send_to_char(p,ch);
    *linelength+=strlen(p)+1;
}

void compass_formatstring(CHAR_DATA *ch,char **compasslines,char *description) {
    char *p,*nextspace;
    int linelength,currentline;

    p=description;
    currentline=1;
    send_to_char(compasslines[0],ch);
    linelength=0;
    while (1) {

        if ((nextspace=strchr(p,' '))==NULL) {
            // famous last words, print them and get out of the loop
            compass_outputline(ch,p,&linelength,&currentline,compasslines);
            send_to_char("\n\r",ch);
            break;
        }

        nextspace[0]=0;
        compass_outputline(ch,p,&linelength,&currentline,compasslines);

        p=nextspace+1;
    }

    while (currentline<5) {
        send_to_char(compasslines[currentline++],ch);
        send_to_char("\n\r",ch);
    }
}

void add_compass(CHAR_DATA *ch,char *description) {
    static char **compasslines=NULL;
    const int coordinates[DIR_MAX*2]={0,7, 2,9, 4,3, 2,1, 0,5, 4,5 };
    char desc[MSL];
    char *p=description;
    char *q=desc;
    int door;
    EXIT_DATA *pexit;

    // reformat description without newlines
    while (p[0]!=0) {
        if (p[0]=='\n') {
            p++;
            continue;
        }
        if (p[0]=='\r') {
            q[0]=' ';
            q++;
            p++;
            continue;
        }
        q[0]=p[0];
        q++;
        p++;
    }
    q[0]=0;

    // initial compass, happens only once.
    if (compasslines==NULL) {
        compasslines=(char **)calloc(6,sizeof(char *));
        compasslines[0]=strdup("     . .    ");
        compasslines[1]=strdup("     |/     ");
        compasslines[2]=strdup(" . -   - .  ");
        compasslines[3]=strdup("    /|      ");
        compasslines[4]=strdup("   . .      ");
        compasslines[5]=strdup("            ");
        logf("[0] Initializing compass");
    }

    // fill in compass according to what player can see
    for (door = 0; door < DIR_MAX; door++) {
        if ((pexit = ch->in_room->exit[door]) != NULL
                &&  pexit ->to_room != NULL
                &&  (can_see_room(ch,pexit->to_room)
                     ||   IS_AFFECTED(ch,EFF_INFRARED))
                &&  !IS_AFFECTED(ch,EFF_BLIND)
                &&  !IS_SET(pexit->exit_info,EX_CLOSED)) {
            compasslines[coordinates[2*door]][coordinates[2*door+1]]=toupper(dir_name[door][0]);
        } else {
            compasslines[coordinates[2*door]][coordinates[2*door+1]]=' ';
        }
    }

    compass_formatstring(ch,compasslines,desc);
}

/* wrapper for look() for examine to work. Cheaper than copying
   or rewriting the whole of the original do_look just for
   lore */


void do_look( CHAR_DATA * ch, char * argument ) {
    look( ch, argument, FALSE, FALSE );
}

void look_room(CHAR_DATA *ch, ROOM_DATA *room, bool enablebrief) {

    if (!ch) {
        bugf("look_room(): ch==NULL");
        return;
    }
    if (!room) {
        bugf("look_room(): room==NULL");
        return;
    }

    /* 'look' or 'look auto' */
    if ( ROOM_HAS_TRIGGER( room, RTRIG_PRELOOK ) )
        if (rp_prelook_trigger( room, ch )==FALSE)
            return;
    if (!IS_VALID(ch))
        return;

    if (!check_blind(ch,FALSE)) {
        send_to_char( "You can't see anything ... \n\r", ch );
    } else if (room_is_dark(ch->in_room) &&
               !STR_IS_SET(ch->strbit_act, PLR_HOLYLIGHT) ) {
        send_to_char( "It is pitch black ... \n\r", ch );
        show_char_to_char( ch->in_room->people, ch );
    } else {

        if (HAS_MXP(ch))
            sprintf_to_char(ch,MXP_SECURE "<RName>" C_ROOMNAME "%s" MXP_SECURE "</RName>",room->name);
        else
            sprintf_to_char(ch,C_ROOMNAME "%s",room->name);

        if (IS_IMMORTAL(ch) &&
                (IS_NPC(ch) || STR_IS_SET(ch->strbit_act,PLR_HOLYLIGHT)))
            sprintf_to_char(ch," [Room %d]",room->vnum);

        send_to_char( "{n\n\r", ch );

        // display picture of room
        if (HAS_PUEBLO(ch)) {
            if (room->pueblo_picture[0]) {
                sprintf_to_char(ch,"</xch_mudtext>"
                                   "<img src=\"%s\" align=\"right\"><xch_mudtext>",
                                url(NULL,room->pueblo_picture,
                                    room->area,FALSE));
            }
        }
#ifdef MXP_IMAGE
        if (HAS_MXP(ch)) {
            if (room->pueblo_picture[0]) {
                sprintf_to_char(ch,
                                MXP"<image url=\"%s\" align=\"right\">\n\r",
                                url(NULL,room->pueblo_picture,
                                    room->area,FALSE));
            }
        }
#endif

        if (!enablebrief || IS_NPC(ch) || !STR_IS_SET(ch->strbit_comm, COMM_BRIEF)) {
            if (HAS_MXP(ch)) {
#ifdef NOTDEF
                char s[MSL];
                char *p,*q;

                if (STR_IS_SET(ch->strbit_act,PLR_COMPASS)) {
                    add_compass(ch,room->description,s);
                } else {
                    strcpy(s,room->description);
                }
                p=s;
                while (p) {
                    if ((q=strchr(p,'\n'))==NULL) {

                        if (strlen(p)!=0)
                            sprintf_to_char(ch,MXP_SECURE "<RDesc>--%s</RDesc>\n\r",p);
                        p=NULL;
                    } else {
                        q[0]=0;
                        sprintf_to_char(ch,MXP_SECURE "<RDesc>%s</RDesc>\n\r",p);
                        p=q+2;
                    }
                }
#endif
                sprintf_to_char(ch,MXP"<RDesc>  %s"MXP"</RDesc>",room->description);

            } else {
                if (STR_IS_SET(ch->strbit_act,PLR_COMPASS)) {
                    add_compass(ch,room->description);
                } else {
                    send_to_char( "  ",ch);
                    send_to_char( room->description, ch );
                }
            }
        }

        if (ch->in_room==room && !IS_NPC(ch) && STR_IS_SET(ch->strbit_act, PLR_AUTOEXIT) && check_blind( ch,TRUE ))
            show_exits(ch, TRUE );

        show_list_to_char( room->contents, ch, NULL, FALSE, FALSE );
        show_char_to_char( room->people,   ch );
    }
    //
    // sixth sense, perhaps they see the danger coming....
    //
    if (ch->in_room==room && skillcheck(ch,gsn_sixth_sense)) {
        int door;
        EXIT_DATA *pexit;
        for (door=0;door<DIR_MAX;door++) {
            if ( ( pexit = ch->in_room->exit[door] ) != NULL
                 &&   pexit->to_room != NULL) {
                if (STR_IS_SET(pexit->to_room->strbit_room_flags,ROOM_DEATHTRAP)) {
                    act("You smell death $T from here.",
                        ch,NULL,dir_name[door],TO_CHAR);
                }
                if (IS_SET(pexit->exit_info,EX_CLOSED)) {
                    if (IS_SET(pexit->exit_info,EX_TRAP)) {
                        act("You feel a shiver on your back when you look $T.",
                            ch,NULL,dir_name[door],TO_CHAR);
                    }
                    if (IS_SET(pexit->exit_info,EX_TRAP_DARTS)) {
                        act("You feel a stab on your back when you look $T.",
                            ch,NULL,dir_name[door],TO_CHAR);
                    }
                    if (IS_SET(pexit->exit_info,EX_TRAP_POISON)) {
                        act("You feel very sick when you look $T.",
                            ch,NULL,dir_name[door],TO_CHAR);
                    }
                    if (IS_SET(pexit->exit_info,EX_TRAP_EXPLODING)) {
                        act("You feel a blast of hot air in your face when "
                            "you look $T.",
                            ch,NULL,dir_name[door],TO_CHAR);
                    }
                    if (IS_SET(pexit->exit_info,EX_TRAP_SLEEPGAS)) {
                        act("You feel an urge to sleep when you look $T.",
                            ch,NULL,dir_name[door],TO_CHAR);
                    }
                    if (IS_SET(pexit->exit_info,EX_TRAP_DEATH)) {
                        act("You smell death when you look $T.",
                            ch,NULL,dir_name[door],TO_CHAR);
                    }
                }
            }
        }
    }

    rp_look_trigger( ch->in_room, ch );
    return;
}

void look_through(CHAR_DATA *ch, OBJ_DATA *obj) {
    ROOM_INDEX_DATA *destination_room;

    if ((obj->item_type!=ITEM_PORTAL) ||
            (IS_SET(obj->value[1],EX_CLOSED))) {
        send_to_char("You can't look through that.\n\r",ch);
        return;
    }

    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRELOOKIN ) )
        if (op_prelookin_trigger( obj, ch )==FALSE)
            return;
    if (!IS_VALID(obj))
        return;

    destination_room=get_portal_destination(ch,obj);
    if (destination_room==NULL) {
        send_to_char("You see nothing but darkness. "
                     "That is not a good sign.\n\r",ch);
        return;
    }
    act("You look through $p and you see the blurred vision of a room:",
        ch,obj,NULL,TO_CHAR);

    look_room(ch,destination_room,FALSE);

    if ( OBJ_HAS_TRIGGER( obj, OTRIG_LOOKIN ) )
        op_lookin_trigger( obj, ch );
    if (!IS_VALID(obj))
        return;

    if (destination_room->people) {
        act("You feel watched.",destination_room->people,NULL,NULL,TO_ROOM);
    }
}

void look_in(CHAR_DATA *ch, OBJ_DATA *obj, char *prefix) {
    switch ( obj->item_type )
    {
    default:
        send_to_char( "That is not a container.\n\r", ch );
        break;

    case ITEM_DRINK_CON:

        if (obj->level>ch->level) {
            act("The $d blurs your vision.",
                ch,NULL,obj->name,TO_CHAR );
            return;
        }

        if ( obj->value[1] <= 0 )
        {
            send_to_char( "It is empty.\n\r", ch );
            break;
        }

        if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRELOOKIN ) )
            if (op_prelookin_trigger( obj, ch )==FALSE)
                break;
        if (!IS_VALID(obj))
            break;

        sprintf_to_char( ch, "It's %sfilled with  a %s liquid.\n\r",
                         obj->value[1] <     obj->value[0] / 4 ? "less than half-" :
                                                                 obj->value[1] < 3 * obj->value[0] / 4 ? "about half-"     :
                                                                                                         "more than half-",
                liq_table[obj->value[2]].liq_color );

        if ( OBJ_HAS_TRIGGER( obj, OTRIG_LOOKIN ) )
            op_lookin_trigger( obj, ch );
        break;

    case ITEM_PORTAL:
    {
        look_through(ch,obj);
        break;
    }

    case ITEM_CONTAINER:
        if (obj->level>ch->level) {
            act("The $d blurs your vision.",
                ch, NULL, obj->name, TO_CHAR );
            return;
        }

        if ( IS_SET(obj->value[1], CONT_CLOSED) ) {
            send_to_char( "It is closed.\n\r", ch );
            break;
        }

        /* Note: fall through to ITEM_CORPSE_NPC and ITEM_CORPSE_PC */

    case ITEM_CORPSE_NPC:
    case ITEM_CORPSE_PC:

        if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRELOOKIN ) )
            if (op_prelookin_trigger( obj, ch )==FALSE)
                break;
        if (!IS_VALID(obj))
            break;

        act( "$p holds:", ch, obj, NULL, TO_CHAR );
        show_list_to_char( obj->contains, ch, prefix, TRUE, TRUE );

        if ( OBJ_HAS_TRIGGER( obj, OTRIG_LOOKIN ) )
            op_lookin_trigger( obj, ch );

        break;
    }
}

void look( CHAR_DATA *ch, char *argument,bool isexamine,bool isread) {
    char buf  [MAX_STRING_LENGTH];
    char arg1 [MAX_INPUT_LENGTH];
    char arg2 [MAX_INPUT_LENGTH];
    char arg3 [MAX_INPUT_LENGTH];
    char arg4 [MAX_INPUT_LENGTH];
    char arg5 [MAX_INPUT_LENGTH];
    EXIT_DATA *pexit;
    CHAR_DATA *victim;
    OBJ_DATA *obj,*obj_list,*container;
    EXTRA_DESCR_DATA *pdesc;
    int door;
    int number,count;

    if ( ch->desc == NULL )
        return;

    if ( ch->position < POS_SLEEPING ) {
        send_to_char( "You can't see anything but stars!\n\r", ch );
        return;
    }

    if ( ch->position == POS_SLEEPING ) {
        send_to_char( "You can't see anything, you're sleeping!\n\r", ch );
        return;
    }

    if ( !check_blind( ch,TRUE ) )
        return;

    if ( !IS_NPC(ch)
         &&   !STR_IS_SET(ch->strbit_act, PLR_HOLYLIGHT)
         &&   room_is_dark( ch->in_room ) ) {
        send_to_char( "It is pitch black ... \n\r", ch );
        show_char_to_char( ch->in_room->people, ch );
        return;
    }

    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );
    argument = one_argument( argument, arg4 );	// look <object> on <victim>
    argument = one_argument( argument, arg5 );	// look for <object> in <object>
    number = number_argument(arg1,arg3);
    count = 0;

    if (arg1[0] == '\0') {
        look_room(ch,ch->in_room,FALSE);
        return;
    }
    if (!str_cmp( arg1, "auto")) {
        look_room(ch,ch->in_room,IS_PC(ch) && STR_IS_SET(ch->strbit_comm, COMM_BRIEF));
        return;
    }


    //
    // LOOK FOR <object>
    //
    if (!isread && !isexamine &&
            !str_cmp(arg1,"for") && arg4[0]==0) {
        char *prefix;

        if ( ROOM_HAS_TRIGGER( ch->in_room, RTRIG_PRELOOK ) )
            if (rp_prelook_trigger( ch->in_room, ch )==FALSE)
                return;
        if (!IS_VALID(ch))
            return;

        if ( arg2[0] == '\0' ) {
            send_to_char( "Look for what?\n\r", ch );
            return;
        }

        prefix=arg2;
        show_list_to_char( ch->in_room->contents, ch, prefix, TRUE, TRUE );

        rp_look_trigger( ch->in_room, ch );
        return;
    }

    //
    // LOOK <IN|ON> <object>
    // LOOK FOR <object> IN <container>
    //
    if (!isread && !isexamine &&
            (!str_cmp(arg1, "in")  || !str_cmp(arg1,"on") ||
             (!str_cmp(arg1, "for") && !str_cmp(arg4,"in")))) {

        char *prefix=NULL;

        // look in
        if (!str_cmp(arg1,"in") || !str_cmp(arg1,"on")) {
            if ( arg2[0] == '\0' ) {
                send_to_char( "Look in what?\n\r", ch );
                return;
            }

            if ( ( obj = get_obj_here( ch, arg2 ) ) == NULL ) {
                send_to_char( "You do not see that here.\n\r", ch );
                return;
            }
        }

        // look for
        if (!str_cmp(arg1,"for")) {
            if ( arg2[0] == '\0' ) {
                send_to_char( "Look for what?\n\r", ch );
                return;
            }
            if ( arg5[0] == '\0' ) {
                send_to_char( "Look in what?\n\r", ch );
                return;
            }

            if ( ( obj = get_obj_here( ch, arg5 ) ) == NULL ) {
                send_to_char( "You do not see that here.\n\r", ch );
                return;
            }

            prefix=arg2;
        }

        look_in(ch,obj,prefix);
        return;
    }

    //
    // LOOK THROUGH <object>
    //
    if (!isread && !isexamine &&
            !str_prefix(arg1,"through")) {

        if ( arg2[0] == '\0' ) {
            send_to_char( "Look through what?\n\r", ch );
            return;
        }

        if ( ( obj = get_obj_here( ch, arg2 ) ) == NULL ) {
            send_to_char( "You do not see that here.\n\r", ch );
            return;
        }
        look_through(ch,obj);
        return;
    }

    //
    // LOOK <person>
    //
    if (!isread && ( victim = get_char_room( ch, arg1 ) ) != NULL )
    {
        if ( MOB_HAS_TRIGGER( victim, MTRIG_PRELOOKAT ) )
            if (mp_prelookat_trigger( victim, ch )==FALSE)
                return;
        if (!IS_VALID(victim)) return;
        if (!IS_VALID(ch)) return;
        show_char_to_char_1( victim, ch );
        if ( MOB_HAS_TRIGGER( victim, MTRIG_LOOKAT ) )
            mp_lookat_trigger( victim, ch );
        // duh!
        //	if (!IS_VALID(victim)) return;
        //	if (!IS_VALID(ch)) return;
        return;
    }
    // also check for extra descr on mobs
    if (!isread)
        for (victim=ch->in_room->people;victim;victim=victim->next_in_room) {
            if ((pdesc=get_extra_descr(arg1,victim->extra_descr)) ||
                    (IS_NPC(victim) && (pdesc=get_extra_descr(arg1,victim->pIndexData->extra_descr)))) {
                if ( MOB_HAS_TRIGGER( victim, MTRIG_PRELOOKAT ) )
                    if (mp_prelookat_trigger( victim, ch )==FALSE)
                        return;
                if (!IS_VALID(victim) || !IS_VALID(ch)) return;
                send_to_char( pdesc->description, ch );
                if ( MOB_HAS_TRIGGER( victim, MTRIG_LOOKAT ) )
                    mp_lookat_trigger( victim, ch );
                return;
            }

        }

    //
    // LOOK <direction>
    //
    if (!isread) {
        int by_name=0;
        door = get_direction(arg1);
        if (door<0) {
            for (door=0;door<MAX_DIR;door++) {
                if (ch->in_room->exit[door] &&
                        !str_prefix(arg1,ch->in_room->exit[door]->keyword)) {
                    by_name=1;
                    break;
                }
            }
            if (door>=MAX_DIR) door=-1;
        }
        if(door!=-1) {
            /* 'look direction' */
            if ( ( pexit = ch->in_room->exit[door] ) == NULL ) {
                send_to_char( "Nothing special there.\n\r", ch );
                return;
            }

            if ( pexit->description != NULL && pexit->description[0] != '\0' &&
                 ( by_name || !IS_SET(pexit->exit_info, EX_SECRET)) )
                send_to_char( pexit->description, ch );
            else
                send_to_char( "Nothing special there.\n\r", ch );

            if ( pexit->keyword    != NULL
                 &&   pexit->keyword[0] != '\0'
                 &&   pexit->keyword[0] != ' '
                 &&   !IS_SET(pexit->exit_info, EX_SECRET) ) {
                if ( IS_SET(pexit->exit_info, EX_CLOSED) )
                    act("The $d is closed.", ch, NULL, pexit->keyword, TO_CHAR );
                else if ( IS_SET(pexit->exit_info, EX_ISDOOR) )
                    act("The $d is open.",   ch, NULL, pexit->keyword, TO_CHAR );
            }

            return;
        }
    }

    //
    // LOOK <object> [ON <char>] (in inventory)
    // LOOK <object> [IN <container>]
    //
    obj_list=NULL;
    container=NULL;
    victim=NULL;
    if (str_cmp(arg2,"on")==0) {
        if (arg4[0]==0) {
            send_to_char("Look for what on who?\n\r",ch);
            return;
        }
        if ((victim = get_char_room( ch, arg4 ) ) == NULL ) {
            send_to_char("You see no person around with that name.\n\r",ch);
            return;
        }
        obj_list=victim->carrying;
    } else if (str_cmp(arg2,"in")==0) {
        if (arg4[0]==0) {
            send_to_char("Look for what on what?\n\r",ch);
            return;
        }
        if ((container = get_obj_here( ch, arg4 ) ) == NULL ) {
            send_to_char("You can't find that.\n\r",ch);
            return;
        }
        obj_list=container->contains;
    } else {
        victim=ch;
        obj_list=victim->carrying;
    }

    for ( obj = obj_list; obj != NULL; obj = obj->next_content ) {
        if ( can_see_obj( ch, obj ) ) {  // player can see object
            pdesc = get_extra_descr( arg3, obj->extra_descr );
            if ( pdesc != NULL ) {
                if (++count == number) {
                    if ( isread && OBJ_HAS_TRIGGER( obj, OTRIG_PREREAD ) )
                        if (op_preread_trigger( obj, ch )==FALSE)
                            return;
                    if (!IS_VALID(obj))
                        return;
                    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRELOOKAT_ED ) ) {
                        if (op_prelookat_ed_trigger( obj, ch, pdesc->keyword )==FALSE)
                            return;
                    } else {
                        if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRELOOKAT ) )
                            if (op_prelookat_trigger( obj, ch )==FALSE)
                                return;
                    }
                    if (!IS_VALID(obj))
                        return;

                    if (ch!=victim && victim!=NULL) {
                        act("You look at $p carried by $N:",ch,obj,victim,TO_CHAR);
                        act("$n looks at $p carried by $N.",ch,obj,victim,TO_NOTVICT);
                        act("$n looks at $p you're carrying.",ch,obj,victim,TO_VICT);
                    } else if (container) {
                        act("You look at $p in $P:",ch,obj,container,TO_CHAR);
                        act("$n looks at $p in $P.",ch,obj,container,TO_NOTVICT);
                    }

#ifdef MXP_IMAGE
                    if (HAS_MXP(ch) && obj->pIndexData->pueblo_picture[0]) {
                        sprintf_to_char(ch,
                                        MXP"<image url=\"%s\" align=\"right\">",
                                        url(buf,obj->pIndexData->pueblo_picture,
                                            obj->pIndexData->area,FALSE));
                    }
#endif

                    if (isexamine && (ch==victim || container))
                        lore( ch, obj, TRUE );
                    send_to_char( pdesc->description, ch );
                    // display picture of object
                    if (HAS_PUEBLO(ch) && obj->pIndexData->pueblo_picture[0]) {
                        sprintf_to_char(ch,"</xch_mudtext>"
                                           "<img src=\"%s\" align=\"right\"><xch_mudtext>",
                                        url(buf,obj->pIndexData->pueblo_picture,
                                            obj->pIndexData->area,FALSE));
                    }

                    if (obj_is_owned_by_another(ch,obj))
                        sprintf_to_char(ch,
                                        "This object belongs to %s.\n\r",Capitalize(obj->owner_name));

                    if ( isread && OBJ_HAS_TRIGGER( obj, OTRIG_READ ) )
                        op_read_trigger( obj, ch );
                    if (!IS_VALID(obj))
                        return;
                    if ( OBJ_HAS_TRIGGER( obj, OTRIG_LOOKAT_ED ) )
                        op_lookat_ed_trigger( obj, ch, pdesc->keyword );
                    else if ( OBJ_HAS_TRIGGER( obj, OTRIG_LOOKAT ) )
                        op_lookat_trigger( obj, ch );

                    return;
                }
                else continue;
            }

            pdesc = get_extra_descr( arg3, obj->pIndexData->extra_descr );
            if ( pdesc != NULL ) {
                if (++count == number)
                {
                    if ( isread && OBJ_HAS_TRIGGER( obj, OTRIG_PREREAD ) )
                        if (op_preread_trigger( obj, ch )==FALSE)
                            return;
                    if (!IS_VALID(obj))
                        return;
                    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRELOOKAT_ED ) ) {
                        if (op_prelookat_ed_trigger( obj, ch, pdesc->keyword )==FALSE)
                            return;
                    } else {
                        if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRELOOKAT ) )
                            if (op_prelookat_trigger( obj, ch )==FALSE)
                                return;
                    }
                    if (!IS_VALID(obj))
                        return;

                    if (ch!=victim && victim!=NULL) {
                        act("You look at $p on $N:",ch,obj,victim,TO_CHAR);
                        act("$n looks at $p on $N.",ch,obj,victim,TO_NOTVICT);
                        act("$n looks at $p on you.",ch,obj,victim,TO_VICT);
                    } else if (container) {
                        act("You look at $p in $P:",ch,obj,container,TO_CHAR);
                        act("$n looks at $p in $P.",ch,obj,container,TO_NOTVICT);
                    }
                    if (isexamine && ( ch==victim || container) )
                        lore( ch, obj, TRUE );
                    send_to_char( pdesc->description, ch );
                    // display picture of object
                    if (HAS_PUEBLO(ch) && obj->pIndexData->pueblo_picture[0]) {
                        sprintf_to_char(ch,"</xch_mudtext>"
                                           "<img src=\"%s\" align=\"right\"><xch_mudtext>",
                                        url(buf,obj->pIndexData->pueblo_picture,
                                            obj->pIndexData->area,FALSE));
                    }

                    if (obj_is_owned_by_another(ch,obj))
                        sprintf_to_char(ch,
                                        "This object belongs to %s.\n\r",Capitalize(obj->owner_name));
#ifdef MXP_IMAGE
                    if (HAS_MXP(ch) && obj->pIndexData->pueblo_picture[0]) {
                        sprintf_to_char(ch,
                                        MXP"<image url=\"%s\" align=\"right\">",
                                        url(buf,obj->pIndexData->pueblo_picture,
                                            obj->pIndexData->area,FALSE));
                    }
#endif

                    if ( isread && OBJ_HAS_TRIGGER( obj, OTRIG_READ ) )
                        op_read_trigger( obj, ch );
                    if (!IS_VALID(obj))
                        return;
                    if ( OBJ_HAS_TRIGGER( obj, OTRIG_LOOKAT_ED ) )
                        op_lookat_ed_trigger( obj, ch, pdesc->keyword );
                    else if ( OBJ_HAS_TRIGGER( obj, OTRIG_LOOKAT ) )
                        op_lookat_trigger( obj, ch );

                    return;
                }
                else continue;
            }

            if ( is_name( arg3, obj->name ) )
                if (++count == number)
                {
                    if ( isread && OBJ_HAS_TRIGGER( obj, OTRIG_PREREAD ) )
                        if (op_preread_trigger( obj, ch )==FALSE)
                            return;
                    if (!IS_VALID(obj))
                        return;
                    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRELOOKAT ) )
                        if (op_prelookat_trigger( obj, ch )==FALSE)
                            return;
                    if (!IS_VALID(obj))
                        return;

                    if (ch!=victim && victim!=NULL) {
                        act("You look at $p on $N:",ch,obj,victim,TO_CHAR);
                        act("$n looks at $p on $N.",ch,obj,victim,TO_NOTVICT);
                        act("$n looks at $p on you.",ch,obj,victim,TO_VICT);
                    } else if (container) {
                        act("You look at $p in $P:",ch,obj,container,TO_CHAR);
                        act("$n looks at $p in $P.",ch,obj,container,TO_NOTVICT);
                    }
                    if (isexamine && (ch==victim || container))
                        lore( ch, obj, TRUE );
                    send_to_char( obj->description, ch );
                    send_to_char( "\n\r",ch);
                    // display picture of object
                    if (HAS_PUEBLO(ch) && obj->pIndexData->pueblo_picture[0]) {
                        sprintf_to_char(ch,"</xch_mudtext>"
                                           "<img src=\"%s\" align=\"right\"><xch_mudtext>",
                                        url(buf,obj->pIndexData->pueblo_picture,
                                            obj->pIndexData->area,FALSE));
                    }

                    if (obj_is_owned_by_another(ch,obj))
                        sprintf_to_char(ch,
                                        "This object belongs to %s.\n\r",Capitalize(obj->owner_name));
#ifdef MXP_IMAGE
                    if (HAS_MXP(ch) && obj->pIndexData->pueblo_picture[0]) {
                        sprintf_to_char(ch,
                                        MXP"<image url=\"%s\" align=\"right\">",
                                        url(buf,obj->pIndexData->pueblo_picture,
                                            obj->pIndexData->area,FALSE));
                    }
#endif

                    if ( isread && OBJ_HAS_TRIGGER( obj, OTRIG_READ ) )
                        op_read_trigger( obj, ch );
                    if (!IS_VALID(obj))
                        return;
                    if ( OBJ_HAS_TRIGGER( obj, OTRIG_LOOKAT ) )
                        op_lookat_trigger( obj, ch );

                    return;
                }
        }
    }

    //
    // LOOK <object> (in room)
    //
    for ( obj = ch->in_room->contents; obj != NULL; obj = obj->next_content )
    {
        if ( can_see_obj( ch, obj ) ) {
            pdesc = get_extra_descr( arg3, obj->extra_descr );
            if ( pdesc != NULL )
                if (++count == number) {

                    if ( isread && OBJ_HAS_TRIGGER( obj, OTRIG_PREREAD ) )
                        if (op_preread_trigger( obj, ch )==FALSE)
                            return;
                    if (!IS_VALID(obj))
                        return;
                    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRELOOKAT_ED ) ) {
                        if (op_prelookat_ed_trigger( obj, ch, pdesc->keyword )==FALSE)
                            return;
                    } else {
                        if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRELOOKAT ) )
                            if (op_prelookat_trigger( obj, ch )==FALSE)
                                return;
                    }
                    if (!IS_VALID(obj))
                        return;

                    if( isexamine )
                        lore( ch, obj, FALSE );
                    send_to_char( pdesc->description, ch );
                    // display picture of object
                    if (HAS_PUEBLO(ch) && obj->pIndexData->pueblo_picture[0]) {
                        sprintf_to_char(ch,"</xch_mudtext>"
                                           "<img src=\"%s\" align=\"right\"><xch_mudtext>",
                                        url(buf,obj->pIndexData->pueblo_picture,
                                            obj->pIndexData->area,FALSE));
                    }
                    if (obj_is_owned_by_another(ch,obj))
                        sprintf_to_char(ch,
                                        "This object belongs to %s.\n\r",Capitalize(obj->owner_name));
#ifdef MXP_IMAGE
                    if (HAS_MXP(ch) && obj->pIndexData->pueblo_picture[0]) {
                        sprintf_to_char(ch,
                                        MXP"<image url=\"%s\" align=\"right\">",
                                        url(buf,obj->pIndexData->pueblo_picture,
                                            obj->pIndexData->area,FALSE));
                    }
#endif

                    if ( isread && OBJ_HAS_TRIGGER( obj, OTRIG_READ ) )
                        op_read_trigger( obj, ch );
                    if (!IS_VALID(obj))
                        return;
                    if ( OBJ_HAS_TRIGGER( obj, OTRIG_LOOKAT_ED ) )
                        op_lookat_ed_trigger( obj, ch, pdesc->keyword );
                    else if ( OBJ_HAS_TRIGGER( obj, OTRIG_LOOKAT ) )
                        op_lookat_trigger( obj, ch );

                    return;
                }

            pdesc = get_extra_descr( arg3, obj->pIndexData->extra_descr );
            if ( pdesc != NULL )
                if (++count == number) {

                    if ( isread && OBJ_HAS_TRIGGER( obj, OTRIG_PREREAD ) )
                        if (op_preread_trigger( obj, ch )==FALSE)
                            return;
                    if (!IS_VALID(obj))
                        return;
                    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRELOOKAT_ED ) ) {
                        if (op_prelookat_ed_trigger( obj, ch, pdesc->keyword )==FALSE)
                            return;
                    } else {
                        if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRELOOKAT ) )
                            if (op_prelookat_trigger( obj, ch )==FALSE)
                                return;
                    }
                    if (!IS_VALID(obj))
                        return;

                    if( isexamine )
                        lore( ch, obj, FALSE );
                    send_to_char( pdesc->description, ch );
                    // display picture of object
                    if (HAS_PUEBLO(ch) && obj->pIndexData->pueblo_picture[0]) {
                        sprintf_to_char(ch,"</xch_mudtext>"
                                           "<img src=\"%s\" align=\"right\"><xch_mudtext>",
                                        url(buf,obj->pIndexData->pueblo_picture,
                                            obj->pIndexData->area,FALSE));
                    }

                    if (obj_is_owned_by_another(ch,obj))
                        sprintf_to_char(ch,
                                        "This object belongs to %s.\n\r",Capitalize(obj->owner_name));
#ifdef MXP_IMAGE
                    if (HAS_MXP(ch) && obj->pIndexData->pueblo_picture[0]) {
                        sprintf_to_char(ch,
                                        MXP"<image url=\"%s\" align=\"right\">",
                                        url(buf,obj->pIndexData->pueblo_picture,
                                            obj->pIndexData->area,FALSE));
                    }
#endif

                    if ( isread && OBJ_HAS_TRIGGER( obj, OTRIG_READ ) )
                        op_read_trigger( obj, ch );
                    if (!IS_VALID(obj))
                        return;
                    if ( OBJ_HAS_TRIGGER( obj, OTRIG_LOOKAT_ED ) )
                        op_lookat_ed_trigger( obj, ch, pdesc->keyword );
                    else if ( OBJ_HAS_TRIGGER( obj, OTRIG_LOOKAT ) )
                        op_lookat_trigger( obj, ch );

                    return;
                }

            if ( is_name( arg3, obj->name ) )
                if (++count == number) {

                    if ( isread && OBJ_HAS_TRIGGER( obj, OTRIG_PREREAD ) )
                        if (op_preread_trigger( obj, ch )==FALSE)
                            return;
                    if (!IS_VALID(obj))
                        return;
                    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRELOOKAT ) )
                        if (op_prelookat_trigger( obj, ch )==FALSE)
                            return;
                    if (!IS_VALID(obj))
                        return;

                    if( isexamine )
                        lore( ch, obj, FALSE );
                    send_to_char( obj->description, ch );
                    send_to_char("\n\r",ch);
                    // display picture of object
                    if (HAS_PUEBLO(ch) && obj->pIndexData->pueblo_picture[0]) {
                        sprintf_to_char(ch,"</xch_mudtext>"
                                           "<img src=\"%s\" align=\"right\"><xch_mudtext>",
                                        url(buf,obj->pIndexData->pueblo_picture,
                                            obj->pIndexData->area,FALSE));
                    }

                    if (obj_is_owned_by_another(ch,obj))
                        sprintf_to_char(ch,
                                        "This object belongs to %s.\n\r",Capitalize(obj->owner_name));
#ifdef MXP_IMAGE
                    if (HAS_MXP(ch) && obj->pIndexData->pueblo_picture[0]) {
                        sprintf_to_char(ch,
                                        MXP"<image url=\"%s\" align=\"right\">",
                                        url(buf,obj->pIndexData->pueblo_picture,
                                            obj->pIndexData->area,FALSE));
                    }
#endif
                    if ( isread && OBJ_HAS_TRIGGER( obj, OTRIG_READ ) )
                        op_read_trigger( obj, ch );
                    if (!IS_VALID(obj))
                        return;
                    if ( OBJ_HAS_TRIGGER( obj, OTRIG_LOOKAT ) )
                        op_lookat_trigger( obj, ch );

                    return;
                }
        }
    }

    pdesc = get_extra_descr(arg3,ch->in_room->extra_descr);
    if (pdesc != NULL)
    {
        if (++count == number)
        {
            if ( ROOM_HAS_TRIGGER( ch->in_room, RTRIG_PRELOOK_ED ) )
                if (rp_prelook_ed_trigger( ch->in_room, ch, pdesc->keyword)==FALSE)
                    return;
            send_to_char(pdesc->description,ch);
            if (ROOM_HAS_TRIGGER( ch->in_room, RTRIG_LOOK_ED ) )
                rp_look_ed_trigger( ch->in_room, ch, pdesc->keyword);
            return;
        }
    }

    if (count > 0 && count != number)
    {
        if (count == 1)
            sprintf_to_char(ch,"You only see one %s here.\n\r",arg3);
        else
            sprintf_to_char(ch,"You only see %d of those here.\n\r",count);
        return;
    }

    send_to_char( "You do not see that here.\n\r", ch );
    return;
}

/* RT added back for the hell of it */
void do_read (CHAR_DATA *ch, char *argument )
{
    if (argument[0]==0) {
        send_to_char("Read what?\n\r",ch);
        return;
    }
    look( ch, argument, FALSE, TRUE );
}

void do_examine( CHAR_DATA *ch, char *argument )
{
    char buf[MAX_STRING_LENGTH];
    char arg[MAX_INPUT_LENGTH];
    OBJ_DATA *obj;

    one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
        send_to_char( "Examine what?\n\r", ch );
        return;
    }

    /* we don't call do_look() any more, instead call look() with
     TRUE meaning we want any objects to be lore()d too */

    look( ch, argument, TRUE, FALSE );

    if ( ( obj = get_obj_here( ch, arg ) ) != NULL )
    {
        switch ( obj->item_type )
        {
        default:
            break;

#ifdef HAS_JUKEBOX
        case ITEM_JUKEBOX:
            do_function(ch, &do_play, "list");
            break;
#endif

        case ITEM_MONEY:
            if (obj->value[0]==0 && obj->value[1]==0)
                sprintf(buf,"Odd...there's no coins in the pile.\n\r");
            if (obj->value[0]==1 && obj->value[1]==0)
                sprintf(buf,"Wow. One silver coin.\n\r");
            if (obj->value[0]==0 && obj->value[1]==1)
                sprintf(buf,"Wow. One gold coin.\n\r");
            if (obj->value[0]>1 || obj->value[1]>1)
                sprintf(buf,
                        "There are %s in the pile.\n\r",
                        money_string(obj->value[1],obj->value[0]));
            send_to_char(buf,ch);
            break;

        case ITEM_DRINK_CON:
        case ITEM_CONTAINER:
        case ITEM_CORPSE_NPC:
        case ITEM_CORPSE_PC:
            look_in(ch,obj,NULL);
        }
    }

    return;
}



/*
 * Thanks to Zrin for auto-exit part.
 */
void do_exits( CHAR_DATA *ch, char *argument ) {
    if ( !check_blind( ch,TRUE ) )
        return;

    show_exits(ch,!str_cmp( argument, "auto" ));
}
void show_exits( CHAR_DATA *ch, bool fAuto )
{
    extern char * const dir_name[];
    char buf[MAX_STRING_LENGTH];
    EXIT_DATA *pexit;
    bool found;
    int door;

    if (fAuto) {
        if (HAS_MXP(ch))
            strcpy(buf,"\n\r" MXP_SECURE "<RExits>[Exits:{g");
        else
            strcpy(buf,"\n\r[Exits:{g");
    } else {
        if (IS_IMMORTAL(ch) && STR_IS_SET(ch->strbit_act,PLR_HOLYLIGHT))
            sprintf(buf,"Obvious exits from room %d:\n\r",ch->in_room->vnum);
        else
            strcpy(buf,"Obvious exits:\n\r");
    }

    found = FALSE;
    for ( door = 0; door < DIR_MAX; door++ )
    {
        if ( ( pexit = ch->in_room->exit[door] ) != NULL
             &&   pexit->to_room != NULL
             &&   can_see_room(ch,pexit->to_room) )
        {
            found = TRUE;
            if ( fAuto )
            {
                if (!IS_SET(pexit->exit_info,EX_CLOSED)) {
                    if (HAS_MXP(ch)) {
                        strcat( buf, " <Ex>" );
                        strcat( buf, dir_name[door] );
                        strcat( buf, "</Ex>" );
                    } else {
                        strcat( buf, " " );
                        strcat( buf, dir_name[door] );
                    }
                }
            } else {
                if (IS_SET(pexit->exit_info,EX_SECRET) &&
                        IS_SET(pexit->exit_info,EX_CLOSED) &&
                        !STR_IS_SET(ch->strbit_act,PLR_HOLYLIGHT))
                    continue;
                sprintf( buf + strlen(buf), "%-5s - %s",
                         capitalize( dir_name[door] ),
                         IS_SET(pexit->exit_info,EX_CLOSED)?
                             "A closed entry":
                             room_is_dark( pexit->to_room )
                             ?  "Too dark to tell"
                              : pexit->to_room->name
                                );
                if (IS_IMMORTAL(ch) && STR_IS_SET(ch->strbit_act,PLR_HOLYLIGHT))
                    sprintf(buf + strlen(buf),
                            " (room %d)\n\r",pexit->to_room->vnum);
                else
                    strcat(buf, "\n\r");
            }
        }
    }

    if ( !found )
        strcat( buf, fAuto ? " none" : "None.\n\r" );

    if ( fAuto ) {
        if (HAS_MXP(ch))
            strcat( buf, "{x]</RExits>\n\r" );
        else
            strcat( buf, "{x]\n\r" );
    }

    send_to_char( buf, ch );
}

void do_peek ( CHAR_DATA *ch, char *argument )
{
    ROOM_INDEX_DATA *tRoom;
    EXIT_DATA *pexit;
    int door;
    CHAR_DATA *victim;

    if ( ch->desc == NULL )
        return;

    if(IS_NPC(ch)) return;

    if(!has_skill_available(ch,gsn_peek)) {
        send_to_char("You don't know how to peek.\n\r",ch);
        return;
    }

    if ( !check_blind( ch,TRUE ) )
        return;

    door = get_direction(argument);
    if (door>=0) {
        if (ch->position<POS_STANDING) {
            send_to_char("Better stand up first.\n\r",ch);
            return;
        }

        if(!skillcheck(ch,gsn_peek)) {
            sprintf_to_char(ch,"You failed to peek %s.\n\r",dir_name[door]);
            return;
        }

        act("You peek $T.", ch, NULL, dir_name[door], TO_CHAR);
        act("$n peeks $T.", ch, NULL, dir_name[door], TO_ROOM);

        if ( ( pexit = ch->in_room->exit[door] ) == NULL ) {
            send_to_char( "You see nothing special there.\n\r", ch );
            return;
        }

        if ( pexit->description != NULL && pexit->description[0] != '\0' )
            send_to_char( pexit->description, ch );
        else
            send_to_char( "Nothing special there.\n\r", ch );

        if ( pexit->keyword    != NULL
             &&   pexit->keyword[0] != '\0'
             &&   pexit->keyword[0] != ' ' ) {
            if ( IS_SET(pexit->exit_info, EX_CLOSED) ) {
                act( "The $d is closed.", ch, NULL, pexit->keyword, TO_CHAR );
                return;
            } else if ( IS_SET(pexit->exit_info, EX_ISDOOR) )
                act( "The $d is open.",   ch, NULL, pexit->keyword, TO_CHAR );
        }

        if((tRoom=exit_target(ch,pexit))) {
            look_room(ch,tRoom,FALSE);
        }
        return;
    }

    if (ch->position<POS_RESTING) {
        send_to_char("Better stand up first.\n\r",ch);
        return;
    }
    if (ch->position==POS_FIGHTING) {
        send_to_char("You're too busy!\n\r",ch);
        return;
    }

    victim=get_char_room(ch,argument);
    if ( victim && victim != ch &&   !IS_NPC(ch)) {
        if (skillcheck(ch,gsn_peek)) {
            act( "You peek at $S inventory:\n\r",ch,NULL,victim,TO_CHAR);
            check_improve(ch,gsn_peek,TRUE,4);
            show_list_to_char( victim->carrying, ch, NULL, TRUE, TRUE );
        } else {
            act("You failed to look in $S inventory.",ch,NULL,victim,TO_CHAR);
        }
        return;
    }


    send_to_char("Peek at who or in which direction?\n\r",ch);
    return;
}

void do_worth( CHAR_DATA *ch, char *argument ) {
    if (IS_NPC(ch)) {
        sprintf_to_char(ch,
                        "You have %s.\n\r",money_string(ch->gold,ch->silver));
        return;
    }

    sprintf_to_char(ch,
                    "You have %s, and %d experience (%d exp to level).\n\r",
                    money_string(ch->gold,ch->silver),ch->exp,exp_to_level(ch));
    return;
}

void concat_penalty(char *buf,CHAR_DATA *ch,int flag) {
    if (STR_IS_SET(ch->strbit_comm,flag)) {
        int pos=strlen(buf);
        if (pos!=0) buf[pos++]=' ';
        strncpy(buf+pos,option_find_value(flag,comm_options),MAX_INPUT_LENGTH-pos-1);
        buf[MAX_INPUT_LENGTH-1]=0;
    }
}

void do_score2 (CHAR_DATA *ch,char *argument) {
    CHAR_DATA *victim;
    char arg1[MAX_INPUT_LENGTH];
    char *s,*ss[4];
    int i;
    int been=0;
    char *message_hunger="n/a";
    char *message_thirst="n/a";
    char *message_excited="n/a";
    char *message_drunk="n/a";

    argument = one_argument( argument, arg1 );
    if (arg1[0]) {
        if (get_trust(ch)<LEVEL_IMMORTAL)
            victim=ch;
        else {
            victim=get_char_world(ch,arg1);
            if (!victim) {
                send_to_char("No such file or directory.\n\r",ch);
                return;
            }
            if (get_trust(ch)<get_trust(victim)) {
                sprintf_to_char(ch,"You're level %d, victim is"
                                   "level %d, so bad luck.\n\r",
                                get_trust(ch),get_trust(victim));
                return;
            }
        }
    } else
        victim=ch;

    /*
            1         2         3         4         5         6         7
  01234567890123456789012345678901234567890123456789012345678901234567890123456
 0  _________________________________________________________________________
 1 /                                                                     /   \
 2| Name : @@@@@@@@@@@@@@@@@ Level   :  @@@   HARDCORE                  |     |
 3| Race : @@@@@@@@@@@@      Age     :  @@@ years                       |\___/
 4| Class: @@@@@@@@@@@@      Played  : @@@@ hours                       |
 5| Sex  : @@@@@@            Position: @@@@@@@@@@@@@@@@                 |
 6|                                                                     |
 7| Str: @@ (@@)     Experience : @@@@@@       Hit  : @@@@/@@@@         |
 8| Int: @@ (@@)     Next level : @@@@@@       Mana : @@@@/@@@@         |
 9| Wis: @@ (@@)     Questpoints: @@@@@@       Move : @@@@/@@@@         |
10| Dex: @@ (@@)     Practices  : @@@@@@                                |
 1| Con: @@ (@@)     Trainings  : @@@@@@       Wimpy: @@@@ hitpoints    |
 2|                                                                     |
 3| Hitroll: @@@@    Gold       : @@@@@@@      Carrying :    @@@@ items |
 4| Damroll: @@@@    Silver     : @@@@@@@      Weight   : @@@@@@@ lbs   |
 5| Holylight is @@@                           Alignment: @@@@@ @@@@@@@ |
 6|                                                                     |
 7| Piercing: @@@@    Bashing: @@@@    Slashing: @@@@    Magic  : @@@@  |
 8| Piercing: @@@@@@@@@@@@@@@@@@@@@    Bashing:  @@@@@@@@@@@@@@@@@@@@@  |
 9| Slashing: @@@@@@@@@@@@@@@@@@@@@    Magic:    @@@@@@@@@@@@@@@@@@@@@  |
20|                                                                     |
21|/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

10| Dex: @@ (@@)     Practices  : @@@@@@                                |
 1| Con: @@ (@@)     Trainings  : @@@@@@       Wimpy: @@@@ hitpoints    |
 2|                                                                     |
 3| Hitroll: @@@@@@@ Hunger     : @@@@@@@      Carrying :    @@@@ items |
 4| Damroll: @@@@@@@ Thirst     : @@@@@@@      Weight   : @@@@@@@ lbs   |
 5| Gold   : @@@@@@@ Adrenaline : @@@@@@@      Alignment: @@@@@ @@@@@@@ |
 6| Silver : @@@@@@@ Drunk      : @@@@@@@      Holylight: @@@           |
 7|                                                                     |
 8| Piercing: @@@@@@@@@@@@@@@@@@@@@    Bashing:  @@@@@@@@@@@@@@@@@@@@@  |
 9| Slashing: @@@@@@@@@@@@@@@@@@@@@    Magic:    @@@@@@@@@@@@@@@@@@@@@  |
*/

    send_to_char("  _________________________________________________________________________\n\r",ch);
    send_to_char(" /                                                                     /   \\\n\r",ch);
    sprintf_to_char(ch,"| Name : %-12s      Level   :  {g%4d{x   {R%8s{x                 |     |\n\r",
                    IS_NPC(victim)?victim->short_descr:victim->name,victim->level,STR_IS_SET(victim->strbit_act,PLR_HARDCORE)?"HARDCORE":"");
    sprintf_to_char(ch,"| Race : %-12s      Age     :  %4d years                      |\\___/\n\r",
                    race_table[victim->race].name,get_age(victim));
    sprintf_to_char(ch,"| Class: %-12s      Played  :  %4d hours                      |\n\r",
                    IS_NPC(victim)?"mobile":class_table[victim->class].name,
                    (victim->played+(int)(current_time-victim->logon))/3600);
    i=UMAX(0,31-strlen(position_table[victim->position].name)-
            (victim->on==NULL?0:
                              3+strlen(victim->on->short_descr)));
    sprintf_to_char(ch,"| Sex  : %-6s            Position: %s %s%s %*s%s\n\r",
                    table_find_value(show_sex(victim),sex_table),
                    position_table[victim->position].name,
            victim->on==NULL?"":"on ",
            victim->on==NULL?"":victim->on->short_descr,
            UMAX(0,i),"",i<0?"":"|");

    send_to_char("|                                                                     |\n\r",ch);
    sprintf_to_char(ch,"| Str: %2d (%2d)     Experience : {g%6d{x       Hit  : {g%5d{x/{g%5d{x       |\n\r",
                    victim->perm_stat[STAT_STR],get_curr_stat(victim,STAT_STR),
                    IS_NPC(victim)?0:victim->exp,
                    victim->hit,victim->max_hit);
    sprintf_to_char(ch,"| Int: %2d (%2d)     Next level : %6d       Mana : {g%5d{x/{g%5d{x       |\n\r",
                    victim->perm_stat[STAT_INT],get_curr_stat(victim,STAT_INT),
                    IS_NPC(victim)?0:exp_to_level(victim),
                    victim->mana,victim->max_mana);
    sprintf_to_char(ch,"| Wis: %2d (%2d)     Questpoints: %6d       Move : {g%5d{x/{g%5d{x       |\n\r",
                    victim->perm_stat[STAT_WIS],get_curr_stat(victim,STAT_WIS),
                    IS_NPC(victim)?0:victim->pcdata->questpoints,
                    victim->move,victim->max_move);
    sprintf_to_char(ch,"| Dex: %2d (%2d)     Practices  : {g%6d{x       %-16s%s     |\n\r",
                    victim->perm_stat[STAT_DEX],get_curr_stat(victim,STAT_DEX),
                    victim->practice,
                    IS_NPC(victim)?"":STR_IS_SET(race_table[victim->race].strbit_eff,EFF_SNEAK)?STR_IS_SET(victim->strbit_affected_by,EFF_SNEAK)?"You are sneaking":"You are not sneaking":"",
            IS_NPC(victim)?"    ":STR_IS_SET(race_table[victim->race].strbit_eff,EFF_SNEAK)?STR_IS_SET(victim->strbit_affected_by,EFF_SNEAK)?"    ":"":"    "
                                                                                                                                             );
    sprintf_to_char(ch,"| Con: %2d (%2d)     Training   : {g%6d{x       Wimpy: %4d hitpoints    |\n\r",
                    victim->perm_stat[STAT_CON],get_curr_stat(victim,STAT_CON),
                    victim->train,victim->wimpy);

    if (!IS_NPC(victim)) {
        if (victim->pcdata->condition[COND_HUNGER]<4) {
            message_hunger="very";
        } else if (victim->pcdata->condition[COND_HUNGER]<8) {
            message_hunger="little";
        } else {
            message_hunger="none";
        }

        if (victim->pcdata->condition[COND_DRUNK]==0) {
            message_drunk="not";
        } else if (victim->pcdata->condition[COND_DRUNK]<4) {
            message_drunk="little";
        } else if (victim->pcdata->condition[COND_DRUNK]<8) {
            message_drunk="tipsy";
        } else {
            message_drunk="totally";
        }

        if (victim->pcdata->condition[COND_THIRST]<4) {
            message_thirst="very";
        } else if (victim->pcdata->condition[COND_THIRST]<8) {
            message_thirst="little";
        } else {
            message_thirst="none";
        }

        if (victim->pcdata->condition[COND_ADRENALINE]<1) {
            message_excited="none";
        } else if (victim->pcdata->condition[COND_ADRENALINE]<3) {
            message_excited="little";
        } else if (victim->pcdata->condition[COND_ADRENALINE]<5) {
            message_excited="rather";
        } else if (victim->pcdata->condition[COND_ADRENALINE]<8) {
            message_excited="high";
        } else {
            message_excited="very";
        }
    }

    send_to_char("|                                                                     |\n\r",ch);
    sprintf_to_char(ch,"| Hitroll: %-6d  Hunger     : %-7s      Carrying :    %4d items |\n\r",
                    GET_HITROLL(victim),message_hunger,victim->carry_number);
    sprintf_to_char(ch,"| Damroll: %-6d  Thirst     : %-7s      Weight   : %7d lbs   |\n\r",
                    GET_DAMROLL(victim),message_thirst,get_carry_weight(victim)/10);
    if ( victim->alignment >  900 ) s="{yangelic{x";
    else if ( victim->alignment >  700 ) s="{ysaintly{x";
    else if ( victim->alignment >  350 ) s="good";
    else if ( victim->alignment >  100 ) s="kind";
    else if ( victim->alignment > -100 ) s="neutral";
    else if ( victim->alignment > -350 ) s="mean";
    else if ( victim->alignment > -700 ) s="evil";
    else if ( victim->alignment > -900 ) s="{rdemonic{x";
    else                                 s="{rsatanic{x";
    if (ch->level>=10)
        sprintf_to_char(ch,"| {yGold{x   : {y%6d{x  Adrenaline : %-7s      Alignment: %5d %-7s |\n\r",
                        victim->gold,message_excited,URANGE(-1000,victim->alignment,1000),s);
    //	    IS_IMMORTAL(victim)?"Holylight is":"",
    //	    IS_IMMORTAL(victim)?(IS_SET(victim->strbit_act, PLR_HOLYLIGHT)?
    //	    "on":"off"):"",victim->alignment,s);
    else
        sprintf_to_char(ch,"| {yGold{x   : {y%6d{x  Adrenaline : %-7s      Alignment: %5d %-7s |\n\r",
                        victim->gold,message_excited,URANGE(-1000,victim->alignment,1000),"");
    //	    IS_IMMORTAL(victim)?"Holylight is":"",
    //	    IS_IMMORTAL(victim)?(IS_SET(victim->strbit_act, PLR_HOLYLIGHT)?
    //	    "on":"off"):"",s);

    if (IS_PC(victim)) {
        int i;

        for (i=0;i<=MAX_VNUMS;i++)
            if (STR_IS_SET(victim->pcdata->beeninroom,i))
                been++;
        // don't need to check for /0 since this command only works in areas which exist
    } else{
        been=0;
    }

    sprintf_to_char(ch,"| {WSilver{x : {W%6d{x  Drunk      : %-7s      Explored : %3d%%          |\n\r",
                    victim->silver,message_drunk,100*been/room_index_allocated);

    send_to_char("|                                                                     |\n\r",ch);
    if (ch->level>=26) {
        sprintf_to_char(ch,"| Piercing: %-5d   Bashing: %-5d   Slashing: %-5d   Exotic : %-5d |\n\r",
                        GET_AC(victim,0),GET_AC(victim,1),GET_AC(victim,2),GET_AC(victim,3));
    }
    if (STR_IS_SET(ch->strbit_comm,COMM_SHOW_ARMOR)) {
        for (i = 0; i < 4; i++) {
            if (GET_AC(victim,i) >=101)   s="hopelessly vulnerable";
            else if (GET_AC(victim,i) >= 80)   s="defenseless";
            else if (GET_AC(victim,i) >= 60)   s="barely protected";
            else if (GET_AC(victim,i) >= 40)   s="slightly armored";
            else if (GET_AC(victim,i) >= 20)   s="somewhat armored";
            else if (GET_AC(victim,i) >= 0)    s="armored";
            else if (GET_AC(victim,i) >= -20)  s="well-armored";
            else if (GET_AC(victim,i) >= -40)  s="very well-armored";
            else if (GET_AC(victim,i) >= -60)  s="heavily armored";
            else if (GET_AC(victim,i) >= -80)  s="superbly armored";
            else if (GET_AC(victim,i) >= -100) s="almost invulnerable";
            else s="divinely armored";
            ss[i]=s;
        }
        sprintf_to_char(ch,"| Piercing: %-21s    Bashing: %-21s   |\n\r",ss[0],ss[1]);
        sprintf_to_char(ch,"| Slashing: %-21s    Exotic : %-21s   |\n\r",ss[2],ss[3]);
    }
    /* replaced with code in the 'config channels' command
    arg1[0]=0;
    concat_penalty(arg1,ch,COMM_NOEMOTE);
    concat_penalty(arg1,ch,COMM_NOSHOUT);
    concat_penalty(arg1,ch,COMM_NOTELL);
    concat_penalty(arg1,ch,COMM_NOCHANNELS);
    concat_penalty(arg1,ch,COMM_NOCOLOR);

    if (arg1[0]!=0) {
    send_to_char("|                                                                     |\n\r",ch);
    sprintf_to_char(ch,"| Penalties : %-55s |\n\r",arg1);
    }
*/
    send_to_char("|                                                                     |\n\r",ch);
    send_to_char("|/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\n\r",ch);
}


void do_score( CHAR_DATA *ch, char *argument )
{
    sprintf_to_char(ch,
                    "You are %s%s, level {g%d{x, %d years old (%d hours).\n\r",
                    ch->name,
                    IS_NPC(ch) ? "" : ch->pcdata->title,
                    ch->level, get_age(ch),
                    ( ch->played + (int) (current_time - ch->logon) ) / 3600);

    if ( get_trust( ch ) != ch->level )
        sprintf_to_char(ch,"You are trusted at level %d.\n\r", get_trust( ch ) );

    sprintf_to_char(ch,"Race: %s  Sex: %s  Class:  %s\n\r",
                    race_table[ch->race].name,
            table_find_value(show_sex(ch),sex_table),
            IS_NPC(ch) ? "mobile\n\r" : class_table[ch->class].name);

    if (!IS_NPC(ch))
        sprintf_to_char(ch,
                        "You have {g%d/%d{x hit, {g%d/%d{x mana, {g%d/%d{x movement.\n\r",
                        ch->hit,  ch->max_hit,
                        ch->mana, ch->max_mana,
                        ch->move, ch->max_move);

    sprintf_to_char(ch,
                    "You have {g%d{x practices and {g%d{x training sessions.\n\r",
                    ch->practice, ch->train);

    sprintf_to_char(ch,
                    "You are carrying %d/%d items with weight %ld/%d pounds.\n\r",
                    ch->carry_number, can_carry_n(ch),
                    get_carry_weight(ch) / 10, can_carry_w(ch) /10 );

    sprintf_to_char(ch,
                    "{cStr{x: %d(%d)  {cInt{x: %d(%d)  {cWis{x: %d(%d)  {cDex{x: %d(%d)  {cCon{x: %d(%d)\n\r",
                    ch->perm_stat[STAT_STR],
                    get_curr_stat(ch,STAT_STR),
                    ch->perm_stat[STAT_INT],
                    get_curr_stat(ch,STAT_INT),
                    ch->perm_stat[STAT_WIS],
                    get_curr_stat(ch,STAT_WIS),
                    ch->perm_stat[STAT_DEX],
                    get_curr_stat(ch,STAT_DEX),
                    ch->perm_stat[STAT_CON],
                    get_curr_stat(ch,STAT_CON) );

    sprintf_to_char(ch,
                    "You have scored {g%d{x exp, and have %s.\n\r",
                    ch->exp,money_string(ch->gold,ch->silver));

    if( !IS_NPC(ch) )
        sprintf_to_char(ch,"You have %d quest points.\n\r",ch->pcdata->questpoints);

    /* RT shows exp to level */
    if (!IS_NPC(ch) && ch->level < LEVEL_HERO)
        sprintf_to_char(ch,"You need {r%d{x exp to level.\n\r",exp_to_level(ch));

    sprintf_to_char(ch, "Wimpy set to %d hit points.\n\r", ch->wimpy );

    if ( !IS_NPC(ch) && ch->pcdata->condition[COND_DRUNK]   > 10 )
        send_to_char( "You are drunk.\n\r",   ch );
    if ( !IS_NPC(ch) && ch->pcdata->condition[COND_THIRST] ==  0 )
        send_to_char( "You are thirsty.\n\r", ch );
    if ( !IS_NPC(ch) && ch->pcdata->condition[COND_HUNGER]   ==  0 )
        send_to_char( "You are hungry.\n\r",  ch );

    switch ( ch->position )
    {
    case POS_DEAD:
        send_to_char( "You are DEAD!!\n\r",ch ); break;
    case POS_MORTAL:
        send_to_char( "You are mortally wounded.\n\r", ch ); break;
    case POS_INCAP:
        send_to_char( "You are incapacitated.\n\r", ch ); break;
    case POS_STUNNED:
        send_to_char( "You are stunned.\n\r", ch ); break;
    case POS_SLEEPING:
        send_to_char( "You are sleeping.\n\r", ch ); break;
    case POS_RESTING:
        send_to_char( "You are resting.\n\r", ch ); break;
    case POS_SITTING:
        send_to_char( "You are sitting.\n\r", ch ); break;
    case POS_STANDING:
        send_to_char( "You are standing.\n\r", ch ); break;
    case POS_FIGHTING:
        send_to_char( "You are fighting.\n\r", ch ); break;
    }

    /* print AC values */
    if (ch->level >= 25)
        sprintf_to_char(ch,
                        "Armor: pierce: %d  bash: %d  slash: %d  magic: %d\n\r",
                        GET_AC(ch,AC_PIERCE), GET_AC(ch,AC_BASH),
                        GET_AC(ch,AC_SLASH),  GET_AC(ch,AC_EXOTIC));

    if (STR_IS_SET(ch->strbit_comm,COMM_SHOW_ARMOR))
    {
        int i;
        char * const armor_name[]={"piercing","bashing","slashing","magic"};
        char *desc;
        for (i = 0; i < 4; i++)
        {
            if (GET_AC(ch,i) >=  101 ) 	 desc="hopelessly vulnerable to";
            else if (GET_AC(ch,i) >= 80)   desc="defenseless against";
            else if (GET_AC(ch,i) >= 60)   desc="barely protected from";
            else if (GET_AC(ch,i) >= 40)   desc="slightly armored against";
            else if (GET_AC(ch,i) >= 20)   desc="somewhat armored against";
            else if (GET_AC(ch,i) >= 0)    desc="armored against";
            else if (GET_AC(ch,i) >= -20)  desc="well-armored against";
            else if (GET_AC(ch,i) >= -40)  desc="very well-armored against";
            else if (GET_AC(ch,i) >= -60)  desc="heavily armored against";
            else if (GET_AC(ch,i) >= -80)  desc="superbly armored against";
            else if (GET_AC(ch,i) >= -100) desc="almost invulnerable to";
            else desc="divinely armored against";
            sprintf_to_char(ch,"You are %s %s.\n\r",desc,armor_name[i]);
        }
    }

    /* RT wizinvis and holy light */
    if ( IS_IMMORTAL(ch))
    {
        sprintf_to_char(ch, "Holy Light: %s",
                        STR_IS_SET(ch->strbit_act,PLR_HOLYLIGHT) ? "on" : "off");

        if (ch->invis_level)
            sprintf_to_char(ch, "  Invisible: level %d",ch->invis_level);

        if (ch->incog_level)
            sprintf_to_char(ch, "  Incognito: level %d",ch->incog_level);
        send_to_char("\n\r",ch);
    }

    if ( ch->level >= 15 )
        sprintf_to_char(ch, "Hitroll: %d  Damroll: %d.\n\r",
                        GET_HITROLL(ch), GET_DAMROLL(ch) );

    if ( ch->level >= 10 )
        sprintf_to_char(ch, "Alignment: %d.  ", ch->alignment );

    send_to_char( "You are ", ch );
    if ( ch->alignment >  900 ) send_to_char( "angelic.\n\r", ch );
    else if ( ch->alignment >  700 ) send_to_char( "saintly.\n\r", ch );
    else if ( ch->alignment >  350 ) send_to_char( "good.\n\r",    ch );
    else if ( ch->alignment >  100 ) send_to_char( "kind.\n\r",    ch );
    else if ( ch->alignment > -100 ) send_to_char( "neutral.\n\r", ch );
    else if ( ch->alignment > -350 ) send_to_char( "mean.\n\r",    ch );
    else if ( ch->alignment > -700 ) send_to_char( "evil.\n\r",    ch );
    else if ( ch->alignment > -900 ) send_to_char( "demonic.\n\r", ch );
    else                             send_to_char( "satanic.\n\r", ch );

    if (STR_IS_SET(ch->strbit_comm,COMM_SHOW_EFFECTS))
        do_function(ch, &do_effects, "");
}

void do_effects(CHAR_DATA *ch, char *argument )
{
    EFFECT_DATA *pef, *pef_last = NULL;
    char buf[MAX_STRING_LENGTH];
    CHAR_DATA *victim;
    bool ispet=FALSE,found=FALSE;

    one_argument(argument,buf);
    if (buf[0]) {
        victim=get_char_room(ch,buf);
        if (victim==NULL) {
            send_to_char("And whom did you see?\n\r",ch);
            return;
        }
        if (ch->pet==victim || IS_IMMORTAL(ch))
            ispet=TRUE;
        if (!IS_AWAKE(ch)) {
            send_to_char("You should open your eyes first.\n\r",ch);
            return;
        }
    } else
        victim=ch;

    for ( pef = victim->affected; pef != NULL; pef = pef->next )
    {
        if (pef->casted_by!=ch->id && ch!=victim && !ispet) continue;

        if (!found) {
            if (ch==victim)
                send_to_char("{yYou are affected by the following spells.\n\r",ch);
            else if (!IS_IMMORTAL(ch)) {
                if (ch->pet==victim) {
                    act("{y$E is affected by:",ch,NULL,victim,TO_CHAR);
                } else {
                    act("{yYou have affected $M by:",ch,NULL,victim,TO_CHAR);
                }
            } else
                act("{y$E is affected by the following spells:",
                    ch,NULL,victim,TO_CHAR);
        }

        if ((pef_last != NULL && pef->type == pef_last->type) ||
                pef->type==-1)
            if (ch->level >= 20)
                sprintf(buf,"                 ");
            else
                continue;
        else {
            if(pef->type<0 || pef->type>=MAX_SKILL)
                strcpy(buf,"{rYou have found a bug. Please report this to the head coder and/or head builder.{x\n\r");
            else
                sprintf( buf, "{y- {x%-15s", skill_name(pef->type,victim));
        }

        send_to_char( buf, ch );

        if ( ch->level >= 20 ) {
            sprintf_to_char(ch,"{y: modifies {x%s{y by {x%d ",
                            effect_loc_name(pef), pef->modifier);
            if ( pef->duration == -1 )
                sprintf( buf, "{ypermanently" );
            else
                sprintf( buf, "{yfor {x%d {yhours", pef->duration );
            send_to_char( buf, ch );

            if (ch==victim && pef->casted_by==ch->id)
                sprintf_to_char( ch, " {y(cast by {xyou{y)");

        }
        send_to_char( "{x\n\r", ch );
        pef_last = pef;
        found=TRUE;
    }

    if (!found) {
        if (ch==victim)
            send_to_char("You are not affected by any spells.\n\r",ch);
        else if (!IS_IMMORTAL(ch))
            act("You haven't affected $M by any spell.",ch,NULL,victim,TO_CHAR);
        else
            act("$E is not affected by any spell.",ch,NULL,victim,TO_CHAR);
    }

    return;
}



void do_time( CHAR_DATA *ch, char *argument )
{
    char *suf;
    int day,i;
    time_t now,there;
    int days,hours,minutes,seconds;

    day     = time_info.day + 1;

    if ( day > 4 && day <  20 ) suf = "th";
    else if ( day % 10 ==  1       ) suf = "st";
    else if ( day % 10 ==  2       ) suf = "nd";
    else if ( day % 10 ==  3       ) suf = "rd";
    else                             suf = "th";

    sprintf_to_char(ch,
                    "It is %d o'clock %s, Day of %s, %d%s the Month of %s, %d AF.\n\r",
                    (time_info.hour % 12 == 0) ? 12 : time_info.hour %12,
                    time_info.hour >= 12 ? "pm" : "am",
                    day_name[day % 7],
            day, suf,
            month_name[time_info.month],
            time_info.year);
    sprintf_to_char(ch,"%s booted at %s\r",MUDHOST,mud_data.boot_time);

    seconds=difftime(time(NULL),mud_data.boottime);
    days=seconds/86400;seconds%=86400;
    hours=seconds/3600;seconds%=3600;
    minutes=seconds/60;seconds%=60;
    sprintf_to_char(ch,
                    "Uptime is %d days, %d hours, %d minutes and %d seconds\n\r",
                    days,hours,minutes,seconds);



    /* very dirty, very dirty. We assume that the server is located in Europe */
    /* but if timezonefiles are used, the whole thing cleans up very nicely */
    now=time(NULL);
    for (i=0;i<4;i++)
        if (mud_data.time_zones[i][0]) {
            if (mud_data.time_zonenames[i] && mud_data.time_zonenames[i][0]) {
                char *tz;
                char *buf=NULL;
                if ((tz=getenv("TZ"))) buf=strdup(tz);

                setenv("TZ",mud_data.time_zonenames[i],1);
                sprintf_to_char(ch,"The time in %-15s is %-3s\r",
                                mud_data.time_zones[i],ctime(&now));
                if (buf) {
                    setenv("TZ",buf,1);
                    free(buf);
                } else {
                    unsetenv("TZ");
                }
            } else {
                there=now+mud_data.time_offsets[i]*3600;
                sprintf_to_char(ch,"The time in %-15s is %-3s\r",
                                mud_data.time_zones[i],ctime(&there));
            }
        }
}



void do_weather( CHAR_DATA *ch, char *argument )
{
    static char * const sky_look[4] =
    {"cloudless","cloudy","rainy","lit by flashes of lightning"};

    if ( !IS_OUTSIDE(ch) )
    {
        send_to_char( "You can't see the weather indoors.\n\r", ch );
        return;
    }

    sprintf_to_char(ch, "The sky is %s and %s.\n\r",
                    sky_look[weather_info.sky],
            weather_info.change >= 0
            ? "a warm southerly breeze blows" : "a cold northern gust blows");
}


char *get_afk_title(CHAR_DATA *ch)
{
    if(IS_NPC(ch)) return "";
    if(STR_IS_SET(ch->strbit_comm,COMM_AFK) && ch->pcdata->afk_text)
        return ch->pcdata->afk_text;
    else
        return ch->pcdata->title;
}


/* needed by the do_who command */
int do_count ( CHAR_DATA *ch, char *argument, bool html)
{
    int count;
    CHAR_DATA *player;

    if (html)
        count=-1;
    else
        count = 0;

    for ( player = player_list; player != NULL; player = player->next_player )
        if ( html || can_see(ch,player) )
            count++;

    max_on = UMAX(count,max_on);

    if (html)
        return max_on;

    if (max_on == count)
        sprintf_to_char(ch,
                        "There %s %d character%s on, the most so far today.\n\r",
                        count==1?"is":"are",count,count==1?"":"s");
    else
        sprintf_to_char(ch,
                        "There %s %d character%s on, the most on today was %d.\n\r",
                        count==1?"is":"are",count,count==1?"":"s",max_on);

    return 0;
}

//
// New 'who' command originally by Alander of Rivers of Mud.
//
void do_who( CHAR_DATA *ch, char *argument )
{
    char	buf[MAX_STRING_LENGTH];
    char	buf2[MAX_STRING_LENGTH];
    char	wizi[16],incog[16],limbo[16],trust[16];
    char	wname[MAX_STRING_LENGTH];
    BUFFER *	output;
    CHAR_DATA *	wch;
    CLAN_INFO *	pclan=NULL;
    CLAN_INFO *	lonerclan=get_clan_by_name("Loner");
    int		count=0;

    int		iClass;
    int		iRace;
    int		iLevelLower;
    int		iLevelUpper;
    int		nNumber;
    int		nMatch;
    bool	rgfClass[MAX_CLASS];
    bool	rgfRace[MAX_PC_RACE];
    bool	fClassRestrict = FALSE;
    bool	fRaceRestrict = FALSE;
    bool	fImmortalOnly = FALSE;

    /*
     * Set default arguments.
     */
    iLevelLower    = 0;
    iLevelUpper    = MAX_LEVEL;
    for ( iClass = 0; iClass < MAX_CLASS; iClass++ )
        rgfClass[iClass] = FALSE;
    for ( iRace = 0; iRace < MAX_PC_RACE; iRace++ )
        rgfRace[iRace] = FALSE;

    //
    // Parse arguments.
    //
    nNumber = 0;
    while (TRUE) {
        char arg[MAX_STRING_LENGTH];

        argument = one_argument( argument, arg );
        if (arg[0]==0)
            break;

        if ( is_number( arg ) ) {
            switch ( ++nNumber )
            {
            case 1: iLevelLower=atoi(arg); break;
            case 2: iLevelUpper=atoi(arg); break;
            default:
                send_to_char( "Only two level numbers allowed.\n\r", ch );
                return;
            }
            continue;
        }

        if (!str_prefix(arg,"immortals")) {
            fImmortalOnly = TRUE;
            continue;
        }

        //
        // Look for classes to turn on.
        //
        iClass = class_lookup(arg);
        if (iClass >= 0) {
            fClassRestrict = TRUE;
            rgfClass[iClass] = TRUE;
            continue;
        }

        iRace = race_lookup(arg);
        if (iRace != 0 && iRace < MAX_PC_RACE) {
            fRaceRestrict = TRUE;
            rgfRace[iRace] = TRUE;
            continue;
        }

        {
            CLAN_INFO *	c=get_clan_by_name(arg);

            if (c!=NULL) {
                if (pclan!=NULL) {
                    send_to_char("Can't specify more than one clan.\n\r",ch);
                    return;
                }
                pclan=c;
                continue;
            }
        }

        sprintf_to_char(ch,
                        "%s: That's not a valid race, class or clan.\n\r",arg);
        return;
    }

    //
    // Now show matching chars.
    //
    nMatch = 0;
    buf[0] = '\0';
    output = new_buf();
    for ( wch = player_list; wch != NULL; wch = wch->next_player ) {
        char const *class;

        /*
         * Check for match against restrictions.
         * Don't use trust as that exposes trusted mortals.
         */
        if ( !can_see( ch, wch ) )
            continue;

        count++;

        if ( wch->level < iLevelLower
             ||   wch->level > iLevelUpper
             || ( fImmortalOnly  && wch->level < LEVEL_IMMORTAL )
             || ( fClassRestrict && !rgfClass[wch->class] )
             || ( fRaceRestrict && !rgfRace[wch->race]))
            continue;

        // filter out everybody who is not in the specified clan
        if (pclan!=NULL &&
                (wch->clan==NULL || wch->clan->clan_info!=pclan))
            continue;

        nMatch++;

        /*
         * Figure out what to print for class.
     */
        class = class_table[wch->class].who_name;
        switch ( wch->level )
        {
        default: break;
        case MAX_LEVEL	: class = "IMP"; break;
        case MAX_LEVEL1	: class = "CRE"; break;
        case MAX_LEVEL2	: class = "SUP"; break;
        case MAX_LEVEL3	: class = "DEI"; break;
        case MAX_LEVEL4	: class = "GOD"; break;
        case MAX_LEVEL5	: class = "IMM"; break;
        case MAX_LEVEL6	: class = "DEM"; break;
        case MAX_LEVEL7	: class = "ANG"; break;
        case MAX_LEVEL8	: class = "GUE"; break;
        case MAX_LEVEL9	: class = "HER"; break;
        }

        //
        // Format it up.
        //
        if (wch->invis_level>0&&IS_IMMORTAL(ch))
            sprintf(wizi,"(Wizi %d) ",wch->invis_level);
        else if (IS_AFFECTED(wch,EFF_INVISIBLE))
            strcpy(wizi,"(Invis) ");
        else
            strcpy(wizi,"");

        strcpy(trust,"");
        if ((wch->trust!=0)&&IS_IMMORTAL(ch))
            sprintf(trust,"(Trust %d) ",wch->trust);

        strcpy(incog,"");
        if (wch->incog_level>0&&IS_IMMORTAL(ch))
            sprintf(incog,"(Incog %d) ",wch->incog_level);

        strcpy(limbo,"");
        if (IS_IMMORTAL(ch))
            if (wch->in_room && wch->in_room->vnum==ROOM_VNUM_LIMBO)
                strcpy(limbo,"(Limbo) ");

        // If pueblo support is enabled make the name a link to the
        // finger command.
        if (HAS_PUEBLO(ch)) {
            sprintf(wname,"</xch_mudtext><a xch_cmd=\"pueblo finger %s\">"
                          "<tt>%s</tt></a><xch_mudtext>", wch->name, wch->name);
        } else if (HAS_MXP(ch)) {
            sprintf(wname,MXP"<send \"finger %s\">%s</send>",
                    wch->name, wch->name);
        } else {
            strcpy(wname, wch->name);
        }

        if (wch->position==POS_DEAD) {
            strcpy(buf,C_WHO_BRACKET"[{x   DECEASED   " C_WHO_BRACKET "]{x ");
        } else if (wch->pcdata->whoname[0]!=0) {
            int len=14-str_len(wch->pcdata->whoname);
            int lenleft,lenright;

            lenleft=len/2;
            lenright=len-lenleft;

            sprintf(buf,C_WHO_BRACKET"[{x" "%s%s%s" C_WHO_BRACKET "]{x ",
                    spaces(lenleft),
                    wch->pcdata->whoname,
                    spaces(lenright));
        } else {
            sprintf( buf, C_WHO_BRACKET "[" C_WHO_LEVEL "%3d "
                     C_WHO_RACE "%6s " C_WHO_CLASS "%s"
                     C_WHO_BRACKET "]{x ",
                     wch->level,
                     wch->race < MAX_PC_RACE ? pc_race_table[wch->race].who_name
                     : "     ",
                     class);
        }
        add_buf(output,buf);
        sprintf( buf, "%s%s%s%s%s%s%s%s%s%s%s%s%s%s{x\n\r",
                 incog,wizi,limbo,trust,
                 wch->clan==NULL ? "" : "[",
                 wch->clan==NULL ? "" : wch->clan->clan_info->who_name,
                 wch->clan==NULL ? "" : wch->clan->clan_info==lonerclan? "   " : wch->clan->rank_info->who_name,
                 wch->clan==NULL ? "" : "] ",
                 STR_IS_SET(wch->strbit_comm, COMM_AFK) ? "[AFK] " : "",
                 STR_IS_SET(wch->strbit_act, PLR_KILLER) ? "(KILLER) " : "",
                 STR_IS_SET(wch->strbit_act, PLR_THIEF)  ? "(THIEF) "  : "",
                 STR_IS_SET(wch->strbit_act, PLR_HARDCORE)? "[HC] " : "",
                 wname,
                 get_afk_title(wch) );
        add_buf(output,buf);
    }

    sprintf( buf2, "\n\r");
    add_buf(output,buf2);

    max_on=UMAX(max_on,count);
    if (max_on == count)
        sprintf(buf2,
                "There %s %d character%s on, the most so far today.\n\r",
                count==1?"is":"are",count,count==1?"":"s");
    else
        sprintf(buf2,
                "There %s %d character%s on, the most on today was %d.\n\r",
                count==1?"is":"are",count,count==1?"":"s",max_on);
    add_buf(output,buf2);

    page_to_char( buf_string(output), ch );
    free_buf(output);
    return;
}

void do_inventory( CHAR_DATA *ch, char *argument )
{
    char s[MAX_STRING_LENGTH];

    if (STR_IS_SET(ch->strbit_comm,COMM_SHOW_WEIGHT)) {
        if (IS_AFFECTED(ch, EFF_BLIND)&&
                (!IS_NPC(ch) && !STR_IS_SET(ch->strbit_act,PLR_HOLYLIGHT))) {
            strcpy(s,"You are carrying a number of objects with a certain weight:\n\r");
        } else {
            sprintf(s,"You are carrying %d (max %d) objects with a weight of %ld (max %d) pounds:\n\r",
                    ch->carry_number,can_carry_n(ch),
                    get_carry_weight(ch)/10,can_carry_w(ch)/10);
        }
        send_to_char(s,ch);
    } else {
        send_to_char( "You are carrying:\n\r", ch );
    }
    show_list_to_char( ch->carrying, ch, NULL, TRUE, TRUE );
    return;
}



void do_equipment( CHAR_DATA *ch, char *argument )
{
    OBJ_DATA *obj,*weapon;
    int iWear;
    bool found;

    weapon=get_eq_char(ch,WEAR_WIELD);

    send_to_char( "You are using:\n\r", ch );
    found = FALSE;
    for ( iWear = 0; iWear < MAX_WEAR; iWear++ ) {
        // it's pretty useless if you display a second-weapon entry when
        // the wielded weapon is two-handed.
        if (iWear==WEAR_SECONDARY) {
            if (weapon && IS_WEAPON_STAT(weapon,WEAPON_TWO_HANDS)) {
                continue;
            }
        }
        if ( ( obj = get_eq_char( ch, iWear ) ) == NULL ) {
            if (STR_IS_SET(ch->strbit_comm,COMM_SHOW_EQUIPMENT)) {
                send_to_char( where_name[iWear], ch );
                send_to_char("-\n\r",ch);
                found = TRUE;
            }
            continue;
        }

        send_to_char( where_name[iWear], ch );
        if ( can_see_obj( ch, obj ) && !STR_IS_SET(obj->strbit_extra_flags,ITEM_NOSHOW))
        {
            send_to_char( format_obj_to_char( obj, ch, TRUE ), ch );
            send_to_char( "\n\r", ch );
        }
        else
        {
            send_to_char( "something.\n\r", ch );
        }
        found = TRUE;
    }

    if ( !found )
        send_to_char( "Nothing.\n\r", ch );

    return;
}

void do_compare_cost( CHAR_DATA *ch, OBJ_DATA *obj1, OBJ_DATA *obj2 )
{
    long diff=(obj2->cost)-(obj1->cost);
    char *msg;

    if (diff>200)	     msg="$p is a lot cheaper than $P.";
    else if (diff>100)   msg="$p is much cheaper than $P.";
    else if (diff>50)    msg="$p is cheaper than $P.";
    else if (diff>-50)   msg="$p costs about the same as $P.";
    else if (diff>-100)  msg="$p is more expensive than $P.";
    else if (diff>-200)  msg="$p is much more expensive than $P.";
    else                 msg="$p is a lot more expensive than $P.";
    act(msg, ch, obj1, obj2, TO_CHAR);
}

void do_compare_light( CHAR_DATA *ch, OBJ_DATA *obj1, OBJ_DATA *obj2 )
{
    /* pre: obj1 and obj2 are both ITEM_LIGHT */
    int v1=obj1->value[2];
    int v2=obj2->value[2];
    char *msg;

    if ((v1<0) || (v1>999)) v1=999;	/* permanent light-source */
    if ((v2<0) || (v2>999)) v2=999;	/* permanent light-source */

    if (v1<v2)	msg="$p will give light for a shorter time than $P.";
    else if (v1>v2) msg="$p will give light for a longer time than $P.";
    else            msg="$p will give light about as long as $P.";
    act(msg, ch, obj1, obj2, TO_CHAR);
    do_compare_cost(ch,obj1,obj2);
}


void do_compare(CHAR_DATA *ch, char *argument ) {
    compare(ch,argument,NULL);
}

int mob_compare(CHAR_DATA *ch,OBJ_DATA *obj) {
    return compare(ch,NULL,obj);
}

int compare( CHAR_DATA *ch, char *argument, OBJ_DATA *obj )
{
    char arg1[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    OBJ_DATA *obj1;
    OBJ_DATA *obj2;
    int value1;
    int value2;
    char *msg;

    if (argument) {
        argument = one_argument( argument, arg1 );
        argument = one_argument( argument, arg2 );
        if ( arg1[0] == '\0' ) {
            send_to_char( "Compare what to what?\n\r", ch );
            return -1;
        }

        if ( ( obj1 = get_obj_carry( ch, arg1, ch ) ) == NULL ) {
            send_to_char( "You do not have that item.\n\r", ch );
            return -1;
        }

        if (arg2[0] == '\0') {
            for (obj2 = ch->carrying; obj2 != NULL; obj2 = obj2->next_content) {
                if (obj2->wear_loc != WEAR_NONE
                        &&  can_see_obj(ch,obj2)
                        &&  obj1->item_type == obj2->item_type
                        &&  (obj1->wear_flags & obj2->wear_flags & ~ITEM_TAKE) != 0 )
                    break;
            }

            if (obj2 == NULL) {
                send_to_char("You aren't wearing anything comparable.\n\r",ch);
                return 1;
            }
        } else if ( (obj2 = get_obj_carry(ch,arg2,ch) ) == NULL ) {
            send_to_char("You do not have that item.\n\r",ch);
            return 1;
        }
    }

    if (obj) {
        obj1=obj;
        for (obj2 = ch->carrying; obj2 != NULL; obj2 = obj2->next_content) {
            if (obj2->wear_loc != WEAR_NONE
                    &&  can_see_obj(ch,obj2)
                    &&  obj1->item_type == obj2->item_type
                    &&  (obj1->wear_flags & obj2->wear_flags & ~ITEM_TAKE) != 0 )
                break;
        }
        if (obj2 == NULL) {
            send_to_char("You aren't wearing anything comparable.\n\r",ch);
            return 1;
        }
    }


    msg		= NULL;
    value1	= 0;
    value2	= 0;

    if ( obj1 == obj2 )
    {
        msg = "You compare $p to itself.  It looks about the same.";
    }
    else if ( obj1->item_type != obj2->item_type )
    {
        msg = "You can't compare $p and $P.";
    }
    else
    {
        switch ( obj1->item_type )
        {
        default:
            msg = "You can't compare $p and $P.";
            break;

        case ITEM_LIGHT: do_compare_light(ch,obj1,obj2); return -1;

        case ITEM_ARMOR:
            value1 = obj1->value[0] + obj1->value[1] + obj1->value[2];
            value2 = obj2->value[0] + obj2->value[1] + obj2->value[2];
            break;

        case ITEM_WEAPON:
            value1 = (1 + obj1->value[2]) * obj1->value[1];
            value2 = (1 + obj2->value[2]) * obj2->value[1];
            break;
        }
    }

    if ( msg == NULL ) {
        if ( value1 == value2 ) msg = "$p and $P look about the same.";
        else if ( value1  > value2 ) msg = "$p looks better than $P.";
        else                         msg = "$p looks worse than $P.";
    }
    act( msg, ch, obj1, obj2, TO_CHAR );
    do_compare_cost(ch,obj1,obj2);

    if ( value1 == value2 ) return 0;
    else if ( value1  > value2 ) return 1;
    else                         return -1;
}



void do_where( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    int count;
    bool found;

    one_argument( argument, arg );
    if (str_prefix("all.",arg)) { // true if NOT prefixed. - see db.c
        count = number_argument( arg, arg2 );
    } else {
        count = -1;
        strcpy(arg2,arg+4);
    }

    if ( arg[0] == '\0' ) {
        sprintf_to_char(ch,"You are in %s's %s, level %d to %d.\n\r",
                        ch->in_room->area->creator,
                        ch->in_room->area->name,
                        ch->in_room->area->llevel,
                        ch->in_room->area->ulevel);

        if (IS_PC(ch)) {
            int been=0,killed=0,seenmob=0,seenobject=0,used=0;
            int i;

            send_to_char("\n\r",ch);

            for (i=ch->in_room->area->lvnum;i<=ch->in_room->area->uvnum;i++)
                if (STR_IS_SET(ch->pcdata->seenthatmob,i))
                    seenmob++;
            for (i=ch->in_room->area->lvnum;i<=ch->in_room->area->uvnum;i++)
                if (STR_IS_SET(ch->pcdata->seenthatobject,i))
                    seenobject++;
            for (i=ch->in_room->area->lvnum;i<=ch->in_room->area->uvnum;i++)
                if (STR_IS_SET(ch->pcdata->beeninroom,i))
                    been++;
            for (i=ch->in_room->area->lvnum;i<=ch->in_room->area->uvnum;i++)
                if (STR_IS_SET(ch->pcdata->usedthatobject,i))
                    used++;
            for (i=ch->in_room->area->lvnum;i<=ch->in_room->area->uvnum;i++)
                if (STR_IS_SET(ch->pcdata->killedthatmob,i))
                    killed++;

            sprintf_to_char(ch,"You have seen %d%% of this area.\n\r",
                            ch->in_room->area->vnum_rooms_in_use==0?100:
                                                                    100*been/ch->in_room->area->vnum_rooms_in_use);
            sprintf_to_char(ch,
                            "You have seen %d%% of the objects in this area.\n\r",
                            ch->in_room->area->vnum_objects_in_use==0?100:
                                                                      100*seenobject/ch->in_room->area->vnum_objects_in_use);
            sprintf_to_char(ch,
                            "You have used %d%% of the objects in this area.\n\r",
                            ch->in_room->area->vnum_objects_in_use==0?100:
                                                                      100*used/ch->in_room->area->vnum_objects_in_use);
            sprintf_to_char(ch,
                            "You have seen %d%% of the mobs in this area.\n\r",
                            ch->in_room->area->vnum_mobs_in_use==0?100:
                                                                   100*seenmob/ch->in_room->area->vnum_mobs_in_use);
            sprintf_to_char(ch,
                            "You have killed %d%% of the mobs in this area.\n\r",
                            ch->in_room->area->vnum_mobs_in_use==0?100:
                                                                   100*killed/ch->in_room->area->vnum_mobs_in_use);
            send_to_char("\n\r",ch);
        }

        send_to_char("Players near you:\n\r",ch);
        found = FALSE;
        for ( victim = player_list; victim!=NULL; victim = victim->next_player )
        {
            if ( !STR_IS_SET(victim->in_room->strbit_room_flags,ROOM_NOWHERE)
                 &&   (is_room_owner(ch,victim->in_room)
                       ||    !room_is_private(victim->in_room))
                 &&   victim->in_room->area == ch->in_room->area
                 &&   ch!=victim
                 &&   can_see( ch, victim ) )
            {
                found = TRUE;
                sprintf_to_char(ch, "%-28s %s\n\r",
                                victim->name, victim->in_room->name );
            }
        }
        if ( !found )
            send_to_char( "None\n\r", ch );
        return;
    }

    found = FALSE;
    for ( victim = char_list; victim != NULL; victim = victim->next )
    {
        if ( victim->in_room != NULL
             &&   victim->in_room->area == ch->in_room->area
             &&   !IS_AFFECTED(victim, EFF_HIDE)
             &&   !IS_AFFECTED(victim, EFF_SNEAK)
             &&   can_see( ch, victim )
             &&   ch != victim
             &&   is_name( arg2, victim->name ) )
        {
            if ((count == -1) || (--count==0)) {
                found = TRUE;
                sprintf_to_char(ch, "%-28s %s\n\r",PERS(victim, ch), victim->in_room->name );
                if (count!=-1) break;
            }
        }
    }
    if ( !found )
        act( "You didn't find any $T.", ch, NULL, arg, TO_CHAR );

    return;
}




void do_consider( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    char *msg;
    int diff;

    one_argument( argument, arg );

    if (arg[0]==0)
        victim=ch->fighting;
    else
        victim=get_char_room(ch,arg);

    if (victim==NULL) {
        send_to_char( "Consider killing whom?\n\r", ch );
        return;
    }

    if (is_safe(ch,victim)) {
        send_to_char("Don't even think about it.\n\r",ch);
        return;
    }

    diff = victim->level - ch->level;

    if ( diff <= -10 ) msg = "You can kill $N naked and weaponless.";
    else if ( diff <=  -5 ) msg = "$N is no match for you.";
    else if ( diff <=  -2 ) msg = "$N looks like an easy kill.";
    else if ( diff <=   1 ) msg = "The perfect match!";
    else if ( diff <=   4 ) msg = "$N says '" C_SAY "Do you feel lucky, punk?{x'";
    else if ( diff <=   9 ) msg = "$N laughs at you mercilessly.";
    else                    msg = "Death will thank you for your gift.";
    act( msg, ch, NULL, victim, TO_CHAR );

    /* Alignment spotting by Yukon. 1998 */
    if (ch->alignment>800) {		// Very good characters can tell a lot
        if (victim->alignment >= 900)
            msg = "$N represents all that is Holy and Good.";
        else if (victim->alignment >= 700)
            msg = "$N is an ally, $E's on your side.";
        else if (victim->alignment >= 250)
            msg = "$N has Good intentions, at least.";
        else if (victim->alignment > -250)
            msg = "$N is neutral.";
        else if (victim->alignment > -900)
            msg = "$N is an enemy, $E's rotten.";
        else
            msg = "$N is black-hearted! $E is Absolutely Evil!";
    } else if (ch->alignment<-800) {	// so can very evil characters
        if (victim->alignment >= 900)
            msg = "$N absolutely stinks of Righteousness.";
        else if (victim->alignment >= 700)
            msg = "$N is a goody goody.";
        else if (victim->alignment >= 250)
            msg = "$N likes to think $E's Good.";
        else if (victim->alignment > -250)
            msg = "$N is just a neutral coward.";
        else if (victim->alignment > -900)
            msg = "$N is merely mean and selfish.";
        else
            msg = "You and $N are two of a kind, $E is truly Evil.";
    } else if( (ch->alignment < 100) && (ch->alignment > -101) ) {
        if (victim->alignment >= 700)
            msg = "$N is committed to the side of Good.";
        else if (victim->alignment >= 250)
            msg = "$N is trying to be Good.";
        else if (victim->alignment >= -250)
            msg = "$N is neither Good nor Evil.";
        else if (victim->alignment > -700)
            msg = "$N is trying to be Evil.";
        else
            msg = "$N is committed to the side of Evil.";
    } else {		// Neither very good nor very evil nor very neutral
        if (victim->alignment > 900)
            msg = "$N seems Good.";
        else if (victim->alignment > -900)
            msg = "You cannot perceive $S alignment.";
        else
            msg = "$N seems Evil.";
    }
    act(msg,ch,NULL,victim,TO_CHAR );

    act("$n looks at you, sizing you up.",ch,NULL,victim,TO_VICT);

    return;
}



void set_title( CHAR_DATA *ch, char *title )
{
    char buf[MAX_STRING_LENGTH];

    if ( IS_NPC(ch) ) {
        bugf( "Set_title: Was called with an NPC (%s)",NAME(ch));
        return;
    }

    if ( title[0] != '.' && title[0] != ',' && title[0] != '!' && title[0] != '?' )
    {
        buf[0] = ' ';
        strcpy( buf+1, title );
    }
    else
    {
        strcpy( buf, title );
    }

    free_string( ch->pcdata->title );
    ch->pcdata->title = str_dup( buf );
    return;
}



void do_title( CHAR_DATA *ch, char *argument )
{
    if ( IS_NPC(ch) )
        return;

    if ( argument[0] == '\0' )
    {
        send_to_char( "Change your title to what?\n\r", ch );
        return;
    }

    if ( strlen(argument) > 45 )
        argument[45] = '\0';

    smash_tilde( argument );
    set_title( ch, argument );
    send_to_char( "Ok.\n\r", ch );
}

void do_description( CHAR_DATA *ch, char *argument )
{
    if (IS_NPC(ch))
        return;

    send_to_char( "Enter your description:\n\r" , ch );
    editor_start(ch,&ch->description);
}

void do_report( CHAR_DATA *ch, char *argument )
{
    char buf[MAX_INPUT_LENGTH];

    if (IS_PC(ch)) {
        sprintf( buf, "I have %d/%d hp, %d/%d mana, %d/%d mv, %d xp and need %d xp to level.",
                 ch->hit,  ch->max_hit,
                 ch->mana, ch->max_mana,
                 ch->move, ch->max_move,
                 ch->exp,  exp_to_level(ch));
    } else {
        sprintf( buf, "I have %d/%d hp, %d/%d mana and %d/%d mv.",
                 ch->hit,  ch->max_hit,
                 ch->mana, ch->max_mana,
                 ch->move, ch->max_move);
    }
    do_function(ch,&do_say,buf);

    return;
}




/*
 * 'Wimpy' originally by Dionysos.
 */
void do_wimpy( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    int wimpy;

    one_argument( argument, arg );

    if ( arg[0] == '\0' )
        wimpy = ch->max_hit / 5;
    else
        wimpy = atoi( arg );

    if ( wimpy < 0 )
    {
        send_to_char( "Your courage exceeds your wisdom.\n\r", ch );
        return;
    }

    if ( wimpy > ch->max_hit/2 )
    {
        send_to_char( "Such cowardice ill becomes you.\n\r", ch );
        return;
    }

    ch->wimpy	= wimpy;
    sprintf_to_char(ch, "Wimpy set to %d hit points.\n\r", wimpy );
    return;
}

/*
 * lore code follows, with some novel limitations on what a char
 * can actually detect from an object, particuarly if the object
 * is well above their level (and thus, beyond what their experiences
 * would have taught them!) and some verbose responses. The reason
 * for this approach is that most muds I've seen with lore allow
 * a level 10 mage to be able to perfectly identify a level 91 dagger
 * without difficulty (except for their own skill %).
 * On another note, the variable responses mean that it makes it
 * slightly tougher for people to use a 'lore practice bot' :)))
 *  -Fara.
 */

/* messages for people wanting to know the weight of an object that they are
   actually carrying. For objects that are in the room, use the
   obj_anon_weight_table below, which is less informative */

const OBJ_WEIGHT_INFO obj_char_weight_table [] = {

    /*
           weight as % of carry_w       ,  message
           */

    {  0,   "This object is almost defying gravity!\n\r" },
    {  1,   "It weighs virtually nothing.\n\r" },
    {  3,   "It's as light as a feather.\n\r" },
    {  6,   "This object weighs very little.\n\r" },
    {  9,   "The weight seems quite reasonable.\n\r" },
    { 12,   "Loads like this are certainly noticeable, but not too bad.\n\r" },
    { 15,   "You can cope with this weight several times over.\n\r" },
    { 18,   "A fairly weighty item.\n\r" },
    { 21,   "This object awakens you to the laws of physics.\n\r" },
    { 24,   "This is a heavy object.\n\r" },
    { 27,   "It is of an uncomfortable weight.\n\r" },
    { 30,   "This is rather burdensome.\n\r" },
    { 33,   "You're struggling with the weight of this item.\n\r" },
    { 36,   "The object weighs more than you're happy with.\n\r" },
    { 39,   "This is distressingly heavy.\n\r" },
    { 42,   "You are encumbered.\n\r" },
    { 45,   "It'd probably be better to drag this item.\n\r" },
    { 48,   "You are fighting with gravity.\n\r" },
    { 51,   "You couldn't carry another of these.\n\r" },
    { 54,   "You've felt buildings which were lighter!\n\r" },
    { 75,   "Ever thought of hiring servants to carry this?\n\r" },
    { 99,   "The Gods issue you with a health warning: 'Put this down now, or risk injury!'\n\r" },
    { 100,  "You are employing every ounce of strength to carry this.\n\r" }

};

const OBJ_WEIGHT_INFO  obj_anon_weight_table [] = {

    {  0, "You're not sure how heavy this is.\n\r" },
    {  1, "It's weighs virtually nothing.\n\r" },
    {  5, "It appears to be pretty light.\n\r" },
    { 10, "The object doesn't look that heavy.\n\r" },
    { 15, "You could probably carry this thing without difficulty.\n\r" },
    { 20, "This looks like it would require effort to pick up.\n\r" },
    { 35, "It appears to be burdensome.\n\r" },
    { 50, "You'd be encumbered with the weight of this thing.\n\r" },
    { 70, "Carrying this item might pop a vein or two.\n\r" },
    { 99, "Lifting this item would certainly help your weight lifting career.\n\r" },
    { 100, "The Gods would probably have trouble hauling this thing around.\n\r" }
};


/* plonk more responses in the next 2 tables to give the punters variety */
const LORE_RESPONSES lore_failure_table [] = {

    { "You are momentarily distracted.\n\r" },
    { "Something gets in your eye and puts you off.\n\r" },
    { "You seem to be having trouble fathoming this object.\n\r" },
    { "You scratch your head in confusion.\n\r" },
    { "Know what it's like when your mind suddenly goes blank?\n\r" },
    { "You lose your concentration briefly.\n\r" },
    { "So, what the hell is this thing, anyway??\n\r" }
};

const LORE_RESPONSES lore_success_table [] = {
    { "This object looks particularly intriguing.\n\r" },
    { "Upon closer inspection, you notice some details about this object.\n\r" },
    { "You find that your lore skill comes in handy sometimes.\n\r" },
    { "Your experience with artifacts is useful again.\n\r" },
    { "You have a tendency to trust your eyesight...\n\r" },
    { "What a funky looking item!\n\r" }
};

/* Caution: this one should have the same number of entries as there are
   lore ability levels..for the time being anyway  */
const LORE_RESPONSES lore_ability_table [] = {

    /* ability: poor */
    { "Hmmm, it's a bit too complex for you to understand.\n\r" },

    /* ability: fair */
    { "You're sure that there is more to this object than you can make out\n\r" },

    /* ability: good */
    { "Objects of this type are almost second nature to you now.\n\r" },

    /* ability: excellent */
    { "Your expertise reveals everything about this object.\n\r" }

};


/* gives verbose feedback on the player's examination of this object */

void lore_verbosity( CHAR_DATA * ch, bool success, lore_ability_t ability ){

    int i = 0, n_elements = 0;
    const LORE_RESPONSES * my_table;
    const char * ability_remark = NULL;

    my_table = ( success ? lore_success_table : lore_failure_table );
    ability_remark = ( success ? lore_ability_table[ability].message
                               : NULL );

    if( success ) {
        n_elements = ( sizeof( lore_success_table ) /
                       sizeof( lore_success_table[0] ));
    }
    else {
        n_elements = ( sizeof( lore_failure_table ) /
                       sizeof( lore_failure_table[0] ));
    }
    i = ( number_percent() % n_elements );
    if( i < 0 )  /* sanity check */
        i = 0;

    /* element 0 of a response array should always be valid */
    send_to_char(  my_table[i].message, ch );
    if( ability_remark != NULL )
        send_to_char( ability_remark, ch );
}


void lore( CHAR_DATA * ch, OBJ_DATA * obj, bool carried ) {

    int chl = 1, objl = 1;
    int ratio = 1;
    lore_ability_t ability;
    char buf[MAX_STRING_LENGTH];
    BUFFER * buffer;

    if (STR_IS_SET(obj->strbit_extra_flags,ITEM_NOSHOW) &&
            (IS_NPC(ch) || !STR_IS_SET(ch->strbit_act, PLR_HOLYLIGHT)))
        return;
    if(!has_skill_available( ch, gsn_lore ))
        return;
    if (!skillcheck(ch,gsn_lore)) {
        /* failure so let them know.. */
        WAIT_STATE( ch, skill_beats(gsn_lore,ch) );
        lore_verbosity( ch, FALSE, 0 );
        check_improve( ch, gsn_lore, FALSE, 1 );
        return;
    }

    if (IS_OBJ_STAT(obj,ITEM_UNIDENTIFABLE))
        return;

    if( ch->level )
        chl = ( ch->level * 100 );
    else
        chl = 100;
    if( obj->level )
        objl =  obj->level ;
    else
        objl = 1; /* don't want divzero now do we? */
    ratio = chl / objl;
    for( ; ; ) { /* ever */
        if( ratio <= LORE_POOR ){
            ability = poor;
            break;
        }
        if( ratio <= LORE_FAIR ) {
            ability = fair;
            break;
        }
        if( ratio <= LORE_GOOD ) {
            ability = good;
            break;
        }
        else {
            ability = excellent;
            break;
        }
    }
    buffer = new_buf();
    if( ability >= poor ) {
        if (obj->item_type==ITEM_ARMOR) {
            sprintf( buf,
                     "object '%s' is armor for %s\n\r",
                     obj->name,option_string_from_long(obj->wear_flags & ITEM_WEAR_MASK,wear_options));
            add_buf( buffer, buf );
        } else {
            sprintf( buf,
                     "object '%s' is type '%s'\n\r",
                     obj->name,item_type(obj));
            add_buf( buffer, buf );
        }
        if (IS_AFFECTED(ch,EFF_HALLUCINATION) &&
                (obj->item_type==ITEM_CORPSE_PC ||
                 obj->item_type==ITEM_CORPSE_NPC))
            add_buf( buffer, "You here a voice say \"It's dead, Jim.\"\n\r" );
        sprintf( buf, "%s", obj_weight_message( ch,
                                                get_obj_weight( obj ),
                                                carried ));
        add_buf( buffer, buf );

    }
    if( ability >= fair ) {
        sprintf( buf,
                 "value is %d\n\r",
                 obj->cost );
        add_buf( buffer, buf );
    }
    if( ability >= good ) {
        sprintf( buf,
                 "level is %d\n\rextra flags %s\n\r",
                 obj->level,obj_extra_string(obj));
        add_buf( buffer, buf );
    }
    lore_verbosity( ch, TRUE, ability );
    page_to_char( buf_string( buffer ), ch );
    free_buf( buffer );

    if( ability < excellent ) {   /* no more info for unskilled ;) */
        if (ch->level<LEVEL_IMMORTAL)
            WAIT_STATE( ch, skill_beats(gsn_lore,ch) );
        check_improve( ch, gsn_lore, TRUE, 1 );
        return;
    }
    identify_more( ch, obj );
    if (ch->level<LEVEL_IMMORTAL)
        WAIT_STATE( ch, skill_beats(gsn_lore,ch) );
    check_improve( ch, gsn_lore, TRUE, 1 );
}


char * obj_weight_message( CHAR_DATA * ch, int weight, bool carried ) {

    const OBJ_WEIGHT_INFO * my_table;
    long ratio = 0;
    long big_weight = weight;
    int i = 0;

    ratio = ( ( big_weight * 100 ) /  can_carry_w( ch )) ;
    if( ratio > 100 )
        return "This weighs more than you can carry!\n\r";

    my_table = ( carried ? obj_char_weight_table : obj_anon_weight_table );
    for( ; my_table[i].proportion < ratio ; i++ )
        ; /* nowt */
    return( my_table[i].message );
}

// translate direction string to numeric exit number
int get_direction(const char *arg1) {
    int door;

    if ( !str_cmp( arg1, "n" ) || !str_cmp( arg1, "north" ) ) door=DIR_NORTH;
    else if ( !str_cmp( arg1, "e" ) || !str_cmp( arg1, "east"  ) ) door=DIR_EAST;
    else if ( !str_cmp( arg1, "s" ) || !str_cmp( arg1, "south" ) ) door=DIR_SOUTH;
    else if ( !str_cmp( arg1, "w" ) || !str_cmp( arg1, "west"  ) ) door=DIR_WEST;
    else if ( !str_cmp( arg1, "u" ) || !str_cmp( arg1, "up"    ) ) door=DIR_UP;
    else if ( !str_cmp( arg1, "d" ) || !str_cmp( arg1, "down"  ) ) door=DIR_DOWN;
    else door=-1;
    return door;
}



void do_mobkill(CHAR_DATA *ch,char *argument) {
    int i;
    BUFFER *buf;
    char s[MSL];

    buf=new_buf();
    for (i=MAX_MOBKILL_SIZE-1;i>=0 && mobkill_table[i]!=NULL ;i--) {
        sprintf(s,"[%4d] %s\n\r",
                mobkill_table[i]->killed,
                mobkill_table[i]->short_descr);
        add_buf(buf,s);;
    }
    page_to_char( buf_string(buf), ch );
    free_buf(buf);
}



void do_room(CHAR_DATA *ch,char *argument) {
    char arg[MSL];

    if (check_ordered(ch)) return;

    if (!is_room_owner(ch,ch->in_room)) {
        send_to_char("You don't own this room.\n\r",ch);
        return;
    }

    argument=one_argument(argument,arg);

    switch (which_keyword(arg,"save","description",NULL)) {
    case 0:
    case -1:
        send_to_char("Usage: room description\t\tChange the room descripion\n\r",ch);
        send_to_char("       room save\t\tSave this room\n\r",ch);
        break;
    case 1:	// save
        SET_BIT(ch->in_room->area->area_flags,AREA_CHANGED);
        send_to_char("This room will be saved.\n\r",ch);
        break;
    case 2:	// description
        editor_start(ch,&ch->in_room->description);
        break;
    }
    return;
}
