// $Id: string.c,v 1.36 2007/03/11 11:47:48 jodocus Exp $
/***************************************************************************
 *  File: string.c                                                         *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 *                                                                         *
 *  This code was freely distributed with the The Isles 1.1 source code,   *
 *  and has been used here for OLC - OLC would not be what it is without   *
 *  all the previous coders who released their source code.                *
 *                                                                         *
 ***************************************************************************/

#include "merc.h"

//
// Substitutes one string for another.
//
char *string_replace(char *orig,char *old,char *new) {
    char	xbuf[MSL];
    int		i;

    xbuf[0]=0;
    strcpy(xbuf,orig);
    if (strstr(orig,old)!=NULL ) {
        i=strlen(orig)-strlen(strstr(orig,old));
        xbuf[i]=0;
        strcat(xbuf,new);
        strcat(xbuf,&orig[i+strlen(old)] );
        free_string(orig);
    }

    return str_dup( xbuf );
}



//
// Format a string, included word-wrapping and paragraph detection
//
char *format_string(char *oldstring) {
    char	newstring[MAX_STRING_LENGTH];
    char	formatstring[MAX_STRING_LENGTH];
    int		oldptr;
    int		formatptr,formatlen,formatlastspace;

    if (strlen(oldstring)>=MAX_STRING_LENGTH-4)	{
	bugf("String to format_string() longer than MAX_STRING_LENGTH.");
	return oldstring;
    }

    /* Rules for reformating
     * - a newline followed by a space is considered a new paragraph
     * - color-codes are not taken in account when counting string-lengths
     * - newlines are \n\r (who invented this should be hung)
     *
     * to format the string:
     *	0         1         2
     *  012345678901234567890123
     * "Dit is een test voor mij" at 18 positions:
     *  ^              ^  ^
     *  lastline lastword ptr
     */

    formatptr=0;
    formatlen=0;
    formatlastspace=0;
    newstring[0]=0;
    formatstring[0]=0;

    for (oldptr=0;oldstring[oldptr]!=0;oldptr++) {
	if ((oldstring[oldptr]=='\n') && (oldstring[oldptr+1]=='\r')) {
	    if ((oldstring[oldptr+2]==' ')||(oldstring[oldptr+2]==0)) {
		/* consider it a new paragraph */
		formatstring[formatptr]=0;
		strcat(newstring,formatstring);
		strcat(newstring,"\n\r");
		strcpy(formatstring," ");
		formatptr=1;
		formatlen=1;
		formatlastspace=0;
		oldptr++;	/* get rid of \r */
		if (oldstring[oldptr+1]!=0)
		    oldptr++;	/* get rid of the space */
	    } else if ((oldstring[oldptr+2]=='\n')&&(oldstring[oldptr+3]=='\r')) {
		formatstring[formatptr]=0;
		strcat(newstring,formatstring);
		strcat(newstring,"\n\r\n\r");
		formatptr=0;
		formatlen=0;
		formatlastspace=0;
		oldptr+=3;	/* get rid of \r\n\r */
	    } else if (formatlastspace==0) {
		formatstring[formatptr]=0;
		strcat(newstring,formatstring);
		strcat(newstring,"\n\r");
		formatptr=0;
		formatlen=0;
		formatlastspace=0;
		oldptr++;	/* get rid of \r */
	    } else {
		/* consider it the same line */
		formatstring[formatptr]=' ';
		formatlastspace=formatptr;
		formatptr++;
		formatlen++;
		oldptr++;	/* get rid of \r */
	    }
	} else if (oldstring[oldptr]=='}') {
	    /* consider it a color sequence */
	    formatstring[formatptr  ]='}';
	    formatstring[formatptr+1]=oldstring[oldptr+1];
	    formatptr+=2;
	    oldptr++;
	} else if (oldstring[oldptr]=='{') {
	    /* consider it a color sequence */
	    formatstring[formatptr  ]='{';
	    formatstring[formatptr+1]=oldstring[oldptr+1];
	    formatptr+=2;
	    oldptr++;
	} else if (oldstring[oldptr]==' ') {
	    /* consider it as the same line */
	    if ((formatptr!=0)&&(formatstring[formatptr-1]!=' ')) {
		formatstring[formatptr]=' ';
		formatlastspace=formatptr;
		formatptr++;
		formatlen++;
	    }
	} else {
	    /* consider it a normal char */
	    formatstring[formatptr]=oldstring[oldptr];
	    formatptr++;
	    formatlen++;
	}

	if (formatlen==72) {
	    /* reformat the string formatstring and append it to newstring */

	    if (oldstring[oldptr+1]=='\n') {
		formatstring[formatptr]=' ';
		formatlastspace=formatptr;
		formatptr++;
		formatlen++;
		oldptr+=2;
	    }
	    if (oldstring[oldptr+1]==' ') {
		formatstring[formatptr]=' ';
		formatlastspace=formatptr;
		formatptr++;
		formatlen++;
		oldptr++;
	    }

	    if (formatlastspace==0) {
		bugf("String to format_string() can't be reformatted because of insufficients spaces");
		formatstring[formatptr]=' ';
		formatlastspace=formatptr;
		formatptr++;
		formatlen++;
	    }

	    formatstring[formatlastspace]=0;
	    formatstring[formatptr]=0;
	    strcat(newstring,formatstring);
	    strcat(newstring,"\n\r");
	    strcpy(formatstring,formatstring+formatlastspace+1);
	    formatlen-=formatlastspace+1;
	    formatptr-=formatlastspace+1;
	    if (formatlen<0) formatlen=0;
	    if (formatptr<0) formatptr=0;
	    formatlastspace=0;
	}
    }

    free_string(oldstring);
    return(str_dup(newstring));
}

//
// Unpad a string (remove double spaces)
//
char *string_unpad(char *argument) {
    char	buf[MSL];
    char *	s;

    s=argument;

    while (*s==' ')
        s++;

    strcpy(buf,s);
    s=buf;

    if (*s!=0) {
        while (*s!=0)
            s++;
        s--;

        while (*s==' ')
            s--;
        s++;
        *s=0;
    }

    free_string(argument);
    return str_dup(buf);
}

//
// Returns an all-caps string.
//
char *all_capitalize(const char *str) {
    static char strcap[MSL];
    int		i;

    for (i=0;str[i]!=0;i++)
	strcap[i]=toupper(str[i]);
    strcap[i]=0;
    return strcap;
}

//
// Convert the string to lowercase
//
void smash_case(char *str) {
    char *c;

    for (c=str;*c!=0;c++) 
	*c=tolower(*c);
}

//
// Remove the tildes from a string
//
void smash_tilde(char *str) {
    char *p=str;

    if (str==NULL)
	return;

    while ((p=strchr(p,'~'))!=NULL)
	p[0]='-';
}

//
// Compare two strings, case insensitive.
// Returns FALSE if astr==bstr
//
bool str_cmp(const char *astr,const char *bstr) {
    if (astr==NULL) {
	bugf("str_cmp(): null astr.");
	return TRUE;
    }
                
    if (bstr==NULL) {
	bugf("str_cmp(): null bstr.");
	return TRUE;
    }
                
    for ( ; *astr || *bstr; astr++, bstr++ )
	if (LOWER(*astr)!=LOWER(*bstr))
	    return TRUE;
      
    return FALSE;
}

//
// Compare strings, case insensitive, for prefix matching.
// Returns FALSE if astr is a prefix of bstr
//
bool str_prefix(const char *astr,const char *bstr) {
    if (astr==NULL) {
	bugf( "str_prefix(): null astr.");
	return TRUE;
    }

    if ( bstr == NULL ) {
	bugf("str_prefix(): null bstr.");
	return TRUE;
    }

    for ( ; *astr; astr++, bstr++ )
	if (LOWER(*astr)!=LOWER(*bstr))
	    return TRUE;

    return FALSE;
}

//
// Compare strings, case insensitive, for prefix matching.
// Returns FALSE if needle is a prefix of haystack
//
bool str_prefix_nocolor(const char *needle,const char *haystack) {
    if (needle==NULL) {
        bugf( "str_prefix_nocolor(): null needle.");
        return TRUE;
    }

    if ( haystack == NULL ) {
        bugf("str_prefix_nocolor(): null haystack.");
        return TRUE;
    }

    for ( ; *needle; needle++, haystack++ ) {
        // skip color codes
        while ((*needle=='{' || *needle=='}') && *(needle+1)) {
            if (*needle==*(needle+1)) {
               needle++;
               break;
            }
            needle+=2;
        }
        while ((*haystack=='{' || *haystack=='}') && *(haystack+1)) {
            if (*haystack==*(haystack+1)) {
               haystack++;
               break;
            }
            haystack+=2;
        }

        if (LOWER(*needle)!=LOWER(*haystack))
            return TRUE;
    }

    return FALSE;
}

//
// Compare strings, case insensitive, for a match anywhere
// Returns FALSE is astr is part of bastr
//
bool str_infix( const char *astr, const char *bstr ) {
    int		sstr1;
    int		sstr2;
    int		ichar;
    char	c0;

    if ((c0=LOWER(astr[0]))==0)
        return FALSE;

    sstr1=strlen(astr);
    sstr2=strlen(bstr);

    for (ichar=0; ichar<=sstr2-sstr1; ichar++)
        if (c0==LOWER(bstr[ichar]) && !str_prefix(astr,bstr+ichar))
            return FALSE;

    return TRUE;
}
bool str_infix_nocolor( const char *astr, const char *bstr ) {
    int		sstr1;
    int		sstr2;
    int		ichar;
    char	c0;

    if ((c0=LOWER(astr[0]))==0)
        return FALSE;

    sstr1=strlen(astr);
    sstr2=strlen(bstr);

    for (ichar=0; ichar<=sstr2-sstr1; ichar++)
        if (c0==LOWER(bstr[ichar]) && !str_prefix_nocolor(astr,bstr+ichar))
            return FALSE;

    return TRUE;
}

//
// Compare strings, case insensitive, for a match anywhere.
// Returns pointer to position of astr in bstr
//
char *str_str(const char *astr, const char *bstr) {
    int		sstr1;
    int		sstr2;
    int		ichar;
    char	c0;

    if ((c0=LOWER(astr[0]))==0)
	return FALSE;

    sstr1=strlen(astr);
    sstr2=strlen(bstr);

    for (ichar=0;ichar<=sstr2-sstr1;ichar++) {
	if (c0==LOWER(bstr[ichar]) && !str_prefix(astr,bstr+ichar))
	    return (char *)(bstr+ichar);
    }

    return NULL;
}

//
// Compare strings, case insensitive, for suffix matching.
// Returns FALSE is astr is not suffix of bstr
//
bool str_suffix( const char *astr, const char *bstr ) {
    int	sstr1;
    int sstr2;

    sstr1=strlen(astr);
    sstr2=strlen(bstr);
    if (sstr1<=sstr2 && !str_cmp(astr,bstr+sstr2-sstr1))
        return FALSE;
    else
        return TRUE;
}

//
// Returns str as a string with capital start and rest lowercase.
// Takes care of colours. Return-value must be used immediatly.
//
char *capitalize(const char *str) {
    static char strcap[MSL];
    int		i;

    for (i=0;str[i]!=0;i++)
        strcap[i]=LOWER(str[i]);
    strcap[i]=0;
    strcap[0]=UPPER(strcap[0]);
    if ((strcap[0]=='{' || strcap[0]=='}') && strcap[1]!=0)
        strcap[2]=UPPER(strcap[2]);
    return strcap;
}

//
// Returns str as a string with capitals after each non-alpha-character
// and the rest lowercase. Takes care of colours. Return-value must
// be used immediatly.
//
char *Capitalize(const char *str) {
    static char strcap[MAX_STRING_LENGTH];
    int		i;
    bool        incol = FALSE;
    char        c, prev='\0';

    for (i=0;str[i]!=0;i++) {
        c = str[i];

        if (incol) {
	    if (c=='{' || c=='}') prev=c;
            incol = FALSE;
            strcap[i] = c;
        } else if (c=='{' || c=='}') {
            incol = TRUE;
            strcap[i] = c;
        } else {
            strcap[i] = isalpha(prev) ? LOWER(c) : UPPER(c);
            prev = c;
        }
    }
    strcap[i]='\0';
    return strcap;
}

//
// Returns the lenght of a string. Takes care of colours.
//
int str_len(const char *str) {
    int		len=0;
    bool	incol=FALSE;

    while (*str) {
	if (*str=='{') {
	    if (incol) { len++;incol=FALSE; }
	    else incol=TRUE;
	} else if (*str=='}') {
	    if (incol) { len++;incol=FALSE; }
	    else incol=TRUE;
	} else {
	    if (!incol) len++;
	    else incol=FALSE;
	}
	str++;
    }
    return len;
}

//
// Return an number of spaces with a maximum of 80
//
char *spaces(int len) {
    static char *spacearray=
	    "                                        "
	    "                                        ";

    if (len<0)  len=0;
    if (len>80) len=80;
    return spacearray+80-len;
}

//
// Returns a truncated string in a static buffer
// len should include room for the \0 at the end
//
char *string_trunc(char *str,int len) {
    static char* buf=NULL;
    static int bufsize=0;

    if (strlen(str)<len) return str;

    if (!buf) {
	buf=(char *)malloc(len);
	bufsize=len;
    } else if (len>bufsize) {
	char *newbuf;
	newbuf=(char *)realloc(buf,len);
	if (newbuf) {
	    buf=newbuf;
	    bufsize=len;
	} else {
	    len=bufsize; // we can't allocate enough room, lets shrink the length.
	}
    }
    strncpy(buf,str,len);
    buf[len-3]='.'; buf[len-2]='.'; buf[len-1]='.'; buf[len]=0;

    return buf;
}

//
// remove all color from the string
//
char *str_nocolor_r(char *buf,int bufsize,const char *str) {
    int spos,dpos;

    for (spos=0,dpos=0;str[spos]!=0 && dpos<bufsize-1;spos++) {
	if (str[spos]=='{' || str[spos]=='}') {
	  spos++;
	  if (str[spos]==0) break;
	  continue;
	}
	buf[dpos++]=str[spos];
    }
    buf[dpos]=0;
    return buf;
}
const char *str_nocolor(const char *str) {
    static char *buf=NULL;
    static int bufsize=0;
    int size;

    size=strlen(str)+1;

    if (bufsize<size) {
	if (buf) free(buf);
        buf=malloc(size);
	bufsize=buf?size:0;
    }

    if (buf) 
	return str_nocolor_r(buf,bufsize,str);
    return str_empty;
}

//
// Transforms a int to a string in a supplied buffer
//
char *itoa_r(int value,char *buf,int bufsize) {
    bool was_negative;
    char *ps,*pd,*end;

    if ((was_negative=value<0)) {
      value=-value;
    }

    ps=buf+bufsize-1;
    *(ps)=0;
    do {
	*(--ps)='0'+(value % 10);
	value=value/10;
    } while ((ps>=buf) && (value!=0));

    if (was_negative && ps!=buf)
      *(--ps)='-';

    pd=buf;
    end=buf+bufsize-1;
    for (;ps<=end;ps++,pd++) {
        *pd=*ps;
    }

    return (buf);
}

char *itoa(int value) {
    static char buffer[INT_STRING_BUF_SIZE];

    return itoa_r(value,buffer,sizeof(buffer));
}

char *ltoa_r(long value,char *buf,int bufsize) {
    bool was_negative;
    char *ps,*pd,*end;

    if ((was_negative=value<0)) {
      value=-value;
    }

    ps=buf+bufsize-1;
    *(ps)=0;
    do {
	*(--ps)='0'+(value % 10);
	value=value/10;
    } while ((ps>=buf) && (value!=0));

    if (was_negative && ps!=buf)
      *(--ps)='-';

    pd=buf;
    end=buf+bufsize-1;
    for (;ps<=end;ps++,pd++) {
        *pd=*ps;
    }

    return (buf);
}

char *ltoa(long value) {
    static char buffer[LONG_STRING_BUF_SIZE];

    return ltoa_r(value,buffer,sizeof(buffer));
}


