//
// $Id: fd_roomprog.c,v 1.40 2008/05/01 18:47:03 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#define NEEDS_TCL
#include "merc.h"
#include "interp.h"
#include "olc.h"

/*
 * Misc thingies
 */
int rp_error(ROOM_DATA *room) {
    char *errormsg;

    bugf("RP: TCL error in namespace %s in trigger %s on line %d: %s",
	namespace_room(room),
	progData==NULL?"(null)":progData->trigName,
    interp->errorLineDontUse,
	Tcl_GetStringResult(interp));

    errormsg=str_dup(Tcl_GetVar(interp,"errorInfo",TCL_GLOBAL_ONLY));
    while (errormsg) {
	char *line=strsep(&errormsg,"\n\r");
	logf("RP: errorInfo: %s",line);
	wiznet(WIZ_TCL_DEBUG,0,NULL,NULL,"RP: errorInfo: %s",line);
    }
    free_string(errormsg);
    return TCL_ERROR;
}



/*
 * ---------------------------------------------------------------------
 * Trigger handlers. These are called from various parts of the code
 * when an event is triggered.
 * ---------------------------------------------------------------------
 */

/*
 * general trigger for "normal" character/room interaction
 */

/*
 * In case of a return value (boolean always), a TRUE means
 * "okay, go on" and a FALSE means "this is wrong"
 */
bool rp_general_trigger_bool_all( ROOM_DATA *room, int trig_type,
				  CHAR_DATA *ch, CHAR_DATA *vict,char *buf,
				  OBJ_DATA *obj,OBJ_DATA *obj2) {
    TCLPROG_LIST *prg;
    int boolResult;

// Don't think we need these.
//    if (buf==NULL)
//	Tcl_SetVar(interp,"t","",0);
//    else
//	Tcl_SetVar(interp,"t",buf,0);

    for ( prg = room->rprogs; prg != NULL; prg = prg->next ) {
	if(prg->trig_type==trig_type) {
	    Tcl_Obj *result;

	    if(tcl_start(prg->name,buf,NULL,ch,obj,vict,obj2,room,room->area))
	        break;

	    if (prg->trig_phrase && str_cmp("",prg->trig_phrase)) {
		if(tcl_run_rp_code(room,prg->trig_phrase)==TCL_ERROR)
		    goto error;

		result=Tcl_GetObjResult(interp);
		boolResult=FALSE;
		if(Tcl_GetBooleanFromObj(interp,result,&boolResult)==TCL_ERROR)
		    goto error;
	    } else
		boolResult=TRUE;

	    if(boolResult) {
		if (tcl_run_rp_code(room,prg->code)==TCL_ERROR)
		    goto error;
		result=Tcl_GetObjResult(interp);
		boolResult=FALSE;
		if(Tcl_GetBooleanFromObj(interp,result,&boolResult)==TCL_ERROR)
		    goto error;
		if(tcl_stop()) return FALSE;
		return boolResult;
	    }

	    if(tcl_stop()) return FALSE;
	}
    }
    return TRUE;

error:
    rp_error(room);
    if(tcl_stop()) return FALSE;
    return FALSE;
}

void rp_general_trigger_all( ROOM_DATA *room,int trig_type,
			     CHAR_DATA *ch, CHAR_DATA *vict,char *buf,
			     OBJ_DATA *obj,OBJ_DATA *obj2)
{
    TCLPROG_LIST *prg;
    int boolResult;

// Don't think we need these.
//    if (buf==NULL)
//	Tcl_SetVar(interp,"t","",0);
//    else
//	Tcl_SetVar(interp,"t",buf,0);

    for ( prg = room->rprogs; prg != NULL; prg = prg->next ) {
	if(prg->trig_type==trig_type) {
	    Tcl_Obj *result;

	    if(tcl_start(prg->name,buf,NULL,ch,obj,vict,obj2,room,room->area)) return;

	    if (prg->trig_phrase && str_cmp("",prg->trig_phrase)) {
		if(tcl_run_rp_code(room,prg->trig_phrase)==TCL_ERROR)
		    goto error;

		result=Tcl_GetObjResult(interp);
		boolResult=FALSE;
		if(Tcl_GetBooleanFromObj(interp,result,&boolResult)==TCL_ERROR)
		    goto error;
	    } else
		boolResult=TRUE;

	    if(boolResult) {
		if (tcl_run_rp_code(room,prg->code)==TCL_ERROR)
		    goto error;
		if(tcl_stop()) return;
		break;
	    }

	    if(tcl_stop()) return;
	}
    }
    return;

error:
    rp_error(room);
    if(tcl_stop()) return;
    return;
}


//
// by making these wrappers it's easier to add new fields to the
// rp_general_trigger() function.
//
// trigger1: room
// trigger2: room + ch
// trigger3: room + ch + victim
// trigger4: room + ch + obj
// trigger5: room + ch + victim + obj
// trigger6: room + ch + text
// trigger7: room + ch + victim + text
//
bool rp_general_trigger1_bool(int trig_type,ROOM_DATA *room)
    { return rp_general_trigger_bool_all(room,trig_type,NULL,NULL,NULL,NULL,NULL); }
bool rp_general_trigger2_bool(int trig_type,ROOM_DATA *room,CHAR_DATA *ch)
    { return rp_general_trigger_bool_all(room,trig_type,ch,NULL,NULL,NULL,NULL); }
bool rp_general_trigger3_bool(int trig_type,ROOM_DATA *room,CHAR_DATA *ch,CHAR_DATA *victim)
    { return rp_general_trigger_bool_all(room,trig_type,ch,victim,NULL,NULL,NULL); }
bool rp_general_trigger4_bool(int trig_type,ROOM_DATA *room,CHAR_DATA *ch,OBJ_DATA *obj)
    { return rp_general_trigger_bool_all(room,trig_type,ch,NULL,NULL,obj,NULL); }
bool rp_general_trigger5_bool(int trig_type,ROOM_DATA *room,CHAR_DATA *ch,CHAR_DATA *vict,OBJ_DATA *obj)
    { return rp_general_trigger_bool_all(room,trig_type,ch,vict,NULL,obj,NULL); }
bool rp_general_trigger6_bool(int trig_type,ROOM_DATA *room,CHAR_DATA *ch,char *txt)
    { return rp_general_trigger_bool_all(room,trig_type,ch,NULL,txt,NULL,NULL); }
bool rp_general_trigger7_bool(int trig_type,ROOM_DATA *room,CHAR_DATA *ch,CHAR_DATA *vict,char *txt)
    { return rp_general_trigger_bool_all(room,trig_type,ch,vict,txt,NULL,NULL); }



void rp_general_trigger1(int trig_type,ROOM_DATA *room)
    { rp_general_trigger_all(room,trig_type,NULL,NULL,NULL,NULL,NULL); }
void rp_general_trigger2(int trig_type,ROOM_DATA *room,CHAR_DATA *ch)
    { rp_general_trigger_all(room,trig_type,ch,NULL,NULL,NULL,NULL); }
void rp_general_trigger3(int trig_type,ROOM_DATA *room,CHAR_DATA *ch,CHAR_DATA *victim)
    { rp_general_trigger_all(room,trig_type,ch,victim,NULL,NULL,NULL); }
void rp_general_trigger4(int trig_type,ROOM_DATA *room,CHAR_DATA *ch,OBJ_DATA *obj)
    { rp_general_trigger_all(room,trig_type,ch,NULL,NULL,obj,NULL); }
void rp_general_trigger5(int trig_type,ROOM_DATA *room,CHAR_DATA *ch,CHAR_DATA *vict,OBJ_DATA *obj)
    { rp_general_trigger_all(room,trig_type,ch,vict,NULL,obj,NULL); }
void rp_general_trigger6(int trig_type,ROOM_DATA *room,CHAR_DATA *ch,char *txt)
    { rp_general_trigger_all(room,trig_type,ch,NULL,txt,NULL,NULL); }
void rp_general_trigger7(int trig_type,ROOM_DATA *room,CHAR_DATA *ch,CHAR_DATA *vict,char *txt)
    { rp_general_trigger_all(room,trig_type,ch,vict,txt,NULL,NULL); }


//
//

//
// lookat trigger
//
bool rp_prelook_trigger(ROOM_DATA *room,CHAR_DATA *ch) {
    return rp_general_trigger2_bool(RTRIG_PRELOOK,room,ch);
}
void rp_look_trigger(ROOM_DATA *room,CHAR_DATA *ch) {
    rp_general_trigger2(RTRIG_LOOK,room,ch);
}

//
// speech trigger
//
bool	rp_prespeech_trigger(CHAR_DATA *ch,char *txt) {
    return rp_general_trigger6_bool(RTRIG_PRESPEECH,ch->in_room,ch,txt);
}
void	rp_speech_trigger(CHAR_DATA *ch,char *txt) {
    rp_general_trigger6(RTRIG_SPEECH,ch->in_room,ch,txt);
}

//
// reset trigger
//
bool	rp_prereset_trigger(ROOM_DATA *room) {
    return rp_general_trigger1_bool(RTRIG_PRERESET,room);
}
void	rp_reset_trigger(ROOM_DATA *room) {
    rp_general_trigger1(RTRIG_RESET,room);
}

//
// combat triggers
//
bool    rp_preattack_trigger(CHAR_DATA *ch,CHAR_DATA *victim,char *string) {
    return rp_general_trigger7_bool(RTRIG_PREATTACK,ch->in_room,ch,victim,string);
}
void	rp_kill_trigger(CHAR_DATA *ch,CHAR_DATA *victim) {
    rp_general_trigger3(RTRIG_KILL,ch->in_room,ch,victim);
}
void	rp_death_trigger(CHAR_DATA *ch,CHAR_DATA *killer) {
    rp_general_trigger3(RTRIG_DEATH,ch->in_room,ch,killer);
}

//
// lookat extra description trigger
//
bool	rp_prelook_ed_trigger(ROOM_DATA *room,CHAR_DATA *ch,char *txt) {
    return rp_general_trigger6_bool(RTRIG_PRELOOK_ED,room,ch,txt);
}
void	rp_look_ed_trigger(ROOM_DATA *room,CHAR_DATA *ch,char *txt) {
    rp_general_trigger6(RTRIG_LOOK_ED,room,ch,txt);
}

//
// enter-leave triggers
//
void	rp_enter_trigger(ROOM_DATA *room,CHAR_DATA *ch,int dir) {
    char txt[INT_STRING_BUF_SIZE];
    itoa_r(dir,txt,sizeof(txt));
    rp_general_trigger6(RTRIG_ENTER,room,ch,txt);
}
bool	rp_leave_trigger(ROOM_DATA *room,CHAR_DATA *ch,int dir) {
    char txt[INT_STRING_BUF_SIZE];
    itoa_r(dir,txt,sizeof(txt));
    return rp_general_trigger6_bool(RTRIG_LEAVE,room,ch,txt);
}

//
// trap triggers
//
bool	rp_pretrap_trigger(ROOM_DATA *room,CHAR_DATA *ch,int dir) {
    char txt[INT_STRING_BUF_SIZE];
    itoa_r(dir,txt,sizeof(txt));
    return rp_general_trigger6_bool(RTRIG_PRETRAP,room,ch,txt);
}
void	rp_trap_trigger(ROOM_DATA *room,CHAR_DATA *ch,int dir) {
    char txt[INT_STRING_BUF_SIZE];
    itoa_r(dir,txt,sizeof(txt));
    rp_general_trigger6(RTRIG_TRAP,room,ch,txt);
}

//
// timed triggers
//
void	rp_hour_trigger(ROOM_DATA *room,int hour) {
    char buf[INT_STRING_BUF_SIZE];
    itoa_r(hour,buf,sizeof(buf));
    rp_general_trigger6(RTRIG_HOUR,room,NULL,buf);
}
void	rp_delay_trigger(ROOM_DATA *room) {
    rp_general_trigger1(RTRIG_DELAY,room);
}
void	rp_timer_trigger(ROOM_DATA *room) {
    char buf[LONG_STRING_BUF_SIZE];
    ltoa_r(room->rprog_timer,buf,sizeof(buf));
    rp_general_trigger6(RTRIG_TIMER,room,NULL,buf);
}
void	rp_random_trigger(ROOM_DATA *room) {
    rp_general_trigger1(RTRIG_RANDOM,room);
}

//
// interpret triggers
//
bool	rp_interpret_unknown(CHAR_DATA *ch,char *string) {
    if (ROOM_HAS_TRIGGER(ch->in_room,RTRIG_INTERPRET_UNKNOWN))
	return !rp_general_trigger6_bool(RTRIG_INTERPRET_UNKNOWN,
							ch->in_room,ch,string);
    else
	return FALSE;
}
bool	rp_interpret_preknown(CHAR_DATA *ch,char *string) {
    if (ROOM_HAS_TRIGGER(ch->in_room,RTRIG_INTERPRET_PREKNOWN))
	return !rp_general_trigger6_bool(RTRIG_INTERPRET_PREKNOWN,
							ch->in_room,ch,string);
    else
	return FALSE;
}
void	rp_interpret_postknown(CHAR_DATA *ch,char *string) {
    if (ROOM_HAS_TRIGGER(ch->in_room,RTRIG_INTERPRET_POSTKNOWN))
	rp_general_trigger6(RTRIG_INTERPRET_POSTKNOWN,ch->in_room,ch,string);
}

bool    rp_presocial_trigger(CHAR_DATA *ch,char *string,CHAR_DATA *victim) {
    if (ROOM_HAS_TRIGGER(ch->in_room,RTRIG_PRESOCIAL))
	return !rp_general_trigger7_bool(RTRIG_PRESOCIAL,ch->in_room,ch,victim,string);
    else
	return FALSE;
}
void    rp_social_trigger(CHAR_DATA *ch,char *string,CHAR_DATA *victim) {
    if (ROOM_HAS_TRIGGER(ch->in_room,RTRIG_SOCIAL))
	rp_general_trigger7(RTRIG_SOCIAL,ch->in_room,ch,victim,string);
}

//
// recall triggers
//
bool	rp_prerecall_trigger(ROOM_DATA *room,CHAR_DATA *ch) {
    return rp_general_trigger2_bool(RTRIG_PRERECALL,room,ch);
}
void	rp_recall_trigger(ROOM_DATA *room,CHAR_DATA *ch) {
    rp_general_trigger2(RTRIG_RECALL,room,ch);
}
void	rp_recallto_trigger(ROOM_DATA *room,CHAR_DATA *ch) {
    rp_general_trigger2(RTRIG_RECALL_TO,room,ch);
}

//
// day / sun triggers
//
void	rp_doallrooms(int trigger,char *text) {
    ROOM_DATA *room;
    ROOM_DATA *room_next;
    int i;

    for (i=0;i<MAX_KEY_HASH;i++)
	for (room=room_index_hash[i];room!=NULL;room=room_next) {
	    room_next=room->next;

	    if (ROOM_HAS_TRIGGER(room,trigger))
		rp_general_trigger_all(room,trigger,NULL,NULL,text,NULL,NULL);
	    room=room_next;
	}
}
void    rp_sunset_trigger(void) {
    rp_doallrooms(RTRIG_SUNSET,NULL);
}
void    rp_sunrise_trigger(void) {
    rp_doallrooms(RTRIG_SUNRISE,NULL);
}
void    rp_dayend_trigger(void) {
    rp_doallrooms(RTRIG_DAYEND,NULL);
}
void    rp_daystart_trigger(void) {
    rp_doallrooms(RTRIG_DAYSTART,NULL);
}

void    rp_weather_trigger(int old_state, int new_state) {
    char *old_descr,*new_descr;
    char *text;

    old_descr=table_find_value(old_state,weather_sky_table);
    new_descr=table_find_value(new_state,weather_sky_table);
    text=malloc(strlen(old_descr)+strlen(new_descr)+2);

    strcpy(text,new_descr);
    strcat(text," ");
    strcat(text,old_descr);
    rp_doallrooms(RTRIG_WEATHER,text);
    free(text);
}

//
// lock and unlock triggers
//
bool    rp_canlock_trigger(ROOM_DATA *room,CHAR_DATA *ch,int dir) {
    char txt[INT_STRING_BUF_SIZE];
    itoa_r(dir,txt,sizeof(txt));
    return rp_general_trigger6_bool(RTRIG_CANLOCK,room,ch,txt);
}

bool    rp_prelock_trigger(ROOM_DATA *room,CHAR_DATA *ch,int dir) {
    char txt[INT_STRING_BUF_SIZE];
    itoa_r(dir,txt,sizeof(txt));
    return rp_general_trigger6_bool(RTRIG_PRELOCK,room,ch,txt);
}

void    rp_lock_trigger(ROOM_DATA *room,CHAR_DATA *ch,int dir) {
    char txt[INT_STRING_BUF_SIZE];
    itoa_r(dir,txt,sizeof(txt));
    rp_general_trigger6(RTRIG_LOCK,room,ch,txt);
}

bool    rp_canunlock_trigger(ROOM_DATA *room,CHAR_DATA *ch,int dir) {
    char txt[INT_STRING_BUF_SIZE];
    itoa_r(dir,txt,sizeof(txt));
    return rp_general_trigger6_bool(RTRIG_CANUNLOCK,room,ch,txt);
}

bool    rp_preunlock_trigger(ROOM_DATA *room,CHAR_DATA *ch,int dir) {
    char txt[INT_STRING_BUF_SIZE];
    itoa_r(dir,txt,sizeof(txt));
    return rp_general_trigger6_bool(RTRIG_PREUNLOCK,room,ch,txt);
}

void    rp_unlock_trigger(ROOM_DATA *room,CHAR_DATA *ch,int dir) {
    char txt[INT_STRING_BUF_SIZE];
    itoa_r(dir,txt,sizeof(txt));
    rp_general_trigger6(RTRIG_UNLOCK,room,ch,txt);
}

