/* $Id: act_comm.c,v 1.148 2009/04/22 18:25:38 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 **************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

/*
 * 19971217 EG	Modified do_group() to support the new OOL rules.
 * 19971219 EG	Modified do_quit() to use argument as gossip just before
		the player leaves.
 */

#include "merc.h"
#include "color.h"
#include "interp.h"
#include "fd_mxp.h"
#include "fd_screen.h"

/* RT code to delete yourself */

void do_delet( CHAR_DATA *ch, char *argument)
{
    send_to_char("You must type the full command to delete yourself.\n\r",ch);
}

void do_delete( CHAR_DATA *ch, char *argument)
{
   char strsave[MAX_INPUT_LENGTH];
   char *name;

   if (IS_NPC(ch)) return;
   if (check_ordered(ch)) return;

   if (ch->pcdata->confirm_delete)
   {
	if (strcmp(argument,ch->name)) // case sensitive on purpose
	{
	    send_to_char("Delete status removed.\n\r",ch);
	    ch->pcdata->confirm_delete = FALSE;
	    return;
	}
	else
	{
	    name=strdup(ch->name);
	    mud_data.players_deleted++;

    	    sprintf(strsave, "%s/%s",
		mud_data.player_dir,
		capitalize( ch->name));

	    wiznet(WIZ_NONE,0,ch,NULL,"$N turns $Mself into line noise.");
	    stop_fighting(ch,TRUE);
	    act("$n screams and turns into a pile of dust.",ch,NULL,NULL,TO_ROOM);
	    if (ch->clan) {
		clan_removemember(ch->clan);
		ch->clan=NULL;
	    }
	    do_quit(ch,"");
	    unlink(strsave);
	    extract_owned_obj(name,0);
	    free(name);
	    return;
 	}
    }

    if (strcmp(argument,ch->name)) // case sensitive on purpose
    {
	sprintf_to_char(ch,"Just type 'delete %s'. (case sensitive)\n\r",ch->name);
	return;
    }

    sprintf_to_char(ch,"Type 'delete %s' again to confirm this command.\n\r",ch->name);
    send_to_char("WARNING: this command is irreversible.\n\r",ch);
    send_to_char("Typing delete with any other argument (or none) will undo delete status.\n\r",
	ch);
    ch->pcdata->confirm_delete = TRUE;
    wiznet(WIZ_NONE,get_trust(ch),ch,NULL,"$N is contemplating deletion.");
}

/* RT deaf blocks out all shouts */

void do_deaf( CHAR_DATA *ch, char *argument) {
    if (STR_IS_SET(ch->strbit_comm,COMM_DEAF)) {
	send_to_char("You can now hear tells again.\n\r",ch);
	STR_REMOVE_BIT(ch->strbit_comm,COMM_DEAF);
    } else {
	send_to_char("From now on, you won't hear tells.\n\r",ch);
	STR_SET_BIT(ch->strbit_comm,COMM_DEAF);
    }
}

/* RT quiet blocks out all communication */

void do_quiet ( CHAR_DATA *ch, char * argument) {
    if (STR_IS_SET(ch->strbit_comm,COMM_QUIET)) {
	send_to_char("Quiet mode removed.\n\r",ch);
	STR_REMOVE_BIT(ch->strbit_comm,COMM_QUIET);
    } else {
	send_to_char("From now on, you will only hear says and emotes.\n\r",ch);
	STR_SET_BIT(ch->strbit_comm,COMM_QUIET);
    }
}

/* afk command */

void do_afk ( CHAR_DATA *ch, char * argument)
{
    char orig_arg[MAX_STRING_LENGTH];
    char arg[MAX_STRING_LENGTH];
    char arg2[MAX_STRING_LENGTH];
    char afktext[MAX_STRING_LENGTH];
    char buf[MAX_STRING_LENGTH];
    int i;
    if (IS_NPC(ch))
	return;

    if (STR_IS_SET(ch->strbit_comm,COMM_AFK)) {
	send_to_char("You feel your keyboard beckoning you to return.\n\r",ch);
	if (buf_string(ch->pcdata->buffer)[0] != '\0')
	    sprintf_to_char(ch,
		"Type '{Wreplay{x' to see your %d missed tell%s.\n\r",
		ch->pcdata->missedtells,
		ch->pcdata->missedtells==1?"":"s");
	else
	    send_to_char("No tells received.\n\r",ch);
	act("$n is back from real life.",ch,NULL,NULL,TO_ROOM);
	STR_REMOVE_BIT(ch->strbit_comm,COMM_AFK);
	STR_REMOVE_BIT(ch->strbit_comm,COMM_AFK_BY_IDLE);
	if(!IS_NPC(ch) && ch->pcdata->afk_text) {
	    free_string(ch->pcdata->afk_text);
	    ch->pcdata->afk_text=NULL;
	}
	return;
    } 

    strcpy(orig_arg,argument);
    argument=one_argument(argument,arg);
    strcpy(afktext,argument);	// saved in case of "message" keyword
    argument=one_argument(argument,arg2);

    switch (which_keyword(arg,"timeout","message","automsg",NULL)) {
    case -1:		// no arg
	afktext[0]=0;
	GetCharProperty(ch,PROPERTY_STRING,"afk-message",afktext);
	strcpy(orig_arg,afktext);
	// fallthrough
    case 0:		// keyword not in list
	send_to_char("You quietly slink away from the keyboard "
	    "back to reality.\n\r",ch);
	act("$n quietly slinks away from the keyboard back to reality.",
	    ch,NULL,NULL,TO_ROOM);
	STR_SET_BIT(ch->strbit_comm,COMM_AFK);

	if (orig_arg[0]) {
	    if(orig_arg[0]!=',' && orig_arg[0]!=','
	    && orig_arg[0]!='!' && orig_arg[0]!='?') {
		buf[0]=' ';
		strcpy(buf+1,orig_arg);
	    } else
		strcpy(buf,orig_arg);
	    free_string(ch->pcdata->afk_text);
	    if(strlen(buf)>41) buf[41]='\0';
	    ch->pcdata->afk_text=str_dup(buf);
	}
	break;

    case 1:
	if (!arg2[0]) {
	    i=12;arg2[0]=0;
	    GetCharProperty(ch,PROPERTY_INT,"afk-timeout",&i);
	    if (i!=12)
		sprintf_to_char(ch,
		    "Your time-out is %d ticks, default is 12 ticks\n\r",i);
	    else
		sprintf_to_char(ch,
		    "Your time-out is the default, 12 ticks\n\r",i);
	    GetCharProperty(ch,PROPERTY_STRING,"afk-message",arg2);
	    if (arg2[0])
		sprintf_to_char(ch,
		    "Your AFK message is '%s'.\n\r",arg2);
	    else
		send_to_char("You have no AFK message.\n\r",ch);
	    GetCharProperty(ch,PROPERTY_STRING,"auto-afk-message",arg2);
	    if (arg2[0])
		sprintf_to_char(ch,
		    "Your AUTO-AFK message is '%s'.\n\r",arg2);
	    return;
	}
	i=atoi(arg2);
	if (!is_number(arg2) || i<=0) {
	    send_to_char(
		"Timeout should be a number larger than 0\n\r",ch);
	    return;
	}
	if (i>20) {
	    send_to_char("Get real! Timeout cannot be larger than 20.\n\r",ch);
	    return;
	}
	sprintf_to_char(ch,
	    "Timeout for auto AFK set to %d ticks.\n\r",i);
	SetCharProperty(ch,PROPERTY_INT,"afk-timeout",&i);
	break;

    case 2:
	if (afktext[0]) {
	    sprintf_to_char(ch,"Setting AFK text to '%s'\n\r",afktext);
	    SetCharProperty(ch,PROPERTY_STRING,
		"afk-message",afktext);
	} else {
	    send_to_char("Removing AFK text\n\r",ch);
	    DeleteCharProperty(ch,PROPERTY_STRING,"afk-message");
	}
	break;
    case 3:
	if (afktext[0]) {
	    sprintf_to_char(ch,"Setting AUTO-AFK text to '%s'\n\r",afktext);
	    SetCharProperty(ch,PROPERTY_STRING,
		"auto-afk-message",afktext);
	} else {
	    send_to_char("Removing AUTO-AFK text\n\r",ch);
	    DeleteCharProperty(ch,PROPERTY_STRING,"auto-afk-message");
	}
	break;

    }
}

void set_afk_state(CHAR_DATA *ch, bool go_afk, bool manual) {
    if (!go_afk) {
	if (STR_IS_SET(ch->strbit_comm,COMM_AFK)) {
	    send_to_char("You feel your keyboard beckoning you to return.\n\r",ch);
	    if (buf_string(ch->pcdata->buffer)[0] != '\0')
		sprintf_to_char(ch,
		    "Type '{Wreplay{x' to see your %d missed tell%s.\n\r",
		    ch->pcdata->missedtells,
		    ch->pcdata->missedtells==1?"":"s");
	    else
		send_to_char("No tells received.\n\r",ch);
	    act("$n is back from real life.",ch,NULL,NULL,TO_ROOM);
	    STR_REMOVE_BIT(ch->strbit_comm,COMM_AFK);
	    STR_REMOVE_BIT(ch->strbit_comm,COMM_AFK_BY_IDLE);
	    if(!IS_NPC(ch) && ch->pcdata->afk_text) {
		free_string(ch->pcdata->afk_text);
		ch->pcdata->afk_text=NULL;
	    }
	} 
    } else {
	char afktext[MAX_STRING_LENGTH];

	afktext[0]=0;
	if (!manual) {
	    GetCharProperty(ch,PROPERTY_STRING,"auto-afk-message",afktext);
	}

	if (!afktext[0])
	    GetCharProperty(ch,PROPERTY_STRING,"afk-message",afktext);

	send_to_char("You quietly slink away from the keyboard "
	    "back to reality.\n\r",ch);
	act("$n quietly slinks away from the keyboard back to reality.",
	    ch,NULL,NULL,TO_ROOM);
	STR_SET_BIT(ch->strbit_comm,COMM_AFK);
	if (!manual)
	    STR_SET_BIT(ch->strbit_comm,COMM_AFK_BY_IDLE);

	if (afktext[0]) {
	    int len=strlen(afktext);
	    char *buf;

	    if (len>40) len=40;

	    free_string(ch->pcdata->afk_text);
	    if (isalpha(afktext[0])) {
		ch->pcdata->afk_text=buf=alloc_mem(len+2);
		buf[0]=' ';
		strncpy(buf+1,afktext,len);
		buf[len+1]=0;
	    } else {
		ch->pcdata->afk_text=buf=alloc_mem(len+1);
		strncpy(buf,afktext,len);
		buf[len]=0;
	    }
	}
    }

}

void do_replay (CHAR_DATA *ch, char *argument) {

    if (IS_NPC(ch)) {
	send_to_char("You can't replay.\n\r",ch);
	return;
    }

    if (buf_string(ch->pcdata->buffer)[0] == '\0') {
	send_to_char("You have no tells to replay.\n\r",ch);
	return;
    }

    page_to_char(buf_string(ch->pcdata->buffer),ch);
    clear_buf(ch->pcdata->buffer);
    ch->pcdata->missedtells=0;
}

/* The drunk code.. Drunk people talk very strange :) */

char *makedrunk (CHAR_DATA *ch, char *argument,bool forced)
{
    static struct drunk_data
    {
	int     min_drunk_level;
	int     number_of_rep;
	char    *replacement[11];
    } drunk[] = {
{3, 10,	{"a", "a", "a", "A", "aa", "ah", "Ah", "ao", "aw", "oa", "ahhhh"} },
{8, 5,	{"b", "b", "b", "B", "B", "vb"}					},
{3, 5,	{"c", "c", "C", "cj", "sj", "zj"}				},
{5, 2,	{"d", "d", "D"}							},
{3, 3,	{"e", "e", "eh", "E"}						},
{4, 5,	{"f", "f", "ff", "fff", "fFf", "F"}				},
{8, 2,	{"g", "g", "G"}							},
{9, 6,	{"h", "h", "hh", "hhh", "Hhh", "HhH", "H"}			},
{7, 6,	{"i", "i", "Iii", "ii", "iI", "Ii", "I"}			},
{9, 5,	{"j", "j", "jj", "Jj", "jJ", "J"}				},
{7, 2,	{"k", "k", "K"}							},
{3, 2,	{"l", "l", "L"}							},
{5, 8,	{"m", "m", "mm", "mmm", "mmmm", "mmmmm", "MmM", "mM", "M"}	},
{6, 6,	{"n", "n", "nn", "Nn", "nnn", "nNn", "N"}			},
{3, 6,	{"o", "o", "ooo", "ao", "aOoo", "Ooo", "ooOo"}			},
{3, 2,	{"p", "p", "P"}							},
{5, 5,	{"q", "q", "Q", "ku", "ququ", "kukeleku"}			},
{4, 2,	{"r", "r", "R"}							},
{2, 5,	{"s", "ss", "zzZzssZ", "ZSssS", "sSzzsss", "sSss"}		},
{5, 2,	{"t", "t", "T"}							},
{3, 6,	{"u", "u", "uh", "Uh", "Uhuhhuh", "uhU", "uhhu"}		},
{4, 2,	{"v", "v", "V"}							},
{4, 2,	{"w", "w", "W"}							},
{5, 6,	{"x", "x", "X", "ks", "iks", "kz", "xz"}			},
{3, 2,	{"y", "y", "Y"}							},
{2, 9, {"z", "z", "ZzzZz", "Zzz", "Zsszzsz", "szz", "sZZz", "ZSz", "zZ", "Z"}}
};
    static char buf[MAX_STRING_LENGTH];
    char temp,i,*string;
    int pos = 0;
    int drunklevel;
    int randomnum;

    /* Check how drunk a person is... */
    drunklevel = IS_NPC(ch) ? 0 : ch->pcdata->condition[COND_DRUNK];
    if (forced) drunklevel=24;

    if (drunklevel > 0)
    {
	string=argument;
	do {
	    temp = toupper (*string);
	    if ( (temp >= 'A') && (temp <= 'Z') ) {
		i=temp-'A';
		if ( drunklevel > drunk[(int)i].min_drunk_level )
		{
			 randomnum = number_range (0, drunk[(int)i].number_of_rep);
			 strcpy (&buf[pos], drunk[(int)i].replacement[randomnum]);
			 pos += strlen (drunk[(int)i].replacement[randomnum]);
		}
		else buf[pos++] = *string;
	    }
	    else if ( (temp >= '0') && (temp <= '9') )
	    {
		temp = '0' + number_range (0, 9);
		buf[pos++] = temp;
	    }
	    else buf[pos++] = *string;
	}
	while(*string++);

	buf[pos] = '\0';          /* Mark end of the string... */
	if(strlen(buf)>MAX_INPUT_LENGTH-1) return argument;
	return buf;
    }

    return argument;
}

char *strip_colors(CHAR_DATA *ch, char *text) {
    static char buf[MAX_STRING_LENGTH];
    int src=0;
    int dst=0;
    int len=strlen(text);

    if ( !STR_IS_SET(ch->strbit_comm, COMM_NOCOLOR) )
	return text;

    while (src<len) {
        if ((text[src]=='{') || (text[src]=='}')) {
	    if ((text[src]==text[src+1]) || (text[src+1]==0)) {
		buf[dst++]=text[src];
	    }
	    src+=2;
	} else
	    buf[dst++]=text[src++];
    }
    buf[dst]=0;
    return buf;
}



void spam_channel(int channelnr,CHAR_DATA *ch,char *argument) {
    CHAR_DATA *		player;
    CHAR_DATA *		victim;
    const CHANNEL_DATA *channel;
    char		buf[MSL];

    channel=&channel_table[channelnr];

    if (argument[0]==0) {
	if (STR_IS_SET(ch->strbit_comm,channel->bit)) {
	    sprintf_to_char(ch,"%s channel is now ON.\n\r",channel->name);
	    STR_REMOVE_BIT(ch->strbit_comm,channel->bit);
	} else {
	    sprintf_to_char(ch,"%s channel is now OFF.\n\r",channel->name);
	    STR_SET_BIT(ch->strbit_comm,channel->bit);
	}
	return;
    }

    if (channel->property!=NULL) {
	bool nothiscomm=FALSE,nocomm=FALSE;

	GetRoomProperty(ch->in_room,PROPERTY_BOOL,channel->property,&nothiscomm);
	GetRoomProperty(ch->in_room,PROPERTY_BOOL,"no-comm",&nocomm);
	if (nothiscomm||nocomm) {
	    send_to_char(
		"This room prevents the use of this kind of communications\n\r",
		ch);
	    return;
	}
    }

    //
    // give me some reasons not to do it
    //
/*
    if (!IS_NPC(ch) && mud_data.newbie_channel_level[channelnr]>ch->level) {
	sprintf_to_char(ch,
	    "You're not allowed to use the %s-channel until you're level %d.\n\r",
	    channel->name,mud_data.newbie_channel_level[channelnr]);
	return;
    }
*/
    if (STR_IS_SET(ch->strbit_comm,COMM_QUIET)) {
	send_to_char("You must turn off quiet mode first.\n\r",ch);
	return;
    }

    if (STR_IS_SET(ch->strbit_comm,COMM_NOCHANNELS)) {
	send_to_char("The gods have revoked your channel privileges.\n\r",ch);
	return;
    }

    if (channel->clanonly && ch->clan==NULL) {
	send_to_char("But you are not a member of a clan!\n\r",ch);
	return;
    }

    if (channel->heroonly && ch->level!=LEVEL_HERO) {
	send_to_char(
	    "Try to use this channel again later when you are a hero.\n\r",ch);
	return;
    }
    if (channel->immonly && !IS_IMMORTAL(ch)) {
	send_to_char("This channel is for immortals only.\n\r",ch);
	return;
    }

    STR_REMOVE_BIT(ch->strbit_comm,channel->bit);

    if (IS_PC(ch) && check_swearing(argument)) {
	logf("[%d] %s swore.",GET_DESCRIPTOR(ch),ch->name);
    }

    argument=makedrunk(ch,argument,FALSE);
    argument=strip_colors(ch,argument);

    if (channel->prefixme!=NULL)
	sprintf(buf,"You %s '%s$t{x'",channel->prefixme,channel->colour);
    else
	sprintf(buf,"$n: %s$t{x",channel->colour);
    act_new(buf,ch,argument,NULL,TO_CHAR,POS_DEAD);

    if (channel->prefixthem!=NULL)
	sprintf(buf,"$n %s '%s$t{x'",channel->prefixthem,channel->colour);
    else
	sprintf(buf,"$n: %s$t{x",channel->colour);

    for ( player = player_list; player != NULL; player = player->next_player ) {
	victim=STR_IS_SET(player->strbit_act,PLR_SWITCHED)
		? player->pcdata->switched_into : player;

	if (victim==ch) continue;
	if (STR_IS_SET(victim->strbit_comm,channel->bit)) continue;
	if (STR_IS_SET(victim->strbit_comm,COMM_QUIET)) continue;

	if (channel->areaonly && ch->in_room->area!=victim->in_room->area)
	    continue;

	if (channel->grouponly && !is_same_group(ch,victim)) continue;

	if (channel->clanonly && !is_same_clan(ch,victim)) continue;

	if (channel->heroonly && victim->level!=LEVEL_HERO) continue;

	if (channel->immonly && !IS_IMMORTAL(victim)) continue;

	if (channel->awakeonly && victim->position<POS_RESTING) continue;

	if (channel->minlevellisten!=0
	&& victim->level<channel->minlevellisten) continue;

	if (STR_IS_SET(victim->strbit_comm,COMM_NONEWBIE)
	&& ch->level<mud_data.newbie_channel_level[channelnr]) continue;

	act_new(buf,ch,argument,victim,TO_VICT,POS_DEAD);
    }
}

void do_gossip( CHAR_DATA *ch,char *argument ) {
    spam_channel(CHANNEL_GOSSIP,ch,argument);
}
void do_music( CHAR_DATA *ch,char *argument ) {
    spam_channel(CHANNEL_MUSIC,ch,argument);
}
void do_auction(CHAR_DATA *ch,char *argument) {
    spam_channel(CHANNEL_AUCTION,ch,argument);
}
void do_question(CHAR_DATA *ch,char *argument) {
    spam_channel(CHANNEL_QUESTION,ch,argument);
}
void do_answer(CHAR_DATA *ch,char *argument) {
    spam_channel(CHANNEL_ANSWER,ch,argument);
}
void do_quest(CHAR_DATA *ch,char *argument) {
    spam_channel(CHANNEL_QUEST,ch,argument);
}
void do_quote(CHAR_DATA *ch,char *argument) {
    spam_channel(CHANNEL_QUOTE,ch,argument);
}
void do_grats(CHAR_DATA *ch,char *argument) {
    spam_channel(CHANNEL_GRATS,ch,argument);
}
void do_pray(CHAR_DATA *ch,char *argument) {
    spam_channel(CHANNEL_PRAY,ch,argument);
}
void do_shout(CHAR_DATA *ch,char *argument) {
    spam_channel(CHANNEL_SHOUT,ch,argument);
}
void do_yell(CHAR_DATA *ch,char *argument) {
    spam_channel(CHANNEL_YELL,ch,argument);
}
void do_gtell(CHAR_DATA *ch,char *argument) {
    spam_channel(CHANNEL_GTELL,ch,argument);
}
void do_herotalk(CHAR_DATA *ch,char *argument) {
    spam_channel(CHANNEL_HEROTALK,ch,argument);
}
void do_clantalk(CHAR_DATA *ch,char *argument) {
    spam_channel(CHANNEL_CLANTALK,ch,argument);
}
void do_immtalk( CHAR_DATA *ch,char *argument ) {
    spam_channel(CHANNEL_IMMTALK,ch,argument);
}

//
// newbie-channel on/off only
//
void do_newbie( CHAR_DATA *ch, char *argument ) {

    if (STR_IS_SET(ch->strbit_comm,COMM_NONEWBIE)) {
        send_to_char("Newbie channels are now ON.\n\r",ch);
        STR_REMOVE_BIT(ch->strbit_comm,COMM_NONEWBIE);
    } else {
	if (ch->level<mud_data.newbie_channel_level[CHANNEL_GOSSIP]) {
	    send_to_char("You can't turn off this channel yet.\n\r",ch);
	    return;
	}
        STR_SET_BIT(ch->strbit_comm,COMM_NONEWBIE);
        send_to_char("Newbie channels are now OFF.\n\r",ch);
    }
}


void announce(CHAR_DATA *ch,const char *string,...) {
    CHAR_DATA *	player;
    CHAR_DATA *	victim;
    va_list	ap;
    char	buf[MSL];

    va_start(ap,string);
    vsprintf(buf,string,ap);
    va_end(ap);

    for ( player = player_list; player != NULL; player = player->next_player ) {
	victim=STR_IS_SET(player->strbit_act,PLR_SWITCHED)
		? player->pcdata->switched_into : player;

        if (STR_IS_SET(victim->strbit_comm,COMM_ANNOUNCE)
	&& !STR_IS_SET(victim->strbit_comm,COMM_NOCHANNELS)
	&& !STR_IS_SET(victim->strbit_comm,COMM_QUIET)
	&& (!ch || can_see(victim,ch))
	&& victim != ch)
            act_new(buf,victim,NULL,ch,TO_CHAR,POS_DEAD);
    }

    return;
}

void do_announce( CHAR_DATA *ch, char *argument )
{
    if (!STR_IS_SET(ch->strbit_comm,COMM_ANNOUNCE)) {
    	send_to_char("Announce channel is now ON.\n\r",ch);
	STR_SET_BIT(ch->strbit_comm,COMM_ANNOUNCE);
    } else {
    	send_to_char("Announce channel is now OFF.\n\r",ch);
	STR_REMOVE_BIT(ch->strbit_comm,COMM_ANNOUNCE);
    }
}

void do_say( CHAR_DATA *ch, char *argument ) {
    char c;

    if ( argument[0] == '\0' ) {
	send_to_char( "Say what?\n\r", ch );
	return;
    }

    MOBtrigger=FALSE;
    c=argument[strlen(argument)-1];
    if (IS_PC(ch) && check_swearing(argument)) {
	logf("[%d] %s swore.",GET_DESCRIPTOR(ch),ch->name);
    }
    argument=makedrunk(ch,argument,FALSE);
    argument=strip_colors(ch,argument);

    //
    // speech trigger for objects
    //
    if (rp_prespeech_trigger(ch,argument)==FALSE)
	return;
    if (op_prespeech_trigger(ch,argument)==FALSE)
	return;
    if (!IS_VALID(ch))
	return;

    switch(c) {
	case '?':
	    act( "You ask '" C_SAY "$T{x'", ch, NULL, argument, TO_CHAR );
	    act( "$n asks '" C_SAY "$T{x'", ch, NULL, argument, TO_ROOM );
	    break;
	case '!':
	    act( "You exclaim '" C_SAY "$T{x'", ch, NULL, argument, TO_CHAR );
	    act( "$n exclaims '" C_SAY "$T{x'", ch, NULL, argument, TO_ROOM );
	    break;
	default :
	    act( "You say '" C_SAY "$T{x'", ch, NULL, argument, TO_CHAR );
	    act( "$n says '" C_SAY "$T{x'", ch, NULL, argument, TO_ROOM );
	    break;
    }
    MOBtrigger=TRUE;

    if (!IS_NPC(ch))
	mp_speech_trigger(ch,argument);

    //
    // speech trigger for objects
    //
    op_speech_trigger(ch,argument);
    rp_speech_trigger(ch,argument);

}



void do_mmmm( CHAR_DATA *ch, char *argument ) {
    if ( argument[0] == '\0' ) {
	check_social( ch, "mmmm", argument );
	return;
    }

    MOBtrigger=FALSE;
    argument=makedrunk(ch,argument,FALSE);
    argument=strip_colors(ch,argument);

    //
    // speech trigger for objects
    //
    if (op_prespeech_trigger(ch,argument)==FALSE)
	return;
    if (!IS_VALID(ch))
	return;

    act( "You say '" C_SAY "MmmMMMMmmmMMMmMMMmmm... $T{x'", ch, NULL, argument, TO_CHAR );
    act( "$n says '" C_SAY "MmmMMMMmmmMMMmMMMmmm... $T{x'", ch, NULL, argument, TO_ROOM );
    check_social( ch, "drool", "" );

    MOBtrigger=TRUE;

    if (!IS_NPC(ch))
	mp_speech_trigger(ch,argument);
    //
    // speech trigger for objects
    //
    op_speech_trigger(ch,argument);
}



void do_tell( CHAR_DATA *ch,char *argument )
{
    char arg[MAX_INPUT_LENGTH],buf[MAX_STRING_LENGTH];
    char person[MIL],*pperson;
    CHAR_DATA *victim;
    CLAN_INFO *clan;

    if (STR_IS_SET(ch->strbit_comm, COMM_NOTELL) ||
	STR_IS_SET(ch->strbit_comm,COMM_DEAF))
    {
	send_to_char( "Your message didn't get through.\n\r", ch );
	return;
    }

    if ( STR_IS_SET(ch->strbit_comm, COMM_QUIET) )
    {
	send_to_char( "You must turn off quiet mode first.\n\r", ch);
	return;
    }

    if (STR_IS_SET(ch->strbit_comm,COMM_DEAF))
    {
	send_to_char("You must turn off deaf mode first.\n\r",ch);
	return;
    }

    argument = one_argument( argument, arg );
    if ( arg[0] == '\0' || argument[0] == '\0' )
    {
	send_to_char( "Tell whom what?\n\r", ch );
	return;
    }

    /*
     * Can tell to PC's anywhere, but NPC's only in same room.
     * -- Furey
     */

    if (IS_PC(ch) && check_swearing(argument)) {
	logf("[%d] %s swore.",GET_DESCRIPTOR(ch),ch->name);
    }

    argument=makedrunk(ch,argument,FALSE);
    argument=strip_colors(ch,argument);

    pperson=arg;

    while (1) {

	// do it here, otherwise it will have to be checked too often
	pperson=one_argument(pperson,person);
	if (person[0]==0)
	    break;

	// tell <clan> <msg>
	if ((clan=get_clan_by_name(person))!=NULL) {
	    CHAR_DATA *player,*victim;

	    sprintf(buf,"$n tells the %s clan '" C_TELL "$t{x'",clan->name);
	    for ( player = player_list; player != NULL; player = player->next_player ) {

		victim=STR_IS_SET(player->strbit_act,PLR_SWITCHED)
			? player->pcdata->switched_into : player;

		if ( victim != ch &&
		     victim->clan &&
		     victim->clan->clan_info==clan &&
		     !STR_IS_SET(victim->strbit_comm,COMM_NOCLAN) &&
		     !STR_IS_SET(victim->strbit_comm,COMM_NOTELL) &&
		     !STR_IS_SET(victim->strbit_comm,COMM_QUIET) ) {
		    act_new(buf,ch,argument,victim,TO_VICT,POS_DEAD);
		}
	    }
	    act("You tell the $t clan: '"C_TELL"$T{x'",ch,clan->name,argument,TO_CHAR);
	    continue;
	}

	// tell '<persons>' <msg>
	// tell <person> <msg>

	if ( ( victim = get_char_world( ch, person ) ) == NULL
	|| ( IS_NPC(victim) && victim->in_room != ch->in_room ) ) {
	    sprintf_to_char(ch,"%s isn't here.\n\r",capitalize(person));
	    continue;
	}

	if (victim==ch) {
	    send_to_char("Talking to yourself?\n\r",ch);
	    continue;
	}

	if ( victim->desc == NULL && !IS_NPC(victim)) {
	    act("$N seems to have misplaced $S link...try again later.",
		ch,NULL,victim,TO_CHAR);
	    sprintf(buf,"%s told you '"C_TELL"%s{x'\n\r",
		PERS(ch,victim),argument);
	    buf[0] = UPPER(buf[0]);
	    add_buf(victim->pcdata->buffer,buf);
	    victim->pcdata->missedtells++;
	    continue;
	}

	if ((STR_IS_SET(victim->strbit_comm,COMM_QUIET) ||
	    STR_IS_SET(victim->strbit_comm,COMM_DEAF))
	&& !IS_IMMORTAL(ch)) {
	    act( "$N is not receiving tells.", ch, 0, victim, TO_CHAR );
	    continue;
	}

	if (STR_IS_SET(victim->strbit_comm,COMM_AFK)) {
	    if (IS_NPC(victim)) {
		act("$N is AFK, and not receiving tells.",
		    ch,NULL,victim,TO_CHAR);
		continue;
	    }

	    act("$N is AFK, but your tell will go through when $E returns.",
		ch,NULL,victim,TO_CHAR);
	    sprintf(buf,"%s told you '" C_TELL "%s{x'\n\r",
		PERS(ch,victim),argument);
	    buf[0] = UPPER(buf[0]);
	    add_buf(victim->pcdata->buffer,buf);
	    victim->pcdata->missedtells++;
	    victim->reply=ch;
	    continue;
	}

	MOBtrigger=FALSE;
	act_new("You tell $N '" C_TELL "$t{x'",
	    ch,argument,victim,TO_CHAR,POS_DEAD);
	act_new("$n tells you '" C_TELL "$t{x'",
	    ch,argument,victim,TO_VICT,POS_DEAD);
	MOBtrigger=TRUE;
	victim->reply	= ch;

	if ( !IS_NPC(ch) && IS_NPC(victim) && MOB_HAS_TRIGGER(victim,MTRIG_SPEECH) )
	    mp_tell_trigger(ch,victim,argument);

    }
    return;
}



void do_reply( CHAR_DATA *ch, char *argument )
{
    CHAR_DATA *victim;
    char buf[MAX_STRING_LENGTH];

    if ( STR_IS_SET(ch->strbit_comm, COMM_NOTELL) )
    {
	send_to_char( "Your message didn't get through.\n\r", ch );
	return;
    }

    if ( ( victim = ch->reply ) == NULL )
    {
	send_to_char( "They aren't here.\n\r", ch );
	return;
    }

    if (IS_PC(ch) && check_swearing(argument)) {
	logf("[%d] %s swore.",GET_DESCRIPTOR(ch),ch->name);
    }
    argument=makedrunk(ch,argument,FALSE);
    argument=strip_colors(ch,argument);

    if (argument[0]==0) {
	send_to_char("Tell what?\n\r",ch);
	return;
    }

    if ( victim->desc == NULL && !IS_NPC(victim))
    {
        act("$N seems to have misplaced $S link...try again later.",
            ch,NULL,victim,TO_CHAR);
        sprintf(buf,"%s told you '" C_REPLY "%s{x'\n\r",PERS(ch,victim),argument);
        buf[0] = UPPER(buf[0]);
        add_buf(victim->pcdata->buffer,buf);
	victim->pcdata->missedtells++;
        return;
    }

    if ((STR_IS_SET(victim->strbit_comm,COMM_QUIET) ||
	STR_IS_SET(victim->strbit_comm,COMM_DEAF))
    &&  !IS_IMMORTAL(ch) && !IS_IMMORTAL(victim))
    {
        act_new( "$E is not receiving tells.", ch, 0, victim, TO_CHAR,POS_DEAD);
        return;
    }

    if (STR_IS_SET(victim->strbit_comm,COMM_AFK))
    {
        if (IS_NPC(victim))
        {
            act_new("$E is AFK, and not receiving tells.",
		ch,NULL,victim,TO_CHAR,POS_DEAD);
            return;
        }

        act_new("$E is AFK, but your tell will go through when $E returns.",
            ch,NULL,victim,TO_CHAR,POS_DEAD);
        sprintf(buf,"%s told you '" C_REPLY "%s{x'\n\r",PERS(ch,victim),argument);
	buf[0] = UPPER(buf[0]);
        add_buf(victim->pcdata->buffer,buf);
	victim->pcdata->missedtells++;
        return;
    }

    act_new("You tell $N '" C_REPLY "$t{x'",ch,argument,victim,TO_CHAR,POS_DEAD);
    act_new("$n tells you '" C_REPLY "$t{x'",ch,argument,victim,TO_VICT,POS_DEAD);
    victim->reply	= ch;

    return;
}



void do_emote( CHAR_DATA *ch, char *argument )
{
    if ( !IS_NPC(ch) && STR_IS_SET(ch->strbit_comm, COMM_NOEMOTE) )
    {
        send_to_char( "You can't show your emotions.\n\r", ch );
        return;
    }

    if ( argument[0] == '\0' )
    {
        send_to_char( "Emote what?\n\r", ch );
        return;
    }

    if (IS_PC(ch) && check_swearing(argument)) {
      logf("[%d] %s swore.",GET_DESCRIPTOR(ch),ch->name);
    }

    MOBtrigger=FALSE;
    act( "$n $T", ch, NULL, argument, TO_ROOM );
    act( "$n $T", ch, NULL, argument, TO_CHAR );
    MOBtrigger=TRUE;
    return;
}


void do_pmote( CHAR_DATA *ch, char *argument )
{
    CHAR_DATA *vch;
    char *letter,*name;
    char last[MAX_INPUT_LENGTH], temp[MAX_STRING_LENGTH];
    int matches = 0;

    if ( !IS_NPC(ch) && STR_IS_SET(ch->strbit_comm, COMM_NOEMOTE) )
    {
        send_to_char( "You can't show your emotions.\n\r", ch );
        return;
    }

    if ( argument[0] == '\0' )
    {
        send_to_char( "Emote what?\n\r", ch );
        return;
    }

    if (IS_PC(ch) && check_swearing(argument)) {
	logf("[%d] %s swore.",GET_DESCRIPTOR(ch),ch->name);
    }
    act( "$n $t", ch, argument, NULL, TO_CHAR );

    for (vch = ch->in_room->people; vch != NULL; vch = vch->next_in_room)
    {
	if (vch->desc == NULL || vch == ch)
	    continue;

	if ((letter = strstr(argument,vch->name)) == NULL)
	{
	    act("$N $t",vch,argument,ch,TO_CHAR);
	    continue;
	}

	strcpy(temp,argument);
	temp[strlen(argument) - strlen(letter)] = '\0';
   	last[0] = '\0';
 	name = vch->name;

	for (; *letter != '\0'; letter++)
	{
	    if (*letter == '\'' && matches == strlen(vch->name))
	    {
		strcat(temp,"r");
		continue;
	    }

	    if (*letter == 's' && matches == strlen(vch->name))
	    {
		matches = 0;
		continue;
	    }

 	    if (matches == strlen(vch->name))
	    {
		matches = 0;
	    }

	    if (*letter == *name)
	    {
		matches++;
		name++;
		if (matches == strlen(vch->name))
		{
		    strcat(temp,"you");
		    last[0] = '\0';
		    name = vch->name;
		    continue;
		}
		strncat(last,letter,1);
		continue;
	    }

	    matches = 0;
	    strcat(temp,last);
	    strncat(temp,letter,1);
	    last[0] = '\0';
	    name = vch->name;
	}

	act("$N $t",vch,temp,ch,TO_CHAR);
    }

    return;
}


/*
 * All the posing stuff.
 */
struct	pose_table_type
{
    char *	message[2*MAX_CLASS];
};

//
// The NOTYETDEFs are related to the new class system
//
//
const	struct	pose_table_type	pose_table	[]	=
{
    {
	{
	    "You sizzle with energy.",
	    "$n sizzles with energy.",
	    "You feel very holy.",
	    "$n looks very holy.",
	    "You perform a small card trick.",
	    "$n performs a small card trick.",
	    "You show your bulging muscles."
#ifdef NOTYETDEF
	    ,
	    "$n shows $s bulging muscles.",
	    "You conjure up strange lights.",
	    "$n is surrounded by strange lights.",
	    "You draw mana from the air.",
	    "$n draws mana from the very air.",
	    "You weave intricate patterns with your hands.",
	    "$n weaves intricate patterns with your hands.",
	    "An energy bolt sits on your shoulder, waves, and disappears.",
	    "An energy bolt sits on $n's shoulder, waves, and disappears.",
	    "You show the healing power of sand.",
	    "$n shows the healing power of sand.",
	    "You scribe down the forces of Nature on a scroll.",
	    "$n scribes down the forces of Nature on a scroll.",
	    "You revitalize the atmosphere.",
	    "$n revitalizes the atmosphere.",
	    "You balance your weapon on your nose.",
	    "$n balances $s weapon on $s nose.",
	    "You sum up everyone's possessions.",
	    "$n sums up your possessions.",
	    "You point at an insect and it faints.",
	    "$n points at an insect and it faints.",
	    "You recite the story of Beowulf and everybody is sad.",
	    "$n recites the story of Beowulf, making you feel sad.",
	    "You turn the coins in the others purses.",
	    "$n turns the coins in your purse.",
	    "You do some shadow boxing.",
	    "$n does some shadow boxing.",
	    "You do a backward salto.",
	    "$n does a backward salto.",
	    "You eat a piece of rock.",
	    "$n eats a piece of rock.",
	    "Your warriors fist makes the earth tremble.",
	    "$n's warrior fist makes the earth tremble."
#endif
	}
    },

    {
	{
	    "You turn into a butterfly, then return to your normal shape.",
	    "$n turns into a butterfly, then returns to $s normal shape.",
	    "You nonchalantly turn wine into water.",
	    "$n nonchalantly turns wine into water.",
	    "You wiggle your ears alternately.",
	    "$n wiggles $s ears alternately.",
	    "You crack nuts between your fingers.",
	    "$n cracks nuts between $s fingers."
#ifdef NOTYETDEF
,
	    "You make a massive foot drop from the sky and bounce back up.",
	    "$n makes a massive foot drop from the sky and bounce back up.",
	    "The astral planes shift at your command.",
	    "The astral planes shift at $n's command.",
	    "You resurrect a dead ant, to make it continue its mindless business.",
	    "$n resurrects a dead ant. Happy, it scurries away.",
	    "Lightning flashes between your fingertops, leaving a burning smell.",
	    "Lightning flashes between $n's fingertops... What's that smell?",
	    "Flowers and herbs pop into existence all around you.",
	    "$n is surrounded by flowers and herbs.",
	    "You throw a snowball of condensed mana --==woosh!",
	    "$n throws a snowball of condensed mana at you: splat!",
	    "Nature begs you for advice.",
	    "Nature begs $n for advice.",
	    "You do the hand-knife trick at amazing speed.",
	    "$n flicks a knife between $s outstretched fingers at amazing speed.",
	    "You read tracks and tell their history.",
	    "$n reads your tracks and tells you from where you came.",
	    "You point out deadly spots where you could kill someone.",
	    "$n points out deadly spots on your body where you are vulnerable.",
	    "Everyone listens in rapture to your song of the Old Ones.",
	    "You listen in rapture to $n's song of the Old Ones.",
	    "You tell the colour of everybody's underwear.",
	    "$n tells the colour of your underwear.",
	    "You stand and stretch your body to the outer limits.",
	    "$n stands and stretches $s body to impossible lengths.",
	    "With a quick movement, your hand catches a fly in flight.",
	    "With eyes closed, $n catches a fly in flight.",
	    "You lift yourself on one hand.",
	    "$n lifts $mself on one hand.",
	    "You beckon a little bird on your hand and then break its neck.",
	    "$n beckons a little bird on $s hand and then breaks its neck."
#endif
	}
    },

    {
	{
	    "Blue sparks fly from your fingers.",
	    "Blue sparks fly from $n's fingers.",
	    "A halo appears over your head.",
	    "A halo appears over $n's head.",
	    "You nimbly tie yourself into a knot.",
	    "$n nimbly ties $mself into a knot.",
	    "You grizzle your teeth and look mean.",
	    "$n grizzles $s teeth and looks mean."
#ifdef NOTYETDEF
,
	    "You point at an ant, and it becomes a mouse.",
	    "$n points at an ant, and it becomes a mouse.",
	    "You pretend to light a pipe and smoke comes out of your nose.",
	    "$n pretends to light a pipe and smoke comes out of $s nose.",
	    "You kiss a lover long dead.",
	    "$n kisses a lover long dead.",
	    "You hiccup a blast of acid.",
	    "$n hiccups a blast of acid.",
	    "You light yourself with a magnificent aura.",
	    "$n is lighted by a magnificent aura.",
	    "You sprout wings of an angel.",
	    "$n sprouts wings of an angel.",
	    "Gregorian monks chant for your piece of mind.",
	    "Gregorian monks chant for $n's piece of mind.",
	    "You radiate a feeling of eternal bless.",
	    "$n radiates an intense feeling of self-complacency.",
	    "You stand frozen in a hunter position.",
	    "$n stands frozen in a hunter position.",
	    "You make a walk on your hands.",
	    "$n takes a stroll on $s hands.",
	    "You talk in tongues and spit from your mouth.",
	    "$n talks in strange languages and spits from $s mouth.",
	    "You show how you untied the Gordian Knot.",
	    "$n shows how the Gordian Knot must be untied.",
	    "You list the mighty Lords who have begged for your assistance.",
	    "$n lists the mighty Lords who have begged for $s assistance.",
	    "You sleep on the sharp edge of your weapon.",
	    "$n sleeps on the sharp edge of $s weapon.",
	    "You show the crushed head of a dear enemy.",
	    "$n shows the crushed head of a dear enemy.",
	    "The ghost of Caligula wants to consult you.",
	    "The ghost of Caligula wants to consult $n."
#endif
	}
    },

    {
	{
	    "Little red lights dance in your eyes.",
	    "Little red lights dance in $n's eyes.",
	    "You recite words of wisdom.",
	    "$n recites words of wisdom.",
	    "You juggle with daggers, apples, and eyeballs.",
	    "$n juggles with daggers, apples, and eyeballs.",
	    "You hit your head, and your eyes roll.",
	    "$n hits $s head, and $s eyes roll."
#ifdef NOTYETDEF
,
	    "A pulsating flow of liquid light comes from your eyes.",
	    "$n emits a flow of liquid pulsating light.",
	    "Your eyes turn inward as you meditate on a spell.",
	    "$n meditates on a spell - $s eyes turn inward.",
	    "The power of your eyes would stone-freeze a Medusa.",
	    "The power of $n's eyes would stone-freeze a Medusa.",
	    "Your eyes bleed as you shift the forcefields of this world.",
	    "$n's eyes bleed as $s powers shifts the forcefields of this world.",
	    "You enquire a tree about its health.",
	    "$n enquires a tree about its health.",
	    "You release a cursed spell from its scroll and it flies to the sky.",
	    "$n releases a cursed spell from its scroll.",
	    "You pray powerful words of healing.",
	    "$n prays powerful words of healing.",
	    "You quote long passages from the books you learned.",
	    "$n quotes long passages from $s studybooks.",
	    "You strike fire with two pieces of wood.",
	    "$n strikes fire with two pieces of wood.",
	    "You rotate a throwing star between your fingers.",
	    "$n rotates a throwing star between $s fingers.",
	    "You pick some mud and mold it in the shape of Don Quichote.",
	    "$n molds some dirt in the shape of Don Quichote.",
	    "You juggly with some equipment taken from the others.",
	    "$n juggles with your equipment.",
	    "You split a tree with your head.",
	    "$n splits a tree with $s head.",
	    "You pierce a rock with your thumb.",
	    "$n pierces a rock with $s thumb.",
	    "You boast that your \"eye-queue\" beats that of a stone.",
	    "$n boasts that $s \"eye-queue\" beats that of a stone.",
	    "With your sharp nail, you draw another scar on your arm.",
	    "$n draws a new scar on $s arm with $s nails."
#endif
	}
    },

    {
	{
	    "A slimy green monster appears before you and bows.",
	    "A slimy green monster appears before $n and bows.",
	    "Deep in prayer, you levitate.",
	    "Deep in prayer, $n levitates.",
	    "You steal the underwear off every person in the room.",
	    "Your underwear is gone!  $n stole it!",
	    "Crunch, crunch -- you munch a bottle.",
	    "Crunch, crunch -- $n munches a bottle."
#ifdef NOTYETDEF
,
	    "Three Leviathans pay their respect to you.",
	    "Three Leviathans appear and worship $n.",
	    "The Nazghul appear before you and tremble.",
	    "The Nazghul appear before $n and tremble.",
	    "The Black Death appears before you and shakes your hand.",
	    "The Black Death appears before $n and shakes hands.",
	    "Eight snakes shoot out of your fingers and crawl away.",
	    "Eight snakes shoot out of $n's fingers and crawl away.",
	    "Upon your prayer, the ground lifts you.",
	    "$n prays, and is lifted by the ground.",
	    "You scratch a symbol in the ground, and fly away.",
	    "$n scratches a symbol in the ground, and flies away.",
	    "You meditate while rotating gently in the air.",
	    "$n meditates while rotating in the air.",
	    "You pray, while standing on your head.",
	    "$n prays while standing on $s head.",
	    "Your whistle brings a bat on everyone.",
	    "$n whistles - A bat lands on your back!",
	    "You disarm everyone.",
	    "You are disarmed!  $n did it!",
	    "You get into a berserk frenzy.",
	    "$n gets into a berserk frenzy.",
	    "You tie up every person in the room.",
	    "You are tied and gagged!  $n laughs.",
	    "You bite a branch in two with your teeth.",
	    "$n bites a branch in two pieces.",
	    "You swallow a dagger and pull it out again.",
	    "$n swallows a dagger and pulls it out again.",
	    "You swallow a dagger. It does not reappear.",
	    "$n swallows a dagger. It does not reappear."
	    "You make a passerby eat his sword.",	// his = passerby
	    "$n makes a passerby eat his own sword."	// his = passerby
#endif
	}
    },

    {
	{
	    "You turn everybody into a little pink elephant.",
	    "You are turned into a little pink elephant by $n.",
	    "An angel consults you.",
	    "An angel consults $n.",
	    "The dice roll ... and you win again.",
	    "The dice roll ... and $n wins again.",
	    "... 98, 99, 100 ... you do pushups.",
	    "... 98, 99, 100 ... $n does pushups."
#ifdef NOTYETDEF
,
	    "You turn everybody into a sleeping bag.",
	    "You are turned into a sleeping bag by $n.",
	    "You command the power of dragons.",
	    "$n calls a dragon and tells it, down sir!",
	    "You shake the hand of a long deceased.",
	    "A hand reaches out of the ground. $n shakes it.",
	    "You tinker with the mana of others.",
	    "You have 50 mana... 8000 mana... $n is tinkering with you.",
	    "You call forth clouds.",
	    "Clouds appear at $n's word.",
	    "You create a nightingale, and it sings for you.",
	    "$n creates a nightingale for comfort.",
	    "Hipocras consults you.",
	    "Hipocras consults $n.",
	    "Xena and Gabriela beg you to join them.",
	    "Xena and Gabriela beg $n to join them.",
	    "You win again at hide and seek!",
	    "You play hide and seek ... and $n wins again.",
	    "You play body parts quest ... and win again.",
	    "You play body parts quest ... and $n wins again.",
	    "You play 'where does this come from?' and win again.",
	    "You play 'where does this come from?' and $n wins again.",
	    "You win again at poker!",
	    "$n wins again at poker!",
	    "... 98, 99, 100 ... you do pushups with one arm.",
	    "... 98, 99, 100 ... $n does pushups with one arm.",
	    "... 98, 99, 100 ... you do pushups with $s eyebrows.",
	    "... 98, 99, 100 ... $n does pushups with $s eyebrows.",
	    "... 298, 299, 300 ... you do pushups forever.",
	    "... 298, 299, 300 ... $n does not know how to stop pushups.",
	    "... 98, 99, 100 ... you make everybody do pushups.",
	    "... 98, 99, 100 ... $n makes you do pushups."
#endif
	}
    },

    {
	{
	    "A small ball of light dances on your fingertips.",
	    "A small ball of light dances on $n's fingertips.",
	    "Your body glows with an unearthly light.",
	    "$n's body glows with an unearthly light.",
	    "You count the money in everyone's pockets.",
	    "Check your money, $n is counting it.",
	    "Arnold Schwarzenegger admires your physique.",
	    "Arnold Schwarzenegger admires $n's physique."
#ifdef NOTYETDEF
,
	    "You light up everybody's clothes.",
	    "$n makes your clothes light up like a christmas tree.",
	    "You make smoke come out of everybody's ears.",
	    "Smoke comes out of your ears - must be $n's work.",
	    "You cause everybody to speak with funny voices.",
	    "$n has changed your voice - it really squeeks!",
	    "A small explosion blackenes your face.",
	    "$n's face is blackened by a small explosion.",
	    "Your body glows green with earth magic.",
	    "$n's body glows green with earth magic.",
	    "Your skin is covered with magic letters.",
	    "$n's skin is covered with magic letters.",
	    "Your body glows like an open hearth.",
	    "$n's body glows like an open hearth - cosy.",
	    "Your body glows with a beneficial aura.",
	    "$n's body glows with a beneficial aura.",
	    "You guess right what everybody is going to say.",
	    "$n has right guessed what you were about to say",
	    "You teach some deadly grasps.",
	    "$n demonstrates some deadly grasps.",
	    "You make everyone laughs on the story of Tirant Lo Blanc.",
	    "$n tells a funny story of Tirant Lo Blanc.",
	    "You make everybody count their money.",
	    "You suddenly feel lighter - did $n take your money?",
	    "Richard Lionheart Plantagenet admires your physique.",
	    "Richard Lionheart Plantagenet admires $n's physique.",
	    "Jacky Chan admires your physique.",
	    "Jacky Chan admires $n's physique.",
	    "Conan admires your physique.",
	    "Conan admires $n's physique.",
	    "Tulsa Doom admires your physique.",
	    "Tulsa Doom admires $n's physique."
#endif
	}
    },

    {
	{
	    "Smoke and fumes leak from your nostrils.",
	    "Smoke and fumes leak from $n's nostrils.",
	    "A spot light hits you.",
	    "A spot light hits $n.",
	    "You balance a pocket knife on your tongue.",
	    "$n balances a pocket knife on $s tongue.",
	    "Watch your feet, you are juggling granite boulders.",
	    "Watch your feet, $n is juggling granite boulders."
#ifdef NOTYETDEF
,
	    "You change yourself into the shape of a huge dragonfly.",
	    "$n changes into the shape of a huge dragonfly.",
	    "You cough out a fireball, burning everybody's hair.",
	    "Duck! $n coughs out a fireball - your hair is singed.",
	    "A glowing cloud of harm emanates from you.",
	    "A glowing cloud of harm emanates from $n.",
	    "You grow an extra limb and scratch your back.",
	    "$n grows an extra limb and scratches $s back.",
	    "You grow branches and leaves.",
	    "$n grows branches and leaves.",
	    "Small vials with potions fall out of your mouth.",
	    "Small vials with potions fall out of $n's mouth.",
	    "Divine light flow on you from above.",
	    "Divine light flows from above on $n.",
	    "A massive crowd cheers you.",
	    "$n is cheered at by a massive crowd.",
	    "You balance a machete on your tongue.",
	    "$n balances a machete on your tongue.",
	    "You balance a dagger on your tongue.",
	    "$n balances a dagger on your tongue.",
	    "You balance a lyre on your tongue.",
	    "$n balances a lyre on your tongue.",
	    "You balance a skeleton-key on your tongue.",
	    "$n balances a skeleton-key on your tongue.",
	    "Watch your feet, you are juggling three flails.",
	    "Watch your feet, $n is juggling three flails.",
	    "Watch your feet, you are a sword dance.",
	    "Better watch out, $n is doing a sword dance.",
	    "You are powerlifting a horse.",
	    "Watch your self, $n is powerlifting a horse.",
	    "You are juggling chopped hands of convicts.",
	    "$n is juggling chopped hands of convicts."
#endif
	}
    },

    {
	{
	    "The light flickers as you rap in magical languages.",
	    "The light flickers as $n raps in magical languages.",
	    "Everyone levitates as you pray.",
	    "You levitate as $n prays.",
	    "You produce a coin from everyone's ear.",
	    "$n produces a coin from your ear.",
	    "Oomph!  You squeeze water out of a granite boulder.",
	    "Oomph!  $n squeezes water out of a granite boulder."
#ifdef NOTYETDEF
,
	    "You change everyone's vision.",
	    "You suddenly see skeletons as $n talks.",
	    "You sprout dragon wings.",
	    "$n sprouts dragon wings.",
	    "Your hair grows as you rap in magical languages.",
	    "$n raps in magical languages and $s hair grows.",
	    "You sit in darkness as you rap in magical languages.",
	    "All light disappears around $n as he talks.",
	    "Everyone bounce around as you pray.",
	    "You bounce around as $n prays.",
	    "Everyone rotates as you pray.",
	    "You rotate as $n prays.",
	    "Everyone lengthens as you pray.",
	    "You lengthen as $n prays.",
	    "Everyone is thrown on the ground as you pray.",
	    "You are forced on the ground as $n prays.",
	    "You produce a little twig from everyone's ear.",
	    "$n produces a little twig from your ear.",
	    "You produce a dead spider from everyone's ear.",
	    "$n produces a dead spider from your ear.",
	    "You make a song based on everyone's name.",
	    "$n makes a song based on your name.",
	    "You produce a coin from everyone's purse.",
	    "$n produces a coin from your purse!",
	    "Oomph!  You squeeze milk out of a cheese.",
	    "Oomph!  $n squeezes milk out of a cheese.",
	    "Oomph!  You squeeze an orange in a thimble.",
	    "Oomph!  $n squeezes an orange in a thimble.",
	    "Oomph!  You produce a diamond from a piece of charcoal.",
	    "Oomph!  $n produces a diamond from a piece of charcoal.",
	    "Oomph!  You squeeze water out of a slave.",
	    "Oomph!  $n squeezes water out of a slave."
#endif
	}
    },

    {
	{
	    "Your head disappears.",
	    "$n's head disappears.",
	    "A cool breeze refreshes you.",
	    "A cool breeze refreshes $n.",
	    "You step behind your shadow.",
	    "$n steps behind $s shadow.",
	    "You pick your teeth with a spear.",
	    "$n picks $s teeth with a spear."
#ifdef NOTYETDEF
,
	    "You are split in two and wrestle with yourself.",
	    "$n is split in two, and he wrestles with $mself.",
	    "You sink to your neck in the ground.",
	    "$n sinks to $s neck in the ground.",
	    "Your arms fall off.",
	    "$n's arms fall off.",
	    "Your head look just like the others.",
	    "$n's head is awfully alike yours!",
	    "The shade from a tree refreshes you.",
	    "The shade from a tree refreshes $n.",
	    "A friendly nature elemental refreshes you.",
	    "A nature elemental refreshes $n.",
	    "Vestal virgins refresh you.",
	    "$n is refreshed by Vestal virgins.",
	    "Bless from above refreshes you.",
	    "Bless from above refreshes $n.",
	    "You surprise your shadow from behind.",
	    "$n surprises $s shadow from behind.",
	    "You tie and gag your shadow.",
	    "$n ties and gags $s shadow.",
	    "You BOOH! your shadow. It runs away and hides.",
	    "$n BOOH! $s shadow! It runs away and hides.",
	    "You rob your shadow.",
	    "$n robs $s shadow.",
	    "You scratch your armpits with a sword.",
	    "$n scratches $s armpits with a sword.",
	    "You massage your legs with a flail.",
	    "$n massages $s legs with a flail.",
	    "You cut your hair with an axe.",
	    "$n cuts $s hair with an axe.",
	    "You beat upon your chest with a mace.",
	    "$n beats upon $s chest with a mace."
#endif
	}
    },

    {
	{
	    "A fire elemental singes your hair.",
	    "A fire elemental singes $n's hair.",
	    "The sun pierces through the clouds to illuminate you.",
	    "The sun pierces through the clouds to illuminate $n.",
	    "Your eyes dance with greed.",
	    "$n's eyes dance with greed.",
	    "Everyone is swept off their feet by your hug.",
	    "You are swept off your feet by $n's hug."
#ifdef NOTYETDEF
,
	    "A firestorm singes your hair.",
	    "A firestorm singes $n's hair.",
	    "A fire dragon singes your hair.",
	    "A fire dragon singes $n's hair.",
	    "A cursed victim of hell singes your hair.",
	    "A cursed victim of hell singes $n's hair.",
	    "A wall of fire surrounds you.",
	    "A wall of fire surrounds $n.",
	    "Dancing fireflies illuminate you.",
	    "Dancing fireflies illuminate $n.",
	    "The power of words illuminates you.",
	    "The power of words illuminates $n.",
	    "Healing powers illuminate you.",
	    "Healing powers illuminate $n.",
	    "Your warrior's prowess illuminates you.",
	    "$n's warrior prowess illuminates $s appearance.",
	    "Your eyes dance with laughter about these clumsy trackers.",
	    "$n's eyes dance with a secret joke.",
	    "Your eyes dance with the idea of killing everyone.",
	    "$n's eyes dance with a flash of inspiration.",
	    "Your eyes dance with inspiration for an epic.",
	    "$n's eyes dance with inspiration.",
	    "Your eyes dance with the idea of robbing everyone.",
	    "$n's eyes dance with future promise.",
	    "You chair everyone on your shoulder.",
	    "$n chairs you on $s shoulder.",
	    "Everyone is shaken by your greeting.",
	    "You are shaken by $n's powerful greeting.",
	    "Everyone has bruises from your friendly pat.",
	    "You have bruises all over from $n's friendly pat.",
	    "Everyone is taken aback by your hug.",
	    "You are taken aback by $n's hug."
#endif
	}
    },

    {
	{
	    "The sky changes color to match your eyes.",
	    "The sky changes color to match $n's eyes.",
	    "The ocean parts before you.",
	    "The ocean parts before $n.",
	    "You deftly steal everyone's weapon.",
	    "$n deftly steals your weapon.",
	    "Your karate chop splits a tree.",
	    "$n's karate chop splits a tree."
#ifdef NOTYETDEF
,
	    "Your eyes become transparent.",
	    "$n's eyes become transparent.",
	    "Your skin changes color to match your eyes.",
	    "$n's skin changes color to match $s eyes.",
	    "Everybody has the color of your eyes.",
	    "You changes into the color of $n's eyes.",
	    "You drain everything from its color.",
	    "All color disappears at $n's whim.",
	    "The wide seas pay you tribute.",
	    "The seas pay tribute to $n.",
	    "You are unaffected by rain and storm.",
	    "$n is unaffected by rain and storm.",
	    "A flock of humming-birds appear above your hand.",
	    "A flock of humming-birds appear above $n's hand.",
	    "Saracens inform about your well being.",
	    "Saracens inform about $n's well being.",
	    "You swiftly clear the area from snakes.",
	    "$n swiftly removes a snake from behind you.",
	    "You sneak around everyone, attaching a \"kick me\" note.",
	    "$n attaches a \"kick me\" note on your back.",
	    "You disappear from everyone's sight.",
	    "$n disappears from your sight.",
	    "You place a wooden dummy in place of everyone's weapon.",
	    "$n has changed your weapon for a wooden dummy.",
	    "Your sideways kick makes a hole in a wall.",
	    "$n's sideways kick makes a hole in a wall.",
	    "You punish a rock with a fast series of blows.",
	    "$n punishes a rock with a fast series of blows.",
	    "Your bare fist brings down an elephant.",
	    "$n's bare fist brings down an elephant.",
	    "You hit a tree, and its leaves wither.",
	    "$n hits a tree, and its leaves wither."
#endif
	}
    },

    {
	{
	    "The stones dance to your command.",
	    "The stones dance to $n's command.",
	    "A thunder cloud kneels to you.",
	    "A thunder cloud kneels to $n.",
	    "The Grey Mouser buys you a beer.",
	    "The Grey Mouser buys $n a beer.",
	    "A strap of your armor breaks over your mighty thews.",
	    "A strap of $n's armor breaks over $s mighty thews."
#ifdef NOTYETDEF
,
	    "You lift a finger - everyone turns blind.",
	    "$n lifts a finger - and that's the last you can see!",
	    "An unnatural mist rises from the ground at your gesture.",
	    "At $n's gesture, an unnatural mist rises from the ground.",
	    "You count everybody's mana.",
	    "$n counts your mana.",
	    "You wink, and it rains pebbles.",
	    "$n winks, and it rains pebbles.",
	    "A morning fog kneels to you.",
	    "A morning fog kneels to $n.",
	    "You read from the infinite library.",
	    "$n reads from the infinite library.",
	    "A blessful stroke of sunlight hits you.",
	    "A blessful stroke of sunlight hits $n.",
	    "The elements of nature kneels to you.",
	    "The elements of nature kneel to $n.",
	    "The patron saint of scouts buys you a beer.",
	    "The patron saint of scouts buys $n a beer.",
	    "The black ninja buys you a beer.",
	    "The black ninja buys $n a beer.",
	    "Homer buys you a beer.",
	    "Homer buys $n a beer.",
	    "Robin Hood buys you a beer.",
	    "Robin buys $n a beer.",
	    "Your armor bends as you breathe in deeply.",
	    "$n's armor bends as $s inhales.",
	    "You quickly show how to armor up.",
	    "$n demonstrates how to armor up real fast.",
	    "You flex your muscles - too bad about that bracer.",
	    "A bracer of $n breaks over $s muscles.",
	    "Your intense frown makes the others armor shudder with fear.",
	    "Your armor shudders with fear as $n gazes at it intensely."
#endif
	}
    },

    {
	{
	    "The heavens and grass change colour as you smile.",
	    "The heavens and grass change colour as $n smiles.",
	    "The Burning Man speaks to you.",
	    "The Burning Man speaks to $n.",
	    "Everyone's pocket explodes with your fireworks.",
	    "Your pocket explodes with $n's fireworks.",
	    "A boulder cracks at your frown.",
	    "A boulder cracks at $n's frown."
#ifdef NOTYETDEF
,
	    "The world cycles through {rc{yo{rl{go{ru{wr{x as you smile.",
	    "{rC{yo{rl{go{ru{wr{rs{x everywhere as $n smiles!",
	    "The heavens and grass change smell as you smile.",
	    "The heavens and grass change smell as $n smiles.",
	    "The heavens and grass exchange place as you smile.",
	    "The heavens and grass exchange place as $n smiles.",
	    "The heavens and grass merge as you smile.",
	    "The heavens and grass merge as $n smiles.",
	    "You command the wild animals in the field.",
	    "$n commands the wild animals in the field.",
	    "Glyphs of power are marked on your head.",
	    "Glyphs of power shine from $n's head.",
	    "Your touch blesses everyone.",
	    "$n touches you - a warm feeling spreads all over you.",
	    "Your words make the earth shake.",
	    "$n's words make the earth shake.",
	    "Everyone's pocket is filled with mud. You grin.",
	    "Your pocket is filled with mud. $n grins.",
	    "You tap everyone on their shoulder without them noticing it.",
	    "You feel a tap on your shoulder.",
	    "Sparks fly from your head as you go into a mad craze.",
	    "Sparks fly from $n's head in $s mad craze.",
	    "You steal colour from a passing butterfly.",
	    "$n steal the colour from a passing butterfly.",
	    "An evil passerby throws himself at your mercy.",
	    "An evil passerby throws himself at $n's mercy.",
	    "The stones line up at your command.",
	    "$n cries \"attention\"! The stones line up.",
	    "The stones crumble at your command.",
	    "$n cries \"attention\"! The stones crumble to dust.",
	    "Everyone lines up when you cry \"attention\"!",
	    "$n cries \"attention\"! You obey."
#endif
	}
    },

    {
	{
	    "Everyone's clothes are transparent, and you are laughing.",
	    "Your clothes are transparent, and $n is laughing.",
	    "An eye in a pyramid winks at you.",
	    "An eye in a pyramid winks at $n.",
	    "Everyone discovers your dagger a centimeter from their eye.",
	    "You discover $n's dagger a centimeter from your eye.",
	    "Mercenaries arrive to do your bidding.",
	    "Mercenaries arrive to do $n's bidding."
#ifdef NOTYETDEF
,
	    "There is a big hole in you body, and you inspect it.",
	    "$n curiously inspects a big hole in $s body.",
	    "At the flick of your wrist, a baby dragon appears.",
	    "$n flicks $s wrist, and a baby dragon sits on it.",
	    "You count everybody's days.",
	    "$n looks worrisome - he is counting your days.",
	    "Plop - you lose an arm. Thonk - there goes your head.",
	    "Plop - $n loses an arm. Thonk - there goes $s head.",
	    "The trees embrace you.",
	    "$n is embraces by the trees.",
	    "Your hands bring forth a wholesome potion.",
	    "A wholesome potion flows from $n's hands.",
	    "Your peaceful state of mind calms everyone.",
	    "You are calmed by $n's peace.",
	    "You perform an elegant Tai-chi sequence.",
	    "$n performs an elegant Tai-chi sequence.",
	    "Everyone discovers your machete a centimeter from their eye.",
	    "You discover $n's machete a centimeter from your eye.",
	    "Everyone discovers your deadly hand a centimeter from their eye.",
	    "You discover $n's deadly hand a centimeter from your eye.",
	    "Everyone discovers your weapon a centimeter from their eye.",
	    "You discover $n's weapon a centimeter from your eye.",
	    "Everyone discovers their inventory displayed before them.",
	    "All your stuff is lying in front of you! Must be $n...",
	    "Crusaders arrive to do your bidding.",
	    "Crusaders arrive to do $n's bidding.",
	    "Soldiers arrive to do your bidding.",
	    "Soldiers arrive to do $n's bidding.",
	    "Savages arrive to do your bidding.",
	    "Savages arrive to do $n's bidding.",
	    "Assassins and cut-throats arrive to do your bidding.",
	    "Assassins and cut-throats arrive to do $n's bidding."
#endif
	}
    },

    {
	{
	    "A black hole swallows you.",
	    "A black hole swallows $n.",
	    "Valentine Michael Smith offers you a glass of water.",
	    "Valentine Michael Smith offers $n a glass of water.",
	    "Where did you go?",
	    "Where did $n go?",
	    "Four matched Percherons bring in your chariot.",
	    "Four matched Percherons bring in $n's chariot."
#ifdef NOTYETDEF
,
	    "You pull a drawer out of your stomach and get a cookie.",
	    "$n pulls a drawer out of $s stomach and gets a cookie.",
	    "You play zeros and naughts with smoke.",
	    "$n plays zeros and naughts with smoke.",
	    "You tinker with the order of everybody's intestines.",
	    "$n tinkers with the order of your intestines!",
	    "Your nose flies away, the nostrils flapping like mad.",
	    "Freedom! $n's nose flies away, with frantically flapping nostrils.",
	    "The grace of the gods fall upon you.",
	    "The grace of the gods falls upon $n.",
	    "The might of words falls upon you.",
	    "The might of words fall upon $n.",
	    "The power of healing is bestowed upon you.",
	    "The power of healing is bestowed upon you.",
	    "You battle meditation makes your weapons glow.",
	    "$n's battle meditation makes $s weapons glow.",
	    "You cough innocently and disappear.",
	    "$n's cough is the last your hear and see of $s",
	    "Only your eyes show as you become one with the background",
	    "Where did $n go? Are those $s eyes?",
	    "You have become very discreet",
	    "$n has moved outside your field of vision",
	    "You hide yourself in a backpack",
	    "$n hides $mself in a backpack",
	    "Four unicorns bring in your chariot.",
	    "Four unicorns bring in $n's chariot.",
	    "A hundred humble subjects bring in your chariot.",
	    "A hundred humble subjects bring in $n's chariot.",
	    "Six lions bring in your chariot.",
	    "Six lions bring in your chariot.",
	    "Twenty slaves bring in your chariot.",
	    "Twenty slaves bring in your chariot.",
#endif
	}
    },

    {
	{
	    "The world shimmers in time with your whistling.",
	    "The world shimmers in time with $n's whistling.",
	    "The great god Fatal gives you a staff.",
	    "The great god Fatal gives $n a staff.",
	    "Click.",
	    "Click.",
	    "Atlas asks you to relieve him.",	// him = atlas
	    "Atlas asks $n to relieve him."	// him = atlas
#ifdef NOTYETDEF
,
	    "You double, triple, and quadruple in size.",
	    "$n becomes twice -thrice- no four times as big!",
	    "You juggle with great fireballs.",
	    "Hot! $n juggles with great fireballs.",
	    "You have a chat with the Ghost of Christmass Past.",
	    "$n has a chat with the Ghost of Chistmass Past.",
	    "You grow so much hair you can't move.",
	    "$n is immobilized by the grow of $s hair.",
	    "For a moment, you halt all decay.",
	    "For a moment, $n halts all decay.",
	    "You pick up a stick and turn it into a powerful wand.",
	    "$n picks up a stick and turns it into a powerful wand.",
	    "Your breath makes flowers radiate with color.",
	    "$n's breath makes flowers radiate with color.",
	    "You descend from the pride of the fighting monk.",
	    "$n follows the tradition of the fighting monk.",
	    "Click. You set a trap for animals.",
	    "Click. $n sets a trap for animals.",
	    "Click. You set a trap for enemies.",
	    "Click. $n sets a trap for enemies.",
	    "Click. You pick a lock. And another one. And another...",
	    "Click. $n picks a lock. And another one. And another...",
	    "Click. You disarm a trap.",
	    "Click. $n disarms a trap.",
	    "Hercules wants to spend his holidays with you.",	// his = 
	    "Hercules asks $n to spend his holidays with $m.",	//	hercules
	    "Grum asks you to relieve him.",	// him = grum
	    "Grum asks $n to relieve him.",	// him = grum
	    "You beat the cyclops with arm-wrestling.",
	    "$n beats the cyclops with arm-wrestling.",
	    "Hades asks you to relieve him.",	// him = hades
	    "Hades asks $n to relieve him."	// him = hades
#endif
	}
    }
};



void do_pose( CHAR_DATA *ch, char *argument )
{
    int level;
    int pose;

    if ( IS_NPC(ch) )
	return;

    level = UMIN( ch->level, sizeof(pose_table) / sizeof(pose_table[0]) - 1 );
    pose  = number_range(0, level);

    act( pose_table[pose].message[2*ch->class+0], ch, NULL, NULL, TO_CHAR );
    act( pose_table[pose].message[2*ch->class+1], ch, NULL, NULL, TO_ROOM );

    return;
}


void do_rent( CHAR_DATA *ch, char *argument )
{
    send_to_char( "There is no rent here.  Just save and quit.\n\r", ch );
    return;
}


void do_qui( CHAR_DATA *ch, char *argument )
{
    send_to_char( "If you want to QUIT, you have to spell it out.\n\r", ch );
    return;
}

/* Quits a character without saving without checking anything,
   but it quits the char savely, after this function call
   ch may not be used anymore  */
void quit_char(CHAR_DATA *ch)
{
    DESCRIPTOR_DATA *d,*d_next;
    int id;

    if ( IS_NPC(ch) )
	return;

    /*
     * After extract_char the ch is no longer valid!
     */
    if (!STR_IS_SET(ch->strbit_act,PLR_NOSAVE)) {
	STR_SET_BIT(ch->strbit_act,PLR_QUITTING);
	save_char_obj( ch );
    }

    id = ch->id;
    d = ch->desc;
    extract_char( ch, TRUE );
    if ( d != NULL )
	close_socket( d );

    /* toast evil cheating bastards */
    for (d = descriptor_list; d != NULL; d = d_next) {
	CHAR_DATA *tch;

	d_next = d->next;
	tch = d->original ? d->original : d->character;
	if (tch && tch->id == id) {
	    extract_char(tch,TRUE);
	    close_socket(d);
	}
    }

    return;
}

void do_quit( CHAR_DATA *ch, char *argument )
{
    if ( IS_NPC(ch) ) {
	act("** WARNING ** $n is trying to escape to reality.",
	    ch,NULL,NULL,TO_CHAR);
	return;
    }

    if ( ch->position == POS_FIGHTING )
    {
	send_to_char( "No way! You are fighting.\n\r", ch );
	return;
    }

    if ( ch->pnote!=NULL ) {
	if (ch->desc) {
	    sprintf_to_char(ch,
		"Stop! You still have %s in progress. "
		"Type {W%s clear{x to remove it.\n\r",
		notes_table[ch->pnote->type].description,
		notes_table[ch->pnote->type].name);
	    return;
	}
	do_function(ch,&do_note,"clear");
    }

    if ( ch->position<POS_STUNNED && ch->position>POS_DEAD)
    {
	send_to_char( "You're not DEAD yet.\n\r", ch );
	return;
    }

    if ( STR_IS_SET(ch->strbit_act,PLR_SWITCHED) ) {
	send_to_char( "But you're switched!\n\r",ch);
	wiznet(0,92,NULL,NULL,"%s is trying to quit while switched!",NAME(ch));
	return;
    }

    if (argument[0]!=0)
	do_gossip(ch,argument);

    send_to_char("Alas, all good things must come to an end.\n\r",ch);
    act( "$n has left the game.", ch, NULL, NULL, TO_ROOM );
    logf("[%d] %s has quit.",GET_DESCRIPTOR(ch),ch->name);
    wiznet(WIZ_LOGINS,get_trust(ch),ch,NULL,"$N rejoins the real world.");
    announce(ch,"$N rejoins the real world.");

    if (ch->position==POS_DEAD && STR_IS_SET(ch->strbit_act,PLR_HARDCORE)) {
	// dead hardcore player -> delete
	char strsave1[MAX_INPUT_LENGTH];
	char strsave2[MAX_INPUT_LENGTH];
	char timestamp[15];
	char *name;
	time_t now=time(NULL);

	strftime(timestamp,15,"%Y%m%d%H%M%S",gmtime(&now));

	sprintf(strsave1, "%s/%s",mud_data.player_dir,capitalize( ch->name));
	sprintf(strsave2, "%s/%s.hc.%s",mud_data.hardcore_dir,capitalize(ch->name),timestamp);

	// create corpse
	make_corpse(ch,ch); // killer != NULL because that would move the corpse to the morgue

	if (ch->clan) {
	    clan_removemember(ch->clan);
	    ch->clan=NULL;
	}

	name=strdup(ch->name);

	// logout
	quit_char(ch);

	// remove playerfile
	if (link(strsave1,strsave2)) {
	    bugf("Unable to make backup of dead hardcore player (%s -> %s) [errno=%d]",strsave1,strsave2,errno);
	}
	unlink(strsave1);
	extract_owned_obj(name,0);
	free(name);
    } else 
	quit_char(ch);

    return;
}



void do_save( CHAR_DATA *ch, char *argument )
{
    if ( IS_NPC(ch) )
	return;

    save_char_obj( ch );
    send_to_char("Saving. Remember that ROM has automatic saving now.\n\r", ch);
    WAIT_STATE(ch,4 * PULSE_VIOLENCE);
    return;
}



void do_follow( CHAR_DATA *ch, char *argument )
{
/* RT changed to allow unlimited following and follow the NOFOLLOW rules */
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;

    one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Follow whom?\n\r", ch );
	return;
    }

    if ( ( victim = get_char_room( ch, arg ) ) == NULL )
    {
	send_to_char( "They aren't here.\n\r", ch );
	return;
    }

    if ( IS_AFFECTED(ch, EFF_CHARM) && ch->master != NULL )
    {
	act( "But you'd rather follow $N!", ch, NULL, ch->master, TO_CHAR );
	return;
    }

    if ( victim == ch )
    {
	if ( ch->master == NULL )
	{
	    send_to_char( "You already follow yourself.\n\r", ch );
	    return;
	}
	stop_follower(ch);
	return;
    }

    if (victim==ch->master) {
	act("You stay a little closer to $N.",ch,NULL,victim,TO_CHAR);
	return;
    }

    if (!IS_NPC(victim) &&
	!IS_IMMORTAL(ch) &&
	STR_IS_SET(victim->strbit_act,PLR_NOFOLLOW))
    {
	act("$N doesn't seem to want any followers.", ch,NULL,victim, TO_CHAR);
        return;
    }

    STR_REMOVE_BIT(ch->strbit_act,PLR_NOFOLLOW);

    if ( ch->master != NULL )
	stop_follower( ch );

    add_follower( ch, victim );
    return;
}


void add_follower( CHAR_DATA *ch, CHAR_DATA *master )
{
    if ( ch->master != NULL )
    {
	bugf("Add_follower: non-null master (ch: %s, master: %s)",
	    NAME(ch),NAME(ch->master));
	return;
    }

    ch->master        = master;
    ch->leader        = NULL;

    if ( can_see( master, ch ) )
	act( "$n now follows you.", ch, NULL, master, TO_VICT );
    else
	send_to_char("For a brief moment, you feel a chill behind you...\n\r",master);

    act( "You now follow $N.",  ch, NULL, master, TO_CHAR );

    return;
}



void stop_follower( CHAR_DATA *ch )
{
    if ( ch->master == NULL ) {
	bugf( "Stop_follower: null master (ch: %s)", NAME(ch));
	return;
    }

    if ( IS_AFFECTED(ch, EFF_CHARM) ) {
	STR_REMOVE_BIT( ch->strbit_affected_by, EFF_CHARM );
	effect_strip( ch, gsn_charm_person );
    }

    if ( can_see( ch->master, ch ) && ch->in_room != NULL)
	act( "$n stops following you.",     ch, NULL, ch->master, TO_VICT    );
    else
	send_to_char("You lose the sense of being followed...\n\r",ch->master);
    act( "You stop following $N.",      ch, NULL, ch->master, TO_CHAR    );

    if (ch->master->pet == ch)
	ch->master->pet = NULL;

    ch->master = NULL;
    ch->leader = NULL;
    return;
}

/* nukes charmed monsters and pets */
void nuke_pets( CHAR_DATA *ch )
{
    CHAR_DATA *pet;

    if ((pet = ch->pet) != NULL)
    {
    	stop_follower(pet);
    	if (pet->in_room != NULL)
    	    act("$n slowly fades away.",pet,NULL,NULL,TO_ROOM);
    	extract_char(pet,TRUE);
    }
    ch->pet = NULL;

    return;
}



void die_follower( CHAR_DATA *ch )
{
    CHAR_DATA *fch;

    if ( ch->master != NULL )
    {
    	if (ch->master->pet == ch)
    	    ch->master->pet = NULL;
	stop_follower( ch );
    }

    ch->leader = NULL;

    for ( fch = char_list; fch != NULL; fch = fch->next )
    {
	if ( fch->master == ch )
	    stop_follower( fch );
	if ( fch->leader == ch )
	    fch->leader = fch;
    }

    return;
}



void do_order( CHAR_DATA *ch, char *argument )
{
    char buf[MAX_STRING_LENGTH];
    char arg[MAX_INPUT_LENGTH],arg2[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    CHAR_DATA *och;
    CHAR_DATA *och_next;
    bool found;
    bool fAll;

    argument = one_argument( argument, arg );
    one_argument(argument,arg2);

    if ( arg[0]==0 || argument[0]==0 ) {
	send_to_char( "Order whom to do what?\n\r", ch );
	return;
    }

    if (IS_AFFECTED( ch, EFF_CHARM ) ) {
	send_to_char( "You feel like taking, not giving, orders.\n\r", ch );
	return;
    }

    if ( !str_cmp( arg, "all" ) )
    {
	fAll   = TRUE;
	victim = NULL;
    }
    else
    {
	fAll   = FALSE;
	if ( ( victim = get_char_room( ch, arg ) ) == NULL )
	{
	    send_to_char( "They aren't here.\n\r", ch );
	    return;
	}

	if ( victim == ch )
	{
	    send_to_char( "Aye aye, right away!\n\r", ch );
	    return;
	}

	if (!IS_AFFECTED(victim, EFF_CHARM) || victim->master != ch
	||  (IS_IMMORTAL(victim) && get_trust(victim) >= get_trust(ch)))
	{
	    send_to_char( "Do it yourself!\n\r", ch );
	    return;
	}
    }

    found = FALSE;
    for ( och = ch->in_room->people; och != NULL; och = och_next )
    {
	och_next = och->next_in_room;

	if ( IS_AFFECTED(och, EFF_CHARM)
	&&   och->master == ch
	&& ( fAll || och == victim ) )
	{
	    int align_margin;

	    found = TRUE;

	    if ( !IS_IMMORTAL(ch) &&
		 GetCharProperty(och,PROPERTY_INT,"pet_align_margin",&align_margin) &&
		 (ch->alignment > och->alignment+align_margin ||
		  ch->alignment < och->alignment-align_margin )) {
		act("$N refuses to obey you.",ch,NULL,och,TO_CHAR);
		continue;
	    }

	    sprintf( buf, "$n orders you to '%s'.", argument );
	    act( buf, ch, NULL, och, TO_VICT );
	    if (IS_PC(och)) STR_SET_BIT(och->strbit_act,PLR_ORDERED);
	    interpret( och, argument );
	    if (IS_PC(och)) STR_REMOVE_BIT(och->strbit_act,PLR_ORDERED);
	}
    }

    if ( found )
    {
	WAIT_STATE(ch,PULSE_VIOLENCE);
	send_to_char( "Ok.\n\r", ch );
    }
    else
	send_to_char( "You have no followers here.\n\r", ch );
    return;
}



void do_group( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;

    argument=one_argument( argument, arg );

    if ( arg[0] == '\0' || !str_cmp(arg,"show"))
    {
	CHAR_DATA *gch;
	CHAR_DATA *leader;
	CHAR_DATA *victim=ch;

	if (IS_IMMORTAL(ch) && arg[0]) {
	    one_argument(argument,arg);
	    victim=get_char_world(ch,arg);
	    if (victim==NULL) {
		send_to_char("Nobody around with that name.\n\r",ch);
		return;
	    }
	    if (IS_NPC(victim)) {
		send_to_char("Not on NPCs.\n\r",ch);
		return;
	    }
	}

	leader = (victim->leader != NULL) ? victim->leader : victim;
	sprintf_to_char(ch,"%s's group:\n\r", PERS(leader, victim) );

	for ( gch = char_list; gch != NULL; gch = gch->next )
	{
	    if ( is_same_group( gch, victim ) )
	    {
		sprintf_to_char( ch,
		"[%2d %s] %-16s %4d/%4d hp %4d/%4d mana %4d/%4d mv %5d xp\n\r",
		    gch->level,
		    IS_NPC(gch) ? "Mob" : class_table[gch->class].who_name,
		    capitalize( PERS(gch, ch) ),
		    gch->hit,   gch->max_hit,
		    gch->mana,  gch->max_mana,
		    gch->move,  gch->max_move,
		    gch->exp    );
	    }
	}
	return;
    }

    if ( !str_cmp(arg,"test") )
    {
	CHAR_DATA *gch;
	CHAR_DATA *leader;

	leader = (ch->leader != NULL) ? ch->leader : ch;
	sprintf_to_char( ch, "%s's group:\n\r", PERS(leader, ch) );

	for ( gch = char_list; gch != NULL; gch = gch->next )
	{
	    if ( is_same_group( gch, ch ) )
	    {
		sprintf_to_char( ch,
		"[%2d %s] %-16s (%lx,%lx) (%lx,%lx)\n\r",
		    gch->level,
		    IS_NPC(gch) ? "Mob" : class_table[gch->class].who_name,
		    capitalize( PERS(gch, ch) ),
		    (unsigned long)ch,(unsigned long)ch->leader,
		    (unsigned long)gch,(unsigned long)gch->leader);
	    }
	}
	return;
    }

    if ( ch->master != NULL || ( ch->leader != NULL && ch->leader != ch ) )
    {
	send_to_char( "But you are following someone else!\n\r", ch );
	return;
    }

    if (!str_cmp(arg,"all"))
    {
      CHAR_DATA *gch;
      char found=FALSE;

      /* group all followers */
      for(gch=char_list;gch!=NULL;gch=gch->next)
      {
        if ( gch->leader==NULL &&
             gch->master == ch && ch != gch &&
             ch->in_room == gch->in_room &&
             can_see(ch,gch) &&
	     abs(ch->level-gch->level)<=10)	/* new OOL rules... */
        {
          found=TRUE;
          gch->leader=ch;
          act_new("$N joins $n's group.",ch,NULL,gch,TO_NOTVICT,POS_RESTING);
          act_new("You join $n's group.",ch,NULL,gch,TO_VICT,POS_SLEEPING);
          act_new("$N joins your group.",ch,NULL,gch,TO_CHAR,POS_SLEEPING);
        }
      }

      if(!found)
        send_to_char("Nobody here to join your group.\n\r",ch);

      return;
    }

    if ( ( victim = get_char_room( ch, arg ) ) == NULL )
    {
	send_to_char( "They aren't here.\n\r", ch );
	return;
    }

    if ( victim->master != ch && ch != victim )
    {
	act_new("$N isn't following you.",ch,NULL,victim,TO_CHAR,POS_SLEEPING);
	return;
    }

    if (IS_AFFECTED(victim,EFF_CHARM))
    {
        send_to_char("You can't remove charmed mobs from your group.\n\r",ch);
        return;
    }

    if (IS_AFFECTED(ch,EFF_CHARM))
    {
    	act_new("You like your master too much to leave $m!",
	    ch,NULL,victim,TO_VICT,POS_SLEEPING);
    	return;
    }

    if ( is_same_group( victim, ch ) && ch != victim )
    {
	victim->leader = NULL;
	act_new("$n removes $N from $s group.",
	    ch,NULL,victim,TO_NOTVICT,POS_RESTING);
	act_new("$n removes you from $s group.",
	    ch,NULL,victim,TO_VICT,POS_SLEEPING);
	act_new("You remove $N from your group.",
	    ch,NULL,victim,TO_CHAR,POS_SLEEPING);
	return;
    }

    /* New OOL rules... */
    if (abs(ch->level-victim->level)>10) {
	act("It's much to dangerous to take $N on your journeys.",
	    ch,NULL,victim,TO_CHAR);
	act("It's much to dangerous to follow $n on $s journeys.",
	    ch,NULL,victim,TO_VICT);
	return;
    }

    victim->leader = ch;
    act_new("$N joins $n's group.",ch,NULL,victim,TO_NOTVICT,POS_RESTING);
    act_new("You join $n's group.",ch,NULL,victim,TO_VICT,POS_SLEEPING);
    act_new("$N joins your group.",ch,NULL,victim,TO_CHAR,POS_SLEEPING);
    return;
}


void do_leader( CHAR_DATA *ch, char *argument ) {
    CHAR_DATA *victim;
    CHAR_DATA *newleader,*oldleader;
    char sNewLeader[MAX_STRING_LENGTH];

    argument=one_argument(argument,sNewLeader);

    if ((newleader=get_char_room(ch,sNewLeader))==NULL) {
	send_to_char("Who do you want to be the new leader?\n\r",ch);
	return;
    }

    if (newleader->leader!=ch) {
	send_to_char("Who are you to decide?\n\r",ch);
	return;
    }
    oldleader=ch;

    if (IS_NPC(newleader)) {
	act("$E doesn't look that trustworthy...",
	    ch,NULL,newleader,TO_CHAR);
	return;
    }

    do_function(newleader,&do_follow,"self");
    for (victim=char_list;victim!=NULL;victim=victim->next) {
	if (victim->leader==oldleader &&
	    victim->master==oldleader &&
	    victim!=newleader &&
	    victim!=oldleader &&
	    oldleader->in_room==victim->in_room &&
	    can_see(oldleader,victim)) {
	    do_function(victim,&do_follow,"self");
	    do_function(victim,&do_follow,newleader->name);
	}
    }
    if (oldleader->master!=NULL)
	do_function(oldleader,&do_follow,"self");
    do_function(oldleader,&do_follow,newleader->name);

    do_function(newleader,&do_group,"all");
}



/*
 * 'Split' originally by Gnort, God of Chaos.
 */
void do_split( CHAR_DATA *ch, char *argument )
{
    char buf[MAX_STRING_LENGTH];
    char arg1[MAX_INPUT_LENGTH],arg2[MAX_INPUT_LENGTH];
    CHAR_DATA *gch;
    int members;
    int amount_gold = 0, amount_silver = 0;
    int share_gold, share_silver;
    int extra_gold, extra_silver;
    bool autosplit=FALSE;

    argument = one_argument( argument, arg1 );
    if (!str_cmp(arg1,"auto")) {
	argument = one_argument( argument, arg1 );
	autosplit=TRUE;
    }
    one_argument( argument, arg2 );

    if ( arg1[0] == '\0' ) {
	send_to_char( "Split how much?\n\r", ch );
	return;
    }

    amount_silver=atoi(arg1);

    if (arg2[0]!='\0')
	amount_gold=atoi(arg2);

    if ( amount_gold < 0 || amount_silver < 0) {
	send_to_char( "Your group wouldn't like that.\n\r", ch );
	return;
    }

    if ( amount_gold == 0 && amount_silver == 0 ) {
	send_to_char( "You hand out zero coins, but no one notices.\n\r", ch );
	return;
    }

    if ( ch->gold <  amount_gold || ch->silver < amount_silver) {
	send_to_char( "You don't have that much to split.\n\r", ch );
	return;
    }

    members = 0;
    for ( gch = ch->in_room->people; gch != NULL; gch = gch->next_in_room )
	if ( is_same_group( gch, ch ) && !IS_AFFECTED(gch,EFF_CHARM))
	    members++;

    if ( members < 2 ) {
	if (!autosplit)
	    send_to_char( "Just keep it all.\n\r", ch );
	return;
    }

    share_silver = amount_silver / members;
    extra_silver = amount_silver % members;

    share_gold   = amount_gold / members;
    extra_gold   = amount_gold % members;

    if ( share_gold == 0 && share_silver == 0 ) {
	send_to_char( "Don't even bother, cheapskate.\n\r", ch );
	return;
    }

    ch->silver	-= amount_silver;
    ch->silver	+= share_silver + extra_silver;
    ch->gold 	-= amount_gold;
    ch->gold 	+= share_gold + extra_gold;

    sprintf_to_char(ch,"You split %s.",money_string(amount_gold,amount_silver));
    sprintf_to_char(ch," Your share is %s.\n\r",
	money_string(share_gold+extra_gold,share_silver+extra_silver));

    sprintf(buf,"$n splits %s, giving you ",
	 money_string(amount_gold,amount_silver));
    strcat(buf,money_string(share_gold,share_silver));
    strcat(buf,".");

    for ( gch = ch->in_room->people; gch != NULL; gch = gch->next_in_room )
    {
	if ( gch != ch && is_same_group(gch,ch) && !IS_AFFECTED(gch,EFF_CHARM))
	{
	    act( buf, ch, NULL, gch, TO_VICT );
	    gch->gold += share_gold;
	    gch->silver += share_silver;
	}
    }

    return;
}


/*
 * It is very important that this be an equivalence relation:
 * (1) A ~ A
 * (2) if A ~ B then B ~ A
 * (3) if A ~ B  and B ~ C, then A ~ C
 */
bool is_same_group( CHAR_DATA *ach, CHAR_DATA *bch )
{
    if ( ach == NULL || bch == NULL)
	return FALSE;

    if ( ach->leader != NULL ) ach = ach->leader;
    if ( bch->leader != NULL ) bch = bch->leader;
    return (bool)(ach == bch);
}

bool is_same_group_recursive( CHAR_DATA *ach, CHAR_DATA *bch )
{
    if ( ach == NULL || bch == NULL)
	return FALSE;

    while ( ach->leader != NULL ) ach = ach->leader;
    while ( bch->leader != NULL ) bch = bch->leader;
    return (bool)(ach == bch);
}
bool has_same_master( CHAR_DATA *ach, CHAR_DATA *bch )
{
    if ( ach == NULL || bch == NULL)
	return FALSE;

    if ( ach->master != NULL ) ach = ach->master;
    if ( bch->master != NULL ) bch = bch->master;
    return (bool)(ach == bch);
}

bool has_same_master_recursive( CHAR_DATA *ach, CHAR_DATA *bch )
{
    if ( ach == NULL || bch == NULL)
	return FALSE;

    while ( ach->master != NULL ) ach = ach->master;
    while ( bch->master != NULL ) bch = bch->master;
    return (bool)(ach == bch);
}

void do_page( CHAR_DATA *ch, char *argument )
{
    CHAR_DATA *victim,*player;
    char arg1[MAX_INPUT_LENGTH];

    argument = one_argument( argument, arg1 );

    if (IS_NPC(ch)) {
	send_to_char("Monsters can't page.. go away.\r\n", ch);
	return;
    }

    if (STR_IS_SET(ch->strbit_comm,COMM_NOCHANNELS)) {
	send_to_char("The gods have revoked your page privileges.\n\r",ch);
	return;
    }

    if ( ! arg1[0] ) {
	send_to_char("Whom do you wish to page?\r\n", ch);
	return;
    }

    if (argument[0]==0) {
	send_to_char("What is the message?\n\r",ch);
	return;
    }

    argument=makedrunk(ch,argument,FALSE);
    argument=strip_colors(ch,argument);

    if (!str_cmp(arg1, "all")) {
	for (player = player_list; player!=NULL; player = player->next_player) {
	    victim=STR_IS_SET(player->strbit_act,PLR_SWITCHED)
		    ? player->pcdata->switched_into : player;

	    if (!STR_IS_SET(victim->strbit_comm,COMM_NOPAGE))
		act("\007\007*$n* " C_TELL "$t{x", ch,argument,victim,TO_VICT);
	}
	act("\007\007*all* " C_TELL "$t{x", ch,argument,NULL,TO_CHAR);
	return;
    }

    if ((victim = get_char_world(ch, arg1)) == NULL) {
	send_to_char("There is no such person in the game!\r\n", ch);
	return;
    }

    if (IS_NPC(victim)) {
	act("$N is a mob, don't bother $M with tells please.",
	    ch,NULL,victim,TO_CHAR);
	return;
    }

    if ( victim->desc == NULL) {
	act("$N seems to have misplaced $S link... try tell to store messages for $M.",
	ch,NULL,victim,TO_CHAR);
	return;
    }

    if (STR_IS_SET(victim->strbit_comm,COMM_NOPAGE)) {
	act("$N doesn't want to be paged.",ch,NULL,victim,TO_CHAR);
	return;
    }

    if (HAS_PUEBLO(victim))
	send_to_char("</xch_mudtext><img xch_alert><xch_mudtext>",victim);
    if (HAS_MXP(victim))
	sprintf_to_char(victim,
	    MXP"<SOUND \"chat.wav\" 50 2 80 \""URLBASE"/~edwin/\">\n\r");
    act("\007\007*$n* " C_TELL "$t{x",ch, argument, victim,TO_VICT);
    act("\007\007*$N* " C_TELL "$t{x",ch, argument, victim,TO_CHAR);
    return;
}


void do_mutter (CHAR_DATA * ch, char *argument) {
    CHAR_DATA   *victim;
    char    arg1[MAX_INPUT_LENGTH];
    char    arg2[MAX_INPUT_LENGTH],muffled[MAX_STRING_LENGTH],*unmuffled;
    bool    lastMufd = TRUE;
    int	    chance,wc;

    if (argument[0] == '\0') {
	send_to_char ("Mutter what to whom?\n\r", ch);
	return;
   }

    /* find target */
    argument = one_argument (argument, arg1);
    if ((victim = get_char_room (ch, arg1)) == NULL) {
	send_to_char ("They aren't here.\n\r", ch);
	return;
    }

    if (ch == victim) {
	send_to_char(
	    "Talking to yourself is the first sign of insanity.\n\r",ch);
	return;
    }

    if (victim->position <= POS_SLEEPING) {
	send_to_char("Perhaps now is not the most opportune time, "
	    "they're not awake.\n\r", ch);
	return;
    }

    argument=makedrunk(ch,argument,FALSE);
    argument=strip_colors(ch,argument);
    /* we need to save original message */
    unmuffled = argument;
    muffled[0] = '\0';
    wc=0;
    while (*argument != '\0') {
	chance=number_range (1, 100);
	argument = one_argument (argument, arg1);
	/* chance of word being overheard */
	if (chance > 15+5*wc) {
	    if (chance > 80)
		strcat (muffled, "...");
	    else if (chance > 50)
		strcat (muffled, "..");
	    else
		strcat (muffled, ".");
	    lastMufd = TRUE;
	} else {
	    sprintf (arg2, "%s%s", lastMufd ? "" : " ", arg1);
	    strcat (muffled, arg2);
	    lastMufd = FALSE;
	}
	wc++;
    }

    /* send it out as appropriate */
    act("You mutter to $N, '"C_TELL"$t"CLEAR"'",ch,unmuffled,victim,TO_CHAR);
    act("$n mutters to you, '"C_TELL"$t"CLEAR"'",ch,unmuffled,victim,TO_VICT);
    act("$n mutters to $N, '"C_TELL"$t"CLEAR"'",ch,muffled,victim,TO_NOTVICT);
    act("Those around you hear, '"C_TELL"$t"CLEAR"'",ch,muffled,NULL,TO_CHAR);
    act("Those around you hear, '"C_TELL"$t"CLEAR"'",victim,muffled,NULL,TO_CHAR);
}

void do_whisper(CHAR_DATA * ch,char *argument) {
    CHAR_DATA   *victim;
    char    arg1[MAX_INPUT_LENGTH];

    // whisper to nobody -> social
    if (argument[0]==0) {
	check_social(ch,"whisper",argument);
	return;
    }

    /* find target */
    argument=one_argument(argument,arg1);
    if ((victim=get_char_room(ch,arg1)) == NULL) {
	send_to_char("They aren't here.\n\r", ch);
	return;
    }

    // whisper to yourself -> social
    if (ch==victim) {
	check_social(ch,"whisper",arg1);
	return;
    }

    // whisper to somebody without args -> social
    if (argument[0]==0) {
	check_social(ch,"whisper",arg1);
	return;
    }

    if (victim->position<=POS_SLEEPING) {
	send_to_char("Perhaps now is not the most opportune time, "
	    "they're not awake.\n\r", ch);
	return;
    }

    argument=makedrunk(ch,argument,FALSE);
    argument=strip_colors(ch,argument);

    /* send it out as appropriate */
    act("You whisper to $N, '"C_TELL"$t"CLEAR"'",ch,argument,victim,TO_CHAR);
    act("$n whispers to you, '"C_TELL"$t"CLEAR"'",ch,argument,victim,TO_VICT);
    act("$n whispers something to $N.",ch,NULL,victim,TO_NOTVICT);
}

/*
 * Pueblo setting and unsetting
 */
void do_puebloclient( CHAR_DATA *ch, char *argument )
{
    char 	arg[ MAX_STRING_LENGTH ];

    if (IS_NPC(ch)) {
	send_to_char("Wierdo...\n\r",ch);
	return;
    }

    argument = one_argument( argument, arg );

    if (arg[0]) {
       STR_SET_BIT( ch->strbit_act, PLR_PUEBLO);
    } else {
       STR_REMOVE_BIT( ch->strbit_act, PLR_PUEBLO);
       send_to_char( "Pueblo support disabled.\n\r", ch );
    }
}

/*
 * MXP setting and unsetting
 */
void do_mxp( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_STRING_LENGTH];

    if (IS_NPC(ch)) {
	send_to_char("Wierdo...\n\r",ch);
	return;
    }

    argument = one_argument( argument, arg );
    if (arg[0]) {
       STR_SET_BIT( ch->strbit_act, PLR_MXP);
    } else {
       STR_REMOVE_BIT( ch->strbit_act, PLR_MXP);
       send_to_char( "MXP support disabled.\n\r", ch );
    }
}



// an ode to advent
void do_xyzzy(CHAR_DATA *ch,char *argument) {
    send_to_char("Nothing happens.\n\r",ch);
}

void do_hardcore(CHAR_DATA *ch, char *argument) {
    if (STR_IS_SET(ch->strbit_act,PLR_HARDCORE)) {
	send_to_char("You can't switch off hardcore gameplay.\n\r",ch);
	return;
    }

    if (str_cmp(argument,"I've been warned")) {
	send_to_char("Please read the help page about hardcore gameplay.\n\r",ch);
	return;
    }

    send_to_char("You are now a hardcore player.\n\r",ch);

    STR_SET_BIT(ch->strbit_act,PLR_HARDCORE);
    SetCharProperty(ch,PROPERTY_INT,"hardcore_start_lvl",&(ch->level));
    SetCharProperty(ch,PROPERTY_INT,"hardcore_start_exp",&(ch->exp));
}

// we need to borrow something from the nanny

void nanny_set_race(CHAR_DATA *ch,int race);
void nanny_calc_points(CHAR_DATA *ch);
void nanny_reset_skills(CHAR_DATA *ch);

CHAR_DATA *restart_char(CHAR_DATA *ch) {
    CHAR_DATA *newch;
    DESCRIPTOR_DATA *d=ch->desc;
    int restart;
    char email[MSL];

    newch = new_char();
    newch->pcdata = new_pcdata();

    free_string(newch->name);
    newch->name=str_dup( ch->name );
    newch->id                              = get_pc_id();
    nanny_set_race(newch,ch->race);

    newch->class                           = ch->class;

    newch->Sex=newch->pcdata->true_sex     = ch->pcdata->true_sex;

    newch->alignment                       = ch->alignment;

    newch->gen_data=new_gen_data();
    newch->gen_data->weapon=weapon_lookup(class_table[newch->class].default_weapon);

    STR_ZERO_BIT(newch->strbit_act,sizeof(ch->strbit_act));
    STR_SET_BIT(newch->strbit_act,PLR_NOSUMMON);
    if (STR_IS_SET(ch->strbit_act,PLR_COLOUR)) STR_SET_BIT(newch->strbit_act,PLR_COLOUR);
    STR_COPY_STR(newch->strbit_comm,ch->strbit_comm,sizeof(ch->strbit_comm));
    free_string(newch->prompt);			newch->prompt=str_dup(ch->prompt);
    free_string(newch->description);		newch->description=str_dup(ch->description);
    newch->pcdata->confirm_delete          = FALSE;
    newch->pcdata->clan_recruit            = 0;
    free_string(newch->pcdata->pwd);		newch->pcdata->pwd=str_dup( ch->pcdata->pwd );
    free_string(newch->pcdata->bamfin);		newch->pcdata->bamfin=str_dup( "" );
    free_string(newch->pcdata->bamfout);	newch->pcdata->bamfout=str_dup( "" );
    free_string(newch->pcdata->title);		newch->pcdata->title=str_dup( "" );
    free_string(newch->pcdata->whoname);	newch->pcdata->whoname=str_dup( "" );
    free_string(newch->pcdata->pueblo_picture);	newch->pcdata->pueblo_picture=str_dup( ch->pcdata->pueblo_picture );
#ifdef HAS_ALTS
    free_string(newch->pcdata->uberalt);	newch->pcdata->uberalt=str_dup( ch->pcdata->uberalt );
    free_string(newch->pcdata->alts);		newch->pcdata->alts=str_dup( ch->pcdata->alts );
#endif
    newch->pcdata->idle                    = time(NULL);
    newch->pcdata->condition[COND_THIRST]  = 48;
    newch->pcdata->condition[COND_FULL]    = 48;
    newch->pcdata->condition[COND_HUNGER]  = 48;
    newch->pcdata->condition[COND_ADRENALINE]= 0;
    newch->pcdata->security                = 0;
    newch->pcdata->default_lines           = ch->pcdata->default_lines;
    memcpy(newch->pcdata->last_notes,ch->pcdata->last_notes,sizeof(ch->pcdata->last_notes));
    STR_ZERO_BIT(newch->pcdata->strbitgrand,sizeof(newch->pcdata->strbitgrand));
    STR_ZERO_BIT(newch->strbit_wiznet,sizeof(newch->strbit_wiznet));

    GetCharProperty(ch,PROPERTY_INT,"restart",&restart);
    restart++;
    SetCharProperty(newch,PROPERTY_INT,"restart",&restart);

    if (GetCharProperty(ch,PROPERTY_STRING,"email",email))
	SetCharProperty(newch,PROPERTY_STRING,"email",email);

    {
	char strsave1[MAX_INPUT_LENGTH];
	char strsave2[MAX_INPUT_LENGTH];
	char timestamp[15];
	time_t now=time(NULL);

	strftime(timestamp,15,"%Y%m%d%H%M%S",gmtime(&now));

	sprintf(strsave1, "%s/%s",mud_data.player_dir,capitalize( ch->name));
	if (STR_IS_SET(ch->strbit_act,PLR_HARDCORE)) {
	    sprintf(strsave2, "%s/%s.hc.%s",mud_data.hardcore_dir,capitalize(ch->name),timestamp);
	} else {
	    strsave2[0]=0;
	}

	// create corpse
	make_corpse(ch,ch); // killer != NULL because that would move the corpse to the morgue

	// remove playerfile
	if (strsave2[0]!=0 && link(strsave1,strsave2))
	    bugf("Unable to make backup of restarting player (%s -> %s)",strsave1,strsave2);
	unlink(strsave1);
    }

    if (ch->clan) {
	clan_removemember(ch->clan);
	ch->clan=NULL;
    }

    extract_char( ch, TRUE );

    extract_owned_obj(newch->name,0);

    newch->desc=d;
    d->character=newch;
    // set connection state
    init_char_descriptor(d,newch);

    nanny_calc_points(newch);
    nanny_reset_skills(newch);
    screen_creation(newch,NULL);

    d->connected = CON_SCREEN_CREATION;

    return newch;
}

void do_restart(CHAR_DATA *ch, char *argument) {
    
    if (IS_NPC(ch)) return;
    if (check_ordered(ch)) return;

    if (ch->position>POS_DEAD) {
        if (ch->pcdata->confirm_delete) {
	    if (str_cmp(argument,ch->name)) {
		send_to_char("Restart cancelled.\n\r",ch);
		ch->pcdata->confirm_delete=FALSE;
		return;
	    }
	} else {
	    send_to_char("You're not dead yet.\n\r",ch);
	    sprintf_to_char(ch,"Type 'restart %s' if you're sure about starting over.\n\r",ch->name);
	    ch->pcdata->confirm_delete=TRUE;
	    return;
	}
    }

    if ( ch->pnote && ch->desc) {
	sprintf_to_char(ch,
	    "Stop! You still have %s in progress. "
	    "Type {W%s clear{x to remove it.\n\r",
	    notes_table[ch->pnote->type].description,
	    notes_table[ch->pnote->type].name);
	return;
    }

    restart_char(ch);
}
