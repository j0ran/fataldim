//
// $Id: fd_tcl_room.c,v 1.38 2008/03/18 20:36:23 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#define NEEDS_TCL
#include "merc.h"

#define TCL_ROOM_CMD(name) static int name(Tcl_Interp *interp,ROOM_DATA *room,CHAR_DATA *ch,int objc,Tcl_Obj *CONST objv[])

TCL_ROOM_CMD(room_name);
TCL_ROOM_CMD(room_count);
TCL_ROOM_CMD(room_oload);
TCL_ROOM_CMD(room_mload);
TCL_ROOM_CMD(room_echo);
TCL_ROOM_CMD(room_randomchar);
TCL_ROOM_CMD(room_charhere);
TCL_ROOM_CMD(room_objhere);
TCL_ROOM_CMD(room_damage);
TCL_ROOM_CMD(room_purge);
TCL_ROOM_CMD(room_vnum);
TCL_ROOM_CMD(room_allpc);
TCL_ROOM_CMD(room_property);
TCL_ROOM_CMD(room_door);
TCL_ROOM_CMD(room_open);
TCL_ROOM_CMD(room_exit);
TCL_ROOM_CMD(room_allmob);
TCL_ROOM_CMD(room_hasdelay);
TCL_ROOM_CMD(room_delay);
TCL_ROOM_CMD(room_id);
TCL_ROOM_CMD(room_roomflag);
TCL_ROOM_CMD(room_sector);
TCL_ROOM_CMD(room_timer);
TCL_ROOM_CMD(room_hastimer);
TCL_ROOM_CMD(room_addtrigger);
TCL_ROOM_CMD(room_deltrigger);
TCL_ROOM_CMD(room_triggers);
TCL_ROOM_CMD(room_chars);
TCL_ROOM_CMD(room_descr);
TCL_ROOM_CMD(room_extradescr);
TCL_ROOM_CMD(room_allobjects);
TCL_ROOM_CMD(room_delproperty);
TCL_ROOM_CMD(room_namespace);
TCL_ROOM_CMD(RoomCmdNotImpl);


struct tcl_Room_Command_Struct {
    char *cmd;
    int  (*func)(Tcl_Interp *,ROOM_DATA *,CHAR_DATA *,int,Tcl_Obj *CONST []);
};

static struct tcl_Room_Command_Struct tclRoomCommands[] = {
	{ "name",		room_name },
	{ "count",		room_count },
	{ "oload",		room_oload },
	{ "mload",		room_mload },
	{ "echo",		room_echo },
	{ "randomchar",		room_randomchar },
	{ "charhere",		room_charhere },
	{ "mobhere",		room_charhere },
	{ "objhere",		room_objhere },
	{ "damage",		room_damage },
	{ "purge",		room_purge },
	{ "vnum",		room_vnum },
	{ "allpc",		room_allpc },
	{ "property",		room_property },
	{ "door",		room_door },
	{ "open",		room_open },
	{ "exit",		room_exit },
	{ "allmob",		room_allmob },
	{ "hasdelay",		room_hasdelay },
	{ "delay",		room_delay },
	{ "id",			room_id },
	{ "roomflag",		room_roomflag },
	{ "sector",		room_sector },
	{ "timer",		room_timer },
	{ "hastimer",		room_hastimer },
	{ "addtrigger",		room_addtrigger },
	{ "deltrigger",		room_deltrigger },
	{ "triggers",		room_triggers },
	{ "chars",		room_chars },
	{ "descr",		room_descr },
	{ "extradescr",		room_extradescr },
	{ "allobjects",		room_allobjects },
	{ "delproperty",	room_delproperty },
	{ "namespace",		room_namespace },
	{ "exists",		RoomCmdNotImpl},
	{ NULL,			RoomCmdNotImpl}
};


int tclRoomObjCmd (client, interp, objc, objv)
    ClientData client;			/* Not used. */
    Tcl_Interp *interp;			/* Current interpreter. */
    int objc;				/* Number of arguments. */
    Tcl_Obj *CONST objv[];		/* Argument values. */
{
    ROOM_INDEX_DATA *room;
    CHAR_DATA *ch;
    int index;

    // we know objv[0] is 'room'          
    objc--; objv++;

    ch=progData->mob;
    if (ch==NULL) ch=progData->ch;
    room=tcl_parse_room_switches(progData->room,&ch,&objc,&objv);

    if (objc==1 && !str_cmp(Tcl_GetString(objv[0]),"exists")) {
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(room!=NULL));
	return TCL_OK;
    }

    if (!room)
	return TCL_ERROR;

    if (objc==0) {
	// No arguments given return name.
	return room_name(interp,room,ch,objc,objv);
    }

    if (Tcl_GetIndexFromObjStruct(interp, objv[0], 
				  (char **)tclRoomCommands, sizeof(struct tcl_Room_Command_Struct), 
				  "room command", 0, &index) != TCL_OK) {
	return TCL_ERROR;
    }

    return (tclRoomCommands[index].func)(interp,room,ch,objc,objv);
}

TCL_ROOM_CMD(RoomCmdNotImpl) {
    Tcl_Obj *result;

    result=Tcl_NewStringObj("Room command not implemented yet: ",-1);
    Tcl_AppendObjToObj(result,objv[0]);
    Tcl_SetObjResult(interp,result);

    return TCL_ERROR;
}

/* name */
TCL_ROOM_CMD(room_name) {
    Tcl_SetObjResult(interp,Tcl_NewStringObj(room->name,-1));
    return TCL_OK;
}

/* count */
TCL_ROOM_CMD(room_count) {
    long vnum=0;
    bool group=FALSE;
    char *arg;
    int index;

    static tclOptionList *countSwitches[]={"-vnum","-group",NULL};
    static tclOptionList *countOptions[]={"all","pc","mob","char",NULL};

    objc--;objv++;

    while(objc) {
	arg=Tcl_GetString(objv[0]);
	if (arg[0]!='-') break;

	if (Tcl_GetIndexFromObj(interp, objv[0], countSwitches, "switch", 0, &index) != TCL_OK)
	    return TCL_ERROR;

	switch(index) {
	    case 0: /* -vnum */
		if (objc<2) {
		    Tcl_SetObjResult(interp,Tcl_NewStringObj("room count: -vnum switch needs an argument",-1));
		    return TCL_ERROR;
		}
		if (Tcl_GetLongFromObj(interp,objv[1],&vnum)!=TCL_OK)
		    return TCL_ERROR;
		objv++;objc--;  // Skip argument
		break;
	    case 1: /* -group */
		group=TRUE;
		break;
	}

	objv++;objc--;
    }

    if (objc>1) {
	Tcl_WrongNumArgs(interp,0,objv,"room count ...");
	return TCL_ERROR;
    }

    index=1;
    if (objc==1) {
	if (Tcl_GetIndexFromObj(interp, objv[0], countOptions, "option", 0, &index) != TCL_OK)
	return TCL_ERROR;
    }

    if (group) index=4;
    if (vnum!=0) index=3;

    Tcl_SetObjResult(interp,Tcl_NewLongObj(tcl_count_people_room(room,ch,index,vnum)));
    return TCL_OK;
}

/* oload */
TCL_ROOM_CMD(room_oload) {
    long vnum,level=-1;
    OBJ_INDEX_DATA *pObjIndex;
    OBJ_DATA *obj;

    if (objc<2 || objc>3) {
	Tcl_WrongNumArgs(interp,0,objv,"room oload vnum ?level?");
	return TCL_ERROR;
    }

    if (Tcl_GetLongFromObj(interp,objv[1],&vnum)!=TCL_OK)
	return TCL_ERROR;

    level=-1;

    if ((pObjIndex=get_obj_index(vnum))==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("room oload: invalid vnum",-1));
	return TCL_ERROR;
    }

    if (objc==3 && Tcl_GetLongFromObj(interp,objv[2],&level)!=TCL_OK)
	return TCL_ERROR;
    if (level==-1) {
	if (ch) level=get_trust(ch);
	else level=pObjIndex->level;
    }

    if (level<0 || (ch && level>get_trust(ch))) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("room oload: invalid level value",-1));
	return TCL_ERROR;
    }

    obj=create_object(pObjIndex,level);
    obj_to_room(obj,room);
    op_load_trigger(obj,progData->ch);

    Tcl_SetObjResult(interp,Tcl_NewLongObj(obj->id));
    return TCL_OK;
}

/* mload */
TCL_ROOM_CMD(room_mload) {
    long vnum;
    MOB_INDEX_DATA *pMobIndex;
    CHAR_DATA *mob;

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"room mload vnum");
	return TCL_ERROR;
    }

    if (Tcl_GetLongFromObj(interp,objv[1],&vnum)!=TCL_OK)
	return TCL_ERROR;

    if ((pMobIndex=get_mob_index(vnum))==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("room mload: invalid vnum",-1));
	return TCL_ERROR;
    }

    mob=create_mobile(pMobIndex);
    mob->zone=room->area;
    char_to_room(mob,room);
    mp_load_trigger(mob,progData->ch);

    Tcl_SetObjResult(interp,Tcl_NewLongObj(mob->id));
    return TCL_OK;
}

/* echo */
TCL_ROOM_CMD(room_echo) {
    Tcl_Obj *concatObj;
    CHAR_DATA *wch;

    if (objc<=1) {
	Tcl_WrongNumArgs(interp,0,objv,"room echo text ?text text ...?");
	return TCL_ERROR;
    }

    concatObj=Tcl_ConcatObj(objc-1,objv+1);
    Tcl_IncrRefCount(concatObj);
    Tcl_AppendToObj(concatObj,"\n\r",-1);

    for (wch=room->people;wch;wch=wch->next_in_room)
	send_to_char(Tcl_GetString(concatObj),wch);

    Tcl_DecrRefCount(concatObj);
    return TCL_OK;
}

/* randomchar */
TCL_ROOM_CMD(room_randomchar) {
    static tclOptionList *randomcharSwitches[]={"-name","-mob","-char",NULL};
    int index,i;
    bool useName=FALSE;
    int type=1;  // default pc
    CHAR_DATA *target;

    i=1;

    while (objc-i>0) {
	char *s;

	s=Tcl_GetString(objv[i]);
	if (*s!='-') break;

	if (Tcl_GetIndexFromObj(interp, objv[i], randomcharSwitches, "switch", 0, &index) != TCL_OK)
	    return TCL_ERROR;

	if (index==0) useName=TRUE;
	else if (index==1) { type=2; }
	else if (index==2) { type=3; }
	i++;
    }

    if (objc-i>1) {
	Tcl_WrongNumArgs(interp,0,objv,"room randomchar ?-name? ?-mob? ?-char? ?tclscript?");
	return TCL_ERROR;
    }

    target=tcl_get_random_char(room,progType==PROGTYPE_CHAR?ch:NULL,type);

    if (target) {
    if (objc-i>0) {
	int error;
	CHAR_DATA *old;

	old=progData->ch;
	progData->ch=target;

	error=Tcl_EvalObjEx(interp,objv[i],0);

	progData->ch=old;

	if (error!=TCL_OK) return error;
    } else {
	if (useName) Tcl_SetObjResult(interp,Tcl_NewStringObj(tcl_firstname(target),-1));
	else Tcl_SetObjResult(interp,Tcl_NewLongObj(target->id));
    }
    } else {
	if (useName) Tcl_SetObjResult(interp,Tcl_NewStringObj("",-1));
	else Tcl_SetObjResult(interp,Tcl_NewLongObj(0));
    }
    return TCL_OK;
}

/* charhere */
TCL_ROOM_CMD(room_charhere) {
    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"room charhere charid");
	return TCL_ERROR;
    }
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(tcl_charid(room,Tcl_GetString(objv[1]),NULL)!=NULL));
    return TCL_OK;
}

/* objhere */
TCL_ROOM_CMD(room_objhere) {
    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"room objhere objid");
	return TCL_ERROR;
    }
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(tcl_objid(room->contents,Tcl_GetString(objv[1]),FALSE)!=NULL));
    return TCL_OK;
}

/* damage -self -lethal min max */
TCL_ROOM_CMD(room_damage) {
    int i=1,index;
    int min,max;
    char *s;
    bool damageSelf=FALSE;
    bool lethal=FALSE;
    CHAR_DATA *victim,*victim_next;
    static tclOptionList *damageSwitches[]={"-self","-lethal",NULL};

    while (objc-i>0) {
	s=Tcl_GetString(objv[i]);
	if (*s!='-') break;

	if (Tcl_GetIndexFromObj(interp, objv[i], damageSwitches, "switch", 0, &index) != TCL_OK)
	    return TCL_ERROR;

	if (index==0) damageSelf=TRUE;
	else if (index==1) lethal=TRUE;

	i++;
    }

    if (objc-i!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"room damage ?-self? ?-lethal? min max");
	return TCL_ERROR;
    }

    if (Tcl_GetIntFromObj(interp,objv[i],&min)!=TCL_OK) return TCL_ERROR;
    if (Tcl_GetIntFromObj(interp,objv[i+1],&max)!=TCL_OK) return TCL_ERROR;

    for ( victim = (room?room:ch->in_room)->people; victim; victim = victim_next ) {
	victim_next = victim->next_in_room;
	if ( damageSelf || victim != ch )
	    damage( victim, victim,
		lethal ? number_range(min,max) : UMIN(victim->hit,number_range(min,max)),
		TYPE_UNDEFINED, DAM_NONE, FALSE, NULL );
    }
    return TCL_OK;
}

/* purge option ?objcharid? */
TCL_ROOM_CMD(room_purge) {
    int i=1;
    int index;
    char *id;
    static tclOptionList *purgeOptions[]={"all","mob","obj","allmob","allobj",NULL};

    if (objc-i<1 || objc-i>2) {
	Tcl_WrongNumArgs(interp,0,objv,"room purge option ?charid/objid?");
	return TCL_ERROR;
    }

    if (Tcl_GetIndexFromObj(interp, objv[i++], purgeOptions, "option", 0, &index) != TCL_OK)
	return TCL_ERROR;

    if (objc-i>0) id=Tcl_GetString(objv[i++]);
    else id=NULL;

    if (index==0 && id) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("room purge: can't use an charid or objid with the all option",-1));
	return TCL_ERROR;
    }

    if (index==0 || index==2 || index==4) {
    // Check all objects in this room
	OBJ_DATA *obj,*obj_next;

	for (obj=room->contents;obj;obj=obj_next) {
	    obj_next=obj->next_content;

	    if (index!=0 && id && !tcl_isobjid(obj,id)) continue;

	    if (!STR_IS_SET(obj->strbit_extra_flags,ITEM_NOPURGE))
		extract_obj(obj);

	    if (index==2) break;
	}
    }

    if (index==0 || index==1 || index==3) {
	// Check all mobs in this room
	CHAR_DATA *mob,*mob_next;

	for (mob=room->people;mob;mob=mob_next) {
	    mob_next=mob->next_in_room;

	    if (IS_PC(mob) || mob==ch) continue;
	    if (index!=0 && id && !tcl_ischarid(mob,id)) continue;

	    if (!STR_IS_SET(mob->strbit_act,ACT_NOPURGE))
		extract_char(mob,TRUE);
	    if (index==1) break;
	}
    }
    return TCL_OK;
}

/* vnum */
TCL_ROOM_CMD(room_vnum) {
    Tcl_SetObjResult(interp,Tcl_NewLongObj(room->vnum));
    return TCL_OK;
}

/* allpc */
TCL_ROOM_CMD(room_allpc) {
    int i=1;
    int error;
    CHAR_DATA *old_ch,*vch,*vch_next;

    if (objc-i<=0) {
	Tcl_WrongNumArgs(interp,0,objv,"room allpc tcl-script");
	return TCL_ERROR;
    }

    if (room==NULL) return TCL_OK;

    old_ch=progData->ch;
    error=TCL_OK;

    ch=room->people;

    for (vch=room->people;vch;vch=vch_next) {
	vch_next=vch->next_in_room;
	if (IS_NPC(vch)) continue;

	progData->ch=vch;
	error=Tcl_EvalObjEx(interp,objv[i],0);

	if (error!=TCL_OK) break;
    }
    progData->ch=tcl_check_ch(old_ch);
    return TCL_OK;
}

// property <key> <type> [value]
TCL_ROOM_CMD(room_property) {
    char *key;
    char *stype;
    char *svalue;
    int itype;
    bool readonly;

    if (objc!=3 && objc!=4) {
	Tcl_WrongNumArgs(interp,0,objv,"room property key type ?value?");
	return TCL_ERROR;
    }

    readonly=(objc==3);

    key=Tcl_GetString(objv[1]);
    stype=Tcl_GetString(objv[2]);
    if (!readonly) svalue=Tcl_GetString(objv[3]);
    itype=PROPERTY_UNDEF;
    if (str_cmp(stype,"int")   ==0) itype=PROPERTY_INT;
    if (str_cmp(stype,"long")  ==0) itype=PROPERTY_LONG;
    if (str_cmp(stype,"bool")  ==0) itype=PROPERTY_BOOL;
    if (str_cmp(stype,"string")==0) itype=PROPERTY_STRING;
    if (str_cmp(stype,"char")  ==0) itype=PROPERTY_CHAR;
    if (itype==PROPERTY_UNDEF) {
	Tcl_WrongNumArgs(interp,0,objv,"type should be: int, long, bool, string or char");
	return TCL_ERROR;
    }

    if (readonly) {
	if (itype==PROPERTY_INT) {
	    int ivalue;
	    if (GetRoomProperty(room,PROPERTY_INT,key,&ivalue)==0)
		Tcl_SetObjResult(interp,Tcl_NewIntObj(0));
	    else
		Tcl_SetObjResult(interp,Tcl_NewIntObj(ivalue));
	}

	if (itype==PROPERTY_LONG) {
	    long lvalue;
	    if (GetRoomProperty(room,PROPERTY_LONG,key,&lvalue)==0)
		Tcl_SetObjResult(interp,Tcl_NewLongObj(0));
	    else
		Tcl_SetObjResult(interp,Tcl_NewLongObj(lvalue));
	}

	if (itype==PROPERTY_BOOL) {
	    bool bvalue;
	    if (GetRoomProperty(room,PROPERTY_BOOL,key,&bvalue)==0)
		Tcl_SetObjResult(interp,Tcl_NewBooleanObj(FALSE));
	    else
		Tcl_SetObjResult(interp,Tcl_NewBooleanObj(bvalue));
	}

	if (itype==PROPERTY_STRING) {
	    char svalue[128];
	    if (GetRoomProperty(room,PROPERTY_STRING,key,svalue)==0)
		Tcl_SetObjResult(interp,Tcl_NewStringObj("",-1));
	    else
		Tcl_SetObjResult(interp,Tcl_NewStringObj(svalue,-1));
	}

	if (itype==PROPERTY_CHAR) {
	    char cvalue,svalue[2];
	    if (GetRoomProperty(room,PROPERTY_CHAR,key,&cvalue)==0) {
		svalue[0]=' ';svalue[1]=0;
		Tcl_SetObjResult(interp,Tcl_NewStringObj(svalue,-1));
	    } else {
		svalue[0]=cvalue;svalue[1]=0;
		Tcl_SetObjResult(interp,Tcl_NewStringObj(svalue,-1));
	    }
	}
    } else {
	if (itype==PROPERTY_INT) {
	    int ivalue;
	    if (Tcl_GetIntFromObj(interp,objv[3],&ivalue)!=TCL_OK) 
		return TCL_ERROR;
	    SetRoomProperty(room,PROPERTY_INT,key,&ivalue);
	}

	if (itype==PROPERTY_LONG) {
	    long lvalue;
	    if (Tcl_GetLongFromObj(interp,objv[3],&lvalue)!=TCL_OK)   
		return TCL_ERROR;
	    SetRoomProperty(room,PROPERTY_LONG,key,&lvalue);
	}

	if (itype==PROPERTY_BOOL) {
	    int i;
	    bool bvalue;
	    if (Tcl_GetBooleanFromObj(interp,objv[3],&i)!=TCL_OK)   
		return TCL_ERROR;
	    bvalue=i!=0;
	    SetRoomProperty(room,PROPERTY_BOOL,key,&bvalue);
	}

	if (itype==PROPERTY_STRING) {
	    SetRoomProperty(room,PROPERTY_STRING,key,svalue);
	}

	if (itype==PROPERTY_CHAR) {
	    char cvalue=svalue[1];
	    SetRoomProperty(room,PROPERTY_CHAR,key,&cvalue);
	}
    }
    return TCL_OK;
}

// door
TCL_ROOM_CMD(room_door) {
    char *dir;
    int exitnum=0;

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"room door direction");
	return TCL_ERROR;
    }
    dir=Tcl_GetString(objv[1]);
    if ((exitnum=get_direction(dir))==-1) {
	Tcl_Obj *result;
	result=Tcl_NewStringObj("room open: unknown direction: ",-1);
	Tcl_AppendToObj(result,dir,-1);
	Tcl_SetObjResult(interp,result);
	return TCL_ERROR;
    }
    // no exit, then there is no door
    if (room->exit[exitnum]==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(FALSE));
	return TCL_OK;
    }
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(IS_SET(room->exit[exitnum]->exit_info,EX_ISDOOR)));
    return TCL_OK;
}

// open
TCL_ROOM_CMD(room_open) {
    char *dir;
    int exitnum=0;

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"room open direction");
	return TCL_ERROR;
    }
    dir=Tcl_GetString(objv[1]);
    if ((exitnum=get_direction(dir))==-1) {
	Tcl_Obj *result;
	result=Tcl_NewStringObj("room open: unknown direction: ",-1);
	Tcl_AppendToObj(result,dir,-1);
	Tcl_SetObjResult(interp,result);
	return TCL_ERROR;
    }
    // no exit, then it's not open
    if (room->exit[exitnum]==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(FALSE));
	return TCL_OK;
    }
    // no door, then it's open
    if (!IS_SET(room->exit[exitnum]->exit_info,EX_ISDOOR)) {
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(TRUE));
	return TCL_OK;
    }
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(!IS_SET(room->exit[exitnum]->exit_info,EX_CLOSED)));
    return TCL_OK;
}

// exit
TCL_ROOM_CMD(room_exit) {
    char *dir;
    int exitnum=0;

    if (objc<2 || objc>5) {
	Tcl_WrongNumArgs(interp,0,objv,"room exit direction [flag [boolean]]");
	return TCL_ERROR;
    }
    dir=Tcl_GetString(objv[1]);
    if ((exitnum=get_direction(dir))==-1) {
	Tcl_Obj *result;
	result=Tcl_NewStringObj("room open: unknown direction: ",-1);
	Tcl_AppendToObj(result,dir,-1);
	Tcl_SetObjResult(interp,result);
	return TCL_ERROR;
    }
    // flag trouble
    if (objc!=2) {
	int flag;

	if (room->exit[exitnum]==NULL) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("No exit in that direction",-1));
	    return TCL_ERROR;
	}

	flag = option_find_name(Tcl_GetString(objv[2]),exit_options,objc>3);
	if (flag == NO_FLAG) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("Unknown flag",-1));
	    return TCL_ERROR;
	}
	if (objc==3) {
	    // read
	    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(IS_SET(room->exit[exitnum]->exit_info,flag)));
	} else {
	    // write
	    int new_value;
	    int one_way;
	    one_way=(objc>4) && Tcl_StringCaseMatch(Tcl_GetString(objv[4]),"oneway",1);
	    if (Tcl_GetBooleanFromObj(interp,objv[3],&new_value)==TCL_ERROR) {
		return TCL_ERROR;
	    }
	    if (new_value) {
		SET_BIT(room->exit[exitnum]->exit_info,flag);
		if (!one_way &&
		    room->exit[exitnum]->to_room &&
		    room->exit[exitnum]->to_room->exit[rev_dir[exitnum]] &&
		    room->exit[exitnum]->to_room->exit[rev_dir[exitnum]]->to_room==room)
		    SET_BIT(room->exit[exitnum]->to_room->exit[rev_dir[exitnum]]->exit_info,flag);
	    } else {
		REMOVE_BIT(room->exit[exitnum]->exit_info,flag);
		if (!one_way &&
		    room->exit[exitnum]->to_room &&
		    room->exit[exitnum]->to_room->exit[rev_dir[exitnum]] &&
		    room->exit[exitnum]->to_room->exit[rev_dir[exitnum]]->to_room==room)
		    REMOVE_BIT(room->exit[exitnum]->to_room->exit[rev_dir[exitnum]]->exit_info,flag);
	    }
	}
	return TCL_OK;
    }
    // no exit, then there is no exit
    if (room->exit[exitnum]==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewLongObj(0));
	return TCL_OK;
    }
    // even if there is no room at the otherside (vnum=0), the value
    // returned is still valid :-)
    Tcl_SetObjResult(interp,Tcl_NewLongObj(room->exit[exitnum]->vnum));
    return TCL_OK;
}

/* allmob <charid> <tclscript> */
TCL_ROOM_CMD(room_allmob) {
    CHAR_DATA *victim;
    CHAR_DATA *old_ch,*next_victim;
    char *charid;
    int error;

    if (objc!=3) {
	Tcl_WrongNumArgs(interp,0,objv,"room allmob charid tcl-script");
	return TCL_ERROR;
    }

    charid=Tcl_GetString(objv[1]);

    victim=tcl_charid(NULL,charid,NULL);
    old_ch=progData->mob;
    error=TCL_OK;

    while(victim!=NULL) {
	progData->mob=victim;
	next_victim=victim->next;

	if (victim->in_room==room) {
	    error=Tcl_EvalObjEx(interp,objv[2],0);

	    if (error!=TCL_OK) break;
	}
	if (next_victim==NULL) break;
	victim=tcl_charid(NULL,charid,next_victim);
    }
    progData->mob=tcl_check_ch(old_ch);

    return error;
}

// hasdelay
TCL_ROOM_CMD(room_hasdelay) {
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(room->rprog_delay>0));
    return TCL_OK;
}

// delay
TCL_ROOM_CMD(room_delay) {
    long delay;
    if (objc==1) {
	Tcl_SetObjResult(interp,Tcl_NewLongObj(room->rprog_delay));
	return TCL_OK;
    }
    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"room delay number");
	return TCL_ERROR;
    }

    if (Tcl_GetLongFromObj(interp,objv[1],&delay)==TCL_ERROR) {
	if (!str_cmp(Tcl_GetString(objv[1]),"cancel")) delay=-1;
	else return TCL_ERROR;
    }
    room->rprog_delay=delay;
    Tcl_SetObjResult(interp,Tcl_NewLongObj(delay));
    return TCL_OK;
}

// id
TCL_ROOM_CMD(room_id) {
    Tcl_SetObjResult(interp,Tcl_NewLongObj(room->vnum));
    return TCL_OK;
}

// roomflag
TCL_ROOM_CMD(room_roomflag) {
    char *flag_name;
    int  flag;
    int  readonly=objc==2;
    int  newvalue;

    if (objc<2) {
	Tcl_WrongNumArgs(interp,0,objv,"room roomflag flagname ?newvalue?");
	return TCL_ERROR;
    }

    flag_name=Tcl_GetString(objv[1]);

    if (!readonly) {
	Tcl_GetBooleanFromObj(interp,objv[2],&newvalue);
    }
    flag=option_find_name(flag_name,room_options,!readonly);
    if (flag==NO_FLAG) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("room flag: Flag not found (or flag is readonly)",-1));
	return TCL_ERROR;
    }
    if (!readonly) {
	if (newvalue) STR_SET_BIT(room->strbit_room_flags,flag);
	else STR_REMOVE_BIT(room->strbit_room_flags,flag);
    }
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(STR_IS_SET(room->strbit_room_flags,flag)));
    return TCL_OK;
}

// sector
TCL_ROOM_CMD(room_sector) {
    if (objc==1) {
	Tcl_SetObjResult(interp,Tcl_NewLongObj(room->sector_type));
    } else {
	if (objc==2) {
	    static tclOptionList *positionSwitches[]={"-name",NULL};
	    int sindex=-1;

	    if (Tcl_GetIndexFromObj(interp, objv[1], positionSwitches, "switch", 0, &sindex) != TCL_OK)
		return TCL_ERROR;

	    /* Only one argument so this is the correct one. */
	    Tcl_SetObjResult(interp,Tcl_NewStringObj(table_find_value(room->sector_type,sector_table),-1));
	}
    }
    return TCL_OK;
}

/* timer */
TCL_ROOM_CMD(room_timer) {
    long time;

    if (objc==1) { /* zero arguments */
	Tcl_SetObjResult(interp,Tcl_NewLongObj(room->rprog_timer));
	return TCL_OK;
    }

    if (objc!=2) { /* 1 argument */
	Tcl_WrongNumArgs(interp,0,objv,"room timer ?number?");
	return TCL_ERROR;
    }

    if (Tcl_GetLongFromObj(interp,objv[1],&time)==TCL_ERROR) {
	if (!str_cmp(Tcl_GetString(objv[1]),"cancel")) time=-1;
	else return TCL_ERROR;
    }
    room->rprog_timer=time;
    Tcl_SetObjResult(interp,Tcl_NewLongObj(time));
    return TCL_OK;
}

/* hastimer */
TCL_ROOM_CMD(room_hastimer) {
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(room->rprog_timer>=0));
    return TCL_OK;
}

TCL_ROOM_CMD(room_addtrigger) {
    return rp_addtrigger(room,objc,objv);
}

// deltrigger
TCL_ROOM_CMD(room_deltrigger) {
    static tclOptionList *deltriggerSwitches[]={"-all","-prefix", NULL};
    int sindex=-1;
    bool delAll,prefix;
    TCLPROG_LIST *trig,*trig_next;
    char *s,l;

    if (objc<=1) {
	Tcl_WrongNumArgs(interp,0,objv,"room deltrigger ?-all? ?-prefix? ?name?");
	return TCL_ERROR;
    }

    s=Tcl_GetString(objv[1]);
    if (*s=='-' && Tcl_GetIndexFromObj(interp, objv[1], deltriggerSwitches, "switch", 0, &sindex) != TCL_OK)
	return TCL_ERROR;

    delAll=sindex==0;
    prefix=sindex==1;

    if ((prefix && objc!=3)
    || (!prefix && objc!=2)) {
	Tcl_WrongNumArgs(interp,0,objv,"room deltrigger ?-all? ?-prefix? ?name?");
	return TCL_ERROR;
    }

    s=Tcl_GetString(objv[objc-1]);
    l=strlen(s);

    for (trig=room->rprogs;trig;trig=trig_next) {
	trig_next=trig->next;
	if (delAll) rp_delonetrigger(room,trig);
	else if (prefix && strncmp(trig->name,s,l)==0) rp_delonetrigger(room,trig);
	else if (strcmp(trig->name,s)==0) rp_delonetrigger(room,trig);
    }
    return TCL_OK;
}

/* triggers */
TCL_ROOM_CMD(room_triggers) {
    TCLPROG_LIST *trig;
    Tcl_Obj *result;

    result=Tcl_NewListObj(0,NULL);
    for (trig=room->rprogs;trig;trig=trig->next) {
        Tcl_ListObjAppendElement(interp,result,Tcl_NewStringObj(trig->name,-1));
    }
    Tcl_SetObjResult(interp,result);
    return TCL_OK;
}

// chars [-name] <type>
TCL_ROOM_CMD(room_chars) {
    static tclOptionList *charsSwitches[]={"-name",NULL};
    static tclOptionList *typesSwitches[]={"char","pc","mob",NULL};
    int sindex=-1;
    bool name=FALSE;
    CHAR_DATA *pc;
    char *s;
    int field;
    Tcl_Obj *result;

    if (objc<=1) {
	Tcl_WrongNumArgs(interp,0,objv,"room chars ?-name? <type>");
	return TCL_ERROR;
    }

    field=1;
    s=Tcl_GetString(objv[1]);
    if (*s=='-') {
	if (Tcl_GetIndexFromObj(interp, objv[1], charsSwitches, "switch", 0, &sindex) != TCL_OK)
	    return TCL_ERROR;
	if (sindex==0) name=TRUE;
	s=Tcl_GetString(objv[2]);
	field=2;
    }
    sindex=0;
    if (s!=NULL) {
	if (Tcl_GetIndexFromObj(interp, objv[field], typesSwitches, "switch", 0, &sindex) != TCL_OK)
	    return TCL_ERROR;
    }

    result=Tcl_NewListObj(0,NULL);
    pc=room->people;
    while (pc!=NULL) {
	if ( (sindex==0)
	 ||( (sindex==1) && IS_PC(pc))
	 ||( (sindex==2 && IS_NPC(pc)))) {
	    if (name) {
		Tcl_ListObjAppendElement(interp,result,Tcl_NewStringObj(pc->name,-1));
	    } else {
		Tcl_ListObjAppendElement(interp,result,Tcl_NewLongObj(pc->id));
	    }
	}
	pc=pc->next_in_room;
    }
    Tcl_SetObjResult(interp,result);
    return TCL_OK;
}

// descr
TCL_ROOM_CMD(room_descr) {
    if (objc==2) {
	char *newname;
	char snewname[MSL];

	newname=Tcl_GetString(objv[1]);
	free_string(room->description);
	strncpy(snewname,newname,MSL);
	snewname[MSL-3]=0;
	strcat(snewname,"\n\r");
	room->description=str_dup(snewname);
    } else
	Tcl_SetObjResult(interp,Tcl_NewStringObj(room->description,-1));
    return TCL_OK;
}

// extradescr add|del|mod|list [key [value]]
TCL_ROOM_CMD(room_extradescr) {
    char *subcmd;
    char *key;
    char *value;

    if (objc<2) {
	Tcl_WrongNumArgs(interp,0,objv,"obj extradescr add|del|mod|list ?key ?value?? ");
	return TCL_ERROR;
    }
    subcmd=Tcl_GetString(objv[1]);
    if (objc>2)
	key=Tcl_GetString(objv[2]);
    if (objc>3)
	value=Tcl_GetString(objv[3]);
    if (!strcasecmp(subcmd,"list")) {
	EXTRA_DESCR_DATA *edd;
	Tcl_Obj *result;

	result=Tcl_NewListObj(0,NULL);
	for (edd=room->extra_descr;edd;edd=edd->next) {
	    Tcl_ListObjAppendElement(interp,result,Tcl_NewStringObj(edd->keyword,-1));
	}
	Tcl_SetObjResult(interp,result);
	return TCL_OK;
    } else if (!strcasecmp(subcmd,"mod") || !strcasecmp(subcmd,"add")) {
	EXTRA_DESCR_DATA *edd;

	if (objc<4) {
	    Tcl_WrongNumArgs(interp,0,objv,"obj extradescr add|mod key value ");
	    return TCL_ERROR;
	}
	edd=get_extra_descr(key,room->extra_descr);
	if (edd==NULL) {
	    edd=new_extra_descr();
	    edd->keyword=str_dup(key);
	    edd->description=str_dup(value);
	    edd->next=room->extra_descr;
	    room->extra_descr=edd;
	    return TCL_OK;
	}
	free_string(edd->description);
	edd->description=str_dup(value);
	return TCL_OK;
    } else if (!strcasecmp(subcmd,"del")) {
	EXTRA_DESCR_DATA *edd,*pedd;
	if (objc<3) {
	    Tcl_WrongNumArgs(interp,0,objv,"obj extradescr del key ");
	    return TCL_ERROR;
	}
	edd=get_extra_descr(key,room->extra_descr);
	if (edd==NULL) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("obj extradescr del: Extra description doesn't exist ",-1));
	    return TCL_ERROR;
	}
	if (room->extra_descr==edd) {
	    room->extra_descr=edd->next;
	} else {
	    for (pedd=room->extra_descr;pedd->next!=edd;pedd=pedd->next) 
		if (pedd==NULL) {
		    Tcl_SetObjResult(interp,Tcl_NewStringObj("obj extradescr del: "
							     "Extra description exists but not found. Contact coder!",-1));
		    return TCL_ERROR;
		}
		    
	    /* pedd->next == edd */
	    pedd->next=edd->next;
	}
	free_extra_descr(edd);
	return TCL_OK;
    } 
    Tcl_SetObjResult(interp,Tcl_NewStringObj("obj extradescr: unknown subcommand (add/del/mod/list) ",-1));
    return TCL_ERROR;
}

// allobjects
TCL_ROOM_CMD(room_allobjects) {
    OBJ_DATA *obj;
    Tcl_Obj *result;

    result=Tcl_NewListObj(0,NULL);
    for (obj=room->contents;obj!=NULL;obj=obj->next_content) {
	Tcl_ListObjAppendElement(interp,result,Tcl_NewLongObj(obj->id));
    }
    Tcl_SetObjResult(interp,result);
    return TCL_OK;
}

// delproperty <name> <type>
TCL_ROOM_CMD(room_delproperty) {
    char *name;
    int type;
    static tclOptionList *types[]={ "int", "long", "bool", "string", "char", (char *)NULL };

    if (objc!=3) {
	Tcl_WrongNumArgs(interp,0,objv,"obj delproperty name type");
	return TCL_ERROR;
    }
    name=Tcl_GetString(objv[1]);
    if (Tcl_GetIndexFromObj(interp,objv[2],types,"type",0,&type)!=TCL_OK)
	return TCL_ERROR;

    DeleteRoomProperty(room,type+1,name);
    return TCL_OK;
}

TCL_ROOM_CMD(room_namespace) {
    Tcl_SetObjResult(interp,Tcl_NewStringObj(namespace_room(room),-1));
    return TCL_OK;
}
