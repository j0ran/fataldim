/* $Id: magic.c,v 1.105 2006/08/25 09:43:20 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

/*
 * 19980322 EG  Modified spell_word_of_recall() to recall to
		recall-points instead of the default temple.
 */

#include "merc.h"
#include "interp.h"
#include "color.h"


/*
 * Lookup a skill by name.
 */
int skill_lookup( const char *name )
{
    int sn, found = -1;
    char *s;

    for ( sn = 0; sn < MAX_SKILL; sn++ )
    {
	if ( !skill_exists(sn) )
	    break;
	s=skill_name(sn,NULL);
	if ( LOWER(name[0]) == LOWER(s[0])
	&&   !str_prefix( name, s )
	&& skill_implemented(sn,NULL)) {
	    if (!str_cmp(name,s)) 
		return sn;
	    else
		found=sn;
	}
    }

    return found;
}

int find_spell( CHAR_DATA *ch, const char *name )
{
    /* finds a spell the character can cast if possible */
    int sn, found = -1, trained = -1;
    char *s;

    if (IS_NPC(ch))
	return skill_lookup(name);

    for ( sn = 0; sn < MAX_SKILL; sn++ )
    {
	if (!skill_exists(sn))
	    break;
	s=skill_name(sn,ch);
	if (LOWER(name[0]) == LOWER(s[0])
	&&  !str_prefix(name,s))
	{
	    if ( found == -1)
		found = sn;
	    if (has_skill_available(ch,sn)
	    &&  trained == -1)
		    trained=sn;
	    if (!str_cmp(name,s))
		return sn;
	}
    }
    if (trained != -1) return trained;
    return found;
}



/*
 * Utter mystical words for an sn.
 */
void say_spell( CHAR_DATA *ch, int sn )
{
    char buf  [MAX_STRING_LENGTH];
    char buf2 [MAX_STRING_LENGTH];
    CHAR_DATA *rch;
    char *pName;
    int iSyl;
    int length;

    struct syl_type
    {
	char *	old;
	char *	new;
    };

    static const struct syl_type syl_table[] =
    {
	{ " ",		" "		},
	{ "ar",		"abra"		},
	{ "au",		"kada"		},
	{ "bless",	"fido"		},
	{ "blind",	"nose"		},
	{ "bur",	"mosa"		},
	{ "cu",		"judi"		},
	{ "de",		"oculo"		},
	{ "en",		"unso"		},
	{ "light",	"dies"		},
	{ "lo",		"hi"		},
	{ "mor",	"zak"		},
	{ "move",	"sido"		},
	{ "ness",	"lacri"		},
	{ "ning",	"illa"		},
	{ "per",	"duda"		},
	{ "ra",		"gru"		},
	{ "fresh",	"ima"		},
	{ "re",		"candus"	},
	{ "son",	"sabru"		},
	{ "tect",	"infra"		},
	{ "tri",	"cula"		},
	{ "ven",	"nofo"		},
	{ "a", "a" }, { "b", "b" }, { "c", "q" }, { "d", "e" },
	{ "e", "z" }, { "f", "y" }, { "g", "o" }, { "h", "p" },
	{ "i", "u" }, { "j", "y" }, { "k", "t" }, { "l", "r" },
	{ "m", "w" }, { "n", "i" }, { "o", "a" }, { "p", "s" },
	{ "q", "d" }, { "r", "f" }, { "s", "g" }, { "t", "h" },
	{ "u", "j" }, { "v", "z" }, { "w", "x" }, { "x", "n" },
	{ "y", "l" }, { "z", "k" },
	{ "", "" }
    };

    buf[0]	= '\0';
    for ( pName = skill_name(sn,ch) ; *pName != '\0'; pName += length )
    {
	for ( iSyl = 0; (length = strlen(syl_table[iSyl].old)) != 0; iSyl++ )
	{
	    if ( !str_prefix( syl_table[iSyl].old, pName ) )
	    {
		strcat( buf, syl_table[iSyl].new );
		break;
	    }
	}

	if ( length == 0 )
	    length = 1;
    }

    sprintf(buf2, "$n utters the words, '"C_SAY"%s{x'.", buf );
    sprintf(buf,  "$n utters the words, '"C_SAY"%s{x'.", skill_name(sn,ch));

    for ( rch = ch->in_room->people; rch; rch = rch->next_in_room )
    {
	if ( rch != ch ) {
	    if (skillcheck(rch,gsn_magic_lore)) {
		act(buf,ch,NULL,rch,TO_VICT);
		check_improve(rch,gsn_magic_lore,TRUE,1);
	    } else {
		act(buf2,ch,NULL,rch,TO_VICT);
		check_improve(rch,gsn_magic_lore,FALSE,1);
	    }
        }
    }

    return;
}


/* make alignment of two characters a little more closer */
void change_alignment(CHAR_DATA *ch1,CHAR_DATA *ch2,int value) {

    if (ch1==ch2)
	return;

    if (abs(ch1->alignment-ch2->alignment)<value) {
	value=abs(ch1->alignment-ch2->alignment)/2;
    }

    if (ch1->alignment < ch2->alignment) {
	ch1->alignment=UMIN(ch1->alignment+value,1000);
	ch2->alignment=UMAX(ch2->alignment-value,-1000);
    } else {
	ch1->alignment=UMAX(ch1->alignment-value,-1000);
	ch2->alignment=UMIN(ch2->alignment+value,1000);
    }
    char_obj_alignment_check(ch1);
    char_obj_alignment_check(ch2);
}


/*
 * Compute a saving throw.
 * Negative apply's make saving throw better.
 */
bool saves_spell( int level, CHAR_DATA *victim, int dam_type )
{
    int save;

    save = 50 + ( victim->level - level) * 5 - victim->saving_throw * 2;
    if (IS_AFFECTED(victim,EFF_BERSERK))
	save += victim->level/2;

    switch(check_immune(victim,dam_type))
    {
	case IS_IMMUNE:		return TRUE;
	case IS_RESISTANT:	save += 2;	break;
	case IS_VULNERABLE:	save -= 2;	break;
    }

    if (!IS_NPC(victim) && class_table[victim->class].fMana)
	save = 9 * save / 10;
    save = URANGE( 5, save, 95 );
    return (bool)(number_percent( ) < save);
}

/* RT save for dispels */

bool saves_dispel( int dis_level, int spell_level, int duration)
{
    int save;

    if (duration == -1)
      spell_level += 5;
      /* very hard to dispel permanent effects */

    save = 50 + (spell_level - dis_level) * 5;
    save = URANGE( 5, save, 95 );
    return (bool)(number_percent( ) < save);
}

/* co-routine for dispel magic and cancellation */

bool check_dispel( int dis_level, CHAR_DATA *victim, int sn)
{
    EFFECT_DATA *ef;
    WEAROFF_FUN *wearoff_fun;

    if (is_affected(victim, sn))
    {
        for ( ef = victim->affected; ef != NULL; ef = ef->next )
        {
            if ( ef->type == sn )
            {
                if (!saves_dispel(dis_level,ef->level,ef->duration))
                {
		    if ((wearoff_fun=skill_wearoff_fun(sn,victim))!=NULL)
			wearoff_fun(victim);

                    effect_strip(victim,sn);

		    if ( skill_msg_off(sn,NULL)) {
			send_to_char( skill_msg_off(sn,NULL), victim );
			send_to_char( "\n\r", victim );
        	    }
		    if (skill_msg_off_room(sn,NULL)) {
			act(skill_msg_off_room(sn,NULL),
			    victim,NULL,NULL,TO_ROOM);
		    }
		    return TRUE;
		}
		else
		    ef->level--;
            }
        }
    }
    return FALSE;
}

/* for finding mana costs -- temporary version */
int mana_cost (CHAR_DATA *ch, int min_mana, int level)
{
    if (ch->level + 2 == level)
	return 1000;
    return UMAX(min_mana,(100/(2 + ch->level - level)));
}

int is_target_reflective(CHAR_DATA *ch, CHAR_DATA *victim) {
    OBJ_DATA *obj;
    int charges_left;
    int reflect_level;

    obj=victim->carrying;

    while (obj!=NULL) {
	if ((obj->wear_loc!=WEAR_NONE) &&
	    STR_IS_SET(obj->strbit_extra_flags,ITEM_REFLECTING)) {
	    if (!GetObjectProperty(obj,PROPERTY_INT,"reflect-magic-charges",&charges_left))
		charges_left=-1;
	    if (!GetObjectProperty(obj,PROPERTY_INT,"reflect-magic-level",&reflect_level))
		reflect_level=obj->level;
	    if (((charges_left>0) || (charges_left==-1)) && (reflect_level>=ch->level)) {
		act_new("Your spell reflects off of $p.",ch,obj,victim,TO_CHAR,POS_DEAD);
		act_new("$s spell reflects off of $p.",ch,obj,victim,TO_VICT,POS_DEAD);
		act_new("$s spell reflects off of $p.",ch,obj,victim,TO_NOTVICT,POS_DEAD);
		if (charges_left!=-1) {
		    if ((--charges_left)<=0) {
			act_new("$p explodes!",ch,obj,NULL,TO_CHAR,POS_DEAD);
			act_new("$p explodes!",ch,obj,NULL,TO_ROOM,POS_DEAD);
			spell_fireball(gsn_fireball, reflect_level, victim, victim, TARGET_CHAR);
			extract_obj(obj);
		    } else {
			SetObjectProperty(obj,PROPERTY_INT,"reflect-magic-charges",&charges_left);
		    }
		}
		return TRUE;
	    }
	}
	obj=obj->next_content;
    }

    return FALSE;
}

/*
 * The kludgy global is for spells who want more stuff from command line.
 */
char *target_name;

void do_cast( CHAR_DATA *ch, char *argument )
{
    char arg1[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    OBJ_DATA *obj;
    void *vo;
    int mana;
    int sn;
    int target;

    /*
     * Switched NPC's can cast spells, but others can't.
     */
    if ( IS_NPC(ch) && ch->desc == NULL)
	return;

    target_name = one_argument( argument, arg1 );
    one_argument( target_name, arg2 );

    if ( arg1[0] == '\0' )
    {
	send_to_char( "Cast which what where?\n\r", ch );
	return;
    }

    if ((sn = find_spell(ch,arg1)) < 1
    ||  skill_spell_fun(sn,ch) == spell_null
    || (!IS_NPC(ch) && (ch->level < skill_level(sn,ch)
    ||  	 	!has_skill_available(ch,sn))))
    {
	send_to_char( "You don't know any spells of that name.\n\r", ch );
	return;
    }

    if( STR_IS_SET(ch->in_room->strbit_room_flags,ROOM_NOMAGIC) && !IS_IMMORTAL(ch) )
    {
	send_to_char("This place prevents you from casting spells.\n\r", ch );
	return;
    }

    if ( ch->position < skill_minimum_position(sn,ch) )
    {
	send_to_char( "You can't concentrate enough.\n\r", ch );
	return;
    }

    if (ch->level + 2 == skill_level(sn,ch))
	mana = 50;
    else
    	mana = UMAX(
	    skill_min_mana(sn,ch),
	    100 / ( 2 + ch->level - skill_level(sn,ch)) );

    /*
     * Locate targets.
     */
    victim	= NULL;
    obj		= NULL;
    vo		= NULL;
    target	= TARGET_NONE;

    switch ( skill_target(sn,ch) )
    {
    default:
	bugf( "Do_cast: bad target for sn %d for %s.", sn,NAME(ch) );
	return;

    case TAR_IGNORE:
	break;

    case TAR_CHAR_OFFENSIVE:
	if ( arg2[0] == '\0' )
	{
	    if ( ( victim = ch->fighting ) == NULL )
	    {
		send_to_char( "Cast the spell on whom?\n\r", ch );
		return;
	    }
	}
	else
	{
	    if ( ( victim = get_char_room( ch, target_name ) ) == NULL )
	    {
		send_to_char( "They aren't here.\n\r", ch );
		return;
	    }
	}
/*
        if ( ch == victim )
        {
            send_to_char( "You can't do that to yourself.\n\r", ch );
            return;
        }
*/


	if ( !IS_NPC(ch) )
	{

            if (is_safe(ch,victim) && victim != ch)
	    {
		send_to_char("Not on that target.\n\r",ch);
		return;
	    }
	    if (MOB_HAS_TRIGGER(victim,MTRIG_PREATTACK) &&
		ch->fighting!=victim &&
		!mp_preattack_trigger(victim,ch,"magic"))
		return;
	    check_killer(ch,victim);
	}

        if ( IS_AFFECTED(ch, EFF_CHARM) && ch->master == victim )
	{
	    send_to_char( "You can't do that on your own follower.\n\r",
		ch );
	    return;
	}

	if (victim->position==POS_DEAD) {
	    act_new("$E is dead, sorry.\n\r",ch,NULL,victim,TO_CHAR,POS_DEAD);
	    return;
	}

	if (IS_NPC(victim) && IS_PC(ch)) {
	    int ool_timeout;
	    if ((victim->fighting) && (ch!=victim->fighting) &&
		(IS_PC(victim->fighting)) &&
		(ch->level>victim->fighting->level+10)) {
		wiznet(WIZ_OOL_DETECTS,0,ch,NULL,
		    "Possible OOL detected: %s[%d] is fighting %s[%d] "
		    "and %s[%d] is casting spells.",
		    victim->fighting->name,
		    victim->fighting->level,
		    victim->pIndexData->short_descr,victim->level,
		    ch->name,ch->level);
		logf("[0] Possible OOL detected: %s[%d] is fighting %s[%d] "
		    "and %s[%d] is casting spells.",
		    victim->fighting->name,
		    victim->fighting->level,
		    victim->pIndexData->short_descr,victim->level,
		    ch->name,ch->level);
		log_char(ch,"OOL trigger",10);
		log_char(victim->fighting,"OOL trigger",10);
		ool_penalize(ch);
	    }
	    SetCharProperty(victim,PROPERTY_INT,"ool_level",&ch->level);
	    SetCharProperty(victim,PROPERTY_STRING,"ool_char",ch->name);
	    ool_timeout=100;
	    SetCharProperty(victim,PROPERTY_INT,"ool_timeout",&ool_timeout);
	}

	vo = (void *) victim;
	target = TARGET_CHAR;
	break;

    case TAR_CHAR_DEFENSIVE:
	if ( arg2[0] == '\0' )
	{
	    victim = ch;
	}
	else
	{
	    if ( ( victim = get_char_room( ch, target_name ) ) == NULL )
	    {
		send_to_char( "They aren't here.\n\r", ch );
		return;
	    }
	}

	if (victim->position==POS_DEAD) {
	    act_new("$E is dead, sorry.\n\r",ch,NULL,victim,TO_CHAR,POS_DEAD);
	    return;
	}

	vo = (void *) victim;
	target = TARGET_CHAR;
	break;

    case TAR_CHAR_SELF:
	if ( arg2[0] != '\0' && !is_name( target_name, ch->name ) )
	{
	    send_to_char( "You cannot cast this spell on another.\n\r", ch );
	    return;
	}

	vo = (void *) ch;
	target = TARGET_CHAR;
	break;

    case TAR_OBJ_INV:
	if ( arg2[0] == '\0' )
	{
	    send_to_char( "What should the spell be cast upon?\n\r", ch );
	    return;
	}

	if ( ( obj = get_obj_carry( ch, target_name, ch ) ) == NULL )
	{
	    send_to_char( "You are not carrying that.\n\r", ch );
	    return;
	}

	vo = (void *) obj;
	target = TARGET_OBJ;
	break;

    case TAR_OBJ_CHAR_OFF:
	if (arg2[0] == '\0')
	{
	    if ((victim = ch->fighting) == NULL)
	    {
		send_to_char("Cast the spell on whom or what?\n\r",ch);
		return;
	    }

	    target = TARGET_CHAR;
	}
	else if ((victim = get_char_room(ch,target_name)) != NULL)
	{
	    target = TARGET_CHAR;
	}

	if (target == TARGET_CHAR) /* check the sanity of the attack */
	{
	    if(is_safe_spell(ch,victim,FALSE) && victim != ch)
	    {
		send_to_char("Not on that target.\n\r",ch);
		return;
	    }
	    if (MOB_HAS_TRIGGER(victim,MTRIG_PREATTACK) &&
		ch->fighting!=victim &&
		!mp_preattack_trigger(victim,ch,"magic"))
		return;

            if ( IS_AFFECTED(ch, EFF_CHARM) && ch->master == victim )
            {
                send_to_char( "You can't do that on your own follower.\n\r",
                    ch );
                return;
            }

	    if (!IS_NPC(ch))
		check_killer(ch,victim);

	    vo = (void *) victim;
 	}
	else if ((obj = get_obj_here(ch,target_name)) != NULL)
	{
	    vo = (void *) obj;
	    target = TARGET_OBJ;
	}
	else
	{
	    send_to_char("You don't see that here.\n\r",ch);
	    return;
	}
	break;

    case TAR_OBJ_CHAR_DEF:
        if (arg2[0] == '\0')
        {
            vo = (void *) ch;
            target = TARGET_CHAR;
        }
        else if ((victim = get_char_room(ch,target_name)) != NULL)
        {
            vo = (void *) victim;
            target = TARGET_CHAR;
	}
	else if ((obj = get_obj_carry(ch,target_name,ch)) != NULL)
	{
	    vo = (void *) obj;
	    target = TARGET_OBJ;
	}
	else
	{
	    send_to_char("You don't see that here.\n\r",ch);
	    return;
	}
	break;

    case TAR_OBJ_ROOM:
	if (arg2[0]==0) {
	    send_to_char(
		"On which object do you want to cast that spell?\n\r",ch);
	    return;
	}

	if ((obj=get_obj_list(ch,arg2,ch->in_room->contents))==NULL) {
	    send_to_char("You don't see that here.\n\r",ch);
	    return;
	}

	vo=(void *)obj;
	target=TARGET_OBJ;
	break;
    }

    if ( !IS_NPC(ch) && ch->mana < mana )
    {
	send_to_char( "You don't have enough mana.\n\r", ch );
	return;
    }

    if ( str_cmp( skill_name(sn,ch), "ventriloquate" ) )
	say_spell( ch, sn );

    if (IS_NPC(ch) || get_trust(ch)<LEVEL_COUNCIL)
        WAIT_STATE( ch, skill_beats(sn,ch) );

    if ( !skillcheck(ch,sn) )
    {
	send_to_char( "You lost your concentration.\n\r", ch );
	check_improve(ch,sn,FALSE,1);
	ch->mana -= mana / 2;
    }
    else
    {
	// ok the spell is succesfull, now will it reflect?
	if ((target==TARGET_CHAR) &&
	    ((skill_target(sn,ch)==TAR_CHAR_DEFENSIVE) || (skill_target(sn,ch)==TAR_CHAR_OFFENSIVE))) {
            if (is_target_reflective(ch,victim)==TRUE) {
		vo=ch;
		victim=ch;
	    }
	}
        ch->mana -= mana;
        if (IS_NPC(ch) || class_table[ch->class].fMana)
	/* class has spells */
            (*skill_spell_fun(sn,ch)) ( sn, ch->level, ch, vo,target);
        else
            (*skill_spell_fun(sn,ch)) (sn, 3 * ch->level/4, ch, vo,target);
        check_improve(ch,sn,TRUE,1);
    }

    if ((skill_target(sn,ch) == TAR_CHAR_OFFENSIVE
    ||   (skill_target(sn,ch) == TAR_OBJ_CHAR_OFF && target == TARGET_CHAR))
    &&   victim != ch
    &&   victim->master != ch)
    {
	CHAR_DATA *vch;
	CHAR_DATA *vch_next;

	for ( vch = ch->in_room->people; vch; vch = vch_next )
	{
	    vch_next = vch->next_in_room;
	    if ( victim == vch && victim->fighting == NULL )
	    {	check_killer(victim,ch);
		multi_hit( victim, ch, TYPE_UNDEFINED );
		break;
	    }
	}
    }

    return;
}



/*
 * Cast spells at targets using a magical object.
 */
void obj_cast_spell( int sn, int level, CHAR_DATA *ch, CHAR_DATA *victim, OBJ_DATA *obj, char *targetname )
{
    void *vo;
    int target = TARGET_NONE;

    if ( sn <= 0 )
	return;

    if ( sn >= MAX_SKILL || skill_spell_fun(sn,ch) == 0 )
    {
	bugf( "Obj_cast_spell: bad sn %d for %s.", sn,NAME(ch));
	return;
    }

    switch ( skill_target(sn,ch) )
    {
    default:
	bugf( "Obj_cast_spell: bad target for sn %d for %s.", sn,NAME(ch) );
	return;

    case TAR_IGNORE:
	vo = NULL;
	break;

    case TAR_CHAR_OFFENSIVE:
	if ( victim == NULL )
	    victim = ch->fighting;
	if ( victim == NULL )
	{
	    send_to_char( "You can't do that.\n\r", ch );
	    return;
	}
	if (is_safe(ch,victim) && ch != victim)
	{
	    send_to_char("Something isn't right...\n\r",ch);
	    return;
	}
        if (IS_NPC(victim) && IS_PC(ch)) {
	    int ool_timeout;
            if ((victim->fighting) && (ch!=victim->fighting) &&
                (IS_PC(victim->fighting)) &&
                (ch->level>victim->fighting->level+10)) {
                wiznet(WIZ_OOL_DETECTS,0,NULL,NULL,
		    "Possible OOL detected: %s[%d] is fighting %s[%d] "
		    "and %s[%d] is casting spells.",
		    victim->fighting->name,victim->fighting->level,
		    victim->pIndexData->short_descr,
		    victim->level,ch->name,ch->level);
                logf("[0] Possible OOL detected: %s[%d] is fighting %s[%d] "
		    "and %s[%d] is casting spells.",
		    victim->fighting->name,victim->fighting->level,
		    victim->pIndexData->short_descr,
		    victim->level,ch->name,ch->level);
		log_char(ch,"OOL trigger",10);
		log_char(victim->fighting,"OOL trigger",10);
		ool_penalize(ch);
            }
            SetCharProperty(victim,PROPERTY_INT,"ool_level",&ch->level);
            SetCharProperty(victim,PROPERTY_STRING,"ool_char",ch->name);
	    ool_timeout=100;
	    SetCharProperty(victim,PROPERTY_INT,"ool_timeout",&ool_timeout);
        }

	vo = (void *) victim;
	target = TARGET_CHAR;
	break;

    case TAR_CHAR_DEFENSIVE:
    case TAR_CHAR_SELF:
	if ( victim == NULL )
	    victim = ch;
	vo = (void *) victim;
	target = TARGET_CHAR;
	break;

    case TAR_OBJ_INV:
	if ( obj == NULL )
	{
	    send_to_char( "You can't do that.\n\r", ch );
	    return;
	}
	vo = (void *) obj;
	target = TARGET_OBJ;
	break;

    case TAR_OBJ_CHAR_OFF:
        if ( victim == NULL && obj == NULL) {
	    if (ch->fighting != NULL)
		victim = ch->fighting;
	    else
	    {
		send_to_char("You can't do that.\n\r",ch);
		return;
	    }
        }

        if (victim != NULL)
	{
            if (is_safe_spell(ch,victim,FALSE) && ch != victim)
	    {
	        send_to_char("Somehting isn't right...\n\r",ch);
	        return;
	    }
            vo = (void *) victim;
	    target = TARGET_CHAR;
	}
	else
	{
            vo = (void *) obj;
	    target = TARGET_OBJ;
	}
        break;


    case TAR_OBJ_CHAR_DEF:
	if (victim == NULL && obj == NULL)
	{
	    vo = (void *) ch;
	    target = TARGET_CHAR;
	}
	else if (victim != NULL)
	{
	    vo = (void *) victim;
	    target = TARGET_CHAR;
	}
	else
	{
	    vo = (void *) obj;
	    target = TARGET_OBJ;
	}

	break;
    }

    target_name = targetname;
    (*skill_spell_fun(sn,ch)) ( sn, level, ch, vo,target);



    if ( (skill_target(sn,ch) == TAR_CHAR_OFFENSIVE
    ||   (skill_target(sn,ch) == TAR_OBJ_CHAR_OFF && target == TARGET_CHAR))
    &&   victim != ch
    &&   victim->master != ch )
    {
	CHAR_DATA *vch;
	CHAR_DATA *vch_next;

	for ( vch = ch->in_room->people; vch; vch = vch_next )
	{
	    vch_next = vch->next_in_room;
	    if ( victim == vch && victim->fighting == NULL )
	    {
		check_killer(victim,ch);
		multi_hit( victim, ch, TYPE_UNDEFINED );
		break;
	    }
	}
    }

    return;
}

int get_obj_level( OBJ_DATA *obj ) {
    EFFECT_DATA *pef;

    if( !IS_OBJ_STAT2( obj, ITEM_LEVEL ) ) return obj->level;
    for (pef=obj->affected;pef;pef=pef->next) {
	if (pef->where==TO_OBJECT2
	&& pef->bitvector==ITEM_LEVEL) return pef->arg1;
    }

    return obj->level;
}

void wearoff_null( CHAR_DATA *ch ) {
    return;
}

void spell_null( int sn, int level, CHAR_DATA *ch, void *vo, int target ) {
    send_to_char( "That's not a spell!\n\r", ch );
    bugf("spell_null(): spell_null called for %d.",sn);
    return;
}
