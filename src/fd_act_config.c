//
// $Id: fd_act_config.c,v 1.46 2006/08/30 13:59:12 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"
#include "interp.h"

void config_autolist(CHAR_DATA *ch, char *argument);
void config_show(CHAR_DATA *ch, char *argument);

static char *OnOffTxt(long b) {
    if (b)
	return "ON";
    else
	return "OFF";
}

void AboutPublish(CHAR_DATA *ch) {
    bool b;

    b=FALSE;
    GetCharProperty(ch,PROPERTY_BOOL,"publish",&b);
    if (b)
	send_to_char("This information will be displayed on the website. "
	    "Use '{Wconfig publish{x' to disable this.\n\r",ch);
    else
	send_to_char("This information will not be displayed on the website. "
	    "Use '{Wconfig publish{x' to enable this.\n\r",ch);
}

void do_config(CHAR_DATA *ch,char *argument) {
    char arg[MSL],buf[MSL];
    int i;

    if (IS_NPC(ch)) return;
    if (check_ordered(ch)) return;

    argument=one_argument(argument,arg);

    switch (which_keyword(arg,	"email","prompt","channels","scroll",
				"auto","noloot","nosummon","combine",
				"brief", "colour", "color", "password",
				"picture", "pueblo", "icq", "yahoo", "homepage",
				"publish","autoloot","autoassist","autogold",
				"autoexit","autosac","autosplit","autotitle",
				"autocompact","autocombine","show","mxp",
				"page","pkill","msn","aim","compass",
#ifdef HAS_ALTS
				"alts","uberalt",
#else
				"XXXXXX","XXXXXX",	// has to be here.
#endif
				"xmpp",NULL)) {
    case -1:
    case 0:
	send_to_char("The following options are available:\n\r",ch);
	send_to_char("  email, prompt, channels, scroll, auto,\n\r",ch);
	send_to_char("  noloot, nosummon, page, combine, brief, colour\n\r",ch);
	send_to_char("  password, picture, icq, yahoo, msn, aim, homepage\n\r",ch);
	send_to_char("  xmpp, publish, show, mxp, pkill, compass,\n\r",ch);
#ifdef HAS_ALTS
	send_to_char("  alts, uberalt\n\r",ch);
#endif
	return;

    case 1:  // email
	if (argument[0]==0) {
	    if (GetCharProperty(ch,PROPERTY_STRING,"email",buf)) {
		sprintf_to_char(ch,
		    "Your current email address is '{W%s{x'.\n\r",buf);
		AboutPublish(ch);
	    } else
		send_to_char("You don't have an "
		    "email address specified yet.\n\r",ch);
	    return;
	}
	if (!str_cmp(argument,"delete")) {
	    send_to_char("Email address removed from your profile.\n\r",ch);
	    DeleteCharProperty(ch,PROPERTY_STRING,"email");
	    return;
	}
	smash_tilde(argument);
	smash_tilde(argument);
	SetCharProperty(ch,PROPERTY_STRING,"email",argument);
	send_to_char( "Ok.\n\r", ch );
	AboutPublish(ch);
	return;

    case 2:  // prompt
	if (argument[0]==0) {
	    sprintf_to_char(ch,
		"Your current prompt is '{W%s{x'.\n\r"
		"The default prompt is "
		"'{W%%h/%%Hhp %%m/%%Mma %%vmv | %%e >{x'\n\r",
		ch->prompt);
	    return;
	}
	strcpy(buf,argument);
	smash_tilde(buf);
	if (str_suffix("%c",buf))
	    strcat(buf," ");
	free_string( ch->prompt );
	ch->prompt = str_dup( buf );
	sprintf_to_char(ch,"Prompt set to %s\n\r",ch->prompt );
	return;

    case 3:  // channels
	send_to_char("channel        status\n\r",ch);
	send_to_char("---------------------\n\r",ch);
	sprintf_to_char(ch,"Gossip         %s\n\r",
		    OnOffTxt(!STR_IS_SET(ch->strbit_comm,COMM_NOGOSSIP)));
	sprintf_to_char(ch,"Newbie gossip  %s\n\r",
		    OnOffTxt(!STR_IS_SET(ch->strbit_comm,COMM_NONEWBIE)));
	sprintf_to_char(ch,"Auction        %s\n\r",
		    OnOffTxt(!STR_IS_SET(ch->strbit_comm,COMM_NOAUCTION)));
	sprintf_to_char(ch,"Music          %s\n\r",
		    OnOffTxt(!STR_IS_SET(ch->strbit_comm,COMM_NOMUSIC)));
	sprintf_to_char(ch,"Q/A            %s\n\r",
		    OnOffTxt(!STR_IS_SET(ch->strbit_comm,COMM_NOQUESTION)));
	sprintf_to_char(ch,"Quest          %s\n\r",
		    OnOffTxt(!STR_IS_SET(ch->strbit_comm,COMM_NOQUEST)));
	sprintf_to_char(ch,"Quote          %s\n\r",
		    OnOffTxt(!STR_IS_SET(ch->strbit_comm,COMM_NOQUOTE)));
	sprintf_to_char(ch,"Grats          %s\n\r",
		    OnOffTxt(!STR_IS_SET(ch->strbit_comm,COMM_NOGRATS)));
	if (IS_IMMORTAL(ch))
	    sprintf_to_char(ch,"God channel    %s\n\r",
		    OnOffTxt(!STR_IS_SET(ch->strbit_comm,COMM_NOWIZ)));
	if (ch->level==LEVEL_HERO)
	    sprintf_to_char(ch,"Hero channel   %s\n\r",
		    OnOffTxt(!STR_IS_SET(ch->strbit_comm,COMM_NOWIZ)));
	send_to_char("Pray channel   ",ch);
	if (!IS_IMMORTAL(ch))
	    send_to_char("Available\n\r",ch);
	else
	    sprintf_to_char(ch,"%s\n\r",
		    OnOffTxt(!STR_IS_SET(ch->strbit_comm,COMM_NOPRAY)));
	sprintf_to_char(ch,"Shouts         %s\n\r",
		    OnOffTxt(!STR_IS_SET(ch->strbit_comm,COMM_SHOUTSOFF)));
	sprintf_to_char(ch,"Tells          %s\n\r",
		    OnOffTxt(!STR_IS_SET(ch->strbit_comm,COMM_DEAF)));
	sprintf_to_char(ch,"Pages          %s\n\r",
		    OnOffTxt(!STR_IS_SET(ch->strbit_comm,COMM_NOPAGE)));
	sprintf_to_char(ch,"Announce       %s\n\r",
		    OnOffTxt(STR_IS_SET(ch->strbit_comm,COMM_ANNOUNCE)));
	sprintf_to_char(ch,"Quiet mode     %s\n\r",
		    OnOffTxt(STR_IS_SET(ch->strbit_comm,COMM_QUIET)));

	if (STR_IS_SET(ch->strbit_comm,COMM_SNOOP_PROOF))
	    send_to_char("You are immune to snooping.\n\r",ch);

	if (STR_IS_SET(ch->strbit_comm,COMM_NOSHOUT))
	    send_to_char("You cannot shout.\n\r",ch);
	if (STR_IS_SET(ch->strbit_comm,COMM_NOTELL))
	    send_to_char("You cannot use tell.\n\r",ch);
	if (STR_IS_SET(ch->strbit_comm,COMM_NOCHANNELS))
	    send_to_char("You cannot use the communication channels.\n\r",ch);
	if (STR_IS_SET(ch->strbit_comm,COMM_NOEMOTE))
	    send_to_char("You cannot use the emote command.\n\r",ch);
	if (STR_IS_SET(ch->strbit_comm,COMM_NOCOLOR))
	    send_to_char("You cannot use any colors.\n\r",ch);

	send_to_char(
	    "Use the name of the channels to enable/disable it.\n\r",ch);
	return;

    case 4:  // scroll

	// this value can also be set by the NAWS telnet option
	if (IS_NPC(ch))
	    return;
	if (ch->desc==NULL)	// ignore these things from linkdead
	    return;			// players

	argument=one_argument(argument,arg);

	if (arg[0]==0) {

	    // show terminal info
	    if (ch->desc->lines==0)
		send_to_char("Terminal height not known.\n\r",ch);
	    else
		sprintf_to_char(ch,"Your terminal displays %d lines.\n\r",
				ch->desc->lines+2);
	    // show configured
	    if (ch->pcdata->default_lines<0)
		sprintf_to_char(ch,"You have configure autodetect paging. (default height %d)\n\r",
				-ch->pcdata->default_lines+2);
	    else if (ch->pcdata->default_lines==0)
		send_to_char("You have configure paging to be disabled.\n\r",ch);
	    else 
		sprintf_to_char(ch,"You have configured a fixed page size of %d.\n\r",
				ch->pcdata->default_lines+2);
	    // show current
	    i=get_page_length(ch->desc,ch)+2;
	    if (i==0)
		send_to_char("paging is disabled.\n\r",ch);
	    else
		sprintf_to_char(ch,"Page length is now %d.\n\r",i);

	    return;
	}

	if (!str_cmp(arg,"auto")) {
	    argument=one_argument(argument,arg);
	    if (!is_number(arg))
		i=PAGELEN+2;
	    else {
		i=atoi(arg);
		if (i<10 || i>100) {
		    send_to_char("Default pagelength doesn't make sence.\n\r",ch);
		    return;
		}
	    }
	    ch->pcdata->default_lines=-(i-2);
	    sprintf_to_char(ch,"Scroll set to automatic, with a default of %d lines.\n\r",i);
	    return;
	}

	if (!is_number(arg)) {
	    send_to_char("You must provide a number.\n\r",ch);
	    return;
	}
	i=atoi(arg);
	if (i==0) {
	    send_to_char("Paging disabled.\n\r",ch);
	    ch->pcdata->default_lines=0;
	    return;
	}
	if (i<10 || i>100) {
	    send_to_char("You must provide a reasonable number.\n\r",ch);
	    return;
	}
	sprintf_to_char(ch,"Scroll set to %d lines.\n\r",i);
	ch->pcdata->default_lines=i-2;
	return;

    case 5:  // auto
	config_autolist(ch,argument);
	return;

    case 6:  // noloot
	if (STR_IS_SET(ch->strbit_act,PLR_CANLOOT)) {
	    send_to_char("Your corpse is now safe from thieves.\n\r",ch);
	    STR_REMOVE_BIT(ch->strbit_act,PLR_CANLOOT);
	} else {
	    send_to_char("Your corpse may now be looted.\n\r",ch);
	    STR_SET_BIT(ch->strbit_act,PLR_CANLOOT);
	}
	if (is_clan(ch))
	    send_to_char("Since you're a member of this clan this option "
		"is useless for you.\n\r",ch);
	return;

    case 7:  // nosummon
	if (STR_IS_SET(ch->strbit_act,PLR_NOSUMMON)) {
	    send_to_char("You are no longer immune to summon.\n\r",ch);
	    STR_REMOVE_BIT(ch->strbit_act,PLR_NOSUMMON);
	} else {
	    send_to_char("You are now immune to summoning.\n\r",ch);
	    STR_SET_BIT(ch->strbit_act,PLR_NOSUMMON);
	}
	return;

    case 8:  // combine
	switch(which_keyword(argument,"on","off","toggle",NULL)) {
	    case 3: STR_TOGGLE_BIT(ch->strbit_comm, COMM_COMBINE); break;
	    case 1: STR_SET_BIT(ch->strbit_comm, COMM_COMBINE); break;
	    case 2: STR_REMOVE_BIT(ch->strbit_comm, COMM_COMBINE); break;
	    case 0:
	    case -1:
		send_to_char(
		    "Usage: config combine [on|off|toggle].\n\r",ch);
		sprintf_to_char(ch,"Currently is %s inventory selected.\n\r",
		    STR_IS_SET(ch->strbit_comm,COMM_COMBINE) ? "combined" : "long" );
		return;
	}
	sprintf_to_char(ch,"%s inventory selected.\n\r",
	    STR_IS_SET(ch->strbit_comm,COMM_COMBINE)?"Combined":"Long");
	return;

    case 9:  // brief
	switch(which_keyword(argument,"on","off","toggle",NULL)) {
	    case 3: STR_TOGGLE_BIT(ch->strbit_comm,COMM_BRIEF); break;
	    case 1: STR_SET_BIT(ch->strbit_comm,COMM_BRIEF); break;
	    case 2: STR_REMOVE_BIT(ch->strbit_comm,COMM_BRIEF); break;
	    case 0:
	    case -1:
		send_to_char("Usage: config brief [on|off|toggle].\n\r",ch);
		sprintf_to_char(ch,
		    "Currently the %s descriptions are activated.\n\r",
		    STR_IS_SET(ch->strbit_comm,COMM_BRIEF)?"short":"full");
		return;
	}
	sprintf_to_char(ch,"%s descriptions activated.\n\r",
	    STR_IS_SET(ch->strbit_comm,COMM_BRIEF) ? "Short" : "Full" );
	return;

    case 10: // colour
    case 11: // color
	if( !STR_IS_SET( ch->strbit_act, PLR_COLOUR ) ) {
	    STR_SET_BIT( ch->strbit_act, PLR_COLOUR );
	    send_to_char( "You put on your {Mpink{x glasses!\n\r", ch );
	} else {
	    STR_REMOVE_BIT( ch->strbit_act, PLR_COLOUR );
	    send_to_char( "You take off your pink glasses.\n\r", ch );
	}
	return;

    case 12: // password
	{
	    char arg1[MAX_INPUT_LENGTH];
	    char arg2[MAX_INPUT_LENGTH];
	    char *pwdnew;
	    char *p;

#ifdef HAS_ALTS
	    if (STR_IS_SET(ch->strbit_act,PLR_IS_ALT)) {
		send_to_char("You are an alt, you should change the password of your UberAlt.\n\r",ch);
		return;
	    }
#endif

	    argument=one_argument(argument,arg1);
	    argument=one_argument(argument,arg2);

	    if ( arg1[0] == '\0' || arg2[0] == '\0' ) {
		send_to_char(
		    "Syntax: {Wconfig password <old> <new>{x.\n\r", ch );
		return;
	    }

	    if ( strcmp( crypt(arg1,ch->pcdata->pwd ), ch->pcdata->pwd ) ) {
		WAIT_STATE( ch, 40 );
		send_to_char( "Wrong password.  Wait 10 seconds.\n\r", ch );
		return;
	    }

	    if ( strlen(arg2) < 5 ) {
		send_to_char("New password must be at least "
		    "five characters long.\n\r", ch );
		return;
	    }

	    /*
	     * No tilde allowed because of player file format.
	     */
	    pwdnew = crypt( arg2, ch->name );
	    for ( p = pwdnew; *p != '\0'; p++ ) {
		if ( *p == '~' ) {
		    send_to_char(
			"New password not acceptable, try again.\n\r", ch );
		    return;
		}
	    }

	    logf("[%d] Password changed for %s.",
		GET_DESCRIPTOR(ch),ch->name);

	    free_string( ch->pcdata->pwd );
	    ch->pcdata->pwd = str_dup( pwdnew );
	    save_char_obj( ch );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}

    case 13: // pueblo picture
	if (argument[0]) {
	    char s[MSL],*p;
	    int i;

	    free_string(ch->pcdata->pueblo_picture);
	    if (!str_cmp(argument,"delete")) {
		ch->pcdata->pueblo_picture=str_dup("");
		send_to_char("Pueblo picture removed from configuration.\n\r",ch);
		return;
	    }

	    p=argument;
	    i=0;
	    while (*p) {
		switch (*p) {
		    case ' ': s[i++]='%';s[i++]='2';s[i++]='0';break;
		    case '~': s[i++]='%';s[i++]='7';s[i++]='e';break;
		    default : s[i++]=*p;
		}
		p++;
	    }
	    s[i]=0;
	    ch->pcdata->pueblo_picture=str_dup(s);
	} else {
	    if (ch->pcdata->pueblo_picture[0])
		sprintf_to_char(ch,"Your picture is stored on '{W%s{x'\n\r",
		    ch->pcdata->pueblo_picture);
	    else
		send_to_char("You don't have a picture defined yet.\n\r",ch);
	    send_to_char("Use '{Wconfig picture <URL>{x' to define it.\n\r",ch);
	}
	return;

    case 14: // pueblo
	if (STR_IS_SET(ch->strbit_act,PLR_PUEBLO)) {
	    STR_REMOVE_BIT( ch->strbit_act, PLR_PUEBLO);
	    if (ch->desc)
		ch->desc->pueblo=FALSE;
	    else
		send_to_char("Warning: you don't have a descriptor!\n\r",ch);
	    send_to_char( "Pueblo support disabled.\n\r", ch );
	} else {
	    STR_SET_BIT( ch->strbit_act, PLR_PUEBLO);
	    if (ch->desc)
		ch->desc->pueblo=TRUE;
	    else
		send_to_char("Warning: you don't have a descriptor!\n\r",ch);
	    send_to_char( "Pueblo support enabled.\n\r", ch );
	}
	return;

    case 15:  // icq
	if (argument[0]==0) {
	    if (GetCharProperty(ch,PROPERTY_STRING,"icq",buf)) {
		sprintf_to_char(ch,
		    "Your current ICQ address is '{W%s{x'.\n\r",buf);
		AboutPublish(ch);
	    } else
		send_to_char(
		    "You don't have an ICQ address specified yet.\n\r",ch);
	    return;
	}
	if (!str_cmp(argument,"delete")) {
	    send_to_char("ICQ address removed from your profile.\n\r",ch);
	    DeleteCharProperty(ch,PROPERTY_STRING,"icq");
	    return;
	}
	smash_tilde(argument);
	SetCharProperty(ch,PROPERTY_STRING,"icq",argument);
	send_to_char( "Ok.\n\r", ch );
	AboutPublish(ch);
	return;

    case 16:  // yahoo
	if (argument[0]==0) {
	    if (GetCharProperty(ch,PROPERTY_STRING,"yahoo",buf)) {
		sprintf_to_char(ch,
		    "Your current Yahoo address is '{W%s{x'.\n\r",buf);
		AboutPublish(ch);
	    } else
		send_to_char("You don't have a "
		    "Yahoo address specified yet.\n\r",ch);
	    return;
	}
	if (!str_cmp(argument,"delete")) {
	    send_to_char("Yahoo address removed from your profile.\n\r",ch);
	    DeleteCharProperty(ch,PROPERTY_STRING,"yahoo");
	    return;
	}
	smash_tilde(argument);
	SetCharProperty(ch,PROPERTY_STRING,"yahoo",argument);
	AboutPublish(ch);
	send_to_char( "Ok.\n\r", ch );
	return;

    case 17: // homepage
	if (argument[0]) {
	    char s[MSL],*p;
	    int i;

	    if (!str_cmp(argument,"delete")) {
		send_to_char("Homepage deleted from your profile.\n\r",ch);
		DeleteCharProperty(ch,PROPERTY_STRING,"homepage");
		return;
	    }

	    p=argument;
	    i=0;
	    while (*p) {
		switch (*p) {
		    case ' ': s[i++]='%';s[i++]='2';s[i++]='0';break;
		    case '~': s[i++]='%';s[i++]='7';s[i++]='e';break;
		    default : s[i++]=*p;
		}
		p++;
	    }
	    s[i]=0;
	    SetCharProperty(ch,PROPERTY_STRING,"homepage",s);
	    send_to_char("Ok.\n\r",ch);
	    AboutPublish(ch);
	} else {
	    char s[MSL];
	    if (GetCharProperty(ch,PROPERTY_STRING,"homepage",s))
		sprintf_to_char(ch,"Your homepage is '{W%s{x'\n\r",s);
	    else
		send_to_char("You don't have a homepage defined yet.\n\r",ch);
	    send_to_char("Use '{Wconfig homepage <URL>{x' to define it.\n\r",ch);
	    AboutPublish(ch);
	}
	return;

    case 18: // publish
	{
	    bool b;

	    switch(which_keyword(argument,"yes","no",NULL)) {
		case 1: b=TRUE;break;
		case 2: b=FALSE;break;
		case 0:
		case -1:
		    b=TRUE;
		    GetCharProperty(ch,PROPERTY_BOOL,"publish",&b);
		    send_to_char(
			"Usage: config publish [yes|no].\n\r",ch);
		    sprintf_to_char(ch,"Publishing of private information "
			"(homepage, email, ICQ etc) is currently %s.\n\r",
			b?"enabled":"disabled");
		    return;
	    }
	    SetCharProperty(ch,PROPERTY_BOOL,"publish",&b);
	    sprintf_to_char(ch,"Publishing of private information "
		"(homepage, email, ICQ etc) is currently %s.\n\r",
		b?"enabled":"disabled");
	    return;
	}

    case 19: // autoloot
	do_function(ch,&do_config,"auto loot");
	break;
    case 20: // autoassist
	do_function(ch,&do_config,"auto assist");
	break;
    case 21: // autogold
	do_function(ch,&do_config,"auto gold");
	break;
    case 22: // autoexit
	do_function(ch,&do_config,"auto exit");
	break;
    case 23: // autosac
	do_function(ch,&do_config,"auto sac");
	break;
    case 24: // autosplit
	do_function(ch,&do_config,"auto split");
	break;
    case 25: // autotitle
	do_function(ch,&do_config,"auto title");
	break;
    case 26: // autocompact
	do_function(ch,&do_config,"auto compact");
	break;
    case 27: // autocombine
	do_function(ch,&do_config,"auto combine");
	break;
    case 28: // show
	config_show(ch,argument);
	return;

    case 29: // mxp
	if (STR_IS_SET(ch->strbit_act,PLR_MXP)) {
	    STR_REMOVE_BIT( ch->strbit_act, PLR_MXP);
	    if (ch->desc)
		ch->desc->mxp=FALSE;
	    else
		send_to_char("Warning: you don't have a descriptor!\n\r",ch);
	    send_to_char( "MXP support disabled.\n\r", ch );
	} else {
	    STR_SET_BIT( ch->strbit_act, PLR_MXP);
	    if (ch->desc)
		ch->desc->mxp=TRUE;
	    else
		send_to_char("Warning: you don't have a descriptor!\n\r",ch);
	    send_to_char( "MXP support enabled.\n\r", ch );
	    check_mxp(ch);
	}
	return;

    case 30: // page
	if( !STR_IS_SET(ch->strbit_comm, COMM_NOPAGE)) {
	    STR_SET_BIT( ch->strbit_comm, COMM_NOPAGE );
	    send_to_char( "Pages are not coming through anymore.\n\r", ch );
	} else {
	    STR_REMOVE_BIT( ch->strbit_comm, COMM_NOPAGE );
	    send_to_char( "Pages are coming through again.\n\r", ch );
	}
	return;

    case 31: // pkilling
	if (ch->fighting) {
	    send_to_char("You cannot change your pkilling settings while fighting.",ch);
	    return;
	}
	if (STR_IS_SET(ch->strbit_act,PLR_PKILLING)) {
	    STR_REMOVE_BIT( ch->strbit_act, PLR_PKILLING);
	    send_to_char(
		"You no longer involve yourself with pkilling.\n\r",ch);
	    if (STR_IS_SET(ch->strbit_act, PLR_KILLER) ||
		STR_IS_SET(ch->strbit_act, PLR_THIEF))
		send_to_char(
		    "You have the killer and/or thief flag, "
		    "so you can still be killed!\n\r", ch);
	} else {
	    STR_SET_BIT( ch->strbit_act, PLR_PKILLING);
	    send_to_char( "You can now involve yourself in pkilling.\n\r", ch );
	}
	return;

    case 32: // msn
	if (argument[0]==0) {
	    if (GetCharProperty(ch,PROPERTY_STRING,"msn",buf)) {
		sprintf_to_char(ch,
		    "Your current MSN address is '{W%s{x'.\n\r",buf);
		AboutPublish(ch);
	    } else
		send_to_char(
		    "You don't have an MSN address specified yet.\n\r",ch);
	    return;
	}
	if (!str_cmp(argument,"delete")) {
	    send_to_char("MSN address removed from your profile.\n\r",ch);
	    DeleteCharProperty(ch,PROPERTY_STRING,"msn");
	    return;
	}
	smash_tilde(argument);
	SetCharProperty(ch,PROPERTY_STRING,"msn",argument);
	send_to_char( "Ok.\n\r", ch );
	AboutPublish(ch);
	return;

    case 33: // aim
	if (argument[0]==0) {
	    if (GetCharProperty(ch,PROPERTY_STRING,"aim",buf)) {
		sprintf_to_char(ch,
		    "Your current AIM address is '{W%s{x'.\n\r",buf);
		AboutPublish(ch);
	    } else
		send_to_char(
		    "You don't have an AIM address specified yet.\n\r",ch);
	    return;
	}
	if (!str_cmp(argument,"delete")) {
	    send_to_char("AIM address removed from your profile.\n\r",ch);
	    DeleteCharProperty(ch,PROPERTY_STRING,"aim");
	    return;
	}
	smash_tilde(argument);
	SetCharProperty(ch,PROPERTY_STRING,"aim",argument);
	send_to_char( "Ok.\n\r", ch );
	AboutPublish(ch);
	return;

    case 34: // compass
	if( !STR_IS_SET( ch->strbit_act, PLR_COMPASS ) ) {
	    STR_SET_BIT( ch->strbit_act, PLR_COMPASS );
	    send_to_char( "Compass enabled.\n\r", ch );
	} else {
	    STR_REMOVE_BIT( ch->strbit_act, PLR_COMPASS );
	    send_to_char( "Compass disabled.\n\r", ch );
	}
	return;

#ifdef HAS_ALTS
    case 35: // alts
    {
	bool found;

	one_argument(argument,arg);

	if (arg[0]==0) {
	    if (ch->pcdata->uberalt[0])
		sprintf_to_char(ch,
		    "Your UberAlt is '%s'\n\r",ch->pcdata->uberalt);
	    else
		send_to_char("You don't have an UberAlt.\n\r",ch);
	    if (ch->pcdata->alts[0])
		sprintf_to_char(ch,
		    "Your alts are '%s'\n\r",ch->pcdata->alts);
	    else
		send_to_char("You don't have any alts.\n\r",ch);
	    return;
	}

	argument=one_argument(argument,arg);
	switch (which_keyword(arg,"add","remove",NULL)) {
	default:
	    send_to_char("Usage: config alt add <alt> [alts...]\n\r",ch);
	    send_to_char("Usage: config alt remove <alt> [alts...]\n\r",ch);
	    return;
	case 1:
	    found=FALSE;
	    argument=one_argument(argument,arg);
	    while (arg[0]) {
		if (is_exact_name(arg,ch->pcdata->alts)) {
		    sprintf_to_char(ch,
			"'%s' is already defined as an alt.\n\r",arg);
		} else if (!player_exist(arg)) {
		    sprintf_to_char(ch,"'%s' does not exist.\n\r",arg);
		} else if (!str_cmp(arg,ch->name)) {
		    send_to_char("You stay in control of yourself.\n\r",ch);
		} else {
		    strcpy(buf,ch->pcdata->alts);
		    if (buf[0]!=0)
			strcat(buf," ");
		    strcat(buf,arg);
		    free_string(ch->pcdata->alts);
		    ch->pcdata->alts=str_dup(buf);
		    sprintf_to_char(ch,"Added '%s' as an alt.\n\r",arg);
		    found=TRUE;
		}
		argument=one_argument(argument,arg);
	    }
	    if (found)
		sprintf_to_char(ch,"You have choosen your alts.\n\r"
		    "Please configure them to accept you as your UberAlt: "
		    "'{Wconfig uberalt %s{x'\n\r",ch->name);
	    break;
	case 2:
	    found=FALSE;
	    argument=one_argument(argument,arg);
	    while (arg[0]) {
		if (!is_exact_name(arg,ch->pcdata->alts)) {
		    sprintf_to_char(ch,
			"'%s' is not defined as an alt.\n\r",arg);
		} else {
		    // do some magic with strings :-)
		    char *p;
		    strcpy(buf,ch->pcdata->alts);
		    strcat(arg," ");
		    if ((p=strstr(buf,arg))==NULL) {
			p=buf;
			if (strlen(buf)>strlen(arg))
			    p=buf+strlen(buf)-strlen(arg);
			p[0]=0;
		    } else {
			strcpy(p,p+strlen(arg));
		    }
		    free_string(ch->pcdata->alts);
		    ch->pcdata->alts=str_dup(buf);
		    sprintf_to_char(ch,"Removed '%s' as an alt.\n\r",arg);
		    found=TRUE;
		}
		argument=one_argument(argument,arg);
	    }
	    if (found)
		send_to_char("You have removed some of your alts.\n\r"
		    "Please configure them to not longer accept you as "
		    "their UberAlt: '{Wconfig uberalt remove{x'\n\r",ch);
	    break;
	}

	return;
    }

    case 36: // uberalt
	one_argument(argument,arg);

	if (arg[0]==0) {
	    if (ch->pcdata->uberalt[0])
		sprintf_to_char(ch,
		    "Your UberAlt is '%s'\n\r",ch->pcdata->uberalt);
	    else
		send_to_char("You don't have an UberAlt.\n\r",ch);
	    if (ch->pcdata->alts[0])
		sprintf_to_char(ch,
		    "Your alts are '%s'\n\r",ch->pcdata->alts);
	    else
		send_to_char("You don't have any alts.\n\r",ch);
	    return;
	}

	if (!str_cmp(arg,"remove")) {
	    send_to_char("Removed your UberAlt.\n\r",ch);
	    sprintf_to_char(ch,"Please configure '%s' to remove you as alt: '{Wconfig alt remove %s{x'\n\r",arg,ch->name);
	    free_string(ch->pcdata->uberalt);
	    ch->pcdata->uberalt=str_dup("");
	    return;
	}

	if (!player_exist(arg)) {
	    sprintf_to_char(ch,
		"A player with the name '%s' was not found.\n\r",arg);
	    return;
	}

	if (!str_cmp(arg,ch->name)) {
	    send_to_char("You stay in control of yourself.\n\r",ch);
	    return;
	}

	free_string(ch->pcdata->uberalt);
	ch->pcdata->uberalt=str_dup(arg);

	sprintf_to_char(ch,"You have configured '%s' as your UberAlt.\n\r"
	    "Please configure it to accept you as alt: '{Wconfig alt add %s{x'\n\r",arg,ch->name);
	return;
#endif

    case 37:  // xmpp
	if (argument[0]==0) {
	    if (GetCharProperty(ch,PROPERTY_STRING,"xmpp",buf)) {
		sprintf_to_char(ch,
		    "Your current XMPP/Jabber address is '{W%s{x'.\n\r",buf);
		AboutPublish(ch);
	    } else
		send_to_char("You don't have a "
		    "XMPP/Jabber address specified yet.\n\r",ch);
	    return;
	}
	if (!str_cmp(argument,"delete")) {
	    send_to_char("XMPP/Jabber address removed from your profile.\n\r",ch);
	    DeleteCharProperty(ch,PROPERTY_STRING,"xmpp");
	    return;
	}
	smash_tilde(argument);
	SetCharProperty(ch,PROPERTY_STRING,"xmpp",argument);
	AboutPublish(ch);
	send_to_char( "Ok.\n\r", ch );
	return;
    }
}


void config_show(CHAR_DATA *ch, char *argument) {
    char s[MAX_STRING_LENGTH];

    if (IS_NPC(ch))
	return;

    if (argument[0]) {
	argument=one_argument(argument,s);
	while (s[0]) {
	    switch (which_keyword(s,"armor","equipment","weight",
		"pain","extra","hunger",NULL)) {
		case -1:
		    sprintf_to_char(ch,"Option '%s' not understood.\n\r",s);
		    break;
		case 0:
		    config_show(ch,"");
		    break;
		case 1:	// armor
		    STR_TOGGLE_BIT(ch->strbit_comm,COMM_SHOW_ARMOR);
		    sprintf_to_char(ch,
			"AC values %s displayed in the score.\n\r",
			STR_IS_SET(ch->strbit_comm,COMM_SHOW_ARMOR)?"are now":"will not");
		    break;

		case 2:	// equipment
		    STR_TOGGLE_BIT(ch->strbit_comm,COMM_SHOW_EQUIPMENT);
		    sprintf_to_char(ch,
			"Empty slots %s displayed in the equipment overview.\n\r",
			STR_IS_SET(ch->strbit_comm,COMM_SHOW_EQUIPMENT)?"are now":"will not be");
		    break;

		case 3:	// weight
		    STR_TOGGLE_BIT(ch->strbit_comm,COMM_SHOW_WEIGHT);
		    sprintf_to_char(ch,
			"Weight %s displayed in the inventory overview.\n\r",
			STR_IS_SET(ch->strbit_comm,COMM_SHOW_WEIGHT)?"is now":"will not be");
		    break;

		case 4:	// pain
		    STR_TOGGLE_BIT(ch->strbit_comm,COMM_SHOW_PAIN);
		    sprintf_to_char(ch,
			"Damage will be displayed as %s.\n\r",
			STR_IS_SET(ch->strbit_comm,COMM_SHOW_PAIN)?"pain":"real damage.");
		    break;

	        case 5: // extra damage message
		    STR_TOGGLE_BIT(ch->strbit_comm,COMM_SHOW_EXTRADAM);
		    sprintf_to_char(ch,
			"Extra damage messages will%sbe displayed.\n\r",
			STR_IS_SET(ch->strbit_comm,COMM_SHOW_EXTRADAM)?" ":" not ");
		    break;

	        case 6: // hunger messages, heroes only
		    if (ch->level==LEVEL_HERO) {
			STR_TOGGLE_BIT(ch->strbit_comm,COMM_SHOW_NOHUNGER);
			sprintf_to_char(ch,
			    "Hunger/thirst messages will%sbe displayed.\n\r",
			    STR_IS_SET(ch->strbit_comm,COMM_SHOW_NOHUNGER)?" not ":" ");
		    } else
			send_to_char("Heroes only, sorry!\n\r",ch);
		    break;
	    }
	    argument=one_argument(argument,s);
	}
	return;
    }


    send_to_char("option           status action\n\r",ch);
    send_to_char("-------------------------------------------------------------------------------\n\r",ch);
    sprintf_to_char(ch,"show armor       %3s    %s\n\r",
	STR_IS_SET(ch->strbit_comm,COMM_SHOW_ARMOR) ? "ON" : "OFF",
	STR_IS_SET(ch->strbit_comm,COMM_SHOW_ARMOR) ?
		"AC values will be displayed in the score":
		"AC values will not be displayed in the score");
    sprintf_to_char(ch,"show equipment   %3s    %s\n\r",
	STR_IS_SET(ch->strbit_comm,COMM_SHOW_EQUIPMENT) ? "ON" : "OFF",
	STR_IS_SET(ch->strbit_comm,COMM_SHOW_EQUIPMENT) ?
		"Empty slots will be displayed in the score.":
		"Empty slots will not be displayed in the score.");
    sprintf_to_char(ch,"show weight      %3s    %s\n\r",
	STR_IS_SET(ch->strbit_comm,COMM_SHOW_WEIGHT) ? "ON" : "OFF",
	STR_IS_SET(ch->strbit_comm,COMM_SHOW_WEIGHT) ?
		"Weight will be shown in the inventory.":
		"Weight will not be shown in the inventory.");
    sprintf_to_char(ch,"show pain        %3s    %s\n\r",
	STR_IS_SET(ch->strbit_comm,COMM_SHOW_PAIN) ? "ON" : "OFF",
	STR_IS_SET(ch->strbit_comm,COMM_SHOW_PAIN) ?
		"Damage will be displayed as pain.":
		"Damage will be displayed as real damage.");
    sprintf_to_char(ch,"show extra       %3s    %s\n\r",
	STR_IS_SET(ch->strbit_comm,COMM_SHOW_EXTRADAM) ? "ON" : "OFF",
	STR_IS_SET(ch->strbit_comm,COMM_SHOW_EXTRADAM) ?
		"Extra damage messages will be displayed.":
		"Extra damage messages will not be displayed.");
    if (ch->level==LEVEL_HERO)
	sprintf_to_char(ch,"show hunger      %3s    %s\n\r",
	    STR_IS_SET(ch->strbit_comm,COMM_SHOW_NOHUNGER) ? "OFF" : "ON",
	    STR_IS_SET(ch->strbit_comm,COMM_SHOW_NOHUNGER) ?
		    "Hunger/thirst messages will not be displayed.":
		    "Hunger/thirst messages will be displayed.");

}


void config_autolist(CHAR_DATA *ch, char *argument) {
    char s[MAX_STRING_LENGTH];

    if (IS_NPC(ch))
      return;

    if (argument[0]) {
	argument=one_argument(argument,s);
	while (s[0]) {
	    switch (which_keyword(s,"all","loot","gold","sac","exit","split",
		"assist","title","combine","compact",NULL)) {
		case -1:
		    sprintf_to_char(ch,"Autocommand '%s' not understood.\n\r",s);
		    break;
		case 0:
		    config_autolist(ch,"");
		    return;
		case 1: // all
		    config_autolist(ch,"loot");
		    config_autolist(ch,"gold");
		    config_autolist(ch,"sac");
		    config_autolist(ch,"exit");
		    config_autolist(ch,"split");
		    config_autolist(ch,"assist");
		    config_autolist(ch,"title");
		    config_autolist(ch,"combine");
		    config_autolist(ch,"compact");
		    break;
		case 2:	// loot
		    STR_TOGGLE_BIT(ch->strbit_act,PLR_AUTOLOOT);
		    sprintf_to_char(ch,
			"Automatic corpse looting is turned %s.\n\r",
			STR_IS_SET(ch->strbit_act,PLR_AUTOLOOT)?"on":"off");
		    break;
		case 3: // gold
		    STR_TOGGLE_BIT(ch->strbit_act,PLR_AUTOGOLD);
		    sprintf_to_char(ch,
			"Automatic gold looting is turned %s.\n\r",
			STR_IS_SET(ch->strbit_act,PLR_AUTOGOLD)?"on":"off");
		    break;
		case 4: // sac
		    STR_TOGGLE_BIT(ch->strbit_act,PLR_AUTOSAC);
		    sprintf_to_char(ch,
			"Automatic corpse sacrificing is turned %s.\n\r",
			STR_IS_SET(ch->strbit_act,PLR_AUTOSAC)?"on":"off");
		    break;
		case 5: // exit
		    STR_TOGGLE_BIT(ch->strbit_act,PLR_AUTOEXIT);
		    sprintf_to_char(ch,"Exits will %s be displayed.\n\r",
			STR_IS_SET(ch->strbit_act,PLR_AUTOEXIT)?"now":"not anymore");
		    break;
		case 6: // split
		    STR_TOGGLE_BIT(ch->strbit_act,PLR_AUTOSPLIT);
		    sprintf_to_char(ch,
			"Automatic gold splitting is turned %s.\n\r",
			STR_IS_SET(ch->strbit_act,PLR_AUTOSPLIT)?"on":"off");
		    break;
		case 7: // assist
		    STR_TOGGLE_BIT(ch->strbit_act,PLR_AUTOASSIST);
		    sprintf_to_char(ch,
			"You will %s assist when needed.\n\r",
			STR_IS_SET(ch->strbit_act,PLR_AUTOASSIST)?"now":"no longer");
		    break;
		case 8: // title
		    STR_TOGGLE_BIT(ch->strbit_act,PLR_FREEZETITLE);
		    sprintf_to_char(ch,
			"Title will %s change when you reach next level.\n\r",
			STR_IS_SET(ch->strbit_act,PLR_FREEZETITLE)?"not":"automaticly");
		    break;
		case 9: // combine
		    STR_TOGGLE_BIT(ch->strbit_comm,COMM_COMBINE);
		    sprintf_to_char(ch,
			"Inventory items will %sbe combined%s.\n\r",
			STR_IS_SET(ch->strbit_comm,COMM_COMBINE)?"":"not ",
			STR_IS_SET(ch->strbit_comm,COMM_COMBINE)?" when possible":"");
		    break;
		case 10: // compact
		    STR_TOGGLE_BIT(ch->strbit_comm,COMM_COMPACT);
		    sprintf_to_char(ch,
			"Compact mode %s.\n\r",
			STR_IS_SET(ch->strbit_comm,COMM_COMPACT)?"set":"removed");
		    break;
	    }
	    argument=one_argument(argument,s);
	}
	return;
    }

    send_to_char("command       status action\n\r",ch);
    send_to_char("-------------------------------------------------------------------------------\n\r",ch);

    sprintf_to_char(ch,"auto assist   %3s    %s\n\r",
	STR_IS_SET(ch->strbit_act,PLR_AUTOASSIST) ? "ON" : "OFF",
	STR_IS_SET(ch->strbit_act,PLR_AUTOASSIST) ?
		"automatic assisting when a group member starts fighting":
		"do not assist when a group member starts fighting");
    sprintf_to_char(ch,"auto exit     %3s    %s\n\r",
	STR_IS_SET(ch->strbit_act,PLR_AUTOEXIT) ? "ON" : "OFF",
	STR_IS_SET(ch->strbit_act,PLR_AUTOEXIT) ?
		"display the exits when you enter the room":
		"do not display the exits when you enter a room");
    sprintf_to_char(ch,"auto gold     %3s    %s\n\r",
	STR_IS_SET(ch->strbit_act,PLR_AUTOGOLD) ? "ON" : "OFF",
	STR_IS_SET(ch->strbit_act,PLR_AUTOGOLD) ?
		"automatic get the gold from the corpse":
		"do not get the gold from a corpse");
    sprintf_to_char(ch,"auto loot     %3s    %s\n\r",
	STR_IS_SET(ch->strbit_act,PLR_AUTOLOOT) ? "ON" : "OFF",
	STR_IS_SET(ch->strbit_act,PLR_AUTOLOOT) ?
		"automatic get the objects from the corpse":
		"do not get the objects from a corpse");
    sprintf_to_char(ch,"auto sac      %3s    %s\n\r",
	STR_IS_SET(ch->strbit_act,PLR_AUTOSAC) ? "ON" : "OFF",
	STR_IS_SET(ch->strbit_act,PLR_AUTOSAC) ?
		"automatic sacrifycing of corpses when looted":
		"do not sacrifice the corpse");
    sprintf_to_char(ch,"auto split    %3s    %s\n\r",
	STR_IS_SET(ch->strbit_act,PLR_AUTOSPLIT) ? "ON" : "OFF",
	STR_IS_SET(ch->strbit_act,PLR_AUTOSPLIT) ?
		"automatic split of gold with your group":
		"do not split gold with your group");
    sprintf_to_char(ch,"auto title    %3s    %s\n\r",
	STR_IS_SET(ch->strbit_act,PLR_FREEZETITLE) ? "OFF" : "ON",  // yes, this is inverted.
	STR_IS_SET(ch->strbit_act,PLR_FREEZETITLE) ?
		"do not change the title when you raise a level":
		"change the title when you raise a level");
    sprintf_to_char(ch,"auto compact  %3s    %s\n\r",
	STR_IS_SET(ch->strbit_comm,COMM_COMPACT) ? "ON" : "OFF",
	STR_IS_SET(ch->strbit_comm,COMM_COMPACT) ?
		"do not print the extra empty line between commands":
		"print the extra empty line between commands");
    sprintf_to_char(ch,"auto combine  %3s    %s\n\r",
	STR_IS_SET(ch->strbit_comm,COMM_COMBINE) ? "ON" : "OFF",
	STR_IS_SET(ch->strbit_comm,COMM_COMBINE) ?
		"combine items with the same name in inventory":
		"do not combine items with the same name in inventory");
    sprintf_to_char(ch,"noloot        %3s    %s\n\r",
	is_clan(ch)?"n/a":STR_IS_SET(ch->strbit_act,PLR_CANLOOT) ? "ON" : "OFF",
	is_clan(ch)?"You are in a clan. Other people may loot your corpse." :
		STR_IS_SET(ch->strbit_act,PLR_CANLOOT) ?
			"your corpse is not safe from thieves":
			"other people cannot loot your corpse");
    sprintf_to_char(ch,"nosummon      %3s    %s\n\r",
	STR_IS_SET(ch->strbit_act,PLR_NOSUMMON) ? "ON" : "OFF",
	STR_IS_SET(ch->strbit_act,PLR_NOSUMMON) ?
		"you cannot be summoned":
		"other people can summon you");
    sprintf_to_char(ch,"nofollow      %3s    %s\n\r",
	STR_IS_SET(ch->strbit_act,PLR_NOFOLLOW) ? "ON" : "OFF",
	STR_IS_SET(ch->strbit_act,PLR_NOFOLLOW) ?
		"you will not allow people to follow you":
		"you allow people to follow you");
}

/*******************************************************************************/

void obsoleted_by_config(CHAR_DATA *ch,char *example) {
    sprintf_to_char(ch,
	"The command you were using is obsoleted by the config command.\n\r"
	"Use '{Wconfig %s{x' now.\n\r",example);
}

#define OBSOLETE(func,text) \
    void (func)(CHAR_DATA *ch,char *argument) { obsoleted_by_config(ch,text); }

OBSOLETE(do_email,"email")
OBSOLETE(do_prompt,"prompt")
OBSOLETE(do_scroll,"scroll")
OBSOLETE(do_channels,"channels")
OBSOLETE(do_autolist,"auto")
OBSOLETE(do_noloot,"noloot")
OBSOLETE(do_nosummon,"nosummon")
OBSOLETE(do_combine,"combine")
OBSOLETE(do_brief,"brief")
OBSOLETE(do_colour,"colour")
OBSOLETE(do_color,"colour")
OBSOLETE(do_password,"password")
OBSOLETE(do_autoloot,"auto loot")
OBSOLETE(do_autoassist,"auto assist")
OBSOLETE(do_autogold,"auto gold")
OBSOLETE(do_autoexit,"auto exit")
OBSOLETE(do_autosac,"auto sac")
OBSOLETE(do_autosplit,"auto split")
OBSOLETE(do_autotitle,"auto title")
OBSOLETE(do_autocompact,"auto compact")
OBSOLETE(do_autocombine,"auto combine")
OBSOLETE(do_show,"show")
