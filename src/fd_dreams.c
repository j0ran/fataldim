//
// $Id: fd_dreams.c,v 1.16 2006/08/25 09:43:19 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

/*
 * Some very basic dreaming stuff: (MavEtJu)
 * - if the character is good and the dream is good, gain extra hp's
 * -                     .... and the dream is neutral, nothing changes
 * -                     .... and the dream is evil, lose hp's
 * -                                           .... and perhaps wake up scared
 *
 * - if the character is evil and the dream is evil, gain extra hp's
 * -                     .... and the dream is neutral, nothing changes
 * -                     .... and the dream is good, lose hp's
 * -                                           .... and perhaps wake up scared
 *
 * - if the character is neutral and the dream is good, nothing changes
 * -                        .... and the dream is neutral, gain extra hp's
 * -                        .... and the dream is evil, nothing changes
 *
 */

#include "merc.h"
#include "interp.h"

DREAMS dreams={TRUE,FALSE,{NULL,NULL,NULL,NULL,NULL},{-2,-2,-2,-2,-2}};

void InitDreams(CHAR_DATA *ch) {
    HELP_DATA *	pHelp;
    BUFFER *	output;
    bool	found,last;
    int		dreamtype;
    char *	ps;
    char *	pt;
    char *	help[5]={"'dreams very bad'","'dreams bad'","'dreams normal'",
		         "'dreams good'","'dreams very good'"};
    DREAM_TYPE *dream;

    dreams.init=TRUE;
    for (dreamtype=0;dreamtype<5;dreamtype++) {
	output=new_buf();

	found=FALSE;
	for ( pHelp=help_first; pHelp!=NULL; pHelp=pHelp->next ) {
	    if (!str_cmp(pHelp->keyword,help[dreamtype])) {
		found=TRUE;
		add_buf(output,pHelp->keyword);
		add_buf(output,"\n\r");

		if ( pHelp->text[0] == '.' )
		    add_buf(output,pHelp->text+1);
		else
		    add_buf(output,pHelp->text);
		/* small hack :) */
		if (ch->desc != NULL && ch->desc->connected != CON_PLAYING
				     && ch->desc->connected != CON_GEN_GROUPS)
		    break;
	    }
	}

	if (!found) {
	    bugf("InitDreams: couldn't find '%s'.",help[dreamtype]);
	    dreams.number[dreamtype]=-1;
	    free_buf(output);
	    continue;
	}

	ps=buf_string(output);
	pt=strchr(ps,'*');	/* here starts the first dream */
	if (pt==NULL) {
	    bugf("InitDreams: couldn't find start of first %s.",
		help[dreamtype]);
	    free_buf(output);
	    continue;
	}

	ps=pt+1;
	last=FALSE;
	dreams.number[dreamtype]=0;
	while (!last) {
	    pt=strchr(ps,'*');
	    if (pt)
		pt[0]=0;
	    else
		last=TRUE;
	    dream=new_dream();
	    dream->string=str_dup(ps);
	    if (!last) {
		pt[0]='*';
		ps=pt+1;
	    }

	    dream->next=dreams.dreams[dreamtype];
	    dreams.dreams[dreamtype]=dream;
	    dreams.number[dreamtype]++;
	}
	free_buf(output);
	logf("[0] DREAMS: Read %s: %d dreams.",
	    help[dreamtype],
	    dreams.number[dreamtype]);
    }
}

#define NUM_DREAMS 9
void update_dream(CHAR_DATA *ch) {
    DREAM_TYPE *s;
    int		kind,number;

    if (!dreams.init)
	InitDreams(ch);

    if (number_percent()>20)
	return;

    kind=number_range(0,NUM_DREAMS);

    if (IS_AFFECTED(ch,EFF_CALM)||IS_AFFECTED(ch,EFF_SLEEP))	kind+=2;

    if (IS_AFFECTED(ch,EFF_HASTE))				kind-=1;
    if (IS_AFFECTED(ch,EFF_CURSE))				kind-=2;

    if (IS_AFFECTED(ch,EFF_PROTECT_EVIL))			kind+=2;
    if (IS_AFFECTED(ch,EFF_PROTECT_GOOD))			kind-=2;

    if (IS_EVIL(ch))						kind-=2;
    if (IS_GOOD(ch))						kind+=2;

    kind=URANGE(0,kind,NUM_DREAMS);

    s=NULL;
    switch (kind) {
    case 0:	// very bad dream
	if (dreams.number[DREAMS_VERY_BAD]<0) return;
	number=number_range(0,dreams.number[DREAMS_VERY_BAD]);
	s=dreams.dreams[DREAMS_VERY_BAD];
	while (number--!=0)
	    s=s->next;
	if (s!=NULL) send_to_char(s->string,ch);

	if (ch->hit!=ch->max_hit)	ch->hit=UMAX(10,ch->hit-20);
	if (ch->move!=ch->max_move)	ch->move=UMAX(10,ch->move-20);
	if (ch->mana!=ch->max_mana)	ch->mana=UMAX(10,ch->mana-20);

	if (number_percent()>80) {
	    do_function(ch,&do_emote,"suddenly wakes up!");
	    do_function(ch,&do_wake,"");
	    send_to_char("\n\rIt seems only a bad dream..."
			 " (or isn't it?)\n\r",ch);
	} else
	    act("$n moves very wildly in $s sleep.",ch,NULL,NULL,TO_ROOM);

	break;

    case 1:	// bad dream
    case 2:
	if (dreams.number[DREAMS_BAD]<0) return;
	number=number_range(0,dreams.number[DREAMS_BAD]);
	s=dreams.dreams[DREAMS_BAD];
	while (number--!=0)
	    s=s->next;
	if (s!=NULL) send_to_char(s->string,ch);

	if (ch->hit!=ch->max_hit)	ch->hit=UMAX(10,ch->hit-10);
	if (ch->move!=ch->max_move)	ch->move=UMAX(10,ch->move-10);
	if (ch->mana!=ch->max_mana)	ch->mana=UMAX(10,ch->mana-10);

	act("$n moves uneasily in $s sleep.",ch,NULL,NULL,TO_ROOM);

	break;

    case 3:	// normal dream
    case 4:
    case 5:
    case 6:
	if (dreams.number[DREAMS_NORMAL]<0) return;
	number=number_range(0,dreams.number[DREAMS_NORMAL]);
	s=dreams.dreams[DREAMS_NORMAL];
	while (number--!=0)
	    s=s->next;
	if (s!=NULL) send_to_char(s->string,ch);

	do_function(ch,&do_emote,"snores.");

	break;

    case 7:	// good dream
    case 8:
	if (dreams.number[DREAMS_GOOD]<0) return;
	number=number_range(0,dreams.number[DREAMS_GOOD]);
	s=dreams.dreams[DREAMS_GOOD];
	while (number--!=0)
	    s=s->next;
	if (s!=NULL) send_to_char(s->string,ch);

	if (ch->hit<=ch->max_hit)   ch->hit=UMIN(ch->max_hit,ch->hit+10);
	if (ch->move<=ch->max_move) ch->move=UMIN(ch->max_move,ch->move+10);
	if (ch->mana<=ch->max_mana) ch->mana=UMIN(ch->max_mana,ch->mana+10);

	act("$n snores happily.",ch,NULL,NULL,TO_ROOM);

	break;

    case 9:	// very good dream
	if (dreams.number[DREAMS_VERY_GOOD]<0) return;
	number=number_range(0,dreams.number[DREAMS_VERY_GOOD]);
	s=dreams.dreams[DREAMS_VERY_GOOD];
	while (number--!=0)
	    s=s->next;
	if (s!=NULL) send_to_char(s->string,ch);

	if (ch->hit<=ch->max_hit)   ch->hit=UMIN(ch->max_hit,ch->hit+20);
	if (ch->move<=ch->max_move) ch->move=UMIN(ch->max_move,ch->move+20);
	if (ch->mana<=ch->max_mana) ch->mana=UMIN(ch->max_mana,ch->mana+20);

	act("$n smiles happily in $s sleep.",ch,NULL,NULL,TO_ROOM);

	break;

    default:	/* Just in case... */
	return;
    }
}
