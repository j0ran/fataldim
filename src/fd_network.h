//
// $Id: fd_network.h,v 1.9 2008/05/11 20:50:47 jodocus Exp $ */
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef INCLUDED_FD_NETWORK_H
#define INCLUDED_FD_NETWORK_H

extern const	char	echo_wont	[];
extern const	char	echo_will	[];

extern DESCRIPTOR_DATA *	descriptor_list;	// All open descriptors
extern FILE *			fpReserve;		// Reserved file handle

int	init_socket		( int port, int version );
void	init_descriptor		( int control, int destype, int version );
bool	read_from_descriptor	( DESCRIPTOR_DATA *d );

int	read_from_buffer	( DESCRIPTOR_DATA *d );
void	stop_idling		( CHAR_DATA *ch );
char *	dns_gethostname		( DESCRIPTOR_DATA *desc );
int	init_socket		( int port, int ipversion );
void	game_loop_unix		( void );
void	send_options		( DESCRIPTOR_DATA *d );
void	init_descriptor		( int control, int destype, int IPversion );
void	close_socket		( DESCRIPTOR_DATA *dclose );
bool	read_from_descriptor	( DESCRIPTOR_DATA *d );
bool	process_output		( DESCRIPTOR_DATA *d, bool fPrompt );
void	write_to_buffer		( DESCRIPTOR_DATA *d, const char *txt, int length );
bool	write_to_real_descriptor( int desc, char *txt, int length );
bool	write_to_descriptor	( DESCRIPTOR_DATA *d, char *txt, int length );
void	check_mxp		( CHAR_DATA *ch );
int	get_page_length		( DESCRIPTOR_DATA *d, CHAR_DATA *ch);
void	show_string		( DESCRIPTOR_DATA *d, char *input );
char *	get_multi_command	( DESCRIPTOR_DATA *d, char *argument );

#endif	// INCLUDED_FD_NETWORK_H
