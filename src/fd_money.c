//
// $Id: fd_money.c,v 1.4 2002/09/21 20:35:09 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"
#include "color.h"
#include "interp.h"

//
// Bank and money routines
//

//
//
//
char *money_string_colour(int gold,int silver,char *colour) {
    static char buf[MSL];

    if (gold==0 && silver==0) {
        strcpy(buf,"no coins at all");
        return buf;
    }

    if (gold==0) {
        sprintf(buf,"%d {wsilver%s coin%s",
                silver,
                colour,
                silver==1?"":"s");
        return buf;
    }

    if (silver==0) {
        sprintf(buf,"%d {ygold%s coin%s",
                gold,
                colour,
                gold==1?"":"s");
        return buf;
    }

    sprintf(buf,
            "%d {ygold%s and %d {wsilver%s coin%s",
            gold,
            colour,
            silver,
            colour,
            (gold==1&&silver==1)?"":"s");
    return buf;
}

char *money_string(int gold,int silver) {
    return money_string_colour(gold,silver,"{x");
}

//
//
//
bool can_afford(CHAR_DATA *ch,CHAR_DATA *keeper,int price) {
    OBJ_DATA *	creditcard;
    bool	has_creditcard;

    creditcard=get_eq_char(ch,WEAR_HOLD);
    has_creditcard=FALSE;
    if (creditcard!=NULL)
        GetObjectProperty(creditcard,PROPERTY_BOOL,
                          "creditcard",&has_creditcard);

    if (has_creditcard) {
        if (IS_NPC(ch)) {
            send_to_char("You're not allowed to carry creditcards!\n\r",ch);
            return FALSE;
        }

        if (ch->pcdata->bank < price) {
            act("$n tells you '"C_TELL"I've been informed that the "
                                      "account on this card is not sufficient.{x'",
                keeper,NULL,ch,TO_VICT);
            return FALSE;
        }
        return TRUE;
    }

    // paying cash
    if (ch->silver+100*ch->gold < price)
        return FALSE;

    return TRUE;
}

//
//
//
CHAR_DATA *find_banker(CHAR_DATA *ch) {
    char	buf[MAX_STRING_LENGTH];
    CHAR_DATA *	banker;
    SPEC_FUN *	spec_banker;

    if ((spec_banker=spec_lookup("spec_banker"))==NULL) {
        bugf("find_banker(): couldn't find `spec_banker'. No banking possible");
        return NULL;
    }

    for (banker=ch->in_room->people;banker!=NULL;banker=banker->next_in_room)
        if (IS_NPC(banker) && (banker->spec_fun==spec_banker))
            break;

    if (banker==NULL) {
        send_to_char( "You can't do that here.\n\r", ch );
        return NULL;
    }

    if (IS_NPC(ch)) {
        do_function(banker,&do_say, "I only serve players." );
        return NULL;
    }

    //
    // Nice for bankers.
    //
    if (STR_IS_SET(ch->strbit_act, PLR_KILLER)) {
        do_function(banker,&do_say,"Killers are not welcome!");
        sprintf(buf,"%s the KILLER is over here!\n\r",ch->name);
        do_function(banker,&do_yell, buf );
        return NULL;
    }

    if (STR_IS_SET(ch->strbit_act, PLR_THIEF)) {
        do_function(banker,&do_say, "Thieves are not welcome!");
        sprintf(buf,"%s the THIEF is over here!\n\r", ch->name);
        do_function(banker,&do_yell,buf);
        return NULL;
    }

    //
    // Invisible or hidden people.
    //
    if (!can_see(banker,ch)) {
        do_function(banker,&do_say,"I don't serve folks I can't see.");
        return NULL;
    }

    return banker;
}

//
//
//
void do_balance( CHAR_DATA *ch, char *argument ) {
    char	buf[MSL];
    CHAR_DATA *	banker;
    long	silver,gold;

    if ((banker=find_banker(ch))==NULL) return;
    if (IS_NPC(ch)) return;	// double, it's checked in find_banker() also

    gold=ch->pcdata->bank/100;
    silver=ch->pcdata->bank%100;

    sprintf(buf,"You have %s in the bank.",
            money_string_colour(gold,silver,"{r"));
    act("$n tells you '"C_TELL"$t{x'",banker,buf,ch,TO_VICT);

    if (ch->clan && ch->clan->clan_info->independent==FALSE) { // no loner
        gold=ch->clan->clan_info->coins/100;
        silver=ch->clan->clan_info->coins%100;

        sprintf(buf,
                "The %s clan has %s in the bank.",
                ch->clan->clan_info->name,
                money_string_colour(gold,silver,"{r")
                );
        act("$n tells you '"C_TELL"$t{x'",banker,buf,ch,TO_VICT);
    }

    ch->reply = banker;
}

//
//
//
void do_deposit( CHAR_DATA *ch, char *argument ) {
    CHAR_DATA *	banker;
    long	valuegold,valuesilver,wealth;
    bool	all=FALSE;
    bool	puresilver=FALSE,puregold=FALSE;
    bool	clan=FALSE;
    char	arg1[MIL],arg2[MIL],arg3[MIL];

    if ((banker=find_banker(ch))==NULL) return;
    if (check_ordered(ch)) return;
    if (IS_NPC(ch)) return;	// double, it's checked in find_banker() also

    argument=one_argument(argument,arg1);
    argument=one_argument(argument,arg2);
    argument=one_argument(argument,arg3);

    if (arg1[0]==0) {
        do_function(banker,&do_say,"And how much would you like to deposit?");
        return;
    }

    if (!str_cmp(arg1,"all"))
        all=TRUE;
    if (!str_cmp(arg2,"gold"))
        puregold=TRUE;
    if (!str_cmp(arg2,"silver"))
        puresilver=TRUE;
    if (!str_cmp(arg2,"clan") || !str_cmp(arg3,"clan"))
        clan=TRUE;

    if (clan)
        if (ch->clan==NULL || ch->clan->clan_info->independent==TRUE) {
            do_function(banker,&do_say,"You are not a member of a clan.");
            return;
        }

    valuesilver=0;
    valuegold=0;
    if (all) {
        if (puregold)
            valuegold=ch->gold;
        if (puresilver)
            valuesilver=ch->silver;
        if (!puregold && !puresilver) {
            valuesilver=ch->silver;
            valuegold=ch->gold;
        }

        if (valuegold==0 && valuesilver==0) {
            interpret(banker,"sigh");
            do_function(banker,&do_say,
                        "And people wonder why we want to charge each transaction!");
            return;
        }
    }

    if (!all) {
        int value=atoi(arg1);

        if (value==0) {
            interpret(banker,"sigh");
            do_function(banker,&do_say,
                        "And people wonder why we want to charge each transaction!");
            return;
        }

        if (value<0) {
            // trick with leaving a certain amount of money
            if (puregold) {
                if (ch->gold<=-value) {
                    do_function(banker,&do_say,"You'd better recount that!");
                    return;
                }
                value=ch->gold+value;
            }
            if (puresilver) {
                if (ch->silver<=-value) {
                    do_function(banker,&do_say,"You'd better recount that!");
                    return;
                }
                value=ch->silver+value;
            }
            if (!puresilver && !puregold) {
                wealth=100*ch->gold+ch->silver;
                if (wealth<=-value) {
                    do_function(banker,&do_say,"You'd better recount that!");
                    return;
                }
                if (value%100>ch->silver) {
                    ch->silver+=100;
                    ch->gold--;
                }
                value=wealth+value;
            }
        }

        if (value>0) {
            if (puregold) {
                if (ch->gold<value) {
                    do_function(banker,&do_say,"You'd better recount that!");
                    return;
                }
                valuegold=value;
            }
            if (puresilver) {
                if (ch->silver<value) {
                    do_function(banker,&do_say,"You'd better recount that!");
                    return;
                }
                valuesilver=value;
            }
            if (!puregold && !puresilver) {
                if (value>100*ch->gold+ch->silver) {
                    do_function(banker,&do_say,"You'd better recount that!");
                    return;
                }
                if (value%100>ch->silver) {
                    ch->silver+=100;
                    ch->gold--;
                }
                valuegold=value/100;
                valuesilver=value%100;
                while (valuegold>ch->gold) {
                    valuegold--;
                    valuesilver+=100;
                }
            }
        }
    }

    deduct_cost(ch,valuegold,valuesilver,FALSE);

    if (clan) {
        sprintf_to_char(ch,
                        "You have deposited %s into your clan account.\n\r",
                        money_string(valuegold,valuesilver));
        act("$n deposits some money.", ch, NULL, NULL, TO_ROOM );
        save_clans();
        ch->clan->clan_info->coins+=100*valuegold+valuesilver;

        do_function(ch,&do_balance,"");
        return;
    }

    sprintf_to_char(ch,
                    "You have deposited %s into your account.\n\r",
                    money_string(valuegold,valuesilver));
    act("$n deposits some money.", ch, NULL, NULL, TO_ROOM );
    ch->pcdata->bank+=100*valuegold+valuesilver;

    do_function(ch,&do_balance,"");
}

//
//
//
void do_withdraw(CHAR_DATA *ch,char *argument) {
    CHAR_DATA *	banker;
    long	valuesilver,valuegold;
    char	arg1[MIL],arg2[MIL],arg3[MIL];
    bool	all=FALSE;
    bool	puresilver=FALSE,puregold=FALSE;
    bool	clan=FALSE;

    if ((banker=find_banker(ch))==NULL) return;
    if (check_ordered(ch)) return;
    if (IS_NPC(ch)) return;	// double, it's checked in find_banker() also

    argument=one_argument(argument,arg1);
    argument=one_argument(argument,arg2);
    argument=one_argument(argument,arg3);

    if (arg1[0]==0) {
        do_function(banker,&do_say,"And how much would you like to withdraw?");
        return;
    }

    if (!str_cmp(arg1,"all"))
        all=TRUE;
    if (!str_cmp(arg2,"silver"))
        puresilver=TRUE;
    if (!str_cmp(arg2,"gold"))
        puregold=TRUE;
    if (!str_cmp(arg2,"clan") || !str_cmp(arg3,"clan"))
        clan=TRUE;

    if (clan) {
        if (ch->clan==NULL || ch->clan->clan_info->independent) {
            do_function(banker,&do_say,"And which clan do you belong to?");
            return;
        }
        if (ch->clan->rank_info->rank<CLAN_LEADER) {
            do_function(banker,&do_say,
                        "Please come back when you're a leader.");
            return;
        }
    }

    valuesilver=0;
    valuegold=0;

    if (all) {
        int value=ch->pcdata->bank;
        if (clan)
            value=ch->clan->clan_info->coins;

        if (puresilver)
            valuesilver=value;
        if (puregold)
            valuegold=value/100;
        if (!puresilver && !puregold) {
            valuegold=value/100;
            valuesilver=value%100;
        }

        if (valuegold==0 && valuesilver==0) {
            interpret(banker,"sigh");
            do_function(banker,&do_say,
                        "And people wonder why we want to charge each transaction!");
            return;
        }
    }
    if (!all) {
        int value=atoi(arg1);

        if (value<0) {
            do_function(banker,&do_say,"Pardon? Maybe you mean to deposit?");
            return;
        }

        if (value==0) {
            interpret(banker,"sigh");
            do_function(banker,&do_say,
                        "And people wonder why we want to charge each transaction!");
            return;
        }

        if (puresilver)
            valuesilver=value;
        if (puregold)
            valuegold=value;
        if (!puresilver && !puregold) {
            valuegold=value/100;
            valuesilver=value%100;
        }
    }

    if (clan) {
        if (100*valuegold+valuesilver>ch->clan->clan_info->coins) {
            do_function(banker,&do_say,
                        "Unfortunatly your clan isn't that wealthy.");
            return;
        }
        ch->gold+=valuegold;
        ch->silver+=valuesilver;
        ch->clan->clan_info->coins-=100*valuegold+valuesilver;
        save_clans();

        sprintf_to_char(ch,
                        "You have withdrawn %s from your clan account.\n\r",
                        money_string(valuegold,valuesilver));

        do_function(ch,&do_balance,"");
        return;
    }

    if (100*valuegold+valuesilver>ch->pcdata->bank) {
        do_function(banker,&do_say,"Unfortunatly you are not that wealthy.");
        return;
    }

    ch->gold+=valuegold;
    ch->silver+=valuesilver;
    ch->pcdata->bank-=100*valuegold+valuesilver;

    sprintf_to_char(ch,
                    "You have withdrawn %s from your account.\n\r",
                    money_string(valuegold,valuesilver));
    do_function(ch,&do_balance,"");
}
