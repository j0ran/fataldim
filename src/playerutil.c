/* $Id: playerutil.c,v 1.9 2005/12/02 19:01:21 jodocus Exp $ */
#include <sys/types.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#if !defined(__FreeBSD__)
#include <malloc.h>
#endif
#ifdef sun
#include <dirent.h>
#else
#ifdef __FreeBSD__
#include <dirent.h>
#else
#include <sys/dir.h>
#endif
#endif
#include <unistd.h>

typedef char bool;

#define MAX_STRING_LENGTH 1024
#define MAX_INPUT_LENGTH 1024
#define PLAYER_DIR "player/"
#define TRUE (-1)
#define FALSE (0)
#define LOWER(c)		((c) >= 'A' && (c) <= 'Z' ? (c)+'a'-'A' : (c))
#define UPPER(c)		((c) >= 'a' && (c) <= 'z' ? (c)+'A'-'a' : (c))

/*
 * Player info structure.
 */
struct player_info {
    int level;
    time_t lastlogin;
    bool online;
    int clan;
    int rank;
    char name[80];
    char email[80];
    char title[160];
};

bool str_cmp( const char *astr, const char *bstr )
{
    if ( astr == NULL )
    {
        return TRUE;
    }

    if ( bstr == NULL )
    {
        return TRUE;
    }

    for ( ; *astr || *bstr; astr++, bstr++ )
    {
        if ( LOWER(*astr) != LOWER(*bstr) )
            return TRUE;
    }

    return FALSE;
}

char *capitalize( const char *str )
{
    static char strcap[MAX_STRING_LENGTH];
    int i;

    for ( i = 0; str[i] != '\0'; i++ )
        strcap[i] = LOWER(str[i]);
    strcap[i] = '\0';
    strcap[0] = UPPER(strcap[0]);
    return strcap;
}

char *one_argument( char *argument, char *arg_first )
{
    char cEnd;

    while ( isspace(*argument) )
        argument++;

    cEnd = ' ';
    if ( *argument == '\'' || *argument == '"' )
        cEnd = *argument++;

    while ( *argument != '\0' )
    {
        if ( *argument == cEnd )
        {
            argument++;
            break;
        }
        *arg_first = LOWER(*argument);
        arg_first++;
        argument++;
    }
    *arg_first = '\0';

    while ( isspace(*argument) )
        argument++;

    return argument;
}


bool getplayerinfo(char *name,struct player_info *pi)
{
    char strsave[MAX_INPUT_LENGTH];
    char buf[MAX_STRING_LENGTH];
    char word[MAX_STRING_LENGTH];
    char *arg;
    char *s;
    FILE *fp;

    pi->level=-1;
    pi->lastlogin=0;
    pi->clan=0;
    pi->rank=0;
    pi->online=FALSE;
    strcpy(pi->name,name);
    strcpy(pi->email,"none");
    pi->title[0]='\0';

    sprintf( strsave, "%s%s", PLAYER_DIR, capitalize( name ) );
    if ( name[0]=='.' || ( fp = fopen( strsave, "r" ) ) == NULL )
        return FALSE;

    while(fgets(buf,sizeof(buf),fp))
    {
        arg=one_argument( buf, word );

        if(pi->level==-1 && (!str_cmp(word,"Level") || !str_cmp(word,"Levl")))
            pi->level=atoi(arg);
        else if(pi->lastlogin==0 && !str_cmp(word,"LogO"))
            pi->lastlogin=atoi(arg);
        else if(!str_cmp(word,"Rank"))
            pi->rank=atoi(arg);
        else if(!str_cmp(word,"Mail"))
        {
            if((s=strchr(arg,'~'))) *s='\0';
            strncpy(pi->email,arg,sizeof(pi->email));
            pi->email[sizeof(pi->email)-1]='\0';
        }
        else if(!str_cmp(word,"Titl"))
        {
            if((s=strchr(arg,'~'))) *s='\0';
            strncpy(pi->title,arg,sizeof(pi->title));
            pi->title[sizeof(pi->title)-1]='\0';
        }
        else if(!str_cmp(word,"End")) break;
    }

    fclose(fp);
    return TRUE;
}

int main(int argc,char *argv[])
{
    DIR *dp;
    struct dirent *de;
    struct player_info pi;
    char buf[MAX_STRING_LENGTH];
    int i;

    int level=4;	/* Lower or equal this level will be deleted. */
    int days=40;	/* And more then this number of days */
    bool doit=FALSE;	/* Really delete and not only test */

    for(i=1;i<argc;i++)
    {
        if(argv[i][0]=='-')
        {
            switch(argv[i][1]) {
            case 'l': level=atoi(argv[i]+2);break;
            case 'd': days=atoi(argv[i]+2);break;
            case 'x': doit=atoi(argv[i]+2);break;
            default : goto showarg;
            }
        }
        else goto showarg;
    }

    dp=opendir(PLAYER_DIR);
    if(dp == NULL) {
        fprintf(stderr, "Can not open player directory: %s\n", PLAYER_DIR);
        exit(1);
    }

    while((de=readdir(dp))!=NULL)
    {
        if(de->d_name[0]=='.') continue;

        if(getplayerinfo(de->d_name,&pi))
        {
            if((difftime(time(NULL),pi.lastlogin)/86400)>days
                    && pi.level<=level)
            {
                if(doit)
                {
                    printf("Character %s is deleted.\n",pi.name);
                    sprintf(buf,"%s%s",PLAYER_DIR,pi.name);
                    unlink(buf);
                }
                else printf("Character %s would be deleted.\n",pi.name);
            }

        }
    }

    closedir(dp);

    exit(0);

showarg:
    printf("%s [-d<days>] [-l<level>] [-x<0|1>]\n",argv[0]);
    exit(1);
}
