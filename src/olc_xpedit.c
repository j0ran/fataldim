//
// $Id: olc_xpedit.c,v 1.12 2004/02/22 10:00:14 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//
#include "merc.h"
#include "olc.h"
#include "interp.h"
#include "db.h"


//
// MPEDIT
//

bool mpedit_show( CHAR_DATA *ch, char *argument )
{
    TCLPROG_CODE *pCode;
    char buf[MAX_STRING_LENGTH];

    EDIT_TCLPROG(ch, pCode);

    sprintf( buf, "{yVnum :{x [%5d] %s\n\r"
		  "{yArea :{x [%5d] [%d] %s\n\r"
		  "{yTitle:{x %s\n\r"
		  "{yCode:{x\n\r",
	pCode->vnum,
	pCode->deleted?"{Rdeleted{x":"",
	!pCode->area ? -1 : pCode->area->vnum,
	!pCode->area ? 0  : pCode->area->version,
	!pCode->area ? "No Area" : pCode->area->name,
	!pCode->title ? "(no title)" : pCode->title );
    send_to_char( buf, ch );

    if(pCode->code[0]!='\0') {
	STR_SET_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);
	send_to_char( pCode->code, ch );
	STR_REMOVE_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);
    } else
	send_to_char( "Empty program.\n\r", ch );

    return FALSE;
}

bool mpedit_create( CHAR_DATA *ch, char *argument )
{
    TCLPROG_CODE *pCode;
    AREA_DATA *pArea;
    int  value;
    int iHash;

    if (argument[0]==0) {
	for (value=ch->in_room->area->lvnum;
	    value<=ch->in_room->area->uvnum;value++)
	    if ( !get_mprog_index( value ) )
		break;
	if (value>ch->in_room->area->uvnum) {
	    send_to_char("No free vnums found in this area.\n\r",ch);
	    return FALSE;
	}
    } else
	value = atoi( argument );

    /* OLC 1.1b */
    if ( value <= 0 || value >= MAX_VNUMS )
    {
	char output[MAX_STRING_LENGTH];

	sprintf( output, "Syntax:  create [0 < vnum < %d]\n\r", MAX_VNUMS );
	send_to_char( output, ch );
	return FALSE;
    }

    pArea = get_vnum_area( value );

    if ( !pArea )
    {
	send_to_char( "MPEdit:  That vnum is not assigned to an area.\n\r", ch );
	return FALSE;
    }

    if ( !IS_BUILDER( ch, pArea ) )
    {
	send_to_char( "MPEdit:  Vnum in an area you cannot build in.\n\r", ch );
	return FALSE;
    }

    if ( get_mprog_index( value ) )
    {
	send_to_char( "MPEdit:  MobProg vnum already exists.\n\r", ch );
	return FALSE;
    }

    pCode			= new_mprog_code_index();
    pCode->vnum			= value;
    pCode->area			= pArea;

    pCode->edit			= TRUE;

    iHash			= value % MAX_KEY_HASH;
    pCode->next			= mprog_index_hash[iHash];
    mprog_index_hash[iHash]	= pCode;

    if(ch->desc->editor == ED_MOBPROG)
    {
	TCLPROG_CODE *pCode2;

	pCode2=(TCLPROG_CODE *)ch->desc->pEdit;
	pCode2->edit=FALSE;
    }

    ch->desc->pEdit		= (void *)pCode;

    send_to_char( "MobProg Created.\n\r", ch );

    return TRUE;
}

bool mpedit_append( CHAR_DATA *ch, char *argument )
{
    TCLPROG_CODE *pCode;

    EDIT_TCLPROG(ch, pCode);

    if ( argument[0] == '\0' )
    {
	editor_start( ch, &pCode->code );
	return TRUE;
    }

    send_to_char( "Syntax:  append\n\r", ch );
    return FALSE;
}

bool mpedit_clear( CHAR_DATA *ch, char *argument )
{
    TCLPROG_CODE *pCode;

    EDIT_TCLPROG(ch, pCode);

    if ( argument[0] == '\0' )
    {
	free_string( pCode->code );
	pCode->code = &str_empty[0];

	return TRUE;
    }

    send_to_char( "Syntax:  clear\n\r", ch );
    return FALSE;
}

bool mpedit_delete( CHAR_DATA *ch, char *argument ) {
    TCLPROG_CODE *pCode;

    EDIT_TCLPROG(ch, pCode);

    pCode->deleted=!pCode->deleted;
    sprintf_to_char(ch,"This entry is %s deleted.\n\r",
	pCode->deleted?"now":"not any more");

   return TRUE;
}

bool mpedit_title( CHAR_DATA *ch, char *argument ) {
    TCLPROG_CODE *pCode;

    EDIT_TCLPROG(ch, pCode);

    if ( argument[0] == '\0' ) {
	send_to_char( "Syntax:  title [string]\n\r", ch );
	return FALSE;
    }

    free_string(pCode->title);
    pCode->title=str_dup(argument);

    send_to_char("Title set.\n\r",ch);
    return TRUE;
}

//
// OPEDIT
//

bool opedit_title( CHAR_DATA *ch, char *argument ) {
    TCLPROG_CODE *pCode;

    EDIT_TCLPROG(ch, pCode);

    if ( argument[0] == '\0' ) {
	send_to_char( "Syntax:  title [string]\n\r", ch );
	return FALSE;
    }

    free_string(pCode->title);
    pCode->title=str_dup(argument);

    send_to_char("Title set.\n\r",ch);
    return TRUE;
}

bool opedit_show( CHAR_DATA *ch, char *argument )
{
    TCLPROG_CODE *pCode;
    char buf[MAX_STRING_LENGTH];

    EDIT_TCLPROG(ch, pCode);

    sprintf( buf, "{yVnum :{x [%5d] %s\n\r"
		  "{yArea :{x [%5d] [%d] %s\n\r"
		  "{yTitle:{x %s\n\r"
		  "{yCode:{x\n\r",
	pCode->vnum,
	pCode->deleted?"{Rdeleted{x":"",
	!pCode->area ? -1 : pCode->area->vnum,
	!pCode->area ? 0  : pCode->area->version,
	!pCode->area ? "No Area" : pCode->area->name,
	!pCode->title ? "(no title)" : pCode->title );
    send_to_char( buf, ch );

    if(pCode->code[0]!='\0') {
	STR_SET_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);
	send_to_char( pCode->code, ch );
	STR_REMOVE_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);
    } else
	send_to_char( "Empty program.\n\r", ch );

    return FALSE;
}

bool opedit_create( CHAR_DATA *ch, char *argument )
{
    TCLPROG_CODE *pCode;
    AREA_DATA *pArea;
    int  value;
    int  iHash;

    if (argument[0]==0) {
	for (value=ch->in_room->area->lvnum;
	    value<=ch->in_room->area->uvnum;value++)
	    if ( !get_oprog_index( value ) )
		break;
	if (value>ch->in_room->area->uvnum) {
	    send_to_char("No free vnums found in this area.\n\r",ch);
	    return FALSE;
	}
    } else
	value = atoi( argument );

    /* OLC 1.1b */
    if ( value <= 0 || value >= MAX_VNUMS )
    {
	char output[MAX_STRING_LENGTH];

	sprintf( output, "Syntax:  create [0 < vnum < %d]\n\r", MAX_VNUMS );
	send_to_char( output, ch );
	return FALSE;
    }

    pArea = get_vnum_area( value );

    if ( !pArea )
    {
	send_to_char( "OPEdit:  That vnum is not assigned to an area.\n\r", ch );
	return FALSE;
    }

    if ( !IS_BUILDER( ch, pArea ) )
    {
	send_to_char( "OPEdit:  Vnum in an area you cannot build in.\n\r", ch );
	return FALSE;
    }

    if ( get_oprog_index( value ) )
    {
	send_to_char( "OPEdit:  MobProg vnum already exists.\n\r", ch );
	return FALSE;
    }

    pCode			= new_oprog_code_index();
    pCode->vnum			= value;
    pCode->area			= pArea;

    pCode->edit			= TRUE;

    iHash			= value % MAX_KEY_HASH;
    pCode->next			= oprog_index_hash[iHash];
    oprog_index_hash[iHash]	= pCode;

    if(ch->desc->editor == ED_OBJPROG)
    {
	TCLPROG_CODE *pCode2;

	pCode2=(TCLPROG_CODE *)ch->desc->pEdit;
	pCode2->edit=FALSE;
    }

    ch->desc->pEdit		= (void *)pCode;

    send_to_char( "ObjProg Created.\n\r", ch );

    return TRUE;
}

bool opedit_append( CHAR_DATA *ch, char *argument )
{
    TCLPROG_CODE *pCode;

    EDIT_TCLPROG(ch, pCode);

    if ( argument[0] == '\0' )
    {
	editor_start( ch, &pCode->code );
	return TRUE;
    }

    send_to_char( "Syntax:  append\n\r", ch );
    return FALSE;
}

bool opedit_clear( CHAR_DATA *ch, char *argument )
{
    TCLPROG_CODE *pCode;

    EDIT_TCLPROG(ch, pCode);

    if ( argument[0] == '\0' )
    {
	free_string( pCode->code );
	pCode->code = &str_empty[0];

	return TRUE;
    }

    send_to_char( "Syntax:  clear\n\r", ch );
    return FALSE;
}

bool opedit_delete( CHAR_DATA *ch, char *argument ) {
    TCLPROG_CODE *pCode;

    EDIT_TCLPROG(ch, pCode);

    pCode->deleted=!pCode->deleted;
    sprintf_to_char(ch,"This entry is %s deleted.\n\r",
	pCode->deleted?"now":"not any more");

   return TRUE;
}

//
// RPEDIT
//

bool rpedit_title( CHAR_DATA *ch, char *argument ) {
    TCLPROG_CODE *pCode;

    EDIT_TCLPROG(ch, pCode);

    if ( argument[0] == '\0' ) {
	send_to_char( "Syntax:  title [string]\n\r", ch );
	return FALSE;
    }

    free_string(pCode->title);
    pCode->title=str_dup(argument);

    send_to_char("Title set.\n\r",ch);
    return TRUE;
}

bool rpedit_show( CHAR_DATA *ch, char *argument )
{
    TCLPROG_CODE *pCode;
    char buf[MAX_STRING_LENGTH];

    EDIT_TCLPROG(ch, pCode);

    sprintf( buf, "{yVnum :{x [%5d] %s\n\r"
		  "{yArea :{x [%5d] [%d] %s\n\r"
		  "{yTitle:{x %s\n\r"
		  "{yCode:{x\n\r",
	pCode->vnum,
	pCode->deleted?"{Rdeleted{x":"",
	!pCode->area ? -1 : pCode->area->vnum,
	!pCode->area ? 0  : pCode->area->version,
	!pCode->area ? "No Area" : pCode->area->name,
	!pCode->title ? "(no title)" : pCode->title );
    send_to_char( buf, ch );

    if(pCode->code[0]!='\0') {
	STR_SET_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);
	send_to_char( pCode->code, ch );
	STR_REMOVE_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);
    } else
	send_to_char( "Empty program.\n\r", ch );

    return FALSE;
}

bool rpedit_create( CHAR_DATA *ch, char *argument )
{
    TCLPROG_CODE *pCode;
    AREA_DATA *pArea;
    int  value;
    int  iHash;

    if (argument[0]==0) {
	for (value=ch->in_room->area->lvnum;
	    value<=ch->in_room->area->uvnum;value++)
	    if ( !get_rprog_index( value ) )
		break;
	if (value>ch->in_room->area->uvnum) {
	    send_to_char("No free vnums found in this area.\n\r",ch);
	    return FALSE;
	}
    } else
	value = atoi( argument );

    /* OLC 1.1b */
    if ( value <= 0 || value >= MAX_VNUMS )
    {
	char output[MAX_STRING_LENGTH];

	sprintf( output, "Syntax:  create [0 < vnum < %d]\n\r", MAX_VNUMS );
	send_to_char( output, ch );
	return FALSE;
    }

    pArea = get_vnum_area( value );

    if ( !pArea )
    {
	send_to_char( "RPEdit:  That vnum is not assigned to an area.\n\r", ch );
	return FALSE;
    }

    if ( !IS_BUILDER( ch, pArea ) )
    {
	send_to_char( "RPEdit:  Vnum in an area you cannot build in.\n\r", ch );
	return FALSE;
    }

    if ( get_rprog_index( value ) )
    {
	send_to_char( "RPEdit:  RoomProg vnum already exists.\n\r", ch );
	return FALSE;
    }

    pCode			= new_rprog_code_index();
    pCode->vnum			= value;
    pCode->area			= pArea;

    pCode->edit			= TRUE;

    iHash			= value % MAX_KEY_HASH;
    pCode->next			= rprog_index_hash[iHash];
    rprog_index_hash[iHash]	= pCode;

    if (ch->desc->editor == ED_ROOMPROG) {
	TCLPROG_CODE *pCode2;

	pCode2=(TCLPROG_CODE *)ch->desc->pEdit;
	pCode2->edit=FALSE;
    }

    ch->desc->pEdit		= (void *)pCode;

    send_to_char( "RoomProg Created.\n\r", ch );

    return TRUE;
}

bool rpedit_append( CHAR_DATA *ch, char *argument )
{
    TCLPROG_CODE *pCode;

    EDIT_TCLPROG(ch, pCode);

    if ( argument[0] == '\0' )
    {
	editor_start( ch, &pCode->code );
	return TRUE;
    }

    send_to_char( "Syntax:  append\n\r", ch );
    return FALSE;
}

bool rpedit_clear( CHAR_DATA *ch, char *argument )
{
    TCLPROG_CODE *pCode;

    EDIT_TCLPROG(ch, pCode);

    if ( argument[0] == '\0' )
    {
	free_string( pCode->code );
	pCode->code = &str_empty[0];

	return TRUE;
    }

    send_to_char( "Syntax:  clear\n\r", ch );
    return FALSE;
}

bool rpedit_delete( CHAR_DATA *ch, char *argument ) {
    TCLPROG_CODE *pCode;

    EDIT_TCLPROG(ch, pCode);

    pCode->deleted=!pCode->deleted;
    sprintf_to_char(ch,"This entry is %s deleted.\n\r",
	pCode->deleted?"now":"not any more");

   return TRUE;
}

//
// APEDIT
//

bool apedit_title( CHAR_DATA *ch, char *argument ) {
    TCLPROG_CODE *pCode;

    EDIT_TCLPROG(ch, pCode);

    if ( argument[0] == '\0' ) {
	send_to_char( "Syntax:  title [string]\n\r", ch );
	return FALSE;
    }

    free_string(pCode->title);
    pCode->title=str_dup(argument);

    send_to_char("Title set.\n\r",ch);
    return TRUE;
}

bool apedit_show( CHAR_DATA *ch, char *argument )
{
    TCLPROG_CODE *pCode;
    char buf[MAX_STRING_LENGTH];

    EDIT_TCLPROG(ch, pCode);

    sprintf( buf, "{yVnum :{x [%5d] %s\n\r"
		  "{yArea :{x [%5d] [%d] %s\n\r"
		  "{yTitle:{x %s\n\r"
		  "{yCode:{x\n\r",
	pCode->vnum,
	pCode->deleted?"{Rdeleted{x":"",
	!pCode->area ? -1 : pCode->area->vnum,
	!pCode->area ? 0  : pCode->area->version,
	!pCode->area ? "No Area" : pCode->area->name,
	!pCode->title ? "(no title)" : pCode->title );
    send_to_char( buf, ch );

    if(pCode->code[0]!='\0') {
	STR_SET_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);
	send_to_char( pCode->code, ch );
	STR_REMOVE_BIT(ch->strbit_act,PLR_HAS_RAW_COLOUR);
    } else
	send_to_char( "Empty program.\n\r", ch );

    return FALSE;
}

bool apedit_create( CHAR_DATA *ch, char *argument )
{
    TCLPROG_CODE *pCode;
    AREA_DATA *pArea;
    int  value;

    if (argument[0]==0)
	value=ch->in_room->area->vnum;
    else
	value = atoi( argument );

    /* OLC 1.1b */
    if ( value <= 0 || value >= MAX_VNUMS )
    {
	char output[MAX_STRING_LENGTH];

	sprintf( output, "Syntax:  create [0 < vnum < %d]\n\r", MAX_VNUMS );
	send_to_char( output, ch );
	return FALSE;
    }

    pArea = get_area_data( value );

    if ( !pArea )
    {
	send_to_char( "APEdit:  That vnum is not assigned to an area.\n\r", ch );
	return FALSE;
    }

    if ( !IS_BUILDER( ch, pArea ) )
    {
	send_to_char( "APEdit:  Vnum in an area you cannot build in.\n\r", ch );
	return FALSE;
    }

    if ( get_aprog_index( value ) )
    {
	send_to_char( "APEdit:  AreaProg vnum already exists.\n\r", ch );
	return FALSE;
    }

    pCode			= new_aprog_code_index();
    pCode->vnum			= value;
    pCode->area			= pArea;

    pCode->edit			= TRUE;

    pArea->aprog=pCode;

    if (ch->desc->editor == ED_AREAPROG) {
	TCLPROG_CODE *pCode2;

	pCode2=(TCLPROG_CODE *)ch->desc->pEdit;
	pCode2->edit=FALSE;
    }

    ch->desc->pEdit		= (void *)pCode;

    send_to_char( "AreaProg Created.\n\r", ch );

    return TRUE;
}

bool apedit_append( CHAR_DATA *ch, char *argument )
{
    TCLPROG_CODE *pCode;

    EDIT_TCLPROG(ch, pCode);

    if ( argument[0] == '\0' )
    {
	editor_start( ch, &pCode->code );
	return TRUE;
    }

    send_to_char( "Syntax:  append\n\r", ch );
    return FALSE;
}

bool apedit_clear( CHAR_DATA *ch, char *argument )
{
    TCLPROG_CODE *pCode;

    EDIT_TCLPROG(ch, pCode);

    if ( argument[0] == '\0' )
    {
	free_string( pCode->code );
	pCode->code = &str_empty[0];

	return TRUE;
    }

    send_to_char( "Syntax:  clear\n\r", ch );
    return FALSE;
}

bool apedit_delete( CHAR_DATA *ch, char *argument ) {
    TCLPROG_CODE *pCode;

    EDIT_TCLPROG(ch, pCode);

    pCode->deleted=!pCode->deleted;
    sprintf_to_char(ch,"This entry is %s deleted.\n\r",
	pCode->deleted?"now":"not any more");

   return TRUE;
}
