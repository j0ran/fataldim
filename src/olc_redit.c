// $Id: olc_redit.c,v 1.17 2006/01/22 14:04:06 jodocus Exp $
/***************************************************************************
 *  File: olc.c                                                            *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 *                                                                         *
 *  This code was freely distributed with the The Isles 1.1 source code,   *
 *  and has been used here for OLC - OLC would not be what it is without   *
 *  all the previous coders who released their source code.                *
 *                                                                         *
 ***************************************************************************/

#include "merc.h"
#include "olc.h"
#include "interp.h"
#include "db.h"

bool redit_mlist( CHAR_DATA *ch, char *argument )
{
    MOB_INDEX_DATA	*pMobIndex;
    AREA_DATA		*pArea;
    char		buf  [ MAX_STRING_LENGTH   ];
    char		buf1 [ MAX_STRING_LENGTH*2 ];
    char		arg  [ MAX_INPUT_LENGTH    ];
    bool fAll, found;
    int vnum;
    int  col = 0;

    one_argument( argument, arg );
    if ( arg[0] == '\0' )
    {
	send_to_char( "Syntax:  mlist <all/name>\n\r", ch );
	return FALSE;
    }

    pArea = ch->in_room->area;
    buf1[0] = '\0';
    fAll    = !str_cmp( arg, "all" );
    found   = FALSE;

    for ( vnum = pArea->lvnum; vnum <= pArea->uvnum; vnum++ )
    {
	if ( ( pMobIndex = get_mob_index( vnum ) ) )
	{
	    if ( fAll || is_name( arg, pMobIndex->player_name ) )
	    {
		found = TRUE;
		sprintf( buf, "[%5d] %-17.16s",
		    pMobIndex->vnum, capitalize( pMobIndex->short_descr ) );
		strcat( buf1, buf );
		if ( ++col % 3 == 0 )
		    strcat( buf1, "\n\r" );
	    }
	}
    }

    if ( !found )
    {
	send_to_char( "Mobile(s) not found in this area.\n\r", ch);
	return FALSE;
    }

    if ( col % 3 != 0 )
	strcat( buf1, "\n\r" );

    send_to_char( buf1, ch );
    return FALSE;
}



bool redit_olist( CHAR_DATA *ch, char *argument )
{
    OBJ_INDEX_DATA	*pObjIndex;
    AREA_DATA		*pArea;
    char		buf  [ MAX_STRING_LENGTH   ];
    BUFFER *		buf1;
    char		arg  [ MAX_INPUT_LENGTH    ];
    bool fAll, found;
    int vnum;
    int  col = 0;

    one_argument( argument, arg );
    if ( arg[0] == '\0' )
    {
	send_to_char( "Syntax:  olist <all/name/item_type>\n\r", ch );
	return FALSE;
    }

    pArea = ch->in_room->area;
    buf1    = new_buf();
    fAll    = !str_cmp( arg, "all" );
    found   = FALSE;

    for ( vnum = pArea->lvnum; vnum <= pArea->uvnum; vnum++ )
    {
	if ( ( pObjIndex = get_obj_index( vnum ) ) )
	{
	    if ( fAll || is_name( arg, pObjIndex->name )
	    || table_find_name(arg,item_table) == pObjIndex->item_type )
	    {
		found = TRUE;
		sprintf( buf, "[%5d] %-17.16s",
		    pObjIndex->vnum, str_nocolor(capitalize( pObjIndex->short_descr ) ));
		if (!add_buf( buf1, buf )) break;
		if ( ++col % 3 == 0 )
		    add_buf( buf1, "\n\r" );
	    }
	}
    }

    if ( !found )
    {
	send_to_char( "Object(s) not found in this area.\n\r", ch);
	free_buf(buf1);
	return FALSE;
    }

    if ( col % 3 != 0 )
	add_buf( buf1, "\n\r" );

    page_to_char(buf_string(buf1),ch);
    free_buf(buf1);
    return FALSE;
}

bool redit_rlist( CHAR_DATA *ch, char *argument )
{
    ROOM_INDEX_DATA	*pRoomIndex;
    AREA_DATA		*pArea;
    char		buf  [ MAX_STRING_LENGTH   ];
    char		buf1 [ MAX_STRING_LENGTH*4 ];
    char		arg  [ MAX_INPUT_LENGTH    ];
    bool fAll, found;
    int vnum;
    int  col = 0;

    one_argument( argument, arg );
    if ( arg[0] == '\0' )
    {
	send_to_char( "Syntax:  rlist <all/name>\n\r", ch );
	return FALSE;
    }

    pArea = ch->in_room->area;
    buf1[0] = '\0';
    fAll    = !str_cmp( arg, "all" );
    found   = FALSE;

    for ( vnum = pArea->lvnum; vnum <= pArea->uvnum; vnum++ )
    {
	if ( ( pRoomIndex = get_room_index( vnum ) ) )
	{
	    if ( fAll || is_name( arg, pRoomIndex->name ) )
	    {
		found = TRUE;
		sprintf( buf, "[%5d] %-17.16s",
		    pRoomIndex->vnum, capitalize( pRoomIndex->name ) );
		strcat( buf1, buf );
		if ( ++col % 3 == 0 )
		    strcat( buf1, "\n\r" );
	    }
	}
    }

    if ( !found )
    {
	send_to_char( "Room(s) not found in this area.\n\r", ch);
	return FALSE;
    }

    if ( col % 3 != 0 )
	strcat( buf1, "\n\r" );

    send_to_char( buf1, ch );
    return FALSE;
}


/*
 * Room Editor Functions.
 */
bool redit_show( CHAR_DATA *ch, char *argument )
{
    ROOM_INDEX_DATA	*pRoom;
    char		buf  [MAX_STRING_LENGTH];
    char		buf1 [2*MAX_STRING_LENGTH];
    OBJ_DATA		*obj;
    CHAR_DATA		*rch;
    int			door;
    bool		fcnt;

    EDIT_ROOM(ch, pRoom);

    buf1[0] = '\0';

    sprintf( buf, "{yDescription: %s{x\n\r%s",
	pRoom->deleted?"{Rdeleted{x":"",
	pRoom->description );
    strcat( buf1, buf );

    sprintf( buf, "{yName:{x       [%s]\n\r{yArea:{x       [%5d] [%d] %s\n\r",
	    pRoom->name, pRoom->area->vnum, pRoom->area->version,
	    pRoom->area->name );
    strcat( buf1, buf );

    sprintf( buf, "{yVnum:{x       [%5d]\n\r{ySector:{x     [%s]\n\r",
	    pRoom->vnum, room_sector_type(pRoom) );
    strcat( buf1, buf );

    sprintf( buf, "{yRoom flags:{x [%s]\n\r",room_flags(pRoom));
    strcat( buf1, buf );

    sprintf( buf, "{yLevel     :{x [%d]\n\r",pRoom->level);
    strcat( buf1, buf );

    if ( pRoom->extra_descr )
    {
	EXTRA_DESCR_DATA *ed;

	strcat( buf1, "{yDesc Kwds:{x  [" );
	for ( ed = pRoom->extra_descr; ed; ed = ed->next )
	{
	    strcat( buf1, ed->keyword );
	    if ( ed->next )
		strcat( buf1, " " );
	}
	strcat( buf1, "]\n\r" );
    }

    strcat( buf1, "{yCharacters:{x [" );
    fcnt = FALSE;
    for ( rch = pRoom->people; rch; rch = rch->next_in_room )
    {
	one_argument( rch->name, buf );
	strcat( buf1, buf );
	strcat( buf1, " " );
	fcnt = TRUE;
    }

    if ( fcnt )
    {
	int end;

	end = strlen(buf1) - 1;
	buf1[end] = ']';
	strcat( buf1, "\n\r" );
    }
    else
	strcat( buf1, "none]\n\r" );

    strcat( buf1, "{yObjects:{x    [" );
    fcnt = FALSE;
    for ( obj = pRoom->contents; obj; obj = obj->next_content )
    {
	one_argument( obj->name, buf );
	strcat( buf1, buf );
	strcat( buf1, " " );
	fcnt = TRUE;
    }

    if ( fcnt )
    {
	int end;

	end = strlen(buf1) - 1;
	buf1[end] = ']';
	strcat( buf1, "\n\r" );
    }
    else
	strcat( buf1, "none]\n\r" );

    if(pRoom->rprog_vnum!=0) {
	TCLPROG_CODE *rProg=get_rprog_index(pRoom->rprog_vnum);

	if(rProg) {
	    sprintf( buf, "{yProg instance:{x [%d] %s\n\r",
		pRoom->rprog_vnum,rProg->title);
	    strcat(buf1,buf);
	}
    }
    else strcat(buf1,"{yProg instance:{x none\n\r");

    if ( pRoom->pueblo_picture[0] ) {
	strcat(buf1,"{yPicture:{x    ");
	strcat(buf1,url(buf,pRoom->pueblo_picture,pRoom->area,TRUE));
	strcat(buf1,"\n\r");
    }

    for ( door = 0; door < MAX_DIR; door++ )
    {
	EXIT_DATA *pexit;

	if ( ( pexit = pRoom->exit[door] ) )
	{
	    char word[MAX_INPUT_LENGTH];
	    char reset_state[MAX_STRING_LENGTH];
	    char *state;
	    int i, length;

	    sprintf( buf, "-%-5s to [%5d] {yKey:{x [%5d]",
		capitalize(dir_name[door]),
		pexit->to_room ? pexit->to_room->vnum : 0,
		pexit->key );
	    strcat( buf1, buf );

	    /*
	     * Format up the exit info.
	     * Capitalize all flags that are not part of the reset info.
	     */
	    strcpy( reset_state, exit_reset_string(pexit));
	    state = exit_flags(pexit);
	    strcat( buf1, " {yExit flags:{x [" );
	    for (; ;)
	    {
		state = one_argument( state, word );

		if ( word[0] == '\0' )
		{
		    int end;

		    end = strlen(buf1) - 1;
		    if (buf1[end]==' ')
			buf1[end] = ']';
		    else {
			buf1[end+1] = ']';
			buf1[end+2] = 0;
		    }
		    strcat( buf1, "\n\r" );
		    break;
		}

		if ( str_infix( word, reset_state ) )
		{
		    length = strlen(word);
		    for (i = 0; i < length; i++)
			word[i] = toupper(word[i]);
		}
		strcat( buf1, word );
		strcat( buf1, " " );
	    }

	    if ( pexit->keyword && pexit->keyword[0] != '\0' )
	    {
		sprintf( buf, "{yKwds:{x [%s]\n\r", pexit->keyword );
		strcat( buf1, buf );
	    }
	    if ( pexit->description && pexit->description[0] != '\0' )
	    {
		sprintf( buf, "%s", pexit->description );
		strcat( buf1, buf );
	    }
	}
    }

    send_to_char( buf1, ch );

    show_properties(ch,pRoom->property,"perm");
    return FALSE;
}


/* OLC 1.1b */
/*****************************************************************************
 Name:		change_exit
 Purpose:	Command interpreter for changing exits.
 Called by:	redit_<dir>.  This is a local function.
 ****************************************************************************/
bool change_exit( CHAR_DATA *ch, char *argument, int door )
{
    ROOM_INDEX_DATA *pRoom;
    char command[MAX_INPUT_LENGTH];
    char arg[MAX_INPUT_LENGTH];
    char total_arg[MAX_STRING_LENGTH];
    int  rev;
    int  value;

    EDIT_ROOM(ch, pRoom);

    /* Often used data. */
    rev = rev_dir[door];

    if ( argument[0] == '\0' )
    {
	show_help(ch,"EXIT");
	return FALSE;
    }

    /*
     * Now parse the arguments.
     */
    strcpy( total_arg, argument );
    argument = one_argument( argument, command );
    one_argument( argument, arg );

    if ( !str_cmp( command, "delete" ) )
    {
	if ( !pRoom->exit[door] )
	{
	    send_to_char( "REdit:  Exit does not exist.\n\r", ch );
	    return FALSE;
	}

	/*
	 * Remove To Room Exit.
	 */
	if ( pRoom->exit[door]->to_room
	&&   pRoom->exit[door]->to_room->exit[rev]
	&& pRoom->exit[door]->to_room->exit[rev]->to_room == pRoom)
	{
	    free_exit( pRoom->exit[door]->to_room->exit[rev] );
	    pRoom->exit[door]->to_room->exit[rev] = NULL;
	}

	/*
	 * Remove this exit.
	 */
	free_exit( pRoom->exit[door] );
	pRoom->exit[door] = NULL;

	send_to_char( "Exit unlinked.\n\r", ch );
	return TRUE;
    }

    /*
     * Create a two-way exit.
     */
    if ( !str_cmp( command, "link" ) )
    {
	EXIT_DATA	*pExit;
	ROOM_INDEX_DATA	*pLinkRoom;

	if ( arg[0] == '\0' || !is_number( arg ) )
	{
	    send_to_char( "Syntax:  [direction] link [vnum]\n\r", ch );
	    return FALSE;
	}

	if ( !( pLinkRoom = get_room_index( atoi(arg) ) ) )
	{
	    send_to_char( "REdit:  Non-existant room.\n\r", ch );
	    return FALSE;
	}

	if ( !IS_BUILDER( ch, pLinkRoom->area ) )
	{
	    send_to_char( "REdit:  Cannot link to that area.\n\r", ch );
	    return FALSE;
	}

	if ( pLinkRoom->exit[rev] )
	{
	    send_to_char( "REdit:  Remote side's exit exists.\n\r", ch );
	    return FALSE;
	}

	if ( !pRoom->exit[door] )		/* No exit.		*/
	    pRoom->exit[door] = new_exit();

	pRoom->exit[door]->to_room = pLinkRoom;	/* Assign data.		*/
	pRoom->exit[door]->vnum = atoi(arg);

	pExit			= new_exit();	/* No remote exit.	*/

	pExit->to_room		= ch->in_room;	/* Assign data.		*/
	pExit->vnum		= ch->in_room->vnum;

	pLinkRoom->exit[rev]	= pExit;	/* Link exit to room.	*/

	send_to_char( "Two-way link established.\n\r", ch );
	REMOVE_BIT(pRoom->exit[door]->rs_flags,EX_NOWARN);
	REMOVE_BIT(pLinkRoom->exit[rev]->rs_flags,EX_NOWARN);
	return TRUE;
    }

    /*
     * Create room and make two-way exit.
     */
    if ( !str_cmp( command, "dig" ) )
    {
	char buf[MAX_INPUT_LENGTH+5];

	if ( arg[0] == '\0' || !is_number( arg ) )
	{
	    send_to_char( "Syntax: [direction] dig <vnum>\n\r", ch );
	    return FALSE;
	}

	redit_create( ch, arg );		/* Create the room.	*/
	sprintf( buf, "link %s", arg );
	change_exit( ch, buf, door);		/* Create the exits.	*/
	return TRUE;
    }

    /*
     * Create one-way exit.
     */
    if ( !str_cmp( command, "room" ) )
    {
	ROOM_INDEX_DATA *pLinkRoom;

	if ( arg[0] == '\0' || !is_number( arg ) )
	{
	    send_to_char( "Syntax:  [direction] room [vnum]\n\r", ch );
	    return FALSE;
	}

	if ( !( pLinkRoom = get_room_index( atoi( arg ) ) ) )
	{
	    send_to_char( "REdit:  Non-existant room.\n\r", ch );
	    return FALSE;
	}

	if ( !pRoom->exit[door] )
	    pRoom->exit[door] = new_exit();

	pRoom->exit[door]->to_room = pLinkRoom;
	pRoom->exit[door]->vnum = atoi( arg );

	send_to_char( "One-way link established.\n\r", ch );
	REMOVE_BIT(pRoom->exit[door]->rs_flags,EX_NOWARN);
	return TRUE;
    }

    if ( !str_cmp( command, "remove" ) )
    {
	if ( arg[0] == '\0' )
	{
	    send_to_char( "Syntax:  [direction] remove [key/name/desc]\n\r", ch );
	    return FALSE;
	}

	if ( !pRoom->exit[door] )
	{
	    send_to_char( "REdit:  Exit does not exist.\n\r", ch );
	    return FALSE;
	}

	if ( !str_cmp( argument, "key" ) )
	{
	    pRoom->exit[door]->key = 0;
            send_to_char( "Exit key removed.\n\r", ch );
            return TRUE;
	}

	if ( !str_cmp( argument, "name" ) )
	{
	    free_string( pRoom->exit[door]->keyword );
	    pRoom->exit[door]->keyword = &str_empty[0];
            send_to_char( "Exit name removed.\n\r", ch );
            return TRUE;
	}

	if ( argument[0] == 'd' && !str_prefix( argument, "description" ) )
	{
	    free_string( pRoom->exit[door]->description );
	    pRoom->exit[door]->description = &str_empty[0];
            send_to_char( "Exit description removed.\n\r", ch );
            return TRUE;
	}

	send_to_char( "Syntax:  [direction] remove [key/name/desc]\n\r", ch );
	return FALSE;
    }

    if ( !str_cmp( command, "key" ) )
    {
	OBJ_INDEX_DATA *pObjIndex;

	if ( arg[0] == '\0' || !is_number( arg ) )
	{
	    send_to_char( "Syntax:  [direction] key [vnum]\n\r", ch );
	    return FALSE;
	}

	if ( !( pObjIndex = get_obj_index( atoi( arg ) ) ) )
	{
	    send_to_char( "REdit:  Item does not exist.\n\r", ch );
	    return FALSE;
	}

	if ( pObjIndex->item_type != ITEM_KEY )
	{
	    send_to_char( "REdit:  Item is not a key.\n\r", ch );
	    return FALSE;
	}

	if ( !pRoom->exit[door] )
	    pRoom->exit[door] = new_exit();

	pRoom->exit[door]->key = pObjIndex->vnum;

	send_to_char( "Exit key set.\n\r", ch );
	return TRUE;
    }

    if ( !str_cmp( command, "name" ) )
    {
	if ( arg[0] == '\0' )
	{
	    send_to_char( "Syntax:  [direction] name [string]\n\r", ch );
	    return FALSE;
	}

	if ( !pRoom->exit[door] )
	    pRoom->exit[door] = new_exit();

	free_string( pRoom->exit[door]->keyword );
	pRoom->exit[door]->keyword = str_dup( argument );

	send_to_char( "Exit name set.\n\r", ch );
	return TRUE;
    }

    if ( command[0] == 'd' && !str_prefix( command, "description" ) )
    {
	if ( arg[0] == '\0' )
	{
	    if ( !pRoom->exit[door] )
	        pRoom->exit[door] = new_exit();

	    editor_start( ch, &pRoom->exit[door]->description );
	    return TRUE;
	}

	send_to_char( "Syntax:  [direction] desc\n\r", ch );
	return FALSE;
    }

    /*
     * Set the exit flags, needs full argument.
     * ----------------------------------------
     */
    if ( ( value = option_find_name(total_arg,exit_options,TRUE) ) != NO_FLAG )
    {
	ROOM_INDEX_DATA *pToRoom;

	/*
	 * Create an exit if none exists.
	 */
	if ( !pRoom->exit[door] )
	    pRoom->exit[door] = new_exit();

	/*
	 * Set door bits for this room.
	 */
	TOGGLE_BIT(pRoom->exit[door]->rs_flags, value);
	pRoom->exit[door]->exit_info = pRoom->exit[door]->rs_flags;

	/*
	 * Set door bits of connected room.
	 * Skip one-way exits and non-existant rooms.
	 */
	if ( ( pToRoom = pRoom->exit[door]->to_room ) && pToRoom->exit[rev] )
	{
	    TOGGLE_BIT(pToRoom->exit[rev]->rs_flags, value);
	    pToRoom->exit[rev]->exit_info =  pToRoom->exit[rev]->rs_flags;
	}

	send_to_char( "Exit flag toggled.\n\r", ch );
	return TRUE;
    }

    return FALSE;
}



bool redit_north( CHAR_DATA *ch, char *argument )
{
    if ( change_exit( ch, argument, DIR_NORTH ) )
	return TRUE;

    return FALSE;
}



bool redit_south( CHAR_DATA *ch, char *argument )
{
    if ( change_exit( ch, argument, DIR_SOUTH ) )
	return TRUE;

    return FALSE;
}



bool redit_east( CHAR_DATA *ch, char *argument )
{
    if ( change_exit( ch, argument, DIR_EAST ) )
	return TRUE;

    return FALSE;
}



bool redit_west( CHAR_DATA *ch, char *argument )
{
    if ( change_exit( ch, argument, DIR_WEST ) )
	return TRUE;

    return FALSE;
}



bool redit_up( CHAR_DATA *ch, char *argument )
{
    if ( change_exit( ch, argument, DIR_UP ) )
	return TRUE;

    return FALSE;
}



bool redit_down( CHAR_DATA *ch, char *argument )
{
    if ( change_exit( ch, argument, DIR_DOWN ) )
	return TRUE;

    return FALSE;
}


/* OLC 1.1b */
bool redit_move( CHAR_DATA *ch, char *argument )
{
    interpret( ch, argument );
    return FALSE;
}



bool redit_ed( CHAR_DATA *ch, char *argument )
{
    ROOM_INDEX_DATA *pRoom;
    EXTRA_DESCR_DATA *ed;
    char command[MAX_INPUT_LENGTH];
    char keyword[MAX_INPUT_LENGTH];

    EDIT_ROOM(ch, pRoom);

    argument = one_argument( argument, command );
    one_argument( argument, keyword );
    smash_case(keyword);

    if ( command[0] == '\0' || keyword[0] == '\0' )
    {
	send_to_char( "Syntax:  ed add [keyword]\n\r", ch );
	send_to_char( "         ed edit [keyword]\n\r", ch );
	send_to_char( "         ed delete [keyword]\n\r", ch );
	send_to_char( "         ed format [keyword]\n\r", ch );
	send_to_char( "         ed show [keyword]\n\r", ch );
	return FALSE;
    }

    if ( !str_cmp( command, "add" ) )
    {
	if ( keyword[0] == '\0' )
	{
	    send_to_char( "Syntax:  ed add [keyword]\n\r", ch );
	    return FALSE;
	}

	ed			=   new_extra_descr();
	ed->keyword		=   str_dup( keyword );
	ed->description		=   str_dup( "" );
	ed->next		=   pRoom->extra_descr;
	pRoom->extra_descr	=   ed;

	editor_start( ch, &ed->description );

	return TRUE;
    }


    if ( !str_cmp( command, "edit" ) )
    {
	if ( keyword[0] == '\0' )
	{
	    send_to_char( "Syntax:  ed edit [keyword]\n\r", ch );
	    return FALSE;
	}

	for ( ed = pRoom->extra_descr; ed; ed = ed->next )
	{
	    if ( is_name( keyword, ed->keyword ) )
		break;
	}

	if ( !ed )
	{
	    send_to_char( "REdit:  Extra description keyword not found.\n\r", ch );
	    return FALSE;
	}

	editor_start( ch, &ed->description );

	return TRUE;
    }


    if ( !str_cmp( command, "delete" ) )
    {
	EXTRA_DESCR_DATA *ped = NULL;

	if ( keyword[0] == '\0' )
	{
	    send_to_char( "Syntax:  ed delete [keyword]\n\r", ch );
	    return FALSE;
	}

	for ( ed = pRoom->extra_descr; ed; ed = ed->next )
	{
	    if ( is_name( keyword, ed->keyword ) )
		break;
	    ped = ed;
	}

	if ( !ed )
	{
	    send_to_char( "REdit:  Extra description keyword not found.\n\r", ch );
	    return FALSE;
	}

	if ( !ped )
	    pRoom->extra_descr = ed->next;
	else
	    ped->next = ed->next;

	free_extra_descr( ed );

	send_to_char( "Extra description deleted.\n\r", ch );
	return TRUE;
    }


    if ( !str_cmp( command, "format" ) )
    {
	if ( keyword[0] == '\0' )
	{
	    send_to_char( "Syntax:  ed format [keyword]\n\r", ch );
	    return FALSE;
	}

	for ( ed = pRoom->extra_descr; ed; ed = ed->next )
	{
	    if ( is_name( keyword, ed->keyword ) )
		break;
	}

	if ( !ed )
	{
	    send_to_char( "REdit:  Extra description keyword not found.\n\r", ch );
	    return FALSE;
	}

	/* OLC 1.1b */
	if ( strlen(ed->description) >= (MAX_STRING_LENGTH - 4) )
	{
	    send_to_char( "String too long to be formatted.\n\r", ch );
	    return FALSE;
	}

	ed->description = format_string( ed->description );

	send_to_char( "Extra description formatted.\n\r", ch );
	return TRUE;
    }

    if ( !str_cmp( command, "show" ) )
    {
	if ( keyword[0] == '\0' )
	{
	    send_to_char( "Syntax:  ed show [keyword]\n\r", ch );
	    return FALSE;
	}

	for ( ed = pRoom->extra_descr; ed; ed = ed->next )
	{
	    if ( is_name( keyword, ed->keyword ) )
		break;
	}

	if ( !ed )
	{
	    send_to_char( "REdit:  Extra description keyword not found.\n\r", ch );
	    return FALSE;
	}

	send_to_char( ed->description , ch );

	return TRUE;
    }

    redit_ed( ch, "" );
    return FALSE;
}



bool redit_create( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;
    ROOM_INDEX_DATA *pRoom;
    int value;
    int iHash;

    EDIT_ROOM(ch, pRoom);

    if (argument[0]==0) {
	for (value=ch->in_room->area->lvnum;
	    value<=ch->in_room->area->uvnum;value++)
	    if ( !get_room_index( value ) )
		break;
	if (value>ch->in_room->area->uvnum) {
	    send_to_char("No free vnums found in this area.\n\r",ch);
	    return FALSE;
	}
    } else
	value = atoi( argument );

    /* OLC 1.1b */
    if ( value <= 0 || value >= MAX_VNUMS )
    {
	sprintf_to_char(ch,"Syntax:  create [0 < vnum < %d]\n\r", MAX_VNUMS );
	return FALSE;
    }

    pArea = get_vnum_area( value );
    if ( !pArea )
    {
	send_to_char( "REdit:  That vnum is not assigned to an area.\n\r", ch );
	return FALSE;
    }

    if ( !IS_BUILDER( ch, pArea ) )
    {
	send_to_char( "REdit:  Vnum in an area you cannot build in.\n\r", ch );
	return FALSE;
    }

    if ( get_room_index( value ) )
    {
	send_to_char( "REdit:  Room vnum already exists.\n\r", ch );
	return FALSE;
    }

    pRoom			= new_room_index();
    pRoom->area			= pArea;
    pRoom->vnum			= value;
    pArea->vnum_rooms_in_use++;

    if ( value > max_vnum_room )
        max_vnum_room = value;

    if (ch->desc->editor == ED_MOBPROG) {
	TCLPROG_CODE *pCode2;

	pCode2=(TCLPROG_CODE *)ch->desc->pEdit;
	pCode2->edit=FALSE;
    }

    iHash			= value % MAX_KEY_HASH;
    pRoom->next			= room_index_hash[iHash];
    room_index_hash[iHash]	= pRoom;
    ch->desc->pEdit		= (void *)pRoom;

    send_to_char( "Room created.\n\r", ch );
    return TRUE;
}

bool redit_clone( CHAR_DATA *ch, char *argument )
{
    ROOM_INDEX_DATA *pRoom;
    ROOM_INDEX_DATA *pSource;
    PROPERTY *prop;
    EXTRA_DESCR_DATA *pEd,*pNewEd;
    int value;

    EDIT_ROOM(ch, pRoom);

    value = atoi( argument );

    if ( argument[0] == '\0' || value <= 0 || value >= MAX_VNUMS )
    {
	char output[MAX_STRING_LENGTH];

	sprintf( output, "Syntax:  clone [0 < vnum < %d]\n\r", MAX_VNUMS );
	send_to_char( output, ch );
	return FALSE;
    }

    if ( argument[0] == '\0' || value <= 0 )
    {
	send_to_char( "Syntax:  clone [vnum > 0]\n\r", ch );
	return FALSE;
    }

    if(!(pSource=get_room_index( value )))
    {
	send_to_char("REdit: Room vnum doesn't exists.\n\r", ch);
	return FALSE;
    }

    /* Clear room */
    free_string( pRoom->name );
    free_string( pRoom->description );
//  free_string( pRoom->owner );

    while( pRoom->extra_descr )
    {
	pEd=pRoom->extra_descr->next;
	free_extra_descr(pRoom->extra_descr);
	pRoom->extra_descr=pEd;
    }

    /* Clone room */
    pRoom->name		= str_dup( pSource->name );
    pRoom->description  = str_dup( pSource->description );
    STR_COPY_STR(pRoom->strbit_room_flags,pSource->strbit_room_flags,MAX_FLAGS);
    pRoom->level	= pSource->level;

    for(pEd=pSource->extra_descr;pEd;pEd=pEd->next)
    {
	pNewEd=new_extra_descr();
	pNewEd->keyword=str_dup(pEd->keyword);
	pNewEd->description=str_dup(pEd->description);
	pNewEd->next=pRoom->extra_descr;
	pRoom->extra_descr=pNewEd;
    }

    /* and the properties */
    for (prop = pSource->property; prop ; prop = prop->next)
	if (prop->sValue==NULL||prop->sValue[0]==0)
	    SetRoomProperty(pRoom,prop->propIndex->type,
				prop->propIndex->key,&prop->iValue);
	else
	    SetRoomProperty(pRoom,prop->propIndex->type,
				prop->propIndex->key,prop->sValue);

    send_to_char("Room data cloned.\n\r", ch );
    return TRUE;
}


bool redit_name( CHAR_DATA *ch, char *argument )
{
    ROOM_INDEX_DATA *pRoom;

    EDIT_ROOM(ch, pRoom);

    if ( argument[0] == '\0' )
    {
	send_to_char( "Syntax:  name [name]\n\r", ch );
	return FALSE;
    }

    free_string( pRoom->name );
    pRoom->name = str_dup( argument );

    send_to_char( "Name set.\n\r", ch );
    return TRUE;
}



bool redit_desc( CHAR_DATA *ch, char *argument )
{
    ROOM_INDEX_DATA *pRoom;

    EDIT_ROOM(ch, pRoom);

    if ( argument[0] == '\0' )
    {
	editor_start( ch, &pRoom->description );
	return TRUE;
    }

    send_to_char( "Syntax:  desc\n\r", ch );
    return FALSE;
}




bool redit_format( CHAR_DATA *ch, char *argument )
{
    ROOM_INDEX_DATA *pRoom;

    EDIT_ROOM(ch, pRoom);

    /* OLC 1.1b */
    if ( strlen(pRoom->description) >= (MAX_STRING_LENGTH - 4) )
    {
	send_to_char( "String too long to be formatted.\n\r", ch );
	return FALSE;
    }

    pRoom->description = format_string( pRoom->description );

    send_to_char( "String formatted.\n\r", ch );
    return TRUE;
}

bool redit_level( CHAR_DATA *ch, char *argument )
{
    ROOM_INDEX_DATA *pRoom;

    EDIT_ROOM(ch, pRoom);

    if ( argument[0] == '\0' )
    {
	send_to_char( "Syntax:  level [value]\n\r", ch );
	return FALSE;
    }

    pRoom->level = atoi( argument );

    send_to_char( "Level set.\n\r", ch );
    return TRUE;
}

bool redit_delete( CHAR_DATA *ch, char *argument) {
    ROOM_INDEX_DATA *pRoom;

    EDIT_ROOM(ch, pRoom);

    pRoom->deleted=!pRoom->deleted;

    sprintf_to_char(ch,"Room is %s deleted.\n\r",
	pRoom->deleted?"now":"not anymore");
    return TRUE;
}

bool redit_picture(CHAR_DATA *ch, char *argument) {
    ROOM_INDEX_DATA *pRoom;
    char s[MSL],*p;
    int i;

    EDIT_ROOM(ch, pRoom);
    free_string(pRoom->pueblo_picture);

    p=argument;
    i=0;
    while (*p) {
	switch (*p) {
	    case ' ': s[i++]='%';s[i++]='2';s[i++]='0';break;
	    case '~': s[i++]='%';s[i++]='7';s[i++]='e';break;
	    default : s[i++]=*p;
	}
	p++;
    }
    s[i]=0;
    pRoom->pueblo_picture=str_dup(s);
    return TRUE;
}

bool redit_property( CHAR_DATA *ch, char *argument) {
    ROOM_INDEX_DATA *pRoom;
    char stype[MAX_STRING_LENGTH];
    char key[MAX_STRING_LENGTH];
    char *svalue;
    long l;
    int  i;
    bool b,delete;
    char c;

    EDIT_ROOM(ch, pRoom);

    argument=one_argument(argument,key);
    argument=one_argument(argument,stype);
    svalue=argument;

    delete=str_cmp(svalue,"delete")?FALSE:TRUE;

    if (svalue==NULL||svalue[0]==0) {
        send_to_char("Usage: property <key> <type> <value>\n\r",ch);
        send_to_char("  Where <type> is \"bool\", \"int\", \"long\", \"char\","
                     "\"string\n\r",ch);
        return FALSE;
    }

    if (!does_property_exist_s(key,stype)) {
	send_to_char("Unknown property, see `{Wpropertylist{x` for overview.\n\r",ch);
	return FALSE;
    }

    switch (which_keyword(stype,"bool","int","long",
				"char","string",NULL)) {
        case 1:
	    if (delete)
		DeleteRoomProperty(pRoom,PROPERTY_BOOL,key);
	    else {
		switch (which_keyword(svalue,"true","false",NULL)) {
		    case 1:
			b=TRUE;
			break;
		    case 2:
			b=FALSE;
			break;
		    default:
			redit_property(ch,"");
			return FALSE;
		}
		SetRoomProperty(pRoom,PROPERTY_BOOL,key,&b);
	    }
            break;
        case 2:
	    if (delete)
		DeleteRoomProperty(pRoom,PROPERTY_INT,key);
	    else {
		i=atoi(svalue);
		SetRoomProperty(pRoom,PROPERTY_INT,key,&i);
	    }
            break;
        case 3:
	    if (delete)
		DeleteRoomProperty(pRoom,PROPERTY_LONG,key);
	    else {
		l=atol(svalue);
		SetRoomProperty(pRoom,PROPERTY_LONG,key,&l);
	    }
            break;
        case 4:
	    if (delete)
		DeleteRoomProperty(pRoom,PROPERTY_CHAR,key);
	    else {
		c=svalue[0];
		SetRoomProperty(pRoom,PROPERTY_CHAR,key,&c);
	    }
            break;
        case 5:
	    if (delete)
		DeleteRoomProperty(pRoom,PROPERTY_STRING,key);
	    else
		SetRoomProperty(pRoom,PROPERTY_STRING,key,svalue);
            break;
        default:
            redit_property(ch,"");
            return FALSE;
    }

    return TRUE;
}

bool redit_proginstance( CHAR_DATA *ch, char *argument )
{
    ROOM_INDEX_DATA *pRoom;
    int vnum;

    EDIT_ROOM(ch, pRoom);

    if ( argument[0] == '\0' || !is_number( argument ) )
    {
	send_to_char( "Syntax:  proginstance [vnum]\n\r", ch );
	return FALSE;
    }

    vnum = atoi( argument );

    if(vnum==0)
    {
      pRoom->rprog_vnum=0;
      send_to_char("Roomprog removed.\n\r", ch);
      return TRUE;
    }

    if(!get_rprog_index(vnum)) {
      sprintf_to_char(ch,"Room program with vnum %d does not exist.\n\r",vnum);
      return FALSE;
    }

    pRoom->rprog_vnum=vnum;

    send_to_char( "Proginstance set.\n\r", ch);
    return TRUE;
}
