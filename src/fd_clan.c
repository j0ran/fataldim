//
// $Id: fd_clan.c,v 1.47 2008/03/23 21:57:54 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"
#include "db.h"

CLAN_INFO *		clan_list=NULL;
CLANMEMBER_TYPE *	clanmember_list=NULL;
RANK_INFO *		rank_list=NULL;

char *rank_name[MAX_CLAN_RANK]={
    "Member",
    "Elite",
    "Master",
    "Knight",
    "Leader"
};

char *rank_who[MAX_CLAN_RANK]={
    "MEM",
    "ELI",
    "MAS",
    "KNI",
    "LEA"
};


void addtoclan(CLAN_INFO *pClan,CLANMEMBER_TYPE *pMember) {
    char plr_filename[MSL];
    struct stat dummy;
    int res;

    snprintf(plr_filename,MSL,"%s/%s",mud_data.player_dir,capitalize(pMember->playername));   
    res=stat(plr_filename,&dummy);
    if (res) {
      if (errno==ENOENT) {
	  // player does not exist
	  bugf("addtoclan(): %s removed from clan %s, because player doesn't exist",pMember->playername,pClan->name);
	  free_clanmember(pMember);
	  return;
      }
      bugf("%s while checking %s",strerror(errno),pMember->playername);
    }

    if (pClan->members==NULL) {
	pClan->members=pMember;
	pMember->next_member=NULL;
    } else {
	pMember->next_member=pClan->members;
	pClan->members=pMember;
    }
}

void load_clan(FILE *fp)
{
    CLAN_INFO *pClan;
    CLANMEMBER_TYPE *pMember;
    char      *word;
    bool      fMatch;

    pClan               = new_clan();

    for ( ; ; )
    {
	word   = feof( fp ) ? "End" : fread_word( fp );
	fMatch = FALSE;

	switch ( UPPER(word[0]) ) {
	case 'C':
	    if(!str_cmp(word,"Conq"))
	    {
		fread_number( fp );
		fread_number( fp );
		fMatch = TRUE;
		break;
	    }
	    KEY( "Coins", pClan->coins , fread_number( fp ) );
	    break;
	case 'D':
	    if(!str_cmp(word,"Description"))
	    {
		pClan->description=fread_string( fp );
		fMatch = TRUE;
	    } else if (!str_cmp(word,"Dele")) {
		pClan->deleted=fread_number(fp)?TRUE:FALSE;
		fMatch = TRUE;
	    }
	    break;
	case 'E':
	    if ( !str_cmp( word, "End" ) )
	    {
		fMatch = TRUE;
		if ( clan_list == NULL )
		    clan_list = pClan;
		else {
		    pClan->next = clan_list;
		    clan_list = pClan;
		}
		return;
	    }
	    if(!str_cmp(word,"Elite"))
	    {
		pMember			= new_clanmember();
		pMember->playername	= str_dup(fread_word(fp));
		pMember->lastonline	= fread_long(fp);
		pMember->rank_info	= get_rank_by_name("Elite");
		pMember->clan_info	= pClan;

		addtoclan(pClan,pMember);

		fMatch = TRUE;
	    }
	    break;
	case 'H':
	    KEY( "Hall", pClan->hall , fread_number( fp ) );
	    break;
	case 'I':
	    KEY( "Independant", pClan->independent , fread_number( fp ) );
	    break;
	case 'K':
	    if(!str_cmp(word,"Knight"))
	    {
		pMember			= new_clanmember();
		pMember->playername	= str_dup(fread_word(fp));
		pMember->lastonline	= fread_long(fp);
		pMember->rank_info	= get_rank_by_name("Knight");
		pMember->clan_info	= pClan;

		addtoclan(pClan,pMember);

		fMatch = TRUE;
	    }
	    break;
	case 'L':
	    if(!str_cmp(word,"Leader"))
	    {
		pMember			= new_clanmember();
		pMember->playername	= str_dup(fread_word(fp));
		pMember->lastonline	= fread_long(fp);
		pMember->rank_info	= get_rank_by_name("Leader");
		pMember->clan_info	= pClan;

		addtoclan(pClan,pMember);

		fMatch = TRUE;
	    }
	    break;
	case 'M':
	    if(!str_cmp(word,"Master"))
	    {
		pMember			= new_clanmember();
		pMember->playername	= str_dup(fread_word(fp));
		pMember->lastonline	= fread_long(fp);
		pMember->rank_info	= get_rank_by_name("Master");
		pMember->clan_info	= pClan;

		addtoclan(pClan,pMember);

		fMatch = TRUE;
		break;
	    }
	    if(!str_cmp(word,"Member"))
	    {
		pMember			= new_clanmember();
		pMember->playername	= str_dup(fread_word(fp));
		pMember->lastonline	= fread_long(fp);
		pMember->rank_info	= get_rank_by_name("Member");
		pMember->clan_info	= pClan;

		addtoclan(pClan,pMember);

		fMatch = TRUE;
		break;
	    }
	    break;
	case 'N':
	    KEYS( "Name", pClan->name, fread_string(fp) );
            break;
	case 'R':
	    KEY( "Recall", pClan->recall , fread_number( fp ) );
	    break;
	case 'U':
	    KEYS("URL",pClan->url, fread_string(fp));
	    break;
	case 'W':
	    KEYS("Whoname",pClan->who_name, fread_string(fp));
	    break;
	}

	if (fMatch==FALSE) {
	    bugf("load_clan(): Unknown keyword not matched (%s)",word);
	}
    }
}

void load_clans(void)
{
    FILE *f;
    char *word;
    RANK_INFO *rank;
    int i;

    for (i=0;i<MAX_CLAN_RANK;i++) {
	rank=new_clanrank();

	rank->who_name=str_dup(rank_who[i]);
	rank->rank_name=str_dup(rank_name[i]);
	rank->rank=i;

	if (rank_list==NULL) {
	    rank_list=rank;
	    rank_list->next=NULL;
	} else {
	    rank->next=rank_list;
	    rank_list=rank;
	}
    }

    if ((f=fopen(mud_data.clan_file,"r"))==NULL) {
	bugf("Load_clans: Can not open %s, no clans available.",
	    mud_data.clan_file );
	logf("[0] load_clans(): fopen(%s): %s",mud_data.clan_file,ERROR);
	return;
    }

    for ( ; ; )
    {
	if( fread_letter( f ) != '#' )
	{
	    bugf( "Load_clans: # not found." );
	    exit( 1 );
	}

	word = fread_word( f );

	     if ( word[0] == '$' ) break;
	else if ( !strcmp( word, "CLAN" ) ) load_clan( f );
	else
	{
	    bugf("Load_clans: bad section name." );
	    exit( 1 );
	}
    }

    fclose(f);
}

void save_clans(void)
{
    FILE *f;
    CLAN_INFO *pClan;
    CLANMEMBER_TYPE *pMember;
    char renamefile[MIL];

    strcpy(renamefile,mud_data.clan_file);
    strcat(renamefile,"~");
    rename(mud_data.clan_file,renamefile);
    if ((f=fopen(mud_data.clan_file,"w"))==NULL) {
	bugf("Save_clans: Can not open %s for writing.", mud_data.clan_file );
	logf("[0] save_clans(): fopen(%s): %s",mud_data.clan_file,ERROR);
	return;
    }

    for (pClan=clan_list;pClan;pClan=pClan->next) {
	fprintf(f,"#CLAN\n");
	fprintf(f,"Name %s~\n",pClan->name);
	fprintf(f,"Whoname %s~\n",pClan->who_name);
	fprintf(f,"Hall %d\n",pClan->hall);
	fprintf(f,"Recall %d\n",pClan->recall);
	fprintf(f,"Dele %d\n",pClan->deleted?1:0);
	fprintf(f,"Independant %d\n",pClan->independent?1:0);
	fprintf(f,"Coins %ld\n",pClan->coins);
	for(pMember=pClan->members;pMember;pMember=pMember->next_member)
	    fprintf(f,"%s %s %d\n",pMember->rank_info->rank_name,pMember->playername,(int)pMember->lastonline);
//	for(pArea=pClan->conquered;pArea;pArea=pArea->next)
//	    fprintf(f,"Conq %d %d\n",pArea->obj_vnum,pArea->room_vnum);

	fprintf(f,"URL\n%s~\n",pClan->url);
	fprintf(f,"Description\n%s~\n",fix_string(pClan->description));
	fprintf(f,"End\n\n");
    }

    fprintf(f,"#$\n");

    fclose(f);
}

void do_loner( CHAR_DATA *ch, char *argument)
{
    if (IS_NPC(ch)) return;
    if (check_ordered(ch)) return;

    if(ch->clan) {
	send_to_char("You already have joined a clan.\n\r",ch);
	return;
    }

    if(ch->level<LOWER_CLAN_LEVEL || ch->level>UPPER_CLAN_LEVEL) {
	sprintf_to_char(ch,
	    "You can only become a loner if you are between level %d and %d.\n\r",
	    LOWER_CLAN_LEVEL,UPPER_CLAN_LEVEL);
	return;
    }

    ch->pcdata->clan_recruit=get_clan_by_name("Loner");

    send_to_char("You now have recruited yourself for the loner clan.\n\r"
		 "Type '{Wrecruit accept{x' to join the clan.\n\r"
		 "Type '{Wrecruit{x' to cancel the recruiting procedure.\n\r",
		 ch);
}

void do_recruit( CHAR_DATA *ch, char *argument)
{
    CHAR_DATA *victim;

    if (IS_NPC(ch)) return;
    if (check_ordered(ch)) return;

    if (!str_cmp(argument,"accept")) {
	if (ch->pcdata->clan_recruit==NULL) {
	    send_to_char("Sorry, but you are not being recruited.\n\r",ch);
	    return;
	}

	if (ch->clan) {
	    clan_removemember(ch->clan);
	    ch->clan=NULL;
	}
	clan_addmember(ch,ch->pcdata->clan_recruit,get_rank_by_name("Member"));

	act("You join the $t clan.",
	    ch,ch->clan->clan_info->name,NULL,TO_CHAR);
	act("$n joins the $t clan.",
	    ch,ch->clan->clan_info->name,NULL,TO_ROOM);
	logf("[%d] %s joins the %s clan",
	    GET_DESCRIPTOR(ch),ch->name,ch->clan->clan_info->name);

	ch->pcdata->clan_recruit=NULL;

	mortalcouncil_update(ch);

	return;
    }

    if(argument[0]=='\0')
    {
	if(ch->pcdata->clan_recruit==NULL)
	{
	    send_to_char("You are not being recruited for a clan.\n\r",ch);
	    return;
	}

	act("You refuse joining the $t clan.",ch,
	    ch->pcdata->clan_recruit->name,
	    NULL,TO_CHAR);
	if(ch->pcdata->clan_recruit==NULL)
	    act("$n refuses to join the $t clan.",ch,
	    	ch->pcdata->clan_recruit->name,
	    	NULL,TO_ROOM);

	ch->pcdata->clan_recruit=NULL;

	return;
    }

    if (ch->clan==NULL) {
	send_to_char("But you are no member of any clan.\n\r",ch);
	return;
    }

    if (ch->clan->clan_info==get_clan_by_name("Loner")) {
	send_to_char("You can't recruit members if you are a loner.\n\r",ch);
	return;
    }

   if (ch->clan->rank_info<get_rank_by_name("Master")) {
	send_to_char("You have a to low rank to recruit new members.\n\r",ch);
	return;
   }

    if( ( victim = get_char_room( ch, argument ) ) == NULL ) {
	send_to_char( "They aren't here.\n\r", ch );
	return;
    }

    if( IS_NPC( victim ) ) {
	send_to_char("You can't recruit mobs.\n\r", ch );
	return;
    }

    if (victim==ch) {
	send_to_char("You are already in your clan.\n\r",ch);
	return;
    }

    if (victim->clan==NULL) {
	act("$N isn't a loner!",ch,NULL,victim,TO_CHAR);
	return;
    }

    if (victim->clan->clan_info!=get_clan_by_name("Loner")) {
	act("$N is already a member of the $t clan.",ch,
	    victim->clan->clan_info->name,victim,TO_CHAR);
	return;
    }

    if (victim->pcdata->clan_recruit!=NULL) {
	act("$N is already being recruited for the $t clan.",ch,
	    victim->pcdata->clan_recruit->name,victim,TO_CHAR);
	return;
    }

    victim->pcdata->clan_recruit=ch->clan->clan_info;

    act("$n recruits $N to join the $t clan.",ch,
	ch->clan->clan_info->name,victim,TO_NOTVICT);
    act("You recruit $N to join your clan.",ch,NULL,victim,TO_CHAR);
    sprintf_to_char(victim,
	"You are being recruited by %s to join the %s clan.\n\r"
	"Type '{Wrecruit accept{x' to join the clan.\n\r"
	"Type '{Wrecruit{x' to cancel the request.\n\r",
	ch->name,ch->clan->clan_info->name);
}


void do_clan( CHAR_DATA *ch, char *argument )
{
    char arg1[MAX_INPUT_LENGTH];
    int areas_conquered;

    if(IS_NPC(ch)) return;

    argument=one_argument(argument,arg1);

    if(arg1[0]=='\0') goto showsyntax;

    if (!str_prefix(arg1,"save")) {
	send_to_char("Saving clans...\n\r",ch);
	save_clans();
	send_to_char("...done\n\r",ch);
	return;
    }

    if(!str_prefix(arg1,"list")) {
	CLAN_INFO *clan;
	AREA_DATA *area;

	send_to_char("Clan Name       Who Entry    Independend Area Owner\n\r",ch);
	send_to_char("--------------- ------------ ----------- ----------\n\r",ch);

	for (clan=clan_list;clan;clan=clan->next) {
	    char buf[MSL];

	    if (clan->deleted && !IS_IMMORTAL(ch))
		continue;

	    areas_conquered=0;
	    area=area_first;
	    while (area) {
		if (area->conquered_by==clan)
		    areas_conquered++;
		area=area->next;
	    }
	    if (areas_conquered)
		sprintf(buf,"%d area%s",
		    areas_conquered,areas_conquered!=1?"s":"");
	    else
		strcpy(buf,"-");

	    sprintf_to_char(ch,"%s%s %s%s %-11s %s %s\n\r",
		clan->name,spaces(15-str_len(clan->name)),
		clan->who_name,spaces(12-str_len(clan->who_name)),
		clan->independent?"Yes":"No",
		buf,
		clan->deleted?"{Rdeleted{x":""
		);
	}
	return;
    }

    if(!str_prefix(arg1,"info"))
    {
	CLAN_INFO *clan;
	CLANMEMBER_TYPE *member;
	int members;

	if(argument[0]=='\0') goto showsyntax;

	if ((clan=get_clan_by_name(argument))==NULL || clan->deleted) {
	    send_to_char("Unknown clan. Type '{Wclan list{x' for list.\n\r", ch );
	    return;
	}

        if(clan->independent)
	    members=0;
	else {
	    members=0;
	    for(member=clan->members;member;member=member->next_member)
		members++;
        }

	sprintf_to_char(ch,"Some information about clan %s:\n\r",clan->name);
	sprintf_to_char(ch,"Who Entry  : %s\n\r"
			   "Members    : %d\n\r"
			   "Indepenend : %s\n\r"
			   "Recall     : %s\n\r"
			   "Website    : %s\n\r"
			   "Description:\n\r",
			    clan->who_name,
			    members,
			    clan->independent?"Yes":"No",
			    clan->recall==ROOM_VNUM_TEMPLE?"No":"Yes",
			    clan->url);
	if(clan->description[0]=='\0') send_to_char("No description available.\n\r",ch);
	else send_to_char(clan->description,ch);
	return;
    }

    if(!str_prefix(arg1,"members")) {
	CLAN_INFO *clan;
	CLAN_INFO *lonerclan=get_clan_by_name("Loner");
	CLANMEMBER_TYPE *member;
	int member_count;

	if(argument[0]=='\0') goto showsyntax;

	if ((clan=get_clan_by_name(argument))==NULL) {
	    send_to_char("Unknown clan. Type '{Wclan list{x' for list.\n\r", ch );
	    return;
	}

	member_count=0;

	sprintf_to_char(ch,"Members of the %s clan:\n\r",clan->name);
	if(clan->members==NULL) {
	    send_to_char("none.\n\r",ch);
	} else {
	    int i,length;
	    for (i=0;i<MAX_CLAN_RANK;i++) {
		RANK_INFO *thisrank=get_rank_by_name(rank_name[i]);

		length=0;
		sprintf_to_char(ch,"{y%-6s{x: ",rank_name[i]);
		for(member=clan->members;member!=NULL;member=member->next_member) {
		    if (thisrank==member->rank_info) {
			if (length+strlen(member->playername)>70) {
			    send_to_char("\n\r        ",ch);
			    length=0;
			}
			length+=strlen(member->playername)+1;
			if (time(NULL)-member->lastonline<14*24*3600)
			    sprintf_to_char(ch,"{W%s{x ",member->playername);
			else if (time(NULL)-member->lastonline>365*24*3600)
			    sprintf_to_char(ch,"{r%s{x ",member->playername);
			else
			    sprintf_to_char(ch,"%s ",member->playername);
			member_count++;
		    }
		}
		send_to_char("\n\r",ch);
		if (clan==lonerclan) break;
	    }
	    send_to_char("\n\r",ch);
	}

	sprintf_to_char(ch,"There %s %d member%s in the clan.\n\r",
	    member_count==1?"is":"are",
	    member_count,
	    member_count==1?"":"s");

	return;
    }

    if(!str_prefix(arg1,"area"))
    {
        CLAN_INFO *clan;
        AREA_DATA *pArea;
        bool found=FALSE;

        if(argument[0]=='\0') {
	    AREA_DATA *pArea;

	    send_to_char("Conquerable areas:\n\r\n\r",ch);

	    for (pArea=area_first;pArea!=NULL;pArea=pArea->next)
		if (IS_SET(pArea->area_flags,AREA_CONQUEST) &&
		    !IS_SET(pArea->area_flags,AREA_UNFINISHED))
		    sprintf_to_char(ch,"%s\n\r",pArea->name);
	} else {
	    if ((clan=get_clan_by_name(argument))==NULL) {
		send_to_char("Unknown clan. Type '{Wclan list{x' for list.\n\r", ch );
		return;
	    }

	    for(pArea=area_first;pArea;pArea=pArea->next) {
		if(pArea->conquered_by==clan) {
		    if(!found) send_to_char("Conquered areas:\n\r",ch);
		    sprintf_to_char(ch,"%s\n\r",pArea->name);
		    found=TRUE;
		}
	    }
	    if(!found)
		send_to_char("This clan has no areas conquered.\n\r",ch);
	}

        return;
    }


showsyntax:
    send_to_char("Syntax: clan area <clan>\n\r"
    	     "        clan info <clan>\n\r"
	     "        clan list\n\r"
	     "        clan members <clan>\n\r",
	ch );

    if (IS_IMMORTAL(ch))
	send_to_char("        clan save\n\r",ch);
}

void do_abandon( CHAR_DATA *ch, char *argument )
{
    if (IS_NPC(ch)) return;
    if (check_ordered(ch)) return;

    if(str_cmp("abandon",argument))
    {
	send_to_char("Type '{Wabandon abandon{x' to abandon your clan.\n\r",ch);
	return;
    }

    if(ch->clan==NULL)
    {
	send_to_char("But you are no member of a clan.\n\r",ch);
	return;
    }

    if(ch->clan->clan_info==get_clan_by_name("Loner")) {
	send_to_char("You can't abandon being a loner. You will have to live with it.\n\r", ch);
	return;
    }

    send_to_char("You have abandoned your clan.\n\r", ch);
    act("$n has abandoned the $T clan.",ch,NULL,ch->clan->clan_info->name,TO_ROOM);
    logf("[%d] %s abandoned the %s clan",GET_DESCRIPTOR(ch),ch->name,ch->clan->clan_info->name);

    clan_removemember(ch->clan);
    ch->clan=NULL;

    mortalcouncil_update(ch);

    return;
}

void do_rank( CHAR_DATA *ch, char *argument )
{
    char arg1[MAX_INPUT_LENGTH];
    CLANMEMBER_TYPE *victim;
    RANK_INFO *rank;

    if(IS_NPC(ch)) return;

    argument=one_argument(argument,arg1);

    if (ch->clan==NULL)
    {
	send_to_char("But you are no member of a clan.\n\r",ch);
	return;
    }

    if (ch->clan->clan_info==get_clan_by_name("Loner")) {
	send_to_char("You can't rank people if you are a loner.\n\r", ch);
	return;
    }

    if (ch->clan->rank_info<get_rank_by_name("Knight")) {
	send_to_char("You don't have a high enough rank to rank other members.\n\r",ch);
	return;
    }

    if (arg1[0]=='\0' || argument[0]=='\0') {
	send_to_char("Syntax: rank <player> <rank>\n\rwhere rank is\n\r",ch);
	for (rank=rank_list;rank;rank=rank->next)
	    sprintf_to_char(ch,"%s\n\r",rank->rank_name);
	return;
    }

    if (( victim = get_clanmember_by_charname( arg1 ) ) == NULL ) {
	send_to_char( "They aren't here.\n\r", ch );
	return;
    }

    if ( victim->clan_info!=ch->clan->clan_info) {
	sprintf_to_char(ch,"%s isn't in your clan.\n\r",victim);
	return;
    }

    if ((rank=get_rank_by_name(argument))==NULL) {
	send_to_char("Rank must be one of the following:\n\r",ch);
	for (rank=rank_list;rank;rank=rank->next)
	    sprintf_to_char(ch,"%s\n\r",rank->rank_name);
	return;
    }

    if (ch->clan->rank_info!=get_rank_by_name("Leader")) {
	if (ch->clan->rank_info->rank<=rank->rank ||
	    ch->clan->rank_info->rank<=victim->rank_info->rank) {
	    send_to_char("Your rank is not high enough to change victims rank.\n\r",ch);
	    return;
	}
    }

    if (rank==victim->rank_info) {
	act("$N already has that rank.",ch,NULL,victim->player,TO_CHAR);
	return;
    }

    victim->rank_info=rank;
    logf("[%d] %s has ranked %s to %s",GET_DESCRIPTOR(ch),ch->name,victim->playername,victim->rank_info->rank_name);


    if (victim->player) {
	act("You are now a $t of the $T clan.",
	    victim->player,
	    victim->rank_info->rank_name,
	    victim->clan_info->name,TO_CHAR);
	mortalcouncil_update(victim->player);
    }
    sprintf(arg1,"%s is now a %s of the %s clan.",
	victim->playername,
	victim->rank_info->rank_name,
	victim->clan_info->name);
    act(arg1,ch,NULL,NULL,TO_CHAR);
}

void do_invite(CHAR_DATA *ch, char *argument) {
    CHAR_DATA *victim;
    EFFECT_DATA ef;
    char arg1[MIL],arg2[MIL];
    int duration;

    if(IS_NPC(ch)) return;

    if(ch->clan==NULL) {
	send_to_char("But you are no member of a clan.\n\r",ch);
	return;
    }

    if(ch->clan->clan_info==get_clan_by_name("Loner")) {
	send_to_char("You can't invite people if you are a loner.\n\r", ch);
	return;
    }

    if (ch->clan->rank_info!=get_rank_by_name("Knight") &&
       ch->clan->rank_info!=get_rank_by_name("Leader")) {
	send_to_char(
	    "You don't have a high enough rank to invite people.\n\r",ch);
	return;
    }

    argument=one_argument(argument,arg1);
    argument=one_argument(argument,arg2);

    if(arg1[0]=='\0') {
	send_to_char("Who do you want to invite and for how long?\n\r",ch);
	return;
    }

    if( ( victim = get_char_room( ch, arg1 ) ) == NULL ) {
	send_to_char("That person isn't here.\n\r",ch);
	return;
    }

    if (victim->clan &&
	victim->clan->clan_info == ch->clan->clan_info) {
	act("You invite $M gracefully in $S own clan-hall.",
	    ch,NULL,victim,TO_CHAR);
	return;
    }

    if (IS_NPC(victim)) {
	send_to_char("You can only invite players.\n\r",ch);
	return;
    }

    if (victim==ch) {
	send_to_char(
	    "You invite yourself to visit the euh... forget about it.\n\r",ch);
	return;
    }

    duration=6;
    if (arg2[0]) {
	if (!is_number(arg2)) {
	    do_invite(ch,"");
	    return;
	}
	duration=atoi(arg2);
	if (duration<1) {
	    act("For how long do you want to invite $M?",ch,NULL,victim,TO_CHAR);
	    return;
	}
	if (duration>100) {
	    send_to_char("That's a veeeery long time.",ch);
	    return;
	}
    }

    if (IS_AFFECTED(victim,EFF_INVITED))
	effect_strip(victim,gsn_invitation);

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where	= TO_EFFECTS;
    ef.type	= gsn_invitation;
    ef.level	= 0;
    ef.duration	= duration;
    ef.modifier	= APPLY_NONE;
    ef.modifier = 0;
    ef.bitvector= EFF_INVITED;
    ef.casted_by= ch?ch->id:0;
    effect_to_char(victim,&ef);

    SetCharProperty(victim,PROPERTY_STRING,"invited_for",
	ch->clan->clan_info->name);
    SetCharProperty(victim,PROPERTY_STRING,"invited_by",ch->name);

    act("You have invited $N to visit your clanhalls.",ch,NULL,victim,TO_CHAR);
    act("$n has invited you to visit $s clanhalls.",ch,NULL,victim,TO_VICT);
}

void do_exile( CHAR_DATA *ch, char *argument )
{
    CLANMEMBER_TYPE *victim;
    char arg[MSL];

    if (IS_NPC(ch)) return;
    if (check_ordered(ch)) return;

    if(ch->clan==NULL) {
	send_to_char("But you are no member of a clan.\n\r",ch);
	return;
    }

    if(ch->clan->clan_info==get_clan_by_name("Loner")) {
	send_to_char("You can't exile people if you are a loner.\n\r", ch);
	return;
    }

    one_argument(argument,arg);
    if (arg[0]==0) {
	send_to_char("Exile who?\n\r",ch);
	return;
    }

    if ((victim=get_clanmember_by_charname(arg))==NULL) {
	sprintf_to_char(ch,"%s is not in a clan.\n\r",arg);
	return;
    }

    if (victim->clan_info!=ch->clan->clan_info) {
	sprintf_to_char(ch,"%s is not in your clan.\n\r",arg);
	return;
    }

    if (ch->clan->rank_info!=get_rank_by_name("Leader") &&
	ch->clan->rank_info!=get_rank_by_name("Knight")) {
	    send_to_char(
		"You don't have a high enough rank to exile members.\n\r",ch);
	return;
    }

    if (argument[0]=='\0') {
	send_to_char("Syntax: exile <player>\n\r",ch);
	return;
    }

    if (victim->player)
	act("You have been exiled from the $T clan",victim->player,NULL,
		victim->clan_info->name,TO_CHAR);
    act("$t is exiled from the $T clan.",ch,victim->playername,
	    victim->clan_info->name,TO_CHAR);
    act("$t is exiled from the $T clan.",ch,victim->playername,
	    victim->clan_info->name,TO_NOTVICT);

    clan_removemember(victim);
    if (victim->player) {
	victim->player->clan=NULL;
	mortalcouncil_update(victim->player);
    }

    return;
}


void do_install( CHAR_DATA *ch, char *argument )
{
    OBJ_DATA *obj;
    OBJ_DATA *prev_flag=NULL;
    CLAN_INFO *pClan;
    AREA_DATA *pArea;
    ROOM_INDEX_DATA *pRoom;
    int vnum;
    char s[MAX_STRING_LENGTH],buf[MAX_STRING_LENGTH];

    if(IS_NPC(ch)) return;

    if( ch->clan==NULL )
    {
        send_to_char(
	    "You can only conquere area's if you are in a clan.\n\r", ch );
        return;
    }

    if( ( obj=get_obj_carry( ch, argument, ch ) ) == NULL )
    {
        send_to_char( "You do not have that item.\n\r", ch );
        return;
    }

    if ( obj->item_type!=ITEM_CLAN )
    {
        send_to_char("You can only install special clan items.\n\r", ch );
        return;
    }

    if( !can_drop_obj( ch, obj ) )
    {
        send_to_char( "You can't let go of it.\n\r", ch );
        return;
    }

    pArea=ch->in_room->area;

    if(!IS_SET(pArea->area_flags,AREA_CONQUEST) ||
       IS_SET(pArea->area_flags,AREA_UNFINISHED))
    {
        send_to_char( "This area can not be conquered.\n\r", ch );
        return;
    }

    if (GetRoomProperty(ch->in_room,PROPERTY_STRING,"clan",s) ||
	GetRoomProperty(ch->in_room,PROPERTY_STRING,"owner",s)) {
        send_to_char("You may not install the flag in this room.\n\r",ch);
        return;
    }

    if (GetObjectProperty(obj,PROPERTY_STRING,"clanitem",s)==FALSE) {
	bugf("do_install(): unknown clan for clan-item (vnum %d): no clanitem property",obj->pIndexData->vnum);
	return;
    }

    if ((pClan=get_clan_by_name(s))==NULL) {
	bugf("do_install(): unknown clan (%s) for clan-item (vnum %d)",s,obj->pIndexData->vnum);
	return;
    }

    if (pArea->conquered_by)
	for (vnum=pArea->lvnum;vnum<=pArea->uvnum;vnum++) {
	    if ((pRoom=get_room_index(vnum))==NULL) continue;
	    for (prev_flag=pRoom->contents;prev_flag;prev_flag=prev_flag->next_content)
		if (IS_OBJ_STAT(prev_flag,ITEM_INSTALLED))
		    break;

	    if (prev_flag)
		break;
	}

    if (prev_flag) {
	if (pClan!=pArea->conquered_by) {
	    sprintf_to_char(ch,"This area is currently owned by %s.\n\r",pArea->conquered_by->name);
	    return;
	}
        STR_REMOVE_BIT(prev_flag->strbit_extra_flags,ITEM_INSTALLED);
    }

    obj_from_char( obj );
    obj_to_room( obj, ch->in_room );
    act( "$n installs $p.", ch, obj, NULL, TO_ROOM );
    act( "You install $p.", ch, obj, NULL, TO_CHAR );
    STR_SET_BIT(obj->strbit_extra_flags,ITEM_INSTALLED);
    if (IS_OBJ_STAT(obj,ITEM_MELT_DROP))
    {
        act("$p dissolves into smoke.",ch,obj,NULL,TO_ALL);
        extract_obj(obj);
        return;
    }

    if (pClan==pArea->conquered_by)
	// nothing left to do
	return;

    pArea->conquered_by=pClan;

    for(vnum=pArea->lvnum;vnum<=pArea->uvnum;vnum++)
    {
	if ((pRoom=get_room_index(vnum)) &&
	    GetRoomProperty(pRoom,PROPERTY_STRING,"clan",s))
	{
	    SetRoomProperty(pRoom,PROPERTY_STRING,"clan",pClan->name);
	}
    }
    SET_BIT(pArea->area_flags,AREA_CHANGED);

    sprintf(buf,"This area has been conquered by the %s.\n\r",pClan->name);
    send_to_zone(buf,pArea);
}


CLAN_INFO *get_clan_by_name(char *clanname) {
    CLAN_INFO *clan;

    for (clan=clan_list;clan;clan=clan->next)
	if (!str_prefix(clanname,clan->name))
	    return clan;
    return NULL;
}

CLAN_INFO *get_clan_by_char(CHAR_DATA *ch) {
    if (ch->clan==NULL)
	return NULL;
    return ch->clan->clan_info;
}

CLANMEMBER_TYPE *get_clanmember_by_charname(char *charname) {
    CLAN_INFO *clans;
    CLANMEMBER_TYPE *member;

    for (clans=clan_list;clans!=NULL;clans=clans->next) {
	for (member=clans->members;member!=NULL;member=member->next_member) {
	    if (str_cmp(charname,member->playername)==0)
		return member;
	}
    }
    return NULL;
}

RANK_INFO *get_rank_by_name(char *rankname) {
    RANK_INFO *rank;

    rank=rank_list;
    while (rank) {
	if (!str_prefix(rankname,rank->rank_name))
	    return rank;
	rank=rank->next;
    }
    return NULL;
}

RANK_INFO *get_rank_by_char(CHAR_DATA *ch) {
    if (ch->clan==NULL)
	return NULL;
    return ch->clan->rank_info;
}



void clan_addmember(CHAR_DATA *ch,CLAN_INFO *clan,RANK_INFO *rank) {
    CLANMEMBER_TYPE *clanmember;

    if (ch==NULL) {
	bugf("clan_addmember: ch=NULL");
	return;
    }
    if (ch->clan!=NULL) {
	bugf("clan_addmember: clan already defined for %s",NAME(ch));
	return;
    }
    clanmember=new_clanmember();
    clanmember->player=ch;
    clanmember->playername=str_dup(ch->name);
    clanmember->lastonline=time(NULL);

    clanmember->clan_info=clan;
    clanmember->rank_info=rank;

    clanmember->next_member=clan->members;
    clan->members=clanmember;
    ch->clan=clanmember;
}

void clan_removemember(CLANMEMBER_TYPE *cm) {
    CLANMEMBER_TYPE *member,*prev_member;
    CLAN_INFO *clan;
    CHAR_DATA *ch;

    if (cm==NULL) {
	bugf("clan_addmember: cm=NULL");
	return;
    }
    ch=cm->player;

    clan=cm->clan_info;

    if (clan->members==cm) {
	clan->members=clan->members->next_member;
	free_clanmember(cm);
    } else {
	member=clan->members;
	prev_member=member;
	while (member) {
	    member=member->next_member;
	    if (member==cm)
		break;
	    prev_member=member;
	}
	prev_member->next_member=member->next_member;
	free_clanmember(member);
    }

    cm->clan_info=NULL;
    cm->rank_info=NULL;
    if (ch)
	ch->clan=NULL;
}
