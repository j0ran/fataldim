//
// $Id: fd_memory.c,v 1.15 2006/08/25 09:43:20 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"
#include "db.h"

// Magic number for memory allocation
#define MAGIC_NUM	19701231

void *		rgFreeList	[MAX_MEM_LIST];
const int	rgSizeList	[MAX_MEM_LIST]  =
	    { 16, 32, 64, 128, 256, 1024, 2048, 4096, 8192, 16384, 32768-64 };
int		rgCountList     [MAX_MEM_LIST];
int		nAllocString;
int		sAllocString;
int		hAllocString;
int		mAllocString;
int		nAllocPerm;
int		sAllocPerm;

void get_memory_overview(BUFFER *buf) {
    char s[MSL];

    sprintf(s,"Areas            : %5d allocated, %5d in use\n\r",
			area_allocated,area_inuse);		add_buf(buf,s);
    sprintf(s,"Mobiles / Chars\n\r");				add_buf(buf,s);
    sprintf(s,"- defined        : %5d\n\r",
			mob_index_allocated);			add_buf(buf,s);
    sprintf(s,"- in use         : %5d allocated, %5d in use\n\r",
			char_allocated,char_inuse);		add_buf(buf,s);
    sprintf(s,"- pcdata         : %5d allocated, %5d in use\n\r",
			pcdata_allocated,pcdata_inuse);		add_buf(buf,s);
    sprintf(s,"- gendata        : %5d allocated, %5d in use\n\r",
			gen_data_allocated,gen_data_inuse);	add_buf(buf,s);
    sprintf(s,"Objects\n\r");					add_buf(buf,s);
    sprintf(s,"- defined        : %5d\n\r",
			obj_index_allocated);			add_buf(buf,s);
    sprintf(s,"- in use         : %5d allocated, %5d in use\n\r",
			obj_allocated,obj_inuse);		add_buf(buf,s);
    sprintf(s,"Resets           : %5d allocated, %5d in use\n\r",
			reset_allocated,reset_inuse);		add_buf(buf,s);
    sprintf(s,"Rooms            : %5d allocated, %5d in use\n\r",
			room_index_allocated,room_index_inuse);	add_buf(buf,s);
    sprintf(s,"- Exits          : %5d allocated, %5d in use\n\r",
			exit_allocated,exit_inuse);		add_buf(buf,s);
    sprintf(s,"- Shops          : %5d allocated, %5d in use\n\r",
			shop_allocated,shop_inuse);		add_buf(buf,s);
    sprintf(s,"Clans\n\r");					add_buf(buf,s);
    sprintf(s,"- Clans          : %5d allocated, %5d in use\n\r",
			clan_allocated,clan_inuse);		add_buf(buf,s);
    sprintf(s,"- Ranks          : %5d allocated, %5d in use\n\r",
			clanrank_allocated,clanrank_inuse);	add_buf(buf,s);
    sprintf(s,"- Members        : %5d allocated, %5d in use\n\r",
			clanmember_allocated,clanmember_inuse);	add_buf(buf,s);
    sprintf(s,"Programming\n\r");				add_buf(buf,s);
    sprintf(s,"- mobmem         : %5d allocated, %5d in use\n\r",
			mobmem_allocated,mobmem_inuse);		add_buf(buf,s);
    sprintf(s,"- objprog-list   : %5d allocated, %5d in use\n\r",
			oprog_list_allocated,oprog_list_inuse);	add_buf(buf,s);
    sprintf(s,"- objprog-code   : %5d allocated, %5d in use\n\r",
			oprog_code_index_allocated,oprog_code_index_inuse);
								add_buf(buf,s);
    sprintf(s,"- mobprog-list   : %5d allocated, %5d in use\n\r",
			mprog_list_allocated,mprog_list_inuse);	add_buf(buf,s);
    sprintf(s,"- mobprog-code   : %5d allocated, %5d in use\n\r",
			mprog_code_index_allocated,mprog_code_index_inuse);
								add_buf(buf,s);
    sprintf(s,"- roomprog-list  : %5d allocated, %5d in use\n\r",
			rprog_list_allocated,rprog_list_inuse);	add_buf(buf,s);
    sprintf(s,"- roomprog-code  : %5d allocated, %5d in use\n\r",
			rprog_code_index_allocated,rprog_code_index_inuse);
								add_buf(buf,s);
    sprintf(s,"- areaprog-list  : %5d allocated, %5d in use\n\r",
			aprog_list_allocated,aprog_list_inuse);	add_buf(buf,s);
    sprintf(s,"- areaprog-code  : %5d allocated, %5d in use\n\r",
			aprog_code_index_allocated,aprog_code_index_inuse);
								add_buf(buf,s);
    sprintf(s,"Properties\n\r");				add_buf(buf,s);
    sprintf(s,"- defined        : %5d allocated, %5d in use\n\r",
			property_index_allocated,property_index_inuse);
								add_buf(buf,s);
    sprintf(s,"- in use         : %5d allocated, %5d in use\n\r",
			property_allocated,property_inuse);	add_buf(buf,s);
    sprintf(s,"Misc\n\r");					add_buf(buf,s);
    sprintf(s,"- Events         : %5d allocated, %5d in use\n\r",
			event_allocated,event_inuse);		add_buf(buf,s);
    sprintf(s,"- Socials        : %5d allocated, %5d in use\n\r",
			social_allocated,social_inuse);		add_buf(buf,s);
    sprintf(s,"- Helps          : %5d allocated, %5d in use\n\r",
			help_allocated,help_inuse);		add_buf(buf,s);
    sprintf(s,"- Effects        : %5d allocated, %5d in use\n\r",
			effect_allocated,effect_inuse);		add_buf(buf,s);
    sprintf(s,"- Aliases        : %5d allocated, %5d in use\n\r",
			alias_allocated,alias_inuse);		add_buf(buf,s);
    sprintf(s,"- ExtraDescr     : %5d allocated, %5d in use\n\r",
			extra_descr_allocated,extra_descr_inuse);add_buf(buf,s);
    sprintf(s,"- Notes          : %5d allocated, %5d in use\n\r",
			note_allocated,note_inuse);		add_buf(buf,s);
#ifdef HAS_POP3
    sprintf(s,"- Noterefs       : %5d allocated, %5d in use\n\r",
			note_ref_allocated,note_ref_inuse);	add_buf(buf,s);
#endif
    sprintf(s,"- Notebooks      : %5d allocated, %5d in use\n\r",
			notebook_allocated,notebook_inuse);	add_buf(buf,s);
    sprintf(s,"- Filesystem     : %5d allocated, %5d in use\n\r",
			fsentry_allocated,fsentry_inuse);	add_buf(buf,s);
    sprintf(s,"- Bans           : %5d allocated, %5d in use\n\r",
			ban_allocated,ban_inuse);		add_buf(buf,s);
    sprintf(s,"- Lockouts       : %5d allocated, %5d in use\n\r",
			lockout_allocated,lockout_inuse);	add_buf(buf,s);
    sprintf(s,"- Dreams         : %5d allocated, %5d in use\n\r",
			dream_allocated,dream_inuse);		add_buf(buf,s);
    sprintf(s,"- Bad names     : %5d allocated, %5d in use\n\r",
			badname_allocated,badname_inuse);	add_buf(buf,s);
    sprintf(s,"Static string-space\n\r");			add_buf(buf,s);
    sprintf(s,"- total          : %5d strings\n\r",
			nAllocString);				add_buf(buf,s);
    sprintf(s,"- hits           : %5d times\n\r",hAllocString);	add_buf(buf,s);
    sprintf(s,"- memory         : %7d bytes\n\r",sAllocString);	add_buf(buf,s);
    sprintf(s,"- maximum        : %7d bytes\n\r",mAllocString);	add_buf(buf,s);
    sprintf(s,"-   after reboot : %7d bytes\n\r",mud_data.max_string);	add_buf(buf,s);
    sprintf(s,"Permanents\n\r");				add_buf(buf,s);
    sprintf(s,"- total          : %5d objects\n\r",nAllocPerm);	add_buf(buf,s);
    sprintf(s,"- memory         : %7d bytes\n\r",sAllocPerm);	add_buf(buf,s);
}

void do_memory( CHAR_DATA *ch, char *argument ) {
    BUFFER *buf;

    buf=new_buf();
    get_memory_overview(buf);
    page_to_char(buf_string(buf),ch);
    free_buf(buf);
}


char *	string_hash[MAX_KEY_HASH];
char *	string_space;
char *	top_string;

void new_string_space(void) {
    if ((string_space=calloc(1,mud_data.max_string))==NULL) {
	bugf("new_string_space(): can't calloc %d string-space.",mAllocString);
	abort();
    }
    top_string=string_space;
    nAllocString=0;
    hAllocString=0;
    sAllocString=0;
    mAllocString=mud_data.max_string;
}

//
// Allocate a permanent string. Put it in a hash-table. If people ask for
// re-allocation of a string which looks about the same, give the old one.
// It will be here forever anyway.
//
char *allocate_perm_string(const char *string) {
    int		iHash;
    char *	pHash;
    char *	pHashPrev;
    char *	pString;
    char *	plast;
    int		stringlenplusone;
    int		stringlen;

    union {
	 char *	pc;
	 char	rgc[sizeof(char *)];
    } u1;

    plast=top_string+sizeof(char *);
    if (plast+MAX_STRING_LENGTH>string_space+mAllocString) {
	bugf("allocate_perm_string(): MAX_STRING %d exceeded.",mAllocString);
	abort();
    }
    {
	int left=string_space+mAllocString-top_string;
	static time_t last_warn=0; // one warning per 10 minutes is enough, really.
	if (left<(65536*2) && (last_warn+600<time(NULL))) {
	    bugf("Low on permanent string space. Only %d bytes left.",left);
	    last_warn=time(NULL);
	    // if mud_data.max_string is unchanged increase it 
	    // to have at least 64kB free and just for the fun
	    // of it make it n*64k
	    if (mAllocString==mud_data.max_string) {
		mud_data.max_string=(mud_data.max_string & ~65535)+2*65536;
		save_config();
		bugf("Setting new value to %d for next reboot",mud_data.max_string);
	    }
	}
    }

    stringlen=strlen(string);
    stringlenplusone=stringlen+1;
    memcpy(plast,string,stringlenplusone);

    // stringlength is the hash-entry
    iHash=UMIN(MAX_KEY_HASH-1,stringlen);
    for (pHash=string_hash[iHash]; pHash!=NULL; pHash=pHashPrev ) {
	memcpy(u1.rgc,pHash,sizeof(char *));
	pHashPrev = u1.pc;
	pHash    += sizeof(char *);

	// if this string already exists, return that one.
	if (top_string[sizeof(char *)]==pHash[0]
	&&  !strcmp(top_string+sizeof(char *)+1,pHash+1)) {
//	    printf("*");
//	    fflush(stdout);
	    if (fBootDb) hAllocString++;
	    return pHash;
	}
    }

    if (!fBootDb) {
//	printf("-");
//	fflush(stdout);
	return str_dup(string);
    }

//  return str_dup(string);

    pString=top_string;
    top_string=plast+stringlenplusone;
    u1.pc=string_hash[iHash];
    memcpy(pString,u1.rgc,sizeof(char *));
    string_hash[iHash]=pString;

    nAllocString++;
    sAllocString+=top_string-pString;
//printf("%5d %5d %5d\n",nAllocString,sAllocString,&string_space[mAllocString]-top_string);
//    printf(".");
//    fflush(stdout);
    return pString+sizeof(char *);
}

/*
 * Allocate some ordinary memory,
 *   with the expectation of freeing it someday.
 */
void *alloc_mem( int sMem )
{
    char *pMem;
    int *magic;
    int iList;

#ifdef DEBUG
    fprintf(stderr," alloc_mem: allocating %d bytes\n",sMem);
#endif

    sMem += sizeof(*magic);

    for ( iList = 0; iList < MAX_MEM_LIST; iList++ )
    {
        if ( sMem <= rgSizeList[iList] )
            break;
    }

    if ( iList == MAX_MEM_LIST )
    {
        bugf( "Alloc_mem: size %d too large.", sMem );
        abort();
    }

    if ( rgFreeList[iList] == NULL )
    {
        pMem              = alloc_perm( rgSizeList[iList] );
	rgCountList[iList]++;
    }
    else
    {
        pMem              = rgFreeList[iList];
        rgFreeList[iList] = * ((void **) rgFreeList[iList]);
    }

    magic = (int *) pMem;
    *magic = MAGIC_NUM;
    pMem += sizeof(*magic);

    return (void *)pMem;
}



/*
 * Free some memory.
 * Recycle it back onto the free list for blocks of that size.
 */
void free_mem( void *cMem, int sMem )
{
    int iList;
    int *magic;
    char *pMem=(char *)cMem;

#ifdef DEBUG
    fprintf(stderr,"  free_mem: freeing %d bytes\n",sMem);
#endif

    pMem -= sizeof(*magic);
    magic = (int *) pMem;

    if (*magic != MAGIC_NUM)
    {
        bugf("free_mem(): Attempt to recycle invalid memory of size %d.",sMem);
        bugf("%s",(char*) pMem + sizeof(*magic));
        return;
    }

    *magic = 0;
    sMem += sizeof(*magic);

    for ( iList = 0; iList < MAX_MEM_LIST; iList++ )
    {
        if ( sMem <= rgSizeList[iList] )
            break;
    }

    if ( iList == MAX_MEM_LIST )
    {
        bugf( "Free_mem: size %d too large.", sMem );
        abort();
    }

    * ((void **) pMem) = rgFreeList[iList];
    rgFreeList[iList]  = pMem;

    return;
}


/*
 * Allocate some permanent memory.
 * Permanent memory is never freed,
 *   pointers into it may be copied safely.
 */
void *alloc_perm( int sMem )
{
    static char *pMemPerm;
    static int iMemPerm;
    void *pMem;

#ifdef DEBUG
    fprintf(stderr,"alloc_perm: allocating %d bytes\n",sMem);
#endif

    while ( sMem % sizeof(long) != 0 )
	sMem++;
    if ( sMem > MAX_PERM_BLOCK )
    {
	bugf( "alloc_perm(): %d too large.", sMem );
	abort();
    }

    if ( pMemPerm == NULL || iMemPerm + sMem > MAX_PERM_BLOCK )
    {
	iMemPerm = 0;
	if ( ( pMemPerm = calloc( 1, MAX_PERM_BLOCK ) ) == NULL )
	{
	    logf("[0] alloc_perm: %s",ERROR);
	    abort();
	}
    }

    pMem        = pMemPerm + iMemPerm;
    iMemPerm   += sMem;
    nAllocPerm++;
    sAllocPerm += sMem;
    return pMem;
}



/*
 * Duplicate a string into dynamic memory.
 * Fread_strings are read-only and shared.
 */
char *str_dup( const char *str )
{
    char *str_new;

    if ( str == NULL )
	return &str_empty[0];

    if ( str[0] == '\0' )
	return &str_empty[0];

    if ( str >= string_space && str < top_string )
	return (char *) str;

    str_new = alloc_mem( strlen(str) + 1 );
    strcpy( str_new, str );
    return str_new;
}



/*
 * Free a string.
 * Null is legal here to simplify callers.
 * Read-only shared strings are not touched.
 */
void free_string( char *pstr )
{
    if ( pstr == NULL
    ||   pstr == &str_empty[0]
    || ( pstr >= string_space && pstr < top_string ) )
	return;

    free_mem( pstr, strlen(pstr) + 1 );
    return;
}
