/* $Id: olc.h,v 1.47 2008/05/01 19:21:42 jodocus Exp $ */
/***************************************************************************
 *  File: olc.h                                                            *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 *                                                                         *
 *  This code was freely distributed with the The Isles 1.1 source code,   *
 *  and has been used here for OLC - OLC would not be what it is without   *
 *  all the previous coders who released their source code.                *
 *                                                                         *
 ***************************************************************************/
/*
 * This is a header file for all the OLC files.  Feel free to copy it into
 * merc.h if you wish.  Many of these routines may be handy elsewhere in
 * the code.  -Jason Dinkel
 */



/*
 * The version info.  Please use this info when reporting bugs.
 * It is displayed in the game by typing 'version' while editing.
 * Do not remove these from the code - by request of Jason Dinkel
 */
#define VERSION	"ILAB Online Creation [Beta 1.1]"
#define AUTHOR	"     By Jason(jdinkel@mines.colorado.edu)"
#define DATE	"     (May. 15, 1995)"
#define CREDITS "     Original by Surreality(cxw197@psu.edu) and Locke(locke@lm.com)"



/*
 * New typedefs.
 */
typedef	bool OLC_FUN		( CHAR_DATA *ch, char *argument );
#define DECLARE_OLC_FUN( fun )	OLC_FUN    fun



/*
 * Connected states for editor.
 */
#define ED_AREA		1
#define ED_ROOM		2
#define ED_OBJECT	3
#define ED_MOBILE	4
#define ED_MOBPROG	5
#define ED_HELP		6
#define ED_SOCIAL	7
#define ED_PROPERTY	8
#define ED_OBJPROG	9
#define ED_ROOMPROG	10
#define ED_RESET	11
#define ED_AREAPROG	12



/*
 * Interpreter Prototypes
 */
bool    aedit           ( CHAR_DATA *ch, char *argument );
bool    redit           ( CHAR_DATA *ch, char *argument );
bool    medit           ( CHAR_DATA *ch, char *argument );
bool    oedit           ( CHAR_DATA *ch, char *argument );
bool    mpedit		( CHAR_DATA *ch, char *argument );
bool    opedit		( CHAR_DATA *ch, char *argument );
bool    rpedit		( CHAR_DATA *ch, char *argument );
bool    apedit		( CHAR_DATA *ch, char *argument );
bool    hedit		( CHAR_DATA *ch, char *argument );
bool    socialedit	( CHAR_DATA *ch, char *argument );
bool    propertyedit	( CHAR_DATA *ch, char *argument );
bool    resetedit	( CHAR_DATA *ch, char *argument );



/*
 * OLC Constants
 */
#define MAX_MOB	1		/* Default maximum number for resetting mobs */



/*
 * Structure for an OLC editor command.
 */
struct olc_cmd_type
{
    char * const	name;
    OLC_FUN *		olc_fun;
};



/*
 * Structure for an OLC editor startup command.
 */
struct	editor_cmd_type
{
    char * const	name;
    DO_FUN *		do_fun;
};

struct olc_mob_recommended {
    int hit[3];
    int mana[3];
    int damage[3];
    int ac;		/* *10 */
};

extern struct olc_mob_recommended olc_mob_recommended[91];

/*
 * Utils.
 */
AREA_DATA *get_vnum_area	( int vnum );
AREA_DATA *get_area_data	( int vnum );
void add_reset			( ROOM_INDEX_DATA *room,
				         RESET_DATA *pReset, int index );
HELP_DATA *get_help_data	( AREA_DATA *pArea, char *keywords );
AREA_DATA *get_help_area	( CHAR_DATA *ch );



/*
 * Interpreter Table Prototypes
 */
extern const struct olc_cmd_type	aedit_table[];
extern const struct olc_cmd_type	redit_table[];
extern const struct olc_cmd_type	oedit_table[];
extern const struct olc_cmd_type	medit_table[];
extern const struct olc_cmd_type	mpedit_table[];
extern const struct olc_cmd_type	opedit_table[];
extern const struct olc_cmd_type	rpedit_table[];
extern const struct olc_cmd_type	apedit_table[];
extern const struct olc_cmd_type	hedit_table[];
extern const struct olc_cmd_type	socialedit_table[];
extern const struct olc_cmd_type	resetedit_table[];



/*
 * General Functions
 */
bool show_commands		( CHAR_DATA *ch, char *argument );
bool show_olc_help		( CHAR_DATA *ch, char *argument );
bool edit_done			( CHAR_DATA *ch );
bool show_version		( CHAR_DATA *ch, char *argument );


/*
 * Area Editor Prototypes
 */
DECLARE_OLC_FUN( aedit_show		);
DECLARE_OLC_FUN( aedit_create		);
DECLARE_OLC_FUN( aedit_name		);
DECLARE_OLC_FUN( aedit_file		);
DECLARE_OLC_FUN( aedit_age		);
DECLARE_OLC_FUN( aedit_recall		);
DECLARE_OLC_FUN( aedit_login_room	);
DECLARE_OLC_FUN( aedit_reset		);
DECLARE_OLC_FUN( aedit_security		);
DECLARE_OLC_FUN( aedit_builder		);
DECLARE_OLC_FUN( aedit_creator		);
DECLARE_OLC_FUN( aedit_urlprefix	);
DECLARE_OLC_FUN( aedit_vnum		);
DECLARE_OLC_FUN( aedit_lvnum		);
DECLARE_OLC_FUN( aedit_uvnum		);
DECLARE_OLC_FUN( aedit_level		);
DECLARE_OLC_FUN( aedit_objlevel		);
DECLARE_OLC_FUN( aedit_remove		);
DECLARE_OLC_FUN( aedit_unfinished	);
DECLARE_OLC_FUN( aedit_purge		);
DECLARE_OLC_FUN( aedit_description	);
DECLARE_OLC_FUN( aedit_comment		);
DECLARE_OLC_FUN( aedit_aprog		);
DECLARE_OLC_FUN( aedit_proginstance	);
DECLARE_OLC_FUN( aedit_program		);
DECLARE_OLC_FUN( aedit_property		);
DECLARE_OLC_FUN( aedit_variable		);



/*
 * Room Editor Prototypes
 */
DECLARE_OLC_FUN( redit_show		);
DECLARE_OLC_FUN( redit_create		);
DECLARE_OLC_FUN( redit_clone		);
DECLARE_OLC_FUN( redit_name		);
DECLARE_OLC_FUN( redit_desc		);
DECLARE_OLC_FUN( redit_ed		);
DECLARE_OLC_FUN( redit_format		);
DECLARE_OLC_FUN( redit_north		);
DECLARE_OLC_FUN( redit_south		);
DECLARE_OLC_FUN( redit_east		);
DECLARE_OLC_FUN( redit_west		);
DECLARE_OLC_FUN( redit_up		);
DECLARE_OLC_FUN( redit_down		);
DECLARE_OLC_FUN( redit_move		);
DECLARE_OLC_FUN( redit_mreset		);
DECLARE_OLC_FUN( redit_oreset		);
DECLARE_OLC_FUN( redit_mlist		);
DECLARE_OLC_FUN( redit_olist		);
DECLARE_OLC_FUN( redit_mshow		);
DECLARE_OLC_FUN( redit_oshow		);
DECLARE_OLC_FUN( redit_owner		);
DECLARE_OLC_FUN( redit_clan		);
DECLARE_OLC_FUN( redit_level		);
DECLARE_OLC_FUN( redit_rlist		);
DECLARE_OLC_FUN( redit_delete		);
DECLARE_OLC_FUN( redit_property		);
DECLARE_OLC_FUN( redit_picture		);
DECLARE_OLC_FUN( redit_proginstance	);


/*
 * Object Editor Prototypes
 */
DECLARE_OLC_FUN( oedit_show		);
DECLARE_OLC_FUN( oedit_create		);
DECLARE_OLC_FUN( oedit_clone		);
DECLARE_OLC_FUN( oedit_name		);
DECLARE_OLC_FUN( oedit_short		);
DECLARE_OLC_FUN( oedit_long		);
DECLARE_OLC_FUN( oedit_addeffect	);
DECLARE_OLC_FUN( oedit_deleffect	);
DECLARE_OLC_FUN( oedit_value0		);
DECLARE_OLC_FUN( oedit_value1		);
DECLARE_OLC_FUN( oedit_value2		);
DECLARE_OLC_FUN( oedit_value3		);
DECLARE_OLC_FUN( oedit_value4		);
DECLARE_OLC_FUN( oedit_weight		);
DECLARE_OLC_FUN( oedit_cost		);
DECLARE_OLC_FUN( oedit_ed		);
DECLARE_OLC_FUN( oedit_material		);
DECLARE_OLC_FUN( oedit_level		);
DECLARE_OLC_FUN( oedit_condition	);
DECLARE_OLC_FUN( oedit_anticlass	);
DECLARE_OLC_FUN( oedit_antirace		);
DECLARE_OLC_FUN( oedit_classonly	);
DECLARE_OLC_FUN( oedit_raceonly		);
DECLARE_OLC_FUN( oedit_racepoison	);
DECLARE_OLC_FUN( oedit_delete		);
DECLARE_OLC_FUN( oedit_property		);
DECLARE_OLC_FUN( oedit_picture		);
DECLARE_OLC_FUN( oedit_proginstance	);



/*
 * Mobile Editor Prototypes
 */
DECLARE_OLC_FUN( medit_show		);
DECLARE_OLC_FUN( medit_create		);
DECLARE_OLC_FUN( medit_name		);
DECLARE_OLC_FUN( medit_short		);
DECLARE_OLC_FUN( medit_long		);
DECLARE_OLC_FUN( medit_ed		);
DECLARE_OLC_FUN( medit_shop		);
DECLARE_OLC_FUN( medit_desc		);
DECLARE_OLC_FUN( medit_level		);
DECLARE_OLC_FUN( medit_align		);
DECLARE_OLC_FUN( medit_spec		);
DECLARE_OLC_FUN( medit_group		);
DECLARE_OLC_FUN( medit_race		);
DECLARE_OLC_FUN( medit_size		);
DECLARE_OLC_FUN( medit_position		);
DECLARE_OLC_FUN( medit_proginstance	);
DECLARE_OLC_FUN( medit_wealth		);
DECLARE_OLC_FUN( medit_material		);
DECLARE_OLC_FUN( medit_hitroll		);
DECLARE_OLC_FUN( medit_damagetype	);
DECLARE_OLC_FUN( medit_ac		);
DECLARE_OLC_FUN( medit_dice		);
DECLARE_OLC_FUN( medit_imm		);
DECLARE_OLC_FUN( medit_res		);
DECLARE_OLC_FUN( medit_vuln		);
DECLARE_OLC_FUN( medit_offensive	);
DECLARE_OLC_FUN( medit_addtrigger	);
DECLARE_OLC_FUN( medit_deltrigger	);
DECLARE_OLC_FUN( medit_form		);
DECLARE_OLC_FUN( medit_delete		);
DECLARE_OLC_FUN( medit_picture		);
DECLARE_OLC_FUN( medit_property		);
DECLARE_OLC_FUN( medit_clone		);

/*
 * MobProg Editor Prototypes
 */
DECLARE_OLC_FUN( mpedit_show		);
DECLARE_OLC_FUN( mpedit_create		);
DECLARE_OLC_FUN( mpedit_clear		);
DECLARE_OLC_FUN( mpedit_append		);
DECLARE_OLC_FUN( mpedit_delete		);
DECLARE_OLC_FUN( mpedit_title		);

/*
 * ObjProg Editor Prototypes
 */
DECLARE_OLC_FUN( opedit_show		);
DECLARE_OLC_FUN( opedit_create		);
DECLARE_OLC_FUN( opedit_clear		);
DECLARE_OLC_FUN( opedit_append		);
DECLARE_OLC_FUN( opedit_delete		);
DECLARE_OLC_FUN( opedit_title		);

/*
 * RoomProg Editor Prototypes
 */
DECLARE_OLC_FUN( rpedit_show		);
DECLARE_OLC_FUN( rpedit_create		);
DECLARE_OLC_FUN( rpedit_clear		);
DECLARE_OLC_FUN( rpedit_append		);
DECLARE_OLC_FUN( rpedit_delete		);
DECLARE_OLC_FUN( rpedit_title		);

/*
 * Area Editor Prototypes
 */

DECLARE_OLC_FUN( apedit_show		);
DECLARE_OLC_FUN( apedit_create		);
DECLARE_OLC_FUN( apedit_clear		);
DECLARE_OLC_FUN( apedit_append		);
DECLARE_OLC_FUN( apedit_delete		);
DECLARE_OLC_FUN( apedit_title		);

/*
 * Help Editor Prototypes
 */
DECLARE_OLC_FUN( hedit_show		);
DECLARE_OLC_FUN( hedit_create		);
DECLARE_OLC_FUN( hedit_clear		);
DECLARE_OLC_FUN( hedit_append		);
DECLARE_OLC_FUN( hedit_keyword		);
DECLARE_OLC_FUN( hedit_hlist		);
DECLARE_OLC_FUN( hedit_level		);
DECLARE_OLC_FUN( hedit_delete		);

/*
 * Social Editor Prototypes
 */
DECLARE_OLC_FUN( socialedit_show	);
DECLARE_OLC_FUN( socialedit_create	);
DECLARE_OLC_FUN( socialedit_name	);
DECLARE_OLC_FUN( socialedit_delete	);
DECLARE_OLC_FUN( socialedit_cna		);
DECLARE_OLC_FUN( socialedit_ona		);
DECLARE_OLC_FUN( socialedit_cf		);
DECLARE_OLC_FUN( socialedit_of		);
DECLARE_OLC_FUN( socialedit_vf		);
DECLARE_OLC_FUN( socialedit_cnf		);
DECLARE_OLC_FUN( socialedit_ca		);
DECLARE_OLC_FUN( socialedit_oa		);

/*
 * Reset Editor Prototypes
 */
DECLARE_OLC_FUN( resetedit_create	);
DECLARE_OLC_FUN( resetedit_change	);
DECLARE_OLC_FUN( resetedit_swap		);
DECLARE_OLC_FUN( resetedit_insert	);
DECLARE_OLC_FUN( resetedit_delete	);
DECLARE_OLC_FUN( resetedit_show		);
DECLARE_OLC_FUN( resetedit_move		);
void	display_resets( CHAR_DATA *ch );

/*
 * Macros
 */
#define IS_BUILDER(ch, Area)	(  !IS_SWITCHED( ch ) \
				&& ( ch->pcdata->security >= Area->security  \
				|| is_exact_name( ch->name, Area->builders )    \
				|| is_exact_name( "All", Area->builders ) ) )

/* Return pointers to what is being edited. */
#define EDIT_MOB(Ch, Mob)	( Mob = (MOB_INDEX_DATA *)Ch->desc->pEdit )
#define EDIT_OBJ(Ch, Obj)	( Obj = (OBJ_INDEX_DATA *)Ch->desc->pEdit )
#define EDIT_ROOM(Ch, Room)	( Room = Ch->in_room )
#define EDIT_AREA(Ch, Area)	( Area = (AREA_DATA *)Ch->desc->pEdit )
#define EDIT_TCLPROG(Ch, Code)	( Code = (TCLPROG_CODE *)Ch->desc->pEdit )
#define EDIT_HELP(Ch, Help)	( Help = (HELP_DATA *)Ch->desc->pEdit )
#define EDIT_SOCIAL(Ch, Social)	( Social = (SOCIAL_TYPE *)Ch->desc->pEdit )
#define EDIT_PROPERTY(Ch, Prop)	( Prop = (PROPERTY_TYPE *)Ch->desc->pEdit )
#define EDIT_RESET(Ch, Room)	( Room = Ch->in_room )






/*
 * Prototypes
 */
MOB_INDEX_DATA *display_reset ( CHAR_DATA *ch,RESET_DATA *pReset,MOB_INDEX_DATA *pMob,int line);
long mob_flag_toggle (
		char *arg,
		const OPTION_TYPE *options,
		long *setflags,
		long raceflags,
		long *clearflags);

char *mob_flag_toggle_string (
		char *arg,
		const OPTION_TYPE *options,
		char *setflags,
		const char *raceflags,
		char *clearflags,
		char *flagstobealtered);

