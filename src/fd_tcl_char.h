//
// $Id: fd_tcl_char.h,v 1.10 2008/03/30 11:30:16 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#define TCL_CHAR_CMD(name) int name(Tcl_Interp *interp,CHAR_DATA *ch,int objc,Tcl_Obj *CONST objv[])
#define TCL_MOB_CMD(name) int name(Tcl_Interp *interp,CHAR_DATA *ch,int objc,Tcl_Obj *CONST objv[])

TCL_CHAR_CMD(CharCmdNotImpl);
TCL_CHAR_CMD(char_name);
TCL_CHAR_CMD(char_id);
TCL_CHAR_CMD(char_isimmortal);
TCL_CHAR_CMD(char_ispc);
TCL_CHAR_CMD(char_isnpc);
TCL_CHAR_CMD(char_isgood);
TCL_CHAR_CMD(char_isevil);
TCL_CHAR_CMD(char_isneutral);
TCL_CHAR_CMD(char_ischarmed);
TCL_CHAR_CMD(char_follows);
TCL_CHAR_CMD(char_isactive);
TCL_CHAR_CMD(char_delay);
TCL_CHAR_CMD(char_hasdelay);
TCL_CHAR_CMD(char_vnum);
TCL_CHAR_CMD(char_hp);
TCL_CHAR_CMD(char_maxhp);
TCL_CHAR_CMD(char_move);
TCL_CHAR_CMD(char_maxmove);
TCL_CHAR_CMD(char_room);
TCL_CHAR_CMD(char_level);
TCL_CHAR_CMD(char_sex);
TCL_CHAR_CMD(char_align);
TCL_CHAR_CMD(char_money);
TCL_CHAR_CMD(char_position);
TCL_CHAR_CMD(char_fullname);
TCL_CHAR_CMD(char_nametitle);
TCL_CHAR_CMD(char_title);
TCL_CHAR_CMD(char_hasjob);
TCL_CHAR_CMD(char_iseff);
TCL_CHAR_CMD(char_eff);
TCL_CHAR_CMD(char_isact);
TCL_CHAR_CMD(char_acts);
TCL_CHAR_CMD(char_isoff);
TCL_CHAR_CMD(char_off);
TCL_CHAR_CMD(char_isimm);
TCL_CHAR_CMD(char_imm);
TCL_CHAR_CMD(char_isres);
TCL_CHAR_CMD(char_res);
TCL_CHAR_CMD(char_isvuln);
TCL_CHAR_CMD(char_vuln);
TCL_CHAR_CMD(char_clan);
TCL_CHAR_CMD(char_rank);
TCL_CHAR_CMD(char_race);
TCL_CHAR_CMD(char_class);
TCL_CHAR_CMD(char_he);
TCL_CHAR_CMD(char_him);
TCL_CHAR_CMD(char_his);
TCL_CHAR_CMD(char_echo);
TCL_CHAR_CMD(char_echoaround);
TCL_CHAR_CMD(char_cansee);
TCL_CHAR_CMD(char_hpcnt);
TCL_CHAR_CMD(char_short);
TCL_CHAR_CMD(char_goto);
TCL_CHAR_CMD(char_act);
TCL_CHAR_CMD(char_iscarrying);
TCL_CHAR_CMD(char_iswearing);
TCL_CHAR_CMD(char_wears);
TCL_CHAR_CMD(char_remove);
TCL_CHAR_CMD(char_do);
TCL_CHAR_CMD(char_damage);
TCL_CHAR_CMD(char_group);
TCL_CHAR_CMD(char_followers);
TCL_CHAR_CMD(char_property);
TCL_CHAR_CMD(char_remeff);
TCL_CHAR_CMD(char_beenhere);
TCL_CHAR_CMD(char_killedmob);
TCL_CHAR_CMD(char_questpoints);
TCL_CHAR_CMD(char_quest);
TCL_CHAR_CMD(char_exp);
TCL_CHAR_CMD(char_tnl);
TCL_CHAR_CMD(char_expforalevel);
TCL_CHAR_CMD(char_dex);
TCL_CHAR_CMD(char_int);
TCL_CHAR_CMD(char_wis);
TCL_CHAR_CMD(char_str);
TCL_CHAR_CMD(char_con);
TCL_CHAR_CMD(char_removespell);
TCL_CHAR_CMD(char_timeskilled);
TCL_CHAR_CMD(char_unequip);
TCL_CHAR_CMD(char_equip);
TCL_CHAR_CMD(char_hunger);
TCL_CHAR_CMD(char_thirst);
TCL_CHAR_CMD(char_adrenaline);
TCL_CHAR_CMD(char_drunk);
TCL_CHAR_CMD(char_carries);
TCL_CHAR_CMD(char_ool_penalty);
TCL_CHAR_CMD(char_canseeobj);
TCL_CHAR_CMD(char_oload);
TCL_CHAR_CMD(char_addeff);
TCL_CHAR_CMD(char_delproperty);
TCL_CHAR_CMD(char_mana);
TCL_CHAR_CMD(char_maxmana);
TCL_CHAR_CMD(char_fighting);
TCL_CHAR_CMD(char_die);
TCL_CHAR_CMD(char_wimpy);
TCL_CHAR_CMD(char_objhere);
TCL_CHAR_CMD(char_listeff);
TCL_CHAR_CMD(char_selectobj);
