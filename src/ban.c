/* $Id: ban.c,v 1.38 2005/10/09 20:00:21 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

#include "merc.h"
#include "interp.h"
#ifdef THREADED_DNS
#include "rdns_cache.h"
#endif

BAN_DATA *ban_list=NULL;

LOCKOUT_DATA *  lockout_list=NULL;
#define		MAX_ATTEMPT	5
#define		RETENTION_TIME	300

void save_bans(void)
{
    BAN_DATA *pban;
    FILE *fp;

    fclose( fpReserve );
    if ( ( fp = fopen( mud_data.ban_file, "w" ) ) == NULL )
    {
	bugf("Couldn't open ban-file for writing.");
	logf("[0] save_bans(): fopen(%s): %s", mud_data.ban_file,ERROR);
	 fpReserve = fopen( NULL_FILE, "r" );
	return;
    }

    for (pban = ban_list; pban != NULL; pban = pban->next)
    {
	if (IS_SET(pban->ban_flags,BAN_PERMANENT)) {
	    fprintf(fp,"'%s' %d %s\n",pban->name,pban->level,
		print_flags(pban->ban_flags));
	}
     }

     fclose(fp);
     fpReserve = fopen( NULL_FILE, "r" );
}

void ban_update(void) {
    BAN_DATA *pban,*pban_next,*prev;

    prev = NULL;
    for ( pban=ban_list; pban!=NULL; prev=pban, pban=pban_next ) {
	pban_next=pban->next;

	if (pban->ban_wearoff>0) {
	    if (time(NULL)>pban->ban_wearoff) {
		if (prev==NULL)
		    ban_list=pban_next;
		else
		    prev->next=pban->next;
		wiznet(WIZ_BAN,0,NULL,NULL,
		    "BAN: Releasing timed-ban for %s",pban->name);
		logf("[0] Releasing timed-ban for %s",pban->name);
		free_ban(pban);
	    }
	    pban=prev;
	}

    }
}

void load_bans(void) {
    FILE *	fp;
    BAN_DATA *	ban_last;
    BAN_DATA *	pban;

    logf("[0] BAN: Loading bans");
    if ((fp=fopen(mud_data.ban_file,"r"))==NULL) {
	bugf("Couldn't open ban-file for loading.");
	logf("[0] load_bans(): fopen(%s): %s",mud_data.ban_file,ERROR);
        return;
    }

    fseek(fp,0,SEEK_END);
    if (ftell(fp)==0) {
	fclose(fp);
	return;
    }
    fseek(fp,0,SEEK_SET);

    ban_last=NULL;
    while (!feof(fp)) {
	pban=new_ban();

	pban->name=str_dup(fread_word(fp));
	pban->level=fread_number(fp);
	pban->ban_flags=fread_flag(fp);
	fread_to_eol(fp);

        if (ban_list==NULL)
	    ban_list=pban;
	else
	    ban_last->next=pban;
	ban_last=pban;
    }
    fclose(fp);
}

BAN_DATA *checkban(char *site) {
    BAN_DATA *pban;
    char host[MAX_STRING_LENGTH];
    char *space;

    strcpy(host,capitalize(site));
    host[0] = LOWER(host[0]);
    space=strchr(host,' ');
    if (space) { space[0]=0; }

    for ( pban = ban_list; pban != NULL; pban = pban->next ) {
	if (IS_SET(pban->ban_flags,BAN_PREFIX)
	&&  IS_SET(pban->ban_flags,BAN_SUFFIX)
	&&  strstr(host,pban->name) != NULL)
	    return pban;

	if (IS_SET(pban->ban_flags,BAN_PREFIX)
	&&  !str_suffix(pban->name,host))
	    return pban;

	if (IS_SET(pban->ban_flags,BAN_SUFFIX)
	&&  !str_prefix(pban->name,host))
	    return pban;

	if (!IS_SET(pban->ban_flags,BAN_PREFIX)
	&&  !IS_SET(pban->ban_flags,BAN_SUFFIX)
	&&  !str_cmp(pban->name,host))
	    return pban;
    }

    return NULL;
}


bool	checkban_siteban(char *site) {
    BAN_DATA *pban;

    if ((pban=checkban(site))==NULL) return FALSE;
    if (!IS_SET(pban->ban_flags,BAN_IMMORTALONLY) &&
        !IS_SET(pban->ban_flags,BAN_NONEWBIE)) return TRUE;
    return FALSE;
}

bool	checkban_immortalonly(char *site) {
    BAN_DATA *pban;

    if ((pban=checkban(site))==NULL) return FALSE;
    if (IS_SET(pban->ban_flags,BAN_IMMORTALONLY)) return TRUE;
    return FALSE;
}

bool	checkban_newbie(char *site) {
    BAN_DATA *pban;

    if ((pban=checkban(site))==NULL) return FALSE;
    if (IS_SET(pban->ban_flags,BAN_NONEWBIE))
	return TRUE;
    return FALSE;
}

void ban_site(CHAR_DATA *ch,char *argument,long options,int duration) {
    char buf[MSL],buf2[MSL];
    char arg1[MIL],arg2[MIL];
    char *name;
    BUFFER *buffer;
    BAN_DATA *pban, *prev;
    bool prefix = FALSE,suffix = FALSE;

    argument = one_argument(argument,arg1);
    argument = one_argument(argument,arg2);

    if ( arg1[0] == 0 ) {
	buffer = new_buf();

	if (ban_list == NULL) {
	    add_buf(buffer,"No sites banned at this time.\n\r");
  	} else {
	    add_buf(buffer,"{yBanned sites                level  type     status       duration{x\n\r");
	    for (pban = ban_list;pban != NULL;pban = pban->next) {
		if (pban->ban_wearoff==0) {
		    if (IS_SET(pban->ban_flags,BAN_PERMANENT))
			strcpy(arg1,"forever");
		    else
			strcpy(arg1,"until reboot");
		} else {
		    time_t remaining=pban->ban_wearoff-time(NULL);

		    if (remaining<0)
			strcpy(arg1,"real soon now");
		    else
			if (remaining<3600)
			    strftime(arg1,sizeof(arg1),"%M:%S remaining",gmtime(&remaining));
			else
			    strftime(arg1,sizeof(arg1),"%H:%M:%S remaining",gmtime(&remaining));
		}
		sprintf(buf2,"%s%s%s",
		    IS_SET(pban->ban_flags,BAN_PREFIX) ? "*" : "",
		    pban->name,
		    IS_SET(pban->ban_flags,BAN_SUFFIX) ? "*" : "");
		sprintf(buf,"%-27s   %3d  %-8s %-12s %s\n\r",
		    buf2, pban->level,
		    IS_SET(pban->ban_flags,BAN_TEMP)	? "temp"	:
		    IS_SET(pban->ban_flags,BAN_PERMANENT)	? "perm"	:
		    IS_SET(pban->ban_flags,BAN_TIMED)	? "timed"	:
							      "(unknown)"	,
		    IS_SET(pban->ban_flags,BAN_NONEWBIE)	? "nonewbies"	:
		    IS_SET(pban->ban_flags,BAN_IMMORTALONLY)? "immortalonly":
							      "all"		,
		    arg1);
		add_buf(buffer,buf);
	    }
	}

	if (lockout_list) {
	    LOCKOUT_DATA *lock;
	    char last[26],release[26];
	    time_t release_time;

	    add_buf(buffer,"Sites lockout data\n\r");
	    add_buf(buffer,"{ySite                                   Attempts  Last attempt                Release after{x\n\r");
	    for (lock=lockout_list;lock;lock=lock->next) {
#ifdef THREADED_DNS
  #ifdef INET6
		if (lock->ipv6)
		    name=gethostname_cached(lock->Host,16,FALSE);
		else
		    name=gethostname_cached(lock->Host,4,FALSE);
  #else
		name=gethostname_cached(lock->Host,4,FALSE);
  #endif
#else
		name=lock->Host;
#endif
		ctime_r(&(lock->last_attempt),last);last[24]=0;
		release_time=lock->last_attempt+UMAX(1,lock->attempts-MAX_ATTEMPT)*RETENTION_TIME;
		ctime_r(&release_time,release);release[24]=0;
		sprintf(buf,"%s%-40s %5d    %s    %s\n\r{x",
			lock->attempts>MAX_ATTEMPT?"{R":"{G",
			name,lock->attempts,
			last,release);
		add_buf(buffer,buf);
	    }

	} else 
	  add_buf(buffer,"No sites locked out.\n\r");

        page_to_char( buf_string(buffer), ch );
	free_buf(buffer);
        return;
    }

    name = arg1;

    if (name[0] == '*') {
	prefix = TRUE;
	name++;
    }

    if (name[strlen(name) - 1] == '*') {
	suffix = TRUE;
	name[strlen(name) - 1] = '\0';
    }

    if (strlen(name) == 0) {
	send_to_char("You have to ban SOMETHING.\n\r",ch);
	return;
    }

    prev = NULL;
    for ( pban = ban_list; pban != NULL; prev = pban, pban = pban->next ) {
        if (!str_cmp(name,pban->name)) {
	    if (pban->level > get_trust(ch)) {
            	send_to_char( "That ban was set by a higher power.\n\r", ch );
            	return;
	    } else {
		if (prev == NULL)
		    ban_list = pban->next;
		else
		    prev->next = pban->next;
		free_ban(pban);
	    }
        }
    }

    pban = new_ban();
    pban->name = str_dup(name);
    pban->level = get_trust(ch);

    /* set ban type */
    pban->ban_flags=options;
    if (duration)
	pban->ban_wearoff=duration+time(NULL);

    if (prefix)
	SET_BIT(pban->ban_flags,BAN_PREFIX);
    if (suffix)
	SET_BIT(pban->ban_flags,BAN_SUFFIX);

    pban->next  = ban_list;
    ban_list    = pban;
    save_bans();
    sprintf_to_char(ch,"'%s' has been banned.\n\r",pban->name);
    logf("[%d] %s has banned '%s'.",GET_DESCRIPTOR(ch),ch->name,pban->name);
    wiznet(WIZ_BAN,0,ch,NULL,"BAN: '%s' has been banned by $N.",pban->name);
    return;
}

void do_ban(CHAR_DATA *ch, char *argument) {
    char	arg[MIL];
    char	host[MIL];
    long	options=0;
    time_t	timedban=0;

    if (!str_cmp(argument,"help") ||
	!str_cmp(argument,"?")) {
	send_to_char("Usage: ban <ipaddress | subnet.* | hostname | *.domain> [permanent | temporary] [time <minutes>] [newbie | immortalonly]\n\r",ch);
	return;
    }

    argument=one_argument(argument,host);
    argument=one_argument(argument,arg);
    while (arg[0]!=0) {
	switch (which_keyword(arg,
	    "permanent","temporary","time","newbie","immortalonly",
	    NULL)) {

	default:
	    if (argument[0]==0)
		break;

	    sprintf_to_char(ch,"Unknown option: %s.\n\r",arg);
	    do_function(ch,&do_ban,"help");
	    return;

	case 1:	// permanent
	    if (IS_SET(options,BAN_TEMP)) {
		send_to_char("Got both permanent and temporary ban.\n\r",ch);
		return;
	    }
	    if (IS_SET(options,BAN_TIMED)) {
		send_to_char("There is already a time defined.\n\r",ch);
		return;
	    }
	    SET_BIT(options,BAN_PERMANENT);
	    break;

	case 2:	// temporary
	    if (IS_SET(options,BAN_PERMANENT)) {
		send_to_char("Got both permanent and temporary ban.\n\r",ch);
		return;
	    }
	    if (IS_SET(options,BAN_TIMED)) {
		send_to_char("There is already a time defined.\n\r",ch);
		return;
	    }
	    SET_BIT(options,BAN_TEMP);
	    break;

	case 3:	// time
	{
	    char time[MIL];

	    if (IS_SET(options,BAN_TIMED)) {
		send_to_char("There is already a time defined.\n\r",ch);
		return;
	    }
	    if (IS_SET(options,BAN_TEMP)||IS_SET(options,BAN_PERMANENT)) {
		send_to_char("A timed ban can't be used together with a permanent or temporary one.\n\r",ch);
		return;
	    }

	    if (argument[0]==0) {
		send_to_char("Invalid time specification.\n\r",ch);
		return;
	    }
	    argument=one_argument(argument,time);
	    if (!is_number(time)) {
		send_to_char("Invalid time-specification.\n\r",ch);
		return;
	    }
	    timedban=atol(time);
	    SET_BIT(options,BAN_TIMED);
	    break;
	}

	case 4:	// newbie
	    SET_BIT(options,BAN_NONEWBIE);
	    break;

	case 5:	// immortalonly
	    SET_BIT(options,BAN_IMMORTALONLY);
	    break;

	}
	argument=one_argument(argument,arg);
    }

    // default is BAN_TEMPORARY
    if (!IS_SET(options,BAN_PERMANENT) &&
	!IS_SET(options,BAN_TIMED))
	SET_BIT(options,BAN_TEMP);

    ban_site(ch,host,options,timedban*60);
}


void do_allow( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    BAN_DATA *prev;
    BAN_DATA *curr;
    int i;

    one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
        send_to_char( "Remove which site from the ban list?\n\r", ch );
        return;
    }

    if (arg[0]=='*')
      memmove(arg,arg+1,MAX_INPUT_LENGTH-1);
    i=strlen(arg);
    if (arg[i-1]=='*') arg[i-1]=0;

    prev = NULL;
    for ( curr = ban_list; curr != NULL; prev = curr, curr = curr->next )
    {
        if ( !str_cmp( arg, curr->name ) )
        {
	    if (curr->level > get_trust(ch))
	    {
		send_to_char(
		   "You are not powerful enough to lift that ban.\n\r",ch);
		return;
	    }
            if ( prev == NULL )
                ban_list   = ban_list->next;
            else
                prev->next = curr->next;

            free_ban(curr);
	    sprintf_to_char(ch,"Ban on %s lifted.\n\r",arg);
	    wiznet(WIZ_BAN,0,ch,NULL,"BAN: Ban on '%s' lifted by $N.",arg);
	    save_bans();
            return;
        }
    }

    send_to_char( "Site is not banned.\n\r", ch );
    return;
}

BADNAME_DATA *	badname_list;

bool checkbadname(char *name) {
    BADNAME_DATA *badname=badname_list;
    char lowername[MSL];

    while (badname!=NULL)
	if (is_exact_name(name,badname->name))
	    return TRUE;
	else
	    badname=badname->next;

    strcpy(lowername,name);
    // check_swearing can overwrite a string, so we use a copy.
    if (check_swearing(lowername)) return TRUE;
    // if check_swearing==FALSE the string is intact

    lowername[0]=tolower(lowername[0]);

    if (strchr(lowername,'a')==NULL
     && strchr(lowername,'e')==NULL
     && strchr(lowername,'i')==NULL
     && strchr(lowername,'o')==NULL
     && strchr(lowername,'u')==NULL
     && strchr(lowername,'y')==NULL)
	return TRUE;

    if (strchr(lowername,'b')==NULL
     && strchr(lowername,'c')==NULL
     && strchr(lowername,'d')==NULL
     && strchr(lowername,'f')==NULL
     && strchr(lowername,'g')==NULL
     && strchr(lowername,'h')==NULL
     && strchr(lowername,'j')==NULL
     && strchr(lowername,'k')==NULL
     && strchr(lowername,'l')==NULL
     && strchr(lowername,'m')==NULL
     && strchr(lowername,'n')==NULL
     && strchr(lowername,'p')==NULL
     && strchr(lowername,'q')==NULL
     && strchr(lowername,'r')==NULL
     && strchr(lowername,'s')==NULL
     && strchr(lowername,'t')==NULL
     && strchr(lowername,'v')==NULL
     && strchr(lowername,'w')==NULL
     && strchr(lowername,'x')==NULL
     && strchr(lowername,'z')==NULL)
	return TRUE;

    return FALSE;
}

void load_badnames(void) {
    FILE *		fp;
    BADNAME_DATA *	badname_last;
    BADNAME_DATA *	pbadname;
    char *p;

    logf("[0] BADNAMES: Loading badnames");
    if ((fp=fopen(mud_data.badname_file,"r"))==NULL) {
	bugf("Couldn't open badname-file for loading.");
	logf("[0] load_badnames(): fopen(%s): %s",mud_data.badname_file,ERROR);
        return;
    }

    fseek(fp,0,SEEK_END);
    if (ftell(fp)==0)
	return;
    fseek(fp,0,SEEK_SET);

    badname_last=NULL;
    while (!feof(fp)) {
	p=fread_word(fp);
	if (p[0]==0) continue;
	if (p[0]=='#') {
	    fread_to_eol(fp);
	    continue;
	}

	pbadname=new_badname();
	pbadname->name=str_dup(p);

        if (badname_list==NULL)
	    badname_list=pbadname;
	else
	    badname_last->next=pbadname;
	badname_last=pbadname;

	if (feof(fp)) break;
    }
    fclose(fp);
}


bool check_lockout(const bool ipv6, const HOST_DATA Host, bool test_only) {
    LOCKOUT_DATA *lock;
    
    // find existing lock entry
    for (lock=lockout_list;lock;lock=lock->next)
#ifdef THREADED_DNS
	if (ipv6==lock->ipv6 && !memcmp(Host,lock->Host,sizeof(HOST_DATA)))
#else
	if (ipv6==lock->ipv6 && !str_cmp(Host,lock->Host))
#endif
	    break;

    // if found: update
    if (lock) {
	lock->last_attempt=time(NULL);
	if (test_only) {
	    if (lock->attempts<=MAX_ATTEMPT) return FALSE;
	    lock->attempts++;
	    return TRUE;
	}
	if (lock->attempts==MAX_ATTEMPT) {
	    char *name;
#ifdef THREADED_DNS
  #ifdef INET6
	    if (lock->ipv6)
		name=gethostname_cached(lock->Host,16,FALSE);
	    else
		name=gethostname_cached(lock->Host,4,FALSE);
  #else
	    name=gethostname_cached(lock->Host,4,FALSE);
  #endif
#else
	    name=lock->Host;
#endif
	    wiznet(WIZ_BAN,0,NULL,NULL,
		    "BAN: Locking out %s",name);
	    logf("[0] locking out %s",name);
	}
	if (++lock->attempts>MAX_ATTEMPT)
	    return TRUE;
    } else {
	if (test_only) return FALSE;
    // not found: create entry
	lock=new_lockout();
	lock->attempts=1;
	lock->last_attempt=time(NULL);
	lock->ipv6=ipv6;
#ifdef THREADED_DNS
	memcpy(lock->Host,Host,sizeof(HOST_DATA));
#else
	lock->Host=str_dup(Host);
#endif
	lock->next=lockout_list;
	lockout_list=lock;
    }
    return FALSE;
}

void lockout_update(void) {
    LOCKOUT_DATA *lock,*next_lock,*prev_lock=NULL;
    time_t now;
    now=time(NULL);

    for (lock=lockout_list;lock;lock=next_lock) {
	next_lock=lock->next;
	if ((lock->last_attempt+UMAX(1,lock->attempts-MAX_ATTEMPT)*RETENTION_TIME)<now) {
	    // release
	    char *name;
#ifdef THREADED_DNS
  #ifdef INET6
	    if (lock->ipv6)
		name=gethostname_cached(lock->Host,16,FALSE);
	    else
		name=gethostname_cached(lock->Host,4,FALSE);
  #else
	    name=gethostname_cached(lock->Host,4,FALSE);
  #endif
#else
	    name=lock->Host;
#endif
	    if (lock->attempts>MAX_ATTEMPT) {
		wiznet(WIZ_BAN,0,NULL,NULL,
			"BAN: Releasing lockout for %s",name);
		logf("[0] Releasing lockout for %s",name);
	    } else {
		wiznet(WIZ_BAN,0,NULL,NULL,
			"BAN: Removing lockout data for %s",name);
		logf("[0] Removing lockout data for %s",name);
	    }
#ifndef THREADED_DNS
	    free_string(lock->Host);
#endif
	    if (prev_lock)
		prev_lock->next=lock->next;
	    else
		lockout_list=lock->next;
	    free_lockout(lock);
	    continue; // no, we don't want to update prev_lock.
	}
	prev_lock=lock;
    }

}
