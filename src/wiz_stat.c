//
// $Id: wiz_stat.c,v 1.33 2008/05/11 20:50:47 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"
#include "olc.h"

void do_stat(CHAR_DATA *ch,char *argument) {
    char	arg[MIL];
    char *	string;
    void *	pSave;
    int		editorSave;

    string = one_argument(argument, arg);
    switch(which_keyword(arg,"dobj","dmob","dchar","droom",
                         "obj","mob","char","room","mud","newbie",
                         "prace","skill","class","group","clan",
                         "equipment","area","desc","files","mrace",
                         "notes","badnames","rlinks","alinks",
                         "social", "help", "mprog", "oprog",
                         "rprog", "aprog", NULL)) {
    default:
        send_to_char("Syntax:\n\r",ch);
        send_to_char("  stat obj <name>\n\r",ch);
        send_to_char("  stat mob <name>\n\r",ch);
        send_to_char("  stat dobj <name|vnum>\n\r",ch);
        send_to_char("  stat dmob <name|vnum>\n\r",ch);
        send_to_char("  stat char <name>\n\r",ch);
        send_to_char("  stat clan <name>\n\r",ch);
        send_to_char("  stat room [number]\n\r",ch);
        send_to_char("  stat rlinks [number]\n\r",ch);
        send_to_char("  stat alinks\n\r",ch);
        send_to_char("  stat mud\n\r",ch);
        send_to_char("  stat newbie\n\r",ch);
        send_to_char("  stat equipment\n\r",ch);
        send_to_char("  stat prace [race]\n\r",ch);
        send_to_char("  stat mrace [race]\n\r",ch);
        send_to_char("  stat skill [skill]\n\r",ch);
        send_to_char("  stat class [class]\n\r",ch);
        send_to_char("  stat group [group]\n\r",ch);
        send_to_char("  stat area\n\r",ch);
        send_to_char("  stat desc\n\r",ch);
        send_to_char("  stat files\n\r",ch);
        send_to_char("  stat notes\n\r",ch);
        send_to_char("  stat badnames\n\r",ch);
        send_to_char("  stat social <name>\n\r",ch);
        send_to_char("  stat help [#.]<keyword>\n\r",ch);
        send_to_char("  stat mprog <name>\n\r",ch);
        send_to_char("  stat oprog <name>\n\r",ch);
        send_to_char("  stat rprog <vnum>\n\r",ch);
        send_to_char("  stat aprog <vnum>\n\r",ch);
        return;

    case  1: // dobj
    {
        OBJ_DATA *	obj;
        OBJ_INDEX_DATA *pObj;

        obj=get_obj_world(ch,string);
        if (obj!=NULL)
            pObj=get_obj_index(obj->pIndexData->vnum);
        else
            pObj=get_obj_index(atoi(string));

        if (pObj==NULL) {
            send_to_char("No such object in hell, earth and heaven.\n\r",ch);
            return;
        }

        editorSave=ch->desc->editor;
        pSave=ch->desc->pEdit;
        ch->desc->pEdit=(void *)pObj;
        ch->desc->editor=ED_OBJECT;
        oedit_show(ch,"");
        ch->desc->pEdit=pSave;
        ch->desc->editor=editorSave;
        return;
    }

    case  2: // dmob
    {
        CHAR_DATA *	victim;
        MOB_INDEX_DATA *pMob;

        pMob=NULL;
        victim=get_char_world(ch,string);
        if (victim!=NULL && IS_NPC(victim))
            pMob=get_mob_index(victim->pIndexData->vnum);
        else
            pMob=get_mob_index(atoi(string));

        if (pMob==NULL) {
            send_to_char("No such creature in hell, earth and heaven.\n\r",ch);
            return;
        }
        editorSave=ch->desc->editor;
        pSave=ch->desc->pEdit;
        ch->desc->pEdit=(void *)pMob;
        ch->desc->editor=ED_MOBILE;
        medit_show(ch,"");
        ch->desc->pEdit=pSave;
        ch->desc->editor=editorSave;
        return;
    }

    case  3: // dchar
        send_to_char("This doesn't work. (yet?)\n\r",ch);
        return;

    case  4: // droom
        redit_show(ch,"");
        return;

    case 5: // obj
        stat_object(ch,string);
        return;

    case 6: // mob
    case 7: // char
        stat_mobile(ch,string);
        return;

    case 8: // room
        stat_room(ch,string);
        return;

    case 9: // mud
        stat_mud(ch);
        return;

    case 10: // newbie
        stat_newbie(ch);
        return;

    case 11: // prace
        stat_prace(ch,string);
        return;

    case 12: // skill
        stat_skill(ch,string);
        return;

    case 13: // class
        stat_class(ch,string);
        return;

    case 14: // group
        stat_group(ch,string);
        return;

    case 15: // clan
        stat_clan(ch,string);
        return;

    case 16: // equipment
        stat_equipment(ch,string);
        return;

    case 17: // area
        editorSave=ch->desc->editor;
        pSave=ch->desc->pEdit;
        ch->desc->pEdit = (void *)ch->in_room->area;
        ch->desc->editor = ED_AREA;
        aedit_show( ch, "" );
        ch->desc->pEdit = pSave;
        ch->desc->editor = editorSave;
        return;

    case 18: // descriptor
        stat_descriptor(ch,string);
        return;

    case 19: // files
        stat_files(ch);
        return;

    case 20: // mrace
        stat_mrace(ch,string);
        return;

    case 21: // notes
        stat_notes(ch);
        return;

    case 22: // badnames
        stat_badnames(ch);
        return;

    case 23: // rlinks
        stat_rlinks(ch,string);
        return;

    case 24: // alinks
        stat_alinks(ch);
        return;

    case 25: // social
        stat_social(ch,string);
        return;

    case 26: // help
        stat_help(ch,string);
        return;

    case 27: // mprog
        stat_mprog(ch,string);
        return;

    case 28: // oprog
        stat_oprog(ch,string);
        return;

    case 29: // rprog
        stat_rprog(ch,string);
        return;

    case 30: // aprog
        stat_aprog(ch,string);
        return;
    }
}


void stat_files(CHAR_DATA *ch) {
    FS_ENTRY *fs;

    fs=fs_list;
    while (fs) {
        sprintf_to_char(ch,"[%2d] %6d %6d %s\n\r",
                        fs->descriptor,
                        fs->size,
                        fs->offset,
                        fs->filename);
        fs=fs->next;
    }
}


void stat_room(CHAR_DATA *ch,char *argument) {
    char		buf[MSL];
    char		arg[MIL];
    ROOM_INDEX_DATA *	location;
    OBJ_DATA *		obj;
    CHAR_DATA *		rch;
    int			door;

    one_argument(argument,arg);
    location=(arg[0]==0) ? ch->in_room : find_location(ch,arg);
    if (location==NULL) {
        send_to_char("No such location.\n\r",ch);
        return;
    }

    if (!is_room_owner(ch,location) &&
            ch->in_room != location &&
            !is_allowed_in_room(ch,location) &&
            room_is_private( location ) && !IS_TRUSTED(ch,LEVEL_COUNCIL)) {
        send_to_char( "That room is private right now.\n\r", ch );
        return;
    }

    sprintf_to_char(ch, "{yName: {x'%s'\n\r{yArea: {x'%s'\n\r",
                    location->name,
                    location->area->name );

    sprintf_to_char(ch,
                    "{yVnum: {x%d  {ySector: {x%d  {yLight: {x%d\n\r",
                    location->vnum,
                    location->sector_type,
                    location->light );

    sprintf_to_char(ch,"{yRoom flags: {x[%s]\n\r",
                    option_string(location->strbit_room_flags,room_options));

    sprintf_to_char(ch,
                    "{yDescription:{x\n\r%s",
                    location->description );

    if ( location->extra_descr != NULL ) {
        EXTRA_DESCR_DATA *ed;

        send_to_char( "{yExtra description keywords: {x'", ch );
        for ( ed = location->extra_descr; ed; ed = ed->next ) {
            send_to_char( ed->keyword, ch );
            if ( ed->next != NULL )
                send_to_char( " ", ch );
        }
        send_to_char( "'.\n\r", ch );
    }

    send_to_char( "{yCharacters:{x", ch );
    for ( rch = location->people; rch; rch = rch->next_in_room ) {
        if (can_see(ch,rch)) {
            send_to_char( " ", ch );
            one_argument( rch->name, buf );
            send_to_char( buf, ch );
        }
    }

    send_to_char( ".\n\r{yObjects:   {x", ch );
    for ( obj = location->contents; obj; obj = obj->next_content ) {
        send_to_char( " ", ch );
        one_argument( obj->name, buf );
        send_to_char( buf, ch );
    }
    send_to_char( ".\n\r", ch );

    sprintf_to_char(ch,"{yTriggers:{x [%s]\n\r",
                    table_string(location->strbit_rprog_triggers,roomtrigger_table));

    if (location->pueblo_picture[0])
        sprintf_to_char(ch,"{yPicture {x%s\n\r",
                        url(buf,location->pueblo_picture,location->area,TRUE));

    for ( door = 0; door < DIR_MAX; door++ ) {
        EXIT_DATA *pexit;

        if ( ( pexit = location->exit[door] ) != NULL ) {
            sprintf_to_char(ch,
                            "{yDoor: {x%s.  {yTo: {x%d.  {yKey: {x%d.  {yExit flags: {x%d.\n\r{yKeyword: {x'%s'.  {yDescription: {x%s",

                            dir_name[door],
                            (pexit->to_room == NULL ? -1 : pexit->to_room->vnum),
                            pexit->key,
                            pexit->exit_info,
                            pexit->keyword,
                            pexit->description[0] != '\0'
                    ? pexit->description : "(none).\n\r" );
        }
    }

    show_properties(ch,location->property,"temp");

    send_to_char( "{x", ch );
    return;
}



void stat_object( CHAR_DATA *ch, char *argument ) {
    char arg[MAX_INPUT_LENGTH];
    char buf[MAX_INPUT_LENGTH];
    EFFECT_DATA *pef;
    OBJ_DATA *obj;

    one_argument( argument, arg );

    if ( arg[0]==0 ) {
        send_to_char( "Stat what?\n\r", ch );
        return;
    }

    if ( ( obj = get_obj_world( ch, argument ) ) == NULL ) {
        send_to_char( "Nothing like that in hell, earth, or heaven.\n\r", ch );
        return;
    }

    sprintf_to_char(ch, "{yName(s): {x%s\n\r",obj->name );

    sprintf_to_char(ch,
                    "{yVnum: {x%d {yId: {x%d {yType: {x%s\n\r",
                    obj->pIndexData->vnum,
                    obj->id,
                    item_type(obj));

    sprintf_to_char(ch,
                    "{yResets: {x%d  {yResetted at: {x%d\n\r",
                    obj->pIndexData->reset_num,
                    obj->resetted_at);

    sprintf_to_char(ch,
                    "{yShort description: {x%s\n\r{yLong description: {x%s\n\r",
                    obj->short_descr, obj->description );

    sprintf_to_char(ch,"{yMaterial: {x[%s]\n\r", obj->material);
    if (obj->pIndexData->pueblo_picture[0])
        sprintf_to_char(ch,"{yPicture {x%s\n\r",
                        url(buf,obj->pIndexData->pueblo_picture,obj->pIndexData->area,TRUE));

    sprintf_to_char(ch,
                    "{yWear bits: {x[%s]\n\r",
                    obj_wear_string(obj));
    sprintf_to_char(ch,
                    "{yExtra bits: {x[%s]\n\r",
                    obj_extra_string(obj));

    sprintf_to_char(ch,"{yTriggers:{x [%s]\n\r",
                    table_string(obj->strbit_oprog_triggers,objtrigger_table));

    sprintf_to_char(ch,
                    "{yNumber: {x%d/%d  {yWeight: {x%d/%d/%d (10th pounds)\n\r",
                    1,           get_obj_number( obj ),
                    obj->weight, get_obj_weight( obj ),get_true_weight(obj) );

    sprintf_to_char(ch,
                    "{yLevel: {x%d  {yCost: {x%d  {yCondition: {x%d  {yTimer: {x%d\n\r",
                    obj->level, obj->cost, obj->condition, obj->timer );

    sprintf_to_char(ch,
                    "{yIn room: {x%d {yIn object: {x%s {yCarried by: {x%s {yWear_loc: {x%d\n\r",
                    obj->in_room    == NULL    ?        0 : obj->in_room->vnum,
                    obj->in_obj     == NULL    ? "(none)" : obj->in_obj->short_descr,
                    obj->carried_by == NULL    ? "(none)" :
                                                 can_see(ch,obj->carried_by) ? obj->carried_by->name
                                                                             : "someone",
                    obj->wear_loc );

    sprintf_to_char(ch, "{yValues: {x%d %d %d %d %d\n\r",
                    obj->value[0], obj->value[1], obj->value[2], obj->value[3],
            obj->value[4] );

    /* now give out vital statistics as per identify */

    switch ( obj->item_type )
    {
    case ITEM_SCROLL:
    case ITEM_POTION:
    case ITEM_PILL:
        sprintf_to_char(ch, "{yLevel {x%d {yspells of:{x", obj->value[0] );

        if ( obj->value[1] >= 0 && obj->value[1] < MAX_SKILL )
            sprintf_to_char(ch," '%s'",skill_name(obj->value[1],ch));

        if ( obj->value[2] >= 0 && obj->value[2] < MAX_SKILL )
            sprintf_to_char(ch," '%s'",skill_name(obj->value[2],ch));

        if ( obj->value[3] >= 0 && obj->value[3] < MAX_SKILL )
            sprintf_to_char(ch," '%s'",skill_name(obj->value[3],ch));

        if (obj->value[4] >= 0 && obj->value[4] < MAX_SKILL)
            sprintf_to_char(ch," '%s'",skill_name(obj->value[4],ch));

        send_to_char( ".\n\r", ch );
        break;

    case ITEM_WAND:
    case ITEM_STAFF:
        sprintf_to_char(ch, "{yHas {x%d(%d) {ycharges of level {x%d",
                        obj->value[1], obj->value[2], obj->value[0] );

        if ( obj->value[3] >= 0 && obj->value[3] < MAX_SKILL )
            sprintf_to_char(ch," '%s'",skill_name(obj->value[3],ch));
        send_to_char( ".\n\r", ch );
        break;

    case ITEM_DRINK_CON:
        sprintf_to_char(ch,"{yIt holds {x%s{y-colored {x%s.\n\r",
                        liq_table[obj->value[2]].liq_color,
                liq_table[obj->value[2]].liq_name);
        break;


    case ITEM_WEAPON:
        send_to_char("{yWeapon type is {x",ch);
        switch (obj->value[0])
        {
        case(WEAPON_EXOTIC):
            send_to_char("exotic\n\r",ch);
            break;
        case(WEAPON_SWORD):
            send_to_char("sword\n\r",ch);
            break;
        case(WEAPON_DAGGER):
            send_to_char("dagger\n\r",ch);
            break;
        case(WEAPON_SPEAR):
            send_to_char("spear/staff\n\r",ch);
            break;
        case(WEAPON_MACE):
            send_to_char("mace/club\n\r",ch);
            break;
        case(WEAPON_AXE):
            send_to_char("axe\n\r",ch);
            break;
        case(WEAPON_FLAIL):
            send_to_char("flail\n\r",ch);
            break;
        case(WEAPON_WHIP):
            send_to_char("whip\n\r",ch);
            break;
        case(WEAPON_POLEARM):
            send_to_char("polearm\n\r",ch);
            break;
        default:
            send_to_char("unknown\n\r",ch);
            break;
        }
        sprintf_to_char(ch,"{yDamage is {x%dd%d {y(average {x%d{y)\n\r",
                        obj->value[1],obj->value[2],
                (1 + obj->value[2]) * obj->value[1] / 2);

        sprintf_to_char(ch,"{yDamage noun is {x%s.\n\r",
                        (obj->value[3] > 0 && obj->value[3] < MAX_DAMAGE_MESSAGE) ?
                    attack_table[obj->value[3]].noun : "undefined");

        if (obj->value[4])  /* weapon flags */
        {
            sprintf_to_char(ch,"{yWeapons flags: {x%s\n\r",
                            obj_weaponflag_string(obj));
        }
        break;

    case ITEM_ARMOR:
        sprintf_to_char(ch,
                        "{yArmor: pierce {x%d {ybash {x%d {yslash {x%d {ymagic {x%d\n\r",
                        obj->value[0], obj->value[1], obj->value[2], obj->value[3] );
        break;

    case ITEM_CONTAINER:
        sprintf_to_char(ch,
                        "{yCapacity: {x%d#  {yMaximum weight: {x%d#  {yflags: {x%s\n\r",
                        obj->value[0], obj->value[3],obj_container_string(obj));
        if (obj->value[4] != 100)
            sprintf_to_char(ch,"{yWeight multiplier: {x%d%%\n\r",
                            obj->value[4]);
        break;

    case ITEM_JUKEBOX:
        if (obj->value[0]<=0)
            sprintf_to_char(ch,"{yPlays all songs{x\n\r");
        else
            sprintf_to_char(ch,"{yPlays playlist {x%d\n\r",obj->value[0]);
        break;

    }


    if ( obj->extra_descr != NULL || obj->pIndexData->extra_descr != NULL )
    {
        EXTRA_DESCR_DATA *ed;

        send_to_char( "{yExtra description keywords: {x'", ch );

        for ( ed = obj->extra_descr; ed != NULL; ed = ed->next )
        {
            send_to_char( ed->keyword, ch );
            if ( ed->next != NULL )
                send_to_char( " ", ch );
        }

        send_to_char("' '",ch);

        for ( ed = obj->pIndexData->extra_descr; ed != NULL; ed = ed->next )
        {
            send_to_char( ed->keyword, ch );
            if ( ed->next != NULL )
                send_to_char( " ", ch );
        }

        send_to_char( "'\n\r", ch );
    }

    for ( pef = obj->affected; pef != NULL; pef = pef->next )
    {
        sprintf_to_char(ch, "{yRemovable effect {x%s {yby {x%d, {ylevel {x%d",
                        effect_loc_name(pef), pef->modifier,pef->level );
        if ( pef->duration > -1)
            sprintf_to_char(ch,", {x%d {yhours. ",pef->duration);
        else
            send_to_char(". ",ch);
        if (pef->bitvector)
        {
            switch(pef->where)
            {
            case TO_EFFECTS:
                sprintf_to_char(ch,"{yAdds {x%s {yeffect.",
                                effect_bit_name(pef));
                break;
            case TO_EFFECTS2:
                sprintf_to_char(ch,"{yAdds extra {x%s {yeffect.",
                                option_find_value(pef->bitvector,effect2_options));
                break;
            case TO_OBJECT:
                sprintf_to_char(ch,"{yAdds {x%s {yobject flag.",
                                option_find_value(pef->bitvector,extra_options));
                break;
            case TO_OBJECT2:
                sprintf_to_char(ch,"{yAdds extra {x%s {yobject flag.",
                                option_find_value(pef->bitvector,extra2_options));
                break;
            case TO_IMMUNE:
                sprintf_to_char(ch, "{yAdds immunity to {x%s.",
                                option_find_value(pef->bitvector,imm_options));
                break;
            case TO_RESIST:
                sprintf_to_char(ch, "{yAdds resistance to {x%s.",
                                option_find_value(pef->bitvector,imm_options));
                break;
            case TO_VULN:
                sprintf_to_char(ch, "{yAdds vulnerability to {x%s.",
                                option_find_value(pef->bitvector,imm_options));
                break;
            case TO_WEAPON:
                sprintf_to_char(ch,"{yAdds {x%s {yweapon flags.",
                                option_find_value(pef->bitvector,weaponflag_options));
                break;
            case TO_SKILLS:
                sprintf_to_char(ch,"{yAdds {x%s {yproficiency.",
                                skill_name(pef->bitvector,ch));
                break;
            default:
                sprintf_to_char(ch, "{yUnknown bit {x%d: %d",
                                pef->where,pef->bitvector);
                break;
            }
        }
        send_to_char("\n\r",ch);
    }

    if (!obj->enchanted)
        for ( pef = obj->pIndexData->affected; pef != NULL; pef = pef->next )
        {
            sprintf_to_char(ch,"{yStatic effect {x%s {yby {x%d{y, level {x%d. ",
                            effect_loc_name(pef), pef->modifier,pef->level );
            if (pef->bitvector) {
                switch(pef->where) {
                case TO_EFFECTS:
                    sprintf_to_char(ch,"{yAdds {x%s {yeffect.",
                                    effect_bit_name(pef));
                    break;
                case TO_EFFECTS2:
                    sprintf_to_char(ch,"{yAdds extra {x%s {yeffect.",
                                    effect2_bit_name(pef));
                    break;
                case TO_OBJECT:
                    sprintf_to_char(ch,"{yAdds {x%s {yobject flag.",
                                    option_find_value(pef->bitvector,extra_options));
                    break;
                case TO_OBJECT2:
                    sprintf_to_char(ch,"{yAdds extra {x%s {yobject flag.",
                                    option_find_value(pef->bitvector,extra2_options));
                    break;
                case TO_IMMUNE:
                    sprintf_to_char(ch,"{yAdds immunity to {x%s.",
                                    option_find_value(pef->bitvector,imm_options));
                    break;
                case TO_RESIST:
                    sprintf_to_char(ch,"{yAdds resistance to {x%s.",
                                    option_find_value(pef->bitvector,imm_options));
                    break;
                case TO_VULN:
                    sprintf_to_char(ch,"{yAdds vulnerability to {x%s.",
                                    option_find_value(pef->bitvector,imm_options));
                    break;
                case TO_WEAPON:
                    sprintf_to_char(ch,"{yAdds {x%s {yweapon flags.",
                                    option_find_value(pef->bitvector,weaponflag_options));
                    break;
                case TO_SKILLS:
                    sprintf_to_char(ch,"{yAdds {x%s {yproficiency.",
                                    skill_name(pef->bitvector,ch));
                    break;
                default:
                    sprintf_to_char(ch,"{yUnknown bit {x%d{y: {x%d",
                                    pef->where,pef->bitvector);
                    break;
                }
            }
            send_to_char("\n\r",ch);
        }

    if (obj_is_owned(obj))
        sprintf_to_char(ch,"{yOwned by {x%s\n\r",obj->owner_name);

    if (obj->pIndexData)
        show_properties(ch,obj->pIndexData->property,"perm");
    show_properties(ch,obj->property,"temp");

    send_to_char( "{x", ch );
    return;
}



void stat_mobile( CHAR_DATA *ch, char *argument ) {
    char buf2[MAX_STRING_LENGTH];
    char arg[MAX_INPUT_LENGTH];
    EFFECT_DATA *pef;
    CHAR_DATA *victim;

    one_argument( argument, arg );

    if ( arg[0]==0 ) {
        send_to_char( "Stat whom?\n\r", ch );
        return;
    }

    if ( ( victim = get_char_world( ch, argument ) ) == NULL ) {
        send_to_char( "They aren't here.\n\r", ch );
        return;
    }

    sprintf_to_char(ch, "{yName: {x%s\n\r",victim->name);

    if (IS_NPC(victim))
        sprintf_to_char(ch, "{yVnum: {x%d   {yId: {x%d   {yGroup: {x%d\n\r",
                        victim->pIndexData->vnum,
                        victim->id,
                        victim->pIndexData->group);

    sprintf_to_char(ch,
                    "{yFormat: {x%s  {yRace: {x%s  {ySex: {x%s{y   Room: {x%d\n\r",
                    IS_NPC(victim) ? "mob" : "pc",
                    race_table[victim->race].name,
            table_find_value(show_sex(victim),sex_table),
            victim->in_room == NULL ? 0 : victim->in_room->vnum);

    if (IS_NPC(victim))
        sprintf_to_char(ch,"{yCount: {x%d  {yKilled: {x%d\n\r",
                        victim->pIndexData->count,
                        victim->pIndexData->killed);
    else
        sprintf_to_char(ch,"{yKilled: {x%d\n\r",
                        victim->pcdata->killed);

    sprintf_to_char(ch,
                    "{yStr: {x%d(%d)  {yInt: {x%d(%d)  {yWis: {x%d(%d)  {yDex: {x%d(%d)  {yCon: {x%d(%d)\n\r",
                    victim->perm_stat[STAT_STR],
                    get_curr_stat(victim,STAT_STR),
                    victim->perm_stat[STAT_INT],
                    get_curr_stat(victim,STAT_INT),
                    victim->perm_stat[STAT_WIS],
                    get_curr_stat(victim,STAT_WIS),
                    victim->perm_stat[STAT_DEX],
                    get_curr_stat(victim,STAT_DEX),
                    victim->perm_stat[STAT_CON],
                    get_curr_stat(victim,STAT_CON) );

    sprintf_to_char(ch,"{yHp: {x%d/%d  {yMana: {x%d/%d  {yMove: {x%d/%d\n\r",
                    victim->hit,	victim->max_hit,
                    victim->mana,	victim->max_mana,
                    victim->move,	victim->max_move);

    if(IS_PC(victim) && victim->trust)
        sprintf(buf2,"{yTrust: {x%d  ",victim->trust);
    else buf2[0]='\0';
    sprintf_to_char(ch,
                    "{yLv: {x%d  %s{yAlign: {x%d  {yGold: {x%ld  {ySilver: {x%ld  {yExp: {x%d\n\r",
                    victim->level,
                    buf2,
                    victim->alignment,
                    victim->gold, victim->silver, victim->exp );

    sprintf_to_char(ch,
                    "{yClass:{x %s   {yEditing:{x %s\n\r",
                    IS_NPC(victim) ? "mobile" : class_table[victim->class].name,
                    ch->pnote==NULL?"no":
                                    ch->pnote->type==NOTE_NOTE?"note":
                                                               ch->pnote->type==NOTE_IDEA?"idea":
                                                                                          ch->pnote->type==NOTE_BUG ?"bug":
                                                                                                                     ch->pnote->type==NOTE_TYPO?"typo":
                                                                                                                                                ch->pnote->type==NOTE_PENALTY?"penalty":
                                                                                                                                                                              ch->pnote->type==NOTE_NEWS?"news":
                                                                                                                                                                                                         ch->pnote->type==NOTE_CHANGES?"changes":
                                                                                                                                                                                                                                       ch->pnote->type==NOTE_MCNOTE?"McNote":
                                                                                                                                                                                                                                                                    "something else"
                                                                                                                                                                                                                                                                    );

    sprintf_to_char(ch,
                    "{yArmor: pierce: {x%d  {ybash: {x%d  {yslash: {x%d  {yexotic: {x%d\n\r",
                    GET_AC(victim,AC_PIERCE), GET_AC(victim,AC_BASH),
                    GET_AC(victim,AC_SLASH),  GET_AC(victim,AC_EXOTIC));

    sprintf_to_char( ch,
                     "{yHit: {x%d  {yDam: {x%d  {ySaves: {x%d  {ySize: {x%s  {yWimpy: {x%d\n\r",
                     GET_HITROLL(victim), GET_DAMROLL(victim), victim->saving_throw,
                     size_table[victim->size].name, victim->wimpy );

    sprintf_to_char( ch,"{yResetted at{x %d  {yPosition: {x%s{y   On: {x%s{y\n\r",
                     victim->resetted_at,
                     position_table[victim->position].name,
            victim->on==NULL?"(nothing)":victim->on->short_descr);

    if (IS_NPC(victim)) {
        sprintf_to_char(ch, "{yDamage: {x%dd%d  {yMessage:  {x%s\n\r",
                        victim->damage[DICE_NUMBER],victim->damage[DICE_TYPE],
                        attack_table[victim->dam_type].noun);
    }
    sprintf_to_char(ch, "{yWait/Daze; {x%d/%d   {yFighting: {x%s\n\r",
                    victim->wait,victim->daze,
                    victim->fighting ? victim->fighting->name : "(none)" );

    sprintf_to_char(ch,"{yHunt-type: {x%s",hunt_table[victim->hunt_type]);
    if (victim->hunt_type!=HUNT_NONE)
        sprintf_to_char(ch,"{y for id{x%d",victim->hunt_id);
    send_to_char("\n\r",ch);

    if ( IS_PC(victim) )
        sprintf_to_char( ch,
                         "{yThirst: {x%d  {yHunger: {x%d  {yFull: {x%d  {yDrunk: {x%d  {yExcited: {x%d\n\r"
                         "{yQuestPoints: {x%d   {yPractices: {x%d   {yTraining: {x%d\n\r",
                         victim->pcdata->condition[COND_THIRST],
                         victim->pcdata->condition[COND_HUNGER],
                         victim->pcdata->condition[COND_FULL],
                         victim->pcdata->condition[COND_DRUNK],
                         victim->pcdata->condition[COND_ADRENALINE],
                         victim->pcdata->questpoints,
                         victim->practice,
                         victim->train);

    sprintf_to_char(ch,
                    "{yCarry number: {x%d  {yCarry weight: {x%ld  {yBank: {x%ld {ysilver{x\n\r",
                    victim->carry_number, get_carry_weight(victim) / 10,
                    IS_NPC(victim)?0:victim->pcdata->bank);

    if (!IS_NPC(victim)) {
        sprintf_to_char(ch,
                        "{yAge: {x%d  {yPlayed: {x%d  {yLast Level: {x%d  {yTimer: {x%d\n\r",
                        get_age(victim),
                        (int) (victim->played + current_time - victim->logon) / 3600,
                        victim->pcdata->last_level,
                        victim->timer );
    }

    if (IS_NPC(victim))
        sprintf_to_char(ch, "{yAct: {x[%s]\n\r",char_act_string(victim));
    else
        sprintf_to_char(ch, "{yAct: {x[%s]\n\r",char_plr_string(victim));

    if (victim->strbit_comm)
        sprintf_to_char(ch,"{yComm: {x[%s]\n\r",char_comm_string(victim));

    if (IS_NPC(victim) &&
            !STR_SAME_STR(victim->strbit_off_flags,ZERO_FLAG,MAX_FLAGS))
        sprintf_to_char(ch,
                        "{yOffense: {x[%s]\n\r",char_off_string(victim));

    if (!STR_SAME_STR(victim->strbit_imm_flags,ZERO_FLAG,MAX_FLAGS))
        sprintf_to_char(ch,
                        "{yImmune: {x[%s]\n\r",char_imm_string(victim));

    if (!STR_SAME_STR(victim->strbit_res_flags,ZERO_FLAG,MAX_FLAGS))
        sprintf_to_char(ch,
                        "{yResist: {x[%s]\n\r", char_res_string(victim));

    if (!STR_SAME_STR(victim->strbit_vuln_flags,ZERO_FLAG,MAX_FLAGS))
        sprintf_to_char(ch,
                        "{yVulnerable: {x[%s]\n\r", char_vuln_string(victim));

    sprintf_to_char(ch, "{yForm: {x[%s]\n\r",
                    char_form_string(victim));
    sprintf_to_char(ch, "{yParts: {x[%s]\n\r",
                    char_part_string(victim));

    sprintf_to_char(ch,
                    "{yAffected by {x[%s]\n\r",char_affected_string(victim));

    sprintf_to_char(ch,"{yTriggers:{x [%s]\n\r",
                    table_string(victim->strbit_mprog_triggers,mobtrigger_table));

    sprintf_to_char(ch, "{yMaster: {x%s  {yLeader: {x%s  {yPet: {x%s  ",
                    victim->master	? victim->master->name   : "(none)",
                    victim->leader	? victim->leader->name   : "(none)",
                    victim->pet	? victim->pet->name	     : "(none)");

    sprintf_to_char(ch, "{ySnooped by: {x%s\n\r",
                    victim->desc ? victim->desc->snoop_by ?
                                       can_see(ch,victim->desc->snoop_by->character) ?
                                           victim->desc->snoop_by->character->name : "(none)" : "(none)" : "(no descriptor)");

    if ( !IS_NPC( victim ) )	/* OLC */
        sprintf_to_char(ch, "{ySecurity: {x%d.\n\r", victim->pcdata->security );

    sprintf_to_char(ch, "{yRecall: {x%d  {yPhasedskill-step {x%d\n\r",
                    victim->recall,victim->phasedskill_step);


    sprintf_to_char(ch, "{yShort description: {x%s\n\r{yLong description: {x%s",
                    victim->short_descr,
                    victim->long_descr[0] != '\0' ? victim->long_descr : "(none)\n\r" );

    if ( victim->extra_descr != NULL ) {
        EXTRA_DESCR_DATA *ed;

        send_to_char( "{yExtra description keywords: {x'", ch );
        for ( ed = victim->extra_descr; ed; ed = ed->next ) {
            send_to_char( ed->keyword, ch );
            if ( ed->next != NULL )
                send_to_char( " ", ch );
        }
        if (IS_NPC(victim)) {
            send_to_char( "' '", ch );
            for ( ed = victim->pIndexData->extra_descr; ed; ed = ed->next ) {
                send_to_char( ed->keyword, ch );
                if ( ed->next != NULL )
                    send_to_char( " ", ch );
            }
        }
        send_to_char( "'.\n\r", ch );
    }

#ifdef HAS_ALTS
    if (IS_PC(victim)) {
        if (victim->pcdata->uberalt[0])
            sprintf_to_char(ch, "{yUberAlt: {x%s\n\r",victim->pcdata->uberalt);
        if (victim->pcdata->alts[0])
            sprintf_to_char(ch, "{yAlts: {x%s\n\r",victim->pcdata->alts);
    }
#endif

    if (IS_NPC(victim) && victim->pIndexData->pueblo_picture[0])
        sprintf_to_char(ch,"{yPicture{x %s\n\r",
                        url(buf2,victim->pIndexData->pueblo_picture,victim->pIndexData->area,TRUE));
    if (IS_PC(victim) && victim->pcdata->pueblo_picture[0])
        sprintf_to_char(ch,"{yPicture: {x%s\n\r",
                        victim->pcdata->pueblo_picture);
    if ( IS_NPC(victim) && victim->spec_fun != 0 )
        sprintf_to_char(ch,"{yMobile has special procedure {x%s.\n\r",
                        spec_string(victim->spec_fun));

    for ( pef = victim->affected; pef != NULL; pef = pef->next )
        sprintf_to_char( ch,
                         "{ySpell: {x'%s' {ymodifies {x%s {yby {x%d {yfor {x%d {yhours with bits {x%s:%s, {ylevel {x%d.\n\r",
                         pef->type==-1?"(unknown)":skill_name(pef->type,ch),
                         effect_loc_name(pef),
                         pef->modifier,
                         pef->duration,
                         table_find_value(pef->where,effect_where_table),
                         eff_bit_name(pef),
                         pef->level
                         );

    if (victim->pIndexData)
        show_properties(ch,victim->pIndexData->property,"perm");
    show_properties(ch,victim->property,"temp");

    send_to_char( "{x", ch );
    return;
}


void stat_clan(CHAR_DATA *ch,char *argument) {
    CLAN_INFO *		clan;
    CLANMEMBER_TYPE *	member;
    int			members,i;
    BUFFER *		output;
    char		s[MSL];
    OBJ_DATA *		obj;

    if (argument[0]==0) {
        clan=clan_list;
        i=0;
        while (clan!=NULL) {
            sprintf_to_char(ch,"{y[{x%-17s{y]{x ",clan->name);
            clan=clan->next;
            if (!clan || (++i)%4==0)
                send_to_char("\n\r",ch);
        }
        return;
    }

    if ((clan=get_clan_by_name(argument))==NULL) {
        stat_clan(ch,"");
        return;
    }

    members=0;
    if (!clan->independent)
        for (i=0;i<MAX_CLAN_RANK;i++)
            for (member=clan->members;member!=NULL;member=member->next_member)
                members++;

    output=new_buf();
    sprintf(s,	"{yWho Entry  : {x%s\n\r"
                "{yMembers    : {x%d\n\r"
                "{yIndependent: {x%s\n\r"
                "{yRecall     : {x%d\n\r"
                "{yHall       : {x%d\n\r"
                "{yWealth     : {x%s\n\r"
                "{yDeleted    : {x%s\n\r"
                "{yURL        : {x%s\n\r"
                "{yConquered  : {x\n\r",
            clan->who_name,
            members,
            clan->independent?"Yes":"No",
            clan->recall,
            clan->hall,
            money_string(clan->coins/100,clan->coins%100),
            clan->deleted?"{Ryes{x":"no",
            clan->url
            );
    add_buf(output,s);
    for ( obj = object_list; obj != NULL; obj = obj->next ) {
        if (!STR_IS_SET(obj->strbit_extra_flags,ITEM_INSTALLED)) continue;
        if (!obj->in_room) continue; // should not be needed, but just in case
        if (!GetObjectProperty(obj,PROPERTY_STRING,"clanitem",s)) {
            // installed and no clan name
            STR_REMOVE_BIT(obj->strbit_extra_flags,ITEM_INSTALLED);
            continue;
        }
        if (get_clan_by_name(s)!=clan) continue;
        sprintf(s,"%s ([%d] %s)\n\r",obj->in_room->area->name,obj->in_room->vnum,obj->in_room->name);
        add_buf(output,s);
    }
    add_buf(output,"{yDescription: {x\n\r");

    if (clan->description[0]==0)
        add_buf(output,"No description available.\n\r");
    else
        add_buf(output,clan->description);
    page_to_char(buf_string(output),ch);
    free_buf(output);
    return;
}


void stat_descriptor(CHAR_DATA *ch,char *argument) {
    int			iDesc;
    DESCRIPTOR_DATA *	desc;
    char		buf[160];
    char		buf2[160];
    int i,j;

    if ((iDesc=atoi(argument))<=0) {
        send_to_char("Syntax: stat desc <number>\n\r",ch);
        return;
    }

    for (desc=descriptor_list;desc!=NULL;desc=desc->next)
        if (desc->descriptor==iDesc)
            break;
    if (desc==NULL) {
        sprintf_to_char(ch,"Descriptor %d not found.\n\r",iDesc);
        return;
    }

    sprintf_to_char(ch,"{yCharacter: {x%s\n\r",
                    desc->character?desc->character->name[0]!=0?
                desc->character->name:"(not logged in yet)":
                "(no character yet)");

    sprintf_to_char(ch,"{yfcommand    : {x%s\n\r",
                    desc->fcommand?"true":"false");
    sprintf_to_char(ch,"{ytelnet      : {x%s\n\r",
                    table_string(desc->telnet_options,telnet_table));
    sprintf_to_char(ch,"{yterminal    : {x%s\n\r",
                    desc->terminaltype);
    sprintf_to_char(ch,"{ypueblo      : {x%3s     {ymxp      : {x%3s\n\r",
                    desc->pueblo?"yes":"no",desc->mxp?"yes":"no");
#ifdef HAS_MCCP
    sprintf_to_char(ch,"{ymccp        : {x%3s     {ymccp2    : {x%3s\n\r",
                    desc->mccp?"yes":"no",desc->mccp2?"yes":"no");
#endif
    sprintf_to_char(ch,"{ygo-ahead    : {x%3s     {ylines    : {x%d\n\r",
                    desc->go_ahead?"yes":"no",desc->lines);
    sprintf_to_char(ch,"{yidle        : {x%d seconds\n\r",time(NULL)-desc->idle);
    sprintf_to_char(ch,"{yrepeat      : {x%d\n\r",
                    desc->repeat);
    sprintf_to_char(ch,"{youtsize     : {x%7d {youttop   : {x%7d\n\r",
                    desc->outsize,desc->outtop);
    sprintf_to_char(ch,"{ysend        : {x%7d {yreceived : {x%7d\n\r",
                    desc->bytessend,desc->bytesreceived);
#ifdef HAS_MCCP
    if (desc->mccp || desc->mccp2) {
        sprintf_to_char(ch,"{ycompression : {x%7d {yfactor   : {x%7.2f\n\r",
                        desc->compressedsend,
                        100.0*desc->compressedsend/desc->bytessend);
    } else
        send_to_char(  "{ycompression : {xdisabled\n\r",ch);

#endif

    if (desc->inbuf) {
        strcpy(buf,"{yinbuf    :{x");
        for (i=0;i<4;i++) {
            for (j=0;j<16;j++) {
                if (desc->pinbuf==desc->inbuf+i*16+j)
                    strcat(buf,">");
                else
                    strcat(buf," ");
                sprintf(buf2,"%02x",(unsigned char)desc->inbuf[i*16+j]);
                strcat(buf,buf2);
            }
            strcat(buf," ");
            for (j=0;j<16;j++) {
                sprintf(buf2,"%c",
                        isprint(desc->inbuf[i*16+j])?desc->inbuf[i*16+j]:'.');
                strcat(buf,buf2);
            }
            sprintf_to_char(ch,"%s\n\r",buf);
            strcpy(buf,"          ");
        }
    }

    if (desc->incomm) {
        strcpy(buf,"{yincomm   : {x");
        for (i=0;i<4;i++) {
            for (j=0;j<16;j++) {
                sprintf(buf2,"%02x ",(unsigned char)desc->incomm[i*16+j]);
                strcat(buf,buf2);
            }
            for (j=0;j<16;j++) {
                sprintf(buf2,"%c",
                        isprint(desc->incomm[i*16+j])?desc->incomm[i*16+j]:'.');
                strcat(buf,buf2);
            }
            sprintf_to_char(ch,"%s\n\r",buf);
            strcpy(buf,"           ");
        }
    }

    if (desc->outbuf) {
        strcpy(buf,"{youtbuf   : {x");
        for (i=0;i<4;i++) {
            for (j=0;j<16;j++) {
                sprintf(buf2,"%02x ",(unsigned char)desc->outbuf[i*16+j]);
                strcat(buf,buf2);
            }
            for (j=0;j<16;j++) {
                sprintf(buf2,"%c",
                        isprint(desc->outbuf[i*16+j])?desc->outbuf[i*16+j]:'.');
                strcat(buf,buf2);
            }
            sprintf_to_char(ch,"%s{x\n\r",buf);
            strcpy(buf,"           ");
        }
    }
}

void stat_equipment(CHAR_DATA *ch,char *argument) {
    CHAR_DATA *		victim;
    char		arg[MAX_STRING_LENGTH];
    int			iWear;
    OBJ_DATA *		obj;
    EFFECT_DATA *	pef;

    one_argument(argument,arg);
    if (arg[0]==0) {
        send_to_char("Stat whom?\n\r",ch);
        return;
    }

    if ((victim=get_char_world(ch,argument))==NULL) {
        send_to_char("They aren't here.\n\r",ch);
        return;
    }

    sprintf_to_char(ch,"{yName: {x%s\n\r",
                    IS_PC(victim)?victim->name:victim->short_descr);
    for (iWear=0;iWear<MAX_WEAR;iWear++) {

        if ((obj=get_eq_char(victim,iWear))==NULL)
            continue;

        sprintf_to_char(ch,"{y%s:{x %s\n\r",
                        where_name[iWear],obj->short_descr);

        if (obj->item_type==ITEM_ARMOR) {
            sprintf_to_char(ch,"                      {yarmor: "
                               "pierce {x%d {ybash {x%d {yslash {x%d {ymagic {x%d\n\r",
                            obj->value[0],obj->value[1],obj->value[2],obj->value[3]);
        }

        for (pef=obj->affected;pef!=NULL;pef=pef->next) {
            sprintf_to_char(ch,
                            "                      {yRemovable effect {x%s {yby {x%d\n\r",
                            effect_loc_name(pef),pef->modifier);
        }
        for (pef=obj->pIndexData->affected;pef!=NULL;pef=pef->next) {
            sprintf_to_char(ch,
                            "                      {yStatic effect {x%s {yby {x%d\n\r",
                            effect_loc_name(pef),pef->modifier);
        }
    }
}


void stat_prace(CHAR_DATA *ch,char *arg) {
    int		i,j;
    char	arg1[MAX_STRING_LENGTH];
    char	s1[MSL],s2[MSL];

    if (arg[0]==0) {
        for (i=1;i<MAX_PC_RACE;i++) {
            sprintf_to_char(ch," {y[{x%-14s{y]",pc_race_table[i].name);
            if (i%4==0) send_to_char("{x\n\r",ch);
        }
        if (i%4!=1) send_to_char("{x\n\r",ch);
        return;
    }

    arg=one_argument(arg,arg1);
    for (i=1;i<MAX_PC_RACE;i++) {
        if (!str_prefix(arg1,pc_race_table[i].name)) {

            for (j=1;race_table[i].name;j++) {
                if (!str_cmp(race_table[j].name,pc_race_table[i].name)) {
                    break;
                }
            }

            sprintf_to_char(ch,"{yrace{x %s\n\r",pc_race_table[i].name);
            sprintf_to_char(ch,"{y[Statistics  Min Max] Size   : {x%s\n\r",
                            size_table[pc_race_table[i].size]);
            sprintf_to_char(ch,
                            "{y[Strength     {x%2d  {x%2d{y] Points : {x%d\n\r",
                            pc_race_table[i].stats[STAT_STR],
                            pc_race_table[i].max_stats[STAT_STR],
                            pc_race_table[i].points);
            sprintf(s1,"%s",
                    option_string(race_table[j].strbit_eff,effect_options));
            sprintf(s2,"%s",
                    option_string(race_table[j].strbit_eff2,effect2_options));
            sprintf_to_char(ch,
                            "{y[Intelligence {x%2d  %2d{y] Effects: {x%s %s\n\r",
                            pc_race_table[i].stats[STAT_INT],
                            pc_race_table[i].max_stats[STAT_INT],
                            s1,s2);
            sprintf_to_char(ch,
                            "{y[Wisdom       {x%2d  %2d{y] Immumne: {x%s\n\r",
                            pc_race_table[i].stats[STAT_WIS],
                            pc_race_table[i].max_stats[STAT_WIS],
                            option_string(race_table[j].strbit_imm,imm_options));
            sprintf_to_char(ch,
                            "{y[Dexterity    {x%2d  %2d{y] Resist : {x%s\n\r",
                            pc_race_table[i].stats[STAT_DEX],
                            pc_race_table[i].max_stats[STAT_DEX],
                            option_string(race_table[j].strbit_res,res_options));
            sprintf_to_char(ch,
                            "{y[Constitution {x%2d  %2d{y] Vulner : {x%s\n\r",
                            pc_race_table[i].stats[STAT_CON],
                            pc_race_table[i].max_stats[STAT_CON],
                            option_string(race_table[j].strbit_vuln,res_options));

            sprintf_to_char(ch,
                            "{ySKILLS/SPELLS (skills you get only if class has them){x\n\r");
            for (j=0;j<5;j++) {		/* ieks! */
                if (pc_race_table[i].skills[j]) {
                    sprintf_to_char(ch,"%-18s ",pc_race_table[i].skills[j]);
                    if (j%4==3) send_to_char("\n\r",ch);
                } else
                    break;
            }
            if (j%4!=3) send_to_char("\n\r",ch);
            return;
        }
    }
}

void stat_mrace(CHAR_DATA *ch,char *arg) {
    int		i;
    char	arg1[MAX_STRING_LENGTH];
    char	s1[MSL],s2[MSL];

    if (arg[0]==0) {
        i=0;
        while (race_table[i].name!=NULL) {
            sprintf_to_char(ch," {y[{x%-14s{y]",race_table[i].name);
            if (i%4==3) send_to_char("{x\n\r",ch);
            i++;
        }
        if (i%4!=0) send_to_char("{x\n\r",ch);
        return;
    }


    arg=one_argument(arg,arg1);
    i=0;
    while (race_table[i].name) {
        if (!str_prefix(arg1,race_table[i].name)) {

            sprintf_to_char(ch,"{yrace{x %s",race_table[i].name);
            if (race_table[i].pc_race)
                sprintf_to_char(ch,
                                " {rSee also: stat prace %s{x",race_table[i].name);
            send_to_char("\n\r",ch);

            sprintf_to_char(ch,"{yForm   : {x%s\n\r",
                            option_string_from_long(race_table[i].form,form_options));
            sprintf_to_char(ch,"{yParts  : {x%s\n\r",
                            option_string_from_long(race_table[i].parts,part_options));
            sprintf(s1,"%s",
                    option_string(race_table[i].strbit_act,act_options));
            sprintf_to_char(ch,"{yAct    : {x%s\n\r",s1);
            sprintf_to_char(ch,"{yOff    : {x%s\n\r",
                            option_string(race_table[i].strbit_off,off_options));
            sprintf(s1,"%s",
                    option_string(race_table[i].strbit_eff,effect_options));
            sprintf(s2,"%s",
                    option_string(race_table[i].strbit_eff2,effect2_options));
            sprintf_to_char(ch,"{yEffects: {x%s %s\n\r",s1,s2);
            sprintf_to_char(ch,"{yImmumne: {x%s\n\r",
                            option_string(race_table[i].strbit_imm,imm_options));
            sprintf_to_char(ch,"{yResist : {x%s\n\r",
                            option_string(race_table[i].strbit_res,res_options));
            sprintf_to_char(ch,"{yVulner : {x%s\n\r",
                            option_string(race_table[i].strbit_vuln,vuln_options));

            return;
        }
        i++;
    }
}

void stat_skill(CHAR_DATA *ch,char *arg) {
    int		i;
    char	arg1[MAX_STRING_LENGTH];
    BUFFER *	output;
    char	s[MAX_STRING_LENGTH];
    int		race,class;

    if (arg[0]==0) {
        output=new_buf();
        for (i=1;i<MAX_SKILL&&skill_exists(i);i++) {
            sprintf(s," {y[{x%-16s{y]",skill_name(i,ch));
            add_buf(output,s);
            if (i%4==0) add_buf(output,"{x\n\r");
        }
        if (i%4!=1) add_buf(output,"{x\n\r");
        page_to_char(buf_string(output),ch);
        free_buf(output);
        return;
    }

    arg=one_argument(arg,arg1);
    for (i=0;i<MAX_SKILL&&skill_exists(i);i++) {
        if (!str_prefix(arg1,skill_name(i,ch))) {
            output=new_buf();
            sprintf(s,"{yName             : {x%s %s\n\r",
                    skill_name(i,ch),
                    skill_implemented(i,ch)?"":"({rnot yet implemented{x}");
            add_buf(output,s);

            sprintf(s,"{yWaiting time     : {x%d{y violence-pulse%s\n\r",
                    skill_beats(i,ch)/PULSE_VIOLENCE,
                    skill_beats(i,ch)/PULSE_VIOLENCE==1?"":"s");
            add_buf(output,s);
            sprintf(s,"{ySpell-function   : {x%s\n\r",
                    skill_spell_fun(i,ch)?"yes":"no");
            add_buf(output,s);
            sprintf(s,"{yWearoff-function : {x%s\n\r",
                    skill_wearoff_fun(i,ch)?"yes":"no");
            add_buf(output,s);
            sprintf(s,"{yTarget           : {x%s\n\r",
                    target_flags[skill_target(i,ch)]);
            add_buf(output,s);
            sprintf(s,"{yMinimum position : {x%s\n\r",
                    position_table[skill_minimum_position(i,ch)].name);
            add_buf(output,s);

            sprintf(s,"{yNoun damage      : {x%s\n\r",
                    skill_noun_damage(i,ch));
            add_buf(output,s);
            sprintf(s,"{ySpell wear off   : {x%s\n\r",
                    skill_msg_off(i,ch));
            add_buf(output,s);
            sprintf(s,"{yObject wear off  : {x%s\n\r",
                    skill_msg_obj(i,ch));
            add_buf(output,s);
            sprintf(s,"{ySpell room off   : {x%s\n\r",
                    skill_msg_off_room(i,ch));
            add_buf(output,s);

            for (race=0;race<MAX_PC_RACE;race++) {
                if (race!=0)
                    sprintf(s,
                            "                   {x%5s ",
                            pc_race_table[race].who_name);
                else
                    sprintf(s,
                            "{yLevel            : {x%5s ",
                            pc_race_table[race].who_name);
                add_buf(output,s);

                for (class=0;class<MAX_CLASS;class++) {
                    sprintf(s,"%2d ",skill_table[i].Skill_level[class][race]);
                    add_buf(output,s);
                }
                add_buf(output,"\n\r");
            }

            for (race=0;race<MAX_PC_RACE;race++) {
                if (race!=0)
                    sprintf(s,
                            "                   {x%5s ",
                            pc_race_table[race].who_name);
                else
                    sprintf(s,
                            "{yRating           : {x%5s ",
                            pc_race_table[race].who_name);
                add_buf(output,s);
                for (class=0;class<MAX_CLASS;class++) {
                    sprintf(s,"%2d ",skill_table[i].Rating[class][race]);
                    add_buf(output,s);
                }
                add_buf(output,"\n\r");
            }

            for (race=0;race<MAX_PC_RACE;race++) {
                if (race!=0)
                    sprintf(s,
                            "                   {x%5s ",
                            pc_race_table[race].who_name);
                else
                    sprintf(s,
                            "{yskill adept      : {x%5s ",
                            pc_race_table[race].who_name);
                add_buf(output,s);
                for (class=0;class<MAX_CLASS;class++) {
                    sprintf(s,"%2d ",skill_table[i].skill_adept[class][race]);
                    add_buf(output,s);
                }
                add_buf(output,"\n\r");
            }

            for (race=0;race<MAX_PC_RACE;race++) {
                if (race!=0)
                    sprintf(s,
                            "                   {x%5s ",
                            pc_race_table[race].who_name);
                else
                    sprintf(s,
                            "{yMin mana         : {x%5s ",
                            pc_race_table[race].who_name);
                add_buf(output,s);
                for (class=0;class<MAX_CLASS;class++) {
                    sprintf(s,"%2d ",skill_table[i].Min_mana[class][race]);
                    add_buf(output,s);
                }
                add_buf(output,"\n\r");
            }

            page_to_char(buf_string(output),ch);
            free_buf(output);
            return;
        }
    }

}

void stat_group(CHAR_DATA *ch,char *arg) {
    int		i,j,k;
    char	arg1[MAX_STRING_LENGTH];
    BUFFER *	output;
    char	s[MAX_STRING_LENGTH];
    int		race,class;

    if (arg[0]==0) {
        output=new_buf();
        for (i=0;i<MAX_GROUP&&group_exists(i);i++) {
            sprintf(s," {y[{x%-15s{y]",group_name(i,ch));
            add_buf(output,s);
            if (i%4==3) add_buf(output,"{x\n\r");
        }
        if (i%4) add_buf(output,"{x\n\r");
        page_to_char(buf_string(output),ch);
        free_buf(output);
        return;
    }

    arg=one_argument(arg,arg1);
    for (i=0;i<MAX_GROUP;i++) {
        if (!str_prefix(arg1,group_name(i,ch))) {
            output=new_buf();
            sprintf(s,"{yGroup  : {x%s\n\r",
                    group_name(i,ch));
            add_buf(output,s);

            for (race=0;race<MAX_PC_RACE;race++) {
                if (race) {
                    sprintf(s,"         {x%5s ",pc_race_table[race].who_name);
                    add_buf(output,s);
                } else {
                    sprintf(s,"{yRating : {x%5s ",pc_race_table[race].who_name);
                    add_buf(output,s);
                }
                for (class=0;class<MAX_CLASS;class++) {
                    sprintf(s,"%2d ",group_table[i].Rating[class][race]);
                    add_buf(output,s);
                }
                add_buf(output,"\n\r");
            }

            sprintf(s,"{ySkills : {x%s",
                    group_spell(i,ch,0));
            add_buf(output,s);
            k=9+strlen(group_spell(i,ch,0));
            for (j=1;j<MAX_IN_GROUP&&group_spell_exists(i,ch,j);j++) {
                if (k+strlen(group_spell(i,ch,j))>75) {
                    sprintf(s,"{y,{x\n\r         %s",
                            group_spell(i,ch,j));
                    add_buf(output,s);
                    k=9+strlen(group_spell(i,ch,j));
                } else {
                    sprintf(s,"{y,{x %s",group_spell(i,ch,j));
                    add_buf(output,s);
                    k+=strlen(group_spell(i,ch,j))+2;
                }
            }
            add_buf(output,"\n\r");
            page_to_char(buf_string(output),ch);
            free_buf(output);
            return;
        }
    }
}

void stat_class(CHAR_DATA *ch,char *arg) {
    int		i;
    char	s[MAX_STRING_LENGTH],arg1[MAX_STRING_LENGTH];
    BUFFER *	output;

    if (arg[0]==0) {
        output=new_buf();
        for (i=0;i<MAX_CLASS;i++) {
            sprintf(s," {y[{x%-14s{y]",class_table[i].name);
            add_buf(output,s);
            if (i%4==3) add_buf(output,"{x\n\r");
        }
        if (i%4) add_buf(output,"{x\n\r");
        page_to_char(buf_string(output),ch);
        free_buf(output);
        return;
    }

    arg=one_argument(arg,arg1);
    for (i=0;i<MAX_CLASS;i++) {
        if (!str_prefix(arg1,class_table[i].name)) {
            output=new_buf();

            sprintf(s,"{yRace                 : {x%s\n\r",
                    class_table[i].name);
            add_buf(output,s);
            sprintf(s,"{yPrimary attribute    : {x%s\n\r",
                    attribute_flags[class_table[i].attr_prime]);
            add_buf(output,s);

            sprintf(s,"{yTHAC0 (level 0)      : {x%d\n\r",
                    class_table[i].thac0_00);
            add_buf(output,s);
            sprintf(s,"{yTHAC0 (level 32)     : {x%d\n\r",
                    class_table[i].thac0_32);
            add_buf(output,s);
            sprintf(s,"{yHitpoints per level  : between {x%d{y and {x%d\n\r",
                    class_table[i].hp_min,class_table[i].hp_max);
            add_buf(output,s);
            sprintf(s,"{yMana user            : {x%s\n\r",
                    class_table[i].fMana?"yes":"no");
            add_buf(output,s);
            if (class_table[i].base_group)
                sprintf(s,"{yBase group           : {x%s\n\r",
                        class_table[i].base_group);
            else
                sprintf(s,"{yBase group           : {xnone\n\r");
            add_buf(output,s);
            sprintf(s,"{yDefault group        : {x%s\n\r",
                    class_table[i].default_group);
            add_buf(output,s);

            page_to_char(buf_string(output),ch);
            free_buf(output);
            return;
        }
    }
}



void stat_newbie(CHAR_DATA *ch) {
    char	time[MSL];
    int		i;
    struct tm	lt;

    send_to_char("{yNewbie Status\n\r=============\n\r\n\r",ch);

    for (i=0;i<MAX_CHANNELS;i++) {
        lt=*localtime(&mud_data.newbie_channel_since[i]);
        strftime(time,sizeof(time),"%Y-%m-%d %H:%M",&lt);
        sprintf_to_char(ch,
                        "%-12s level {x%2d{y since {x%s{y by {x%s{y\n\r",
                        channel_table[i].name,
                        mud_data.newbie_channel_level[i],
                        time,
                        mud_data.newbie_channel_by[i]);
    }
    send_to_char("\n\r",ch);

    for (i=0;i<MAX_NOTES;i++) {
        strcpy(time,asctime(localtime(&mud_data.newbie_notes_since[i])));
        time[strlen(time)-1]=0;
        sprintf_to_char(ch,"%12-s level {x%2d{y since {x%s{y by {x%s{y\n\r",
                        notes_table[i].name,
                        mud_data.newbie_notes_level[i],
                        time,
                        mud_data.newbie_notes_by[i]);
    }
    send_to_char("\n\r",ch);

    strcpy(time,asctime(localtime(&mud_data.poll_since)));
    time[strlen(time)-1]=0;
    sprintf_to_char(ch,"Poll         level {x%2d{y since {x%s{y by {x%s{y\n\r",
                    mud_data.poll_level,time,mud_data.poll_by);

    send_to_char("\n\r",ch);

    strcpy(time,asctime(localtime(&mud_data.newbie_room_since)));
    time[strlen(time)-1]=0;
    sprintf_to_char(ch,"Room         level {x%2d{y since {x%s{y by {x%s{y\n\r",
                    mud_data.newbie_room_level,time,mud_data.newbie_room_by);

    send_to_char("{x",ch);
}


void stat_mud(CHAR_DATA *ch) {
    char	time[MAX_STRING_LENGTH];
    int		i;

    send_to_char("{yMUD Status\n\r==========\n\r",ch);

    sprintf_to_char(ch,"Boot time       {x%s{y",mud_data.boot_time);
    sprintf_to_char(ch,"Reason was      {x%s{y\n\r",
                    mud_data.copyover?"copyover":"reboot/crash");

    sprintf_to_char(ch,"Reboot-status   {x%s{y\n\r",reboot_state());
    sprintf_to_char(ch,"Reboot-timer	{x%d time%s .5 minutes{y\n\r",
                    mud_data.reboot_timer,mud_data.reboot_timer==1?"":"s");
    sprintf_to_char(ch,"Reboot by	{x%s{y\n\r",mud_data.reboot_by);

    strcpy(time,asctime(localtime(&mud_data.clearcounter_since)));
    time[strlen(time)-1]=0;
    sprintf_to_char(ch,"Counter cleared on {x%s{y by {x%s{y\n\r",
                    time,mud_data.clearcounter_by);

    strcpy(time,asctime(localtime(&mud_data.newlock_since)));
    time[strlen(time)-1]=0;
    sprintf_to_char(ch,
                    "newlock         {x%-5s{y (last modified on {x%s{y by {x%s{y)\n\r",
                    mud_data.newlock?"true":"false",
                    time,
                    mud_data.newlock_by);

    strcpy(time,asctime(localtime(&mud_data.wizlock_since)));
    time[strlen(time)-1]=0;
    sprintf_to_char(ch,
                    "wizlock         {x%-5s{y (last modified on {x%s{y by {x%s{y)\n\r",
                    mud_data.wizlock?"true":"false",
                    time,
                    mud_data.wizlock_by);

    strcpy(time,asctime(localtime(&mud_data.dnslock_since)));
    time[strlen(time)-1]=0;
    sprintf_to_char(ch,
                    "dnslock         {x%-5s{y (last modified on {x%s{y by {x%s{y)\n\r",
                    mud_data.dnslock?"true":"false",
                    time,
                    mud_data.dnslock_by);

    sprintf_to_char(ch,"violations      "
                #ifdef HAS_HTTP
                       "HTTP   : {x%3d{y    "
                #endif
                #ifdef HAS_POP3
                       "POP3   : {x%3d{y    "
                #endif
                       "MUD: {x%3d{y\n\r\
                    player : {x%3d{y    site   : {x%3d{y    new: {x%3d{y\n\r\
                                                                       newlock: {x%3d{y    wizlock: {x%3d{y\n\r",
                                                                                                      #ifdef HAS_HTTP
                                                                                                          mud_data.banhttp_violations,
                                                                                                      #endif
                                                                                                      #ifdef HAS_POP3
                                                                                                          mud_data.banpop3_violations,
                                                                                                      #endif
                                                                                                          mud_data.banmud_violations,  mud_data.banplayer_violations,
                                                                                                          mud_data.bansite_violations, mud_data.bannew_violations,
                                                                                                          mud_data.newlock_violations, mud_data.wizlock_violations);

                                                                                                          sprintf_to_char(ch,"players         {x%d{y new  {x%d{y deleted\n\r",
                                                                                                          mud_data.players_new,
                                                                                                          mud_data.players_deleted);

                                                                                                          sprintf_to_char(ch,"connections     {x%d{y ({x%d{y players  "
                                                                                                      #ifdef HAS_HTTP
                                                                                                          "{x%d {yHTML-who  {x%d{y HTML-help  {x%d{y HTML-failed  "
                                                                                                      #endif
                                                                                                      #ifdef HAS_POP3
                                                                                                          "{x%d{y POP3)"
                                                                                                      #endif
                                                                                                          "{x\n\r"
                                                                                                          ,mud_data.connections
                                                                                                          ,mud_data.players_total
                                                                                                      #ifdef HAS_HTTP
                                                                                                          ,mud_data.http_who_connections
                                                                                                          ,mud_data.http_help_connections
                                                                                                          ,mud_data.http_errors
                                                                                                      #endif
                                                                                                      #ifdef HAS_POP3
                                                                                                          ,mud_data.pop3_connections
                                                                                                      #endif
                                                                                                          );

                                                                                                          for (i=0;i<MAX_TIMEZONES;i++) {
                                                                                                              sprintf_to_char(ch,"{yTimezone %d{x      {x%-15s ",
                                                                                                              i,
                                                                                                              mud_data.time_zones[i]);
                                                                                                              if (mud_data.time_zonenames[i] && mud_data.time_zonenames[i][0] )
                                                                                                              sprintf_to_char(ch,"{yzonefile{x %s\n\r",mud_data.time_zonenames[i]);
                                                                                                              else
                                                                                                              sprintf_to_char(ch,"{yoffset{x %3-d\n\r",mud_data.time_offsets[i]);
                                                                                                          }
                                                                                                     }


                                                                                                     void stat_notes(CHAR_DATA *ch) {
                                                                                                         NOTES_DATA *	board;
                                                                                                         int			i;

                                                                                                         sprintf_to_char(ch,"%-15s %-15s\n\r","Name","File");
                                                                                                         for (i=0;i<MAX_NOTES;i++) {
                                                                                                             board=&notes_table[i];

                                                                                                             sprintf_to_char(ch,"%-15s %-15s\n\r",
                                                                                                             board->name,
                                                                                                             "no information");
                                                                                                         }
                                                                                                     }


                                                                                                     void stat_badnames(CHAR_DATA *ch) {
                                                                                                         BADNAME_DATA *	badname=badname_list;
                                                                                                         int len=0;

                                                                                                         send_to_char("The following names are banned:\n\r",ch);
                                                                                                         while (badname!=NULL) {
                                                                                                             sprintf_to_char(ch,"%s ",badname->name);
                                                                                                             len+=strlen(badname->name)+1;
                                                                                                             if (len>70) {
                                                                                                                 len=0;
                                                                                                                 send_to_char("\n\r",ch);
                                                                                                             }
                                                                                                             badname=badname->next;
                                                                                                         }
                                                                                                         if (len!=0)
                                                                                                         send_to_char("\n\r",ch);
                                                                                                     }

                                                                                                     void stat_rlinks(CHAR_DATA *ch, char *argument) {
                                                                                                         ROOM_DATA *room,*this_room;
                                                                                                         OBJ_INDEX_DATA *obj;
                                                                                                         int dir,i;

                                                                                                         this_room=ch->in_room;
                                                                                                         if (argument && argument[0]!=0 &&
                                                                                                         is_number(argument)) {
                                                                                                             int vnum;
                                                                                                             vnum=atoi(argument);
                                                                                                             if (!(this_room=get_room_index(vnum))) {
                                                                                                                 sprintf_to_char(ch,"Room %d doesn't exist\n\r");
                                                                                                                 return;
                                                                                                             }
                                                                                                         }
                                                                                                         sprintf_to_char(ch,"Links out of the room:\n\r");
                                                                                                         for (dir=0;dir<MAX_DIR;dir++)
                                                                                                         if (this_room->exit[dir])
                                                                                                         sprintf_to_char(ch,"  %5s to [%d] %s\n\r",
                                                                                                         dir_name[dir],this_room->exit[dir]->vnum,
                                                                                                         this_room->exit[dir]->to_room?this_room->exit[dir]->to_room->name:"BROKEN!");

                                                                                                         sprintf_to_char(ch,"Links into the room:\n\r");
                                                                                                         for (i=0;i<MAX_KEY_HASH;i++)
                                                                                                         for (room=room_index_hash[i];room;room=room->next)
                                                                                                         for (dir=0;dir<MAX_DIR;dir++)
                                                                                                         if (room->exit[dir] && (room->exit[dir]->vnum==this_room->vnum))
                                                                                                         sprintf_to_char(ch,"  %5s from [%d] %s\n\r",
                                                                                                         dir_name[dir],room->vnum,room->name);

                                                                                                         sprintf_to_char(ch,"Portals pointing to the room:\n\r");
                                                                                                         for (i=0;i<MAX_KEY_HASH;i++)
                                                                                                         for (obj=obj_index_hash[i];obj;obj=obj->next)
                                                                                                         if ((obj->item_type==ITEM_PORTAL) &&
                                                                                                         (obj->value[3]==this_room->vnum))
                                                                                                         sprintf_to_char(ch,"  [%d] %s\n\r",obj->vnum,obj->short_descr);

                                                                                                     }

                                                                                                     void stat_alinks(CHAR_DATA *ch) {
                                                                                                         AREA_DATA *this_area;
                                                                                                         ROOM_DATA *room;
                                                                                                         OBJ_INDEX_DATA *obj;
                                                                                                         int dir,i;

                                                                                                         this_area=ch->in_room->area;

                                                                                                         sprintf_to_char(ch,"Links into this area:\n\r");
                                                                                                         for (i=0;i<MAX_KEY_HASH;i++)
                                                                                                         for (room=room_index_hash[i];room;room=room->next)
                                                                                                         if (room->vnum<this_area->lvnum ||
                                                                                                         room->vnum>this_area->uvnum)
                                                                                                         for (dir=0;dir<MAX_DIR;dir++)
                                                                                                         if (room->exit[dir] && (room->exit[dir]->vnum>=this_area->lvnum)
                                                                                                         && (room->exit[dir]->vnum<=this_area->uvnum))
                                                                                                         sprintf_to_char(ch,"  from [%d] %s %s to [%d] %s\n\r",
                                                                                                         room->vnum,room->name,dir_name[dir],
                                                                                                         room->exit[dir]->vnum,room->exit[dir]->to_room->name);
                                                                                                         sprintf_to_char(ch,"Links out of this area:\n\r");
                                                                                                         for (i=0;i<MAX_KEY_HASH;i++)
                                                                                                         for (room=room_index_hash[i];room;room=room->next)
                                                                                                         if (room->vnum>=this_area->lvnum &&
                                                                                                         room->vnum<=this_area->uvnum)
                                                                                                         for (dir=0;dir<MAX_DIR;dir++)
                                                                                                         if (room->exit[dir] && ((room->exit[dir]->vnum<this_area->lvnum) ||
                                                                                                         (room->exit[dir]->vnum>this_area->uvnum))
                                                                                                         && ((room->exit[dir]->vnum!=0) ||
                                                                                                         (room->exit[dir]->description[0]==0)))
                                                                                                         sprintf_to_char(ch,"  from [%d] %s %s to [%d] %s\n\r",
                                                                                                         room->vnum,room->name,
                                                                                                         dir_name[dir],
                                                                                                         room->exit[dir]->vnum,
                                                                                                         room->exit[dir]->to_room?room->exit[dir]->to_room->name:"BROKEN");
                                                                                                         sprintf_to_char(ch,"Portals pointing into this area:\n\r");
                                                                                                         for (i=0;i<MAX_KEY_HASH;i++)
                                                                                                         for (obj=obj_index_hash[i];obj;obj=obj->next)
                                                                                                         if ((obj->item_type==ITEM_PORTAL) &&
                                                                                                         (obj->value[3]>=this_area->lvnum) &&
                                                                                                         (obj->value[3]<=this_area->uvnum))
                                                                                                         sprintf_to_char(ch,"  [%d] %s\n\r",obj->vnum,obj->short_descr);
                                                                                                     }

                                                                                                     extern SOCIAL_TYPE *social_hash[];

                                                                                                     void stat_social(CHAR_DATA *ch, char *argument) {
                                                                                                         SOCIAL_TYPE *social=NULL;

                                                                                                         if (!argument[0]) {
                                                                                                             send_to_char("USAGE: stat social <name>\n\r",ch);
                                                                                                             return;
                                                                                                         }

                                                                                                         for (social=social_hash[(unsigned char)argument[0]];
                                                                                                         social;social=social->next) {
                                                                                                             if ( LOWER(argument[0]) == social->name[0] &&
                                                                                                             !str_prefix( argument, social->name ) )
                                                                                                             break;
                                                                                                         }

                                                                                                         if (!social) {
                                                                                                             send_to_char("No such social\n\r",ch);
                                                                                                             return;
                                                                                                         }

                                                                                                         sprintf_to_char( ch, "{y    Name           : {x%s {r%s{x\n\r"
                                                                                                         "{ycna Char no arg    : {x%s\n\r"
                                                                                                         "{yona Others no arg  : {x%s\n\r"
                                                                                                         "{ycf  Char found     : {x%s\n\r"
                                                                                                         "{yof  Others found   : {x%s\n\r"
                                                                                                         "{yvf  Victim found   : {x%s\n\r"
                                                                                                         "{ycnf Char not found : {x%s\n\r"
                                                                                                         "{yca  Char auto      : {x%s\n\r"
                                                                                                         "{yoa  Others auto    : {x%s\n\r",
                                                                                                         social->name,
                                                                                                         social->deleted?"*deleted*":"",
                                                                                                         social->char_no_arg,
                                                                                                         social->others_no_arg,
                                                                                                         social->char_found,
                                                                                                         social->others_found,
                                                                                                         social->vict_found,
                                                                                                         social->char_not_found,
                                                                                                         social->char_auto,
                                                                                                         social->others_auto);
                                                                                                     }

                                                                                                     void stat_help(CHAR_DATA *ch, char *argument) {
                                                                                                         HELP_DATA *pHelp;
                                                                                                         char arg[MSL];
                                                                                                         int count;

                                                                                                         count=number_argument(argument,arg);

                                                                                                         for (pHelp=help_first; pHelp!=NULL; pHelp=pHelp->next ) {

                                                                                                             if (pHelp->keyword[0]=='*'?is_exact_name(arg,pHelp->keyword+1):
                                                                                                             is_name(arg,pHelp->keyword) ) {

                                                                                                                 if (--count!=0) continue;

                                                                                                                 sprintf_to_char(ch, "{yVnum:    {x [%8p]\n\r"
                                                                                                                 "{yArea:    {x [%5d] %s\n\r"
                                                                                                                 "{yLevel:   {x [%d] %s\n\r"
                                                                                                                 "{yKeywords:{x %s\n\r"
                                                                                                                 "{yText:    {x\n\r",
                                                                                                                 pHelp,
                                                                                                                 !pHelp->area ? -1 : pHelp->area->vnum,
                                                                                                                 !pHelp->area ? "No Area" : pHelp->area->name,
                                                                                                                 pHelp->level, pHelp->deleted?"{Rdeleted{x":"",
                                                                                                                 pHelp->keyword );

                                                                                                                 if(pHelp->text[0]!='\0')
                                                                                                                 send_to_char( pHelp->text, ch );
                                                                                                                 else
                                                                                                                 send_to_char( "Empty help.\n\r", ch );

                                                                                                                 return;
                                                                                                             }
                                                                                                         }
                                                                                                     }

                                                                                                     static void stat_prog(CHAR_DATA *ch, TCLPROG_LIST *prog_list, const TABLE_TYPE *table) {
                                                                                                         int max_name=1;
                                                                                                         int max_trig=1;
                                                                                                         int max_phrase=1;
                                                                                                         char format[MSL];
                                                                                                         TCLPROG_LIST *prog;

                                                                                                         prog=prog_list;
                                                                                                         while (prog) {
                                                                                                             int i;

                                                                                                             i=strlen(prog->name);         if (i>max_name) max_name=i;
                                                                                                             i=strlen(table_find_value(prog->trig_type,table)); if (i>max_trig) max_trig=i;
                                                                                                             i=prog->trig_phrase?strlen(prog->trig_phrase):6;  if (i>max_phrase) max_phrase=i;
                                                                                                             prog=prog->next;
                                                                                                         }
                                                                                                         snprintf(format,MSL,"%%-%ds %%-%ds  %%-%ds -> %%s\n\r",max_name,max_trig,max_phrase);
                                                                                                         prog=prog_list;
                                                                                                         while (prog) {
                                                                                                             sprintf_to_char(ch,format,
                                                                                                             prog->name,table_find_value(prog->trig_type,table),
                                                                                                             prog->trig_phrase?prog->trig_phrase:"\"\"",prog->code);
                                                                                                             prog=prog->next;
                                                                                                         }
                                                                                                     }

                                                                                                     void stat_mprog(CHAR_DATA *ch, char *argument) {
                                                                                                         CHAR_DATA *victim;
                                                                                                         char arg[MSL];

                                                                                                         one_argument( argument, arg );

                                                                                                         if ( arg[0]==0 ) {
                                                                                                             send_to_char( "Stat whom?\n\r", ch );
                                                                                                             return;
                                                                                                         }

                                                                                                         if ( ( victim = get_char_world( ch, argument ) ) == NULL ) {
                                                                                                             send_to_char( "They aren't here.\n\r", ch );
                                                                                                             return;
                                                                                                         }

                                                                                                         stat_prog(ch,victim->mprogs,mobtrigger_table);
                                                                                                     }

                                                                                                     void stat_oprog(CHAR_DATA *ch, char *argument) {
                                                                                                         OBJ_DATA *obj;
                                                                                                         char arg[MSL];

                                                                                                         one_argument( argument, arg );

                                                                                                         if ( arg[0]==0 ) {
                                                                                                             send_to_char( "Stat what?\n\r", ch );
                                                                                                             return;
                                                                                                         }

                                                                                                         if ( ( obj = get_obj_world( ch, argument ) ) == NULL ) {
                                                                                                             send_to_char( "Nothing like that in hell, earth, or heaven.\n\r", ch );
                                                                                                             return;
                                                                                                         }

                                                                                                         stat_prog(ch,obj->oprogs,objtrigger_table);
                                                                                                     }

                                                                                                     void stat_rprog(CHAR_DATA *ch, char *argument) {
                                                                                                         ROOM_INDEX_DATA *	location;
                                                                                                         char arg[MSL];

                                                                                                         one_argument(argument,arg);
                                                                                                         location=(arg[0]==0) ? ch->in_room : find_location(ch,arg);
                                                                                                         if (location==NULL) {
                                                                                                             send_to_char("No such location.\n\r",ch);
                                                                                                             return;
                                                                                                         }

                                                                                                         stat_prog(ch,location->rprogs,roomtrigger_table);
                                                                                                     }

                                                                                                     void stat_aprog(CHAR_DATA *ch, char *argument) {
                                                                                                         AREA_DATA *	location;
                                                                                                         int vnum;
                                                                                                         char arg[MSL];

                                                                                                         one_argument(argument,arg);
                                                                                                         vnum=arg[0]!=0?atoi(arg):0;
                                                                                                         location=(vnum==0) ? ch->in_room->area : get_area_data(vnum);
                                                                                                         if (location==NULL) {
                                                                                                             send_to_char("No such location.\n\r",ch);
                                                                                                             return;
                                                                                                         }

                                                                                                         stat_prog(ch,location->aprogs,areatrigger_table);
                                                                                                     }
