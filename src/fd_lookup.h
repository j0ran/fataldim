/* $Id: fd_lookup.h,v 1.6 2006/08/25 09:43:20 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,	   *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *									   *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael	   *
 *  Chastain, Michael Quan, and Mitchell Tse.				   *
 *									   *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc	   *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.						   *
 *									   *
 *  Much time and thought has gone into this software and you are	   *
 *  benefitting.  We hope that you share your changes too.  What goes	   *
 *  around, comes around.						   *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

//
// xxx_find	: returns the integer value of the record "name"
// xxx_string	: returns a string with all the names which are set by bits
//
// e.g.
// TABLE_TYPE this_table[]=(
//	( "test",	TEST	),
//	( "party",	PARTY	),
//	( "woops",	WOOPS	),
//	( NULL,		0	)
// )
//
// xxx_find_value("test",this_xxx)		returns TEST
// xxx_find_name(PARTY,this_xxx)		returns "party"
// xxx_string(TEST|PARTY,this_xxx)		returns "test party"
// xxx_string_from_long(TEST|PARTY,this_xxx)	returns "test party"
// xxx_all(this_xxx)				returns "test" party woops"
//

int	table_find_name		(const char *name,const TABLE_TYPE *table);
char *	table_find_value	(const int value,const TABLE_TYPE *table);
char *	table_string		(const char *bits,const TABLE_TYPE *table);
char *	table_string_from_long	(long bits,const TABLE_TYPE *table);
char *	table_all		(const TABLE_TYPE *table);

int	option_find_name	(const char *name,const OPTION_TYPE *table, bool settable);
char *	option_find_value	(const int value,const OPTION_TYPE *table);
char *	option_string		(const char *bits,const OPTION_TYPE *table);
char *	option_string_from_long	(long bits,const OPTION_TYPE *table);
char *	option_all		(const OPTION_TYPE *table);

//
// specific lookups, but all refering to table_xxx() or option_xxx()
//

char *	reboot_state		(void);
char *	eff_bit_name		(EFFECT_DATA *effect);
char *	effect_loc_name		(EFFECT_DATA *effect);
char *	effect_bit_name		(EFFECT_DATA *effect);
char *	effect2_bit_name	(EFFECT_DATA *effect);
char *	item_type		(OBJ_DATA *item);
char *	obj_extra_string	(OBJ_DATA *obj);
char *	obj_wear_string		(OBJ_DATA *obj);
char *	obj_weaponflag_string	(OBJ_DATA *obj);
char *	obj_container_string	(OBJ_DATA *obj);
char *	char_act_string		(CHAR_DATA *ch);
char *	char_plr_string		(CHAR_DATA *ch);
char *	char_comm_string	(CHAR_DATA *ch);
char *	char_off_string		(CHAR_DATA *ch);
char *	char_imm_string		(CHAR_DATA *ch);
char *	char_res_string		(CHAR_DATA *ch);
char *	char_vuln_string	(CHAR_DATA *ch);
char *	char_form_string	(CHAR_DATA *ch);
char *	char_part_string	(CHAR_DATA *ch);
char *	char_affected_string	(CHAR_DATA *ch);
char *	char_affected2_string	(CHAR_DATA *ch);
char *	area_flags		(AREA_DATA *area);
char *	room_sector_type	(ROOM_INDEX_DATA *room);
char *	room_flags		(ROOM_INDEX_DATA *room);
char *	exit_flags		(EXIT_DATA *exit);
char *	exit_reset_string	(EXIT_DATA *exit);

void	olc_help_table		(CHAR_DATA *ch,char *msg,const TABLE_TYPE *table);
void	olc_help_options	(CHAR_DATA *ch,char *msg,const OPTION_TYPE *options);
///////////////////////////////////////////////////////////////////////////////

int	position_lookup		(const char *name);
int	short_position_lookup	(const char *name);
int 	sex_lookup		(const char *name);
int 	size_lookup		(const char *name);
int	material_lookup		(const char *name);
int	race_lookup		(const char *name);
int	liq_lookup		(const char *name);
int	weapon_lookup		(const char *name);
int	weapon_type		(const char *name);
char *	weapon_name		(int weapon_type);
int	attack_lookup		(const char *name);
long	wiznet_lookup		(const char *name);
char *	wiznet_name_lookup	(int flag);
int	class_lookup		(const char *name);

