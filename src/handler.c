/* $Id: handler.c,v 1.141 2008/05/11 20:50:47 jodocus Exp $ */
/**************************************************************************r
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

#include "merc.h"
#include "interp.h"
#include "recycle.h"

/*
 * Local functions.
 */
void	effect_modify	( CHAR_DATA *ch, EFFECT_DATA *pef, bool fAdd );


/* friend stuff -- for NPC's mostly */
bool is_friend(CHAR_DATA *ch,CHAR_DATA *victim)
{
    if (is_same_group(ch,victim))
        return TRUE;


    if (!IS_NPC(ch))
        return FALSE;

    if (!IS_NPC(victim))
    {
        if (STR_IS_SET(ch->strbit_off_flags,ASSIST_PLAYERS))
            return TRUE;
        else
            return FALSE;
    }

    if (IS_AFFECTED(ch,EFF_CHARM))
        return FALSE;

    if (STR_IS_SET(ch->strbit_off_flags,ASSIST_ALL))
        return TRUE;

    if (ch->group && ch->group == victim->group)
        return TRUE;

    if (STR_IS_SET(ch->strbit_off_flags,ASSIST_VNUM)
            &&  ch->pIndexData == victim->pIndexData)
        return TRUE;

    if (STR_IS_SET(ch->strbit_off_flags,ASSIST_RACE) && ch->race == victim->race)
        return TRUE;

    if (STR_IS_SET(ch->strbit_off_flags,ASSIST_ALIGN)
            &&  !STR_IS_SET(ch->strbit_act,ACT_NOALIGN)
            &&  !STR_IS_SET(victim->strbit_act,ACT_NOALIGN)
            &&  ((IS_GOOD(ch) && IS_GOOD(victim))
                 ||	 (IS_EVIL(ch) && IS_EVIL(victim))
                 ||   (IS_NEUTRAL(ch) && IS_NEUTRAL(victim))))
        return TRUE;

    return FALSE;
}

/* returns number of people on an object */
int count_users(OBJ_DATA *obj)
{
    CHAR_DATA *fch;
    int count = 0;

    if (obj->in_room == NULL)
        return 0;

    for (fch = obj->in_room->people; fch != NULL; fch = fch->next_in_room)
        if (fch->on == obj)
            count++;

    return count;
}

// command name to number and vice-versa
int exact_command_to_vnum(const char arg[]) {
    int cmd;

    for (cmd=0;cmd_table[cmd].name!=NULL;cmd++)
        if (!strcmp(arg,cmd_table[cmd].name))
            return cmd_table[cmd].vnum;

    return -1;
}
int command_to_vnum(const char arg[]) {
    int cmd;

    for (cmd=0;cmd_table[cmd].name!=NULL;cmd++)
        if (!str_prefix(arg,cmd_table[cmd].name))
            return cmd_table[cmd].vnum;

    return -1;
}
const char *vnum_to_command(const int vnum) {
    int cmd;
    char *best = NULL;

    for (cmd=0;cmd_table[cmd].name!=NULL;cmd++)
        if (cmd_table[cmd].vnum==vnum) {
            if (cmd_table[cmd].show)
                return cmd_table[cmd].name;
            else
                if (!best) best=cmd_table[cmd].name;
        }

    return best;
}


/* for immunity, vulnerabiltiy, and resistant
   the 'globals' (magic and weapons) may be overriden
   three other cases -- wood, silver, and iron -- are checked in fight.c */

int check_immune(CHAR_DATA *ch, int dam_type)
{
    int immune, def;
    int bit;

    immune = -1;
    def = IS_NORMAL;

    if (dam_type == DAM_NONE)
        return immune;

    if (dam_type <= 3)
    {
        if (STR_IS_SET(ch->strbit_imm_flags,IMM_WEAPON))
            def = IS_IMMUNE;
        else if (STR_IS_SET(ch->strbit_res_flags,RES_WEAPON))
            def = IS_RESISTANT;
        else if (STR_IS_SET(ch->strbit_vuln_flags,VULN_WEAPON))
            def = IS_VULNERABLE;
    }
    else /* magical attack */
    {
        if (STR_IS_SET(ch->strbit_imm_flags,IMM_MAGIC))
            def = IS_IMMUNE;
        else if (STR_IS_SET(ch->strbit_res_flags,RES_MAGIC))
            def = IS_RESISTANT;
        else if (STR_IS_SET(ch->strbit_vuln_flags,VULN_MAGIC))
            def = IS_VULNERABLE;
    }

    /* set bits to check -- VULN etc. must ALL be the same or this will fail */
    switch (dam_type)
    {
    case(DAM_BASH):		bit = IMM_BASH;		break;
    case(DAM_PIERCE):	bit = IMM_PIERCE;	break;
    case(DAM_SLASH):	bit = IMM_SLASH;	break;
    case(DAM_FIRE):		bit = IMM_FIRE;		break;
    case(DAM_COLD):		bit = IMM_COLD;		break;
    case(DAM_LIGHTNING):	bit = IMM_LIGHTNING;	break;
    case(DAM_ACID):		bit = IMM_ACID;		break;
    case(DAM_POISON):	bit = IMM_POISON;	break;
    case(DAM_NEGATIVE):	bit = IMM_NEGATIVE;	break;
    case(DAM_HOLY):		bit = IMM_HOLY;		break;
    case(DAM_ENERGY):	bit = IMM_ENERGY;	break;
    case(DAM_MENTAL):	bit = IMM_MENTAL;	break;
    case(DAM_DISEASE):	bit = IMM_DISEASE;	break;
    case(DAM_DROWNING):	bit = IMM_DROWNING;	break;
    case(DAM_LIGHT):	bit = IMM_LIGHT;	break;
    case(DAM_CHARM):	bit = IMM_CHARM;	break;
    case(DAM_SOUND):	bit = IMM_SOUND;	break;
    default:		return def;
    }

    if (STR_IS_SET(ch->strbit_imm_flags,bit))
        immune = IS_IMMUNE;
    else if (STR_IS_SET(ch->strbit_res_flags,bit) && immune != IS_IMMUNE)
        immune = IS_RESISTANT;
    else if (STR_IS_SET(ch->strbit_vuln_flags,bit))
    {
        if (immune == IS_IMMUNE)
            immune = IS_RESISTANT;
        else if (immune == IS_RESISTANT)
            immune = IS_NORMAL;
        else
            immune = IS_VULNERABLE;
    }

    if (immune == -1)
        return def;
    else
        return immune;
}

bool is_clan(CHAR_DATA *ch)
{
    return ch->clan!=NULL;
}

bool is_same_clan(CHAR_DATA *ch, CHAR_DATA *victim)
{
    if ((ch->clan==NULL) || (victim->clan==NULL))
        return FALSE;
    if (ch->clan->clan_info->independent)
        return FALSE;
    return (bool)(ch->clan->clan_info == victim->clan->clan_info);
}

/* checks mob format */
bool is_old_mob(CHAR_DATA *ch)
{
    return FALSE;
}

/* for returning weapon information */
int get_weapon_sn(CHAR_DATA *ch,bool secondary)
{
    OBJ_DATA *wield;
    int sn;

    if(!secondary)
        wield = get_eq_char( ch, WEAR_WIELD );
    else
        wield = get_eq_char( ch, WEAR_SECONDARY);
    if (wield == NULL || wield->item_type != ITEM_WEAPON)
        sn = gsn_hand_to_hand;
    else switch (wield->value[0])
    {
    default :               sn = -1;                break;
    case(WEAPON_SWORD):     sn = gsn_sword;         break;
    case(WEAPON_DAGGER):    sn = gsn_dagger;        break;
    case(WEAPON_SPEAR):     sn = gsn_spear;         break;
    case(WEAPON_MACE):      sn = gsn_mace;          break;
    case(WEAPON_AXE):       sn = gsn_axe;           break;
    case(WEAPON_FLAIL):     sn = gsn_flail;         break;
    case(WEAPON_WHIP):      sn = gsn_whip;          break;
    case(WEAPON_POLEARM):   sn = gsn_polearm;       break;
    }
    return sn;
}

int get_weapon_skill(CHAR_DATA *ch, int sn)
{
    int skill;

    /* -1 is exotic */
    if (IS_NPC(ch))
    {
        if (sn == -1)
            skill = 3 * ch->level;
        else if (sn == gsn_hand_to_hand)
            skill = 40 + 2 * ch->level;
        else
            skill = 40 + 5 * ch->level / 2;
    }

    else
    {
        if (sn == -1)
            skill = 3 * ch->level;
        else
            skill = get_skill(ch,sn);
    }

    return URANGE(0,skill,100);
}


/* used to de-screw characters */
void reset_char(CHAR_DATA *ch)
{
    int loc,mod,stat;
    OBJ_DATA *obj;
    EFFECT_DATA *ef;
    int i;

    if (IS_NPC(ch))
        return;

    if (ch->pcdata->perm_hit <= 0
            ||	ch->pcdata->perm_mana <= 0
            ||  ch->pcdata->perm_move <= 0
            ||	ch->pcdata->last_level <= 0)
    {
        /* do a FULL reset */
        for (loc = 0; loc < MAX_WEAR; loc++)
        {
            obj = get_eq_char(ch,loc);
            if (obj == NULL)
                continue;
            if (!obj->enchanted)
                for ( ef = obj->pIndexData->affected; ef != NULL; ef = ef->next )
                {
                    mod = ef->modifier;
                    switch(ef->location)
                    {
                    case APPLY_SEX:	ch->Sex		-= mod;		break;
                    case APPLY_MANA:	ch->max_mana	-= mod;		break;
                    case APPLY_HIT:	ch->max_hit	-= mod;		break;
                    case APPLY_MOVE:	ch->max_move	-= mod;		break;
                    }
                }

            for ( ef = obj->affected; ef != NULL; ef = ef->next )
            {
                mod = ef->modifier;
                switch(ef->location)
                {
                case APPLY_SEX:     ch->Sex         -= mod;         break;
                case APPLY_MANA:    ch->max_mana    -= mod;         break;
                case APPLY_HIT:     ch->max_hit     -= mod;         break;
                case APPLY_MOVE:    ch->max_move    -= mod;         break;
                }
            }
        }
        /* now reset the permanent stats */
        ch->pcdata->perm_hit 	= ch->max_hit;
        ch->pcdata->perm_mana 	= ch->max_mana;
        ch->pcdata->perm_move	= ch->max_move;
        ch->pcdata->last_level	= ch->played/3600;
    }

    /* now restore the character to his/her true condition */
    for (stat = 0; stat < MAX_STATS; stat++)
        ch->mod_stat[stat] = 0;

    if ((ch->pcdata->true_sex < 0 || ch->pcdata->true_sex > 2))
        ch->pcdata->true_sex=0;
    ch->Sex		= ch->pcdata->true_sex;	// we read it from the file!
    ch->max_hit 	= ch->pcdata->perm_hit;
    ch->max_mana	= ch->pcdata->perm_mana;
    ch->max_move	= ch->pcdata->perm_move;

    for (i = 0; i < 4; i++)
        ch->armor[i]	= 100;

    ch->hitroll		= 0;
    ch->damroll		= 0;
    ch->saving_throw	= 0;

    /* now start adding back the effects */
    for (loc = 0; loc < MAX_WEAR; loc++)
    {
        obj = get_eq_char(ch,loc);
        if (obj == NULL)
            continue;
        for (i = 0; i < 4; i++)
            ch->armor[i] -= apply_ac( obj, loc, i );

        if (!obj->enchanted)
            for ( ef = obj->pIndexData->affected; ef != NULL; ef = ef->next )
            {
                mod = ef->modifier;
                switch(ef->location)
                {
                case APPLY_STR:		ch->mod_stat[STAT_STR]	+= mod;	break;
                case APPLY_DEX:		ch->mod_stat[STAT_DEX]	+= mod; break;
                case APPLY_INT:		ch->mod_stat[STAT_INT]	+= mod; break;
                case APPLY_WIS:		ch->mod_stat[STAT_WIS]	+= mod; break;
                case APPLY_CON:		ch->mod_stat[STAT_CON]	+= mod; break;

                case APPLY_SEX:		ch->Sex			+= mod; break;
                case APPLY_MANA:	ch->max_mana		+= mod; break;
                case APPLY_HIT:		ch->max_hit		+= mod; break;
                case APPLY_MOVE:	ch->max_move		+= mod; break;

                case APPLY_AC:
                    for (i = 0; i < 4; i ++)
                        ch->armor[i] += mod;
                    break;
                case APPLY_HITROLL:	ch->hitroll		+= mod; break;
                case APPLY_DAMROLL:	ch->damroll		+= mod; break;

                case APPLY_SAVES:		ch->saving_throw += mod; break;
                case APPLY_SAVING_ROD: 		ch->saving_throw += mod; break;
                case APPLY_SAVING_PETRI:	ch->saving_throw += mod; break;
                case APPLY_SAVING_BREATH: 	ch->saving_throw += mod; break;
                case APPLY_SAVING_SPELL:	ch->saving_throw += mod; break;
                case APPLY_ALIGNMENT:  		ch->alignment	 += mod; break;
                }
            }

        for ( ef = obj->affected; ef != NULL; ef = ef->next )
        {
            mod = ef->modifier;
            switch(ef->location)
            {
            case APPLY_STR:         ch->mod_stat[STAT_STR]  += mod; break;
            case APPLY_DEX:         ch->mod_stat[STAT_DEX]  += mod; break;
            case APPLY_INT:         ch->mod_stat[STAT_INT]  += mod; break;
            case APPLY_WIS:         ch->mod_stat[STAT_WIS]  += mod; break;
            case APPLY_CON:         ch->mod_stat[STAT_CON]  += mod; break;

            case APPLY_SEX:         ch->Sex                 += mod; break;
            case APPLY_MANA:        ch->max_mana            += mod; break;
            case APPLY_HIT:         ch->max_hit             += mod; break;
            case APPLY_MOVE:        ch->max_move            += mod; break;

            case APPLY_AC:
                for (i = 0; i < 4; i ++)
                    ch->armor[i] += mod;
                break;
            case APPLY_HITROLL:     ch->hitroll             += mod; break;
            case APPLY_DAMROLL:     ch->damroll             += mod; break;

            case APPLY_SAVES:         ch->saving_throw += mod; break;
            case APPLY_SAVING_ROD:          ch->saving_throw += mod; break;
            case APPLY_SAVING_PETRI:        ch->saving_throw += mod; break;
            case APPLY_SAVING_BREATH:       ch->saving_throw += mod; break;
            case APPLY_SAVING_SPELL:        ch->saving_throw += mod; break;
            case APPLY_ALIGNMENT:  		ch->alignment	 += mod; break;
            }
        }
    }

    /* now add back spell effects */
    for (ef = ch->affected; ef != NULL; ef = ef->next)
    {
        mod = ef->modifier;
        switch(ef->location)
        {
        case APPLY_STR:         ch->mod_stat[STAT_STR]  += mod; break;
        case APPLY_DEX:         ch->mod_stat[STAT_DEX]  += mod; break;
        case APPLY_INT:         ch->mod_stat[STAT_INT]  += mod; break;
        case APPLY_WIS:         ch->mod_stat[STAT_WIS]  += mod; break;
        case APPLY_CON:         ch->mod_stat[STAT_CON]  += mod; break;

        case APPLY_SEX:         ch->Sex                 += mod; break;
        case APPLY_MANA:        ch->max_mana            += mod; break;
        case APPLY_HIT:         ch->max_hit             += mod; break;
        case APPLY_MOVE:        ch->max_move            += mod; break;

        case APPLY_AC:
            for (i = 0; i < 4; i ++)
                ch->armor[i] += mod;
            break;
        case APPLY_HITROLL:     ch->hitroll             += mod; break;
        case APPLY_DAMROLL:     ch->damroll             += mod; break;

        case APPLY_SAVES:         ch->saving_throw += mod; break;
        case APPLY_SAVING_ROD:          ch->saving_throw += mod; break;
        case APPLY_SAVING_PETRI:        ch->saving_throw += mod; break;
        case APPLY_SAVING_BREATH:       ch->saving_throw += mod; break;
        case APPLY_SAVING_SPELL:        ch->saving_throw += mod; break;
        case APPLY_ALIGNMENT:  		ch->alignment	 += mod; break;
        }
    }
    char_obj_alignment_check(ch);
}


/*
 * Retrieve a character's trusted level for permission checking.
 */
int get_trust( CHAR_DATA *ch )
{
    if ( ch->desc != NULL && ch->desc->original != NULL )
        ch = ch->desc->original;

    if (ch->trust)
        return ch->trust;

    if ( IS_NPC(ch) && ch->level >= LEVEL_HERO )
        return LEVEL_HERO - 1;
    else
        return ch->level;
}


/*
 * Retrieve a character's age.
 */
int get_age( CHAR_DATA *ch )
{
    return 17 + ( ch->played + (int) (current_time - ch->logon) ) / 72000;
}

/* command for retrieving stats */
int get_curr_stat( CHAR_DATA *ch, int stat )
{
    int max;

    if (IS_NPC(ch) || ch->level > LEVEL_IMMORTAL)
        max = 25;

    else
    {
        max = pc_race_table[ch->race].max_stats[stat] + 4;

        if (class_table[ch->class].attr_prime == stat)
            max += 2;

        if ( ch->race == race_lookup("human"))
            max += 1;

        max = UMIN(max,25);
    }

    return URANGE(3,ch->perm_stat[stat] + ch->mod_stat[stat], max);
}

/* command for returning max training score */
int get_max_train( CHAR_DATA *ch, int stat )
{
    int max;

    if (IS_NPC(ch) || ch->level > LEVEL_IMMORTAL)
        return 25;

    max = pc_race_table[ch->race].max_stats[stat];
    if (class_table[ch->class].attr_prime == stat) {
        if (ch->race == race_lookup("human"))
            max += 3;
        else
            max += 2;
    }

    return UMIN(max,25);
}


/*
 * Retrieve a character's carry capacity.
 */
int can_carry_n( CHAR_DATA *ch )
{
    if ( !IS_NPC(ch) && ch->level >= LEVEL_IMMORTAL )
        return 1000;

    if ( IS_NPC(ch) && STR_IS_SET(ch->strbit_act, ACT_PET) )
        return 0;

    return MAX_WEAR +  2 * get_curr_stat(ch,STAT_DEX) + ch->level;
}



/*
 * Retrieve a character's carry capacity.
 */
int can_carry_w( CHAR_DATA *ch )
{
    if ( !IS_NPC(ch) && ch->level >= LEVEL_IMMORTAL )
        return 10000000;

    if ( IS_NPC(ch) && STR_IS_SET(ch->strbit_act, ACT_PET) )
        return 0;

    return str_app[get_curr_stat(ch,STAT_STR)].carry * 10 + ch->level * 25;
}

void char_obj_alignment_check( CHAR_DATA *ch ) {
    OBJ_DATA *obj, *obj_next;
    for ( obj = ch->carrying; obj != NULL; obj = obj_next )
    {
        obj_next = obj->next_content;
        if ( obj->wear_loc == WEAR_NONE )
            continue;

        if ( ( IS_OBJ_STAT(obj, ITEM_ANTI_EVIL)    && IS_EVIL(ch)    )
             ||   ( IS_OBJ_STAT(obj, ITEM_ANTI_GOOD)    && IS_GOOD(ch)    )
             ||   ( IS_OBJ_STAT(obj, ITEM_ANTI_NEUTRAL) && IS_NEUTRAL(ch) ) )
            zap_char_obj(obj,FALSE);
    }
}



/*
 * See if a string is one of the names of an object.
 */

bool is_name ( char *str, char *namelist )
{
    char name[MAX_INPUT_LENGTH], part[MAX_INPUT_LENGTH];
    char *list, *string;

    /* fix crash on NULL namelist */
    if (namelist == NULL || namelist[0] == '\0')
        return FALSE;

    /* fixed to prevent is_name on "" returning TRUE */
    if (str[0] == '\0')
        return FALSE;

    string = str;
    /* we need ALL parts of string to match part of namelist */
    for ( ; ; )  /* start parsing string */
    {
        str = one_argument(str,part);

        if (part[0] == '\0' )
            return TRUE;

        /* check to see if this is part of namelist */
        list = namelist;
        for ( ; ; )  /* start parsing namelist */
        {
            list = one_argument(list,name);
            if (name[0] == '\0')  /* this name was not found */
                return FALSE;

            if (!str_prefix(string,name))
                return TRUE; /* full pattern match */

            if (!str_prefix(part,name))
                break;
        }
    }
}

bool is_exact_name(char *str, char *namelist )
{
    char name[MAX_INPUT_LENGTH];

    if (namelist == NULL)
        return FALSE;

    for ( ; ; )
    {
        namelist = one_argument( namelist, name );
        if ( name[0] == '\0' )
            return FALSE;
        if ( !str_cmp( str, name ) )
            return TRUE;
    }
}

/* enchanted stuff for eq */
void effect_enchant(OBJ_DATA *obj)
{
    /* okay, move all the old flags into new vectors if we have to */
    if (!obj->enchanted)
    {
        EFFECT_DATA *pef, *ef_new;
        obj->enchanted = TRUE;

        for (pef = obj->pIndexData->affected;
             pef != NULL; pef = pef->next)
        {
            ef_new = new_effect();

            ef_new->next = obj->affected;
            obj->affected = ef_new;

            ef_new->where	= pef->where;
            ef_new->type        = UMAX(0,pef->type);
            ef_new->level       = pef->level;
            ef_new->duration    = pef->duration;
            ef_new->location    = pef->location;
            ef_new->modifier    = pef->modifier;
            ef_new->bitvector   = pef->bitvector;
            ef_new->arg1	= pef->arg1;
        }
    }
}


/*
 * Apply or remove an effect to a character.
 */
void effect_modify( CHAR_DATA *ch, EFFECT_DATA *pef, bool fAdd )
{
    OBJ_DATA *wield;
    int mod,i;

    mod = pef->modifier;

    if ( fAdd )
    {
        switch (pef->where)
        {
        case TO_EFFECTS:
            STR_SET_BIT(ch->strbit_affected_by,pef->bitvector);
            break;
        case TO_EFFECTS2:
            STR_SET_BIT(ch->strbit_affected_by2,pef->bitvector);
            break;
        case TO_IMMUNE:
            STR_SET_BIT(ch->strbit_imm_flags,pef->bitvector);
            break;
        case TO_RESIST:
            STR_SET_BIT(ch->strbit_res_flags,pef->bitvector);
            break;
        case TO_VULN:
            STR_SET_BIT(ch->strbit_vuln_flags,pef->bitvector);
            break;
        }
    }
    else
    {
        switch (pef->where)
        {
        case TO_EFFECTS:
            STR_REMOVE_BIT(ch->strbit_affected_by,pef->bitvector);
            break;
        case TO_EFFECTS2:
            STR_REMOVE_BIT(ch->strbit_affected_by2,pef->bitvector);
            break;
        case TO_IMMUNE:
            STR_REMOVE_BIT(ch->strbit_imm_flags,pef->bitvector);
            break;
        case TO_RESIST:
            STR_REMOVE_BIT(ch->strbit_res_flags,pef->bitvector);
            break;
        case TO_VULN:
            STR_REMOVE_BIT(ch->strbit_vuln_flags,pef->bitvector);
            break;
        }
        mod = 0 - mod;
    }

    switch ( pef->location )
    {
    default:
        bugf("Effect_modify: unknown location %d for %s",
             pef->location,NAME(ch));
        return;

    case APPLY_NONE:						break;
    case APPLY_STR:             ch->mod_stat[STAT_STR]	+= mod;	break;
    case APPLY_DEX:             ch->mod_stat[STAT_DEX]	+= mod;	break;
    case APPLY_INT:             ch->mod_stat[STAT_INT]	+= mod;	break;
    case APPLY_WIS:             ch->mod_stat[STAT_WIS]	+= mod;	break;
    case APPLY_CON:             ch->mod_stat[STAT_CON]	+= mod;	break;
    case APPLY_SEX:             ch->Sex			+= mod;	break;
    case APPLY_CLASS:						break;
    case APPLY_LEVEL:						break;
    case APPLY_AGE:						break;
    case APPLY_HEIGHT:						break;
    case APPLY_WEIGHT:						break;
    case APPLY_MANA:            ch->max_mana		+= mod;	break;
    case APPLY_HIT:             ch->max_hit		+= mod;	break;
    case APPLY_MOVE:            ch->max_move		+= mod;	break;
    case APPLY_GOLD:						break;
    case APPLY_EXP:						break;
    case APPLY_AC:
        for (i = 0; i < 4; i ++)
            ch->armor[i] += mod;
        break;
    case APPLY_HITROLL:         ch->hitroll		+= mod;	break;
    case APPLY_DAMROLL:         ch->damroll		+= mod;	break;
    case APPLY_SAVES:		ch->saving_throw	+= mod;	break;
    case APPLY_SAVING_ROD:      ch->saving_throw	+= mod;	break;
    case APPLY_SAVING_PETRI:    ch->saving_throw	+= mod;	break;
    case APPLY_SAVING_BREATH:   ch->saving_throw	+= mod;	break;
    case APPLY_SAVING_SPELL:    ch->saving_throw	+= mod;	break;
    case APPLY_SPELL_EFFECT:  					break;
    case APPLY_ALIGNMENT:  	ch->alignment		+= mod; break;
    }
    char_obj_alignment_check(ch);

    /*
     * Check for weapon wielding.
     * Guard against recursion (for weapons with effects).
     */
    if ( !IS_NPC(ch) && ( wield = get_eq_char( ch, WEAR_WIELD ) ) != NULL
         &&   get_obj_weight(wield) > (str_app[get_curr_stat(ch,STAT_STR)].wield*10))
    {
        static int depth;
        OBJ_DATA *sec;

        // the check for quitting is because people were loosing their
        // weapons if the player was quitting, got undressed and was
        // loosing his weapon because of it.
        if ( depth == 0 && !STR_IS_SET(ch->strbit_act,PLR_QUITTING))
        {
            depth++;
            act( "You drop $p.", ch, wield, NULL, TO_CHAR );
            act( "$n drops $p.", ch, wield, NULL, TO_ROOM );
            obj_from_char( wield );
            obj_to_room( wield, ch->in_room );

            if ((sec=get_eq_char(ch,WEAR_SECONDARY))!=NULL) {
                unequip_char(ch,sec);
                equip_char(ch,sec,WEAR_WIELD);
                act("$n now uses $p as primary weapon.",ch,sec,NULL,TO_ROOM);
                act("You now use $p as primary weapon.",ch,sec,NULL,TO_CHAR);
            }
            depth--;
        }
    }

    return;
}


/* find an effect in an effect list */
EFFECT_DATA  *effect_find(EFFECT_DATA *pef, int where, int sn)
{
    EFFECT_DATA *pef_find;

    for ( pef_find = pef; pef_find != NULL; pef_find = pef_find->next )
    {
        if ( pef_find->type == sn && pef_find->where == where )
            return pef_find;
    }

    return NULL;
}

/* fix object effects when removing one */
void effect_check(CHAR_DATA *ch,int where,int vector)
{
    EFFECT_DATA *pef;
    OBJ_DATA *obj;

    if (where == TO_OBJECT || where == TO_WEAPON || vector == 0 )
        return;

    /* Fix permanent race effects and permenent mob effects */
    if(IS_NPC(ch)) {
        switch(where) {
        case TO_EFFECTS:
            if (STR_IS_SET(ch->pIndexData->strbit_affected_by,vector))
                STR_SET_BIT(ch->strbit_affected_by,vector);
            break;
        case TO_EFFECTS2:
            if (STR_IS_SET(ch->pIndexData->strbit_affected_by2,vector))
                STR_SET_BIT(ch->strbit_affected_by2,vector);
            break;
        case TO_IMMUNE:
            if (STR_IS_SET(ch->pIndexData->strbit_imm_flags,vector))
                STR_SET_BIT(ch->strbit_imm_flags,vector);
            break;
        case TO_RESIST:
            if (STR_IS_SET(ch->pIndexData->strbit_res_flags,vector))
                STR_SET_BIT(ch->strbit_res_flags,vector);
            break;
        case TO_VULN:
            if (STR_IS_SET(ch->pIndexData->strbit_vuln_flags,vector))
                STR_SET_BIT(ch->strbit_vuln_flags,vector);
            break;
        }
    } else {
        const struct race_type *race=&race_table[ch->race];

        switch(where) {
        case TO_EFFECTS:
            if (STR_IS_SET(race->strbit_eff,vector))
                STR_SET_BIT(ch->strbit_affected_by,vector);
            break;
        case TO_EFFECTS2:
            if (STR_IS_SET(race->strbit_eff2,vector))
                STR_SET_BIT(ch->strbit_affected_by2,vector);
            break;
        case TO_IMMUNE:
            if (STR_IS_SET(race->strbit_imm,vector))
                STR_SET_BIT(ch->strbit_imm_flags,vector);
            break;
        case TO_RESIST:
            if (STR_IS_SET(race->strbit_res,vector))
                STR_SET_BIT(ch->strbit_res_flags,vector);
            break;
        case TO_VULN:
            if (STR_IS_SET(race->strbit_vuln,vector))
                STR_SET_BIT(ch->strbit_vuln_flags,vector);
            break;
        }
    }

    for (pef = ch->affected; pef != NULL; pef = pef->next)
        if (pef->where == where && pef->bitvector==vector)
        {
            switch (where)
            {
            case TO_EFFECTS:
                STR_SET_BIT(ch->strbit_affected_by,vector);
                break;
            case TO_EFFECTS2:
                STR_SET_BIT(ch->strbit_affected_by2,vector);
                break;
            case TO_IMMUNE:
                STR_SET_BIT(ch->strbit_imm_flags,vector);
                break;
            case TO_RESIST:
                STR_SET_BIT(ch->strbit_res_flags,vector);
                break;
            case TO_VULN:
                STR_SET_BIT(ch->strbit_vuln_flags,vector);
                break;
            }
            return;
        }

    for (obj = ch->carrying; obj != NULL; obj = obj->next_content)
    {
        if (obj->wear_loc == WEAR_NONE)
            continue;

        for (pef = obj->affected; pef != NULL; pef = pef->next)
            if (pef->where == where && pef->bitvector == vector)
            {
                switch (where)
                {
                case TO_EFFECTS:
                    STR_SET_BIT(ch->strbit_affected_by,vector);
                    break;
                case TO_EFFECTS2:
                    STR_SET_BIT(ch->strbit_affected_by2,vector);
                    break;
                case TO_IMMUNE:
                    STR_SET_BIT(ch->strbit_imm_flags,vector);
                    break;
                case TO_RESIST:
                    STR_SET_BIT(ch->strbit_res_flags,vector);
                    break;
                case TO_VULN:
                    STR_SET_BIT(ch->strbit_vuln_flags,vector);

                }
                return;
            }

        if (obj->enchanted)
            continue;

        for (pef = obj->pIndexData->affected; pef != NULL; pef = pef->next)
            if (pef->where == where && pef->bitvector == vector)
            {
                switch (where)
                {
                case TO_EFFECTS:
                    STR_SET_BIT(ch->strbit_affected_by,vector);
                    break;
                case TO_EFFECTS2:
                    STR_SET_BIT(ch->strbit_affected_by2,vector);
                    break;
                case TO_IMMUNE:
                    STR_SET_BIT(ch->strbit_imm_flags,vector);
                    break;
                case TO_RESIST:
                    STR_SET_BIT(ch->strbit_res_flags,vector);
                    break;
                case TO_VULN:
                    STR_SET_BIT(ch->strbit_vuln_flags,vector);
                    break;
                }
                return;
            }
    }
}

/*
 * Give an effect to a char.
 */
void effect_to_char( CHAR_DATA *ch, EFFECT_DATA *pef )
{
    EFFECT_DATA *pef_new;

    pef_new = new_effect();

    *pef_new		= *pef;

    pef_new->next	= ch->affected;
    ch->affected	= pef_new;

    effect_modify( ch, pef_new, TRUE );
    return;
}

/* give an effect to an object */
void effect_to_obj(OBJ_DATA *obj, EFFECT_DATA *pef)
{
    EFFECT_DATA *pef_new;

    pef_new = new_effect();

    *pef_new		= *pef;

    pef_new->next	= obj->affected;
    obj->affected	= pef_new;

    /* apply any effect vectors to the object's extra_flags */
    if (pef->bitvector)
        switch (pef->where)
        {
        case TO_OBJECT:
            STR_SET_BIT(obj->strbit_extra_flags,pef->bitvector);
            break;
        case TO_OBJECT2:
            STR_SET_BIT(obj->strbit_extra_flags2,pef->bitvector);
            break;
        case TO_WEAPON:
            if (obj->item_type == ITEM_WEAPON && pef->bitvector)
                SET_BIT(obj->value[4],pef->bitvector);
            break;
        }


    return;
}

/* give an temporary effect to a room */
void effect_to_room(ROOM_INDEX_DATA *room, EFFECT_DATA *pef)
{
    EFFECT_DATA *pef_new;

    pef_new = new_effect();

    *pef_new		= *pef;

    pef_new->next	= room->eff_temp;
    room->eff_temp	= pef_new;

    /* apply any effect vectors to the object's extra_flags */
    switch(pef->where) {
    case TO_ROOM1:
        STR_SET_BIT(room->strbit_room_flags,pef->bitvector);
        break;
    case TO_ROOM2:
        SET_BIT(room->extra_flags,pef->bitvector);
        break;
    case TO_EXIT1:
        break;
    }

    return;
}



/*
 * Remove an effect from a char.
 */
void effect_remove( CHAR_DATA *ch, EFFECT_DATA *pef )
{
    int where;
    char vector;

    if ( ch->affected == NULL ) {
        bugf( "Effect_remove: no effect for %s.",NAME(ch));
        return;
    }

    effect_modify( ch, pef, FALSE );
    where = pef->where;
    vector= pef->bitvector;

    if ( pef == ch->affected ) {
        ch->affected	= pef->next;
    } else {
        EFFECT_DATA *prev;

        for ( prev = ch->affected; prev != NULL; prev = prev->next )
        {
            if ( prev->next == pef )
            {
                prev->next = pef->next;
                break;
            }
        }

        if ( prev == NULL )
        {
            bugf( "Effect_remove: cannot find pef for %s.", NAME(ch) );
            return;
        }
    }

    free_effect(pef);

    effect_check(ch,where,vector);
    return;
}

void effect_remove_obj( OBJ_DATA *obj, EFFECT_DATA *pef)
{
    int where;
    int vector;

    if ( obj->affected == NULL )
    {
        bugf( "Effect_remove_object: no effect for %s (vnum %d).",
              obj->short_descr,obj->pIndexData->vnum );
        return;
    }

    if (obj->carried_by != NULL && obj->wear_loc != WEAR_NONE)
        effect_modify( obj->carried_by, pef, FALSE );

    where = pef->where;
    vector=pef->bitvector;

    /* remove flags from the object if needed */
    switch( pef->where) {
    case TO_OBJECT:
        STR_REMOVE_BIT(obj->strbit_extra_flags,vector);
        break;
    case TO_OBJECT2:
        STR_REMOVE_BIT(obj->strbit_extra_flags2,vector);
        break;
    case TO_WEAPON:
        if (obj->item_type == ITEM_WEAPON)
            REMOVE_BIT(obj->value[4],pef->bitvector);
        break;
    }

    if ( pef == obj->affected ) {
        obj->affected    = pef->next;
    } else {
        EFFECT_DATA *prev;

        for ( prev = obj->affected; prev != NULL; prev = prev->next )
        {
            if ( prev->next == pef )
            {
                prev->next = pef->next;
                break;
            }
        }

        if ( prev == NULL )
        {
            bugf( "Effect_remove_object: cannot find pef for %s (vnum: %d)",
                  obj->short_descr,obj->pIndexData->vnum);
            return;
        }
    }

    free_effect(pef);

    if (obj->carried_by != NULL && obj->wear_loc != WEAR_NONE)
        effect_check(obj->carried_by,where,vector);
    return;
}

void effect_remove_room( ROOM_INDEX_DATA *room, EFFECT_DATA *pef)
{
    if ( room->eff_temp == NULL )
    {
        bugf( "Effect_remove_room: no effect for %s (vnum %d).",
              room->name,room->vnum);
        return;
    }

    /* remove flags from the object if needed */
    if (pef->bitvector) switch( pef->where) {
    case TO_ROOM1:
        STR_REMOVE_BIT(room->strbit_room_flags,pef->bitvector);
        break;
    case TO_ROOM2:
        REMOVE_BIT(room->extra_flags,pef->bitvector);
        break;
    case TO_EXIT1:
        break;
    }

    if ( pef == room->eff_temp )
    {
        room->eff_temp    = pef->next;
    }
    else
    {
        EFFECT_DATA *prev;

        for ( prev = room->eff_temp; prev != NULL; prev = prev->next )
        {
            if ( prev->next == pef )
            {
                prev->next = pef->next;
                break;
            }
        }

        if ( prev == NULL )
        {
            bugf( "Effect_remove_room: cannot find pef for %s (vnum %d).",
                  room->name,room->vnum);
            return;
        }
    }

    free_effect(pef);

    /* Fix bits */
    for(pef=room->eff_perm;pef;pef=pef->next)
        if(pef->bitvector) switch(pef->where) {
        case TO_ROOM1:
            STR_SET_BIT(room->strbit_room_flags,pef->bitvector);
            break;
        case TO_ROOM2:
            SET_BIT(room->extra_flags,pef->bitvector);
            break;
        case TO_EXIT1:
            break;
        }

    for(pef=room->eff_temp;pef;pef=pef->next)
        if(pef->bitvector) switch(pef->where) {
        case TO_ROOM1:
            SET_BIT(room->extra_flags,pef->bitvector);
            break;
        case TO_ROOM2:
            SET_BIT(room->extra_flags,pef->bitvector);
            break;
        case TO_EXIT1:
            break;
        }

    return;
}



/*
 * Strip all effects of a given sn.
 */
void effect_strip( CHAR_DATA *ch, int sn )
{
    EFFECT_DATA *pef;
    EFFECT_DATA *pef_next;

    for ( pef = ch->affected; pef != NULL; pef = pef_next )
    {
        pef_next = pef->next;
        if ( pef->type == sn )
            effect_remove( ch, pef );
    }

    return;
}



/*
 * Return true if a char is effected by a spell.
 */
bool is_affected( CHAR_DATA *ch, int sn )
{
    EFFECT_DATA *pef;

    for ( pef = ch->affected; pef != NULL; pef = pef->next )
    {
        if ( pef->type == sn )
            return TRUE;
    }

    return FALSE;
}


/*
 * Return effect if an object has the effect.
 */
EFFECT_DATA *obj_has_effect( OBJ_DATA *obj, int effect ) {
    EFFECT_DATA *pef;

    if (obj->enchanted) {
        for ( pef = obj->affected; pef != NULL; pef = pef->next )
            if (pef->where==TO_EFFECTS && pef->bitvector==effect)
                return pef;
        return NULL;
    } else {
        for ( pef = obj->pIndexData->affected; pef != NULL; pef = pef->next )
            if (pef->where==TO_EFFECTS && pef->bitvector==effect)
                return pef;
        return NULL;
    }
}



/*
 * Add or enhance an effect.
 */
void effect_join( CHAR_DATA *ch, EFFECT_DATA *pef )
{
    EFFECT_DATA *pef_old;

    for ( pef_old = ch->affected; pef_old != NULL; pef_old = pef_old->next )
    {
        if ( pef_old->type == pef->type && pef_old->where==pef->where)
        {
            pef->level = (pef->level + pef_old->level) / 2;
            pef->duration += pef_old->duration;
            pef->modifier += pef_old->modifier;
            effect_remove( ch, pef_old );
            break;
        }
    }

    effect_to_char( ch, pef );
    return;
}



/*
 * Move a char out of a room.
 */
void char_from_room( CHAR_DATA *ch )
{
    OBJ_DATA *obj;

    if ( ch->in_room == NULL ) {
        bugf( "Char_from_room: ch is NULL" );
        return;
    }

    if ( !IS_NPC(ch) ) {
        --ch->in_room->area->nplayer;
    }

    if ( ( obj = get_eq_char( ch, WEAR_LIGHT ) ) != NULL
         &&   obj->item_type == ITEM_LIGHT
         &&   obj->value[2] != 0
         &&   ch->in_room->light > 0 )
        --ch->in_room->light;

    if ( ch == ch->in_room->people )
    {
        ch->in_room->people = ch->next_in_room;
    }
    else
    {
        CHAR_DATA *prev;

        for ( prev = ch->in_room->people; prev; prev = prev->next_in_room )
        {
            if ( prev->next_in_room == ch )
            {
                prev->next_in_room = ch->next_in_room;
                break;
            }
        }

        if ( prev == NULL )
            bugf( "Char_from_room: ch not found (%s).", NAME(ch) );
    }

    ch->in_room      = NULL;
    ch->next_in_room = NULL;
    ch->on 	     = NULL;  /* sanity check! */
    return;
}



/*
 * Move a char into a room.
 */
void char_to_room( CHAR_DATA *ch, ROOM_INDEX_DATA *pRoomIndex )
{
    OBJ_DATA *obj;

    if ( pRoomIndex == NULL )
    {
        ROOM_INDEX_DATA *room;

        bugf( "Char_to_room: room is NULL." );

        if ((room = get_room_index(ROOM_VNUM_TEMPLE)) != NULL)
            char_to_room(ch,room);

        return;
    }

    ch->in_room		= pRoomIndex;
    ch->next_in_room	= pRoomIndex->people;
    pRoomIndex->people	= ch;

    if ( !IS_NPC(ch) )
    {
        if (ch->in_room->area->empty)
        {
            ch->in_room->area->empty = FALSE;
            ch->in_room->area->age = 0;
        }
        ++ch->in_room->area->nplayer;
        ch->pcdata->newhere=STR_IS_SET(ch->pcdata->beeninroom,pRoomIndex->vnum);
        STR_SET_BIT(ch->pcdata->beeninroom,pRoomIndex->vnum);
    }

    if ( ( obj = get_eq_char( ch, WEAR_LIGHT ) ) != NULL
         &&   obj->item_type == ITEM_LIGHT
         &&   obj->value[2] != 0 )
        ++ch->in_room->light;

    if (IS_AFFECTED(ch,EFF_PLAGUE))
    {
        EFFECT_DATA *ef, plague;
        CHAR_DATA *vch;

        for ( ef = ch->affected; ef != NULL; ef = ef->next )
        {
            if (ef->type == gsn_plague)
                break;
        }

        if (ef == NULL)
        {
            STR_REMOVE_BIT(ch->strbit_affected_by,EFF_PLAGUE);
            return;
        }

        if (ef->level == 1)
            return;

        ZEROVAR(&plague,EFFECT_DATA);
        plague.where		= TO_EFFECTS;
        plague.type 		= gsn_plague;
        plague.level 		= ef->level - 1;
        plague.duration 	= number_range(1,2 * plague.level);
        plague.location		= APPLY_STR;
        plague.modifier 	= -5;
        plague.bitvector	= EFF_PLAGUE;
        plague.casted_by	= 0;

        for ( vch = ch->in_room->people; vch != NULL; vch = vch->next_in_room)
        {
            if (!saves_spell(plague.level - 2,vch,DAM_DISEASE)
                    &&  !IS_IMMORTAL(vch) &&
                    !IS_AFFECTED(vch,EFF_PLAGUE) && number_bits(6) == 0)
            {
                send_to_char("You feel hot and feverish.\n\r",vch);
                act("$n shivers and looks very ill.",vch,NULL,NULL,TO_ROOM);
                effect_join(vch,&plague);
            }
        }
    }


    return;
}



/*
 * Give an obj to a char.
 */
void obj_to_char( OBJ_DATA *obj, CHAR_DATA *ch )
{
    if ( obj->in_room != NULL ) {
        bugf("obj_to_char: obj[%d] is still in a room[%d]",obj->pIndexData->vnum,obj->in_room->vnum);
        obj_from_room( obj );
    } else if ( obj->carried_by != NULL ) {
        bugf("obj_to_char: obj[%d] is still on a char[%s]",obj->pIndexData->vnum,NAME(obj->carried_by));
        obj_from_char( obj );
    } else if ( obj->in_obj != NULL ) {
        bugf("obj_to_char: obj[%d] is still in an obj[%d]",obj->pIndexData->vnum,obj->in_obj->pIndexData->vnum);
        obj_from_obj( obj );
    }

    obj->next_content	 = ch->carrying;
    ch->carrying	 = obj;
    obj->carried_by	 = ch;
    obj->in_room	 = NULL;
    obj->in_obj		 = NULL;
    ch->carry_number	+= get_obj_number( obj );
    ch->carry_weight	+= get_obj_weight( obj );

    /* Check objects from unfinished area's */
    if(!IS_NPC(ch)
            && !obj->timer
            && obj->pIndexData->area->area_flags&AREA_UNFINISHED) obj->timer=5;
}



/*
 * Take an obj from its character.
 */
void obj_from_char( OBJ_DATA *obj )
{
    CHAR_DATA *ch;

    if ( ( ch = obj->carried_by ) == NULL )
    {
        bugf( "Obj_from_char: null ch for %s (vnum: %d).",
              obj->short_descr,obj->pIndexData->vnum );
        return;
    }

    if ( obj->wear_loc != WEAR_NONE )
        unequip_char( ch, obj );

    if ( ch->carrying == obj ) {
        ch->carrying = obj->next_content;
    } else {
        OBJ_DATA *prev;

        for ( prev = ch->carrying; prev != NULL; prev = prev->next_content )
        {
            if ( prev->next_content == obj )
            {
                prev->next_content = obj->next_content;
                break;
            }
        }

        if ( prev == NULL )
            bugf( "Obj_from_char: obj '%s' not in list (vnum: %d.",
                  obj->short_descr,obj->pIndexData->vnum );
    }

    obj->carried_by	 = NULL;
    obj->next_content	 = NULL;
    ch->carry_number	-= get_obj_number( obj );
    ch->carry_weight	-= get_obj_weight( obj );
    return;
}



/*
 * Find the ac value of an obj, including position effect.
 */
int apply_ac( OBJ_DATA *obj, int iWear, int type )
{
    if ( obj->item_type != ITEM_ARMOR )
        return 0;

    switch ( iWear )
    {
    case WEAR_BODY:	return 3 * obj->value[type];
    case WEAR_HEAD:	return 2 * obj->value[type];
    case WEAR_LEGS:	return 2 * obj->value[type];
    case WEAR_FINGER_L:	return     obj->value[type];
    case WEAR_FINGER_R:	return     obj->value[type];
    case WEAR_FEET:	return     obj->value[type];
    case WEAR_HANDS:	return     obj->value[type];
    case WEAR_ARMS:	return     obj->value[type];
    case WEAR_SHIELD:	return     obj->value[type];
    case WEAR_NECK_1:	return     obj->value[type];
    case WEAR_NECK_2:	return     obj->value[type];
    case WEAR_ABOUT:	return 2 * obj->value[type];
    case WEAR_WAIST:	return     obj->value[type];
    case WEAR_WRIST_L:	return     obj->value[type];
    case WEAR_WRIST_R:	return     obj->value[type];
    case WEAR_HOLD:	return     obj->value[type];
    }

    return 0;
}



/*
 * Find a piece of eq on a character.
 */
OBJ_DATA *get_eq_char( CHAR_DATA *ch, int iWear )
{
    OBJ_DATA *obj;

    if (ch == NULL)
        return NULL;

    for ( obj = ch->carrying; obj != NULL; obj = obj->next_content )
    {
        if ( obj->wear_loc == iWear )
            return obj;
    }

    return NULL;
}



/*
 * Equip a char with an obj.
 */
void equip_char( CHAR_DATA *ch, OBJ_DATA *obj, int iWear )
{
    EFFECT_DATA *pef;
    int i;
    char sex[MAX_STRING_LENGTH];
    OBJ_DATA *dobj,*dobj_next;

    if ( get_eq_char( ch, iWear ) != NULL )
    {
        if(IS_NPC(ch))
        {
            bugf("Equip_char: mob %d already equipped (%d) in room %d.",
                 ch->pIndexData->vnum,iWear,ch->in_room?ch->in_room->vnum:-1);
        }
        else bugf( "Equip_char: already equipped (%d) for %s.",iWear,NAME(ch));
        return;
    }

    // first time
    if ( ( IS_OBJ_STAT(obj, ITEM_ANTI_EVIL)    && IS_EVIL(ch)    )
         ||   ( IS_OBJ_STAT(obj, ITEM_ANTI_GOOD)    && IS_GOOD(ch)    )
         ||   ( IS_OBJ_STAT(obj, ITEM_ANTI_NEUTRAL) && IS_NEUTRAL(ch) )
         ||	 ( !IS_NPC(ch) && (
                   (( IS_OBJ_STAT2(obj,ITEM_ANTI_CLASS)||IS_OBJ_STAT2(obj,ITEM_CLASS_ONLY)) && is_anti_class(obj,ch->class))
                   || (( IS_OBJ_STAT2(obj,ITEM_ANTI_RACE)||IS_OBJ_STAT2(obj,ITEM_RACE_ONLY)) && is_anti_race(obj,ch->race))
                   )
               ) )
    {
        zap_char_obj(obj,FALSE);	/* in fight.c */
        return;
    }

    if (GetObjectProperty(obj,PROPERTY_STRING,"anti-sex",sex)) {
        if (!str_cmp(table_find_value(show_sex(ch),sex_table),sex)) {
            zap_char_obj(obj,FALSE);
            return;
        }
    }

    if (GetObjectProperty(obj,PROPERTY_STRING,"sex-only",sex)) {
        if (str_cmp(table_find_value(show_sex(ch),sex_table),sex)) {
            zap_char_obj(obj,FALSE);
            return;
        }
    }

    for (i = 0; i < 4; i++)
        ch->armor[i]      	-= apply_ac( obj, iWear,i );
    obj->wear_loc	 = iWear;

    if (IS_PC(ch))
        STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);

    if (!obj->enchanted)
        for ( pef = obj->pIndexData->affected; pef != NULL; pef = pef->next )
            if ( pef->location != APPLY_SPELL_EFFECT )
                effect_modify( ch, pef, TRUE );
    for ( pef = obj->affected; pef != NULL; pef = pef->next )
        if ( pef->location == APPLY_SPELL_EFFECT )
            effect_to_char ( ch, pef );
        else
            effect_modify( ch, pef, TRUE );

    for ( dobj = ch->carrying; dobj != NULL; dobj = dobj_next ) {
        dobj_next = dobj->next_content;
        if ( dobj->wear_loc == WEAR_NONE )
            continue;

        if ( ( IS_OBJ_STAT(dobj, ITEM_ANTI_EVIL)    && IS_EVIL(ch)    )
             ||   ( IS_OBJ_STAT(dobj, ITEM_ANTI_GOOD)    && IS_GOOD(ch)    )
             ||   ( IS_OBJ_STAT(dobj, ITEM_ANTI_NEUTRAL) && IS_NEUTRAL(ch) ) ) {
            zap_char_obj(dobj,FALSE);
        }
    }

    if ( obj->item_type == ITEM_LIGHT
         &&   obj->value[2] != 0
         &&   ch->in_room != NULL )
        ++ch->in_room->light;

    return;
}



/*
 * Unequip a char with an obj.
 */
void unequip_char( CHAR_DATA *ch, OBJ_DATA *obj )
{
    EFFECT_DATA *pef = NULL;
    EFFECT_DATA *lpef = NULL;
    EFFECT_DATA *lpef_next = NULL;
    OBJ_DATA *dobj,*dobj_next;
    int i;

    if ( obj->wear_loc == WEAR_NONE )
    {
        bugf( "Unequip_char: already unequipped '%s' for %s.",
              obj->short_descr,NAME(ch) );
        return;
    }

    for (i = 0; i < 4; i++)
        ch->armor[i]	+= apply_ac( obj, obj->wear_loc,i );
    obj->wear_loc	 = WEAR_NONE;
    if (IS_PC(ch))
        STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);

    if (!obj->enchanted) {
        for ( pef = obj->pIndexData->affected; pef != NULL; pef = pef->next )
            if ( pef->location == APPLY_SPELL_EFFECT )
            {
                for ( lpef = ch->affected; lpef != NULL; lpef = lpef_next )
                {
                    lpef_next = lpef->next;
                    if ((lpef->type == pef->type) &&
                            (lpef->level == pef->level) &&
                            (lpef->location == APPLY_SPELL_EFFECT))
                    {
                        effect_remove( ch, lpef );
                        lpef_next = NULL;
                    }
                }
            }
            else
            {
                effect_modify( ch, pef, FALSE );
                effect_check(ch,pef->where,pef->bitvector);
            }
    }

    for ( pef = obj->affected; pef != NULL; pef = pef->next )
        if ( pef->location == APPLY_SPELL_EFFECT )
        {
            for ( lpef = ch->affected; lpef != NULL; lpef = lpef_next )
            {
                lpef_next = lpef->next;
                if ((lpef->type == pef->type) &&
                        (lpef->level == pef->level) &&
                        (lpef->location == APPLY_SPELL_EFFECT))
                {
                    effect_remove( ch, lpef );
                    lpef_next = NULL;
                }
            }
        } else {
            effect_modify( ch, pef, FALSE );
            effect_check(ch,pef->where,pef->bitvector);
        }

    for ( dobj = ch->carrying; dobj != NULL; dobj = dobj_next ) {
        dobj_next = dobj->next_content;
        if ( dobj->wear_loc == WEAR_NONE )
            continue;

        if ( ( IS_OBJ_STAT(dobj, ITEM_ANTI_EVIL)    && IS_EVIL(ch)    )
             ||   ( IS_OBJ_STAT(dobj, ITEM_ANTI_GOOD)    && IS_GOOD(ch)    )
             ||   ( IS_OBJ_STAT(dobj, ITEM_ANTI_NEUTRAL) && IS_NEUTRAL(ch) ) ) {
            zap_char_obj(dobj,FALSE);
        }
    }

    if ( obj->item_type == ITEM_LIGHT
         &&   obj->value[2] != 0
         &&   ch->in_room != NULL
         &&   ch->in_room->light > 0 )
        --ch->in_room->light;

    return;
}



/*
 * Count occurrences of an obj in a list.
 */
int count_obj_list( OBJ_INDEX_DATA *pObjIndex, OBJ_DATA *list )
{
    OBJ_DATA *obj;
    int nMatch;

    nMatch = 0;
    for ( obj = list; obj != NULL; obj = obj->next_content )
    {
        if ( obj->pIndexData == pObjIndex )
            nMatch++;
    }

    return nMatch;
}



/*
 * Move an obj out of a room.
 */
void obj_from_room( OBJ_DATA *obj )
{
    ROOM_INDEX_DATA *in_room;
    CHAR_DATA *ch;

    if ( ( in_room = obj->in_room ) == NULL )
    {
        bugf( "obj_from_room: NULL for %s (vnum: %d)",
              obj->short_descr,obj->pIndexData->vnum );
        return;
    }

    for (ch = in_room->people; ch != NULL; ch = ch->next_in_room)
        if (ch->on == obj)
            ch->on = NULL;

    if ( obj == in_room->contents )
    {
        in_room->contents = obj->next_content;
    }
    else
    {
        OBJ_DATA *prev;

        for ( prev = in_room->contents; prev; prev = prev->next_content )
        {
            if ( prev->next_content == obj )
            {
                prev->next_content = obj->next_content;
                break;
            }
        }

        if ( prev == NULL )
        {
            bugf("Obj_from_room: obj (%d) not found in room (%d).",
                 obj->pIndexData->vnum,
                 obj->in_room?obj->in_room->vnum:-1);
            return;
        }
    }

    obj->in_room      = NULL;
    obj->next_content = NULL;
    STR_REMOVE_BIT(obj->strbit_extra_flags,ITEM_INSTALLED);

    return;
}



/*
 * Move an obj into a room.
 */
void obj_to_room( OBJ_DATA *obj, ROOM_INDEX_DATA *pRoomIndex )
{
    if ( obj->in_room != NULL ) {
        bugf("obj_to_room: obj[%d] is still in a room[%d]",obj->pIndexData->vnum,obj->in_room->vnum);
        obj_from_room( obj );
    } else if ( obj->carried_by != NULL ) {
        bugf("obj_to_room: obj[%d] is still on a char[%s]",obj->pIndexData->vnum,NAME(obj->carried_by));
        obj_from_char( obj );
    } else if ( obj->in_obj != NULL ) {
        bugf("obj_to_room: obj[%d] is still in an obj[%d]",obj->pIndexData->vnum,obj->in_obj->pIndexData->vnum);
        obj_from_obj( obj );
    }

    obj->next_content		= pRoomIndex->contents;
    pRoomIndex->contents	= obj;
    obj->in_room		= pRoomIndex;
    obj->carried_by		= NULL;
    obj->in_obj			= NULL;
    return;
}



/*
 * Move an object into an object.
 */
void obj_to_obj( OBJ_DATA *obj, OBJ_DATA *obj_to )
{
    if ( obj==obj_to )
    {
        bugf("Obj_to_obj: putting an object in itself (vnum=%d).",
             obj->pIndexData?obj->pIndexData->vnum:-1);
        return;
    }

    if ( obj->in_room != NULL ) {
        bugf("obj_to_obj: obj[%d] is still in a room[%d]",obj->pIndexData->vnum,obj->in_room->vnum);
        obj_from_room( obj );
    } else if ( obj->carried_by != NULL ) {
        bugf("obj_to_obj: obj[%d] is still on a char[%s]",obj->pIndexData->vnum,NAME(obj->carried_by));
        obj_from_char( obj );
    } else if ( obj->in_obj != NULL ) {
        bugf("obj_to_obj: obj[%d] is still in an obj[%d]",obj->pIndexData->vnum,obj->in_obj->pIndexData->vnum);
        obj_from_obj( obj );
    }

    obj->next_content		= obj_to->contains;
    obj_to->contains		= obj;
    obj->in_obj			= obj_to;
    obj->in_room		= NULL;
    obj->carried_by		= NULL;

    for ( ; obj_to != NULL; obj_to = obj_to->in_obj )
    {
        if ( obj_to->carried_by != NULL )
        {
            obj_to->carried_by->carry_number += get_obj_number( obj );
            obj_to->carried_by->carry_weight += get_obj_weight( obj )
                    * WEIGHT_MULT(obj_to) / 100;
        }
    }

    return;
}



/*
 * Move an object out of an object.
 */
void obj_from_obj( OBJ_DATA *obj )
{
    OBJ_DATA *obj_from;

    if ( ( obj_from = obj->in_obj ) == NULL )
    {
        bugf( "Obj_from_obj: null obj_from for %s (vnum %d).",
              obj->short_descr,obj->pIndexData->vnum);
        return;
    }

    if ( obj == obj_from->contains )
    {
        obj_from->contains = obj->next_content;
    }
    else
    {
        OBJ_DATA *prev;

        for ( prev = obj_from->contains; prev; prev = prev->next_content )
        {
            if ( prev->next_content == obj )
            {
                prev->next_content = obj->next_content;
                break;
            }
        }

        if ( prev == NULL )
        {
            bugf( "Obj_from_obj: obj not found for %s (vnum: %d)",
                  obj->short_descr,obj->pIndexData->vnum );
            return;
        }
    }

    obj->next_content = NULL;
    obj->in_obj       = NULL;

    for ( ; obj_from != NULL; obj_from = obj_from->in_obj )
    {
        if ( obj_from->carried_by != NULL )
        {
            obj_from->carried_by->carry_number -= get_obj_number( obj );
            obj_from->carried_by->carry_weight -= get_obj_weight( obj )
                    * WEIGHT_MULT(obj_from) / 100;
        }
    }

    return;
}

void extract_owned_obj(const char *owner, long except) {
    OBJ_DATA *obj;

    obj=object_list;

    while (obj) {

        // yes, this might seem like a weird way to go about it
        // but it't the only simple way to handle obj dissapearing
        // randomly in the list
        while (obj && (obj->owner_name==NULL ||
                       str_cmp(obj->owner_name,owner) ||
                       (except>0 && (
                            obj->owner_id==-1 ||
                            obj->owner_id==except))))
            obj=obj->next;

        if (obj) {
            wiznet(WIZ_DEATHS,0,NULL,NULL,"Extracting %s[%d] owned by %s because char is know to have deleted/restarted",
                   obj->name,
                   obj->pIndexData?obj->pIndexData->vnum:0,
                   obj->owner_name);
            logf("Extracting %s[%d] owned by %s because char is know to have deleted/restarted",
                 obj->name,
                 obj->pIndexData?obj->pIndexData->vnum:0,
                 obj->owner_name);
            extract_obj(obj);
            obj=object_list;
            continue;
        }
    }

}

void extract_invalid_owned_obj_from_char(CHAR_DATA *ch) {
    OBJ_DATA *obj;
    CHAR_DATA *och;

    do {
        for (obj=ch->carrying;obj;obj=obj->next_content) {

            if (!obj->owner_name || obj->owner_name[0]==0) continue;
            if (!str_cmp(obj->owner_name,ch->name) && obj->owner_id==ch->id) continue;

            for (och=char_list;och;och=och->next)
                if (IS_PC(och) && !str_cmp(obj->owner_name,och->name)) break;

            if (!och) continue;
            if (obj->owner_id==-1) obj->owner_id=och->id;
            if (och->id==obj->owner_id) continue;

            //real owner is online and id doesn't match -> extract obj

            wiznet(WIZ_DEATHS,0,NULL,NULL,"Extracting %s[%d] owned by %s because char is know to have deleted/restarted",
                   obj->name,
                   obj->pIndexData?obj->pIndexData->vnum:0,
                   obj->owner_name);
            logf("Extracting %s[%d] owned by %s because char is know to have deleted/restarted",
                 obj->name,
                 obj->pIndexData?obj->pIndexData->vnum:0,
                 obj->owner_name);
            extract_obj(obj);
        }
    } while (obj);

}

/*
 * Extract an obj from the world.
 */
void extract_obj( OBJ_DATA *obj )
{
    TCLPROG_LIST *pList,*pList_next;

    if ( obj->in_room != NULL )
        obj_from_room( obj );
    else if ( obj->carried_by != NULL )
        obj_from_char( obj );
    else if ( obj->in_obj != NULL )
        obj_from_obj( obj );

    while ( obj->contains )
        extract_obj( obj->contains );

    {
        int vnum=obj->pIndexData->vnum;
        if ((vnum==OBJ_VNUM_DONATION_VIAL) ||
                (vnum==OBJ_VNUM_DONATION_SCROLL) ||
                (vnum==OBJ_VNUM_DONATION_FOOD))
            donate_force_find_targets=TRUE;
    }

    // Remove object references from the mob programs.
    tcl_delete_obj_instance(obj);

    /* remove all triggers */
    for ( pList = obj->oprogs; pList!=NULL;pList=pList_next)
    {
        pList_next=pList->next;
        free_oprog_list(pList);
    }
    tcl_extracted_obj(obj);

    if ( object_list == obj )
    {
        object_list = obj->next;
    }
    else
    {
        OBJ_DATA *prev;

        for ( prev = object_list; prev != NULL; prev = prev->next )
        {
            if ( prev->next == obj )
            {
                prev->next = obj->next;
                break;
            }
        }

        if ( prev == NULL )
        {
            bugf( "Extract_obj: obj %d not found (room %d)",
                  obj->pIndexData->vnum,
                  obj->in_room==NULL?-1:obj->in_room->vnum);
            return;
        }
    }

    --obj->pIndexData->count;
    free_obj(obj);
    return;
}



/*
 * Extract a char from the world.
 */
void extract_char( CHAR_DATA *ch, bool fPull )
{
    CHAR_DATA *wch;
    OBJ_DATA *obj;
    OBJ_DATA *obj_next;
    TCLPROG_LIST *pList,*pList_next;

    nuke_pets(ch);
    ch->pet = NULL; /* just in case */

    if ( fPull ) die_follower( ch );

    stop_fighting( ch, TRUE );

    tcl_delete_char_instance(ch);

    // remove all triggers
    for ( pList = ch->mprogs; pList!=NULL;pList=pList_next) {
        pList_next=pList->next;
        free_mprog_list(pList);
    }

    // remove all objects from player (can be recursive)
    for ( obj = ch->carrying; obj != NULL; obj = obj_next ) {
        obj_next = obj->next_content;
        extract_obj( obj );
    }

    if (IS_PC(ch) && !fPull &&
            ch->in_room && (ch->in_room->vnum==ROOM_VNUM_HELL)) {
        // so you died in hell.... That's no escape.
        // from_room
        // back to same room.
        return;
    }

    /* Death room is set in the clan tabe now */
    if ( !fPull )
    {
        int to_room_i;
        ROOM_DATA *to_room=NULL;
        ROOM_DATA *from_room=ch->in_room;
        if (GetRoomProperty(from_room,PROPERTY_INT,"resurrection-room",&to_room_i))
            to_room=get_room_index(to_room_i);
        if (to_room==NULL && ch->clan)
            to_room=get_room_index(ch->clan->clan_info->hall);
        if (to_room==NULL)
            to_room=get_room_index(ROOM_VNUM_TEMPLE);

        ap_leave_trigger(ch,to_room);
        if (ch->in_room!= NULL)
            char_from_room( ch );

        char_to_room(ch,to_room);
        ap_enter_trigger(ch,from_room);
        return;
    }

    if (ch->in_room!= NULL)
        char_from_room( ch );

    if ( IS_NPC(ch) )
        --ch->pIndexData->count;

    if ( ch->desc != NULL && ch->desc->original != NULL )
    {
        do_function(ch,&do_return,"");
        ch->desc = NULL;
    }

    for ( wch = char_list; wch != NULL; wch = wch->next )
    {
        if ( wch->reply == ch )
            wch->reply = NULL;
        if ( wch->mprog_target == ch )
            wch->mprog_target = NULL;
    }

    // Remove references in mob programs.
    tcl_extracted_char(ch);

    // remove ch from the char_list
    if ( ch == char_list ) {
        char_list = ch->next;
    } else {
        CHAR_DATA *prev;

        for ( prev = char_list; prev != NULL; prev = prev->next ) {
            if ( prev->next == ch ) {
                prev->next = ch->next;
                break;
            }
        }

        if ( prev == NULL ) {
            bugf( "Extract_char(): char not found in char_list for %s", NAME(ch) );
            return;
        }
    }

    // remove ch from the player_list
    if ( IS_PC(ch)) {
        if (ch==player_list) {
            player_list=player_list->next_player;
        } else {
            CHAR_DATA *prev;

            for ( prev = player_list; prev != NULL; prev = prev->next_player ) {
                if ( prev->next_player == ch ) {
                    prev->next_player = ch->next_player;
                    break;
                }
            }

            if ( prev == NULL ) {
                bugf("Extract_char(): char not found in player_list for %s",NAME(ch) );
                return;
            }
        }
        update_wiznet(FALSE,NULL);
    }

    if ( ch->clan)
        ch->clan->player=NULL;
    if ( ch->desc != NULL )
        ch->desc->character = NULL;
    free_char( ch );
    return;
}



/*
 * Find a char in the room.
 */
CHAR_DATA *get_char_room( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *rch;
    int number;
    int count;

    number = number_argument( argument, arg );
    count  = 0;
    if ( !str_cmp( arg, "self" ) )
        return ch;
    for ( rch = ch->in_room->people; rch != NULL; rch = rch->next_in_room )
    {
        if ( !can_see( ch, rch ) || !is_name( arg, rch->name ) )
            continue;
        if ( ++count == number )
            return rch;
    }

    return NULL;
}




/*
 * Find a char in the world.
 */
CHAR_DATA *get_char_world( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *wch,*best_match=NULL;
    int number;
    int count,exact_count;

    if (ch)
        best_match=get_char_room( ch, argument );

    number = number_argument( argument, arg );
    count  = 0;
    exact_count  = 0;

    if (best_match && is_exact_name(arg, best_match->name))
        return best_match;

    for ( wch = char_list; wch != NULL ; wch = wch->next )
    {
        if ( wch->in_room == NULL || !can_see( ch, wch )
             ||   !is_name( arg, wch->name ) )
            continue;
        if (!best_match)
            if ( ++count == number )
                best_match=wch;
        if (is_exact_name(arg,wch->name) )
            if ( ++exact_count == number )
                return wch;
    }

    return best_match;
}


/*
 * Find a char in an area
 */
CHAR_DATA *get_char_area( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *wch;
    int number;
    int count;

    if ( ( wch = get_char_room( ch, argument ) ) != NULL )
        return wch;

    number = number_argument( argument, arg );
    count  = 0;
    for ( wch = char_list; wch != NULL ; wch = wch->next )
    {
        if ( wch->in_room == NULL
             || wch->in_room->area!=ch->in_room->area
             || !can_see( ch, wch )
             || !is_name( arg, wch->name ) )
            continue;
        if ( ++count == number )
            return wch;
    }

    return NULL;
}

/*
 * Find some object with a given index data.
 * Used by area-reset 'P' command.
 */
OBJ_DATA *get_obj_type( OBJ_INDEX_DATA *pObjIndex )
{
    OBJ_DATA *obj;

    for ( obj = object_list; obj != NULL; obj = obj->next )
    {
        if ( obj->pIndexData == pObjIndex )
            return obj;
    }

    return NULL;
}


/*
 * Find an obj in a list.
 */
OBJ_DATA *get_obj_list_numbered( CHAR_DATA *ch, char *argument, OBJ_DATA *list, int *count )
{
    char arg[MAX_INPUT_LENGTH];
    OBJ_DATA *obj;
    int number;

    number = number_argument( argument, arg );
    for ( obj = list; obj != NULL; obj = obj->next_content )
    {
        if ( can_see_obj( ch, obj ) && is_name( arg, obj->name ) )
        {
            if ( ++*count == number )
                return obj;
        }
    }

    return NULL;
}
OBJ_DATA *get_obj_list( CHAR_DATA *ch, char *argument, OBJ_DATA *list ) {
    int counted=0;
    return get_obj_list_numbered(ch,argument,list,&counted);
}



/*
 * Find an obj in player's inventory.
 */
OBJ_DATA *get_obj_carry_numbered( CHAR_DATA *ch, char *argument, CHAR_DATA *viewer , int *count)
{
    char arg[MAX_INPUT_LENGTH];
    OBJ_DATA *obj;
    int number;

    number = number_argument( argument, arg );
    for ( obj = ch->carrying; obj != NULL; obj = obj->next_content )
    {
        if ( obj->wear_loc == WEAR_NONE
             &&   (can_see_obj( viewer, obj ) )
             &&   is_name( arg, obj->name ) )
        {
            if ( ++*count == number )
                return obj;
        }
    }

    return NULL;
}
OBJ_DATA *get_obj_carry( CHAR_DATA *ch, char *argument, CHAR_DATA *viewer ) {
    int counted=0;
    return get_obj_carry_numbered(ch,argument,viewer,&counted);
}


/*
 * Find an obj by vnum in player's inventory.
 */
bool get_obj_carry_by_vnum( CHAR_DATA *ch, int vnum)
{
    OBJ_DATA *obj;

    for ( obj = ch->carrying; obj != NULL; obj = obj->next_content ) {
        if (( obj->wear_loc == WEAR_NONE )
                && ( obj->pIndexData )
                && ( obj->pIndexData->vnum==vnum )) {
            return TRUE;
        }
    }

    return FALSE;
}



/*
 * Find an obj in player's equipment.
 */
OBJ_DATA *get_obj_wear_numbered( CHAR_DATA *ch, char *argument,int *count )
{
    char arg[MAX_INPUT_LENGTH];
    OBJ_DATA *obj;
    int number;

    number = number_argument( argument, arg );
    for ( obj = ch->carrying; obj != NULL; obj = obj->next_content )
    {
        if ( obj->wear_loc != WEAR_NONE
             &&   can_see_obj( ch, obj )
             &&   is_name( arg, obj->name ) )
        {
            if ( ++*count == number )
                return obj;
        }
    }

    return NULL;
}
OBJ_DATA *get_obj_wear( CHAR_DATA *ch, char *argument ) {
    int counted=0;
    return get_obj_wear_numbered(ch,argument,&counted);
}

/*
 * Find an object in the room based by object_type
 * Currently only used by 'P' kind of resetting (object in object).
 *
 */
OBJ_DATA *get_obj_here_by_objtype(OBJ_INDEX_DATA *pObjIndex,OBJ_DATA *list) {
    while (list) {
        if (list->pIndexData==pObjIndex)
            return list;
        list=list->next;
    }
    return NULL;
}


/*
 * Find an obj in the room or in inventory.
 */
OBJ_DATA *get_obj_here( CHAR_DATA *ch, char *argument )
{
    OBJ_DATA *obj;
    int counted=0;

    if ((obj=get_obj_list_numbered(ch,argument,ch->in_room->contents,&counted)))
        return obj;

    if ((obj=get_obj_carry_numbered(ch,argument,ch,&counted)))
        return obj;

    if ((obj=get_obj_wear_numbered(ch,argument,&counted)))
        return obj;

    return NULL;
}



/*
 * Find an obj in the world.
 */
OBJ_DATA *get_obj_world( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    OBJ_DATA *obj;
    int number;
    int count;

    if ( ( obj = get_obj_here( ch, argument ) ) != NULL )
        return obj;

    number = number_argument( argument, arg );
    count  = 0;
    for ( obj = object_list; obj != NULL; obj = obj->next )
    {
        if ( can_see_obj( ch, obj ) && is_name( arg, obj->name ) )
        {
            if ( ++count == number )
                return obj;
        }
    }

    return NULL;
}

//
// deduct cost from a character
//
void deduct_cost(CHAR_DATA *ch,int costgold,int costsilver,bool canuseCreditCard) {
    int silver = 0, gold = 0;
    OBJ_DATA *creditcard;
    bool has_creditcard;

    has_creditcard=FALSE;
    creditcard=get_eq_char(ch,WEAR_HOLD);
    if (creditcard)
        GetObjectProperty(creditcard,PROPERTY_BOOL,
                          "creditcard",&has_creditcard);
    if (has_creditcard && canuseCreditCard) {
        if (IS_NPC(ch)) {
            send_to_char("You're not allowed to carry creditcards!\n\r",ch);
            return;
        }
        ch->pcdata->bank-=100*costgold+costsilver;
        return;
    }

    silver=UMIN(ch->silver,costsilver);
    if (silver<costsilver) {
        gold=((costsilver-silver+99)/100);
        silver=costsilver-100*gold;
    }

    ch->gold-=gold+costgold;
    ch->silver-=silver;

    if (ch->gold<0) {
        bugf("deduct_costs: gold %d < 0 for %s",ch->gold,NAME(ch));
        ch->gold=0;
    }
    if (ch->silver<0) {
        bugf("deduct costs: silver %d < 0 for %s",ch->silver,NAME(ch));
        ch->silver=0;
    }
}

//
// Create a 'money' obj.
//
OBJ_DATA *create_money( int gold, int silver )
{
    char buf[MAX_STRING_LENGTH];
    OBJ_DATA *obj;

    if ( gold < 0 || silver < 0 || (gold == 0 && silver == 0) )
    {
        bugf("Create_money: zero or negative money (%d gold, %d silver).",
             gold,silver);
        gold = UMAX(1,gold);
        silver = UMAX(1,silver);
    }

    if (gold == 0 && silver == 1)
    {
        obj = create_object( get_obj_index( OBJ_VNUM_SILVER_ONE ), 0 );
    }
    else if (gold == 1 && silver == 0)
    {
        obj = create_object( get_obj_index( OBJ_VNUM_GOLD_ONE), 0 );
    }
    else if (silver == 0)
    {
        obj = create_object( get_obj_index( OBJ_VNUM_GOLD_SOME ), 0 );
        sprintf( buf, obj->short_descr, gold );
        free_string( obj->short_descr );
        obj->short_descr        = str_dup( buf );
        obj->value[1]           = gold;
        obj->cost               = gold;
        obj->weight		= gold * 2 / 5;
    }
    else if (gold == 0)
    {
        obj = create_object( get_obj_index( OBJ_VNUM_SILVER_SOME ), 0 );
        sprintf( buf, obj->short_descr, silver );
        free_string( obj->short_descr );
        obj->short_descr        = str_dup( buf );
        obj->value[0]           = silver;
        obj->cost               = silver;
        obj->weight		= silver/10;
    }

    else
    {
        obj = create_object( get_obj_index( OBJ_VNUM_COINS ), 0 );
        sprintf( buf, obj->short_descr, silver, gold );
        free_string( obj->short_descr );
        obj->short_descr	= str_dup( buf );
        obj->value[0]		= silver;
        obj->value[1]		= gold;
        obj->cost		= 100 * gold + silver;
        obj->weight		= (gold * 2 / 5) + (silver / 10);
    }

    return obj;
}



/*
 * Return # of objects which an object counts as.
 * Thanks to Tony Chamberlain for the correct recursive code here.
 */
int get_obj_number( OBJ_DATA *obj )
{
    int number;

    if (obj->item_type == ITEM_CONTAINER || obj->item_type == ITEM_MONEY
            ||  obj->item_type == ITEM_GEM || obj->item_type == ITEM_JEWELRY)
        number = 0;
    else
        number = 1;

    for ( obj = obj->contains; obj != NULL; obj = obj->next_content )
        number += get_obj_number( obj );

    return number;
}


/*
 * Return weight of an object, including weight of contents.
 */
int get_obj_weight( OBJ_DATA *obj )
{
    int weight;
    OBJ_DATA *tobj;

    weight = obj->weight;
    for ( tobj = obj->contains; tobj != NULL; tobj = tobj->next_content )
        weight += get_obj_weight( tobj ) * WEIGHT_MULT(obj) / 100;

    return weight;
}

int get_true_weight(OBJ_DATA *obj)
{
    int weight;

    weight = obj->weight;
    for ( obj = obj->contains; obj != NULL; obj = obj->next_content )
        weight += get_obj_weight( obj );

    return weight;
}

/*
 * True if room is dark.
 */
bool room_is_dark( ROOM_INDEX_DATA *pRoomIndex )
{
    if ( STR_IS_SET(pRoomIndex->strbit_room_flags, ROOM_PITCHBLACK) )
        return TRUE;

    if ( pRoomIndex->light > 0 )
        return FALSE;

    if ( STR_IS_SET(pRoomIndex->strbit_room_flags, ROOM_DARK) )
        return TRUE;

    if ( STR_IS_SET(pRoomIndex->strbit_room_flags, ROOM_LIGHT) )
        return FALSE;

    if ( pRoomIndex->sector_type == SECT_INSIDE
         ||   pRoomIndex->sector_type == SECT_CITY )
        return FALSE;

    if ( weather_info.sunlight == SUN_SET
         ||   weather_info.sunlight == SUN_DARK )
        return TRUE;

    return FALSE;
}


bool is_room_owned(CHAR_DATA *ch, ROOM_INDEX_DATA *room)
{
    char s[MAX_STRING_LENGTH];
    return GetRoomProperty(room,PROPERTY_STRING,"owner",s);
}

bool is_room_owner(CHAR_DATA *ch, ROOM_INDEX_DATA *room)
{
    char s[MAX_STRING_LENGTH];

    if (!GetRoomProperty(room,PROPERTY_STRING,"owner",s))
        return FALSE;

    if (IS_NPC(ch) && ch->master)
        return is_exact_name(ch->master->name,s);
    else
        return is_exact_name(ch->name,s);
}

/*
 * True if person is allowed in room. This only works if you
 * have checked if the room is private.
 */
bool is_allowed_in_room( CHAR_DATA *ch,ROOM_INDEX_DATA *pRoomIndex ) {
    char s[MAX_STRING_LENGTH];
    bool b;

    if ( STR_IS_SET(pRoomIndex->strbit_room_flags,ROOM_CLANONLY) && ch->clan && ch->clan->clan_info->independent==FALSE)
        return TRUE;

    b=FALSE;
    GetRoomProperty(pRoomIndex,PROPERTY_BOOL,"openhouse",&b);
    if (b)
        return TRUE;

    if (IS_IMMORTAL(ch) && is_exact_name(ch->name,pRoomIndex->area->builders))
        return TRUE;

    /* If the owner has opened the house, allow access */
    if (GetRoomProperty(pRoomIndex,PROPERTY_STRING,"openhouse",s)) {
        if (IS_NPC(ch) && ch->master)
            return (is_exact_name(ch->master->name,s));
        else
            return (is_exact_name(ch->name,s));
    }

    return FALSE;
}

/*
 * True if room is private.
 */
bool room_is_private( ROOM_INDEX_DATA *pRoomIndex )
{
    CHAR_DATA *rch;
    int count;
    char s[MAX_STRING_LENGTH];
    int max;

    /* If the room has an owner, deny access */
    if (GetRoomProperty(pRoomIndex,PROPERTY_STRING,"owner",s))
        return TRUE;

    count = 0;
    for ( rch = pRoomIndex->people; rch != NULL; rch = rch->next_in_room )
        count++;

    if (GetRoomProperty(pRoomIndex,PROPERTY_INT,"maxpeople",&max)&&
            (count>=max))
        return TRUE;

    if ( STR_IS_SET(pRoomIndex->strbit_room_flags, ROOM_CLANONLY))
        return TRUE;

    if ( STR_IS_SET(pRoomIndex->strbit_room_flags, ROOM_PRIVATE) && count >= 2 )
        return TRUE;

    if ( STR_IS_SET(pRoomIndex->strbit_room_flags, ROOM_SOLITARY) && count >= 1)
        return TRUE;

    if ( STR_IS_SET(pRoomIndex->strbit_room_flags, ROOM_IMP_ONLY) )
        return TRUE;

    return FALSE;
}

/* visibility on a room -- for entering and exits */
bool can_see_room( CHAR_DATA *ch, ROOM_INDEX_DATA *pRoomIndex )
{
    int i;

    if (STR_IS_SET(pRoomIndex->strbit_room_flags, ROOM_IMP_ONLY)
            &&  get_trust(ch) < MAX_LEVEL)
        return FALSE;

    if (!IS_IMMORTAL(ch)) {
        if (STR_IS_SET(pRoomIndex->strbit_room_flags, ROOM_GODS_ONLY))
            return FALSE;

        if (STR_IS_SET(pRoomIndex->strbit_room_flags, ROOM_HEROES_ONLY))
            return FALSE;

        if (STR_IS_SET(pRoomIndex->strbit_room_flags,ROOM_NONEWBIE)
                &&  ch->level < mud_data.newbie_room_level)
            return FALSE;

        if (STR_IS_SET(pRoomIndex->strbit_room_flags,ROOM_NEWBIES_ONLY)
                &&  ch->level >= mud_data.newbie_room_level )
            return FALSE;

        /* this is now defined in move_char()
    if (GetRoomProperty(pRoomIndex,PROPERTY_STRING,"clan",s)) {
        CLAN_TYPE *pClan;

        if (ch->clan<=1)
        return FALSE;
        pClan=get_clan(ch->clan);
        if (!is_exact_name(pClan->name,s))
        return FALSE;
    }
    */

        if (GetRoomProperty(pRoomIndex,PROPERTY_INT,"levelmin",&i)
                && ch->level<i)
            return FALSE;

        if (GetRoomProperty(pRoomIndex,PROPERTY_INT,"levelmax",&i)
                && ch->level>i)
            return FALSE;
    }

    return TRUE;
}



/*
 * True if char can see victim.
 */
bool can_see( CHAR_DATA *ch, CHAR_DATA *victim )
{
    /* RT changed so that WIZ_INVIS has levels */
    if ( ch == victim )
        return TRUE;

    if ( !ch || !victim )
        return FALSE;

    if ( get_trust(ch) < victim->invis_level)
        return FALSE;

    if (get_trust(ch) < victim->incog_level && ch->in_room != victim->in_room)
        return FALSE;

    if ( (!IS_NPC(ch) && STR_IS_SET(ch->strbit_act, PLR_HOLYLIGHT))
         ||   (IS_NPC(ch) && IS_IMMORTAL(ch)))
        return TRUE;

    if ( IS_AFFECTED(ch, EFF_BLIND) )
        return FALSE;

    // just to please Galah.
    if ( IS_AFFECTED(victim, EFF_FAERIE_FIRE) )
        return TRUE;

    if ( room_is_dark( ch->in_room ) && !IS_AFFECTED(ch, EFF_INFRARED) )
        return FALSE;

    if ( IS_AFFECTED(victim, EFF_INVISIBLE)
         &&   !IS_AFFECTED(ch, EFF_DETECT_INVIS) )
        return FALSE;

    if ( IS_AFFECTED2(victim, EFF_IMPINVISIBLE) )
        return FALSE;

    /* sneaking */
    if ( IS_AFFECTED(victim, EFF_SNEAK)
         &&   !IS_AFFECTED(ch,EFF_DETECT_HIDDEN)
         &&   victim->fighting == NULL)
    {
        float chance;
        chance = 100;
        chance *= 1+ get_curr_stat(victim,STAT_DEX) *0.015;
        chance /= 1+ get_curr_stat(ch,STAT_INT) * 0.02;
        chance /= 1+ (ch->level - victim->level * 3/2)*0.01;

        if (skillcheck2(victim,gsn_sneak,chance))
            return FALSE;
    }

    if ( IS_AFFECTED(victim, EFF_HIDE)
         &&   !IS_AFFECTED(ch, EFF_DETECT_HIDDEN)
         &&   victim->fighting == NULL)
        return FALSE;

    return TRUE;
}



/*
 * True if char can see obj.
 */
bool can_see_obj( CHAR_DATA *ch, OBJ_DATA *obj )
{
    if ( !IS_NPC(ch) && STR_IS_SET(ch->strbit_act, PLR_HOLYLIGHT) )
        return TRUE;

    if ( STR_IS_SET(obj->strbit_extra_flags,ITEM_VIS_DEATH))
        return FALSE;

    if ( STR_IS_SET(obj->strbit_extra_flags,ITEM_UNUSABLE))
        return FALSE;

    if ( IS_AFFECTED( ch, EFF_BLIND ) && obj->item_type != ITEM_POTION)
        return FALSE;

    if ( obj->item_type == ITEM_LIGHT && obj->value[2] != 0 )
        return TRUE;

    if ( STR_IS_SET(obj->strbit_extra_flags, ITEM_INVIS)
         &&   !IS_AFFECTED(ch, EFF_DETECT_INVIS) )
        return FALSE;

    if ( IS_OBJ_STAT(obj,ITEM_GLOW))
        return TRUE;

    if ( room_is_dark( ch->in_room ) && !IS_AFFECTED(ch, EFF_DARK_VISION) )
        return FALSE;

    return TRUE;
}



/*
 * True if char can drop obj.
 */
bool can_drop_obj( CHAR_DATA *ch, OBJ_DATA *obj )
{
    if ( !STR_IS_SET(obj->strbit_extra_flags, ITEM_NODROP) )
        return TRUE;

    if ( !IS_NPC(ch) && ch->level >= LEVEL_IMMORTAL )
        return TRUE;

    return FALSE;
}


char *weapon_bit_name(long weapon_flags)
{
    static char buf[512];

    buf[0] = '\0';
    if (weapon_flags & WEAPON_FLAMING	) strcat(buf, " flaming");
    if (weapon_flags & WEAPON_FROST	) strcat(buf, " frost");
    if (weapon_flags & WEAPON_VAMPIRIC	) strcat(buf, " vampiric");
    if (weapon_flags & WEAPON_SHARP	) strcat(buf, " sharp");
    if (weapon_flags & WEAPON_VORPAL	) strcat(buf, " vorpal");
    if (weapon_flags & WEAPON_TWO_HANDS ) strcat(buf, " two-handed");
    if (weapon_flags & WEAPON_SHOCKING 	) strcat(buf, " shocking");
    if (weapon_flags & WEAPON_POISON	) strcat(buf, " poison");

    return ( buf[0] != '\0' ) ? buf+1 : "none";
}

bool is_anti_race( OBJ_DATA *obj, int race )
{
    EFFECT_DATA *pef;
    bool raceonly_found=FALSE;

    race=1<<race;

    /* First check if the object has a valid RACEONLY flag */
    for(pef=obj->affected;pef;pef=pef->next) {
        if(pef->where==TO_OBJECT2
                && pef->bitvector==ITEM_RACE_ONLY) {
            if (pef->arg1&race)
                return FALSE;
            else
                raceonly_found=TRUE;
        }
    }

    for(pef=obj->pIndexData->affected;pef;pef=pef->next)
    {
        if(pef->where==TO_OBJECT2
                && pef->bitvector==ITEM_RACE_ONLY) {
            if (pef->arg1&race)
                return FALSE;
            else
                raceonly_found=TRUE;
        }
    }
    if (raceonly_found)
        return TRUE;
    /* ^^^--- if there were RACEONLY flags but the one specified not found
          then it should be considered anti that race */

    /* If not, check if the object has a valid ANTIRACE flag */
    for(pef=obj->affected;pef;pef=pef->next) {
        if(pef->where==TO_OBJECT2
                && pef->bitvector==ITEM_ANTI_RACE
                && (pef->arg1&race))
            return TRUE;
    }

    for(pef=obj->pIndexData->affected;pef;pef=pef->next)
    {
        if(pef->where==TO_OBJECT2
                && pef->bitvector==ITEM_ANTI_RACE
                && (pef->arg1&race))
            return TRUE;
    }

    return FALSE;
}

bool is_anti_class( OBJ_DATA *obj, int class )
{
    EFFECT_DATA *pef;
    bool classonly_found=FALSE;

    class=1<<class;

    for(pef=obj->affected;pef;pef=pef->next) {
        if(pef->where==TO_OBJECT2
                && pef->bitvector==ITEM_CLASS_ONLY) {
            if (pef->arg1&class)
                return FALSE;
            else
                classonly_found=TRUE;
        }
    }
    if (classonly_found)
        return TRUE;

    for(pef=obj->pIndexData->affected;pef;pef=pef->next)
    {
        if(pef->where==TO_OBJECT2
                && pef->bitvector==ITEM_CLASS_ONLY) {
            if (pef->arg1&class)
                return FALSE;
            else
                classonly_found=TRUE;
        }
    }
    if (classonly_found)
        return TRUE;

    for(pef=obj->affected;pef;pef=pef->next)
    {
        if(pef->where==TO_OBJECT2
                && pef->bitvector==ITEM_ANTI_CLASS
                && (pef->arg1&class))
            return TRUE;
    }

    for(pef=obj->pIndexData->affected;pef;pef=pef->next)
    {
        if(pef->where==TO_OBJECT2
                && pef->bitvector==ITEM_ANTI_CLASS
                && (pef->arg1&class))
            return TRUE;
    }

    return FALSE;
}

int is_race_poison( OBJ_DATA *obj, int race )
{
    EFFECT_DATA *pef,*next_pef;
    int modi=0;

    race=1<<race;

    for(pef=obj->affected;pef;pef=next_pef) {
        next_pef=pef->next;

        if(pef->where==TO_OBJECT2
                && pef->bitvector==ITEM_RACE_POISON) {
            if (pef->arg1&race)
                modi=UMAX(modi,pef->modifier);

            if (pef->duration<-1) {
                pef->duration++;
                if (pef->duration>=-1) {
                    effect_remove_obj(obj,pef);
                }
            }
        }
    }
    if (modi>0)
        return modi;

    for(pef=obj->pIndexData->affected;pef;pef=pef->next)
    {
        if(pef->where==TO_OBJECT2
                && pef->bitvector==ITEM_RACE_POISON) {
            if (pef->arg1&race)
                return pef->modifier;
        }
    }

    return 0;
}

time_t get_mem_when( CHAR_DATA *mob, CHAR_DATA *ch, long type)
{
    MEM_DATA *mem;

    if(!IS_NPC(mob) || ch==NULL || IS_NPC(ch)) return 0;

    for(mem=mob->memory;mem;mem=mem->next)
        if(ch->id==mem->id && type==mem->type) return mem->when;

    return 0;
}

int get_mem( CHAR_DATA *mob, CHAR_DATA *ch, long type)
{
    MEM_DATA *mem;

    if(!IS_NPC(mob) || ch==NULL || IS_NPC(ch)) return 0;

    for(mem=mob->memory;mem;mem=mem->next)
        if(ch->id==mem->id && type==mem->type) return mem->times;

    return 0;
}

void set_mem( CHAR_DATA *mob, CHAR_DATA *ch, long type)
{
    MEM_DATA *mem;

    if(!IS_NPC(mob) || ch==NULL || IS_NPC(ch)) return;

    for(mem=mob->memory;mem;mem=mem->next)
        if(ch->id==mem->id && type==mem->type)
        {
            mem->when=time(NULL);
            mem->times++;
            return;
        }

    mem=new_mobmem();
    mem->id=ch->id;
    mem->type=type;
    mem->when=time(NULL);
    mem->times=1;
    mem->next=mob->memory;
    mob->memory=mem;
}

void forget_mem( CHAR_DATA *mob, CHAR_DATA *ch, long type)
{
    MEM_DATA *mem,*prev_mem=NULL;

    if(!IS_NPC(mob) || ch==NULL || IS_NPC(ch)) return;

    for (mem=mob->memory;mem;mem=mem->next) {
        if (ch->id==mem->id && type==mem->type) {
            if (prev_mem)
                prev_mem->next=mem->next;
            else
                mob->memory=mem->next;
            free_mobmem(mem);
        }
        prev_mem=mem;
    }
}


static int chartohex(char c)
{
    if(!isxdigit(c)) return 0;

    if(isdigit(c)) return c-'0';

    return 10+c-'A';
}

bool isstrbitset(char *string,int bit)
{
    int byte=bit/4;
    int bitnum=bit%4;
    int hex;

    if(strlen(string)<=byte) return FALSE;
    hex=chartohex(string[byte]);
    if(hex&(1<<bitnum)) return TRUE;
    else return FALSE;
}


#define STR_NIBBLETOHEX(nibble)   ((nibble)<=9?(nibble)+'0':(nibble)-10+'A')
#define STR_HEXTONIBBLE(hex)      ((hex)<='9'?(hex)-'0':(hex)-'A'+10)

char *stringtohex(char *input,char *output,int length) {
    char *poutput;
    int i;

    poutput=output;
    for (i=0;i<length;i++) {
        *(poutput++)=STR_NIBBLETOHEX(input[i]&0xf);
        *(poutput++)=STR_NIBBLETOHEX((input[i]>>4)&0xf);
    }
    *poutput=0;
    return output;
}

// converts a hex-representation into a bitstring
// length is the length of the hex-representation
char *hextostring(char *input,char *output,int length) {
    char *poutput;
    int i;

    STR_ZERO_BIT(output,length/2);
    poutput=output;
    for (i=0;i<length;i++) {
        if (i%2) {
            *poutput+=(STR_HEXTONIBBLE(input[i]))<<4;
            poutput++;
        } else {
            *poutput=(STR_HEXTONIBBLE(input[i]));
        }
    }
    return output;
}



int exp_to_level(CHAR_DATA *ch)
{
    if (IS_NPC(ch))
        return 0;
    else
        return (ch->level + 1) * exp_per_level(ch,ch->pcdata->points) - ch->exp;
}

char *url(char *url,char *file,AREA_DATA *area,bool show) {
    static char buf  [MAX_STRING_LENGTH];
    if (url==NULL) url=buf;
    if (!str_prefix("http",file)) {
        strcpy(url,file);
        return url;
    }
    if (file[0]=='/') {
        sprintf(url,"%s%s%s%s",show?"{D":"",URLBASE,show?"{x":"",file);
        return url;
    }
    sprintf(url,"%s%s%s%s",show?"{D":"",area->urlprefix,show?"{x":"",file);
    return url;
}


void set_phased_skill(CHAR_DATA *ch,PHASE_FUN *proc,void *target,int step,long flags) {
    ch->phasedskill_proc=proc;
    ch->phasedskill_target=target;
    ch->phasedskill_step=step;
    ch->phasedskill_flags=flags;
}


CHAR_DATA *hallucination_char(void) {
    static CHAR_DATA *hallucinate_ch=NULL;

    if (hallucinate_ch==NULL) {
        hallucinate_ch=char_list;
    } else if (!IS_VALID(hallucinate_ch)) {
        int count,i=number_range(1,char_inuse-1);
        hallucinate_ch=char_list;
        for (count=0;count<i;count++)
            hallucinate_ch=hallucinate_ch->next;
    } else {
        if ((hallucinate_ch=hallucinate_ch->next)==NULL)
            hallucinate_ch=char_list;
    }
    return hallucinate_ch;
}

OBJ_DATA *hallucination_obj(void) {
    static OBJ_DATA *hallucinate_obj=NULL;

    if (hallucinate_obj==NULL) {
        hallucinate_obj=object_list;
    } else if (!IS_VALID(hallucinate_obj)) {
        int count,i=number_range(1,char_inuse-1);
        hallucinate_obj=object_list;
        for (count=0;count<i;count++)
            hallucinate_obj=hallucinate_obj->next;
    } else {
        if ((hallucinate_obj=hallucinate_obj->next)==NULL)
            hallucinate_obj=object_list;
    }
    return hallucinate_obj;
}



int show_sex(CHAR_DATA *ch) {
    if (ch->Sex==0) return SEX_NEUTRAL;
    if (ch->Sex%2) return SEX_MALE;
    return SEX_FEMALE;
}



bool player_exist(char *name) {
    struct stat sb;
    char filename[MSL];

    sprintf(filename,"%s/%s",mud_data.player_dir,Capitalize(name));
    if (stat(filename,&sb)==0)
        return TRUE;
    return FALSE;
}

bool check_ordered(CHAR_DATA *ch) {
    if (IS_NPC(ch))
        return FALSE;

    if (!STR_IS_SET(ch->strbit_act,PLR_ORDERED))
        return FALSE;

    if (ch->master==NULL) {	// this shouldn't happen, but okay
        bugf("Found an ordered player (%s) without a master!\n\r",ch->name);
        return FALSE;
    }

    send_to_char("Tsk tsk tsk...\n\r",ch->master);
    return TRUE;
}



ROOM_INDEX_DATA *find_location( CHAR_DATA *ch, char *arg ) {
    CHAR_DATA *victim;
    OBJ_DATA *obj;
    int room;

    if ( is_number(arg) &&
         (room=atoi(arg))>0)
        return get_room_index(room);

    if ( ( victim = get_char_world( ch, arg ) ) != NULL )
        return victim->in_room;

    if ( ( obj = get_obj_world( ch, arg ) ) != NULL )
        return obj->in_room;

    return NULL;
}
