//
// $Id: magic2.c,v 1.112 2009/04/22 20:23:26 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"
#include "interp.h"
#include "color.h"

void spell_farsight( int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    if (IS_AFFECTED(ch,EFF_BLIND))
    {
        send_to_char("Maybe it would help if you could see?\n\r",ch);
        return;
    }

    do_function(ch,&do_scan,target_name);
}


void spell_portal( int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    CHAR_DATA *victim;
    OBJ_DATA *portal, *stone;

        if ( ( victim = get_char_world( ch, target_name ) ) == NULL
    ||   victim == ch
    ||   victim->in_room == NULL
    ||   !can_see_room(ch,victim->in_room)
    ||   STR_IS_SET(victim->in_room->strbit_room_flags, ROOM_SAFE)
    ||   STR_IS_SET(victim->in_room->strbit_room_flags, ROOM_PRIVATE)
    ||   STR_IS_SET(victim->in_room->strbit_room_flags, ROOM_SOLITARY)
    ||   STR_IS_SET(victim->in_room->strbit_room_flags, ROOM_NO_RECALL)
    ||   STR_IS_SET(ch->in_room->strbit_room_flags, ROOM_NO_RECALL)
    ||   victim->level >= level + 3
    ||   (!IS_NPC(victim) && victim->level >= LEVEL_HERO)  /* NOT trust */
    ||   (IS_NPC(victim) && STR_IS_SET(victim->strbit_imm_flags,IMM_SUMMON))
    ||   (IS_NPC(victim) && saves_spell( level, victim,DAM_NONE) )
    ||	(is_clan(victim) && !is_same_clan(ch,victim)))
    {
        send_to_char( "You failed.\n\r", ch );
        return;
    }

    stone = get_eq_char(ch,WEAR_HOLD);
    if (!IS_IMMORTAL(ch)
    &&  (stone == NULL || stone->item_type != ITEM_WARP_STONE))
    {
	send_to_char("You lack the proper component for this spell.\n\r",ch);
	return;
    }

    if (stone != NULL && stone->item_type == ITEM_WARP_STONE)
    {
     	act("You draw upon the power of $p.",ch,stone,NULL,TO_CHAR);
     	act("It flares brightly and vanishes!",ch,stone,NULL,TO_CHAR);
     	extract_obj(stone);
    }

    portal = create_object(get_obj_index(OBJ_VNUM_PORTAL),0);
    portal->timer = 2 + level / 25;
    portal->value[3] = victim->in_room->vnum;

    obj_to_room(portal,ch->in_room);
    op_load_trigger(portal,ch);

    act("$p rises up from the ground.",ch,portal,NULL,TO_ROOM);
    act("$p rises up before you.",ch,portal,NULL,TO_CHAR);
}

void spell_nexus( int sn, int level, CHAR_DATA *ch, void *vo, int target)
{
    CHAR_DATA *victim;
    OBJ_DATA *portal, *stone;
    ROOM_INDEX_DATA *to_room, *from_room;

    from_room = ch->in_room;

        if ( ( victim = get_char_world( ch, target_name ) ) == NULL
    ||   victim == ch
    ||   (to_room = victim->in_room) == NULL
    ||   !can_see_room(ch,to_room) || !can_see_room(ch,from_room)
    ||   STR_IS_SET(to_room->strbit_room_flags, ROOM_SAFE)
    ||	 STR_IS_SET(from_room->strbit_room_flags,ROOM_SAFE)
    ||   STR_IS_SET(to_room->strbit_room_flags, ROOM_PRIVATE)
    ||   STR_IS_SET(to_room->strbit_room_flags, ROOM_SOLITARY)
    ||   STR_IS_SET(to_room->strbit_room_flags, ROOM_NO_RECALL)
    ||   STR_IS_SET(from_room->strbit_room_flags,ROOM_NO_RECALL)
    ||   victim->level >= level + 3
    ||   (!IS_NPC(victim) && victim->level >= LEVEL_HERO)  /* NOT trust */
    ||   (IS_NPC(victim) && STR_IS_SET(victim->strbit_imm_flags,IMM_SUMMON))
    ||   (IS_NPC(victim) && saves_spell( level, victim,DAM_NONE) )
    ||	 (is_clan(victim) && !is_same_clan(ch,victim)))
    {
        send_to_char( "You failed.\n\r", ch );
        return;
    }

    stone = get_eq_char(ch,WEAR_HOLD);
    if (!IS_IMMORTAL(ch)
    &&  (stone == NULL || stone->item_type != ITEM_WARP_STONE))
    {
        send_to_char("You lack the proper component for this spell.\n\r",ch);
        return;
    }

    if (stone != NULL && stone->item_type == ITEM_WARP_STONE)
    {
        act("You draw upon the power of $p.",ch,stone,NULL,TO_CHAR);
        act("It flares brightly and vanishes!",ch,stone,NULL,TO_CHAR);
        extract_obj(stone);
    }

    /* portal one */
    portal = create_object(get_obj_index(OBJ_VNUM_PORTAL),0);
    portal->timer = 1 + level / 10;
    portal->value[3] = to_room->vnum;

    obj_to_room(portal,from_room);
    op_load_trigger(portal,ch);

    act("$p rises up from the ground.",ch,portal,NULL,TO_ROOM);
    act("$p rises up before you.",ch,portal,NULL,TO_CHAR);

    /* no second portal if rooms are the same */
    if (to_room == from_room)
	return;

    /* portal two */
    portal = create_object(get_obj_index(OBJ_VNUM_PORTAL),0);
    portal->timer = 1 + level/10;
    portal->value[3] = from_room->vnum;

    obj_to_room(portal,to_room);
    op_load_trigger(portal,ch);

    if (to_room->people != NULL)
    {
	act("$p rises up from the ground.",to_room->people,portal,NULL,TO_ROOM);
	act("$p rises up from the ground.",to_room->people,portal,NULL,TO_CHAR);
    }
}

void show_cantrip(CHAR_DATA *ch, CHAR_DATA *victim, int type) {
    char buf[MSL];

    if (!IS_AFFECTED2(victim,EFF_CANTRIP))
	return;

    buf[0]=0;
    GetCharProperty(victim,PROPERTY_STRING,"cantrip",buf);
    if (buf[0]!=0)
	act(buf,victim,NULL,ch,type);
}

void wearoff_cantrip(CHAR_DATA *ch) {
    DeleteCharProperty(ch,PROPERTY_STRING,"cantrip");
}


void spell_cantrip( int sn, int level, CHAR_DATA *ch, void *vo, int target)
{
    extern char *target_name;
    CHAR_DATA *victim;
    EFFECT_DATA ef;
    char *bigparts[]={"nose","ears","hands","feet","breasts","arms","legs",
			"butt",NULL};
    char *smallparts[]={NULL};
    char *colours[]={"green","red","blue","yellow","purple","gray","white",
		     "orange","black",NULL};
    char *colourparts[]={"hair","hands","face","nose","arms","legs",
			"butt",NULL};
    char *noparts[]={"eyebrows",NULL};
    char *lookinglike[]={"magnificent","like a frog",NULL};
    char name[MAX_INPUT_LENGTH];
    char action[MAX_INPUT_LENGTH];
    char part[MAX_INPUT_LENGTH];
    char cantbuf[MAX_INPUT_LENGTH];
    char *arg,**a,**p;
    bool found;

    arg = one_argument( target_name, name );    /* Skip target name */

    if( name[0]=='\0' ) {
	send_to_char( "Cast cantrip on whom?\n\r", ch );
	return;
    }

    if ( ( victim = get_char_room( ch, name ) ) == NULL ) {
	send_to_char( "They aren't here.\n\r", ch );
	return;
    }

    if( is_affected( victim, sn ) ) {
	if(victim==ch)
	    send_to_char("You already are affected by a cantrip.\n\r",ch);
	else
	    act("$N is already affected by a cantrip.",ch,NULL,victim,TO_CHAR);
	return;
    }

    arg = one_argument( arg, action );
    one_argument( arg, part );

    if(action[0]=='\0') {
	send_to_char("You should give an action type for your cantrip.\n\r",ch);
	return;
    }

    if(part[0]=='\0') {
	send_to_char("You should give a part type for your cantrip.\n\r", ch );
	return;
    }

    found=FALSE;

    for (a=colours;*a;a++)
	if (!str_cmp(action,*a))
	    break;
    if (*a) {
	for (p=colourparts;*p;p++)
	    if (!str_cmp(part,*p))
		break;
	if (*p) {
	    sprintf(cantbuf,"$e has a %s %s.",action,part);
	    found=TRUE;
	} else {
	    send_to_char(
		"You can't think of a way to cast it on that part.\n\r",ch);
	    return;
	}
    }

    if (!*a)
	if (!str_cmp(action,"small")) {
	    *a="small";
	    for (p=smallparts;*p;p++)
		if (!str_cmp(part,*p))
		    break;
	    if (!str_cmp(part,"penis"))
		if (show_sex(victim)!=SEX_MALE) {
		    act("After careful inspection your realize $E has no such thing.",ch,NULL,victim,TO_CHAR);
		    check_social(ch,"curse","");
		    return;
		}
	    if (*p) {
		sprintf(cantbuf,"$e has a very small %s.",part);
		found=TRUE;
	    } else {
		send_to_char(
		    "You can't think of a way to resize that part.\n\r",ch);
		return;
	    }
	}

    if (!*a)
	if (!str_cmp(action,"big")) {
	    *a="big";
	    for (p=bigparts;*p;p++)
		if (!str_cmp(part,*p))
		    break;
	    if (*p) {
		sprintf(cantbuf,"$e has a very big %s.",part);
		found=TRUE;
	    } else {
		send_to_char(
		    "You can't think of a way to resize that part.\n\r",ch);
		return;
	    }
	}

    if (!*a)
	if (!str_cmp(action,"no")) {
	    *a="no";
	    for (p=noparts;*p;p++)
		if (!str_cmp(part,*p))
		    break;
	    if (*p) {
		sprintf(cantbuf,"$e has no %s.",part);
		found=TRUE;
	    } else {
		send_to_char(
		    "You can't think of a way to remove that part.\n\r",ch);
		return;
	    }
	}
    if (!*a)
	if (!str_cmp(action,"looking")) {
	    *a="looking";
	    for (p=lookinglike;*p;p++)
		if (!str_cmp(part,*p))
		    break;
	    if (*p) {
		sprintf(cantbuf,"$e is looking %s.",part);
		found=TRUE;
	    } else {
		act("You can't think of a way for $M to look like that.",ch,NULL,victim,TO_CHAR);
		return;
	    }
	}

    if (!found) {
	send_to_char("Hmmmm... that doesn't work.\n\rTry something like:",ch);
	send_to_char(" big <part of the body>\n\r",ch);
	send_to_char(" small <part of the male body>\n\r",ch);
	send_to_char(" <colour> <part of the body>\n\r",ch);
	send_to_char(" no <part of the face>\n\r",ch);
	send_to_char(" looking <magnificent|'like a frog'>\n\r",ch);
	return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where    = TO_EFFECTS2;
    ef.type     = sn;
    ef.level    = level;
    if (victim == ch )
	ef.duration = level/2;
    else
	ef.duration = level/4;
    ef.bitvector = EFF_CANTRIP;
    ef.casted_by = ch->id;
    effect_to_char( victim, &ef );
    SetCharProperty(victim,PROPERTY_STRING,"cantrip",cantbuf);

    send_to_char("You feel something changing.\n\r", victim );
    act("$n has changed.",victim,NULL,NULL,TO_ROOM);
    show_cantrip( NULL, victim, TO_ROOM );
    if( ch != victim )
	send_to_char( "Ok.\n\r", ch );
}


void spell_simplify( int sn, int level, CHAR_DATA *ch, void *vo, int target)
{
    OBJ_DATA *obj = (OBJ_DATA *) vo;
    EFFECT_DATA ef;
    int i;

    if (IS_OBJ_STAT2(obj,ITEM_LEVEL))
    {
	act("$p can't be simplyfied.",ch,obj,NULL,TO_CHAR);
	return;
    }

    /* Make some kind of saving throw depending on casters level
       and the objects level. */

    if( number_percent()< (obj->level-ch->level)*10+10 )
    {
	/* Failing is no good... The item will explode!!! */
	act("$p shivers violently and implodes!",ch,obj,NULL,TO_CHAR);
	act("$p shivers violently and implodes!",ch,obj,NULL,TO_ROOM);
	extract_obj(obj);
	return;
    }

    /* Now determine the new level of the object */
    /* If you are a lower level then the object : */
    i=number_range(-2,5);
    if(i<=0)
    {
	send_to_char("You failed.\n\r",ch);
	return;
    }
    if(i>6) i=6;

    i=obj->level-i;
    if(i<0) i=0;

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where		= TO_OBJECT2;
    ef.type		= sn;
    ef.level		= level;
    ef.duration		= level / 2;
    ef.location		= 0;
    ef.modifier		= 0;
    ef.bitvector	= ITEM_LEVEL;
    ef.arg1		= i;
    ef.casted_by	= ch?ch->id:0;
    effect_to_obj(obj,&ef);

    act("$p buzzes for a short moment.",ch,obj,NULL,TO_ALL);
}

void spell_imprint( int sn, int level, CHAR_DATA *ch, void *vo, int target )
{
    OBJ_DATA *	obj = (OBJ_DATA *) vo;
    int		sp_slot, i, mana;
    char	buf[ MAX_STRING_LENGTH ];
    char	*item_what;

    item_what=item_type(obj);

    if (skill_spell_fun(sn,ch) == spell_null ) {
	send_to_char("That is not a spell.\n\r",ch);
	return;
    }

    if(!skill_implemented(sn,ch)) {
        send_to_char("That spell is not implemented yet.\n\r",ch);
        return;
    }

    /* counting the number of spells contained within */

    for (sp_slot = i = 1; i < 4; i++)
	if (obj->value[i] != -1)
	    sp_slot++;

    if (sp_slot > 3) {
	act ("$p cannot contain any more spells.", ch, obj, NULL, TO_CHAR);
	return;
    }

   /* scribe/brew costs 4 times the normal mana required to cast the spell */

    if (ch->level + 2 == skill_level(sn,ch))
	mana = 50;
    else
    	mana = UMAX(
	    skill_min_mana(sn,ch),
	    100 / ( 2 + ch->level -
		    skill_level(sn,ch)) );

    mana*=4;

    if ( !IS_NPC(ch) && ch->mana < mana ) {
	send_to_char( "You don't have enough mana.\n\r", ch );
	return;
    }

    if ( !skillcheck(ch,sn) ) {
	send_to_char( "You lost your concentration.\n\r", ch );
	check_improve(ch,sn,FALSE,1);
	ch->mana -= mana / 2;
	return;
    }

    /* executing the imprinting process */
    ch->mana -= mana;
    obj->value[sp_slot] = sn;

    /* Making it successively harder to pack more spells into potions or
       scrolls - JH */

    switch( sp_slot ) {

    default:
	bugf( "sp_slot has more than %d spells for %s.", sp_slot,NAME(ch) );
	return;

    case 1:
        if ( number_percent() > 80 ) {
	    sprintf_to_char(ch, "The magic enchantment has failed --- "
		"the %s vanishes.\n\r",item_what);
	    extract_obj( obj );
	    return;
	}
	break;
    case 2:
        if ( number_percent() > 50 ) {
	    sprintf_to_char(ch, "The magic enchantment has failed --- "
		"the %s vanishes.\n\r",item_what);
	    extract_obj( obj );
	    return;
	}
	break;

    case 3:
        if ( number_percent() > 25 ) {
	    sprintf_to_char(ch, "The magic enchantment has failed --- "
		"the %s vanishes.\n\r",item_what);
	    extract_obj( obj );
	    return;
	}
	break;
    }

    check_improve(ch,sn,TRUE,1);

    /* labeling the item */

    sprintf ( buf, "a %s of ", item_what);
    for (i = 1; i <= sp_slot ; i++)
	if (obj->value[i] != -1) {
	    strcat (buf, skill_name(obj->value[i],ch));
	    (i != sp_slot ) ? strcat (buf, ", ") : strcat (buf, "") ;
    }
    free_string (obj->short_descr);
    obj->short_descr = str_dup(buf);

    strcpy(buf,item_what);
    for (i = 1; i <= sp_slot ; i++)
	if (obj->value[i] != -1) {
	    strcat(buf," '");
	    strcat(buf, skill_name(obj->value[i],ch));
	    strcat(buf,"'");
    }
    free_string( obj->name );
    obj->name = str_dup( buf );

    sprintf_to_char(ch, "You have imbued a new spell to the %s.\n\r",
	item_what);

    return;
}


void spell_abort_spell(int sn,int level,CHAR_DATA *ch,void *vo,int target ) {
    extern char *target_name; /* defined in magic.c */
    int	tsn,osn;
    bool found=FALSE;
    EFFECT_DATA *pef;
    char buf[MAX_STRING_LENGTH];

    tsn = find_spell(ch,target_name);
    if (tsn<0) {
	sprintf_to_char(ch,
	    "You don't know a spell by that name.\n\r",target_name);
	return;
    }

    if ( ch->affected != NULL )
    {
        for ( pef = ch->affected; pef != NULL; pef = pef->next )
        {
	    osn=find_spell(ch,skill_name(pef->type,ch));
	    if (osn==tsn) {
		found=TRUE;
		if (pef->casted_by!=ch->id)
		    send_to_char("You are not in control of that spell!\n\r",ch);
		else if (!has_skill_available(ch,osn)) {
		    send_to_char(
			"You have no clue how that spell works.\n\r",ch);
		} else {

		    if (check_dispel(pef->level,ch,osn)) {
			sprintf(buf,"You managed to abort your %s!\n\r",
				skill_name(pef->type,ch));
			send_to_char(buf,ch);
		    } else {
			sprintf(buf,"You failed to dispel your %s!\n\r",
				skill_name(pef->type,ch));
			send_to_char(buf,ch);
		    }
		}
		break;
	    }
        }
    }

    if (!found)
	sprintf_to_char(ch,
	    "You're not affected with that spell (%s).\n\r",target_name);
}


/* returns TRUE if object saves */
int save_object_vs_spell(OBJ_DATA *obj,CHAR_DATA *ch) {
    EFFECT_DATA *pef;
    int fail=25,result;

    for (pef=obj->affected;pef;pef=pef->next)
	if (pef->duration!=0)
	    fail+=25;
    fail-=3*(ch->level-obj->level)/2;
    if (IS_OBJ_STAT(obj,ITEM_BLESS))
	fail-=15;
    if (IS_OBJ_STAT(obj,ITEM_GLOW))
	fail-=5;

    result=number_percent();
    if (result<(fail/5)) {
	act("$p shivers violently and explodes!",ch,obj,NULL,TO_CHAR);
	act("$p shivers violently and explodes!",ch,obj,NULL,TO_ROOM);
	extract_obj(obj);
	return FALSE;
    }
    if (result<(fail/2)) {
	EFFECT_DATA *pef_next;
	act("$p glows brightly, then fades...oops.",ch,obj,NULL,TO_CHAR);
	act("$p glows brightly, then fades.",ch,obj,NULL,TO_ROOM);
	obj->enchanted = TRUE;
	/* remove all effects */
	for (pef = obj->affected; pef != NULL; pef = pef_next) {
	    pef_next = pef->next;
	    free_effect(pef);
	}
	obj->affected = NULL;
	/* clear all flags */
	STR_ZERO_BIT(obj->strbit_extra_flags,MAX_FLAGS);
	return FALSE;
    }

    /* failed, no bad result */
    if ( result <= fail ) {
	send_to_char("Nothing seemed to happen.\n\r",ch);
	return FALSE;
    }

    return TRUE;
}


void spell_flame_blade(int sn,int level,CHAR_DATA *ch,void *vo,int target ) {
    OBJ_DATA *obj = (OBJ_DATA *) vo;
    EFFECT_DATA ef;

    if (obj->item_type != ITEM_WEAPON) {
	send_to_char("That isn't a weapon.\n\r",ch);
	return;
    }

    if (obj->wear_loc != WEAR_NONE) {
	send_to_char("The item must be carried to be enchanted.\n\r",ch);
	return;
    }

    if (IS_WEAPON_STAT(obj,WEAPON_FLAMING)) {
	act("$p is already flaming.",ch,obj,NULL,TO_CHAR);
	return;
    }

    /* It's flame *blade*, not flame *mace* */
    switch (obj->value[0]) {
        case WEAPON_EXOTIC:
            send_to_char("You have no idea how to put fire in this extraordinary weapon.\n\r",ch);
            return;

        case WEAPON_SWORD:
        case WEAPON_DAGGER:
        case WEAPON_SPEAR:
        case WEAPON_AXE:
            break;

        case WEAPON_POLEARM:
        case WEAPON_MACE:
        case WEAPON_FLAIL:
        case WEAPON_WHIP:
        default:
            send_to_char("It's not a blade!\n\r",ch);
            return;
    }

    if (IS_OBJ_STAT(obj,ITEM_BURN_PROOF)) {
	act("You can't ignite $p.",ch,obj,NULL,TO_CHAR);
	return;
    }

    if (IS_WEAPON_STAT(obj,WEAPON_FROST)) {
	act("When trying to ignite $p, the frost on it starts to melt but can't keep it up with the transfer of heat!\n\r$p explodes and shatters!",ch,obj,NULL,TO_CHAR);
	act("$p explodes when tries to ignite it!",ch,obj,NULL,TO_ROOM);
	extract_obj(obj);
	return;
    }

    if (!save_object_vs_spell(obj,ch))
	return;

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_WEAPON;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = 5*number_fuzzy(level / 2);
    ef.location  = 0;
    ef.modifier  = 0;
    ef.bitvector = WEAPON_FLAMING;
    ef.casted_by = ch->id;
    effect_to_obj(obj,&ef);

    act("$n is thrilled when flames erupt from $p",ch,obj,NULL,TO_ROOM);
    act("Wow! The flame of $p nearly burned your hair!",ch,obj,NULL,TO_CHAR);
}


void spell_shock_blade(int sn,int level,CHAR_DATA *ch,void *vo,int target ) {
    OBJ_DATA *obj = (OBJ_DATA *) vo;
    EFFECT_DATA ef;

    if (obj->item_type != ITEM_WEAPON) {
	send_to_char("That isn't a weapon.\n\r",ch);
	return;
    }

    if (obj->wear_loc != WEAR_NONE) {
	send_to_char("The item must be carried to be enchanted.\n\r",ch);
	return;
    }

    if (IS_WEAPON_STAT(obj,WEAPON_SHOCKING)) {
	act("$p is already shocking.",ch,obj,NULL,TO_CHAR);
	return;
    }

    /* It's shock *blade*, not shock *mace* */
    switch (obj->value[0]) {
        case WEAPON_EXOTIC:
            send_to_char("You have no idea how to load this weapon.\n\r",ch);
            return;

        case WEAPON_SWORD:
        case WEAPON_DAGGER:
        case WEAPON_SPEAR:
        case WEAPON_AXE:
            break;

        case WEAPON_POLEARM:
        case WEAPON_MACE:
        case WEAPON_FLAIL:
        case WEAPON_WHIP:
        default:
            send_to_char("It's not a blade!\n\r",ch);
            return;
    }

    if (!save_object_vs_spell(obj,ch))
	return;

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_WEAPON;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = 5*number_fuzzy(level / 2);
    ef.location  = 0;
    ef.modifier  = 0;
    ef.bitvector = WEAPON_SHOCKING;
    ef.casted_by = ch->id;
    effect_to_obj(obj,&ef);

    act("$n is thrilled when sparks appear on $p.",ch,obj,NULL,TO_ROOM);
    act("*Ouch* Yes, it's shocking now!",ch,NULL,NULL,TO_CHAR);
}


void spell_ice_blade(int sn,int level,CHAR_DATA *ch,void *vo,int target ) {
    OBJ_DATA *obj = (OBJ_DATA *) vo;
    EFFECT_DATA ef;

    if (obj->item_type != ITEM_WEAPON) {
	send_to_char("That isn't a weapon.\n\r",ch);
	return;
    }

    if (obj->wear_loc != WEAR_NONE) {
	send_to_char("The item must be carried to be enchanted.\n\r",ch);
	return;
    }

    if (IS_WEAPON_STAT(obj,WEAPON_FROST)) {
	act("$p is already as cold as ice.",ch,obj,NULL,TO_CHAR);
	return;
    }

    /* It's shock *blade*, not shock *mace* */
    switch (obj->value[0]) {
        case WEAPON_EXOTIC:
            send_to_char("You have no idea how to freeze this weapon.\n\r",ch);
            return;

        case WEAPON_SWORD:
        case WEAPON_DAGGER:
        case WEAPON_SPEAR:
        case WEAPON_AXE:
            break;

        case WEAPON_POLEARM:
        case WEAPON_MACE:
        case WEAPON_FLAIL:
        case WEAPON_WHIP:
        default:
            send_to_char("It's not a blade!\n\r",ch);
            return;
    }

    if (IS_OBJ_STAT(obj,ITEM_FREEZE_PROOF)) {
	act("You can't freeze $p.",ch,obj,NULL,TO_CHAR);
	return;
    }

    if (IS_WEAPON_STAT(obj,WEAPON_FLAMING)) {
	act("When trying to freeze $p, the heat on it starts to disappear but can't keep it up with the transfer of heat!\n\r$p explodes and shatters!",ch,obj,NULL,TO_CHAR);
	act("$p explodes when tries to ignite it!",ch,obj,NULL,TO_ROOM);
	extract_obj(obj);
	return;
    }

    if (!save_object_vs_spell(obj,ch))
	return;

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_WEAPON;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = 5*number_fuzzy(level / 2);
    ef.location  = 0;
    ef.modifier  = 0;
    ef.bitvector = WEAPON_FROST;
    ef.casted_by = ch->id;
    effect_to_obj(obj,&ef);

    act("$ns face gets covered with hoar-frost when $e enchants $p.",
	ch,obj,NULL,TO_ROOM);
    act("The air you exhale is turning into little clouds when its near $p.",
	ch,obj,NULL,TO_CHAR);
}


void spell_vamperic_blade(int sn,int level,CHAR_DATA *ch,void *vo,int target ) {
    OBJ_DATA *obj = (OBJ_DATA *) vo;
    EFFECT_DATA ef;

    if (obj->item_type != ITEM_WEAPON) {
	send_to_char("That isn't a weapon.\n\r",ch);
	return;
    }

    if (obj->wear_loc != WEAR_NONE) {
	send_to_char("The item must be carried to be enchanted.\n\r",ch);
	return;
    }

    if (IS_WEAPON_STAT(obj,WEAPON_VAMPIRIC)) {
	act("$p is already vampiric.",ch,obj,NULL,TO_CHAR);
	return;
    }

    /* It's shock *blade*, not shock *mace* */
    switch (obj->value[0]) {
        case WEAPON_EXOTIC:
            send_to_char(
		"You have no idea how to let this weapon suck blood.\n\r",ch);
            return;

        case WEAPON_SWORD:
        case WEAPON_DAGGER:
        case WEAPON_SPEAR:
        case WEAPON_AXE:
            break;

        case WEAPON_POLEARM:
        case WEAPON_MACE:
        case WEAPON_FLAIL:
        case WEAPON_WHIP:
        default:
            send_to_char("It's not a blade!\n\r",ch);
            return;
    }

    if (!save_object_vs_spell(obj,ch))
	return;

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_WEAPON;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = 5*number_fuzzy(level / 2);
    ef.location  = 0;
    ef.modifier  = 0;
    ef.bitvector = WEAPON_VAMPIRIC;
    ef.casted_by = ch->id;
    effect_to_obj(obj,&ef);

    act("You take a little step back when you realise $n has made $p vamperic!",
	ch,obj,NULL,TO_ROOM);
    act("$p is now looking for blood!!",ch,obj,NULL,TO_CHAR);
}


void spell_vorpal_coating(int sn,int level,CHAR_DATA *ch,void *vo,int target ) {
    OBJ_DATA *obj = (OBJ_DATA *) vo;
    EFFECT_DATA ef;

    if (obj->item_type != ITEM_WEAPON) {
	send_to_char("That isn't a weapon.\n\r",ch);
	return;
    }

    if (obj->wear_loc != WEAR_NONE) {
	send_to_char("The item must be carried to be enchanted.\n\r",ch);
	return;
    }

    if (IS_WEAPON_STAT(obj,WEAPON_VORPAL)) {
	act("$p is already vorpal.",ch,obj,NULL,TO_CHAR);
	return;
    }

    /* It's shock *blade*, not shock *mace* */
    switch (obj->value[0]) {
        case WEAPON_EXOTIC:
            send_to_char(
		"You have no idea how to make this weapon vorpal.\n\r",ch);
            return;

        case WEAPON_SWORD:
        case WEAPON_DAGGER:
        case WEAPON_SPEAR:
        case WEAPON_AXE:
            break;

        case WEAPON_POLEARM:
        case WEAPON_MACE:
        case WEAPON_FLAIL:
        case WEAPON_WHIP:
        default:
            send_to_char("It would make the weapon useless if you try to make it vorpal.\n\r",ch);
            return;
    }

    if (!save_object_vs_spell(obj,ch))
	return;

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_WEAPON;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = 5*number_fuzzy(level / 2);
    ef.location  = 0;
    ef.modifier  = 0;
    ef.bitvector = WEAPON_VORPAL;
    ef.casted_by = ch->id;
    effect_to_obj(obj,&ef);

    act("$n has made $p vorpal.",ch,obj,NULL,TO_ROOM);
    act("You've made $p vorpal.",ch,obj,NULL,TO_CHAR);
}



void spell_purify_food(int sn,int level,CHAR_DATA *ch,void *vo,int target ) {
    OBJ_DATA *obj = (OBJ_DATA *) vo;

    if (obj->item_type!=ITEM_FOOD&&obj->item_type!=ITEM_CORPSE_NPC&&
	obj->item_type!=ITEM_CORPSE_PC) {
	send_to_char("You can't eat that!\n\r",ch);
	return;
    }

    /* hoe kun je ooit ergens de timer van resetten als het originele
     * object (i.e. in de object_index) geen timer heeft...
     * En waar wordt een object eventueel poisoned gemaakt?????
     * Nouja, eerst maar eens de poison flag eraf halen.
     */

    obj->value[3]=0;
    if (obj->timer!=0)
	obj->timer=number_range(obj->timer,10*obj->timer);

    act("$n has purified $p. But how about the taste?",ch,obj,NULL,TO_ROOM);
    act("You've purified $p.",ch,obj,NULL,TO_CHAR);
}

void spell_purify_drinks(int sn,int level,CHAR_DATA *ch,void *vo,int target ) {
    OBJ_DATA *obj = (OBJ_DATA *) vo;

    if (obj->item_type!=ITEM_FOUNTAIN&&obj->item_type!=ITEM_DRINK_CON) {
	send_to_char("You can't drink that!\n\r",ch);
	return;
    }

    obj->value[3]=0;
    if (obj->timer!=0)
	obj->timer=number_range(obj->timer,10*obj->timer);

    act("$n has purified $p. But how about the taste?",ch,obj,NULL,TO_ROOM);
    act("You've purified $p.",ch,obj,NULL,TO_CHAR);
}

void spell_defile_food(int sn,int level,CHAR_DATA *ch,void *vo,int target ) {
    OBJ_DATA *obj = (OBJ_DATA *) vo;

    if (obj->item_type!=ITEM_FOOD&&obj->item_type!=ITEM_CORPSE_NPC&&
	obj->item_type==ITEM_CORPSE_PC) {
	send_to_char("You can't eat that!\n\r",ch);
	return;
    }

    obj->value[3]=1;
    obj->timer=number_range(5,10);

    act("$n has defiled $p. Better leave it here.",ch,obj,NULL,TO_ROOM);
    act("You've defiled $p.",ch,obj,NULL,TO_CHAR);
}

void spell_defile_drinks(int sn,int level,CHAR_DATA *ch,void *vo,int target ) {
    OBJ_DATA *obj = (OBJ_DATA *) vo;

    if (obj->item_type!=ITEM_FOUNTAIN&&obj->item_type!=ITEM_DRINK_CON) {
	send_to_char("You can't drink that!\n\r",ch);
	return;
    }

    obj->value[3]=1;
    obj->timer=number_range(5,10);

    act("$n has defiled $p. Better don't think about it...",ch,obj,NULL,TO_ROOM);
    act("You've defiled $p.",ch,obj,NULL,TO_CHAR);
}



void spell_create_tshirt(int sn,int level,CHAR_DATA *ch,void *vo,int target ) {
    OBJ_DATA *obj;
    extern char *target_name;
    char text[MAX_STRING_LENGTH];
    char an[MAX_STRING_LENGTH];
    char c;

    if (target_name==NULL || target_name[0]==0) {
	send_to_char("What text do you want?\n\r",ch);
	return;
    }

    strcpy(an,"a");
    c=(target_name[0]);
    if (c=='{')
	c=(target_name[2]);
    c=toupper(c);
    if (c=='E' || c=='I' || c=='O' || c=='U' || c=='A')
	strcat(an,"n");

    obj=create_object(get_obj_index(OBJ_VNUM_TSHIRT),0);

    sprintf(text,"%s '%s{x' t-shirt",an,target_name);
    free_string(obj->short_descr);
    obj->short_descr=str_dup(text);

    sprintf(text,"%s '%s{x' t-shirt.",an,target_name);
    free_string(obj->description);
    obj->description=str_dup(text);

    obj_to_char(obj,ch);
    op_load_trigger(obj,ch);

    act("$n has created a t-shirt!",ch,NULL,NULL,TO_ROOM);
    act("You have created a t-shirt!",ch,NULL,NULL,TO_CHAR);
}



void event_summon_corpse(EVENT_DATA *event) {
    CHAR_DATA *	ch=event->ch;
    char	target_name[MSL];
    char	buf[MSL];
    OBJ_DATA *	obj;
    OBJ_DATA *	corpse;

    strcpy(target_name,(char *)event->data);
    if (target_name[0]!=0) {
	corpse=create_object(get_obj_index(OBJ_VNUM_CORPSE_PC),0);
	sprintf(buf,corpse->short_descr,target_name);
	extract_obj(corpse);

	for ( obj = object_list; obj != NULL; obj = obj->next ) {
	    if ( !can_see_obj( ch, obj ) || str_cmp(buf,obj->short_descr))
		continue;
	    if (obj->carried_by) {
		act("$p fades away and disappears.",
		    obj->carried_by,obj,NULL,TO_CHAR);
		obj_from_char(obj);
	    } else if (obj->in_room) {
		if (obj->in_room->people) {
		    act("$p fades away and disappears.",
			obj->in_room->people,obj,NULL,TO_ROOM);
		    act("$p fades away and disappears.",
			obj->in_room->people,obj,NULL,TO_CHAR);
		}
		obj_from_room(obj);
	    } else if (obj->in_obj) {
		if (obj->in_obj->carried_by)
		    act("$p seems lighter.",
			obj->in_obj->carried_by,obj->in_obj,NULL,TO_CHAR);
		obj_from_obj(obj);
	    }
	    obj_to_room(obj,ch->in_room);

	    act("$n has summoned $p.",ch,obj,NULL,TO_ROOM);
	    act("You have summoned $p.",ch,obj,NULL,TO_CHAR);
	    return;
	}
    }

    obj=create_object(get_obj_index(OBJ_VNUM_CORPSE_NPC),0);

    sprintf(buf,obj->short_descr,"a puppy");
    free_string(obj->short_descr);
    obj->short_descr=str_dup(buf);

    sprintf(buf,obj->description,"a puppy");
    free_string(obj->description);
    obj->description=str_dup(buf);
    obj->timer=30;
    obj_to_room(obj,ch->in_room);
    op_load_trigger(obj,NULL);

    act("$n has summoned the corpse of... of... A PUPPY!",ch,NULL,NULL,TO_ROOM);
    act("You have summoned the corpse of... a puppy?!?!?",ch,NULL,NULL,TO_CHAR);
    check_social(ch,"blush","");
}
void spell_summon_corpse(int sn,int level,CHAR_DATA *ch,void *vo,int target ) {
    extern char *target_name;
    char *	ps;

    if (target_name==NULL || target_name[0]==0) {
	send_to_char("Whose corpse do you want to summon?\n\r",ch);
	return;
    }

    sprintf_to_char(ch,
	"You invoke your powers to summon the corpse of %s.\n\r",target_name);
    ps=(char *)calloc(1,strlen(target_name)+1);
    strcpy(ps,target_name);
    create_event(2,ch,NULL,NULL,event_summon_corpse,ps);
}


void spell_restore_strength(int sn,int level,CHAR_DATA *ch,void *vo,int target ) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;

    if ( is_affected( victim, gsn_chill_touch ) ) {
	if (check_dispel(level,victim,gsn_chill_touch)) {
	    act("$n stops shivering.",victim,NULL,NULL,TO_ROOM);
	} else
	    send_to_char("Spell failed.\n\r",ch);
	return;
    }

    if ( is_affected( victim, gsn_weaken ) ) {
	if (check_dispel(level,victim,gsn_weaken)) {
	    act("$n looks stronger.",victim,NULL,NULL,TO_ROOM);
	} else
	    send_to_char("Spell failed.\n\r",ch);
	return;
    }

    if (ch==victim)
	send_to_char("You are at full strength.\n\r",ch);
    else
	act("$N is at full strength.",ch,NULL,victim,TO_CHAR);
}



void spell_restore_armor(int sn,int level,CHAR_DATA *ch,void *vo,int target ) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;

    if ( !is_affected( victim, gsn_faerie_fire ) ) {
	if (ch==victim)
	    send_to_char("You have no aura around you.\n\r",ch);
	else
	    act("$N has no aura around $m.",ch,NULL,victim,TO_CHAR);
	return;
    }

    if (check_dispel(level,victim,gsn_faerie_fire)) {
	act("$n's outline fades.",victim,NULL,NULL,TO_ROOM);
    } else
	send_to_char("Spell failed.\n\r",ch);
}


void event_summon_pet(EVENT_DATA *event) {
    CHAR_DATA *	ch=event->ch;
    CHAR_DATA *	pet=event->victim;

    if (!IS_VALID(ch)) return;
    if (!IS_VALID(pet)) {
	send_to_char("You feel a disturbance in your powers.\n\r",ch);
	return;
    }

    if (pet->in_room==NULL
    ||  STR_IS_SET(pet->in_room->strbit_room_flags, ROOM_NO_RECALL)
    ||  pet->fighting != NULL
    ||  STR_IS_SET(pet->strbit_imm_flags,IMM_SUMMON)) {
	send_to_char("You failed.\n\r",ch);
	act("$n fades in again.",pet,NULL,NULL,TO_ROOM);
	return;
    }

    act("$n fades out and is gone.",pet,NULL,NULL,TO_ROOM);
    char_from_room(pet);
    char_to_room(pet,ch->in_room);
    act("$n arrives suddenly.",pet,NULL,NULL,TO_ROOM);
}
void spell_summon_pet(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    CHAR_DATA *pet,*owner;

    if (target_name[0]==0)
	owner=ch;
    else if ((owner=get_char_world(ch,target_name))==NULL) {
	send_to_char("Whose pet do you want to summon?\n\r",ch);
	return;
    }

    if (ch->in_room!=owner->in_room
	|| !can_see(ch,owner)) {
	act("$N is not here.",ch,NULL,owner,TO_CHAR);
	return;
    }

    if ((pet=owner->pet)==NULL) {
	act("$N doesn't have a pet.",ch,NULL,owner,TO_CHAR);
	return;
    }

    if (pet->in_room==ch->in_room) {
	char buf[MSL];

	sprintf(buf,"emote waves to %s.",ch->name);
	interpret(pet,buf);
	return;
    }

    act("$n starts to fade out.",pet,NULL,NULL,TO_ROOM);
    act("You invoke your powers to summon the pet of $N.",
	ch,NULL,owner,TO_CHAR);
    create_event(2,ch,pet,NULL,event_summon_pet,NULL);
}

void event_animate_corpse2(EVENT_DATA *event) {
    CHAR_DATA *	zombie=event->ch;
    CHAR_DATA *	ch=event->victim;
    OBJ_DATA *	content;
    OBJ_DATA *	content_next;

    if (!IS_VALID(zombie)) return;

    for (content=zombie->carrying;content!=NULL;content=content_next) {
	content_next=content->next_content;
	wear_obj(zombie,content,FALSE,TRUE);
    }

    if (!IS_VALID(ch)) return;
    if (ch->in_room!=zombie->in_room) return;

    add_follower(zombie,ch);
}
void event_animate_corpse1(EVENT_DATA *event) {
    CHAR_DATA *	zombie;
    CHAR_DATA *	ch=event->ch;
    OBJ_DATA *	corpse=event->object;
    bool	b;
    OBJ_DATA *	content;
    OBJ_DATA *	content_next;
    char	s[MSL];

    if (!IS_VALID(ch)) return;
    if (!IS_VALID(corpse)) return;

    zombie=create_mobile(get_mob_index(MOB_VNUM_ZOMBIE));

    free_string(zombie->short_descr);
    zombie->short_descr=str_dup(corpse->short_descr);

    sprintf(s,"%s is standing here.\n\r",zombie->short_descr);
    free_string(zombie->long_descr);
    zombie->long_descr=str_dup(s);

    b=TRUE;
    SetCharProperty(zombie,PROPERTY_BOOL,"nocorpse",&b);
    STR_SET_BIT(zombie->strbit_affected_by,EFF_CHARM);

    char_to_room(zombie,ch->in_room);
    act("$n animates a corpse!",ch,NULL,NULL,TO_ROOM);
    send_to_char("You have animated a corpse!\n\r",ch);

    mp_load_trigger(zombie,ch);

    for (content=corpse->contains;content!=NULL;content=content_next) {
	content_next=content->next_content;
	obj_from_obj(content);
	obj_to_char(content,zombie);
    }
    obj_from_char(corpse);
    extract_obj(corpse);

    create_event(2,zombie,ch,NULL,event_animate_corpse2,NULL);
}
void spell_animate_corpse(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    OBJ_DATA *	corpse=vo;

    if (IS_AFFECTED(ch,EFF_CHARM)) {
	send_to_char("You are too giddy to have any followers!\r\n",ch);
	return;
    }

    if (corpse->item_type==ITEM_CORPSE_PC && get_trust(ch)<=LEVEL_HERO) {
	send_to_char("You cannot animate that!\r\n",ch);
	return;
    }
    if (corpse->item_type!=ITEM_CORPSE_NPC &&
	corpse->item_type!=ITEM_CORPSE_PC) {
	send_to_char("You cannot animate that!\r\n",ch);
	return;
    }

    send_to_char("You invoke your powers to animate the corpse.\n\r",ch);
    act("$n hold $s hands above $P.",ch,NULL,corpse,TO_ROOM);

    create_event(2,ch,NULL,corpse,event_animate_corpse1,NULL);
}



void spell_chaos_shield(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    EFFECT_DATA ef;

    if (is_affected(ch,sn)) {
	send_to_char("You are already protected by a chaos-shield!\n\r",ch);
	return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where	= TO_RESIST;
    ef.type	= sn;
    ef.level	= level;
    ef.duration	= ch->level/3;
    ef.modifier	= 1;
    ef.location	= APPLY_NONE;
    ef.bitvector= EFF_CHAOS_SHIELD;
    ef.casted_by= ch?ch->id:0;
    effect_to_char(ch,&ef);

    act("$n is surrounded by a chaos shield.",ch,NULL,NULL,TO_ROOM);
    send_to_char("You are surrounded by a chaos shield.\n\r",ch);
}

void spell_wild_shield(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    EFFECT_DATA ef;

    if (is_affected(ch,sn)) {
	send_to_char("You are already protected by a wild-shield!\n\r",ch);
	return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where	= TO_RESIST;
    ef.type	= sn;
    ef.level	= level;
    ef.duration	= ch->level/3;
    ef.modifier	= 1;
    ef.location	= APPLY_NONE;
    ef.bitvector= EFF_WILD_SHIELD;
    ef.casted_by= ch?ch->id:0;
    effect_to_char(ch,&ef);

    act("$n is surrounded by a wild shield.",ch,NULL,NULL,TO_ROOM);
    send_to_char("You are surrounded by a wild shield.\n\r",ch);
}

void spell_water_walk(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    EFFECT_DATA ef;
    CHAR_DATA *victim=vo;

    if (is_affected(victim,sn)) {
	if (victim==ch)
	    send_to_char("Your toes are already webbed.\n\r",ch);
	else
	    act("$e is already able to walk on water.",
		victim,NULL,ch,TO_VICT);
	return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where	= TO_EFFECTS;
    ef.type	= sn;
    ef.level	= level;
    ef.duration	= ch->level/3;
    ef.modifier	= 1;
    ef.location	= APPLY_NONE;
    ef.bitvector= EFF_WATERWALK;
    ef.casted_by= ch?ch->id:0;
    effect_to_char(victim,&ef);

    send_to_char("You feel webbing between your toes.\n\r",victim);
    if (victim!=ch)
	act("$e can now walk on water.",victim,NULL,ch,TO_VICT);
}

void spell_exorcise(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    CHAR_DATA *victim=vo;

    if (ch==victim) {
	send_to_char("You are not sure if that is safe.\n\r",ch);
	return;
    }

    if (!IS_NPC(victim) || victim->desc==NULL) {
	act("$N doesn't seem to be posesed.",ch,NULL,victim,TO_CHAR);
	return;
    }

    do_function(victim,&do_return,"");
    if (IS_EVIL(victim)) {
	act("$N seems to be freed of the power of goodness.",
	    ch,NULL,victim,TO_CHAR);
	act("$n lays $s hands on $N and exorcises the angels out of $M.",
	    ch,NULL,victim,TO_ROOM);
    } else if (IS_NEUTRAL(victim)) {
	act("$N seems to be freed of external powers.",
	    ch,NULL,victim,TO_CHAR);
	act("$n lays $s hands on $N and let $M regain control of $Mself.",
	    ch,NULL,victim,TO_ROOM);
    } else {
	act("$N seems to be freed of the power of evil.",
	    ch,NULL,victim,TO_CHAR);
	act("$n lays $s hands on $N and exorcises the devils out of $M.",
	    ch,NULL,victim,TO_ROOM);
    }
}

void spell_locate_self(int sn,int level,CHAR_DATA *ch,void *vo,int target) {

    sprintf_to_char(ch,"You are in %s, located in %s\n\r",
	ch->in_room->name,ch->in_room->area->name);
    show_exits(ch,FALSE);
}


void spell_blinding_flash(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    CHAR_DATA *targ;
    int spelllevel;
    EFFECT_DATA ef;
    bool isfighting=ch->fighting!=NULL;

    act("$n claps $s hands and an intense amount of light fills the room!",
	ch,NULL,NULL,TO_ROOM);
    act("You clap your hands and an intense amount of light fills the room!",
	ch,NULL,NULL,TO_CHAR);
    for (targ=ch->in_room->people;targ!=NULL;targ=targ->next_in_room) {
	if (targ==ch)
	    continue;

	/* when not fighting people in your group are not affected
	   (assuming they're smart enough to find out what you're
	   doing */
	if (is_same_group(ch,targ) && !isfighting)
	    continue;

	spelllevel=level;
	if (is_same_group(ch,targ))
	    spelllevel/=2;

	/* first get them blinded */
	if (!IS_AFFECTED(targ,EFF_BLIND) &&
	    !saves_spell(spelllevel,targ,DAM_LIGHT)) {
	    ZEROVAR(&ef,EFFECT_DATA);
	    ef.where     = TO_EFFECTS;
	    ef.type      = sn;
	    ef.level     = spelllevel;
	    ef.location  = APPLY_HITROLL;
	    ef.modifier  = -2;
	    ef.duration  = 1;
	    ef.bitvector = EFF_BLIND;
	    ef.casted_by = ch?ch->id:0;
	    effect_to_char(targ,&ef);

	    act("You are blinded by $Ns flash!",targ,NULL,ch,TO_CHAR);
	    act("$n appears to be blinded.",targ,NULL,NULL,TO_ROOM);
	}

	/* then do them "spelllevel" points of damage */
	if (!is_same_group(ch,targ))
	    damage(ch,targ,spelllevel,sn,DAM_LIGHT,TRUE, NULL);
    }
}


/* A pretty wierd spell, but only here to be put into potions */
void spell_restore_mana( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;

    victim->mana += dice(2,8) + level / 3;	// same rules as from healer.c
    victim->mana=UMIN(victim->mana, victim->max_mana );

    if (victim->max_mana == victim->mana)
	send_to_char("Your manapool is full again!\n\r",victim);
    else
	send_to_char( "You feel you have more magical powers.\n\r", victim );

    if ( ch != victim )
	send_to_char( "Ok.\n\r", ch );
}



void spell_bark_skin( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( is_affected( victim, sn ) ) {
	if (victim == ch)
	    send_to_char("Your skin is already as hard as an oak.\n\r",ch);
	else
	    act("$Ns skin is already as hard as can be.",ch,NULL,victim,TO_CHAR);
	return;
    }

    if (is_affected(victim,gsn_stone_skin)) {
	if (victim==ch)
	    send_to_char("Your skin is already hardened.\n\r",ch);
	else
	    act("$Ns skin is already hardened.",ch,NULL,victim,TO_CHAR);
	return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = level;
    ef.location  = APPLY_AC;
    ef.modifier  = -30;
    ef.bitvector = EFF_BARKSKIN;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    act( "$n's skin becomes darker and gets grains.",victim,NULL,NULL,TO_ROOM);
    send_to_char( "Your skin becomes darker and gets grains.\n\r", victim );
    return;
}

// 17 months of 35 days of 24 hours
#define TREE_LIFE 6*35*24

void spell_create_tree(int sn,int level,CHAR_DATA *ch,void *vo,int target)
{
    ROOM_INDEX_DATA *pRoomIndex=ch->in_room;
    char realname[MAX_INPUT_LENGTH],writtenname[MAX_INPUT_LENGTH];
    char txt[MSL];
    char *s;
    OBJ_DATA *tree;
    EXTRA_DESCR_DATA *edd;
    char *treename,*prefix="a";

    s=one_argument(target_name,realname);
    one_argument(s,writtenname);

    if(realname[0]=='\0')
    {
      send_to_char("You need to give your tree a name to create it.\n\r",ch);
      return;
    }

    if(STR_IS_SET(pRoomIndex->strbit_room_flags,ROOM_INDOORS))
    {
      // Produce a lot of smoke and sparkles, but not a tree
      act("You produce a lot of smoke and sparkles, but not a tree.",ch,NULL,NULL,TO_CHAR);
      act("$n produces a lot of smoke and sparkles.",ch,NULL,NULL,TO_ROOM);
      return;
    }

    switch (ch->in_room->sector_type) {
	case SECT_CITY:		treename="oak";		prefix="an";	break;
	case SECT_FIELD:	treename="beech";			break;
	case SECT_FOREST:	treename="maple";			break;
	case SECT_HILLS:	treename="ash";		prefix="an";	break;
	case SECT_MOUNTAIN:	treename="spruce";			break;
	case SECT_DESERT:	treename="cactus";			break;
	case SECT_WATER_SWIM:	treename="mangrove";			break;
	default:
	    act("You produce a lot of smoke and sparkles, but not a tree.",
		ch,NULL,NULL,TO_CHAR);
	    act("$n produces a lot of smoke and sparkles.",
		ch,NULL,NULL,TO_ROOM);
	    return;
    }

    // Create the tree
    tree=create_object(get_obj_index(OBJ_VNUM_PORTALTREE),0);

    tree->timer=TREE_LIFE;

    // Assign it his name
    SetObjectProperty(tree,PROPERTY_STRING,"PortalTree",realname);

    sprintf(txt,"%s big",treename);
    free_string(tree->name);
    tree->name=str_dup(txt);

    sprintf(txt,"A big %s is standing here.",treename);
    free_string(tree->description);
    tree->description=str_dup(txt);

    sprintf(txt,"the %s",treename);
    free_string(tree->short_descr);
    tree->short_descr=str_dup(txt);

    // Scribe the name
    if(writtenname[0]=='\0') strcpy(writtenname,realname);

    edd=new_extra_descr();
    edd->next=tree->extra_descr;
    edd->keyword=str_dup(treename);
    sprintf(txt,"It's %s %s tree. But somehow it appears to be different\n\r"
		"from others. There is a writing on the stem.\n\r",
		prefix,treename);
    edd->description=str_dup(txt);
    tree->extra_descr=edd;

    edd=new_extra_descr();
    edd->next=tree->extra_descr;
    edd->keyword=str_dup("writing");
    // we don't need realname
    sprintf(realname,"The text on the %s reads: '%s'.\n\r",treename,writtenname);
    edd->description=str_dup(realname);
    // Add the description to the object
    tree->extra_descr=edd;

    // store its creator
    sprintf(realname,"Created by %s",NAME(ch));
    SetObjectProperty(tree,PROPERTY_STRING,"comment",realname);

    // And let it grow
    obj_to_room(tree,pRoomIndex);
    op_load_trigger(tree,ch);

    sprintf(txt,
	"Within a minute %s %s grows out of the ground.",prefix,treename);
    act(txt,ch,NULL,NULL,TO_ALL);

    return;
}

void spell_enter_tree(int sn,int level,CHAR_DATA *ch,void *vo,int target)
{
    char treename[MAX_INPUT_LENGTH],name[MAX_INPUT_LENGTH];
    int count;
    int vnumroom,i;
    OBJ_INDEX_DATA *treedata;
    OBJ_DATA *obj,*thistree,*thattree;
    OBJ_DATA *portal;
    bool cactus=FALSE;

    treedata=get_obj_index(OBJ_VNUM_PORTALTREE);

    // There needs to be a tree in this room
    for (thistree = ch->in_room->contents;
	 thistree != NULL; thistree = thistree->next_content ) {
	if ( thistree->pIndexData==treedata && can_see_obj( ch, thistree ))
	    break;
    }

    if (thistree==NULL) {
	send_to_char("There is no tree you can use in this room.\n\r",ch);
	return;
    }

    if (str_cmp(thistree->short_descr,"the cactus")==0)
	cactus=TRUE;

    one_argument(target_name,treename);

    if(treename[0]=='\0') {
	send_to_char("You need to give a treename of the tree you want to go to.\n\r",ch);
	return;
    }

    count=0;
    vnumroom=0;

    // Search all the trees in the mud and remember the vnum of the
    // room the last tree is standing in
    thattree=NULL;
    for(obj=object_list;obj!=NULL;obj=obj->next) {
	if(obj->pIndexData==treedata && obj->in_room
	&& GetObjectProperty(obj,PROPERTY_STRING,"PortalTree",name)
	&& !str_cmp(treename,name)) {
	    count++;
	    vnumroom=obj->in_room->vnum;
	    thattree=obj;
	}
    }

//   portal=new_obj( );
    portal=create_object(get_obj_index(OBJ_VNUM_PORTALTREE),0);
    portal->name=str_dup("tree");
    portal->short_descr=str_dup(thistree->short_descr);
    portal->item_type=ITEM_PORTAL;
    portal->value[0]=0;  // Infine charges
    portal->value[1]=0;  // Exit flags
    portal->value[2]=0;  // Gate flags
    portal->value[3]=0;  // Room to go to

    if(count==0) {
	// The tree does not exists, you have 50% chance you won't enter
	// the tree and 50% ending up at a random room
	if(number_percent()<50) {
	    send_to_char("You can't sense a tree with that name.\n\r",ch);
	    extract_obj(portal);
	    return;
	}
	portal->value[2]=GATE_RANDOM;
	portal->value[3]=ROOM_VNUM_TEMPLE;
    }
    else if(count==1)
    {
	// You will enter the tree without any problems and endup where
	// you want to.
	portal->value[3]=vnumroom;
	if (str_cmp(thattree->short_descr,"the cactus")==0)
	    cactus=TRUE;
    }
    else
    {
	// A random tree with the correct name will be selected. You have
	// a 75% chance you will step into a buggy portal (with the chance
	// of ending up somewhere else.)
	if(number_bits(2)!=0) portal->value[2]=GATE_BUGGY;
	i=number_range(1,count);

	count=0;
	for(obj=object_list;obj!=NULL;obj=obj->next) {
	    if(obj->pIndexData==treedata && obj->in_room
	    && GetObjectProperty(obj,PROPERTY_STRING,"PortalTree",name)
	    && !str_cmp(treename,name)) {
		count++;
		vnumroom=obj->in_room->vnum;
		if(count==i) {
		    if (str_cmp(obj->short_descr,"the cactus")==0)
			cactus=TRUE;
		    break;
		}
	    }
	}

	portal->value[3]=vnumroom;
    }

    obj_to_room(portal,ch->in_room);
    enter(ch,"",0,portal);
    obj_from_room(portal);

    if (cactus) {
	send_to_char(C_HITYOU"Ouch, you've hurt yourself on the cactus. [10]{x\n\r",ch);
	ch->hit-=10;
    }

    extract_obj(portal);

    thistree->timer=TREE_LIFE;
    if (thattree) thattree->timer=TREE_LIFE;
}


void spell_soothe(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    int gain=level;

    if (ch->class == CLASS_CLERIC) {
        gain*=2;
    }
    if (skillcheck(ch,gsn_enhanced_healing)) {
        gain*=2;
        check_improve(ch,gsn_enhanced_healing,TRUE,1);
    } else
        check_improve(ch,gsn_enhanced_healing,FALSE,1);


    if (IS_PC(victim))
	gain_condition(victim,COND_ADRENALINE,-gain/10);

    if (ch==victim)
	send_to_char("You regain your rest of mind!\n\r",victim);
    else
	act("$N regains $S rest of mind!",ch,NULL,victim,TO_CHAR);

    if ( ch != victim ) {
	if (!IS_NPC(ch)) {
	    if (IS_NPC(victim) || victim->pcdata->condition[COND_ADRENALINE]==0)
	    {
		act("$N has $S mind clear.",ch,NULL,victim,TO_CHAR);
	    } else {
		act("$N looks lot calmer now!",ch,NULL,victim,TO_CHAR);
	    }
	} // don't care about NPCs, they see nothing anyway :-)
    }
}


void spell_protection_disease(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if (is_affected(victim,sn)) {
	if (ch==victim)
	    send_to_char("Your skin has already a red tone.\n\r",ch);
	else
	    act("$Ns skin has already a red tone.",ch,NULL,victim,TO_CHAR);
	return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_RESIST;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = 24;
    ef.location  = APPLY_NONE;
    ef.modifier  = 0;
    ef.bitvector = RES_DISEASE;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char("Your skin takes on a red tone.\n\r",victim);
    if (ch!=victim)
	act("$Ns skin takes on a red tone.",ch,NULL,victim,TO_CHAR);

}


void spell_protection_poison(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if (is_affected(victim,sn)) {
	if (ch==victim)
	    send_to_char("You are already protected against poison.\n\r",ch);
	else
	    act("$N is already protected against poison.",ch,NULL,victim,TO_CHAR);
	return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_RESIST;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = 24;
    ef.location  = APPLY_NONE;
    ef.modifier  = 0;
    ef.bitvector = RES_POISON;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char("You feel a mild sparkle through your body.\n\r",victim);
    if (ch!=victim)
	send_to_char("Ok.\n\r",ch);
}


void spell_protection_negative(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if (is_affected(victim,sn)) {
	if (ch==victim)
	    send_to_char("You already have an aura around you.\n\r",ch);
	else
	    act("$N has already an aura around $M.",ch,NULL,victim,TO_CHAR);
	return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_RESIST;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = 24;
    ef.location  = APPLY_NONE;
    ef.modifier  = 0;
    ef.bitvector = RES_NEGATIVE;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char("An aura of positive energy surrounds you.\n\r",victim);
    act("An aura of positive energy surrounds $n.",
	victim,NULL,victim,TO_NOTVICT);
    if (ch!=victim)
	send_to_char("Ok.\n\r",ch);
}


void spell_protection_fire(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if (is_affected(victim,sn)) {
	if (ch==victim)
	    send_to_char("Your skin is already being cooled.\n\r",ch);
	else
	    act("$N is already protected.",ch,NULL,victim,TO_CHAR);
	return;
    }

    if (is_affected(victim,gsn_protection_cold)) {
	if (ch!=victim)
	    act("$N is also protected from cold, adding this one wouldn't "
		"be smart.\n\r",ch,NULL,victim,TO_CHAR);
	else {
	    // woohoo! party!
	    act("$n begins to shiver and to shake.",
		ch,NULL,victim,TO_ROOM);
	    send_to_char("Your protection from cold spell resists...\n\r",ch);
	    spell_frost_breath(gsn_frost_breath,UMIN(ch->level+10,LEVEL_HERO-1),
		ch,ch,TARGET_CHAR);
	    effect_strip(ch,gsn_protection_cold);
	}
	return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where	= TO_RESIST;
    ef.type	= sn;
    ef.level	= level;
    ef.duration	= 24;
    ef.modifier	= 0;
    ef.bitvector= RES_FIRE;
    ef.casted_by= ch?ch->id:0;
    effect_to_char( victim, &ef );

    if (IS_AFFECTED(victim,EFF_HALLUCINATION))
	send_to_char("You be chillin'.\n\r",victim);
    else
	send_to_char("Your blood cools down.\n\r",victim);
    if (ch!=victim)
	send_to_char("Ok.\n\r",ch);
}


void spell_protection_cold(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if (is_affected(victim,sn)) {
	if (ch==victim)
	    send_to_char("Your skin is already being heated.\n\r",ch);
	else
	    act("$N is already protected.",ch,NULL,victim,TO_CHAR);
	return;
    }

    if (is_affected(victim,gsn_protection_fire)) {
	if (ch!=victim)
	    act("$N is also protected from fire, adding this one wouldn't "
		"be smart.\n\r",ch,NULL,victim,TO_CHAR);
	else {
	    // woohoo! party!
	    act("$n begins to shake and to shiver.",
		ch,NULL,victim,TO_ROOM);
	    send_to_char("Your protection from fire spell resists...\n\r",ch);
	    spell_fireball(gsn_fireball,UMIN(ch->level+10,LEVEL_HERO-1),
		ch,ch,TARGET_CHAR);
	    effect_strip(ch,gsn_protection_fire);
	}
	return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where	= TO_RESIST;
    ef.type	= sn;
    ef.level	= level;
    ef.duration	= 24;
    ef.modifier	= 0;
    ef.bitvector= RES_COLD;
    ef.casted_by= ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char("Your blood warms up.\n\r",victim);
    if (ch!=victim)
	send_to_char("Ok.\n\r",ch);
}

void spell_protection_light(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if (is_affected(victim,sn)) {
	if (ch!=victim)
	    act("If you do that $E won't be able to see anything anymore.",
		ch,NULL,victim,TO_CHAR);
	else {
	    // woohoo! party!
	    send_to_char("Your iris begins to shrink more...\n\r",ch);
	    spell_blindness(gsn_blindness,UMIN(ch->level+10,
		LEVEL_HERO-1),ch,ch,TARGET_CHAR);
	    if (is_affected(victim,gsn_blindness))
		effect_strip(ch,gsn_protection_light);
	}
	return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_RESIST;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = 24;
    ef.location  = APPLY_NONE;
    ef.modifier  = 0;
    ef.bitvector = RES_LIGHT;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char("Your iris becomes smaller.\n\r",victim);
    if (ch!=victim)
	send_to_char("Ok.\n\r",ch);
}

void spell_protection_sound(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if (is_affected(victim,sn)) {
	if (ch==victim)
	    send_to_char(
		"You don't want the sound to become more dull.\n\r",ch);
	else
	    act("$N is already protected against sound.",ch,NULL,victim,TO_CHAR);
	return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_RESIST;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = 24;
    ef.location  = APPLY_NONE;
    ef.modifier  = 0;
    ef.bitvector = RES_SOUND;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char("The sounds become dull.\n\r",victim);
    if (ch!=victim)
	send_to_char("Ok.\n\r",ch);
}

void spell_protection_silver(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if (is_affected(victim,sn)) {
	if (ch==victim)
	    send_to_char(
		"You already have an iron aura around you.\n\r",ch);
	else
	    act("$N has already an iron aura around $M.",
		ch,NULL,victim,TO_CHAR);
	return;
    }

    if (is_affected(victim,gsn_protection_iron)) {
	if (ch!=victim) {
	    act("$N has a negative polarisation around $M, it doesn't seem "
		"wise to give $M protection from silver.",
		ch,NULL,victim,TO_CHAR);
	} else {
	    // woohoo! party!
	    act("$n begins to shake and to shiver.",
		ch,NULL,victim,TO_ROOM);
	    send_to_char("Your protection from iron spell resists...\n\r",ch);
	    spell_shocking_grasp(gsn_shocking_grasp,UMIN(ch->level+10,
		LEVEL_HERO-1),ch,ch,TARGET_CHAR);
	    effect_strip(ch,gsn_protection_iron);
	}
	return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_RESIST;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = 24;
    ef.location  = APPLY_NONE;
    ef.modifier  = 0;
    ef.bitvector = RES_SILVER;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char("An iron aura surrounds you.\n\r",victim);
    act("An iron aura surrounds $n.",
	victim,NULL,victim,TO_NOTVICT);
    if (ch!=victim)
	send_to_char("Ok.\n\r",ch);
}

void spell_protection_iron(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if (is_affected(victim,sn)) {
	if (ch==victim)
	    send_to_char(
		"You already have a negative polarisation around you.\n\r",
		ch);
	else
	    act("$N has already a negative polarisation around $M.",
		ch,NULL,victim,TO_CHAR);
	return;
    }

    if (is_affected(victim,gsn_protection_silver)) {
	if (ch!=victim) {
	    act("Seeing the iron aura around $N you don't think it's a good "
		"idea.",ch,NULL,victim,TO_CHAR);
	    return;
	}
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_RESIST;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = 24;
    ef.location  = APPLY_NONE;
    ef.modifier  = 0;
    ef.bitvector = RES_IRON;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char("A negative polarisation surrounds you.\n\r",victim);
    act("A negative polarisation surrounds $n.",victim,NULL,victim,TO_NOTVICT);
    if (ch!=victim)
	send_to_char("Ok.\n\r",ch);

    if (is_affected(victim,gsn_protection_silver)) {
	send_to_char("Your iron aura comes in conflict with your "
	    "negative polarisation...\n\r",ch);
	act("The iron aura around $n comes in conflict with the "
	    "negative polarisation...",ch,NULL,NULL,TO_ROOM);
	spell_shocking_grasp(gsn_shocking_grasp,UMIN(ch->level+10,
	    LEVEL_HERO-1),ch,ch,TARGET_CHAR);
	effect_strip(ch,gsn_protection_iron);
	effect_strip(ch,gsn_protection_silver);
    }
}

void spell_protection_lightning(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if (is_affected(victim,sn)) {
	if (ch==victim)
	    send_to_char("You are already grounded.\n\r",ch);
	else
	    act("$N is already grounded.",ch,NULL,victim,TO_CHAR);
	return;
    }

    if (ch->in_room)
	if (ch->in_room->sector_type==SECT_AIR ||
	    ch->in_room->sector_type==SECT_WATER_SWIM ||
	    ch->in_room->sector_type==SECT_WATER_NOSWIM ||
	    ch->in_room->sector_type==SECT_WATER_BELOW ) {
	    if (ch==victim)
		send_to_char("Your feet don't touch the ground!\n\r",ch);
	    else
		act("$Ns feet don't touch the ground.",ch,NULL,victim,TO_CHAR);
	    return;
	}

    if (is_affected(victim,gsn_fly) || IS_AFFECTED(victim,EFF_FLYING)) {
	if (ch==victim)
	    send_to_char("Your feet don't touch the ground!\n\r",ch);
	else
	    act("$Ns feet don't touch the ground.",ch,NULL,victim,TO_CHAR);
	return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_RESIST;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = 24;
    ef.location  = APPLY_NONE;
    ef.modifier  = 0;
    ef.bitvector = RES_LIGHTNING;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char("You feel yourself safely connected to the ground.\n\r",
	victim);
    act("$n gets connected to the ground $n.",victim,NULL,victim,TO_NOTVICT);
    if (ch!=victim)
	send_to_char("Ok.\n\r",ch);
}

void spell_protection_holy(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if (is_affected(victim,sn)) {
	if (ch==victim)
	    send_to_char(
		"You are already protected from divine powers.\n\r",ch);
	else
	    act("$N is already protected.",ch,NULL,victim,TO_CHAR);
	return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_RESIST;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = 24;
    ef.location  = APPLY_NONE;
    ef.modifier  = 0;
    ef.bitvector = RES_HOLY;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char("You are protected against divine powers.\n\r",victim);
    if (ch!=victim)
	send_to_char("Ok.\n\r",ch);
}

void spell_protection_energy(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if (is_affected(victim,sn)) {
	if (ch==victim)
	    send_to_char("You are already grounded.\n\r",ch);
	else
	    act("$N is already grounded.",ch,NULL,victim,TO_CHAR);
	return;
    }

    if (ch->in_room)
	if (ch->in_room->sector_type==SECT_AIR ||
	    ch->in_room->sector_type==SECT_WATER_SWIM ||
	    ch->in_room->sector_type==SECT_WATER_NOSWIM ||
	    ch->in_room->sector_type==SECT_WATER_BELOW ) {
	    if (ch==victim)
		send_to_char("Your feet don't touch the ground!\n\r",ch);
	    else
		act("$Ns feet don't touch the ground.",ch,NULL,victim,TO_CHAR);
	    return;
	}

    if (is_affected(victim,gsn_fly) || IS_AFFECTED(victim,EFF_FLYING)) {
	if (ch==victim)
	    send_to_char("Your feet don't touch the ground!\n\r",ch);
	else
	    act("$Ns feet don't touch the ground.",ch,NULL,victim,TO_CHAR);
	return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_RESIST;
    ef.type      = sn;
    ef.level     = level;
    ef.duration  = 24;
    ef.location  = APPLY_NONE;
    ef.modifier  = 0;
    ef.bitvector = RES_ENERGY;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char("You feel yourself safely connected to the ground.\n\r",
	victim);
    act("$n gets connected to the ground $n.",victim,NULL,victim,TO_NOTVICT);
    if (ch!=victim)
	send_to_char("Ok.\n\r",ch);
}


void spell_protection_wood(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
}

void spell_protection_acid(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
}

void spell_protection_mental(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
}

void spell_protection_drowning(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
}

void spell_waterbreathing(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    EFFECT_DATA ef;
    CHAR_DATA *victim=vo;

    if (is_affected(victim,sn)) {
	if (victim==ch)
	    send_to_char("You can already breath water.\n\r",ch);
	else
	    act("$e can already breath water.",
		victim,NULL,ch,TO_VICT);
	return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where	= TO_EFFECTS;
    ef.type	= sn;
    ef.level	= level;
    ef.duration	= ch->level/3;
    ef.modifier	= 1;
    ef.location	= APPLY_NONE;
    ef.bitvector= EFF_WATERBREATHING;
    ef.casted_by= ch?ch->id:0;
    effect_to_char(victim,&ef);

    send_to_char("Your lungs can now handle water.\n\r",victim);
    if (victim!=ch)
	act("$e can now breath under water.",victim,NULL,ch,TO_VICT);
}

void spell_entangle(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    CHAR_DATA *victim;
    bool found;

    found=FALSE;

    for (victim=ch->in_room->people;victim;victim=victim->next_in_room) {
	if (!is_same_group(ch,victim) &&
	    !is_safe(ch,victim)) {
	    if (!saves_spell(level,victim,DAM_OTHER)) {
		spell_selective_entangle(sn,level,ch,victim,target);
	    } else {
		act("Your entanglement of $N fails.",ch,NULL,victim,TO_CHAR);
		act("$n tries to entangle you, but it fails.",
		    ch,NULL,victim,TO_VICT);
		act("The entanglement of $N by $n fails.",
		    ch,NULL,victim,TO_NOTVICT);

	    }
	    check_killer(victim,ch);
	    multi_hit( victim, ch, TYPE_UNDEFINED );
	    found=TRUE;
	}
    }

    if (!found)
	send_to_char(
	    "Your entanglement fails due to absence of enemies.\n\r",ch);
}

void spell_selective_entangle(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if (is_affected(victim,gsn_entangle)) {
	if (ch==victim)
	    send_to_char("The vines are struggling harder to hold you.\n\r",ch);
	else
	    act("$N is already entangled.",ch,NULL,victim,TO_CHAR);
	return;
    }

    if (ch->in_room)
	if (ch->in_room->sector_type!=SECT_FIELD &&
	    ch->in_room->sector_type!=SECT_FOREST &&
	    ch->in_room->sector_type!=SECT_MOUNTAIN &&
	    ch->in_room->sector_type!=SECT_DESERT &&
	    ch->in_room->sector_type!=SECT_HILLS) {
	    send_to_char("You're not in the right kind of area to grow "
		"twines and tendrils.\n\r",ch);
	    return;
	}

    if (is_affected(victim,gsn_fly) ||
	IS_AFFECTED(victim,EFF_FLYING)) {
	if (ch==victim)
	    send_to_char("The vines can't reach you.\n\r",ch);
	else
	    act("The vines can't reach $M.",ch,NULL,victim,TO_VICT);
	return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = gsn_entangle;
    ef.level     = level;
    ef.duration  = 4;
    ef.location  = APPLY_DEX;
    ef.modifier  = -1*(1+level/30);
    ef.bitvector = EFF_ENTANGLE;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    ef.where     = TO_EFFECTS;
    ef.type      = gsn_entangle;
    ef.level     = level;
    ef.duration  = 4;
    ef.location  = APPLY_STR;
    ef.modifier  = -1*(1+level/30);
    ef.bitvector = EFF_ENTANGLE;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    act("Twines and tendrils seem to hold $n.",victim,NULL,NULL,TO_ROOM);
    act("Twines and tendrils seem to hold you.",victim,NULL,NULL,TO_CHAR);
}

void spell_unentangle(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;

    if (!is_affected(victim,gsn_entangle)) {
	if (ch==victim)
	    send_to_char("You are not entangled!\n\r",ch);
	else
	    act("$N is not entangled.",ch,NULL,victim,TO_CHAR);
	return;
    }

    check_dispel(level,victim,gsn_entangle);
}



void spell_sandstorm( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim,*next_victim;
    static const int dam_each[] =
    {
	  0,
	  0,   0,   0,   0,   0,	  0,   0,   0,   0,   0,
	  0,   0,   0,   0,  35,	 40,  45,  50,  55,  60,
	 65,  70,  75,  80,  85,	 87,  89,  91,  93,  95,
	 97,  99, 101, 103, 105,	107, 109, 111, 113, 115,
	117, 119, 121, 123, 125,	127, 129, 131, 133, 135,
	137, 139, 141, 143, 145,	146, 147, 148, 149, 150
    };
    int dam;

    if (ch->in_room->sector_type!=SECT_DESERT) {
	send_to_char("Sandstorms can only be initiated from deserts.\n\r",ch);
	return;
    }

    level	= UMIN(level, sizeof(dam_each)/sizeof(dam_each[0]) - 1);
    level	= UMAX(0, level);

    for (victim=ch->in_room->people;victim;victim=next_victim) {
	next_victim=victim->next_in_room;

	if (get_trust(ch)<victim->invis_level) continue;

	dam	= number_range( dam_each[level] / 2, dam_each[level] * 2 );
	if (victim==ch) dam/=2;
	if (saves_spell(level,victim,DAM_OTHER) ) dam/=2;
	damage( ch, victim, dam, sn, DAM_OTHER ,TRUE, NULL);
    }
    return;
}


void spell_refresh_portal(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    OBJ_DATA *portal;
    CHAR_DATA *victim,*next_victim;
    ROOM_INDEX_DATA *room;

    room=ch->in_room;
    portal=(OBJ_DATA *)vo;

    // don't worry about non-portals or non-temporary portals
    if (portal->item_type!=ITEM_PORTAL || portal->timer<=0) {
	sprintf_to_char(ch,"Your spell has no effect on %s.",portal->short_descr);
	return;
    }

    if (number_percent()<portal->timer) {
	if (STR_IS_SET(room->strbit_room_flags,ROOM_SAFE)) {
	    act("$p glows brightly and vanishes!",ch,portal,NULL,TO_ALL);
	} else {
	    act("$p glows brightly and explodes!",ch,portal,NULL,TO_ALL);
	    for (victim=room->people;victim;victim=next_victim) {
		next_victim=victim->next_in_room;
		damage(ch,victim,portal->timer,TYPE_HIT,DAM_PIERCE,FALSE, NULL);
		// this will go wrong of the caster of this spell gets killed
		// here.
		if (ch->in_room!=room)
		    return;
	    }
	}
	extract_obj(portal);
    } else {

	portal->timer+=1+level/10;
	ch->mana-=portal->timer;
	act("$p will last longer.",ch,portal,NULL,TO_CHAR);
	act("$n has reinforced $p.",ch,portal,NULL,TO_ROOM);
	if (ch->mana<0) {
	    send_to_char("You've ran out of mana!\n\r",ch);
	}
    }
}


void spell_divine_relic(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    OBJ_DATA *relic;
    OBJ_INDEX_DATA *relic_index;
    EFFECT_DATA ef;
    char s[MSL];

    relic=get_eq_char(ch,WEAR_FLOAT);
    if (relic) {
	if (relic->pIndexData->vnum==OBJ_VNUM_RELIC) {
	    send_to_char("You are already using a Divine Relic!\n\r",ch);
	    return;
	}
    }

    relic_index=get_obj_index(OBJ_VNUM_RELIC);
    if (relic_index==NULL) {
	bugf("spell_divine_relic: couldn't find object with vnum %d for %s",
	    OBJ_VNUM_RELIC,NAME(ch));
	send_to_char("Error in spell_divine_relic, please contact the implementor.\n\r",ch);
	return;
    }
    relic=create_object(relic_index,0);
    obj_to_char(relic,ch);
    relic->timer=ch->level/2;
    relic->level=ch->level;
    free_string(relic->short_descr);
    free_string(relic->description);
    sprintf(s,"%s's Divine Relic",ch->name);
    relic->short_descr=str_dup(s);
    relic->description=str_dup(s);
    STR_SET_BIT(relic->strbit_extra_flags,ITEM_ANTI_EVIL);

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where	= TO_OBJECT;
    ef.type	= sn;
    ef.level	= level;
    ef.duration	= -1;
    ef.location	= APPLY_AC;
    ef.modifier	= -level/2;
    ef.bitvector= EFF_DIVINE_RELIC;
    ef.casted_by= 0;
    effect_to_obj(relic,&ef);

    send_to_char("You create a Relic.\n\r",ch);
    act("$n has created a Relic.",ch,NULL,NULL,TO_ROOM);
    wear_obj(ch,relic,TRUE,FALSE);
    op_load_trigger(relic,ch);
}

void spell_unholy_relic(int sn,int level,CHAR_DATA *ch,void *vo,int target) {
    OBJ_DATA *relic;
    OBJ_INDEX_DATA *relic_index;
    EFFECT_DATA ef;
    char s[MSL];

    relic=get_eq_char(ch,WEAR_FLOAT);
    if (relic) {
	if (relic->pIndexData->vnum==OBJ_VNUM_RELIC) {
	    send_to_char("You are already using a Unholy Relic!\n\r",ch);
	    return;
	}
    }

    relic_index=get_obj_index(OBJ_VNUM_RELIC);
    if (relic_index==NULL) {
	bugf("spell_unholy_relic: couldn't find object with vnum %d for %s",
	    OBJ_VNUM_RELIC,NAME(ch));
	send_to_char("Error in spell_unholy_relic, please contact the implementor.\n\r",ch);
	return;
    }
    relic=create_object(relic_index,0);
    obj_to_char(relic,ch);
    relic->timer=ch->level/2;
    relic->level=ch->level;
    free_string(relic->short_descr);
    free_string(relic->description);
    sprintf(s,"%s's Unholy Relic",ch->name);
    relic->short_descr=str_dup(s);
    relic->description=str_dup(s);
    STR_SET_BIT(relic->strbit_extra_flags,ITEM_ANTI_GOOD);

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where	= TO_OBJECT;
    ef.type	= sn;
    ef.level	= level;
    ef.duration	= -1;
    ef.location	= APPLY_AC;
    ef.modifier	= -level/2;
    ef.bitvector= EFF_UNHOLY_RELIC;
    ef.casted_by= 0;
    effect_to_obj(relic,&ef);

    send_to_char("You create a Relic.\n\r",ch);
    act("$n has created a Relic.",ch,NULL,NULL,TO_ROOM);
    wear_obj(ch,relic,TRUE,FALSE);
    op_load_trigger(relic,ch);
}



void spell_detect_curse( int sn, int level, CHAR_DATA *ch, void *vo,int target )
{
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( IS_AFFECTED(victim, EFF_DETECT_CURSE) )
    {
        if (victim == ch)
          send_to_char("You can already sense cursed auras.\n\r",ch);
        else
          act("$N can already detect curses.",ch,NULL,victim,TO_CHAR);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level	 = level;
    ef.duration  = level;
    ef.modifier  = 0;
    ef.location  = APPLY_NONE;
    ef.bitvector = EFF_DETECT_CURSE;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char( "Your eyes tingle.\n\r", victim );
    if ( ch != victim )
	send_to_char( "Ok.\n\r", ch );
    return;
}



void spell_hallucination( int sn, int level, CHAR_DATA *ch, void *vo,int target ) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if ( IS_AFFECTED(victim, EFF_HALLUCINATION) ) {
        if (victim == ch)
          send_to_char("The stars blink at you.\n\r",ch);
        else
          act("$N is already hallucinating.",ch,NULL,victim,TO_CHAR);
        return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level	 = level;
    ef.duration  = level;
    ef.modifier  = 0;
    ef.location  = APPLY_NONE;
    ef.bitvector = EFF_HALLUCINATION;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    send_to_char( "Oh wow!  Everything seems so cosmic!\n\r", victim );
    if ( ch != victim )
	send_to_char( "Ok.\n\r", ch );
    return;
}


void spell_clarify(int sn, int level, CHAR_DATA *ch, void *vo,int target) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;

    if (is_affected(victim,gsn_hallucination)) {
	if (!check_dispel(level,victim,gsn_hallucination))
	    send_to_char("Spell failed.\n\r",ch);
	return;
    }

    if (ch==victim)
	send_to_char("Your mind isn't wandering.\n\r",ch);
    else
	act("$N has a clear mind.",ch,NULL,victim,TO_CHAR);
}


void spell_hunger(int sn, int level, CHAR_DATA *ch, void *vo,int target) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if (IS_AFFECTED(victim,EFF_HUNGER)) {
	send_to_char("You are hungry.\n\r",victim);
	if (ch!=victim)
	    act("$E looks hungry.",ch,NULL,victim,TO_CHAR);
	return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level	 = level;
    ef.duration  = level;
    ef.modifier  = -1;
    ef.location  = APPLY_DAMROLL;
    ef.bitvector = EFF_HUNGER;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    if (IS_PC(victim))
	victim->pcdata->condition[COND_HUNGER]=0;

    if (IS_AFFECTED(victim,EFF_HALLUCINATION))
	send_to_char( "The munchies are interfering with your motor capabilities.\n\r",victim);
    else
	send_to_char( "You are hungry.\n\r",victim);
    if ( ch != victim )
	act("$E looks hungry.",ch,NULL,victim,TO_CHAR);
}


void spell_thirst(int sn, int level, CHAR_DATA *ch, void *vo,int target) {
    CHAR_DATA *victim = (CHAR_DATA *) vo;
    EFFECT_DATA ef;

    if (IS_AFFECTED(victim,EFF_THIRST)) {
	send_to_char("You are thirsty.\n\r",victim);
	if (ch!=victim)
	    act("$E looks thirsty.",ch,NULL,victim,TO_CHAR);
	return;
    }

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = sn;
    ef.level	 = level;
    ef.duration  = level;
    ef.modifier  = -1;
    ef.location  = APPLY_HITROLL;
    ef.bitvector = EFF_THIRST;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( victim, &ef );

    if (IS_PC(victim))
	victim->pcdata->condition[COND_THIRST]=0;

    send_to_char( "You are thirsty.\n\r",victim);
    if ( ch != victim )
	act("$E looks thirsty.",ch,NULL,victim,TO_CHAR);
}



void spell_worthless( int sn, int level, CHAR_DATA *ch, void *vo,int target)
{
    OBJ_DATA *obj = (OBJ_DATA *) vo;

    obj->cost=0;
    act("$p is now worthless...",ch,obj,NULL,TO_CHAR);
}
