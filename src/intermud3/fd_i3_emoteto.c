// $Id: fd_i3_emoteto.c,v 1.6 2001/08/14 05:46:03 edwin Exp $

/*
 * Copyright (c) 2000 Fatal Dimensions
 *
 * See the file "LICENSE" or information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 *
 */

#include <stdio.h>
#include <time.h>
#include <strings.h>
#include "../merc.h"
#include "fd_i3.h"

int I3_send_emoteto(CHAR_DATA *ch,char *to,I3_MUD *mud,char *message) {

    if (!I3_is_connected())
	return 0;
    I3_stats.count_emoteto_commands++;

    I3_escape(to);
    I3_write_header("emoteto",I3_THISMUD,ch->name,mud->name,to);
    I3_write_buffer("\"");
    I3_write_buffer(ch->name);
    I3_write_buffer("\",\"");
    I3_write_buffer(message);
    I3_write_buffer("\",})\r");
    I3_send_buffer();

    return 0;
}


int I3_process_emoteto(char *s) {
    CHAR_DATA *victim,*ch;
    I3_HEADER header;
    char *ps=s,*next_ps;
    char visname[MIL];
    char *pmessage,message[MSL],buf[MSL];

    I3_get_header(&ps,&header);

    if ((ch=I3_find_user(header.target_username))==NULL) {
	I3_send_error(header.originator_mudname,header.originator_username,
	    "unk-user","The user you specified is unknown.");
	return 0;
    }

    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    sprintf(visname,"%s@%s",ps,header.originator_mudname);

    ps=next_ps;
    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    pmessage=message;
    while (ps[0]) {
	pmessage[0]=ps[0];
	if (ps[0]=='$' && ps[1]!='N') { // convert $x into $$x
	    pmessage[1]='$';
	    pmessage++;
	}
	pmessage++;ps++;
    }
    pmessage[0]=0;
    if (strstr(message,"$N")==NULL)
	strcat(message," (from $N)");

    victim=create_mobile(get_mob_index(MOB_VNUM_I3_ZOMBIE));
    free_string(victim->short_descr);
    victim->short_descr=str_dup(visname);
    char_to_room(victim,get_room_index(1));		// void
    sprintf(buf,"%s",message);

    act(buf,ch,NULL,victim,TO_CHAR);
    send_to_char("{x",ch);
    return 0;
}
