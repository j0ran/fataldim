// $Id: fd_i3_channel.c,v 1.20 2001/08/14 05:46:03 edwin Exp $

/*
 * Copyright (c) 2000 Fatal Dimensions
 *
 * See the file "LICENSE" or information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 *
 */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include "../merc.h"
#include "fd_i3.h"

// sees if a channel should be connected or disconnected
int I3_check_channel(I3_CHANNEL *channel) {

    if (channel->listener==NULL && channel->connected!=0) {
	I3_send_channel_listen(channel,FALSE);
	wiznet(WIZ_I3_DEBUG,0,NULL,NULL,
	    "I3 - check_channel: unsubscribing from %s (%s@%s)",
	    channel->local_name,channel->i3_name,channel->host_mud);
	channel->connected=FALSE;
	return 0;
    }
    if (channel->listener!=NULL && channel->connected==0) {
	I3_send_channel_listen(channel,TRUE);
	wiznet(WIZ_I3_DEBUG,0,NULL,NULL,
	    "I3 - check_channel: subscribing to %s (%s@%s)",
	    channel->local_name,channel->i3_name,channel->host_mud);
	channel->connected=TRUE;
	return 0;
    }
    return 0;
}

/*
   When a mud sends a startup-req-2 packet, it includes its chanlist-id
   in the packet. The router will potentially respond with a
   chanlist-reply message to update the mud's channel list.

   The router will respond to channel list changes with the
   chanlist-reply packet.
    ({
        (string)  "chanlist-reply",
        (int)     5,
        (string)  originator_mudname,     // the router
        (string)  0,
        (string)  target_mudname,
        (string)  0,
        (int)     chanlist_id,
        (mapping) channel_list
    })

   channel_list is mapping with channel names as keys, and an array of
   two elements as the values. If the value is 0, then the channel has
   been deleted. The array contains the host mud, and the type of the
   channel:
        0  selectively banned
        1  selectively admitted
        2  filtered (selectively admitted)

   All channel messages are delivered to the router. It will then pass
   the message to the appropriate set of muds. If the channel is
   filtered, then the packet will be delivered to the host mud for
   filtering; it will then return to the router network for distribution.
   It is assumed that a channel packet for a filtered channel that comes
   from the channel host has been filtered.


*/

int I3_process_chanlist_reply(char *s) {
    char *ps=s,*next_ps;
    I3_CHANNEL *channel;
    I3_HEADER header;

    I3_get_header(&ps,&header);
    I3_get_field(ps,&next_ps);	// chanlist_id
    ps=next_ps;
    ps+=2;

    while (1) {
	char *next_ps2;

	// get the name of the channel
	I3_get_field(ps,&next_ps);
	I3_remove_quotes(&ps);
//	printf("Downloading %s\n",ps);

	if ((channel=find_i3_channel_by_name(ps))==NULL) {
	    channel=new_i3_channel();
	    free_string(channel->i3_name);
	    channel->i3_name=str_dup(ps);
	}

	ps=next_ps;
	I3_get_field(ps,&next_ps2);
	if (ps[0]!='0') {
	    ps+=2;
	    I3_get_field(ps,&next_ps);
	    I3_remove_quotes(&ps);
	    free_string(channel->host_mud);
	    channel->host_mud=str_dup(ps);
	    ps=next_ps;
	    I3_get_field(ps,&next_ps);
	    channel->status=atoi(ps);
	}
	ps=next_ps2;
	if (ps[0]==']')
	    break;
    }

    return 0;
}

/*
   A mud may decide whether or not it is listening to any given channel
   by sending a channel-listen packet. This packet is also used to tune
   out a channel, which should be done whenever no one on the mud is
   listening to the channel. The format of this packet is:
    ({
        (string) "channel-listen",
        (int)    5,
        (string) originator_mudname,
        (string) 0,
        (string) target_mudname,         // the router
        (string) 0,
        (string) channel_name,
        (int)    on_or_off
    })

   The on_or_off will contain one of the following values:
        0 The mud does not wish to receive this channel.
        1 The mud wishes to receive this channel.

  ...8({"channel-listen",5,"NoNameMUD",0,"*gjs",0,"test",1,}).
  ...?({"channel-listen",5,"NoNameMUD",0,"*gjs",0,"imud_gossip",1,}).
*/
int I3_send_channel_listen(I3_CHANNEL *channel,bool connect) {

    if (!I3_is_connected())
	return 0;
    I3_stats.count_channel_listen++;

    I3_write_header("channel-listen",I3_THISMUD,NULL,I3_ROUTER_NAME,NULL);
    I3_write_buffer("\"");
    I3_write_buffer(channel->i3_name);
    I3_write_buffer("\",");
    if (connect)
	I3_write_buffer("1,})\r");
    else
	I3_write_buffer("0,})\r");
    I3_send_buffer();
    return 0;
}

/*
   Channel messages come in three flavors: standard messages, emotes, and
   targetted emotes. These use packets channel-m, channel-e, and
   channel-t, respectively. They are:
    ({
        (string) "channel-m",
        (int)    5,
        (string) originator_mudname,
        (string) originator_username,
        (string) 0,
        (string) 0,
        (string) channel_name,
        (string) visname,
        (string) message
    })
*/
int I3_send_channel_message(I3_CHANNEL *channel,char *name,char *message) {

    if (!I3_is_connected())
	return 0;
    I3_stats.count_channel_m_commands++;

    I3_write_header("channel-m",I3_THISMUD,name,NULL,NULL);
    I3_write_buffer("\"");
    I3_write_buffer(channel->i3_name);
    I3_write_buffer("\",\"");
    I3_write_buffer(name);
    I3_write_buffer("\",\"");
    I3_write_buffer(I3_escape(message));
    I3_write_buffer("\",})\r");
    I3_send_buffer();

    return 0;
}

/*
   Channel messages come in three flavors: standard messages, emotes, and
   targetted emotes. These use packets channel-m, channel-e, and
   channel-t, respectively. They are:
    ({
        (string) "channel-e",
        (int)    5,
        (string) originator_mudname,
        (string) originator_username,
        (string) 0,
        (string) 0,
        (string) channel_name,
        (string) visname,
        (string) message
    })
*/
int I3_send_channel_emote(I3_CHANNEL *channel,char *name,char *message) {

    if (!I3_is_connected())
	return 0;
    I3_stats.count_channel_e_commands++;

    I3_write_header("channel-e",I3_THISMUD,name,NULL,NULL);
    I3_write_buffer("\"");
    I3_write_buffer(channel->i3_name);
    I3_write_buffer("\",\"");
    I3_write_buffer(name);
    I3_write_buffer("\",\"");
    if (strstr(message,"$N")==NULL)
	I3_write_buffer("$N ");
    I3_write_buffer(I3_escape(message));
    I3_write_buffer("\",})\r");
    I3_send_buffer();

    return 0;
}

int I3_process_channel_m(char *s) {
    char *ps=s,*next_ps;
    I3_HEADER header;
    char visname[MSL];
    char message[MSL];
    char buf[MSL];
    I3_CHANNEL *channel;
    I3_LISTENER *listener;

    I3_get_header(&ps,&header);

    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    if ((channel=find_i3_channel_by_name(ps))==NULL) {
	wiznet(WIZ_I3_ERRORS,0,NULL,NULL,
	    "I3 - channel_m: received unknown channel (%s)",ps);
	return 0;
    }

    ps=next_ps;
    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    strcpy(visname,ps);

    ps=next_ps;
    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    strcpy(message,ps);

    sprintf(buf,channel->layout_m,
	channel->local_name,visname,header.originator_mudname,message);
    for (listener=channel->listener;listener;listener=listener->next) {
	if (listener->desc->connected == CON_PLAYING &&
#ifdef IS_FD
	    !STR_IS_SET(listener->desc->character->strbit_comm,COMM_I3MUTE))
#else
	    !IS_SET(listener->desc->character->comm,COMM_I3MUTE))
#endif
	    send_to_char(buf,listener->desc->character);
    }

    return 0;
}


int I3_process_channel_e(char *s) {
    char *ps=s,*next_ps,*pmessage;
    I3_HEADER header;
    char visname[MSL];
    char message[MSL];
    char buf[MSL];
    I3_CHANNEL *channel;
    I3_LISTENER *listener;
    CHAR_DATA *victim;

    I3_get_header(&ps,&header);

    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    if ((channel=find_i3_channel_by_name(ps))==NULL) {
	wiznet(WIZ_I3_ERRORS,0,NULL,NULL,
	    "I3 - channel_e: received unknown channel (%s)",ps);
	return 0;
    }

    ps=next_ps;
    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    sprintf(visname,"%s@%s",ps,header.originator_mudname);

    ps=next_ps;
    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    pmessage=message;
    while (ps[0]) {
	pmessage[0]=ps[0];
	if (ps[0]=='$' && ps[1]!='N') {	// convert $x into $$x
	    pmessage[1]='$';
	    pmessage++;
	}
	pmessage++;ps++;
    }
    pmessage[0]=0;

    victim=create_mobile(get_mob_index(MOB_VNUM_I3_ZOMBIE));
    free_string(victim->short_descr);
    victim->short_descr=str_dup(visname);
    char_to_room(victim,get_room_index(1));		// void

    sprintf(buf,channel->layout_e,channel->local_name,message);
    for (listener=channel->listener;listener;listener=listener->next) {
	if (listener->desc->connected == CON_PLAYING &&
#ifdef IS_FD
	    !STR_IS_SET(listener->desc->character->strbit_comm,COMM_I3MUTE))
#else
	    !IS_SET(listener->desc->character->comm,COMM_I3MUTE))
#endif
	    act(buf,listener->desc->character,NULL,victim,TO_CHAR);
//	send_to_char(buf,listener->ch);
    }
    extract_char(victim,TRUE);

    return 0;
}

/*
   A list of who is listening to a channel on a remote mud may be
   requested with the following packet:
    ({
        (string) "chan-who-req",
        (int)    5,
        (string) originator_mudname,
        (string) originator_username,
        (string) target_mudname,
        (string) 0,
        (string) channel_name
    })
*/
int I3_process_chan_who_req(char *s) {
    I3_HEADER header;
    char *ps=s,*next_ps;
    I3_CHANNEL *channel;
    I3_LISTENER *listener;

    I3_get_header(&ps,&header);
    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    if ((channel=find_i3_channel_by_name(ps))==NULL) {
	char buf[MSL];

	sprintf(buf,"The channel you specified (%s) is unknown at %s",
	    ps,I3_THISMUD);
	I3_escape(buf);
	I3_send_error(header.originator_mudname,header.originator_username,
	    "unk-channel",buf);

	wiznet(WIZ_I3_ERRORS,0,NULL,NULL,
	    "I3 - chan_who_req: received unknown channel (%s)",ps);
	return 0;
    }
    if (channel->local_name[0]==0) {
	char buf[MSL];

	sprintf(buf,"The channel you specified (%s) is not registered at %s",
	    ps,I3_THISMUD);
	I3_escape(buf);
	I3_send_error(header.originator_mudname,header.originator_username,
	    "unk-channel",buf);
	return 0;
    }

    I3_write_header("chan-who-reply",I3_THISMUD,NULL,
	header.originator_mudname,header.originator_username);
    I3_write_buffer("\"");
    I3_write_buffer(channel->i3_name);
    I3_write_buffer("\",({");

    for (listener=channel->listener;listener;listener=listener->next) {
	I3_write_buffer("\"");
	I3_write_buffer(listener->desc->character->name);
	I3_write_buffer("\",");
    }
    I3_write_buffer("}),})\r");
    I3_send_buffer();

    return 0;
}

/*
   The reply for the who request takes the following format:
    ({
        (string)   "chan-who-reply",
        (int)      5,
        (string)   originator_mudname,
        (string)   0,
        (string)   target_mudname,
        (string)   target_username,
        (string)   channel_name,
        (string *) user_list
    })

   The user_list should be an array of strings, representing the users'
   "visual" names.
*/
int I3_process_chan_who_reply(char *s) {
    char *ps=s,*next_ps;
    I3_HEADER header;
    CHAR_DATA *ch;

    I3_get_header(&ps,&header);
    if ((ch=I3_find_user(header.target_username))==NULL) {
	bugf("I3 - I3_process_chan_who_reply(): user %s not found.",
	    header.target_username);
	return 0;
    }

    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    sprintf_to_char(ch,"CHANWHO reply from %s for %s\n\r",
	header.originator_mudname,ps);

    ps=next_ps;
    I3_get_field(ps,&next_ps);
    ps+=2;
    while (1) {
	if (ps[0]=='}') {
	    send_to_char(
		"No information returned or no people listening.\n\r",ch);
	    return 0;
	}

	I3_get_field(ps,&next_ps);
	I3_remove_quotes(&ps);
	sprintf_to_char(ch,"- %s\n\r",ps);

	ps=next_ps;
	if (ps[0]=='}')
	    break;
    }

    send_to_char("{x",ch);
    return 0;
}

int I3_send_chan_who(CHAR_DATA *ch,I3_CHANNEL *channel,I3_MUD *mud) {

    if (!I3_is_connected())
	return 0;
    I3_stats.count_channel_who_commands++;

    I3_write_header("chan-who-req",I3_THISMUD,ch->name,mud->name,NULL);
    I3_write_buffer("\"");
    I3_write_buffer(channel->i3_name);
    I3_write_buffer("\",})\r");
    I3_send_buffer();

    return 0;
}

