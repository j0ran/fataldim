// $Id: fd_i3_who.c,v 1.14 2001/08/14 05:46:03 edwin Exp $

/*
 * Copyright (c) 2000 Fatal Dimensions
 *
 * See the file "LICENSE" or information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 *
 */

#include <stdio.h>
#include <strings.h>
#include <time.h>
#include "../merc.h"
#include "fd_i3.h"

int I3_send_who(CHAR_DATA *ch,char *mud) {

    if (!I3_is_connected())
	return 0;
    I3_stats.count_who_commands++;

    I3_escape(mud);
    I3_write_header("who-req",I3_THISMUD,ch->name,mud,NULL);
    I3_write_buffer("})\r");
    I3_send_buffer();
    return 0;
}

/*
   The router will route the packet to another router or to the target
   mud. The target mud returns:
    ({
        (string)  "who-reply",
        (int)     5,
        (string)  originator_mudname,
        (string)  0,
        (string)  target_mudname,
        (string)  target_username,
        (mixed *) who_data
    })

   where who_data is an array containing an array of the following format
   for each user on the mud:
    ({
        (string)  user_visname,
        (int)     idle_time,
        (string)  xtra_info
    })

   Each user_visname should specify the user's visual name. idle_time
   should be measured in seconds and xtra_info should be a string.
 */
int I3_process_who_reply(char *s) {
    char *ps=s,*next_ps,*next_ps2;
    CHAR_DATA *ch;
    I3_HEADER header;

    I3_get_header(&ps,&header);

    if ((ch=I3_find_user(header.target_username))==NULL) {
	// just ignore, message was mud generated
	return 0;
    }

    ps+=2;

// ({"who-reply",5,"NoNameMUD",0,"MavEtJu","MavEtJu",({({"Mavetju",8,"Mavetju the Newbie",}),}),})
    sprintf_to_char(ch,"RWHO reply from %s\n\r",header.originator_mudname);
    while (1) {
	if (ps[0]=='}') {
	    send_to_char("No information returned.\n\r",ch);
	    return 0;
	}

	I3_get_field(ps,&next_ps);

	ps+=2;
	I3_get_field(ps,&next_ps2);
	I3_remove_quotes(&ps);
	sprintf_to_char(ch,"%-15s",ps);
	ps=next_ps2;
	I3_get_field(ps,&next_ps2);
	sprintf_to_char(ch,"(idle %s secs)%s",
	    ps,strlen(ps)==1?"   ":strlen(ps)==2?"  ":strlen(ps)==3?" ":"");
	ps=next_ps2;
	I3_get_field(ps,&next_ps2);
	I3_remove_quotes(&ps);
	sprintf_to_char(ch," %s{x\n\r",ps);
	ps=next_ps2;

	ps=next_ps;
	if (ps[0]=='}')
	    break;
    }

    send_to_char("{x",ch);
    return 0;
}

int I3_process_who_req(char *s) {
    char *ps=s;
    I3_HEADER header;
    DESCRIPTOR_DATA *d;
    char smallbuf[MSL];

    I3_get_header(&ps,&header);

    wiznet(WIZ_I3_INFO,0,NULL,NULL,
	"I3 - Who: request from %s @ %s.",
	header.originator_username,header.originator_mudname);

    I3_write_header("who-reply",I3_THISMUD,NULL,
	header.originator_mudname,header.originator_username);
    I3_write_buffer("({");

    for (d=descriptor_list;d;d=d->next) {
	if (d->character &&
	    d->original==NULL &&		// don't do switched ones
	    d->connected==CON_PLAYING) {
	    I3_write_buffer("({\"");
	    I3_write_buffer(I3_escape(d->character->name));
	    I3_write_buffer("\",");
	    sprintf(smallbuf,"%ld",I3_user_idle(d->character));
	    I3_write_buffer(smallbuf);
	    I3_write_buffer(",\"");
	    I3_write_buffer(I3_escape(d->character->name));
	    I3_write_buffer(I3_escape(d->character->pcdata->title));
	    I3_write_buffer("\",}),");
	}
    }
    I3_write_buffer("}),})\r");
    I3_send_buffer();

    return 0;
}
