// $Id: fd_i3_locate.c,v 1.12 2001/08/14 05:46:03 edwin Exp $

/*
 * Copyright (c) 2000 Fatal Dimensions
 *
 * See the file "LICENSE" or information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 *
 */

#include <stdio.h>
#include <time.h>
#include <strings.h>
#include "../merc.h"
#include "fd_i3.h"

int I3_send_locate(CHAR_DATA *ch,char *user) {

    if (!I3_is_connected())
	return 0;
    I3_stats.count_locate_commands++;

    I3_escape(user);
    I3_write_header("locate-req",I3_THISMUD,ch->name,NULL,NULL);
    I3_write_buffer("\"");
    I3_write_buffer(user);
    I3_write_buffer("\",})");
    I3_send_buffer();
    return 0;
}

/*
   If the requested user is logged into the receiving mud, then the
   following reply is returned:
    ({
        (string) "locate-reply",
        (int)    5,
        (string) originator_mudname,
        (string) 0,
        (string) target_mudname,
        (string) target_username,
        (string) located_mudname,
        (string) located_visname,
        (int)    idle_time,
        (string) status,
    })

   located_visname should contain the correct visual name of the user.
   idle_time will have the idle time (in seconds) of the located user.

   status specifies any special status of the user. This will typically
   be zero to indicate that the user has no special status. The values
   for this string are arbitrary, but certain statuses have predefined
   values that can be used:
     * "link-dead"
     * "editing"
     * "inactive"
     * "invisible"
     * "hidden"

({"locate-reply",5,"NoNameMUD",0,"MavEtJu","MavEtJu","NoNameMUD","Mavetju",277,"active",})
*/

int I3_process_locate_reply(char *s) {
    char *ps=s,*next_ps;
    CHAR_DATA *ch;
    I3_HEADER header;

    I3_get_header(&ps,&header);

    if ((ch=I3_find_user(header.target_username))==NULL) {
	// don't print anything since it's a mud-message
	return 0;
    }

    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    sprintf_to_char(ch,"RLOCATE reply from %s\n\r",ps);
    ps=next_ps;

    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    sprintf_to_char(ch,"Username  : %s\n\r",ps);
    ps=next_ps;

    I3_get_field(ps,&next_ps);
    sprintf_to_char(ch,"Idle since: %s seconds\n\r",ps);
    ps=next_ps;

    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    sprintf_to_char(ch,"Status    : %s\n\r",ps);

    send_to_char("{x",ch);
    return 0;
}

int I3_process_locate_req(char *s) {
    char *ps=s,*next_ps;
    I3_HEADER header;
    char smallbuf[MSL];
    CHAR_DATA *ch;

    I3_get_header(&ps,&header);
    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    wiznet(WIZ_I3_INFO,0,NULL,NULL,
	"I3 - Locate: request from %s @ %s for %s.",
	header.originator_username,header.originator_mudname,ps);

    if ((ch=I3_find_user(ps))==NULL) {
	return 0;
    }

    I3_write_header("locate-reply",I3_THISMUD,NULL,
		      header.originator_mudname,header.originator_username);
    I3_write_buffer("\"");
    I3_write_buffer(I3_THISMUD);
    I3_write_buffer("\",\"");
    I3_write_buffer(ch->name);
    I3_write_buffer("\",");
    sprintf(smallbuf,"%ld",I3_user_idle(ch));
    I3_write_buffer(smallbuf);
    I3_write_buffer(",\"active\",})");
    I3_send_buffer();

    return 0;
}

