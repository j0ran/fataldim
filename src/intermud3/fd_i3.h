// $Id: fd_i3.h,v 1.14 2001/02/25 17:06:07 edwin Exp $

/*
 * Copyright (c) 2000 Fatal Dimensions
 *
 * See the file "LICENSE" or information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 *
 */

// I3 router information
//#define I3_ROUTER             "216.98.238.194"
//#define I3_PORT               9000

// I3 client information
#define I3_PASSWORD           "0"
#define I3_MUDLIST_ID         "10000000"
#define I3_CHANLIST_ID        "10000000"

// internal structures
typedef struct i3_channel	I3_CHANNEL;
typedef struct i3_listener	I3_LISTENER;
typedef struct i3_mud		I3_MUD;
typedef struct i3_stats		I3_STATS;
typedef struct i3_header	I3_HEADER;

struct i3_listener {
    I3_LISTENER *next;
    DESCRIPTOR_DATA *desc;
};

struct i3_header {
    char originator_mudname[256];
    char originator_username[256];
    char target_mudname[256];
    char target_username[256];
};

struct i3_channel {
    I3_CHANNEL *next;
    I3_LISTENER *listener;
    char *local_name;
    char *host_mud;
    char *i3_name;
    char *layout_m;
    char *layout_e;
    int status;
    int local_level;
    bool connected;
};

struct i3_mud {
    I3_MUD *next;
    int  status;
    char *name;
    char *ipaddress;
    int  player_port;
    int  imud_tcp_port;
    int  imud_udp_port;
    char *mudlib;
    char *base_mudlib;
    char *driver;
    char *mud_type;
    char *open_status;
    char *admin_email;

    bool tell:1;
    bool emoteto:1;
    bool who:1;
    bool finger:1;
    bool locate:1;
    bool channel:1;
    bool news:1;
    bool mail:1;
    bool file:1;
    bool auth:1;
    bool ucache:1;

    int smtp;
    int ftp;
    int nntp;
    int http;
    int pop3;
    int rcp;
    int amrcp;

    // only used for this mud
    char *routerIP;
    int  routerPort;
    char *routerName;
    bool autoconnect;
};

struct i3_stats {
    int count_tell_commands;
    int count_tell;
    int count_emoteto_commands;
    int count_emoteto;
    int count_who_commands;
    int count_who_req;
    int count_who_reply;
    int count_finger_commands;
    int count_finger_req;
    int count_finger_reply;
    int count_locate_commands;
    int count_locate_req;
    int count_locate_reply;
    int count_chanlist_reply;
    int count_channel_m_commands;
    int count_channel_m;
    int count_channel_e_commands;
    int count_channel_e;
    int count_channel_t_commands;
    int count_channel_t;
    int count_channel_add;
    int count_channel_remove;
    int count_channel_admin;
    int count_channel_filter_req;
    int count_channel_filter_reply;
    int count_channel_who_commands;
    int count_channel_who_req;
    int count_channel_who_reply;
    int count_channel_listen;
    int count_chan_user_req;
    int count_chan_user_reply;
    int count_news_read_req;
    int count_news_post_req;
    int count_news_grplist_req;
    int count_mail;
    int count_mail_ack;
    int count_file_list_req;
    int count_file_list_reply;
    int count_file_put;
    int count_file_get_req;
    int count_file_get_reply;
    int count_auth_mud_req;
    int count_auth_mud_reply;
    int count_ucache_update;
    int count_error;
    int count_startup_req_3;
    int count_startup_reply;
    int count_shutdown;
    int count_mudlist;
    int count_oob_req;
    int count_oob_begin;
    int count_oob_end;

    int count_unknown;
    int count_total;
};

extern I3_CHANNEL *I3_channel_list;
extern I3_MUD *I3_mud_list;
extern I3_STATS I3_stats;

// fd_i3_recycle.c
I3_CHANNEL *new_i3_channel(void);
void destroy_i3_channel(I3_CHANNEL *channel);
I3_CHANNEL *find_i3_channel_by_name(char *name);
I3_CHANNEL *find_i3_channel_by_localname(char *name);

I3_MUD *new_i3_mud(void);
void destroy_i3_mud(I3_MUD *mud);
I3_MUD *find_i3_mud_by_name(char *name);

I3_LISTENER *find_i3_listener_by_char(I3_CHANNEL *channel,CHAR_DATA *ch);
void destroy_i3_listener(I3_CHANNEL *channel,I3_LISTENER *listener);
I3_LISTENER *new_i3_listener(I3_CHANNEL *channel);
I3_LISTENER *find_i3_listener_by_descriptor(I3_CHANNEL *channel,DESCRIPTOR_DATA *desc);


// fd_i3_main.c
char *I3_get_field(char *packet,char **ps);
//void I3_write_packet(char *msg);
void I3_write_buffer(char *msg);
void I3_send_buffer();
void I3_main(bool forced);
void I3_loop(void);
void I3_shutdown(int delay);
void I3_remove_quotes(char **ps);
char *I3_escape(char *ps);
void I3_get_header(char **pps,I3_HEADER *header);
void I3_write_header(char *identifier,char *originator_mudname,
		       char *originator_username,char *target_mudname,
		       char *target_username);
CHAR_DATA *I3_find_user(char *name);
bool I3_command_hook(CHAR_DATA *ch,char *command,char *argument);
bool I3_is_connected(void);
#ifndef HAVE_SPRINTFTOCHAR
void sprintf_to_char(CHAR_DATA *ch, const char *fmt, ...);
#endif

// fd_i3_tools.c
void i3_printstats(int i);

// fd_i3_support.c
int I3_process_startup_reply(char *s);
int I3_process_mudlist(char *s);
void I3_startup_packet(void);
void I3_process_error(char *s);
int I3_send_shutdown(int delay);
void I3_send_error(char *mud,char *user,char *code,char *message);

// fd_i3_channel.c
int I3_check_channel(I3_CHANNEL *channel);
int I3_process_chanlist_reply(char *s);
int I3_process_channel_m(char *s);
int I3_process_channel_e(char *s);
int I3_process_chan_who_req(char *s);
int I3_process_chan_who_reply(char *s);
int I3_send_channel_listen(I3_CHANNEL *channel,bool connect);
int I3_send_channel_message(I3_CHANNEL *channel,char *name,char *message);
int I3_send_channel_emote(I3_CHANNEL *channel,char *name,char *message);
int I3_send_chan_who(CHAR_DATA *ch,I3_CHANNEL *channel,I3_MUD *mud);

// fd_i3_who.c
int I3_send_who(CHAR_DATA *ch,char *mud);
int I3_process_who_reply(char *s);
int I3_process_who_req(char *s);

// fd_i3_finger.c
int I3_send_finger(CHAR_DATA *ch,char *user,char *mud);
int I3_process_finger_reply(char *s);
int I3_process_finger_req(char *s);

// fd_i3_locate.c
int I3_send_locate(CHAR_DATA *ch,char *user);
int I3_process_locate_reply(char *s);
int I3_process_locate_req(char *s);

// fd_i3_tell.c
int I3_send_tell(CHAR_DATA *ch,char *to,I3_MUD *mud,char *message);
int I3_process_tell(char *s);

// fd_i3_emoteto.c
int I3_send_emoteto(CHAR_DATA *ch,char *to,I3_MUD *mud,char *message);
int I3_process_emoteto(char *s);

// fd_i3_config.c
void I3_read_config(void);
void I3_read_channel_config(void);
void I3_write_channel_config(void);
extern I3_MUD this_mud;
extern char *I3_THISMUD;
extern char *I3_ROUTER_NAME;

// fd_i3_rom.c
void i3_listen_channel(DESCRIPTOR_DATA *desc,char *argument,bool silent);

// fd_i3_local.c
long I3_user_idle(CHAR_DATA *ch);
char *I3_user_email(CHAR_DATA *ch);
char *I3_user_whoname(CHAR_DATA *ch);
