// $Id: fd_i3_rom.c,v 1.18 2001/08/15 02:20:43 edwin Exp $

/*
 * Copyright (c) 2000 Fatal Dimensions
 *
 * See the file "LICENSE" or information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 *
 */

#include <time.h>
#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include "../merc.h"
#include "fd_i3.h"

void i3_mudlist(CHAR_DATA *ch,char *argument) {
    I3_MUD *mud;
    BUFFER *buf;
    char s[MSL],filter[MIL];
    bool all=FALSE;

    if (IS_NPC(ch)) return;

    argument=one_argument(argument,filter);
    if (str_cmp(filter,"all")==0) {
	all=TRUE;
	argument=one_argument(argument,filter);
    }

    buf=new_buf();
    add_buf(buf,"Name                      Type   Mudlib                  Address           Port\n\r");
    for (mud=I3_mud_list;mud;mud=mud->next) {
	if (filter[0] && str_prefix(filter,mud->name))
	    continue;
	if (!all && mud->status==0)		// do not print down
	    continue;

	switch (mud->status) {
	case -1:
	    sprintf(s,"%-26s%-7s%-24s%-17s %d\n\r",
		mud->name,mud->mud_type,mud->mudlib,mud->ipaddress,
		mud->player_port);
	    break;
	case 0:
	    sprintf(s,"%-26s(down)\n\r",mud->name);
	    break;
	default:
	    sprintf(s,"%-26s(rebooting, back in %d seconds)\n\r",
		mud->name,mud->status);
	    break;
	}
	add_buf(buf,s);
    }
    page_to_char(buf_string(buf),ch);
    free_buf(buf);
    return;
}

void i3_chanlist(CHAR_DATA *ch,char *argument) {
    I3_CHANNEL *channel;
    I3_LISTENER *listener;
    BUFFER *buf;
    char s[MSL];
    bool all=FALSE,found=FALSE;

    if (IS_NPC(ch)) return;

    if (str_cmp(argument,"all")==0)
	all=TRUE;

    buf=new_buf();
    add_buf(buf,"  Local name        Lvl I3 Name           Hosted at\n\r");
    for (channel=I3_channel_list;channel;channel=channel->next) {
	if (!all && channel->local_name[0]==0)
	    continue;
	if ((listener=find_i3_listener_by_char(channel,ch)))
	    found=TRUE;
	sprintf(s,"%c %-18s%-4d%-20s%-20s\n\r",
	    listener?'*':' ',
	    channel->local_name,channel->local_level,
	    channel->i3_name,channel->host_mud);
	add_buf(buf,s);
    }
    if (found)
	add_buf(buf,"*: You are subscribed to this channel.\n\r");
#ifdef IS_FD
    if (STR_IS_SET(ch->strbit_comm,COMM_I3MUTE))
#else
    if (IS_SET(ch->comm,COMM_I3MUTE))
#endif
	add_buf(buf,"Note: You are currently muting the channels. Use '{WI3 mute{x' to remove this.\n\r");
    page_to_char(buf_string(buf),ch);
    free_buf(buf);
    return;
}

void i3_stats(CHAR_DATA *ch,char *argument) {

    if (IS_NPC(ch)) return;

    sprintf_to_char(ch,"messages	: %d (%d unknown)\n",
					I3_stats.count_total,
					I3_stats.count_unknown);
    sprintf_to_char(ch,"private		: %d/%d tells/%d/%d emotes\n",
					I3_stats.count_tell_commands,
					I3_stats.count_tell,
					I3_stats.count_emoteto_commands,
					I3_stats.count_emoteto);
    sprintf_to_char(ch,"who		: %d commands/%d req/%d reply\n",
					I3_stats.count_who_commands,
					I3_stats.count_who_req,
					I3_stats.count_who_reply);
    sprintf_to_char(ch,"finger		: %d commands/%d req/%d reply\n",
					I3_stats.count_finger_commands,
					I3_stats.count_finger_req,
					I3_stats.count_finger_reply);
    sprintf_to_char(ch,"locate		: %d commands/%d req/%d reply\n",
					I3_stats.count_locate_commands,
					I3_stats.count_locate_req,
					I3_stats.count_locate_reply);
    sprintf_to_char(ch,"channels	: %d m/%d e/%d t sent\n",
					I3_stats.count_channel_m_commands,
					I3_stats.count_channel_e_commands,
					I3_stats.count_channel_t_commands);
    sprintf_to_char(ch,"		: %d m/%d e/%d t\n",
					I3_stats.count_channel_m,
					I3_stats.count_channel_e,
					I3_stats.count_channel_t);
    sprintf_to_char(ch,"		: %d list-reply/%d add/%d remove\n",
					I3_stats.count_chanlist_reply,
					I3_stats.count_channel_add,
					I3_stats.count_channel_remove);
    sprintf_to_char(ch,"		: filter %d req/%d reply\n",
					I3_stats.count_channel_filter_req,
					I3_stats.count_channel_filter_reply);
    sprintf_to_char(ch,"		: who %d commands/%d req/%d reply\n",
					I3_stats.count_channel_who_commands,
					I3_stats.count_channel_who_req,
					I3_stats.count_channel_who_reply);
    sprintf_to_char(ch,"		: user %d req/%d reply\n",
					I3_stats.count_chan_user_req,
					I3_stats.count_chan_user_reply);
    sprintf_to_char(ch,"news		: %d read/%d post/%d grplist_req\n",
					I3_stats.count_news_read_req,
					I3_stats.count_news_post_req,
					I3_stats.count_news_grplist_req);
    sprintf_to_char(ch,"mail		: %d/%d ack\n",
					I3_stats.count_mail,
					I3_stats.count_mail_ack);
    sprintf_to_char(ch,"filelist	: %d req/%d reply\n",
					I3_stats.count_file_list_req,
					I3_stats.count_file_list_reply);
    sprintf_to_char(ch,"file		: %d put/%d getreq/%d getreply\n",
					I3_stats.count_file_put,
					I3_stats.count_file_get_req,
					I3_stats.count_file_get_reply);
    sprintf_to_char(ch,"auth		: %d req/%d reply\n",
					I3_stats.count_auth_mud_req,
					I3_stats.count_auth_mud_reply);
    sprintf_to_char(ch,"startup		: %d req/%d reply\n",
					I3_stats.count_startup_req_3,
					I3_stats.count_startup_reply);
    sprintf_to_char(ch,"oob		: %d req/%d begin/%d end\n",
					I3_stats.count_oob_req,
					I3_stats.count_oob_begin,
					I3_stats.count_oob_end);
    sprintf_to_char(ch,"errors		: %d\n",
					I3_stats.count_error);
    sprintf_to_char(ch,"mudlist		: %d\n",
					I3_stats.count_mudlist);
    sprintf_to_char(ch,"shutdown	: %d\n",
					I3_stats.count_shutdown);
    sprintf_to_char(ch,"ucache		: %d\n",
					I3_stats.count_ucache_update);
}

void i3_setup_channel(CHAR_DATA *ch,char *argument) {
    char localname[MIL];
    char i3_name[MIL];
    char level[MIL];
    I3_CHANNEL *channel,*channel2;
    int ilevel=0;

    if (ch->level<LEVEL_IMMORTAL) {
	send_to_char("This is a privileged command.\n\r",ch);
	return;
    }

    argument=one_argument(argument,i3_name);
    argument=one_argument(argument,localname);
    argument=one_argument(argument,level);

    ilevel=atoi(level);

    if ((channel=find_i3_channel_by_name(i3_name))==NULL) {
	send_to_char("Unknown channel\n\r"
	    "(use {Wi3 chanlist{x to get an overview of the channels available)\n\r",ch);
	return;
    }

    if (localname[0]==0) {
	if (channel->local_name[0]==0) {
	    sprintf_to_char(ch,"Channel %s@%s isn't configured.\n\r",
		channel->i3_name,channel->host_mud);
	    return;
	}
	while (channel->listener) {
	    sprintf_to_char(ch,"Channel %s (%s@%s) has been removed by %s.\n\r",
		channel->local_name,channel->i3_name,channel->host_mud,
		ch->name);
	    destroy_i3_listener(channel,channel->listener);
	}
	wiznet(WIZ_I3_INFO,0,NULL,NULL,
	    "I3 - setup_channel: removing %s as %s@%s",
	    channel->local_name,channel->i3_name,channel->host_mud);
	I3_check_channel(channel);
	free_string(channel->local_name);
	channel->local_name=str_dup("");
    } else {
	if (channel->local_name[0]) {
	    sprintf_to_char(ch,"Channel %s@%s is already known as %s.\n\r",
		channel->i3_name,channel->host_mud,channel->local_name);
	    return;
	}
	if ((channel2=find_i3_channel_by_localname(localname))) {
	    sprintf_to_char(ch,"Channel %s@%s is already known as %s.\n\r",
		channel2->i3_name,channel2->host_mud,channel2->local_name);
	    return;
	}
	free_string(channel->local_name);
	channel->local_name=str_dup(localname);
	channel->local_level=ilevel;
	sprintf_to_char(ch,"%s@%s is now locally known as %s\n\r",
	    channel->i3_name,channel->host_mud,channel->local_name);
	wiznet(WIZ_I3_INFO,0,NULL,NULL,
	    "I3 - setup_channel: setting up to %s@%s as %s",
	    channel->i3_name,channel->host_mud,channel->local_name);
    }

    I3_write_channel_config();
}

void i3_chan_who(CHAR_DATA *ch,char *argument) {
    char channel_name[MIL];
    char mud_name[MIL];
    I3_CHANNEL *channel;
    I3_MUD *mud;

    argument=one_argument(argument,channel_name);
    argument=one_argument(argument,mud_name);
    if ((channel=find_i3_channel_by_localname(channel_name))==NULL) {
	send_to_char("Unknown channel.\n\r"
	    "(use {Wi3 chanlist{x to get an overview of the channels available)\n\r",ch);
	return;
    }
    if ((mud=find_i3_mud_by_name(mud_name))==NULL) {
	send_to_char("Unknown mud.\n\r"
	    "(use {Wi3 mudlist{x to get an overview of the muds available)\n\r",
	    ch);
	return;
    }

    if (mud->status>=0) {
	sprintf_to_char(ch,"%s is marked as down.\n\r",mud->name);
	return;
    }

    I3_send_chan_who(ch,channel,mud);
}


void i3_listen_channel(DESCRIPTOR_DATA *desc,char *argument,bool silent) {
    char channel_name[MIL];
    I3_CHANNEL *channel;
    I3_LISTENER *listener;

    argument=one_argument(argument,channel_name);
    if ((channel=find_i3_channel_by_localname(channel_name))==NULL) {
	if (!silent)
	    send_to_char("Unknown channel.\n\r"
		"(use {Wi3 chanlist{x to get an overview of the channels available)\n\r",desc->character);
	return;
    }
    if ((listener=find_i3_listener_by_descriptor(channel,desc))!=NULL) {
	destroy_i3_listener(channel,listener);
	if (!silent)
	    sprintf_to_char(desc->character,
		"You are now removed from %s (%s@%s)\n",
		channel->local_name,channel->i3_name,channel->host_mud);

	wiznet(WIZ_I3_INFO,0,NULL,NULL,
	    "I3: Removing %s from %s",
	    desc->character?desc->character->name:"(unknown)",channel_name);
	I3_check_channel(channel);
    } else {
	listener=new_i3_listener(channel);
	listener->desc=desc;
	if (!silent)
	    sprintf_to_char(desc->character,
		"You are now subscribed to %s (%s@%s)\n",
		channel->local_name,channel->i3_name,channel->host_mud);
	wiznet(WIZ_I3_INFO,0,NULL,NULL,
	    "I3: Adding %s to %s",
	    desc->character?desc->character->name:"(unknown)",channel_name);
	I3_check_channel(channel);
    }
}

void i3_mudinfo(CHAR_DATA *ch,char *argument) {
    I3_MUD *mud;

    if (argument[0]==0) {
	send_to_char("Which mud do you want information about?\n\r"
	    "(use {Wi3 mudlist{x to get an overview of the muds available)\n\r",
	    ch);
	return;
    }

    if ((mud=find_i3_mud_by_name(argument))==NULL) {
	send_to_char("Unknown mud.\n\r"
	    "(use {Wi3 mudlist{x to get an overview of the muds available)\n\r",ch);
	return;
    }

    sprintf_to_char(ch,"Information about %s\n\r\n\r",mud->name);
    if (mud->status==0)
	send_to_char("Status     : Currently down\n\r",ch);
    else if (mud->status>0)
	sprintf_to_char(ch,
	    "Status     : Currently rebooting, back in %d seconds\n\r",
	    mud->status);
    sprintf_to_char(ch,"MUD port   : %s %d\n\r",mud->ipaddress,mud->player_port);
    sprintf_to_char(ch,"Base mudlib: %s\n\r",mud->base_mudlib);
    sprintf_to_char(ch,"Mudlib     : %s\n\r",mud->mudlib);
    sprintf_to_char(ch,"Driver     : %s\n\r",mud->driver);
    sprintf_to_char(ch,"Type       : %s\n\r",mud->mud_type);
    sprintf_to_char(ch,"Open status: %s\n\r",mud->open_status);
    sprintf_to_char(ch,"Admin      : %s\n\r",mud->admin_email);

    send_to_char("Supports   : ",ch);
    if (mud->tell)	send_to_char("tell, ",ch);
    if (mud->emoteto)	send_to_char("emoteto, ",ch);
    if (mud->who)	send_to_char("who, ",ch);
    if (mud->finger)	send_to_char("finger, ",ch);
    if (mud->locate)	send_to_char("locate, ",ch);
    if (mud->channel)	send_to_char("channel, ",ch);
    if (mud->news)	send_to_char("news, ",ch);
    if (mud->mail)	send_to_char("mail, ",ch);
    if (mud->file)	send_to_char("file, ",ch);
    if (mud->auth)	send_to_char("auth, ",ch);
    if (mud->ucache)	send_to_char("ucache, ",ch);
    send_to_char("\n\r",ch);

    send_to_char("Supports   : ",ch);
    if (mud->smtp)	sprintf_to_char(ch,"smtp (port %d), ",mud->smtp);
    if (mud->http)	sprintf_to_char(ch,"http (port %d), ",mud->http);
    if (mud->ftp)	sprintf_to_char(ch,"ftp (port %d), ",mud->ftp);
    if (mud->pop3)	sprintf_to_char(ch,"pop3 (port %d), ",mud->pop3);
    if (mud->nntp)	sprintf_to_char(ch,"nntp (port %d), ",mud->nntp);
    if (mud->rcp)	sprintf_to_char(ch,"rcp (port %d), ",mud->rcp);
    if (mud->amrcp)	sprintf_to_char(ch,"amrcp (port %d), ",mud->amrcp);
    send_to_char("\n\r",ch);
}

void i3_chanlayout(CHAR_DATA *ch,char *argument) {
    if (ch->level<LEVEL_IMMORTAL) {
	send_to_char("This is a privileged command.\n\r",ch);
	return;
    }
}

void i3_connect(CHAR_DATA *ch,char *argument) {
    if (ch->level<LEVEL_IMMORTAL) {
	send_to_char("This is a privileged command.\n\r",ch);
	return;
    }

    if (I3_is_connected()) {
	send_to_char(
	    "The MUD is already connected to the Intermud-3 router\n\r",ch);
	return;
    }
    send_to_char("Connecting to Intermud-3 router\n\r",ch);
    this_mud.autoconnect=1;	// we don't need it anymore after this.
    I3_main(TRUE);
}

void i3_disconnect(CHAR_DATA *ch,char *argument) {
    if (ch->level<LEVEL_IMMORTAL) {
	send_to_char("This is a privileged command.\n\r",ch);
	return;
    }

    if (!I3_is_connected()) {
	send_to_char(
	    "The MUD isn't connected to the Intermud-3 router\n\r",ch);
	return;
    }
    I3_shutdown(0);
    send_to_char("Disconnected from Intermud-3 router\n\r",ch);
}

void i3_mute(CHAR_DATA *ch,char *argument) {
#ifdef IS_FD
    if (STR_IS_SET(ch->strbit_comm,COMM_I3MUTE)) {
	STR_REMOVE_BIT(ch->strbit_comm,COMM_I3MUTE);
	send_to_char("You are no longer muted from the I3 channels.\n\r",ch);
    } else {
	STR_SET_BIT(ch->strbit_comm,COMM_I3MUTE);
	send_to_char("You are muted from the I3 channels.\n\r",ch);
    }
#else
    if (IS_SET(ch->comm,COMM_I3MUTE)) {
	REMOVE_BIT(ch->comm,COMM_I3MUTE);
	send_to_char("You are no longer muted from the I3 channels.\n\r",ch);
    } else {
	SET_BIT(ch->comm,COMM_I3MUTE);
	send_to_char("You are muted from the I3 channels.\n\r",ch);
    }
#endif
}

void do_i3(CHAR_DATA *ch,char *argument) {
    char arg[MIL];

    if (IS_NPC(ch))
	return;
    if (ch->desc==NULL)
	return;

    argument=one_argument(argument,arg);
    switch (which_keyword(arg,"stats","setup","chanlist","mud","mudlist",
		"listen","chanwho","mudinfo","chanlayout","connect",
		"disconnect","mute",NULL)) {
	case  1: i3_stats(ch,argument);break;
	case  2: i3_setup_channel(ch,argument);break;
	case  3: i3_chanlist(ch,argument);break;
	case  4:
	case  5: i3_mudlist(ch,argument);break;
	case  6: i3_listen_channel(ch->desc,argument,FALSE);break;
	case  7: i3_chan_who(ch,argument);break;
	case  8: i3_mudinfo(ch,argument);break;
	case  9: i3_chanlayout(ch,argument);break;
	case 10: i3_connect(ch,argument);break;
	case 11: i3_disconnect(ch,argument);break;
	case 12: i3_mute(ch,argument);break;
	default:
	    send_to_char("keywords expected:\n\r"
		"stats, chanlist [all] [filter], mudlist [filter],\n\r"
		"setup <channel> <localname> [level], listen <localname>,\n\r"
		"chanwho <channel> <mud>, mudinfo <mud>,\n\r"
		"layout <channel> <message> <emote>, connect, disconnect\n\r"
		"mute\n\r",
		ch);
    }
}


void do_i3who(CHAR_DATA *ch,char *argument) {
    I3_MUD *mud;

    if (IS_NPC(ch)) return;

    if (argument[0]==0) {
	send_to_char("Get an overview of which mud?\n\r"
	    "(use {Wi3 mudlist{x to get an overview of the muds available)\n\r",
	    ch);
	return;
    }

    if ((mud=find_i3_mud_by_name(argument))==NULL) {
	send_to_char("No such mud known.\n\r"
	    "(use {Wi3 mudlist{x to get an overview of the muds available)\n\r",
	    ch);
	return;
    }

    if (mud->status>=0) {
	sprintf_to_char(ch,"%s is marked as down.\n\r",mud->name);
	return;
    }

    if (mud->who==0)
	sprintf_to_char(ch,
	    "%s does not support the 'who' command. Sending anyway.\n\r",
	    mud->name);

    I3_send_who(ch,mud->name);
}

void do_i3locate(CHAR_DATA *ch,char *argument) {

    if (IS_NPC(ch)) return;

    if (argument[0]==0) {
	send_to_char("Locate who?\n\r",ch);
	return;
    }
    I3_send_locate(ch,argument);
}

void do_i3finger(CHAR_DATA *ch,char *argument) {
    char user[MSL],mud[MSL];
    char *ps;
    I3_MUD *pmud;

    if (IS_NPC(ch)) return;

    if (argument[0]==0) {
	send_to_char("Finger who at which mud?\n\r"
	    "(use {Wi3 mudlist{x to get an overview of the muds available)\n\r",
	    ch);
	return;
    }
    if ((ps=strchr(argument,'@'))==NULL) {
	send_to_char("You should specify a person and a mud.\n\r"
	    "(use {Wi3 mudlist{x to get an overview of the muds available)\n\r",
	    ch);
	return;
    }

    ps[0]=0;
    strcpy(user,argument);
    strcpy(mud,ps+1);

    if (user[0]==0 || mud[0]==0) {
	send_to_char("You should specify a person and a mud.\n\r"
	    "(use {Wi3 mudlist{x to get an overview of the muds available)\n\r",
	    ch);
	return;
    }

    if ((pmud=find_i3_mud_by_name(mud))==NULL) {
	send_to_char("No such mud known.\n\r"
	    "(use {Wi3 mudlist{x to get an overview of the muds available)\n\r",
	    ch);
	return;
    }

    if (pmud->status>=0) {
	sprintf_to_char(ch,"%s is marked as down.\n\r",pmud->name);
	return;
    }

    if (pmud->finger==0)
	sprintf_to_char(ch,
	    "%s does not support the 'finger' command. Sending anyway.\n\r",
	    pmud->name);

    I3_send_finger(ch,user,pmud->name);
}

void do_i3tell(CHAR_DATA *ch,char *argument) {
    char to[MIL],*ps;
    char mud[MIL];
    I3_MUD *pmud;

    argument=one_argument(argument,to);
    ps=strchr(to,'@');

    if (to[0]==0 || argument[0]==0 || ps==NULL) {
	send_to_char("You should specify a person and a mud.\n\r"
	    "(use {Wi3 mudlist{x to get an overview of the muds available)\n\r",
	    ch);
	return;
    }

    ps[0]=0;
    ps++;
    strcpy(mud,ps);

    if ((pmud=find_i3_mud_by_name(mud))==NULL) {
	send_to_char("No such mud known.\n\r"
	    "(use {Wi3 mudlist{x to get an overview of the muds available)\n\r",
	    ch);
	return;
    }

    if (pmud->status>=0) {
	sprintf_to_char(ch,"%s is marked as down.\n\r",pmud->name);
	return;
    }

    if (pmud->tell==0)
	sprintf_to_char(ch,
	    "%s does not support the 'tell' command. Sending anyway.\n\r",
	    pmud->name);

    I3_send_tell(ch,to,pmud,argument);
}

void do_i3emote(CHAR_DATA *ch,char *argument) {
    char to[MIL],*ps;
    char mud[MIL];
    I3_MUD *pmud;

    argument=one_argument(argument,to);
    ps=strchr(to,'@');

    if (to[0]==0 || argument[0]==0 || ps==NULL) {
	send_to_char("You should specify a person and a mud.\n\r"
	    "(use {Wi3 mudlist{x to get an overview of the muds available)\n\r",
	    ch);
	return;
    }

    ps[0]=0;
    ps++;
    strcpy(mud,ps);

    if ((pmud=find_i3_mud_by_name(mud))==NULL) {
	send_to_char("No such mud known.\n\r"
	    "(use {Wi3 mudlist{x to get an overview of the muds available)\n\r",
	    ch);
	return;
    }

    if (pmud->status>=0) {
	sprintf_to_char(ch,"%s is marked as down.\n\r",pmud->name);
	return;
    }

    if (pmud->emoteto==0)
	sprintf_to_char(ch,
	    "%s does not support the 'emoteto' command. Sending anyway.\n\r",
	    pmud->name);

    I3_send_emoteto(ch,to,pmud,argument);
}
