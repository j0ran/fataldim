// $Id: fd_i3_finger.c,v 1.13 2001/08/14 05:46:03 edwin Exp $

/*
 * Copyright (c) 2000 Fatal Dimensions
 *
 * See the file "LICENSE" or information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 *
 */

#include <stdio.h>
#include <time.h>
#include <strings.h>
#include "../merc.h"
#include "fd_i3.h"

int I3_send_finger(CHAR_DATA *ch,char *user,char *mud) {

    if (!I3_is_connected())
	return 0;
    I3_stats.count_finger_commands++;

    I3_escape(mud);
    I3_escape(user);

    I3_write_header("finger-req",I3_THISMUD,ch->name,mud,NULL);
    I3_write_buffer("\"");
    I3_write_buffer(user);
    I3_write_buffer("\",})\r");
    I3_send_buffer();
    return 0;
}

/*
   The target mud will return:
    ({
        (string) "finger-reply",
        (int)    5,
        (string) originator_mudname,
        (string) 0,
        (string) target_mudname,
        (string) target_username,
        (string) visname,
        (string) title,
        (string) real_name,
        (string) e_mail,
        (string) loginout_time,
        (int)    idle_time,
        (string) ip_name,
        (string) level,
        (string) extra  // eg, a .plan file, or other info
    })

   A mud may return 0 for any item if they wish to keep the information
   private. In particular, it is suggested that information about players
   (as opposed to wizards) be kept confidential.

   The returned visname should contain the user's visual name.
   loginout_time specifies the (local) time the user logged in (if they
   are currently on) or the time the user logged out. The value should be
   expressed as a string. It should be 0 to indicate no information. The
   idle_time is expressed as an integer number of seconds of idle time.
   If this value is -1, then the user is not logged onto the mud at the
   moment.

   If extra is given, then it should be terminated with a carriage
   return.

({"finger-reply",5,"NoNameMUD",0,"MavEtJu","MavEtJu","Mavetju","Mavetju the Newbie","Mavetju","[Private]","Sat Feb 12 22:47:44 2000",45,0,"Player",0,})
*/

int I3_process_finger_reply(char *s) {
    I3_HEADER header;
    CHAR_DATA *ch;
    char *ps=s,*next_ps;

    I3_get_header(&ps,&header);
    if ((ch=I3_find_user(header.target_username))==NULL) {
	// don't notify since the original message was send by a mud.
	return 0;
    }

    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    sprintf_to_char(ch,"FINGER information for %s at %s\n\r",
	ps,header.originator_mudname);
    ps=next_ps;

    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    sprintf_to_char(ch,"Title     : %s\n\r",ps);
    ps=next_ps;

    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    sprintf_to_char(ch,"Real name : %s\n\r",ps);
    ps=next_ps;

    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    sprintf_to_char(ch,"Email     : %s\n\r",ps);
    ps=next_ps;

    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    sprintf_to_char(ch,"Login at  : %s\n\r",ps);
    ps=next_ps;

    I3_get_field(ps,&next_ps);
    sprintf_to_char(ch,"Idle for  : %s seconds\n\r",ps);
    ps=next_ps;

    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    sprintf_to_char(ch,"IP address: %s\n\r",ps);
    ps=next_ps;

    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    sprintf_to_char(ch,"Level     : %s\n\r",ps);
    ps=next_ps;

    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    sprintf_to_char(ch,"Other info: %s\n\r",ps);

    send_to_char("{x",ch);
    return 0;
}

/*
    ({
        (string) "finger-req",
        (int)    5,
        (string) originator_mudname,
        (string) originator_username,
        (string) target_mudname,
        (string) 0,
        (string) username
    })

*/
int I3_process_finger_req(char *s) {
    I3_HEADER header;
    CHAR_DATA *ch;
    char *ps=s,*next_ps;
    char smallbuf[MSL];

    I3_get_header(&ps,&header);
    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    if ((ch=I3_find_user(ps))==NULL) {
	// perhaps we should be nice to the person...
	I3_send_error(header.originator_mudname,header.originator_username,
	    "unk-user","User is not online right now");
	wiznet(WIZ_I3_INFO,0,NULL,NULL,
	    "I3 - Finger: failed request from %s @ %s for %s.",
	    header.originator_username,header.originator_mudname,ps);
	return 0;
    }

    wiznet(WIZ_I3_INFO,0,NULL,NULL,
	"I3 - Finger: request from %s @ %s for %s.",
	header.originator_username,header.originator_mudname,
	ch->name);
    sprintf_to_char(ch,"You have been i3fingered by %s @ %s\n\r",
	header.originator_username,header.originator_mudname);

    I3_write_header("finger-reply",I3_THISMUD,NULL,
	header.originator_mudname,header.originator_username);
    I3_write_buffer("\"");
    I3_write_buffer(I3_escape(ch->name));
    I3_write_buffer("\",\"");
    I3_write_buffer(I3_escape(ch->name));
    I3_write_buffer(I3_escape(ch->pcdata->title));
    I3_write_buffer("\",\"\",\"");			// real name
    I3_write_buffer(I3_user_email(ch));			// email address
    I3_write_buffer("\",\"");
    I3_write_buffer(ctime_r(&ch->logon,smallbuf));	// online since
    I3_write_buffer("\",");
    sprintf(smallbuf,"%ld",I3_user_idle(ch));
    I3_write_buffer(smallbuf);			// idle since
    I3_write_buffer(",\"");
    I3_write_buffer("[PRIVATE]");			// IP address
    I3_write_buffer("\",\"");
    I3_write_buffer(I3_user_whoname(ch));
    I3_write_buffer("\",\"none\",})\r");		// No extra info
    I3_send_buffer();
    return 0;
}

