// $Id: fd_i3_mudlist.c,v 1.5 2001/08/14 05:46:03 edwin Exp $

/*
 * Copyright (c) 2000 Fatal Dimensions
 *
 * See the file "LICENSE" or information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 *
 */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "../merc.h"
#include "fd_i3.h"

/*
   The router will send this to a mud whenever the mud's list needs to be
   updated. Typically, this will happen once right after login (based on
   the old_mudlist_id that a mud provided in the startup-req-3 packet),
   and then as changes occur within the intermud network. A mud should
   remember the mudlist and its associated mudlist_id across reconnects
   to the router.
    ({
        (string)  "mudlist",
        (int)     5,
        (string)  originator_mudname,     // the router
        (string)  0,
        (string)  target_mudname,
        (string)  0,
        (int)     mudlist_id
        (mapping) info_mapping
    })

   The info_mapping contains mud names as keys and information about each
   mud as the value. This information is specified as an array with the
   following format:
    ({
        (int)     state,
        (string)  ip_addr,
        (int)     player_port,
        (int)     imud_tcp_port,
        (int)     imud_udp_port,
        (string)  mudlib,
        (string)  base_mudlib,
        (string)  driver,
        (string)  mud_type,
        (string)  open_status,
        (string)  admin_email,
        (mapping) services
        (mapping) other_data
    })

   Each record of information should replace any prior record for a
   particular mud. If the mapping's value is zero, then the mud has been
   deleted (it went down and has not come back for a week) from the
   Intermud.

   state is an integer with the following values:
        -1  mud is up
         0  mud is down
         n  mud will be down for n seconds
*/

int I3_process_mudlist(char *s) {
    int i;
    char *ps=s,*next_ps;
    I3_MUD *mud;

    for (i=0;i<6;i++) {
	I3_get_field(ps,&next_ps);
//	printf("Got field %d: .%s.\n",i,ps);
	ps=next_ps;
    }
    ps+=2;

    // info_mapping
    while (1) {
	char *next_ps2;
	// first the name
	I3_get_field(ps,&next_ps);

	mud=find_i3_mud_by_name(ps);
	if (mud==NULL) {
	    mud=new_i3_mud();
	    I3_remove_quotes(&ps);
	    free_string(mud->name);mud->name=str_dup(ps);
	}
//	printf("- name: %s\n",mud->name);

	ps=next_ps;
	I3_get_field(ps,&next_ps2);	// next_ps2 is the end of the services
//	printf("  %s\n",ps);

	if (ps[0]!='0') {
	    ps+=2;

	    I3_get_field(ps,&next_ps);
	    mud->status=atoi(ps);
	    ps=next_ps;

	    I3_get_field(ps,&next_ps);
	    I3_remove_quotes(&ps);
	    free_string(mud->ipaddress);mud->ipaddress=str_dup(ps);
	    ps=next_ps;
//	    printf("  ipaddress: %s\n",mud->ipaddress);

	    I3_get_field(ps,&next_ps);
	    mud->player_port=atoi(ps);
	    ps=next_ps;

	    I3_get_field(ps,&next_ps);
	    mud->imud_tcp_port=atoi(ps);
	    ps=next_ps;

	    I3_get_field(ps,&next_ps);
	    mud->imud_udp_port=atoi(ps);
	    ps=next_ps;

	    I3_get_field(ps,&next_ps);
	    I3_remove_quotes(&ps);
	    free_string(mud->mudlib);mud->mudlib=str_dup(ps);
	    ps=next_ps;
//	    printf("  mudlib: %s\n",mud->mudlib);

	    I3_get_field(ps,&next_ps);
	    I3_remove_quotes(&ps);
	    free_string(mud->base_mudlib);mud->base_mudlib=str_dup(ps);
	    ps=next_ps;
//	    printf("  base_mudlib: %s\n",mud->base_mudlib);

	    I3_get_field(ps,&next_ps);
	    I3_remove_quotes(&ps);
	    free_string(mud->driver);mud->driver=str_dup(ps);
	    ps=next_ps;
//	    printf("  driver: %s\n",mud->driver);

	    I3_get_field(ps,&next_ps);
	    I3_remove_quotes(&ps);
	    free_string(mud->mud_type);mud->mud_type=str_dup(ps);
	    ps=next_ps;
//	    printf("  type: %s\n",mud->mud_type);

	    I3_get_field(ps,&next_ps);
	    I3_remove_quotes(&ps);
	    free_string(mud->open_status);mud->open_status=str_dup(ps);
	    ps=next_ps;
//	    printf("  status: %s\n",mud->open_status);

	    I3_get_field(ps,&next_ps);
	    I3_remove_quotes(&ps);
	    free_string(mud->admin_email);mud->admin_email=str_dup(ps);
	    ps=next_ps;
//	    printf("  admin: %s\n",mud->admin_email);

	    I3_get_field(ps,&next_ps);
//	    printf("  junk: %s\n",ps);
	    ps=next_ps;

	    I3_get_field(ps,&next_ps);
//	    printf("  services: %s\n",ps);
	    ps=next_ps;

	}
	ps=next_ps2;
	if (ps[0]==']')
	    break;
    }


    return 0;
}

