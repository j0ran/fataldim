// $Id: fd_i3_tell.c,v 1.6 2001/08/14 05:46:03 edwin Exp $

/*
 * Copyright (c) 2000 Fatal Dimensions
 *
 * See the file "LICENSE" or information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 *
 */

#include <stdio.h>
#include <time.h>
#include "../merc.h"
#include "fd_i3.h"

int I3_send_tell(CHAR_DATA *ch,char *to,I3_MUD *mud,char *message) {
    if (!I3_is_connected())
	return 0;
    I3_stats.count_tell_commands++;

    I3_escape(to);
    I3_write_header("tell",I3_THISMUD,ch->name,mud->name,to);
    I3_write_buffer("\"");
    I3_write_buffer(ch->name);
    I3_write_buffer("\",\"");
    I3_write_buffer(message);
    I3_write_buffer("\",})\r");
    I3_send_buffer();

    return 0;
}


int I3_process_tell(char *s) {
    char *ps=s,*next_ps;
    CHAR_DATA *ch;
    I3_HEADER header;

    I3_get_header(&ps,&header);
    if ((ch=I3_find_user(header.target_username))==NULL) {
	I3_send_error(header.originator_mudname,header.originator_username,
	    "unk-user","The user you specified is unknown.");
	return 0;
    }

    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    sprintf_to_char(ch,"%s@%s: ",ps,header.originator_mudname);

    ps=next_ps;
    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    send_to_char(ps,ch);
    send_to_char("\n\r",ch);

    send_to_char("{x",ch);
    return 0;
}
