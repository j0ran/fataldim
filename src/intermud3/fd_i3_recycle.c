// $Id: fd_i3_recycle.c,v 1.11 2001/08/14 05:46:03 edwin Exp $

/*
 * Copyright (c) 2000 Fatal Dimensions
 *
 * See the file "LICENSE" or information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 *
 */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "../merc.h"
#include "fd_i3.h"

I3_CHANNEL *I3_channel_list=NULL;
I3_MUD *I3_mud_list=NULL;

I3_CHANNEL *find_i3_channel_by_localname(char *name) {
    I3_CHANNEL *this=I3_channel_list;

    while (this) {
	if (this->local_name[0] &&
	    toupper(this->local_name[0])==toupper(name[0]) &&
	    str_cmp(this->local_name,name)==0)
	    break;
	this=this->next;
    }
    return this;
}

I3_CHANNEL *find_i3_channel_by_name(char *name) {
    I3_CHANNEL *this=I3_channel_list;

    while (this) {
	if (toupper(this->i3_name[0])==toupper(name[0]) &&
	    str_cmp(this->i3_name,name)==0)
	    break;
	this=this->next;
    }
    return this;
}

I3_CHANNEL *new_i3_channel(void) {
    I3_CHANNEL *new;

    new=(I3_CHANNEL *)malloc(sizeof(*new));
    bzero(new,sizeof(I3_CHANNEL));
    new->next=I3_channel_list;
    I3_channel_list=new;
    new->local_name=str_dup("");
    new->host_mud=str_dup("");
    new->i3_name=str_dup("");
    new->layout_m=str_dup("[%s] %s@%s: %s\n\r");// with \n\r since send_to_char
    new->layout_e=str_dup("[%s] %s");		// without \n\r since act()
    return new;
}

void destroy_i3_channel(I3_CHANNEL *channel) {
    if (channel==NULL) {
	bugf("destroy_i3_channel: Null parameter",0);
	return;
    }

    if (channel==I3_channel_list) {
	I3_channel_list=I3_channel_list->next;
    } else {
	I3_CHANNEL *temp=I3_channel_list->next;
	while (temp->next!=channel && temp)
	    temp=temp->next;
	if (temp==NULL) {
	    bugf("destroy_i3_channel: Channel '%s' not found in list",
		channel->local_name);
	    return;
	}
	temp->next=channel->next;
    }
    free_string(channel->local_name);
    free_string(channel->i3_name);
    free_string(channel->host_mud);
    free_string(channel->layout_e);
    free_string(channel->layout_m);

    while (channel->listener)
	destroy_i3_listener(channel,channel->listener);
}

// I3_MUD functions

I3_MUD *find_i3_mud_by_name(char *name) {
    I3_MUD *this=I3_mud_list;

    while (this) {
	if (toupper(this->name[0])==toupper(name[0]) &&
	    str_cmp(this->name,name)==0)
	    break;
	this=this->next;
    }
    return this;
}

I3_MUD *new_i3_mud(void) {
    I3_MUD *new;

    new=(I3_MUD *)malloc(sizeof(I3_MUD));
    bzero(new,sizeof(I3_MUD));
    new->next=I3_mud_list;
    I3_mud_list=new;
    new->name=str_dup("");
    new->ipaddress=str_dup("");
    new->mudlib=str_dup("");
    new->base_mudlib=str_dup("");
    new->driver=str_dup("");
    new->mud_type=str_dup("");
    new->open_status=str_dup("");
    new->admin_email=str_dup("");

    // although only used for this mud, just initialize them.
    new->routerIP=str_dup("");
    new->routerName=str_dup("");
    return new;
}

void destroy_i3_mud(I3_MUD *mud) {
    if (mud==NULL) {
	bugf("destroy_i3_mud: Null parameter");
	return;
    }

    if (mud==I3_mud_list) {
	I3_mud_list=I3_mud_list->next;
    } else {
	I3_MUD *temp=I3_mud_list->next;
	while (temp->next!=mud && temp)
	    temp=temp->next;
	if (temp==NULL) {
	    bugf("destroy_i3_mud: Mud '%s' not found in list",mud->name);
	    return;
	}
	temp->next=mud->next;
    }
    free_string(mud->name);
    free_string(mud->ipaddress);
    free_string(mud->mudlib);
    free_string(mud->base_mudlib);
    free_string(mud->driver);
    free_string(mud->mud_type);
    free_string(mud->open_status);
    free_string(mud->admin_email);
    free_string(mud->routerIP);
    free_string(mud->routerName);
}

// listeners

I3_LISTENER *new_i3_listener(I3_CHANNEL *channel) {
    I3_LISTENER *new;

    new=(I3_LISTENER *)malloc(sizeof(I3_LISTENER));
    bzero(new,sizeof(I3_LISTENER));
    new->next=channel->listener;
    channel->listener=new;
    return new;
}

void destroy_i3_listener(I3_CHANNEL *channel,I3_LISTENER *listener) {
    if (channel==NULL) {
	bugf("destroy_i3_listener: Null parameter for channel");
	return;
    }
    if (listener==NULL) {
	bugf("destroy_i3_listener: Null parameter for listener");
	return;
    }

    if (channel->listener==listener) {
	channel->listener=channel->listener->next;
    } else {
	I3_LISTENER *temp=channel->listener;
	while (temp->next!=listener && temp)
	    temp=temp->next;
	if (temp==NULL) {
	    bugf("destroy_i3_listener: Listener '' not found in list");
	    return;
	}
	temp->next=listener->next;
    }
}

I3_LISTENER *find_i3_listener_by_char(I3_CHANNEL *channel,CHAR_DATA *ch) {
    I3_LISTENER *this=channel->listener;

    while (this && this->desc!=ch->desc)
	this=this->next;

    return this;
}

I3_LISTENER *find_i3_listener_by_descriptor(I3_CHANNEL *channel,DESCRIPTOR_DATA *desc) {
    I3_LISTENER *this=channel->listener;

    while (this && this->desc!=desc)
	this=this->next;

    return this;
}
