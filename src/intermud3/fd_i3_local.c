// $Id: fd_i3_local.c,v 1.3 2001/07/02 07:46:07 edwin Exp $

/*
 * Copyright (c) 2000 Fatal Dimensions
 *
 * See the file "LICENSE" or information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 *
 */

#include <time.h>
#include <stdio.h>

#include "../merc.h"
#include "fd_i3.h"
#include "../fd_property.h"

// These are the callback-functions for I3 for local mud enhancements

// returns the time the user is idle in seconds
long I3_user_idle(CHAR_DATA *ch) {
    return time(NULL)-ch->pcdata->idle;
}

char I3smallbuf[MIL];
char *I3_user_email(CHAR_DATA *ch) {
    if (GetCharProperty(ch,PROPERTY_STRING,"email",I3smallbuf)) {
	return I3smallbuf;
    } else {
	return "none";
    }
}

char *I3_user_whoname(CHAR_DATA *ch) {
    if (ch->pcdata->whoname[0])
	return ch->pcdata->whoname;
    else
	return "Player";
}
