// $Id: fd_i3_support.c,v 1.12 2004/03/04 10:44:17 jodocus Exp $

/*
 * Copyright (c) 2000 Fatal Dimensions
 *
 * See the file "LICENSE" or information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 *
 */

#include <stdio.h>
#include <strings.h>
#include <stdlib.h>
#include <time.h>
#include "../merc.h"
#include "fd_i3.h"


void I3_startup_packet(void) {
    char s[10];

    if (!I3_is_connected())
	return;
    I3_stats.count_startup_req_3++;

    I3_write_header("startup-req-3",this_mud.name,NULL,this_mud.routerName,0);

    I3_write_buffer("2286,523060,57,4000,0,0,\"");
    I3_write_buffer(this_mud.mudlib);
    I3_write_buffer("\",\"");
    I3_write_buffer(this_mud.base_mudlib);
    I3_write_buffer("\",\"");
    I3_write_buffer(this_mud.driver);
    I3_write_buffer("\",\"");
    I3_write_buffer(this_mud.mud_type);
    I3_write_buffer("\",\"");
    I3_write_buffer(this_mud.open_status);
    I3_write_buffer("\",\"");
    I3_write_buffer(this_mud.admin_email);
    I3_write_buffer("\",");

    I3_write_buffer("([\"emoteto\":");
    I3_write_buffer(this_mud.emoteto?"1":"0");
    I3_write_buffer(",\"news\":");
    I3_write_buffer(this_mud.news?"1":"0");
    I3_write_buffer(",\"ucache\":");
    I3_write_buffer(this_mud.ucache?"1":"0");
    I3_write_buffer(",\"auth\":");
    I3_write_buffer(this_mud.auth?"1":"0");
    I3_write_buffer(",\"ftp\":");
    sprintf(s,"%d",this_mud.ftp);
    I3_write_buffer(s);
    I3_write_buffer(",\"nntp\":");
    sprintf(s,"%d",this_mud.nntp);
    I3_write_buffer(s);
    I3_write_buffer(",\"rcp\":");
    sprintf(s,"%d",this_mud.rcp);
    I3_write_buffer(s);
    I3_write_buffer(",\"amrcp\":");
    sprintf(s,"%d",this_mud.amrcp);
    I3_write_buffer(s);
    I3_write_buffer(",\"tell\":");
    I3_write_buffer(this_mud.tell?"1":"0");
    I3_write_buffer(",\"mail\":");
    I3_write_buffer(this_mud.mail?"1":"0");
    I3_write_buffer(",\"file\":");
    I3_write_buffer(this_mud.file?"1":"0");
    I3_write_buffer(",\"http\":");
    sprintf(s,"%d",this_mud.http);
    I3_write_buffer(s);
    I3_write_buffer(",\"smtp\":");
    sprintf(s,"%d",this_mud.smtp);
    I3_write_buffer(s);
    I3_write_buffer(",\"pop3\":");
    sprintf(s,"%d",this_mud.pop3);
    I3_write_buffer(s);
    I3_write_buffer(",\"locate\":");
    I3_write_buffer(this_mud.locate?"1":"0");
    I3_write_buffer(",\"finger\":");
    I3_write_buffer(this_mud.finger?"1":"0");
    I3_write_buffer(",\"channel\":");
    I3_write_buffer(this_mud.channel?"1":"0");
    I3_write_buffer(",\"who\":");
    I3_write_buffer(this_mud.who?"1":"0");
    I3_write_buffer(",]),0,})\r");

    I3_send_buffer();

    wiznet(WIZ_I3_INFO,0,NULL,NULL,
	"I3 - sending startup_packet to %s",this_mud.routerName);
}


/*
startup-reply

   This packet will be delivered to a mud for three conditions: in
   response to a startup-req packet, when the router wishes the mud to
   connect to a different router, or when the set of routers change for
   some reason.
    ({
        (string)   "startup-reply",
        (int)      5,
        (string)   originator_mudname,     // the router
        (string)   0,
        (string)   target_mudname,
        (string)   0,
        (string *) router_list,
        (int)      password
    })

   The router_list is an array representing an ordered list of routers to
   use. The first element should be the router that the mud should use.
   Typically, this will be the router that the mud initially connected
   to. If not, however, then the mud should close the connection and
   reopen to the designated router. The list should be saved and used in
   case of failure to connect to a router. Each element in the list is an
   array of two elements; the first element is the router name, the
   second element is the router's address in the following format:
   "ip.ad.re.ss portnum". Note that this address can be passed to MudOS's
   socket_connect() function. For example: ({ "*nightmare",
   "199.199.122.10 9000" }).

   The first router specified in the list will be the mud's preferred
   router. Future initial connections and startup-req packets will go to
   that router until told otherwise.

*/

int I3_process_startup_reply(char *s) {
    int i;
    char *ps=s,*next_ps;
    I3_HEADER header;

    I3_get_header(&ps,&header);

    for (i=0;i<2;i++) {
	I3_get_field(ps,&next_ps);
//	printf("Got field %d: .%s.\n",i,ps);
	ps=next_ps;
    }
    wiznet(WIZ_I3_INFO,0,NULL,NULL,
	"I3 - received startup_reply from %s",header.originator_mudname);
    return 0;
}



/*
   The router will send this to a mud whenever the mud's list needs to be
   updated. Typically, this will happen once right after login (based on
   the old_mudlist_id that a mud provided in the startup-req-3 packet),
   and then as changes occur within the intermud network. A mud should
   remember the mudlist and its associated mudlist_id across reconnects
   to the router.
    ({
        (string)  "mudlist",
        (int)     5,
        (string)  originator_mudname,     // the router
        (string)  0,
        (string)  target_mudname,
        (string)  0,
        (int)     mudlist_id
        (mapping) info_mapping
    })

   The info_mapping contains mud names as keys and information about each
   mud as the value. This information is specified as an array with the
   following format:
    ({
        (int)     state,
        (string)  ip_addr,
        (int)     player_port,
        (int)     imud_tcp_port,
        (int)     imud_udp_port,
        (string)  mudlib,
        (string)  base_mudlib,
        (string)  driver,
        (string)  mud_type,
        (string)  open_status,
        (string)  admin_email,
        (mapping) services
        (mapping) other_data
    })

   Each record of information should replace any prior record for a
   particular mud. If the mapping's value is zero, then the mud has been
   deleted (it went down and has not come back for a week) from the
   Intermud.

   state is an integer with the following values:
        -1  mud is up
         0  mud is down
         n  mud will be down for n seconds
*/

int I3_process_mudlist(char *s) {
    char *ps=s,*next_ps;
    I3_MUD *mud;
    I3_HEADER header;

    I3_get_header(&ps,&header);
    I3_get_field(ps,&next_ps);	// password
    ps=next_ps;
    ps+=2;

    // info_mapping
    while (1) {
	char *next_ps2;
	// first the name
	I3_get_field(ps,&next_ps);
	I3_remove_quotes(&ps);
	mud=find_i3_mud_by_name(ps);
	if (mud==NULL) {
	    mud=new_i3_mud();
	    free_string(mud->name);mud->name=str_dup(ps);
	}
//	printf("- name: %s\n",mud->name);

	ps=next_ps;
	I3_get_field(ps,&next_ps2);	// next_ps2 is the end of the services
//	printf("  %s\n",ps);

	if (ps[0]!='0') {		// mapping==0 ? mud deleted.
	    ps+=2;

	    I3_get_field(ps,&next_ps);
	    mud->status=atoi(ps);
	    ps=next_ps;

	    I3_get_field(ps,&next_ps);
	    I3_remove_quotes(&ps);
	    free_string(mud->ipaddress);mud->ipaddress=str_dup(ps);
	    ps=next_ps;
//	    printf("  ipaddress: %s\n",mud->ipaddress);

	    I3_get_field(ps,&next_ps);
	    mud->player_port=atoi(ps);
	    ps=next_ps;

	    I3_get_field(ps,&next_ps);
	    mud->imud_tcp_port=atoi(ps);
	    ps=next_ps;

	    I3_get_field(ps,&next_ps);
	    mud->imud_udp_port=atoi(ps);
	    ps=next_ps;

	    I3_get_field(ps,&next_ps);
	    I3_remove_quotes(&ps);
	    free_string(mud->mudlib);mud->mudlib=str_dup(ps);
	    ps=next_ps;
//	    printf("  mudlib: %s\n",mud->mudlib);

	    I3_get_field(ps,&next_ps);
	    I3_remove_quotes(&ps);
	    free_string(mud->base_mudlib);mud->base_mudlib=str_dup(ps);
	    ps=next_ps;
//	    printf("  base_mudlib: %s\n",mud->base_mudlib);

	    I3_get_field(ps,&next_ps);
	    I3_remove_quotes(&ps);
	    free_string(mud->driver);mud->driver=str_dup(ps);
	    ps=next_ps;
//	    printf("  driver: %s\n",mud->driver);

	    I3_get_field(ps,&next_ps);
	    I3_remove_quotes(&ps);
	    free_string(mud->mud_type);mud->mud_type=str_dup(ps);
	    ps=next_ps;
//	    printf("  type: %s\n",mud->mud_type);

	    I3_get_field(ps,&next_ps);
	    I3_remove_quotes(&ps);
	    free_string(mud->open_status);mud->open_status=str_dup(ps);
	    ps=next_ps;
//	    printf("  status: %s\n",mud->open_status);

	    I3_get_field(ps,&next_ps);
	    I3_remove_quotes(&ps);
	    free_string(mud->admin_email);mud->admin_email=str_dup(ps);
	    ps=next_ps;
//	    printf("  admin: %s\n",mud->admin_email);

	    I3_get_field(ps,&next_ps);
//	    printf("  services: %s\n",ps);
	    ps+=2;
	    while (1) {
		char *next_ps2;
		char key[MIL];

		if (ps[0]==']')
		    break;

		I3_get_field(ps,&next_ps2);
		I3_remove_quotes(&ps);
		strcpy(key,ps);
		ps=next_ps2;
		I3_get_field(ps,&next_ps2);

//		printf("  key: %s value %s\n",key,ps);
		switch (key[0]) {
		case 'a':
		    if (strcmp(key,"auth")==0)
			{mud->auth=ps[0]=='0'?0:1;break;}
		    if (strcmp(key,"amrcp")==0) {mud->amrcp=atoi(ps);break;}
		    break;
		case 'c':
		    if (strcmp(key,"channel")==0)
			{mud->channel=ps[0]=='0'?0:1;break;}
		    break;
		case 'e':
		    if (strcmp(key,"emoteto")==0)
			{mud->emoteto=ps[0]=='0'?0:1;break;}
		    break;
		case 'f':
		    if (strcmp(key,"file")==0)
			{mud->file=ps[0]=='0'?0:1;break;}
		    if (strcmp(key,"finger")==0)
			{mud->finger=ps[0]=='0'?0:1;break;}
		    if (strcmp(key,"ftp")==0) {mud->ftp=atoi(ps);break;}
		    break;
		case 'h':
		    if (strcmp(key,"http")==0) {mud->http=atoi(ps);break;}
		    break;
		case 'l':
		    if (strcmp(key,"locate")==0)
			{mud->locate=ps[0]=='0'?0:1;break;}
		    break;
		case 'm':
		    if (strcmp(key,"mail")==0)
			{mud->mail=ps[0]=='0'?0:1;break;}
		    break;
		case 'n':
		    if (strcmp(key,"news")==0)
			{mud->news=ps[0]=='0'?0:1;break;}
		    if (strcmp(key,"nntp")==0) {mud->nntp=atoi(ps);break;}
		    break;
		case 'p':
		    if (strcmp(key,"pop3")==0) {mud->pop3=atoi(ps);break;}
		    break;
		case 'r':
		    if (strcmp(key,"rcp")==0) {mud->rcp=atoi(ps);break;}
		    break;
		case 's':
		    if (strcmp(key,"smtp")==0) {mud->smtp=atoi(ps);break;}
		    break;
		case 't':
		    if (strcmp(key,"tell")==0)
			{mud->tell=ps[0]=='0'?0:1;break;}
		    break;
		case 'u':
		    if (strcmp(key,"ucache")==0)
			{mud->ucache=ps[0]=='0'?0:1;break;}
		    break;
		case 'w':
		    if (strcmp(key,"who")==0)
			{mud->who=ps[0]=='0'?0:1;break;}
		    break;
		//default:
		    //printf("Unknown service: %s %s %s\n",mud->name,key,ps);
		}


		ps=next_ps2;
		if (ps[0]==']')
		    break;
	    }
	    ps=next_ps;

	    I3_get_field(ps,&next_ps);
//	    printf("  services: %s\n",ps);
	    ps=next_ps;

	}
	ps=next_ps2;
	if (ps[0]==']')
	    break;
    }


    return 0;
}


void I3_send_error(char *mud,char *user,char *code,char *message) {
    if (!I3_is_connected())
	return;
    I3_write_header("error",I3_THISMUD,0,mud,user);
    I3_write_buffer("\"");
    I3_write_buffer(code);
    I3_write_buffer("\",\"");
    I3_write_buffer(message);
    I3_write_buffer("\",0,})\r");
    I3_send_buffer();
}

/*
({"error",5,"FF-Test",0,"NoNameMUD","mavetju","unk-type","type 'locate-req' is unrecognized",({"locate-req",5,"NoNameMUD","mavetju",0,0,"blaat",}),}).
*/
void I3_process_error(char *s) {
    CHAR_DATA *ch;
    I3_HEADER header;
    char *next_ps,*ps=s;
    char type[MSL],message[MSL],error[MSL];

    I3_get_header(&ps,&header);

    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    strcpy(type,ps);
    ps=next_ps;

    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    strcpy(message,ps);

    sprintf(error,"I3 - Error: from %s to %s@%s\n\r%s: %s",
	header.originator_mudname,
	header.target_username,header.target_mudname,
	type,message);
    if ((ch=I3_find_user(header.target_username))==NULL) {
	wiznet(WIZ_I3_ERRORS,0,NULL,NULL,error);
    } else {
	send_to_char(error,ch);
	send_to_char("\n\r",ch);
    }
}


int I3_send_shutdown(int delay) {
    char s[MIL];

    if (!I3_is_connected())
	return 0;
    I3_stats.count_shutdown++;

    I3_write_header("shutdown",I3_THISMUD,NULL,I3_ROUTER_NAME,NULL);
    sprintf(s,"%d",delay);
    I3_write_buffer(s);
    I3_write_buffer(",})\r");
    I3_send_buffer();

    return 0;
}
