// $Id: fd_i3_config.c,v 1.9 2001/08/14 05:46:03 edwin Exp $

/*
 * Copyright (c) 2000 Fatal Dimensions
 *
 * See the file "LICENSE" or information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 *
 */

#include <stdio.h>
#include <strings.h>
#include <time.h>
#include <stdlib.h>
#include "../merc.h"
#include "fd_i3.h"

I3_MUD this_mud;
char *I3_THISMUD;
char *I3_ROUTER_NAME;

#define KEYWORD_B(needed,key,value,struct,var) 			\
	if (!str_cmp((key),(needed))) {				\
	    struct.var=value[0]=='0'?0:1;			\
	    found=TRUE;						\
	    break;						\
	}
#define KEYWORD_I(needed,key,value,struct,var) 			\
	if (!str_cmp((key),(needed))) {				\
	    struct.var=atoi(value);				\
	    found=TRUE;						\
	    break;						\
	}
#define KEYWORD_S(needed,key,value,struct,var) 			\
	if (!str_cmp((key),(needed))) {				\
	    struct.var=str_dup((value));			\
	    found=TRUE;						\
	    break;						\
	}

void I3_read_config(void) {
    FILE *fin;
    char line[80],*ps;
    bool found;

    if ((fin=fopen(mud_data.i3_config,"rt"))==NULL) {
	printf("Can't open configuration file: %s\n",mud_data.i3_config);
	exit(0);
    }

    this_mud.ipaddress=str_dup("127.0.0.1");
    this_mud.status=-1;
    this_mud.autoconnect=0;

    while (fgets(line,sizeof(line),fin)) {
	if (line[0]=='#')
	    continue;

	if ((ps=strchr(line,'='))==NULL) {
	    printf("Invalid configuration line: %s\n",line);
	    exit(0);
	}

	ps[0]=0;
	ps++;
	ps[strlen(ps)-1]=0;

	found=FALSE;
	switch (line[0]) {
	case 'a':
		KEYWORD_S("adminemail",line,ps,this_mud,admin_email);
		KEYWORD_I("amrcp",line,ps,this_mud,amrcp);
		KEYWORD_B("auth",line,ps,this_mud,auth);
		KEYWORD_B("autoconnect",line,ps,this_mud,autoconnect);
		break;
	case 'b':
		KEYWORD_S("basemudlib",line,ps,this_mud,base_mudlib);
		break;
	case 'c':
		KEYWORD_B("channel",line,ps,this_mud,channel);
		break;
	case 'd':
		KEYWORD_S("driver",line,ps,this_mud,driver);
		break;
	case 'e':
		KEYWORD_B("emoteto",line,ps,this_mud,emoteto);
		break;
	case 'f':
		KEYWORD_B("file",line,ps,this_mud,file);
		KEYWORD_B("finger",line,ps,this_mud,finger);
		KEYWORD_I("ftp",line,ps,this_mud,ftp);
		break;
	case 'h':
		KEYWORD_I("http",line,ps,this_mud,http);
		break;
	case 'l':
		KEYWORD_B("locate",line,ps,this_mud,locate);
		break;
	case 'm':
		KEYWORD_B("mail",line,ps,this_mud,mail);
		KEYWORD_S("mudlib",line,ps,this_mud,mudlib);
		KEYWORD_I("mudport",line,ps,this_mud,player_port);
		KEYWORD_S("mudtype",line,ps,this_mud,mud_type);
		break;
	case 'n':
		KEYWORD_B("news",line,ps,this_mud,news);
		KEYWORD_I("nntp",line,ps,this_mud,nntp);
		break;
	case 'o':
		KEYWORD_S("openstatus",line,ps,this_mud,open_status);
		break;
	case 'p':
		KEYWORD_I("port",line,ps,this_mud,routerPort);
		KEYWORD_I("pop3",line,ps,this_mud,pop3);
		break;
	case 'r':
		KEYWORD_I("rcp",line,ps,this_mud,rcp);
		KEYWORD_S("router",line,ps,this_mud,routerIP);
		KEYWORD_S("routername",line,ps,this_mud,routerName);
		break;
	case 's':
		KEYWORD_I("smtp",line,ps,this_mud,smtp);
		break;
	case 't':
		KEYWORD_B("tell",line,ps,this_mud,tell);
		KEYWORD_S("thismud",line,ps,this_mud,name);
		break;
	case 'u':
		KEYWORD_B("ucache",line,ps,this_mud,ucache);
		break;
	case 'w':
		KEYWORD_B("who",line,ps,this_mud,who);
		break;
	}

	if (!found) {
	    printf("Bad keyword: %s %s\n\r",line,ps);
	}
    }

    I3_THISMUD=this_mud.name;
    I3_ROUTER_NAME=this_mud.routerName;

    fclose(fin);
}


void I3_read_channel_config(void) {
    FILE *fin;
    char s[100];
    I3_CHANNEL *channel;

    if ((fin=fopen("fd_i3.channels","rt"))==NULL) {
	logf("[0] I3_read_channel_config(): fopen(%s): %s","fd_i3.channels",ERROR);
	return;
    }

    fgets(s,sizeof(s),fin);s[strlen(s)-1]=0;
    while (!feof(fin)) {
	if ((channel=find_i3_channel_by_name(s))==NULL)
	    channel=new_i3_channel();

	free_string(channel->i3_name);
	channel->i3_name=str_dup(s);

	fgets(s,sizeof(s),fin);s[strlen(s)-1]=0;
	free_string(channel->host_mud);
	channel->host_mud=str_dup(s);

	fgets(s,sizeof(s),fin);s[strlen(s)-1]=0;
	free_string(channel->local_name);
	channel->local_name=str_dup(s);

	fgets(s,sizeof(s),fin);s[strlen(s)-1]=0;
	free_string(channel->layout_m);
	strcat(s,"\n\r");
	channel->layout_m=str_dup(s);

	fgets(s,sizeof(s),fin);s[strlen(s)-1]=0;
	free_string(channel->layout_e);
	channel->layout_e=str_dup(s);

	channel->connected=0;

	logf("[0] I3_read_channel_config(): Auto-subscribing to %s (%s@%s)",
	    channel->local_name,channel->i3_name,channel->host_mud);

	if (fgets(s,sizeof(s),fin))
	    s[strlen(s)-1]=0;
    }
    fclose(fin);
}

void I3_write_channel_config(void) {
    FILE *fout;
    I3_CHANNEL *channel;
    char layout_m[MIL];

    if ((fout=fopen("fd_i3.channels","wt"))==NULL) {
	logf("[0] I3_write_channel_config(): fopen(%s): %s","fd_i3.channels",ERROR);
	return;
    }

    for (channel=I3_channel_list;channel;channel=channel->next) {
	if (channel->local_name && channel->local_name[0]) {
	    strcpy(layout_m,channel->layout_m);
	    layout_m[strlen(layout_m)-2]=0;
	    fprintf(fout,"%s\n%s\n%s\n%s\n%s\n",
		channel->i3_name,
		channel->host_mud,
		channel->local_name,
		layout_m,
		channel->layout_e);
	}
    }
    fclose(fout);
}
