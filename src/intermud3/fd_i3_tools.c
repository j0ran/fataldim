// $Id: fd_i3_tools.c,v 1.5 2001/08/14 05:46:03 edwin Exp $

/*
 * Copyright (c) 2000 Fatal Dimensions
 *
 * See the file "LICENSE" or information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 *
 */

#include <stdio.h>
#include <time.h>
#include "../merc.h"
#include "fd_i3.h"

#define COUNTER 3
void i3_printstats(int i) {
    I3_MUD *mud;
    I3_CHANNEL *channel;
    static counter=-1;

    counter++;

    if (counter%COUNTER==0) {
    printf("messages	: %d (%d unknown)\n",
					I3_stats.count_total,
					I3_stats.count_unknown);
    printf("private		: %d tells/%d emotes\n",
					I3_stats.count_tell,
					I3_stats.count_emoteto);
    printf("who		: %d/%d\n",	I3_stats.count_who_req,
					I3_stats.count_who_reply);
    printf("finger		: %d/%d\n",
					I3_stats.count_finger_req,
					I3_stats.count_finger_reply);
    printf("locate		: %d/%d\n",
					I3_stats.count_locate_req,
					I3_stats.count_locate_reply);
    printf("channels	: %d m/%d e/%d t\n",
					I3_stats.count_channel_m,
					I3_stats.count_channel_e,
					I3_stats.count_channel_t);
    printf("		  %d list-reply/%d add/%d remove\n",
					I3_stats.count_chanlist_reply,
					I3_stats.count_channel_add,
					I3_stats.count_channel_remove);
    printf("		  filter %d/%d\n",
					I3_stats.count_channel_filter_req,
					I3_stats.count_channel_filter_reply);
    printf("		  who %d/%d\n", I3_stats.count_channel_who_req,
					I3_stats.count_channel_who_reply);
    printf("		  user %d/%d\n",I3_stats.count_chan_user_req,
					I3_stats.count_chan_user_reply);
    printf("news		: %d read/%d post/%d grplist_req\n",
					I3_stats.count_news_read_req,
					I3_stats.count_news_post_req,
					I3_stats.count_news_grplist_req);
    printf("mail		: %d/%d\n",
					I3_stats.count_mail,
					I3_stats.count_mail_ack);
    printf("filelist	: %d/%d\n",	I3_stats.count_file_list_req,
					I3_stats.count_file_list_reply);
    printf("file		: %d put/%d getreq/%d getreply\n",
					I3_stats.count_file_put,
					I3_stats.count_file_get_req,
					I3_stats.count_file_get_reply);
    printf("auth		: %d/%d\n",
					I3_stats.count_auth_mud_req,
					I3_stats.count_auth_mud_reply);
    printf("startup		: %d/%d\n",
					I3_stats.count_startup_req_3,
					I3_stats.count_startup_reply);
    printf("oob		: %d req/%d begin/%d end\n",
					I3_stats.count_oob_req,
					I3_stats.count_oob_begin,
					I3_stats.count_oob_end);
    printf("errors		: %d\n",I3_stats.count_error);
    printf("mudlist		: %d\n",I3_stats.count_mudlist);
    printf("shutdown	: %d\n",	I3_stats.count_shutdown);
    printf("ucache		: %d\n",I3_stats.count_ucache_update);
    printf("\n");
    }

    if (counter%COUNTER==1) {
	mud=I3_mud_list;
	while (mud) {
	    printf("%s\n",mud->name);
	    mud=mud->next;
	}
	printf("\n");
	return;
    }

    if (counter%COUNTER==2) {
	channel=I3_channel_list;
	while (channel) {
	    printf("%s @ %s\n",channel->i3_name,channel->host_mud);
	    channel=channel->next;
	}
	printf("\n");
	return;
    }
}
