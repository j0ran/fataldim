// $Id: fd_i3_main.c,v 1.21 2001/08/14 05:46:03 edwin Exp $

/*
 * Copyright (c) 2000 Fatal Dimensions
 *
 * See the file "LICENSE" or information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 *
 */

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <stdarg.h>
#include <ctype.h>
#include "../merc.h"
#include "fd_i3.h"

char I3_input_buffer[256*256];
char I3_output_buffer[256*256];
long I3_input_pointer=0;
long I3_output_pointer=4;
char I3_currentpacket[256*256];
int  I3_socket=0;
I3_STATS I3_stats;

/*
 * Setup a TCP session to the router. Returns socket or <0 if failed.
 *
 */
int I3_connection_open(char *host, int port) {
    struct sockaddr_in s_addr;
    struct hostent *hostp;
    u_long ip_addr;

    logf("[0] I3_connection_open(): Attempting connect to %s on port %d", host, port);

    I3_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (I3_socket < 0) {
	bugf("Cannot create socket (host %s, port %d)",host,port);
	return -1;
    }

    memset(&s_addr, 0, sizeof(s_addr));
    s_addr.sin_family = AF_INET;
    if ((ip_addr = inet_addr(host)) != INADDR_NONE) {
	memcpy(&s_addr.sin_addr, &ip_addr, sizeof(ip_addr));
    } else {
	hostp = gethostbyname(host);
	if (!hostp) {
	    bugf("Cannot resolve hostname (%s)",host);
	    return -1;
	}
	memcpy(&s_addr.sin_addr, hostp->h_addr, hostp->h_length);
    }
    s_addr.sin_port = htons(port);
    if (connect(I3_socket, (struct sockaddr *)&s_addr, sizeof(s_addr)) < 0) {
	bugf("Cannot connect to router");
	return -1;
    }
    logf("[0] I3_connection_open(): Connected to router.");
    return I3_socket;
}

/*
 * Close the socket to the router. Should be called via I3_shutdown.
 *
 */
void I3_connection_close(void) {
    if (I3_socket>0)
	close(I3_socket);
}

/*
 * Write a string into the send-buffer. Does not yet send it.
 *
 */
void I3_write_buffer(char *msg) {
    long newsize=I3_output_pointer+strlen(msg);

    if (newsize>256*256-1) {
	bugf("I3 - I3_write_buffer: buffer too large (would become %d bytes)",newsize);
	return;
    }
    strcpy(I3_output_buffer+I3_output_pointer,msg);
//  printf("Buffering: %ld %ld >%s<\n",I3_output_pointer,newsize,I3_output);
    I3_output_pointer=newsize;
}

/*
 * Writes the string into the socket, prefixed by the size.
 *
 */
void I3_write_packet(char *msg) {
    int oldsize,size;
    char *s=I3_output_buffer;

    oldsize=size=strlen(msg+4);
    s[3]=size%256;size>>=8;
    s[2]=size%256;size>>=8;
    s[1]=size%256;size>>=8;
    s[0]=size%256;
    write(I3_socket,msg,oldsize+4);
}

/*
 * Writes the send-buffer into the socket.
 *
 */
void I3_send_buffer() {
//  printf("Sending: >%s<\n",I3_output);
    I3_write_packet(I3_output_buffer);
    I3_output_pointer=4;
}


/*
 * Checks for new data in the socket and perhaps for old TCP packets which
 * hold more than one I3 packet. Returns 1 if a full I3 packet is
 * available.
 *
 */
int I3_local_loop(void) {
    int ret;
    long size;
    fd_set in_set,out_set,exc_set;
    static struct timeval null_time;

    if (I3_socket<=0)
	return 0;

    FD_ZERO( &in_set  );
    FD_ZERO( &out_set );
    FD_ZERO( &exc_set );
    FD_SET( I3_socket , &in_set );
    if (select(I3_socket+1, &in_set, &out_set, &exc_set, &null_time ) < 0 ) {
	perror( "I3_local_loop: select" );
	return 1;
    }
    if (!FD_ISSET(I3_socket,&in_set)) {
	if (I3_input_pointer==0)	// perhaps we have enough now
	    return 0;
	ret=0;
    } else {
	ret=read(I3_socket,I3_input_buffer+I3_input_pointer,MSL);
	if (ret<=0)
	    return -1;
	I3_input_pointer+=ret;
    }
    memcpy(&size,I3_input_buffer,4);
    size=ntohl(size);
//  printf("Read %d bytes, have %d bytes, need %ld bytes\n",
//	ret,I3_input_pointer-4,size);
    if (size<=I3_input_pointer-4)
	return 1;
    return 0;
}

/*
 * Read one I3 packet into the I3_input_buffer
 *
 */
int I3_read_packet(void) {
    long size;

    memcpy(&size,I3_input_buffer,4);
    size=ntohl(size);
//  printf("read_packet, %ld bytes\n",size);
//  if (I3_currentpacket)
//	free(I3_currentpacket);
//  I3_currentpacket=(char *)malloc(size+10);
    memcpy(I3_currentpacket,I3_input_buffer+4,size);
    I3_currentpacket[size+1]=0;
//  printf("got %s\n",I3_currentpacket);
    memcpy(I3_input_buffer,I3_input_buffer+size+4,I3_input_pointer-size-4);
    I3_input_pointer-=size+4;
    return 1;
}

/*
 * Gets the next I3 field, that is when the amount of {[("'s and
 * ")]}'s match each other when a , is read. It's not foolproof, it
 * should honestly be some kind of statemachine, which does error-
 * checking. Right now I trust the I3-router to send proper packets
 * only. How naive :-)
 *
 * ps will point to the beginning of the next field.
 *
 */
char *I3_get_field(char *packet,char **ps) {
    int count[256];
    char has_apostrophe=0,has_backslash=0;
    char foundit=0;

    bzero(count,sizeof(count));

    *ps=packet;
    while (1) {
	switch (*ps[0]) {
	    case '{': if (!has_apostrophe) count['{']++;break;
	    case '}': if (!has_apostrophe) count['}']++;break;
	    case '[': if (!has_apostrophe) count['[']++;break;
	    case ']': if (!has_apostrophe) count[']']++;break;
	    case '(': if (!has_apostrophe) count['(']++;break;
	    case ')': if (!has_apostrophe) count[')']++;break;
	    case '\\':
		if (has_backslash)
		    has_backslash=0;
		else
		    has_backslash=1;
		break;
	    case '"':
		if (has_backslash) {
		    has_backslash=0;
		} else {
		    if (has_apostrophe)
			has_apostrophe=0;
		    else
			has_apostrophe=1;
		}
		break;
	    case ',':
	    case ':':
		if (has_apostrophe)
		    break;
		if (has_backslash)
		    break;
		if (count['{']!=count['}'])
		    break;
		if (count['[']!=count[']'])
		    break;
		if (count['(']!=count[')'])
		    break;
		foundit=1;
		break;
	}
	if (foundit)
	    break;
	(*ps)++;
    }
    *ps[0]=0;
    (*ps)++;
    return *ps;
}


/*
 * Read the first field of an I3 packet and call the proper function to
 * process it. Afterwards the original I3 packet is completly messed up.
 *
 */
int I3_parse_packet(void) {
    char *ps,*next_ps;

    ps=I3_currentpacket;
    if ( ps[0]!='(' || ps[1]!='{' ) {
	return 1;
    }
    ps+=2;
    I3_get_field(ps,&next_ps);

    I3_stats.count_total++;

    if (!strcmp(ps,"\"tell\"")) {
	I3_stats.count_tell++;
	I3_process_tell(next_ps);
	return 0;
    }
    if (!strcmp(ps,"\"emoteto\"")) {
	I3_stats.count_emoteto++;
	I3_process_emoteto(next_ps);
	return 0;
    }
    if (!strcmp(ps,"\"channel-m\"")) {
	I3_stats.count_channel_m++;
	I3_process_channel_m(next_ps);
	return 0;
    }
    if (!strcmp(ps,"\"channel-e\"")) {
	I3_stats.count_channel_e++;
	I3_process_channel_e(next_ps);
	return 0;
    }
    if (!strcmp(ps,"\"finger-req\"")) {
	I3_stats.count_finger_req++;
	I3_process_finger_req(next_ps);
	return 0;
    }
    if (!strcmp(ps,"\"finger-reply\"")) {
	I3_stats.count_finger_reply++;
	I3_process_finger_reply(next_ps);
	return 0;
    }
    if (!strcmp(ps,"\"locate-req\"")) {
	I3_stats.count_locate_req++;
	I3_process_locate_req(next_ps);
	return 0;
    }
    if (!strcmp(ps,"\"locate-reply\"")) {
	I3_stats.count_locate_reply++;
	I3_process_locate_reply(next_ps);
	return 0;
    }
    if (!strcmp(ps,"\"chan-who-req\"")) {
	I3_stats.count_channel_who_req++;
	I3_process_chan_who_req(next_ps);
	return 0;
    }
    if (!strcmp(ps,"\"chan-who-reply\"")) {
	I3_stats.count_channel_who_reply++;
	I3_process_chan_who_reply(next_ps);
	return 0;
    }
    if (!strcmp(ps,"\"who-req\"")) {
	I3_stats.count_who_req++;
	I3_process_who_req(next_ps);
	return 0;
    }
    if (!strcmp(ps,"\"who-reply\"")) {
	I3_stats.count_who_reply++;
	I3_process_who_reply(next_ps);
	return 0;
    }
    if (!strcmp(ps,"\"chanlist-reply\"")) {
	I3_stats.count_chanlist_reply++;
	I3_process_chanlist_reply(next_ps);
	return 0;
    }
    if (!strcmp(ps,"\"startup-reply\"")) {
	I3_stats.count_startup_reply++;
	I3_process_startup_reply(next_ps);
	return 0;
    }
    if (!strcmp(ps,"\"mudlist\"")) {
	I3_stats.count_mudlist++;
	I3_process_mudlist(next_ps);
	return 0;
    }
    if (!strcmp(ps,"\"error\"")) {
	I3_stats.count_error++;
	I3_process_error(next_ps);
	return 0;
    }

    I3_stats.count_unknown++;
    printf("got %s\n",I3_currentpacket);
    printf("Found unknown field: .%s.\n",ps);
    return 1;
}


/*
 *
 *
 */
bool I3_is_connected(void) {
    return I3_socket;
}

/*
 * Connect to the router and send the startup-packet.
 *
 */
void I3_main(bool forced) {
    I3_CHANNEL *channel;

    if (I3_socket>0) {
	logf("[0] I3_main(): main was called but socket existed");
	return;
    }

    I3_read_config();
    I3_read_channel_config();

    if (!this_mud.autoconnect && !forced)
	return;

    if ((I3_socket=I3_connection_open(this_mud.routerIP,this_mud.routerPort))==-1) {
	logf("[0] I3_main(): Couldn't connect to router.");
	return;
    }

    sleep(1);
//  signal(SIGINFO,i3_printstats);		// ^T

    I3_startup_packet();
    for (channel=I3_channel_list;channel;channel=channel->next) {
	I3_check_channel(channel);
    }
}

/*
 * Check for a packet and if one available read it and parse it.
 *
 */
void I3_loop(void) {
    int ret;

    if (I3_socket<=0)
	return;

    ret=I3_local_loop();
    if (ret<0)
	return;
    if (ret>0) {
	I3_read_packet();
	I3_parse_packet();
    }
}

/*
 * Shutdown the connection towards the router. Should also send the
 * shutdown-packet, not done yet.
 *
 */
void I3_shutdown(int delay) {
    if (I3_socket<=0) {
	logf("[0] I3_shutdown(): shutdown was called but no socket existed");
	return;
    }

    logf("[0] I3_shutdown(): Closing connection to router.");
    I3_send_shutdown(delay);
    I3_connection_close();
    I3_write_channel_config();
    I3_socket=0;
    I3_input_pointer=0;
    I3_output_pointer=4;
}

/*
 * Remove "'s at begin/end of string
 * If a character is prefixed by \'s it also will be unescaped
 *
 */
void I3_remove_quotes(char **ps) {
    char *ps1,*ps2;

    if (*ps[0]=='"')			// remove at beginning...
	(*ps)++;
    if ((*ps)[strlen(*ps)-1]=='"')	// ...and end
	(*ps)[strlen(*ps)-1]=0;

    ps1=ps2=*ps;
    while (ps2[0]) {
	if (ps2[0]=='\\') {
	    ps2++;
	}
	ps1[0]=ps2[0];
	ps1++;
	ps2++;
    }
    ps1[0]=0;
}

/*
 * Add backslashes in front of the " and \'s
 *
 */
char *I3_escape(char *ps) {
    static char new[MSL];
    char *pnew=new;

    while (ps[0]) {
	if (ps[0]=='"') {
	    pnew[0]='\\';
	    pnew++;
	}
	if (ps[0]=='\\') {
	    pnew[0]='\\';
	    pnew++;
	}
	pnew[0]=ps[0];
	pnew++;
	ps++;
    }
    pnew[0]=0;
    return new;
}

/*
 * Returns a CHAR_DATA structure which matches the string
 *
 */
CHAR_DATA *I3_find_user(char *name) {
    CHAR_DATA *ch=NULL;
    DESCRIPTOR_DATA *d;

    for ( d = descriptor_list; d ; d = d->next ) {
	if (d->character &&
	    d->character->name &&
	    tolower(d->character->name[0])==tolower(name[0]) &&
	    !str_cmp(d->character->name,name)) {
	    ch=d->character;
	    break;
	}
    }
    return ch;
}

/*
 * Put a I3-header in the send-buffer. If a field is NULL it will
 * be replaced by a 0 (zero).
 *
 */
void I3_write_header(char *identifier,char *originator_mudname,
		       char *originator_username,char *target_mudname,
		       char *target_username) {
    I3_write_buffer("({\"");
    I3_write_buffer(identifier);
    I3_write_buffer("\",5,");
    if (originator_mudname) {
	I3_write_buffer("\"");
	I3_write_buffer(originator_mudname);
	I3_write_buffer("\",");
    } else I3_write_buffer("0,");
    if (originator_username) {
	I3_write_buffer("\"");
	I3_write_buffer(originator_username);
	I3_write_buffer("\",");
    } else I3_write_buffer("0,");
    if (target_mudname) {
	I3_write_buffer("\"");
	I3_write_buffer(target_mudname);
	I3_write_buffer("\",");
    } else I3_write_buffer("0,");
    if (target_username) {
	I3_write_buffer("\"");
	I3_write_buffer(target_username);
	I3_write_buffer("\",");
    } else I3_write_buffer("0,");
}


/*
 * Read the header of an I3 packet. pps will point to the next field
 * of the packet.
 *
 */
void I3_get_header(char **pps,I3_HEADER *header) {
    char *ps=*pps,*next_ps;

    header->originator_mudname[0]=0;	header->originator_mudname[255]=0;
    header->originator_username[0]=0;	header->originator_username[255]=0;
    header->target_mudname[0]=0;	header->target_mudname[255]=0;
    header->target_username[0]=0;	header->target_username[255]=0;

    I3_get_field(ps,&next_ps);
    ps=next_ps;
    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    strncpy(header->originator_mudname,ps,254);
    ps=next_ps;
    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    strncpy(header->originator_username,ps,254);
    ps=next_ps;
    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    strncpy(header->target_mudname,ps,254);
    ps=next_ps;
    I3_get_field(ps,&next_ps);
    I3_remove_quotes(&ps);
    strncpy(header->target_username,ps,254);
    *pps=next_ps;
}

/*
 * This is how channels are interpreted. If they are no commands
 * or socials, this function will go through the list of channels
 * and send it to it if the name matches the local channel name.
 *
 */
bool I3_command_hook(CHAR_DATA *ch,char *command,char *argument) {
    I3_CHANNEL *channel;

    if (I3_socket<=0)
	return 0;

    if ((channel=find_i3_channel_by_localname(command))==NULL)
	return FALSE;

    if (!find_i3_listener_by_char(channel,ch)) {
	sprintf_to_char(ch,"You were trying to send something to an I3 "
	    "channel but you're not subcribed to it. Please use the command "
	    "'{WI3 listen %s{x' to subscribe to it.\n\r",
	    channel->local_name);
	return TRUE;
    }

#ifdef IS_FD
    if (STR_IS_SET(ch->strbit_comm,COMM_I3MUTE)) {
#else
    if (IS_SET(ch->comm,COMM_I3MUTE)) {
#endif
	send_to_char("The I3 channels are muted, use '{WI3 mute{x' to remove it.\n\r",ch);
	return 0;
    }

    switch (argument[0]) {
	case 0:
	    sprintf_to_char(ch,
		"Use '{WI3 listen %s{x' to turn off a channel.\n",command);
	    return 0;
	case ',':
	    I3_send_channel_emote(channel,ch->name,argument+1);
	    break;
	default:
	    I3_send_channel_message(channel,ch->name,argument);
	    break;
    }
    return 1;
}


#ifndef HAVE_SPRINTFTOCHAR
/*
 * do a formatted send_to_char()l
 *
 */
void sprintf_to_char(CHAR_DATA *ch, const char *fmt, ...)
{
    char buf[MAX_STRING_LENGTH];
    va_list ap;

    va_start(ap,fmt);
    vsprintf(buf,fmt,ap);
    va_end(ap);
    send_to_char(buf,ch);
}
#endif

#ifndef HAVE_WHICHKEYWORD
/*
 * Easy way to go through a list of options.
 *
 */
int which_keyword(char *keyword, ...) {
    va_list ap;
    int i=1;
    char *arg;
    char k[MAX_STRING_LENGTH];

    if (!keyword || !keyword[0])
        return -1;

    va_start(ap,keyword);
    keyword=one_argument(keyword,k);
    do {
	arg=va_arg(ap, char *);
	if (arg) {
	    if (!str_prefix(k,arg)) {
		va_end(ap);
		return i;
	    }
	}
        i++;
    } while(arg);
    va_end(ap);
    return 0;
}
#endif

#ifndef HAVE_LOGF
/*
 * Formatted log_string
 *
 */
void logf( const char *str,... )
{
    char buf[MAX_STRING_LENGTH];
    char *strtime;
    va_list ap;

    strtime                    = ctime( &current_time );
    strtime[strlen(strtime)-1] = '\0';

    va_start(ap,str);
    vsprintf(buf,str,ap);
    va_end(ap);

    fprintf( stderr, "%s %s\n",strtime,buf);

    return;
}
#endif
