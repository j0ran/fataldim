//
// $Id: act_donate.c,v 1.10 2007/02/27 20:24:24 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"
#include "interp.h"

char *players_god( CHAR_DATA *ch ) {
    // joran promised to fix this one day or another.
    return "Fatal";
}


bool sacrifice( CHAR_DATA *ch, OBJ_DATA *obj, int *reward, bool mShow ) {
    int silver;
    CHAR_DATA *gch;
    char *god=players_god(ch);

    if ( obj->item_type == ITEM_CORPSE_PC ) {
        if (obj->contains) {
            if(mShow) act("$t wouldn't like that.",ch,god,NULL,TO_CHAR);
            return FALSE;
        }
    }

    if ( !CAN_WEAR(obj, ITEM_TAKE) || CAN_WEAR(obj, ITEM_NO_SAC)
         || obj->item_type==ITEM_CLAN || obj->item_type==ITEM_CORPSE_PC)
    {
        if(mShow)
            act( "$p is not an acceptable sacrifice.", ch, obj, 0, TO_CHAR );
        return FALSE;
    }

    if (obj_is_owned(obj)) {
        if (obj_is_owned_by_another(ch,obj)) {
            if(mShow)
                act( "$t would not like that.", ch, obj->owner_name, 0, TO_CHAR );
            wiznet(WIZ_SACCING,get_trust(ch),ch,obj,
                   "$N tried to sacrifice [%d] $p owned by %s",obj->pIndexData->vnum,obj->owner_name);
            logf("%s tried to sacrifice [%d] %s owned by %s",
                 ch->name,obj->pIndexData->vnum,obj->short_descr,obj->owner_name);
            return FALSE;
        }
        logf("%s sacrifices owned [%d] %s",
             ch->name,obj->pIndexData->vnum,obj->short_descr);
    }

    if (obj->in_room != NULL)
    {
        for (gch = obj->in_room->people; gch != NULL; gch = gch->next_in_room)
            if (gch->on == obj)
            {
                if(mShow) {
                    if (ch==gch)
                        act("You appear to be using $p.",ch,obj,gch,TO_CHAR);
                    else
                        act("$N appears to be using $p.",ch,obj,gch,TO_CHAR);
                }
                return FALSE;
            }
    }

    silver = UMAX(1,obj->level * 3);

    if (obj->item_type != ITEM_CORPSE_NPC && obj->item_type != ITEM_CORPSE_PC)
        silver = UMIN(silver,obj->cost);

    *reward+=silver;

    if (IS_PC(ch))
        STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);

    act( "You sacrifice $p to $T.", ch, obj, god, TO_CHAR );
    act( "$n sacrifices $p to $T.", ch, obj, god, TO_ROOM );
    wiznet(WIZ_SACCING,0,ch,obj,"$N sends up $p as a burnt offering.");
    extract_obj( obj );

    return TRUE;
}


void award_sacrifice(CHAR_DATA *ch,int silver) {
    char *god=players_god(ch);
    /* variables for AUTOSPLIT */
    CHAR_DATA *gch;
    int members;
    char buffer[100];

    if (silver == 1)
        act("$t gives you one silver coin for your sacrifice.",
            ch,god,NULL,TO_CHAR);
    else {
        sprintf_to_char(ch,
                        "%s gives you %s for your sacrifice.\n\r",
                        god,money_string(silver/100,silver%100));
    }

    ch->silver += silver%100;
    ch->gold   += silver/100;

    if (STR_IS_SET(ch->strbit_act,PLR_AUTOSPLIT) )
    { /* AUTOSPLIT code */
        members = 0;
        for (gch = ch->in_room->people; gch != NULL; gch = gch->next_in_room )
        {
            if ( is_same_group( gch, ch ) )
                members++;
        }

        if ( members > 1 && silver > 1)
        {
            sprintf(buffer,"auto %d %d",silver%100,silver/100);
            do_function(ch, &do_split, buffer);
        }
    }

    return;
}

void do_sacrifice( CHAR_DATA *ch, char *argument )
{
    char argn[MAX_INPUT_LENGTH];
    char arg[MAX_INPUT_LENGTH];
    OBJ_DATA *obj,*obj_next;
    int silver=0;
    char *god=players_god(ch);
    int number,i;


    one_argument( argument, argn );
    number = mult_argument(argn,arg);

    if(!str_cmp("all",arg) || !str_prefix("all.", arg ) ) {
        //
        // sac all
        //
        bool found=FALSE;

        if (number!=1) {
            send_to_char("You can't sacrifice everything more than once.\n\r",ch);
            return;
        }

        for (obj=ch->in_room->contents;obj!=NULL;obj=obj_next) {
            obj_next=obj->next_content;
            if ((arg[3]==0 || is_name(&arg[4],obj->name))
                    && can_see_obj( ch, obj )) {
                if (sacrifice(ch,obj,&silver,FALSE)) {
                    found=TRUE;
                    if (STR_IS_SET(ch->in_room->strbit_room_flags,
                                   ROOM_NOSACRIFICE))
                        break;
                }
            }
        }

        if ( !found ) {
            if ( arg[3] == '\0' )
                send_to_char( "I see nothing here.\n\r", ch );
            else
                act( "I see no $T here.", ch, NULL, &arg[4], TO_CHAR );

            return;
        }
    } else {
        //
        // sac object
        //
        bool gotone=FALSE;
        int thissilver;

        if ( arg[0] == '\0' || !str_cmp( arg, ch->name ) )
        {
            act( "$n offers $mself to $t, who graciously declines.",
                 ch, god, NULL, TO_ROOM );
            act("$t appreciates your offer and may accept it later.",
                ch,god,NULL,TO_CHAR);
            return;
        }



        silver=0;
        for (i=0;i<number;i++) {
            obj = get_obj_list( ch, arg, ch->in_room->contents );
            if ( obj == NULL ) {
                if (i==0) {
                    send_to_char( "You can't find it.\n\r", ch );
                    return;
                } else {
                    sprintf_to_char(ch,"You saw here only %d %s%s.\n\r",
                                    i,arg,i==1?"":"s");
                }
                break;
            }
            thissilver=0;
            if (sacrifice(ch,obj,&thissilver,TRUE)) {
                gotone=TRUE;
                silver+=thissilver;
            } else
                break;
        }
        if (!gotone) return;
    }

    /* Handle the money stuff */
    award_sacrifice(ch,silver);
}

bool donate_into_room( CHAR_DATA *ch, OBJ_DATA *obj, ROOM_INDEX_DATA *room,int *reward, bool mShow )
{
    int silver=0;
    CHAR_DATA *gch;

    if ( !CAN_WEAR(obj, ITEM_TAKE) || CAN_WEAR(obj, ITEM_NO_SAC)
         || obj->item_type==ITEM_CLAN) {
        if(mShow) act( "$p can't be donated.", ch, obj, 0, TO_CHAR );
        return FALSE;
    }

    if (obj->in_room != NULL)
    {
        for (gch = obj->in_room->people; gch != NULL; gch = gch->next_in_room)
            if (gch->on == obj)
            {
                if (ch==gch)
                    act("You appear to be using $p.",ch,obj,gch,TO_CHAR);
                else
                    act("$N appears to be using $p.",ch,obj,gch,TO_CHAR);
                return FALSE;
            }
    }

    //    silver = UMAX(1,obj->level * 3);
    //    if (obj->item_type != ITEM_CORPSE_NPC && obj->item_type != ITEM_CORPSE_PC)
    //	silver = UMIN(silver,obj->cost);
    if (obj->item_type != ITEM_MONEY)
        silver = UMIN(obj->level*3,obj->cost);


    /* make sure corpses of mobs don't contain any valuable objects anymore */
    if (obj->item_type == ITEM_CORPSE_NPC) {
        OBJ_DATA *obj_in_corpse;
        OBJ_DATA *obj_next;

        for ( obj_in_corpse = obj->contains;
              obj_in_corpse != NULL; obj_in_corpse = obj_next )
        {
            obj_next = obj_in_corpse->next_content;
            obj_in_corpse->cost=0;
        }
    }

    if (IS_PC(ch))
        STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);

    *reward+=silver;
    obj->cost=0;
    obj_from_room(obj);

    if(!mShow) act( "You donate $p.", ch, obj, NULL, TO_CHAR );
    act( "$n donates $p.", ch, obj, NULL, TO_ROOM );
    wiznet(WIZ_DONATE,0,ch,obj,"$N donates $p.");

    if (obj->item_type==ITEM_CONTAINER) {
        OBJ_DATA *obj_in_container;
        OBJ_DATA *obj_next;

        for ( obj_in_container = obj->contains;
              obj_in_container != NULL; obj_in_container = obj_next ) {
            obj_next = obj_in_container->next_content;
            if (obj_in_container->item_type==ITEM_KEY) {
                obj_from_obj(obj_in_container);
                extract_obj(obj_in_container);
            }
        }
    }

    /* Prevent (often unique) keys to stay in a pit */
    if ( obj->item_type != ITEM_KEY ) {
        obj_to_room(obj,room);
        for (gch = room->people; gch != NULL; gch = gch->next_in_room )
            act("Suddenly $p appears in the room.",gch,obj,NULL,TO_CHAR);
    } else
        extract_obj( obj );

    return TRUE;
}

bool donate_into_obj( CHAR_DATA *ch, OBJ_DATA *obj, OBJ_DATA *target_obj,int *reward, bool mShow )
{
    int silver;
    CHAR_DATA *gch;

    if ( !CAN_WEAR(obj, ITEM_TAKE) || CAN_WEAR(obj, ITEM_NO_SAC)
         || obj->item_type==ITEM_CLAN) {
        if(mShow) act( "$p can't be donated.", ch, obj, 0, TO_CHAR );
        return FALSE;
    }

    if (obj->in_room != NULL) {
        for (gch = obj->in_room->people; gch != NULL; gch = gch->next_in_room)
            if (gch->on == obj) {
                if(mShow) {
                    if (ch==gch)
                        act("You appear to be using $p.",ch,obj,gch,TO_CHAR);
                    else
                        act("$N appears to be using $p.",ch,obj,gch,TO_CHAR);
                }
                return FALSE;
            }
    }

    silver = UMAX(1,obj->level * 3);

    if (obj->item_type != ITEM_CORPSE_NPC && obj->item_type != ITEM_CORPSE_PC)
        silver = UMIN(silver,obj->cost);

    /* make sure corpses of mobs don't contain any valuable objects anymore */
    if (obj->item_type == ITEM_CORPSE_NPC) {
        OBJ_DATA *obj_in_corpse;
        OBJ_DATA *obj_next;

        for ( obj_in_corpse = obj->contains;
              obj_in_corpse != NULL; obj_in_corpse = obj_next ) {
            obj_next = obj_in_corpse->next_content;
            obj_in_corpse->cost=0;
        }
    }

    *reward+=silver;
    obj->cost=0;

    obj_from_room(obj);

    if (IS_PC(ch))
        STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);

    if(!mShow) act( "You donate $p.", ch, obj, NULL, TO_CHAR );
    act( "$n donates $p.", ch, obj, NULL, TO_ROOM );
    wiznet(WIZ_DONATE,0,ch,obj,"$N donates $p.");

    /* Prevent (often unique) keys to stay in a pit */
    if ( obj->item_type != ITEM_KEY ) {
        obj_to_obj(obj,target_obj);
        //	for (gch = room->people; gch != NULL; gch = gch->next_in_room )
        //	    act("Suddenly $p appears in the room.",gch,obj,NULL,TO_CHAR);
    } else
        extract_obj( obj );

    return TRUE;
}


bool donate_force_find_targets=FALSE;

void do_donate( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    char argn[MAX_INPUT_LENGTH];
    OBJ_DATA *obj,*obj_next;
    int silver=0,thissilver=0;
    int number,i;
    char *god=players_god(ch);

    /* variables for AUTOSPLIT */
    CHAR_DATA *gch;
    int members;
    char buffer[100];

    ROOM_INDEX_DATA *room_donation;
    static OBJ_DATA *box_donation_vial=NULL;
    static OBJ_DATA *box_donation_scroll=NULL;
    static OBJ_DATA *box_donation_food=NULL;

    if (donate_force_find_targets) {
        box_donation_vial=NULL;
        box_donation_scroll=NULL;
        box_donation_food=NULL;
        donate_force_find_targets=FALSE;
        logf("Rescanning donate targets");
    }

    if(!(room_donation=get_room_index(ROOM_VNUM_DONATION))) {
        bugf("Do_donate: where is the pit (room %d) for %s?",
             ROOM_VNUM_DONATION,NAME(ch));
        return;
    }

    if (box_donation_vial==NULL) {
        for (obj=object_list;obj!=NULL;obj=obj->next) {
            if (obj->pIndexData->vnum==OBJ_VNUM_DONATION_VIAL) {
                box_donation_vial=obj;
            }
        }
        if (box_donation_vial==NULL)
            bugf("Do_donate: where is the glasscontainer (object %d) for %s?",
                 OBJ_VNUM_DONATION_VIAL,NAME(ch));
    }
    if (box_donation_scroll==NULL) {
        for (obj=object_list;obj!=NULL;obj=obj->next) {
            if (obj->pIndexData->vnum==OBJ_VNUM_DONATION_SCROLL) {
                box_donation_scroll=obj;
            }
        }
        if (box_donation_scroll==NULL)
            bugf("Do_donate: where is the papercontainer (object %d) for %s?",
                 OBJ_VNUM_DONATION_SCROLL,NAME(ch));
    }
    if (box_donation_food==NULL) {
        for (obj=object_list;obj!=NULL;obj=obj->next) {
            if (obj->pIndexData->vnum==OBJ_VNUM_DONATION_FOOD) {
                box_donation_food=obj;
            }
        }
        if (box_donation_food==NULL)
            bugf("Do_donate: where is the foodcontainer (object %d) for %s?",
                 OBJ_VNUM_DONATION_FOOD,NAME(ch));
    }

    one_argument( argument, argn );
    number=mult_argument(argn,arg);

    if ( arg[0] == '\0' || !str_cmp( arg, ch->name ) ) {
        send_to_char("Donate what?\n\r", ch );
        return;
    }

    if (ch->in_room->vnum==ROOM_VNUM_DONATION) {
        send_to_char(
                    "You can't donate stuff when you are in the donation room.\n\r",ch);
        return;
    }

    if(!str_cmp("all",arg) || !str_prefix("all.", arg ) )
    {
        bool found=FALSE;

        if (number!=1) {
            send_to_char("You can't donate everything more than once.\n\r",ch);
            return;
        }

        for(obj=ch->in_room->contents;obj!=NULL;obj=obj_next) {
            obj_next=obj->next_content;
            if((arg[3]=='\0' || is_name(&arg[4],obj->name))
                    && can_see_obj( ch, obj ))
            {
                switch (obj->pIndexData->vnum) {
                case OBJ_VNUM_EMPTYVIAL:
                    if (box_donation_vial) {
                        if (donate_into_obj(ch,obj,box_donation_vial,
                                            &silver,FALSE))
                            found=TRUE;
                    } else {
                        if (donate_into_room(ch,obj,room_donation,
                                             &silver,FALSE))
                            found=TRUE;
                    }
                    break;
                case OBJ_VNUM_EMPTYSCROLL:
                    if (box_donation_scroll) {
                        if (donate_into_obj(ch,obj,box_donation_scroll,
                                            &silver,FALSE))
                            found=TRUE;
                    } else {
                        if (donate_into_room(ch,obj,room_donation,
                                             &silver,FALSE))
                            found=TRUE;
                    }
                    break;
                default:
                    if (obj->item_type==ITEM_CORPSE_NPC ||
                            obj->item_type==ITEM_FOOD) {
                        if (box_donation_food) {
                            if (donate_into_obj(ch,obj,box_donation_food,
                                                &silver,FALSE))
                                found=TRUE;
                        } else {
                            if (donate_into_room(ch,obj,room_donation,
                                                 &silver,FALSE))
                                found=TRUE;
                        }
                    } else if (donate_into_room(ch,obj,room_donation,
                                                &silver,FALSE))
                        found=TRUE;
                }
                if (found
                        &&  STR_IS_SET(ch->in_room->strbit_room_flags,ROOM_NOSACRIFICE))
                    break;
            }
        }

        if ( !found ) {
            if ( arg[3] == '\0' )
                send_to_char( "I see nothing here.\n\r", ch );
            else
                act( "I see no $T here.", ch, NULL, &arg[4], TO_CHAR );

            return;
        }
    } else {
        for (i=0;i<number;i++) {

            if ((obj=get_obj_list( ch, arg, ch->in_room->contents ))==NULL) {
                if (i==0) {
                    send_to_char( "You can't find it.\n\r", ch );
                    return;
                }
                sprintf_to_char(ch,"There %s here only %d %s%s.\n\r",
                                i==1?"was":"were",i,arg,i==1?"":"s");
                break;
            }

            if ( !CAN_WEAR(obj, ITEM_TAKE) || CAN_WEAR(obj, ITEM_NO_SAC)) {
                act( "$p can't be donated.", ch, obj, 0, TO_CHAR );
                break;
            }

            thissilver=0;
            switch (obj->pIndexData->vnum) {
            case OBJ_VNUM_EMPTYVIAL:
                if (box_donation_vial) donate_into_obj(ch,obj,box_donation_vial, &thissilver,FALSE);
                else donate_into_room(ch,obj,room_donation, &thissilver,TRUE);
                break;
            case OBJ_VNUM_EMPTYSCROLL:
                if (box_donation_scroll) donate_into_obj(ch,obj,box_donation_scroll, &thissilver,FALSE);
                else donate_into_room(ch,obj,room_donation, &thissilver,TRUE);
                break;
            default:
                if (obj->item_type==ITEM_CORPSE_NPC || obj->item_type==ITEM_FOOD) {
                    if (box_donation_food) donate_into_obj(ch,obj,box_donation_food, &thissilver,FALSE);
                    else donate_into_room(ch,obj,room_donation, &thissilver,FALSE);
                }
                else {
                    donate_into_room(ch,obj,room_donation, &thissilver,TRUE);
                }
            }
            silver+=thissilver;
        }
    }

    if (silver==0) {
        sprintf_to_char(ch, "%s gracefully accepts your donation.\n\r", god );
        return;
    }

    if (silver == 1)
        sprintf_to_char(ch,
                        "%s gives you one silver coin for your donation.\n\r", god );
    else
        sprintf_to_char(ch,
                        "%s gives you %s for your donation.\n\r",
                        god,money_string(silver/100,silver%100));

    ch->silver += silver%100;
    ch->gold   += silver/100;

    if (STR_IS_SET(ch->strbit_act,PLR_AUTOSPLIT) )
    { /* AUTOSPLIT code */
        members = 0;
        for (gch = ch->in_room->people; gch != NULL; gch = gch->next_in_room )
        {
            if ( is_same_group( gch, ch ) )
                members++;
        }

        if ( members > 1 && silver > 1)
        {
            sprintf(buffer,"auto %d %d",silver%100,silver/100);
            do_function(ch,&do_split,buffer);
        }
    }

    return;
}
