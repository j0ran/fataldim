//
// $Id: fd_tcl_obj.c,v 1.44 2008/05/01 19:15:49 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#define NEEDS_TCL
#include "merc.h"

#define TCL_OBJ_CMD(name) static int name(Tcl_Interp *interp,OBJ_DATA *obj,int objc,Tcl_Obj *CONST objv[])

struct tcl_Obj_Command_Struct {
    char *cmd;
    int  (*func)(Tcl_Interp *,OBJ_DATA *,int,Tcl_Obj *CONST []);
};

TCL_OBJ_CMD(obj_name);
TCL_OBJ_CMD(obj_fullname);
TCL_OBJ_CMD(obj_id);
TCL_OBJ_CMD(obj_vnum);
TCL_OBJ_CMD(obj_short);
TCL_OBJ_CMD(obj_type);
TCL_OBJ_CMD(obj_property);
TCL_OBJ_CMD(obj_putin);
TCL_OBJ_CMD(obj_carried_by);
TCL_OBJ_CMD(obj_contained_in);
TCL_OBJ_CMD(obj_remember);
TCL_OBJ_CMD(obj_forget);
TCL_OBJ_CMD(obj_hastarget);
TCL_OBJ_CMD(obj_delay);
TCL_OBJ_CMD(obj_hasdelay);
TCL_OBJ_CMD(obj_targethere);
TCL_OBJ_CMD(obj_value);
TCL_OBJ_CMD(obj_contains);
TCL_OBJ_CMD(obj_extract);
TCL_OBJ_CMD(obj_long);
TCL_OBJ_CMD(obj_decaytimer);
TCL_OBJ_CMD(obj_timer);
TCL_OBJ_CMD(obj_flag);
TCL_OBJ_CMD(obj_extra);
TCL_OBJ_CMD(obj_wear);
TCL_OBJ_CMD(obj_weapon);
TCL_OBJ_CMD(obj_level);
TCL_OBJ_CMD(obj_addtrigger);
TCL_OBJ_CMD(obj_deltrigger);
TCL_OBJ_CMD(obj_triggers);
TCL_OBJ_CMD(obj_condition);
TCL_OBJ_CMD(obj_material);
TCL_OBJ_CMD(obj_cost);
TCL_OBJ_CMD(obj_weight);
TCL_OBJ_CMD(obj_extradescr);
TCL_OBJ_CMD(obj_transfer);
TCL_OBJ_CMD(obj_delproperty);
TCL_OBJ_CMD(obj_transfer_content);
TCL_OBJ_CMD(obj_namespace);
TCL_OBJ_CMD(obj_listeff);
TCL_OBJ_CMD(obj_addeff);
TCL_OBJ_CMD(obj_hasowner);
TCL_OBJ_CMD(obj_owner);
TCL_OBJ_CMD(ObjCmdNotImpl);

static struct tcl_Obj_Command_Struct tclObjCommands[] = {
	{ "name",		obj_name		},
	{ "fullname",		obj_fullname		},
	{ "id",			obj_id			},
	{ "vnum",		obj_vnum		},
	{ "short",		obj_short		},
	{ "type",		obj_type		},
	{ "property",		obj_property		},
	{ "putin",		obj_putin		},
	{ "carried_by",		obj_carried_by		},
	{ "contained_in",	obj_contained_in	},
	{ "remember",		obj_remember		},
	{ "forget",		obj_forget		},
	{ "hastarget",		obj_hastarget		},
	{ "delay",		obj_delay		},
	{ "hasdelay",		obj_hasdelay		},
	{ "targethere",		obj_targethere		},
	{ "value",		obj_value		},
	{ "contains",		obj_contains		},
	{ "extract",		obj_extract		},
	{ "long",		obj_long		},
	{ "timer",		obj_timer		},
	{ "decaytimer",		obj_decaytimer		},
	{ "flag",		obj_flag		},
	{ "extra",		obj_extra		},
	{ "wear",		obj_wear		},
	{ "weapon",		obj_weapon		},
	{ "level",		obj_level		},
	{ "addtrigger",		obj_addtrigger		},
	{ "deltrigger",		obj_deltrigger		},
	{ "triggers",		obj_triggers		},
	{ "condition",		obj_condition		},
	{ "material",		obj_material		},
	{ "cost",		obj_cost		},
	{ "weight",		obj_weight		},
	{ "extradescr",		obj_extradescr		},
	{ "transfer",		obj_transfer		},
	{ "delproperty",	obj_delproperty		},
	{ "transfer-content",	obj_transfer_content	},
	{ "namespace",		obj_namespace		},
	{ "listeff",		obj_listeff		},
	{ "addeff",		obj_addeff		},
	{ "listaff",		obj_listeff		},
	{ "hasowner",		obj_hasowner		},
	{ "owner",		obj_owner		},
	{ "exists",		ObjCmdNotImpl		},
	{ NULL,			ObjCmdNotImpl		}
};

int tclObjObjCmd (client, interp, objc, objv)
    ClientData client;			/* Not used. */
    Tcl_Interp *interp;			/* Current interpreter. */
    int objc;				/* Number of arguments. */
    Tcl_Obj *CONST objv[];		/* Argument values. */
{
    int index;
    OBJ_DATA *obj;

    // we know objv[0] is 'obj'          
    objc--; objv++;

    obj=tcl_parse_obj_switches(progData->obj,&objc,&objv);

    if (objc==1 && !str_cmp(Tcl_GetString(objv[0]),"exists")) {
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(obj!=NULL));
	return TCL_OK;
    }

    if (!obj) return TCL_ERROR;

    if (objc==0) {
	// No arguments given return name
	return obj_name(interp,obj,objc,objv);
    }

    if (Tcl_GetIndexFromObjStruct(interp, objv[0], 
				  (char **)tclObjCommands, sizeof(struct tcl_Obj_Command_Struct), 
				  "obj command", 0, &index) != TCL_OK) {
	return TCL_ERROR;
    }

    return (tclObjCommands[index].func)(interp,obj,objc,objv);
}

TCL_OBJ_CMD(ObjCmdNotImpl) {
    Tcl_Obj *result;

    result=Tcl_NewStringObj("Obj command not implemented yet: ",-1);
    Tcl_AppendObjToObj(result,objv[0]);
    Tcl_SetObjResult(interp,result);

    return TCL_ERROR;
}

// name
TCL_OBJ_CMD(obj_name) {
    Tcl_SetObjResult(interp,Tcl_NewStringObj(tcl_firstobjname(obj),-1));
    return TCL_OK;
}

// fullname [new names]
TCL_OBJ_CMD(obj_fullname) {
    char *newname;

    if (objc==2) {
	newname=Tcl_GetString(objv[1]);
	free_string(obj->name);
	obj->name=str_dup(newname);
    }
    Tcl_SetObjResult(interp,Tcl_NewStringObj(obj->name,-1));
    return TCL_OK;
}

/* id */
TCL_OBJ_CMD(obj_id) {
    Tcl_SetObjResult(interp,Tcl_NewLongObj(obj->id));
    return TCL_OK;
}

/* vnum */
TCL_OBJ_CMD(obj_vnum) {
    Tcl_SetObjResult(interp,Tcl_NewLongObj(obj->pIndexData->vnum));
    return TCL_OK;
}

/* short */
TCL_OBJ_CMD(obj_short) {
    char *newname;

    if (objc==2) {
	newname=Tcl_GetString(objv[1]);
	free_string(obj->short_descr);
	obj->short_descr=str_dup(newname);
    } else {
	Tcl_SetObjResult(interp,Tcl_NewStringObj(obj->short_descr,-1));
    }
    return TCL_OK;
}

/* type */
TCL_OBJ_CMD(obj_type) {
    Tcl_SetObjResult(interp,Tcl_NewStringObj( item_type(obj), -1));
    return TCL_OK;
}

// putin <objid>
TCL_OBJ_CMD(obj_putin) {
    OBJ_DATA *container=NULL;
    char *id;

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"char putin objid");
	return TCL_ERROR;
    }

    id=Tcl_GetString(objv[1]);
    container=tcl_objid(NULL,id,TRUE);
    if (container==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("obj putin: Container not found.",-1));
	return TCL_ERROR;
    }

    if (container->item_type!=ITEM_CONTAINER) {
    //
    // this gives a warning regarding 'expected integer but got string'
    //
	Tcl_SetObjResult(interp,Tcl_NewStringObj("obj putin: Container is no container.",-1));
	return TCL_ERROR;
    }

    if (obj->carried_by)
	obj_from_char(obj);
    if (obj->in_obj)
	obj_from_obj(obj);
    if (obj->in_room)
	obj_from_room(obj);
    obj_to_obj(obj,container);

    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(obj!=NULL));
    return TCL_OK;
}


// property <key> <type> [value]
TCL_OBJ_CMD(obj_property) {
    char *key;
    char *stype;
    char *svalue;
    int itype;
    bool readonly;

    if (objc!=3 && objc!=4) {
	Tcl_WrongNumArgs(interp,0,objv,"obj property key type ?value?");
	return TCL_ERROR;
    }

    readonly=(objc==3);

    key=Tcl_GetString(objv[1]);
    stype=Tcl_GetString(objv[2]);
    if (!readonly) svalue=Tcl_GetString(objv[3]);
    itype=PROPERTY_UNDEF;
    if (str_cmp(stype,"int")   ==0) itype=PROPERTY_INT;
    if (str_cmp(stype,"long")  ==0) itype=PROPERTY_LONG;
    if (str_cmp(stype,"bool")  ==0) itype=PROPERTY_BOOL;
    if (str_cmp(stype,"string")==0) itype=PROPERTY_STRING;
    if (str_cmp(stype,"char")  ==0) itype=PROPERTY_CHAR;
    if (itype==PROPERTY_UNDEF) {
	Tcl_WrongNumArgs(interp,0,objv,"type should be: int, long, bool, string or char");
	return TCL_ERROR;
    }

    if (readonly) {
	switch (itype) {
	case PROPERTY_INT: {
		int ivalue;
		if (GetObjectProperty(obj,PROPERTY_INT,key,&ivalue)==0)
		    Tcl_SetObjResult(interp,Tcl_NewIntObj(0));
		else
		    Tcl_SetObjResult(interp,Tcl_NewIntObj(ivalue));
	    }
	    break;

	case PROPERTY_LONG: {
		long lvalue;
		if (GetObjectProperty(obj,PROPERTY_LONG,key,&lvalue)==0)
		    Tcl_SetObjResult(interp,Tcl_NewLongObj(0));
		else
		    Tcl_SetObjResult(interp,Tcl_NewLongObj(lvalue));
	    }
	    break;

	case PROPERTY_BOOL: {
		bool bvalue;
		if (GetObjectProperty(obj,PROPERTY_BOOL,key,&bvalue)==0)
		    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(FALSE));
		else
		    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(bvalue));
	    }
	    break;

	case PROPERTY_STRING: {
		char svalue[128];
		if (GetObjectProperty(obj,PROPERTY_STRING,key,svalue)==0)
		    Tcl_SetObjResult(interp,Tcl_NewStringObj("",-1));
		else
		    Tcl_SetObjResult(interp,Tcl_NewStringObj(svalue,-1));
	    }
	    break;

	case PROPERTY_CHAR: {
		char cvalue,svalue[2];
		if (GetObjectProperty(obj,PROPERTY_CHAR,key,&cvalue)==0) {
		    svalue[0]=' ';svalue[1]=0;
		    Tcl_SetObjResult(interp,Tcl_NewStringObj(svalue,-1));
		} else {
		    svalue[0]=cvalue;svalue[1]=0;
		    Tcl_SetObjResult(interp,Tcl_NewStringObj(svalue,-1));
		}
	    }
	    break;
	}
    } else {
	switch (itype) {
	case PROPERTY_INT: {
		int ivalue;
		if (Tcl_GetIntFromObj(interp,objv[3],&ivalue)!=TCL_OK)
		    return TCL_ERROR;
		SetObjectProperty(obj,PROPERTY_INT,key,&ivalue);
	    }
	    break;

	case PROPERTY_LONG: {
		long lvalue;
		if (Tcl_GetLongFromObj(interp,objv[3],&lvalue)!=TCL_OK)
		    return TCL_ERROR;
		SetObjectProperty(obj,PROPERTY_LONG,key,&lvalue);
	    }
	    break;

	case PROPERTY_BOOL: {
		int i;
		bool bvalue;
		if (Tcl_GetBooleanFromObj(interp,objv[3],&i)!=TCL_OK)
		    return TCL_ERROR;
		bvalue=i!=0;
		SetObjectProperty(obj,PROPERTY_BOOL,key,&bvalue);
	    }
	    break;

	case PROPERTY_STRING: {
		SetObjectProperty(obj,PROPERTY_STRING,key,svalue);
	    }
	    break;

	case PROPERTY_CHAR: {
		char cvalue=svalue[1];
		SetObjectProperty(obj,PROPERTY_CHAR,key,&cvalue);
	    }
	    break;
	}
    }
    return TCL_OK;
}

// carried_by
TCL_OBJ_CMD(obj_carried_by) {
    Tcl_SetObjResult(interp,Tcl_NewLongObj(obj->carried_by==NULL?0:obj->carried_by->id));
    return TCL_OK;
}

TCL_OBJ_CMD(obj_contained_in) {
    if (obj->in_obj)
	Tcl_SetObjResult(interp,Tcl_NewLongObj(obj->in_obj->id));
    else
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(0));
    return TCL_OK;
}


// remember
TCL_OBJ_CMD(obj_remember) {
    if (objc>2) {
	Tcl_WrongNumArgs(interp,0,objv,"obj remember ?charid?");
	return TCL_ERROR;
    }

    if (objc==1) {
	Tcl_SetObjResult(interp,Tcl_NewLongObj(obj->oprog_target?obj->oprog_target->id:0));
    } else {
	CHAR_DATA *target;

	if (!(target=tcl_charid(NULL,Tcl_GetString(objv[1]),NULL))) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("obj remember: target not found",-1));
	    return TCL_ERROR;
	}

	obj->oprog_target=target;
	Tcl_SetObjResult(interp,Tcl_NewLongObj(target->id));
    }
    return TCL_OK;
}

// forget
TCL_OBJ_CMD(obj_forget) {
    if (obj->oprog_target) Tcl_SetObjResult(interp,Tcl_NewLongObj(obj->oprog_target->id));
    else Tcl_SetObjResult(interp,Tcl_NewLongObj(0));
    obj->oprog_target=NULL;
    return TCL_OK;
}

// hastarget
TCL_OBJ_CMD(obj_hastarget) {
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(obj->oprog_target!=NULL));
    return TCL_OK;
}

// delay
TCL_OBJ_CMD(obj_delay) {
    long delay;

    if (objc==1) {
	Tcl_SetObjResult(interp,Tcl_NewLongObj(obj->oprog_delay));
	return TCL_OK;
    }

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"obj delay number");
	return TCL_ERROR;
    }


    if (Tcl_GetLongFromObj(interp,objv[1],&delay)==TCL_ERROR) {
	if (!str_cmp(Tcl_GetString(objv[1]),"cancel")) delay=-1;
	else return TCL_ERROR;
    }
    obj->oprog_delay=delay;
    Tcl_SetObjResult(interp,Tcl_NewLongObj(delay));
    return TCL_OK;
}

// hasdelay
TCL_OBJ_CMD(obj_hasdelay) {
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(obj->oprog_delay>0));
    return TCL_OK;
}

// targethere
TCL_OBJ_CMD(obj_targethere) {
    if (obj->oprog_target==NULL)
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(FALSE));
    else if (obj->in_room==obj->oprog_target->in_room)
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(TRUE));
    else if (obj->carried_by==obj->oprog_target)
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(TRUE));
    else
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(FALSE));
    return TCL_OK;
}

// value <value>
TCL_OBJ_CMD(obj_value) {
    int value;

    if (objc!=2 && objc!=3) {
	Tcl_WrongNumArgs(interp,0,objv,"obj value number [new_value]");
	return TCL_ERROR;
    }

    if (Tcl_GetIntFromObj(interp,objv[1],&value)==TCL_ERROR) return TCL_ERROR;

    if (value<0 || value>4) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("obj value: number should be >= 0  and <= 4",-1));
	return TCL_ERROR;
    }
    if (objc==3) {
	int newval;
	CHAR_DATA *ch;

	if (Tcl_GetIntFromObj(interp,objv[2],&newval)==TCL_ERROR) return TCL_ERROR;

	if ( obj->item_type == ITEM_ARMOR &&
	     obj->wear_loc != WEAR_NONE &&
	     (ch=obj->carried_by) != NULL) {

	    ch->armor[value]    += apply_ac( obj, obj->wear_loc,value );
	    obj->value[value]=newval;
	    ch->armor[value]    -= apply_ac( obj, obj->wear_loc,value );
	} else {
	    obj->value[value]=newval;
	}
    }
    Tcl_SetObjResult(interp,Tcl_NewIntObj(obj->value[value]));
    return TCL_OK;
}

// contains
TCL_OBJ_CMD(obj_contains) {
    OBJ_DATA *ding=obj->contains;
    Tcl_Obj *list;

    list=Tcl_NewListObj(0,NULL);
    while (ding) {
	Tcl_ListObjAppendElement(interp,list,Tcl_NewLongObj(ding->id));
	ding=ding->next_content;
    }
    Tcl_SetObjResult(interp,list);
    return TCL_OK;
}

// extract
TCL_OBJ_CMD(obj_extract) {
    extract_obj(obj);
    return TCL_OK;
}

// long [new longname]
TCL_OBJ_CMD(obj_long) {
    char *newname;

    if (objc==2) {
	newname=Tcl_GetString(objv[1]);
	free_string(obj->description);
	obj->description=str_dup(newname);
    } else {
	Tcl_SetObjResult(interp,Tcl_NewStringObj(obj->description,-1));
    }
    return TCL_OK;
}

// timer [value]
TCL_OBJ_CMD(obj_timer) {
    Tcl_Obj *result;

    Tcl_Eval(interp,"namespace current");
    result=Tcl_GetObjResult(interp);

    wiznet(WIZ_TCL_DEBUG,0,NULL,NULL,"%s: Depricated: [obj timer]. please use [obj decaytimer]",Tcl_GetString(result));
    logf("%s: Depricated: [obj timer]. please use [obj decaytimer]",Tcl_GetString(result));
    return obj_decaytimer(interp,obj,objc,objv);
}

// decaytimer [value]
TCL_OBJ_CMD(obj_decaytimer) {
    int newtimer;

    if (objc==2) {
	if (Tcl_GetIntFromObj(interp,objv[1],&newtimer)==TCL_ERROR)
	    return TCL_ERROR;
	obj->timer=newtimer;
    }
    Tcl_SetObjResult(interp,Tcl_NewIntObj(obj->timer));
    return TCL_OK;
}

// flag <flagset> <flag> [boolean]
// flagset: "extra" "wear"
TCL_OBJ_CMD(obj_flag) {
    char *table_name,*flag_name;
    int  flag;
    int  readonly=objc==3;
    int  newvalue;

    bugf("TCL: obj flag: Depricated, please use 'obj extra' or 'obj wear'.");
    if (objc<3) {
	Tcl_WrongNumArgs(interp,0,objv,"obj flag <flag_set> <flag> [new_value]");
	return TCL_ERROR;
    }

    table_name=Tcl_GetString(objv[1]);
    flag_name=Tcl_GetString(objv[2]);

    if (!readonly) {
	char *newval;
	newval=Tcl_GetString(objv[3]);
	Tcl_GetBoolean(interp,newval,&newvalue);
    }

    if (!str_cmp(table_name,"extra")) {
	char *flagset=obj->strbit_extra_flags;

	flag=option_find_name(flag_name,extra_options,!readonly);
	if (flag==NO_FLAG) {
	    flagset=obj->strbit_extra_flags2;
	    flag=option_find_name(flag_name,extra2_options,!readonly);
	    if (flag==NO_FLAG) {
		Tcl_SetObjResult(interp,Tcl_NewStringObj("obj flag: Flag not found (or readonly)",-1));
		return TCL_ERROR;
	    }
	}
	if (!readonly) {
	    if (newvalue) STR_SET_BIT(flagset,flag);
	    else STR_REMOVE_BIT(flagset,flag);
	}
	Tcl_SetObjResult(interp,Tcl_NewIntObj(STR_IS_SET(flagset,flag)));
    } else if (!str_cmp(table_name,"wear")) {
	flag=option_find_name(flag_name,wear_options,!readonly);
	if (flag==NO_FLAG) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("obj flag: Flag not found (or readonly)",-1));
	    return TCL_ERROR;
	}
	if (readonly) {
	    if (newvalue) SET_BIT(obj->wear_flags,flag);
	    else REMOVE_BIT(obj->wear_flags,flag);
	}
	Tcl_SetObjResult(interp,Tcl_NewIntObj(IS_SET(obj->wear_flags,flag)));
    } else {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("obj flag: unknown set of flags",-1));
	return TCL_ERROR;
    }
    return TCL_OK;
}

// extra <flag> [boolean]
TCL_OBJ_CMD(obj_extra) {
    char *flag_name,*flagset;
    int  flag;
    int  readonly=objc==2;
    int  newvalue;

    if (objc<2) {
	Tcl_WrongNumArgs(interp,0,objv,"obj extra <flag> [new_value]");
	return TCL_ERROR;
    }

    flag_name=Tcl_GetString(objv[1]);

    if (!readonly) {
	char *newval;
	newval=Tcl_GetString(objv[2]);
	Tcl_GetBoolean(interp,newval,&newvalue);
    }
    flagset=obj->strbit_extra_flags;
    flag=option_find_name(flag_name,extra_options,!readonly);
    if (flag==NO_FLAG) {
	flagset=obj->strbit_extra_flags2;
	flag=option_find_name(flag_name,extra2_options,!readonly);
	if (flag==NO_FLAG) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("obj flag: Flag not found (or flag is readonly)",-1));
	    return TCL_ERROR;
	}
    }
    if (!readonly) {
	if (newvalue) STR_SET_BIT(flagset,flag);
	else STR_REMOVE_BIT(flagset,flag);
    }
    Tcl_SetObjResult(interp,Tcl_NewIntObj(STR_IS_SET(flagset,flag)));
    return TCL_OK;
}

// wear [<flag> [boolean]]
TCL_OBJ_CMD(obj_wear) {
    char *flag_name;
    int  flag;
    int  readonly=objc==2;
    int  newvalue;

    if (objc==1) {
	Tcl_Obj *result;
	const OPTION_TYPE *wear_option=wear_options;
	result=Tcl_NewListObj(0,NULL);
	for (;wear_option->name;wear_option++)
	    if (((wear_option->value & ITEM_WEAR_MASK)!=0) &&
		(IS_SET(obj->wear_flags,wear_option->value)))
		Tcl_ListObjAppendElement(interp,result,Tcl_NewStringObj(wear_option->name,-1));
	Tcl_SetObjResult(interp,result);
	return TCL_OK;
    }

    flag_name=Tcl_GetString(objv[1]);

    if (!readonly) {
	char *newval;
	newval=Tcl_GetString(objv[2]);
	Tcl_GetBoolean(interp,newval,&newvalue);
    }

    flag=option_find_name(flag_name,wear_options,!readonly);
    if (flag==NO_FLAG) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("obj flag: Flag not found (or readonly)",-1));
	return TCL_ERROR;
    }
    if (!readonly) {
	if (newvalue) SET_BIT(obj->wear_flags,flag);
	else REMOVE_BIT(obj->wear_flags,flag);
    }
    Tcl_SetObjResult(interp,Tcl_NewIntObj(IS_SET(obj->wear_flags,flag)));
    return TCL_OK;
}

// weapon [<flag> [boolean]]
TCL_OBJ_CMD(obj_weapon) {
    char *flag_name;
    int  flag;
    int  readonly=objc==2;
    int  newvalue;

    if (obj->item_type!=ITEM_WEAPON) {
	Tcl_AddErrorInfo(interp,"Obj is not a weapon");
	return TCL_ERROR;
    }

    if (objc==1) {
	Tcl_Obj *result;
	const OPTION_TYPE *weapon_option=weaponflag_options;
	result=Tcl_NewListObj(0,NULL);
	for (;weapon_option->name;weapon_option++)
	    if ((IS_SET(obj->value[4],weapon_option->value)))
		Tcl_ListObjAppendElement(interp,result,Tcl_NewStringObj(weapon_option->name,-1));
	Tcl_SetObjResult(interp,result);
	return TCL_OK;
    }

    flag_name=Tcl_GetString(objv[1]);

    if (!readonly) {
	char *newval;
	newval=Tcl_GetString(objv[2]);
	Tcl_GetBoolean(interp,newval,&newvalue);
    }

    flag=option_find_name(flag_name,weaponflag_options,!readonly);
    if (flag==NO_FLAG) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("obj weapon: Flag not found (or readonly)",-1));
	return TCL_ERROR;
    }
    if (!readonly) {
	if (newvalue) SET_BIT(obj->value[4],flag);
	else REMOVE_BIT(obj->value[4],flag);
    }
    Tcl_SetObjResult(interp,Tcl_NewIntObj(IS_SET(obj->value[4],flag)));
    return TCL_OK;
}

// level [new level]
TCL_OBJ_CMD(obj_level) {
    int newlevel;

    if (objc==2) {
	if (Tcl_GetIntFromObj(interp,objv[1],&newlevel)==TCL_ERROR)
	    return TCL_ERROR;
	obj->level=newlevel;
    }
    Tcl_SetObjResult(interp,Tcl_NewIntObj(obj->level));
    return TCL_OK;
}

TCL_OBJ_CMD(obj_addtrigger) {
    return op_addtrigger(obj,objc,objv);
}

// deltrigger
TCL_OBJ_CMD(obj_deltrigger) {
    static tclOptionList *deltriggerSwitches[]={"-all","-prefix", NULL};
    int sindex=-1;
    bool delAll,prefix;
    TCLPROG_LIST *trig,*trig_next;
    char *s,l;

    if (objc<=1) {
	Tcl_WrongNumArgs(interp,0,objv,"obj deltrigger ?-all? ?-prefix? ?name?");
	return TCL_ERROR;
    }

    s=Tcl_GetString(objv[1]);
    if (*s=='-' && Tcl_GetIndexFromObj(interp, objv[1], deltriggerSwitches, "switch", 0, &sindex) != TCL_OK)
	return TCL_ERROR;

    delAll=sindex==0;
    prefix=sindex==1;

    if ((prefix && objc!=3)
    || (!prefix && objc!=2)) {
	Tcl_WrongNumArgs(interp,0,objv,"obj deltrigger ?-all? ?-prefix? ?name?");
	return TCL_ERROR;
    }

    s=Tcl_GetString(objv[objc-1]);
    l=strlen(s);

    for (trig=obj->oprogs;trig;trig=trig_next) {
	trig_next=trig->next;
	if (delAll) op_delonetrigger(obj,trig);
	else if (prefix && strncmp(trig->name,s,l)==0) op_delonetrigger(obj,trig);
	else if (strcmp(trig->name,s)==0) op_delonetrigger(obj,trig);
    }
    return TCL_OK;
}

/* triggers */
TCL_OBJ_CMD(obj_triggers) {
    TCLPROG_LIST *trig;
    Tcl_Obj *result;

    result=Tcl_NewListObj(0,NULL);
    for (trig=obj->oprogs;trig;trig=trig->next) {
	Tcl_ListObjAppendElement(interp,result,Tcl_NewStringObj(trig->name,-1));
    }
    Tcl_SetObjResult(interp,result);
    return TCL_OK;
}

// condition [new condition]
TCL_OBJ_CMD(obj_condition) {
    if (objc==2) {
	int newcondition;
	if (Tcl_GetIntFromObj(interp,objv[1],&newcondition)==TCL_ERROR)
	    return TCL_ERROR;
	obj->condition=URANGE(0,newcondition,100);
    }
    Tcl_SetObjResult(interp,Tcl_NewIntObj(obj->condition));
    return TCL_OK;
}


// cost [value]
TCL_OBJ_CMD(obj_cost) {
    int newcost;

    if (objc==2) {
	if (Tcl_GetIntFromObj(interp,objv[1],&newcost)==TCL_ERROR)
	    return TCL_ERROR;
	obj->cost=newcost;
    }
    Tcl_SetObjResult(interp,Tcl_NewIntObj(obj->cost));
    return TCL_OK;
}

// weight [value]
TCL_OBJ_CMD(obj_weight) {
    int newweight;

    if (objc==2) {
	if (Tcl_GetIntFromObj(interp,objv[1],&newweight)==TCL_ERROR)
	    return TCL_ERROR;
	obj->weight=newweight;
    }
    Tcl_SetObjResult(interp,Tcl_NewIntObj(obj->weight));
    return TCL_OK;
}

// material [new material]
TCL_OBJ_CMD(obj_material) {
    char *newmaterial;

    if (objc==2) {
	newmaterial=Tcl_GetString(objv[1]);
	free_string(obj->material);
	obj->material=str_dup(newmaterial);
    }
    Tcl_SetObjResult(interp,Tcl_NewStringObj(obj->material,-1));
    return TCL_OK;
}

// extradescr add|del|mod|list [key [value]]
TCL_OBJ_CMD(obj_extradescr) {
    char *subcmd;
    char *key;
    char *value;

    if (objc<2) {
	Tcl_WrongNumArgs(interp,0,objv,"obj extradescr add|del|mod|list ?key ?value?? ");
	return TCL_ERROR;
    }
    subcmd=Tcl_GetString(objv[1]);
    if (objc>2)
	key=Tcl_GetString(objv[2]);
    if (objc>3)
	value=Tcl_GetString(objv[3]);
    if (!strcasecmp(subcmd,"list")) {
	EXTRA_DESCR_DATA *edd;
	Tcl_Obj *list;

	list=Tcl_NewListObj(0,NULL);
	for (edd=obj->extra_descr;edd;edd=edd->next) {
	    Tcl_ListObjAppendElement(interp,list,Tcl_NewStringObj(edd->keyword,-1));
	}
	Tcl_SetObjResult(interp,list);
	return TCL_OK;
    } else if (!strcasecmp(subcmd,"mod") || !strcasecmp(subcmd,"add")) {
	EXTRA_DESCR_DATA *edd;

	if (objc<4) {
	    Tcl_WrongNumArgs(interp,0,objv,"obj extradescr add|mod key value ");
	    return TCL_ERROR;
	}
	edd=get_extra_descr(key,obj->extra_descr);
	if (edd==NULL) {
	    edd=new_extra_descr();
	    edd->keyword=str_dup(key);
	    edd->description=str_dup(value);
	    edd->next=obj->extra_descr;
	    obj->extra_descr=edd;
	    return TCL_OK;
	}
	free_string(edd->description);
	edd->description=str_dup(value);
	return TCL_OK;
    } else if (!strcasecmp(subcmd,"del")) {
	EXTRA_DESCR_DATA *edd,*pedd;
	if (objc<3) {
	    Tcl_WrongNumArgs(interp,0,objv,"obj extradescr del key ");
	    return TCL_ERROR;
	}
	edd=get_extra_descr(key,obj->extra_descr);
	if (edd==NULL) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("obj extradescr del: Extra description doesn't exist ",-1));
	    return TCL_ERROR;
	}
	if (obj->extra_descr==edd) {
	    obj->extra_descr=edd->next;
	} else {
	    for (pedd=obj->extra_descr;pedd->next!=edd;pedd=pedd->next) 
		if (pedd==NULL) {
		    Tcl_SetObjResult(interp,Tcl_NewStringObj("obj extradescr del: Extra description exists but not found. Contact coder!",-1));
		    return TCL_ERROR;
		}
		    
	    /* pedd->next == edd */
	    pedd->next=edd->next;
	}
	free_extra_descr(edd);
	return TCL_OK;
    } else {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("obj extradescr: unknown subcommand (add/del/mod/list) ",-1));
	return TCL_ERROR;
    }
	
    return TCL_OK;
}

// transfer mob|room <id>
TCL_OBJ_CMD(obj_transfer) {
    char *type;
    if (objc!=3) {
	Tcl_WrongNumArgs(interp,0,objv,"obj transfer mob|room id ");
	return TCL_ERROR;
    }
    type=Tcl_GetString(objv[1]);
    if (!strcasecmp(type,"mob")) {
	CHAR_DATA *target=tcl_charid_obj(obj,Tcl_GetString(objv[2]),NULL);
	if (!target) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("obj transfer: mob not found.",-1));
	    return TCL_ERROR;
	}
	if (obj->in_room)    obj_from_room(obj);
	if (obj->in_obj)     obj_from_obj(obj);
	if (obj->carried_by) obj_from_char(obj);
	obj_to_char(obj,target);
    } else if (!strcasecmp(type,"room")) {
	ROOM_DATA *target;
	int vnum;
	if (Tcl_GetIntFromObj(interp,objv[2],&vnum)==TCL_ERROR) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("obj transfer: vnum has to be a number",-1));
	    return TCL_ERROR;
	}
	if (!(target=get_room_index(vnum))) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("obj transfer: vnum doesn't exist",-1));
	    return TCL_ERROR;
	}
	if (obj->in_room)    obj_from_room(obj);
	if (obj->in_obj)     obj_from_obj(obj);
	if (obj->carried_by) obj_from_char(obj);
	obj_to_room(obj,target);
    } else {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("obj transfer: you can only transfer to a room or to a mob.",-1));
	return TCL_ERROR;
    }
    return TCL_OK;
}

// delproperty <name> <type>
TCL_OBJ_CMD(obj_delproperty) {
    char *name;
    int type;
    static tclOptionList *types[]={ "int", "long", "bool", "string", "char", (char *)NULL };

    if (objc!=3) {
	Tcl_WrongNumArgs(interp,0,objv,"obj delproperty name type");
	return TCL_ERROR;
    }

    name=Tcl_GetString(objv[1]);
    if (Tcl_GetIndexFromObj(interp,objv[2],types,"type",0,&type)!=TCL_OK)
	return TCL_ERROR;

    DeleteObjectProperty(obj,type+1,name);

    return TCL_OK;
}


// transfer-content mob|room <id>
TCL_OBJ_CMD(obj_transfer_content) {
    char *type;
    if (objc!=3) {
	Tcl_WrongNumArgs(interp,0,objv,"obj transfer-content mob|room id ");
	return TCL_ERROR;
    }
    type=Tcl_GetString(objv[1]);
    if (!strcasecmp(type,"mob")) {
	CHAR_DATA *target=tcl_charid_obj(progData->obj,Tcl_GetString(objv[2]),NULL);
	if (!target) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("obj transfer-content: mob not found.",-1));
	    return TCL_ERROR;
	}
	while (obj->contains) {
	    OBJ_DATA *tobj=obj->contains;
	    obj_from_obj(tobj);
	    obj_to_char(tobj,target);
	}
    } else if (!strcasecmp(type,"room")) {
	ROOM_DATA *target;
	int vnum;
	if (Tcl_GetIntFromObj(interp,objv[2],&vnum)==TCL_ERROR) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("obj transfer-content: vnum has to be a number",-1));
	    return TCL_ERROR;
	}
	if (!(target=get_room_index(vnum))) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("obj transfer-content: vnum doesn't exist",-1));
	    return TCL_ERROR;
	}
	while (obj->contains) {
	    OBJ_DATA *tobj=obj->contains;
	    obj_from_obj(tobj);
	    obj_to_room(tobj,target);
	}
    } else {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("obj transfer-content: you can only transfer to a room or to a mob.",-1));
	return TCL_ERROR;
    }
    return TCL_OK;
}

TCL_OBJ_CMD(obj_namespace) {
    if (objc>1 && !str_cmp("-db",Tcl_GetString(objv[1])))
	Tcl_SetObjResult(interp,Tcl_NewStringObj(namespace_dobj(obj->pIndexData),-1));
    else
	Tcl_SetObjResult(interp,Tcl_NewStringObj(namespace_obj(obj),-1));             
    return TCL_OK;
}

TCL_OBJ_CMD(obj_listeff) {
    EFFECT_DATA *eff;
    Tcl_Obj *result;
    bool show_perm=FALSE,show_temp=FALSE;
    static tclOptionList *realms[]={ "permanent", "temporary", NULL };

    while (objc>1) {
	int realm;
	if (Tcl_GetIndexFromObj(interp,objv[1],realms,"realm",0,&realm)!=TCL_OK) return TCL_ERROR;
	switch (realm) {
	    case 0: show_perm=TRUE; break;
	    case 1: show_temp=TRUE; break;
	}
	objc--;objv++;
    }

    result=Tcl_NewListObj(0,NULL);

    if (show_perm || !show_temp) 
    for (eff=obj->pIndexData->affected;eff;eff=eff->next) {
	Tcl_ListObjAppendElement(interp,result,Effect_to_ListObj(eff));
    }

    if (show_temp || !show_perm) 
    for (eff=obj->affected;eff;eff=eff->next) {
	Tcl_ListObjAppendElement(interp,result,Effect_to_ListObj(eff));
    }

    Tcl_SetObjResult(interp,result);

    return TCL_OK;
}

TCL_OBJ_CMD(obj_hasowner) {
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(obj_is_owned(obj)));
    return TCL_OK;
}
TCL_OBJ_CMD(obj_owner) {
    if (objc>1) {
	set_object_owner(NULL,obj,Tcl_GetString(objv[1]));
    } else {
	if (obj_is_owned(obj))
	    Tcl_SetObjResult(interp,Tcl_NewStringObj(obj->owner_name,-1));
	else
	    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(0));
    }
    return TCL_OK;
}

// addeff <where> <type> <level> <location> <mod> <vector> [<arg1>]
TCL_OBJ_CMD(obj_addeff) {
    int i=-1;
    EFFECT_DATA eff;
    static tclOptionList *addeffOptions[]={
      "effects", "immune", "resist", "vuln", "effects2", "object2", "skill",(char *)NULL
    };

    if (objc!=8 && objc!=9) {
        Tcl_WrongNumArgs(interp,0,objv,"obj addeff <where> <type> <level> <duration> <location> <mod> <vector> [<arg1>]");
        return TCL_ERROR;
    }

    // where = EFFECTS IMMUNE RESIST VULN EFFECTS2
    // type  = gsn
    // duration in ticks
    // location is in apply_options[]
    // mod   = +- number
    //

    if (Tcl_GetIndexFromObj(interp,objv[1],addeffOptions,"where option",0,&i)!=TCL_OK) {
        return TCL_ERROR;
    }
    switch (i) {
        case 0: eff.where=TO_EFFECTS;  break;
        case 1: eff.where=TO_IMMUNE;   break;
        case 2: eff.where=TO_RESIST;   break;
        case 3: eff.where=TO_VULN;     break;
        case 4: eff.where=TO_EFFECTS2; break;
        case 5: eff.where=TO_OBJECT2;  break;
        case 6: eff.where=TO_SKILLS;   break;
        default:
            Tcl_SetObjResult(interp,Tcl_NewStringObj("obj addeff: you should not get this error",-1));
            return TCL_ERROR;
    };
    // where is ok, next: type
    eff.type=skill_lookup(Tcl_GetString(objv[2]));
    if (eff.type==-1 && str_cmp("(unknown)",Tcl_GetString(objv[2]))) {
        Tcl_SetObjResult(interp,Tcl_NewStringObj("obj addeff: Invalid spell/skill name",-1));
        return TCL_ERROR;
    }
    // ..., next: level
    if (Tcl_GetIntFromObj(interp,objv[3],&eff.level)!=TCL_OK ||
        eff.level<0 || eff.level>100) {
        Tcl_SetObjResult(interp,Tcl_NewStringObj("obj addeff: Invalid level",-1));
        return TCL_ERROR;
    }
    // ..., next: duration
    if (Tcl_GetIntFromObj(interp,objv[4],&eff.duration)!=TCL_OK) {
        Tcl_SetObjResult(interp,Tcl_NewStringObj("obj addeff: Invalid duration",-1));
        return TCL_ERROR;
    }
    // ..., next: location
    if ((eff.location=option_find_name(Tcl_GetString(objv[5]),apply_options,TRUE))==NO_FLAG) {
        Tcl_SetObjResult(interp,Tcl_NewStringObj("obj addeff: Invalid location",-1));
        return TCL_ERROR;
    }
    // ..., next: mod
    if (Tcl_GetIntFromObj(interp,objv[6],&eff.modifier)!=TCL_OK) {
        Tcl_SetObjResult(interp,Tcl_NewStringObj("obj addeff: Invalid modifier",-1));
        return TCL_ERROR;
    }
    // ..., next: vector
    // where==TO_EFFECTS : bitvector= effect_options
    //        IMM/RES/VULN:             imm_options
    //        TO_EFFECTS2: bitvector= effect2_options
    switch (eff.where) {
        case TO_EFFECTS:   i=option_find_name(Tcl_GetString(objv[7]),effect_options,FALSE);     break;
        case TO_IMMUNE:
        case TO_RESIST:
        case TO_VULN:      i=option_find_name(Tcl_GetString(objv[7]),imm_options,FALSE);        break;
        case TO_EFFECTS2:  i=option_find_name(Tcl_GetString(objv[7]),effect2_options,FALSE);    break;
        case TO_OBJECT2:   i=option_find_name(Tcl_GetString(objv[7]),extra2_options,FALSE);     break;
    }
    if (i==NO_FLAG) {
        Tcl_SetObjResult(interp,Tcl_NewStringObj("obj addeff: Invalid vector",-1));
        return TCL_ERROR;
    }
    eff.bitvector=i;
    // optional arg1
    if (objc==9)
        if (Tcl_GetIntFromObj(interp,objv[8],&eff.arg1)!=TCL_OK) {
            Tcl_SetObjResult(interp,Tcl_NewStringObj("obj addeff: Invalid arg1",-1));
            return TCL_ERROR;
        }

    effect_to_obj(obj,&eff);

    return TCL_OK;
}
