//
// $Id: string.h,v 1.7 2007/03/11 11:47:49 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef INCLUDED_STRING_H
#define INCLUDED_STRING_H

char *	string_replace		(char *orig,char *old,char *new);
char *	format_string		(char *oldstring);
char *	string_unpad		(char *argument);
char *	all_capitalize		(const char *str);
char *	string_trunc		(char *str,int len);

bool	str_cmp			(const char *astr,const char *bstr);
bool	str_prefix		(const char *astr,const char *bstr);
bool	str_prefix_nocolor	(const char *needle,const char *haystack);
bool	str_suffix		(const char *astr,const char *bstr);
bool	str_infix		(const char *astr,const char *bstr);
bool	str_infix_nocolor	(const char *astr,const char *bstr);
int	str_len			(const char *str);
char *	str_str			(const char *astr,const char *bstr);

char *	capitalize		(const char *astr);
char *	Capitalize		(const char *astr);
char *	spaces			(int len);
void 	smash_tilde		(char *str);
void	smash_case		(char *str);

char *	str_nocolor_r		(char *buf,int bufsize,const char *str);
const char *str_nocolor		(const char *str);

#define INT_STRING_BUF_SIZE	((sizeof(int)*8)/3+2)
#define LONG_STRING_BUF_SIZE	((sizeof(long)*8)/3+2)
char *	itoa_r			(int value,char *buf,int bufsize);
char *	itoa			(int value);
char *	ltoa_r			(long value,char *buf,int bufsize);
char *	ltoa			(long value);
#endif	// INCLUDED_STRING_H
