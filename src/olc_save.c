/* $Id: olc_save.c,v 1.64 2007/11/04 14:58:57 jodocus Exp $ */
/***************************************************************************
 *  File: olc_save.c                                                       *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 *                                                                         *
 *  This code was freely distributed with the The Isles 1.1 source code,   *
 *  and has been used here for OLC - OLC would not be what it is without   *
 *  all the previous coders who released their source code.                *
 *                                                                         *
 ***************************************************************************/
/* OLC_SAVE.C
 * This takes care of saving all the .are information.
 * Notes:
 * -If a good syntax checker is used for setting vnum ranges of areas
 *  then it would become possible to just cycle through vnums instead
 *  of using the iHash stuff and checking that the room or reset or
 *  mob etc is part of that area.
 */

#include "merc.h"
#include "interp.h"
#include "olc.h"
#include "db.h"



/*****************************************************************************
 Name:		fix_string
 Purpose:	Returns a string without \r and ~.
 ****************************************************************************/
char *fix_string( const char *str )
{
    static char strfix[4*MAX_STRING_LENGTH];
    int i;
    int o;

    if ( str == NULL )
    {
        strfix[0] = '\0';
        return strfix;
    }

    for ( o = i = 0; str[i+o] != '\0'; i++ )
    {
        if (str[i+o] == '\r' || str[i+o] == '~')
            o++;
//      strfix[i] = str[i+o];
	if ((strfix[i] = str[i+o]) == '\0')
	    break;
    }
    strfix[i] = '\0';
    return strfix;
}



/*****************************************************************************
 Name:		save_area_list
 Purpose:	Saves the listing of files to be loaded at startup.
 Called by:	do_asave(olc_save.c).
 ****************************************************************************/
void save_area_list(void)
{
    FILE *fp;
    AREA_DATA *pArea;
    char renamefile[MSL];

    strcpy(renamefile,mud_data.area_lst);
    strcat(renamefile,"~");
    rename(mud_data.area_lst,renamefile);

    if ( ( fp = fopen( mud_data.area_lst, "w" ) ) == NULL )
    {
	logf("[0] save_area_lst: fopen(%s): %s", mud_data.area_lst,ERROR);
	bugf( "Save_area_lst: Couldn't save '%s'", mud_data.area_lst );
	return;
    }

    /*
     * Add any help files that need to be loaded at
     * startup to this section.
     */
    fprintf( fp, SOCIAL_FILE"\n" );

    for( pArea = area_first; pArea; pArea = pArea->next )
    {
	if(!IS_SET(pArea->area_flags,AREA_DELETE))
	    fprintf( fp, "%s\n", pArea->filename );
    }

    fprintf( fp, "$\n" );
    fclose( fp );
}

char *flagstr( long f )
{
    static char buf[128];
    char *p;
    int i;

    if(!f) return "0";

    p=buf;
    for(i=0;i<32;i++)
    {
	if(f&(1<<i))
	{
	    if(i<26) *(p++)='A'+i;
	    else *(p++)='a'+i-26;
	}
    }

    *p='\0';

    return buf;
}

void save_mobile_remove_flag(FILE *fp,char *flagname,long flag)
{
    if(flag) fprintf(fp, "F %s %s\n",flagname,flagstr(flag));
}

void save_mobile_remove_flag_string(FILE *fp,char *flagname,char *flag) {
    char temp[MSL];
    if (!STR_SAME_STR(flag,ZERO_FLAG,MAX_FLAGS))
	fprintf(fp, "F %s %s~\n",flagname,stringtohex(flag,temp,MAX_FLAGS));
}

void save_extradesc(FILE *fp,EXTRA_DESCR_DATA *pEd) {
    if (pEd==NULL)
	return;

    save_extradesc(fp,pEd->next);

    fprintf( fp, "E\n%s~\n%s~\n", pEd->keyword,
	  fix_string( pEd->description ) );
}

/*****************************************************************************
 Name:		save_mobiles
 Purpose:	Save #MOBILES secion of an area file.
 Called by:	save_area(olc_save.c).
 ****************************************************************************/
void save_mobiles( FILE *fp, AREA_DATA *pArea )
{
    int vnum;
    MOB_INDEX_DATA *pMobIndex;
    char temp[MSL];

    fprintf( fp, "#MOBILES\n" );
    for( vnum = pArea->lvnum; vnum <= pArea->uvnum; vnum++ )
    {
	if( ( pMobIndex = get_mob_index(vnum) ) )
	{
	    if (( pMobIndex->area == pArea )
	      && !pMobIndex->deleted)
            {
                fprintf( fp, "#%d\n",		pMobIndex->vnum );
                fprintf( fp, "%s~\n",		pMobIndex->player_name );
                fprintf( fp, "%s~\n",		pMobIndex->short_descr );
                fprintf( fp, "%s~\n",		fix_string( pMobIndex->long_descr ) );
                fprintf( fp, "%s~\n",		fix_string( pMobIndex->description ) );
		fprintf( fp, "%s~\n",		race_table[pMobIndex->race].name );
                fprintf( fp, "%s~\n",		stringtohex( pMobIndex->strbit_act_read,temp,MAX_FLAGS) );
//              fprintf( fp, "%s ",		flagstr( pMobIndex->affected_mem[0] ) );
                fprintf( fp, "%d ",		pMobIndex->alignment );
		fprintf( fp, "%d\n",		pMobIndex->group );
                fprintf( fp, "%s~\n",		stringtohex( pMobIndex->strbit_affected_read,temp,MAX_FLAGS ) );
                fprintf( fp, "%d ",		pMobIndex->level );
                fprintf( fp, "%d ",		pMobIndex->hitroll );
                fprintf( fp, "%dd%d+%d ",	pMobIndex->hit[DICE_NUMBER],
                                                pMobIndex->hit[DICE_TYPE],
                                                pMobIndex->hit[DICE_BONUS] );
                fprintf( fp, "%dd%d+%d ",	pMobIndex->mana[DICE_NUMBER],
                                                pMobIndex->mana[DICE_TYPE],
                                                pMobIndex->mana[DICE_BONUS] );
                fprintf( fp, "%dd%d+%d ",	pMobIndex->damage[DICE_NUMBER],
                                                pMobIndex->damage[DICE_TYPE],
                                                pMobIndex->damage[DICE_BONUS] );
		fprintf( fp, "'%s'\n",		attack_table[pMobIndex->dam_type].name );
		fprintf( fp, "%d %d %d %d\n",	pMobIndex->ac[AC_PIERCE]/10,
						pMobIndex->ac[AC_BASH]/10,
						pMobIndex->ac[AC_SLASH]/10,
						pMobIndex->ac[AC_EXOTIC]/10 );
//		fprintf( fp, "%s ",		flagstr( pMobIndex->off_mem[0] ) );
//		fprintf( fp, "%s ",		flagstr( pMobIndex->imm_mem[0] ) );
//		fprintf( fp, "%s ",		flagstr( pMobIndex->res_mem[0] ) );
//		fprintf( fp, "%s ",		flagstr( pMobIndex->vuln_mem[0] ) );
		fprintf( fp, "%s~\n",		stringtohex( pMobIndex->strbit_off_read,temp,MAX_FLAGS));
		fprintf( fp, "%s~\n",		stringtohex( pMobIndex->strbit_imm_read,temp,MAX_FLAGS));
		fprintf( fp, "%s~\n",		stringtohex( pMobIndex->strbit_res_read,temp,MAX_FLAGS));
		fprintf( fp, "%s~\n",		stringtohex( pMobIndex->strbit_vuln_read,temp,MAX_FLAGS));
		fprintf( fp, "%s ",		position_table[pMobIndex->start_pos].short_name );
		fprintf( fp, "%s ",		position_table[pMobIndex->default_pos].short_name );
		fprintf( fp, "%s ",		table_find_value(pMobIndex->sex,sex_table));
                fprintf( fp, "%d\n",		(int)pMobIndex->wealth );
		fprintf( fp, "%s ",		flagstr( pMobIndex->form_mem[0] ) );
		fprintf( fp, "%s ",		flagstr( pMobIndex->parts_mem[0] ) );
		fprintf( fp, "%s ",		size_table[pMobIndex->size].name );
		if(pMobIndex->material && *pMobIndex->material!='0' && *pMobIndex->material!='\0')
		    fprintf( fp, "'%s'\n",	pMobIndex->material );
		else
		    fprintf( fp, "0\n" );

		/* Save new fields first */
		if (!STR_SAME_STR(pMobIndex->strbit_affected_by2,ZERO_FLAG,MAX_FLAGS))
		    fprintf(fp, "N aff2 %s~\n",	stringtohex( pMobIndex->strbit_affected_read2,temp,MAX_FLAGS ) );

		save_mobile_remove_flag_string(fp,"act",pMobIndex->strbit_act_saved);
		save_mobile_remove_flag_string(fp,"aff",pMobIndex->strbit_affected_saved);
		save_mobile_remove_flag_string(fp,"aff2",pMobIndex->strbit_affected_saved2);
		save_mobile_remove_flag_string(fp,"off",pMobIndex->strbit_off_saved);
		save_mobile_remove_flag_string(fp,"imm",pMobIndex->strbit_imm_saved);
		save_mobile_remove_flag_string(fp,"res",pMobIndex->strbit_res_saved);
		save_mobile_remove_flag_string(fp,"vul",pMobIndex->strbit_vuln_saved);
		save_mobile_remove_flag(fp,"for",pMobIndex->form_mem[1]);
		save_mobile_remove_flag(fp,"par",pMobIndex->parts_mem[1]);

		/* Save new mobprograms */
		if(pMobIndex->mprog_vnum)
		    fprintf( fp, "L %d\n",pMobIndex->mprog_vnum);
		if(pMobIndex->pueblo_picture[0])
		    fprintf( fp, "Q %s~\n",pMobIndex->pueblo_picture);

		save_property(fp,"P",pMobIndex->property,
		    "save_mobiles",pMobIndex->player_name);

//		save_mob_mobprogs(fp,pMobIndex->mprogs);
		save_extradesc(fp,pMobIndex->extra_descr);
            }
        }
    }
    fprintf( fp, "#0\n\n\n\n" );
    return;
}

static char *olc_skill_name(int skill)
{
  if(skill<0) return "";
  else return skill_name(skill,NULL);
}

void save_effects(FILE *fp,EFFECT_DATA *pef) {

    if (pef==NULL)
	return;

    save_effects(fp,pef->next);

    switch(pef->where)
    {
	case TO_OBJECT:
	    fprintf( fp, "A\n%d %d\n",
		pef->location,
		pef->modifier );
	    break;
	case TO_OBJECT2:
	    fprintf( fp, "B\n%d %d %d %d\n",
		pef->location,
		pef->modifier,
		pef->bitvector,
		pef->arg1 );
	    break;
	case TO_EFFECTS:
	    fprintf( fp, "F\nA %d %d %d\n",
		pef->location,
		pef->modifier,
		pef->bitvector );
	    break;
	case TO_EFFECTS2:
	    fprintf( fp, "F\nB %d %d %d %d\n",
		pef->location,
		pef->modifier,
		pef->bitvector,
		pef->arg1 );
	    break;
	case TO_IMMUNE:
	    fprintf( fp, "F\nI %d %d %d\n",
		pef->location,
		pef->modifier,
		pef->bitvector );
	    break;
	case TO_RESIST:
	    fprintf( fp, "F\nR %d %d %d\n",
		pef->location,
		pef->modifier,
		pef->bitvector );
	    break;
	case TO_VULN:
	    fprintf( fp, "F\nV %d %d %d\n",
		pef->location,
		pef->modifier,
		pef->bitvector );
	    break;
	case TO_SKILLS:
	    fprintf( fp, "F\nS %d %d %d\n",
		pef->location,
		pef->modifier,
		pef->bitvector );
	    break;
    }
}


/*****************************************************************************
 Name:		save_objects
 Purpose:	Save #OBJECTS section of an area file.
 Called by:	save_area(olc_save.c).
 ****************************************************************************/
void save_objects( FILE *fp, AREA_DATA *pArea )
{
    int vnum;
    OBJ_INDEX_DATA *pObjIndex;
    char c;
    char temp[MSL];

    fprintf( fp, "#OBJECTS\n" );
    for( vnum = pArea->lvnum; vnum <= pArea->uvnum; vnum++ )
    {
	if( ( pObjIndex = get_obj_index(vnum) )
	  && !pObjIndex->deleted)
	{
	    if ( pObjIndex->area == pArea )
            {
                fprintf( fp, "#%d\n",    pObjIndex->vnum );
                fprintf( fp, "%s~\n",    pObjIndex->name );
                fprintf( fp, "%s~\n",    pObjIndex->short_descr );
                fprintf( fp, "%s~\n",    fix_string( pObjIndex->description ) );
                fprintf( fp, "%s~\n",	 pObjIndex->material);
                fprintf( fp, "%s ",      table_find_value(pObjIndex->item_type,item_table));
                fprintf( fp, "%s~\n",    stringtohex( pObjIndex->strbit_extra_flags,temp,MAX_FLAGS ) );
                fprintf( fp, "%s\n",     flagstr( pObjIndex->wear_flags ) );

                switch ( pObjIndex->item_type )
                {
                default:
                fprintf( fp, "%d %d %d %d %d\n",
			pObjIndex->value[0],
			pObjIndex->value[1],
			pObjIndex->value[2],
			pObjIndex->value[3],
			pObjIndex->value[4] );
                break;
		case ITEM_WEAPON:
		fprintf( fp, "'%s' %d %d '%s' %s\n",
			weapon_name(pObjIndex->value[0]),
			pObjIndex->value[1],
			pObjIndex->value[2],
			attack_table[pObjIndex->value[3]].name,
			flagstr( pObjIndex->value[4] ) );
		break;
		case ITEM_CONTAINER:
		fprintf( fp, "%d %s %d %d %d\n",
			pObjIndex->value[0],
			flagstr( pObjIndex->value[1] ),
			pObjIndex->value[2],
			pObjIndex->value[3],
			pObjIndex->value[4] );
		break;
	        case ITEM_DRINK_CON:
		case ITEM_FOUNTAIN:
		fprintf( fp, "%d %d '%s' %d %d\n",
			pObjIndex->value[0],
			pObjIndex->value[1],
			liq_table[pObjIndex->value[2]].liq_name,
			pObjIndex->value[3],
			pObjIndex->value[4] );
		break;
		case ITEM_WAND:
		case ITEM_STAFF:
		fprintf( fp, "%d %d %d '%s' %d\n",
			pObjIndex->value[0],
			pObjIndex->value[1],
			pObjIndex->value[2],
			olc_skill_name(pObjIndex->value[3]),
			pObjIndex->value[4] );
		break;
		case ITEM_POTION:
		case ITEM_PILL:
		case ITEM_SCROLL:
		fprintf( fp, "%d '%s' '%s' '%s' '%s'\n",
			pObjIndex->value[0],
			olc_skill_name(pObjIndex->value[1]),
			olc_skill_name(pObjIndex->value[2]),
			olc_skill_name(pObjIndex->value[3]),
			olc_skill_name(pObjIndex->value[4]) );
		break;
                }

		fprintf( fp, "%d ", pObjIndex->level );
                fprintf( fp, "%d ", pObjIndex->weight );
                fprintf( fp, "%d ", pObjIndex->cost );
		switch(pObjIndex->condition)
		{
		    case 100: c='P';break;
		    case  90: c='G';break;
		    case  75: c='A';break;
		    case  50: c='W';break;
		    case  25: c='D';break;
		    case  10: c='B';break;
		    case   0: c='R';break;
		    default : c='P';break;
		}
		fprintf( fp, "%c\n",c);

		/* Save new fields first */
		if (!STR_SAME_STR(pObjIndex->strbit_extra_flags2,ZERO_FLAG,MAX_FLAGS))
		    fprintf(fp,"N extra2 %s~\n",
			stringtohex(pObjIndex->strbit_extra_flags2,
				temp,MAX_FLAGS));

		save_effects(fp,pObjIndex->affected);

		if(pObjIndex->oprog_vnum)
		    fprintf( fp, "L %d\n",pObjIndex->oprog_vnum);
		save_property(fp,"P",pObjIndex->property,
		    "save_objects",pObjIndex->name);

		if(pObjIndex->pueblo_picture[0])
		    fprintf( fp, "Q %s~\n",pObjIndex->pueblo_picture);

		save_extradesc(fp,pObjIndex->extra_descr);
            }
        }
    }
    fprintf( fp, "#0\n\n\n\n" );
    return;
}



/* OLC 1.1b */
/*****************************************************************************
 Name:		save_rooms
 Purpose:	Save #ROOMDATA section of an area file.
 Called by:	save_area(olc_save.c).
 ****************************************************************************/
void save_rooms( FILE *fp, AREA_DATA *pArea )
{
    ROOM_INDEX_DATA *pRoomIndex;
    EXIT_DATA *pExit;
    int vnum;
    int door;
    char temp[MSL];

    fprintf( fp, "#ROOMDATA\n" );
    for( vnum = pArea->lvnum; vnum <= pArea->uvnum; vnum++ )
    {
	if( ( pRoomIndex = get_room_index(vnum) )
	  && !pRoomIndex->deleted)
	{
	    if ( pRoomIndex->area == pArea )
	    {
		fprintf( fp, "#%d\n", pRoomIndex->vnum );
		fprintf( fp, "%s~\n", pRoomIndex->name );
		fprintf( fp, "%s~\n", fix_string( pRoomIndex->description ) );
		fprintf( fp, "0 " );
		fprintf( fp, "%s~\n", stringtohex(pRoomIndex->strbit_room_flags,temp,MAX_FLAGS) );
//		fprintf( fp, "%s ", flagstr( pRoomIndex->room_flags ) );
		fprintf( fp, "%d\n", pRoomIndex->sector_type );
		fprintf( fp, "L %d\n", pRoomIndex->rprog_vnum );

		for( door = 0; door < MAX_DIR; door++ )
		{
		    if ( ( pExit = pRoomIndex->exit[door] ) )
		    {
			fprintf( fp, "D%d\n", door );
			fprintf( fp, "%s~\n",
			    fix_string( pExit->description ) );
			fprintf( fp, "%s~\n", pExit->keyword );
			fprintf( fp, "%d %d %d\n", pExit->rs_flags,
			    pExit->key,
			    pExit->to_room ? pExit->to_room->vnum : 0 );
		    }
		}

		save_extradesc(fp,pRoomIndex->extra_descr);

		if(pRoomIndex->level)
		    fprintf( fp, "N level %d\n",pRoomIndex->level);

//		if(pRoomIndex->clan)
//		{
//		    CLAN_TYPE *clan=get_clan(pRoomIndex->clan);
//		    if(clan->vnum) fprintf( fp, "C\n%s~\n",clan->name);
//		}

		save_property(fp,"P",pRoomIndex->property,
		    "save_rooms",pRoomIndex->name);

		if(pRoomIndex->pueblo_picture[0])
		    fprintf( fp, "Q %s~\n",pRoomIndex->pueblo_picture);

//		if(*pRoomIndex->owner)
//		    fprintf( fp, "O\n%s~\n",pRoomIndex->owner);

		fprintf( fp, "S\n" );
	    }
	}
    }
    fprintf( fp, "#0\n\n\n\n" );
    return;
}


/*****************************************************************************
 save all properties defined for this area
 ****************************************************************************/
void save_property_indexes( FILE *fp, AREA_DATA *pArea ) {
    PROPERTY_INDEX_TYPE *pProp;
    if (!pArea->properties) return;
    fprintf( fp, "#PROPERTIES\n" );

    for (pProp=pArea->properties;pProp;pProp=pProp->next) {
      fprintf(fp,"Prop %s %s\n",pProp->key,table_find_value(pProp->type,property_table));
    }

    fprintf( fp, "END\n\n\n\n" );
    return;
}


/* OLC 1.1b */
/*****************************************************************************
 Name:		save_specials
 Purpose:	Save #SPECIALS section of area file.
 Called by:	save_area(olc_save.c).
 ****************************************************************************/
void save_specials( FILE *fp, AREA_DATA *pArea )
{
    int vnum;
    MOB_INDEX_DATA *pMobIndex;

    fprintf( fp, "#SPECIALS\n" );

    for( vnum = pArea->lvnum; vnum <= pArea->uvnum; vnum++ )
    {
	if( ( pMobIndex = get_mob_index(vnum) )
	  && !pMobIndex->deleted)
	{
	    if ( pMobIndex->area == pArea && pMobIndex->spec_fun )
            {
                fprintf( fp, "M %d %s\n", pMobIndex->vnum,
                                          spec_string( pMobIndex->spec_fun ) );
            }
        }
    }

    fprintf( fp, "S\n\n\n\n" );
    return;
}

/* OLC 1.1b */
/*****************************************************************************
 Name:		vsave_specials
 Purpose:	Save #SPECIALS section of area file.
 		New formating thanks to Rac.
 Called by:	save_area(olc_save.c).
 ****************************************************************************/
void vsave_specials( FILE *fp, AREA_DATA *pArea )
{
    int vnum;
    MOB_INDEX_DATA *pMobIndex;

    fprintf( fp, "#SPECIALS\n" );

    for( vnum = pArea->lvnum; vnum <= pArea->uvnum; vnum++ )
    {
	if( ( pMobIndex = get_mob_index(vnum) )
	  && !pMobIndex->deleted)
	{
	    if ( pMobIndex->area == pArea && pMobIndex->spec_fun )
            {
                fprintf( fp, "M %d %s \t* %s\n", pMobIndex->vnum,
		    spec_string( pMobIndex->spec_fun ),
		    pMobIndex->short_descr );
            }
        }
    }

    fprintf( fp, "S\n\n\n\n" );
    return;
}



/* OLC 1.1b */
/*****************************************************************************
 Name:		save_resets
 Purpose:	Saves the #RESETS section of an area file.
                New format thanks to Rac.
 Called by:	save_area(olc_save.c)
 ****************************************************************************/
void save_resets( FILE *fp, AREA_DATA *pArea )
{
    RESET_DATA *pReset;
    MOB_INDEX_DATA *pLastMob = NULL;
    OBJ_INDEX_DATA *pLastObj = NULL;
    ROOM_INDEX_DATA *pRoomIndex;
    int vnum;

    fprintf( fp, "#RESETS\n" );

    for( vnum = pArea->lvnum; vnum <= pArea->uvnum; vnum++ )
    {
	if( ( pRoomIndex = get_room_index(vnum) )
	  && !pRoomIndex->deleted)
	{
	    pLastMob = NULL;
	    pLastObj = NULL;

	    if ( pRoomIndex->area == pArea )
	    {

    for ( pReset = pRoomIndex->reset_first; pReset; pReset = pReset->next )
    {
	switch ( pReset->command )
	{
	default:
	    bugf( "Save_resets: bad command %c.", pReset->command );
	    break;

	case 'M':
            pLastMob = get_mob_index( pReset->arg1 );
            if ( !pLastMob )
            {
                bugf("Save_resets: !NO_MOB! in room %d of [%s]", vnum, pArea->filename );
		break;
            }
	    if (pLastMob->deleted)
		break;
	    fprintf( fp, "M %5d %5d %5d %5d  * %s\n",
	        pReset->arg1,
                pReset->arg2,
                pReset->arg3,
		pReset->arg4,
		pLastMob->short_descr
		);
            break;

	case 'O':
            pLastObj = get_obj_index( pReset->arg1 );
	    if(!pLastObj) {
                bugf("Save_resets: !NO_OBJ! in room %d of [%s]", vnum, pArea->filename );
		break;
	    }
	    if (pLastObj->deleted)
		break;
	    fprintf( fp, "O %5d %5d %5d %5d * %s\n",
	        pReset->arg1,
                pReset->arg2,
                pReset->arg3,
                pReset->arg4,
		pLastObj->short_descr
		);
            break;

	case 'P':
            pLastObj = get_obj_index( pReset->arg1 );
	    if(!pLastObj) {
                bugf("Save_resets: !NO_OBJ! in room %d of [%s]", vnum, pArea->filename );
		break;
	    }
	    if (pLastObj->deleted)
		break;
	    fprintf( fp, "P %5d %5d %5d %5d  * - %s\n",
	        pReset->arg1,
		pReset->arg2,
                pReset->arg3,
		pReset->arg4,
		pLastObj->short_descr
		);
            break;

	case 'G':
            if ( !pLastMob )
            {
                bugf("Save_resets: !NO_MOB! in room %d of [%s]", vnum, pArea->filename );
		break;
            }
	    if (pLastMob->deleted)
		break;
            pLastObj = get_obj_index( pReset->arg1 );
	    if(!pLastObj) {
                bugf("Save_resets: !NO_OBJ! in room %d of [%s]", vnum, pArea->filename );
		break;
	    }
	    fprintf( fp, "G %5d %5d              * - %s\n",
		pReset->arg1,
		pReset->arg2,
		pLastObj->short_descr
		);
            break;

	case 'E':
            if ( !pLastMob )
            {
                bugf( "Save_resets: !NO_MOB! in room %d of [%s]", vnum, pArea->filename );
		break;
            }
	    if (pLastMob->deleted)
		break;
            pLastObj = get_obj_index( pReset->arg1 );
	    if(!pLastObj) {
                bugf("Save_resets: !NO_OBJ! in room %d of [%s]", vnum, pArea->filename );
		break;
	    }
	    fprintf( fp, "E %5d %5d %5d        * - %s\n",
	        pReset->arg1,
		pReset->arg2,
                pReset->arg3,
		pLastObj->short_descr
		);
            break;

	case 'D':
            break;

	case 'R':
	    fprintf( fp, "R %d %d\n",
	        pReset->arg1,
                pReset->arg2 );
            break;
        }	/* End switch */
    }	/* End for pReset */
	    }	/* End if correct area */
	}	/* End if pRoomIndex */
    }	/* End for vnum */
    fprintf( fp, "S\n\n\n\n" );
    return;
}



/* OLC 1.1b */
/*****************************************************************************
 Name:		save_resets
 Purpose:	Saves the #RESETS section of an area file.
                New format thanks to Rac.
 Called by:	save_area(olc_save.c)
 ****************************************************************************/
void vsave_resets( FILE *fp, AREA_DATA *pArea )
{
    RESET_DATA *pReset;
    MOB_INDEX_DATA *pLastMob = NULL;
    OBJ_INDEX_DATA *pLastObj;
    ROOM_INDEX_DATA *pRoomIndex;
    int vnum;

    fprintf( fp, "#RESETS\n" );

    for( vnum = pArea->lvnum; vnum <= pArea->uvnum; vnum++ )
    {
	if( ( pRoomIndex = get_room_index(vnum) )
	  && !pRoomIndex->deleted)
	{
	    if ( pRoomIndex->area == pArea )
	    {
    for ( pReset = pRoomIndex->reset_first; pReset; pReset = pReset->next )
    {
	switch ( pReset->command )
	{
	default:
	    bugf( "Save_resets: bad command %c.", pReset->command );
	    break;

	case 'M':
            pLastMob = get_mob_index( pReset->arg1 );
	    if (pLastMob->deleted)
		break;
	    fprintf( fp, "M 0 %d %2d %-5d %2d \t* %s to %s\n",
	        pReset->arg1,
                pReset->arg2,
                pReset->arg3,
		pReset->arg4,
                pLastMob->short_descr,
                pRoomIndex->name );
            break;

	case 'O':
            pLastObj = get_obj_index( pReset->arg1 );
	    if (pLastObj->deleted)
		break;
	    fprintf( fp, "O 0 %d  0 %-5d \t* %s to %s\n",
	        pReset->arg1,
                pReset->arg3,
                capitalize(pLastObj->short_descr),
                pRoomIndex->name );
            break;

	case 'P':
            pLastObj = get_obj_index( pReset->arg1 );
	    if (pLastObj->deleted)
		break;
	    fprintf( fp, "P 0 %d %d %-5d %2d \t* %s inside %s\n",
	        pReset->arg1,
		pReset->arg2,
                pReset->arg3,
		pReset->arg4,
                capitalize(get_obj_index( pReset->arg1 )->short_descr),
                pLastObj ? pLastObj->short_descr : "!NO_OBJ!" );

            if ( !pLastObj )	/* Thanks Rac! */
            {
                bugf( "Save_resets: P with !NO_OBJ! in [%s]", pArea->filename );
            }

            break;

	case 'G':
            pLastObj = get_obj_index( pReset->arg1 );
	    if (pLastObj->deleted)
		break;
	    fprintf( fp, "G 0 %d %d      \t*   %s\n",
	        pReset->arg1,pReset->arg2,
	        capitalize(pLastObj->short_descr) );

            if ( !pLastMob )
            {
                bugf( "Save_resets: !NO_MOB! in [%s]", pArea->filename );
            }
            break;

	case 'E':
	    fprintf( fp, "E 0 %d %d %-5d \t*   %s %s\n",
	        pReset->arg1,
		pReset->arg2,
                pReset->arg3,
                capitalize(get_obj_index( pReset->arg1 )->short_descr),
                option_find_value( pReset->arg3, wear_loc_options ) );
            if ( !pLastMob )
            {
                bugf( "Save_resets: !NO_MOB! in [%s]", pArea->filename );
            }
            break;

	case 'D':
            break;

	case 'R':
	    fprintf( fp, "R 0 %d %2d      \t* Randomize %s\n",
	        pReset->arg1,
                pReset->arg2,
                pRoomIndex->name );
            break;
        }	/* End switch */
    }	/* End for pReset */
	    }	/* End if correct area */
	}	/* End if pRoomIndex */
    }	/* End for vnum */
    fprintf( fp, "S\n\n\n\n" );
    return;
}



/* OLC 1.1b */
/*****************************************************************************
 Name:		save_shops
 Purpose:	Saves the #SHOPS section of an area file.
 Called by:	save_area(olc_save.c)
 ****************************************************************************/
void save_shops( FILE *fp, AREA_DATA *pArea )
{
    SHOP_DATA *pShopIndex;
    MOB_INDEX_DATA *pMobIndex;
    int iTrade;
    int vnum;

    fprintf( fp, "#SHOPS1\n" );

    for( vnum = pArea->lvnum; vnum <= pArea->uvnum; vnum++ )
    {
	if( ( pMobIndex = get_mob_index(vnum) )
	   && !pMobIndex->deleted)
	{
	    if ( pMobIndex->area == pArea && pMobIndex->pShop )
            {
                pShopIndex = pMobIndex->pShop;

                fprintf( fp, "%d ", pShopIndex->keeper );
                for ( iTrade = 0; iTrade < MAX_TRADE; iTrade++ )
                {
                    if ( pShopIndex->buy_type[iTrade] != 0 )
                    {
                       fprintf( fp, "%d ", pShopIndex->buy_type[iTrade] );
                    }
                    else
                       fprintf( fp, "0 ");
                }
                fprintf( fp, "%d %d ", pShopIndex->profit_buy, pShopIndex->profit_sell );
                fprintf( fp, "%d %d ", pShopIndex->open_hour, pShopIndex->close_hour );
		fprintf( fp, "%d\n", pShopIndex->type );
            }
        }
    }

    fprintf( fp, "0\n\n\n\n" );
    return;
}



/* OLC 1.1b */
/*****************************************************************************
 Name:		vsave_shops
 Purpose:	Saves the #SHOPS section of an area file.
                New formating thanks to Rac.
 Called by:	save_area(olc_save.c)
 ****************************************************************************/
void vsave_shops( FILE *fp, AREA_DATA *pArea )
{
    SHOP_DATA *pShopIndex;
    MOB_INDEX_DATA *pMobIndex;
    int iTrade;
    int vnum;

    fprintf( fp, "#SHOPS1\n" );

    for( vnum = pArea->lvnum; vnum <= pArea->uvnum; vnum++ )
    {
	if( ( pMobIndex = get_mob_index(vnum) )
	   && !pMobIndex->deleted)
	{
	    if ( pMobIndex->area == pArea && pMobIndex->pShop )
            {
                pShopIndex = pMobIndex->pShop;

                fprintf( fp, "%d ", pShopIndex->keeper );
                for ( iTrade = 0; iTrade < MAX_TRADE; iTrade++ )
                {
                    if ( pShopIndex->buy_type[iTrade] != 0 )
                    {
                       fprintf( fp, "%2d ", pShopIndex->buy_type[iTrade] );
                    }
                    else
                       fprintf( fp, " 0 ");
                }
                fprintf( fp, "%3d %3d ", pShopIndex->profit_buy, pShopIndex->profit_sell );
                fprintf( fp, "%2d %2d ", pShopIndex->open_hour, pShopIndex->close_hour );
		fprintf( fp, "%2d ", pShopIndex->type );
                fprintf( fp, "\t* %s\n", get_mob_index( pShopIndex->keeper )->short_descr );
            }
        }
    }

    fprintf( fp, "0\n\n\n\n" );
    return;
}



/*****************************************************************************
 Name:		save_helps
 Purpose:	Save #HELPS section of an area file.
 Written by:	Walker <nkrendel@evans.Denver.Colorado.EDU>
 Called by:	save_area(olc_save.c).
 ****************************************************************************/
void save_helps( FILE *fp, AREA_DATA *pArea )
{
    HELP_DATA *pHelp;
    bool found = FALSE;

    for( pHelp = help_first; pHelp; pHelp = pHelp->next )
    {
        if( pHelp->area && pHelp->area == pArea && !pHelp->deleted)
	{
	    if( !found )
	    {
		fprintf( fp, "#HELPS\n\n" );
		found = TRUE;
	    }
	    fprintf( fp, "%d %s~\n%s~\n",
		pHelp->level,
		all_capitalize( pHelp->keyword ),
		fix_string( pHelp->text ) );
	}
    }

    if( found )
        fprintf( fp, "\n0 $~\n\n" );

    return;
}

void save_mobprogs( FILE *fp, AREA_DATA *pArea )
{
    TCLPROG_CODE *pCodeIndex;
    int vnum;

    fprintf( fp, "#MOBPROGS\n" );

    for (vnum = pArea->lvnum; vnum <= pArea->uvnum; vnum++ ) {
	if ((pCodeIndex=get_mprog_index(vnum))==NULL) continue;
	if (pCodeIndex->deleted)
	    continue;

	fprintf( fp, "#%d\n", pCodeIndex->vnum);
	if (pCodeIndex->title)
	    fprintf( fp, "Title %s~\n", pCodeIndex->title);
	fprintf( fp, "%s~\n", fix_string( pCodeIndex->code ) );
    }

    fprintf( fp, "#0\n\n\n\n" );

    return;
}

void save_objprogs( FILE *fp, AREA_DATA *pArea )
{
    TCLPROG_CODE *pCodeIndex;
    int vnum;

    fprintf( fp, "#OBJPROGS\n" );

    for( vnum = pArea->lvnum; vnum <= pArea->uvnum; vnum++ )
    {
	if ((pCodeIndex=get_oprog_index(vnum))==NULL) continue;
	if (pCodeIndex->deleted)
	    continue;

	fprintf( fp, "#%d\n", pCodeIndex->vnum);
	if (pCodeIndex->title)
	    fprintf( fp, "Title %s~\n", pCodeIndex->title);
	fprintf( fp, "%s~\n", fix_string( pCodeIndex->code ) );
    }

    fprintf( fp, "#0\n\n\n\n" );

    return;
}

void save_roomprogs( FILE *fp, AREA_DATA *pArea )
{
    TCLPROG_CODE *pCodeIndex;
    int vnum;

    fprintf( fp, "#ROOMPROGS\n" );

    for( vnum = pArea->lvnum; vnum <= pArea->uvnum; vnum++ )
    {
	if ((pCodeIndex=get_rprog_index(vnum))==NULL) continue;
	if (pCodeIndex->deleted)
	    continue;

	fprintf( fp, "#%d\n", pCodeIndex->vnum);
	if (pCodeIndex->title)
	    fprintf( fp, "Title %s~\n", pCodeIndex->title);
	fprintf( fp, "%s~\n", fix_string( pCodeIndex->code ) );
    }

    fprintf( fp, "#0\n\n\n\n" );

    return;
}

void save_areaprogs( FILE *fp, AREA_DATA *pArea )
{
    TCLPROG_CODE *pCodeIndex;

    // if there is an area-program, save it
    if ((pCodeIndex=pArea->aprog)!=NULL
    &&  !pCodeIndex->deleted) {
	fprintf( fp, "#AREAPROGS\n#%d\n",pArea->vnum );

	if (pCodeIndex->title)
	    fprintf( fp, "Title %s~\n", pCodeIndex->title);
	fprintf( fp, "%s~\n", fix_string( pCodeIndex->code ) );

	fprintf( fp, "#0\n\n\n\n" );
    }

    // if there is an area-wide program, save it
    if (pArea->areaprogram[0]!=0) {
	fprintf( fp, "#AREAPROG\n");

	fprintf( fp, "%s~\n", fix_string( pArea->areaprogram ) );
    }

    return;
}

/*****************************************************************************
 Name:		do_asave
 Purpose:	Entry point for saving area data.
 Called by:	interpreter(interp.c)
 ****************************************************************************/
void save_socials(void) {
    FILE *fp;
    SOCIAL_TYPE *social;
    int i;
    char renamefile[MSL];

    strcpy(renamefile,mud_data.social_file);
    strcat(renamefile,"~");
    rename(mud_data.social_file,renamefile);

    fclose(fpReserve);
    if ( !( fp = fopen( mud_data.social_file, "w" ) ) ) {
	logf("[0] save_socials(): fopen(%s): %s",mud_data.social_file,ERROR);
	bugf("OLC: Couldn't open %s, saved in %s",
	    mud_data.social_file,renamefile);
	fpReserve = fopen( NULL_FILE, "r" );
	return;
    }

    fprintf(fp,"#SOCIALS\n\n");

    for (i=0;i<MAX_SOCIAL_HASH;i++) {
	social=social_hash[i];
	while (social) {
	    if (social->name[0] && !social->deleted) {
		fprintf(fp,"%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n\n",
		social->name,
		!social->char_no_arg || !social->char_no_arg[0] ?
		    "$":social->char_no_arg,
		!social->others_no_arg || !social->others_no_arg[0] ?
		    "$":social->others_no_arg,
		!social->char_found || !social->char_found[0] ?
		    "$":social->char_found,
		!social->others_found || !social->others_found[0] ?
		    "$":social->others_found,
		!social->vict_found || !social->vict_found[0] ?
		    "$":social->vict_found,
		!social->char_not_found || !social->char_not_found[0]?
		    "$":social->char_not_found,
		!social->char_auto || !social->char_auto[0] ?
		    "$":social->char_auto,
		!social->others_auto || !social->others_auto[0] ?
		    "$":social->others_auto);
	    }
	    social=social->next;
	}
    }

    fprintf(fp,"\n#0\n#$\n");

    fclose(fpReserve);
    fpReserve = fopen( NULL_FILE, "r" );

    wiznet(WIZ_ASAVE,0,NULL,NULL,"Socials saved.");
}


/*****************************************************************************
 Name:		save_area
 Purpose:	Save an area, note that this format is new.
 Called by:	do_asave(olc_save.c).
 ****************************************************************************/
void save_area( AREA_DATA *pArea )
{
    char renamefile[MSL];
    char filename[MSL];
    FILE *fp;

    sprintf(filename,"%s/%s",mud_data.area_dir,pArea->filename);
    strcpy(renamefile,filename);
    strcat(renamefile,"~");
    rename(filename,renamefile);

    fclose( fpReserve );
    if ( !( fp = fopen(filename, "w" ) ) )
    {
	logf("[0] save_area(): fopen(%s): %s",filename,ERROR);
	bugf("OLC: Couldn't open %s for writing, saved as %s~",
	    filename,renamefile);
	fpReserve = fopen( NULL_FILE, "r" );
	return;
    }


    /*
     * Version
     *  0	never issued, stock rom
     *  1	stringbits
     *  2	disentangle levels, creator and area-name
     *  3	act has become stringbits
     *	4	arg2 removed from effects
     *	5	change in layout of properties
     *  6	O resets get 4 arguments
     *
     */

    fprintf( fp, "#AREADATA\n" );
    fprintf( fp, "Name        %s~\n",        pArea->name );
    fprintf( fp, "Builders    %s~\n",        fix_string( pArea->builders ) );
    fprintf( fp, "VNUMs       %d %d\n",      pArea->lvnum, pArea->uvnum );
    fprintf( fp, "Level       %d %d\n",      pArea->llevel, pArea->ulevel );
    fprintf( fp, "Creator     %s~\n",        pArea->creator);
    fprintf( fp, "Security    %d\n",         pArea->security );
    fprintf( fp, "Recall      %d\n",         pArea->recall );
    fprintf( fp, "Version     %d\n",         6 );
    fprintf( fp, "Urlprefix   %s~\n",        pArea->urlprefix );
    /* Don't save the first 5 area flags */
    fprintf( fp, "Flags       %ld\n",        pArea->area_flags&~31 );
    fprintf( fp, "Comment     %s~\n",        pArea->comment);
    fprintf( fp, "Description %s~\n",        pArea->description);
    fprintf( fp, "ProgInstance %d\n",	     pArea->aprog_enabled?pArea->vnum:0);
    fprintf( fp, "Login_room %d\n",          pArea->login_room);
    if (pArea->conquered_by)
	fprintf( fp, "Conquered   %s~\n",    pArea->conquered_by->name);
    fprintf( fp, "End\n\n\n\n" );

    save_property_indexes( fp, pArea );

    save_helps( fp, pArea );				/* OLC 1.1b */
    save_mobiles( fp, pArea );
    save_objects( fp, pArea );
    save_rooms( fp, pArea );

    if ( IS_SET(pArea->area_flags, AREA_VERBOSE) )	/* OLC 1.1b */
    {
	vsave_specials( fp, pArea );
	vsave_resets( fp, pArea );
	vsave_shops( fp, pArea );
    }
    else
    {
	save_specials( fp, pArea );
	save_resets( fp, pArea );
	save_shops( fp, pArea );
    }

    save_mobprogs( fp, pArea );
    save_objprogs( fp, pArea );
    save_roomprogs( fp, pArea );
    save_areaprogs( fp, pArea );

    fprintf( fp, "#$\n" );

    fclose( fp );
    fpReserve = fopen( NULL_FILE, "r" );
    return;
}


/* OLC 1.1b */
/*****************************************************************************
 Name:		do_asave
 Purpose:	Entry point for saving area data.
 Called by:	interpreter(interp.c)
 ****************************************************************************/
void do_asave( CHAR_DATA *ch, char *argument )
{
    char arg1 [MAX_INPUT_LENGTH];
    AREA_DATA *pArea;
    int value;

    if ( !ch )       /* Do an autosave */
    {
	save_area_list();
	for( pArea = area_first; pArea; pArea = pArea->next )
	{
	    if(IS_SET(pArea->area_flags, AREA_CHANGED )
	    || IS_SET(pArea->area_flags, AREA_ADDED ) )
	    {
	    	save_area( pArea );
	    	REMOVE_BIT( pArea->area_flags, AREA_CHANGED | AREA_ADDED );
		wiznet(WIZ_ASAVE,0,NULL,NULL,
		    "%s autosaved into %s.",pArea->name,pArea->filename);
	    }
	}
	return;
    }

    argument = one_argument( argument, arg1 );

    if ( arg1[0] == '\0' )
    {
    send_to_char( "Syntax:\n\r", ch );
    send_to_char( "  asave <vnum>    - saves a particular area\n\r",	 ch );
    send_to_char( "  asave list      - saves the area.lst file\n\r",	 ch );
    send_to_char( "  asave area      - saves the area being edited\n\r", ch );
    send_to_char( "  asave changed   - saves all changed zones\n\r",	 ch );
    send_to_char( "  asave world     - saves the world! (db dump)\n\r",	 ch );
    send_to_char( "  asave ^ verbose - saves in verbose mode\n\r", ch );
    send_to_char( "\n\r", ch );
        return;
    }

    /* Snarf the value (which need not be numeric). */
    value = atoi( arg1 );

    /* Save the area of given vnum. */
    /* ---------------------------- */

    if ( !( pArea = get_area_data( value ) ) && is_number( arg1 ) )
    {
	send_to_char( "That area does not exist.\n\r", ch );
	return;
    }

    if ( is_number( arg1 ) )
    {
	if ( !IS_BUILDER( ch, pArea ) )
	{
	    send_to_char( "You are not a builder for this area.\n\r", ch );
	    return;
	}

	save_area_list();
	if ( !str_cmp( "verbose", argument ) )
	    SET_BIT( pArea->area_flags, AREA_VERBOSE );
	save_area( pArea );
	REMOVE_BIT( pArea->area_flags, AREA_VERBOSE );
	sprintf_to_char(ch,"Saving %24s - '%s'\n\r",pArea->name,pArea->filename);
	return;
    }

    /* Save the world, only authorized areas. */
    /* -------------------------------------- */

    if ( !str_cmp( "world", arg1 ) )
    {
	save_area_list();
	for( pArea = area_first; pArea; pArea = pArea->next )
	{
	    /* Builder must be assigned this area. */
	    if ( !IS_BUILDER( ch, pArea ) )
		continue;

	    if ( !str_cmp( "verbose", argument ) )
		SET_BIT( pArea->area_flags, AREA_VERBOSE );
	    save_area( pArea );
	    REMOVE_BIT( pArea->area_flags, AREA_CHANGED | AREA_ADDED | AREA_VERBOSE );
	}
	send_to_char( "You saved the world.\n\r", ch );
	send_to_all_char( "Database saved.\n\r" );
	return;
    }

    /* Save changed areas, only authorized areas. */
    /* ------------------------------------------ */

    if ( !str_cmp( "changed", arg1 ) )
    {
	char buf[MAX_INPUT_LENGTH];

	save_area_list();

	send_to_char( "Saved zones:\n\r", ch );
	sprintf( buf, "None.\n\r" );

	for( pArea = area_first; pArea; pArea = pArea->next )
	{
	    /* Builder must be assigned this area. */
	    if ( !IS_BUILDER( ch, pArea ) )
		continue;

	    /* Save changed areas. */
	    if ( IS_SET(pArea->area_flags, AREA_CHANGED)
	      || IS_SET(pArea->area_flags, AREA_ADDED) )
	    {
		if ( !str_cmp( "verbose", argument ) )
		    SET_BIT( pArea->area_flags, AREA_VERBOSE );
		save_area( pArea );
		REMOVE_BIT( pArea->area_flags, AREA_CHANGED | AREA_ADDED | AREA_VERBOSE );
		sprintf( buf, "%24s - '%s'\n\r", pArea->name, pArea->filename );
		send_to_char( buf, ch );
	    }
        }
	if ( !str_cmp( buf, "None.\n\r" ) )
	    send_to_char( buf, ch );
        return;
    }

    /* Save the area.lst file. */
    /* ----------------------- */
    if ( !str_cmp( arg1, "list" ) )
    {
	save_area_list();
	return;
    }

    /* Save area being edited, if authorized. */
    /* -------------------------------------- */
    if ( !str_cmp( arg1, "area" ) )
    {
	/* Find the area to save. */
	switch (ch->desc->editor)
	{
	    case ED_AREA:
		pArea = (AREA_DATA *)ch->desc->pEdit;
		break;
	    case ED_ROOM:
		pArea = ch->in_room->area;
		break;
	    case ED_OBJECT:
		pArea = ( (OBJ_INDEX_DATA *)ch->desc->pEdit )->area;
		break;
	    case ED_MOBILE:
		pArea = ( (MOB_INDEX_DATA *)ch->desc->pEdit )->area;
		break;
	    default:
		pArea = ch->in_room->area;
		break;
	}

	if ( !IS_BUILDER( ch, pArea ) )
	{
	    send_to_char( "You are not a builder for this area.\n\r", ch );
	    return;
	}

	save_area_list();
	if ( !str_cmp( "verbose", argument ) )
	    SET_BIT( pArea->area_flags, AREA_VERBOSE );
	save_area( pArea );
	REMOVE_BIT( pArea->area_flags, AREA_CHANGED | AREA_ADDED | AREA_VERBOSE );
	send_to_char( "Area saved.\n\r", ch );
	return;
    }

    /* Show correct syntax. */
    /* -------------------- */
    do_function(ch,&do_asave, "" );
    return;
}
