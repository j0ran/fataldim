#
# $Id: Makefile.FreeBSD,v 1.45 2008/01/12 18:34:21 jodocus Exp $
#

#
# This Makefile is optimized for FreeBSD.
#
# Feel free to change the "optional mud game settings"
# Feel free to change the "optional mud server settings"
# Be carefull with changing the "required mud server settings"
#

#
# programs
#
RM	= rm
CC      = gcc
CPP	= g++

#
# optional mud game settings
#
C_POP3	= -DHAS_POP3
L_POP3	=
O_POP3	= fd_pop3.o

C_HTTP	= -DHAS_HTTP
L_HTTP	=
O_HTTP	= fd_http.o

#C_HASALTS = -DHAS_ALTS
#L_HASALTS = 
#O_HASALTS =

C_I3	= -DI3
L_I3	= 
O_I3	=

C_MISC	= -DHAS_JUKEBOX
L_MISC	=
O_MISC	=

TUNABLES = -DMAX_STRING_LENGTH=4096

#
# optional mud server settings 
#
C_DNS	= -pthread -DTHREADED_DNS -D_THREAD_SAFE
L_DNS	= -pthread -D_THREAD_SAFE
O_DNS	= rdns_cache.o

C_MCCP	= -DHAS_MCCP
L_MCCP	= -lz
O_MCCP	= fd_mccp.o

C_INET6	= -DINET6
L_INET6	= 
O_INET6 =

#C_CRYPT	= -DNEEDS_CRYPT_H
L_CRYPT	= -lcrypt
O_CRYPT	=

#
# required mud server settings
#
.if !defined(USETCL)
.if exists(/usr/local/include/tcl8.5/tcl.h)
USETCL=8.5
.elif exists(/usr/local/include/tcl8.4/tcl.h)
USETCL=8.4
.elif exists(/usr/local/include/tcl8.3/tcl.h)
USETCL=8.3
.elif exists(/usr/local/include/tcl8.2/tcl.h)
USETCL=8.2
.else
.error can't find tcl. please adjust Makefile
.endif
.endif

.if $(USETCL)==8.5
C_TCL = -I/usr/local/include/tcl8.5
L_TCL = -L/usr/local/lib -ltcl85 -lm
O_TCL =
.elif $(USETCL)==8.4
C_TCL = -I/usr/local/include/tcl8.4
L_TCL = -L/usr/local/lib -ltcl84 -lm
O_TCL =
.elif $(USETCL)==8.3
C_TCL = -I/usr/local/include/tcl8.3
L_TCL = -L/usr/local/lib -ltcl83 -lm
O_TCL =
.elif $(USETCL)==8.2
C_TCL = -I/usr/local/include/tcl8.2
L_TCL = -L/usr/local/lib -ltcl82 -lm
O_TCL =
.else
.error unknown tcl version
.endif

#
# Other options, not related to mud setting but more to options during compiling
#
C_PROF = -pg
L_PROF = -pg
O_PROF =

C_SYSTEM = $(C_PROF) $(C_CRYPT) -Wall -g -DUNIX
L_SYSTEM = $(L_PROF) $(L_CRYPT)
O_SYSTEM = $(O_PROF) $(O_CRYPT)

#
# glue everything together
#
MY_CFLAGS = $(C_MISC) $(C_I3) $(C_DNS) $(C_INET6) $(C_MCCP) $(C_HASALTS) -DDIRENT -DOLD_RAND -DIDEA_BUG_VIA_WEB
MY_LFLAGS = $(L_MISC) $(L_I3) $(L_DNS) $(L_INET6) $(L_MCCP) $(L_HASALTS)
MY_OFILES = $(O_MISC) $(O_I3) $(O_DNS) $(O_INET6) $(O_MCCP) $(O_HASALTS)

C_FLAGS = $(C_SYSTEM) $(MY_CFLAGS) $(C_TCL) 
L_FLAGS = $(L_SYSTEM) $(MY_LFLAGS) $(L_TCL) 
O_FILES = $(O_SYSTEM) $(MY_OFILES) $(O_TCL) 

LIBS	= intermud3/fd_i3.a

SUBDIRS = intermud3

OFILES	= act_comm.o act_enter.o act_info.o act_move.o act_obj.o \
	  act_food.o act_equipment.o act_magic.o act_donate.o \
	  alias.o ban.o comm.o db.o db2.o effects.o fight.o fight_action.o \
	  handler.o healer.o interp.o note.o fd_lookup.o \
	  magic.o magic2.o magic_spells.o \
	  music.o recycle.o save.o scan.o skills.o special.o \
	  update.o string.o help.o \
	  wiz_stat.o wiz_set.o wiz_where.o wiz_misc.o \
	  olc.o olc_act.o olc_save.o \
	  olc_aedit.o olc_redit.o olc_medit.o olc_oedit.o \
	  olc_hedit.o olc_xpedit.o olc_socialedit.o olc_resetedit.o \
	  fd_swear.o fd_hunt.o \
	  fd_dreams.o fd_screen.o quest.o fd_property.o \
	  fd_act_config.o fd_audit.o fd_copyover.o fd_network.o \
	  fd_output.o fd_memory.o fd_shops.o fd_fileio.o fd_editor.o fd_math.o \
	  \
	  fd_progcmds.o fd_tcl_core.o fd_tcl_area.o fd_tcl_char.o \
	  fd_tcl_clan.o fd_tcl_data.o fd_tcl_misc.o fd_tcl_mob.o \
	  fd_tcl_mud.o fd_tcl_obj.o fd_tcl_room.o fd_tcl_trig.o \
	  fd_roomprog.o fd_mobprog.o fd_objprog.o fd_areaprog.o \
	  fd_tcl_const.o \
	  \
	  fd_clan.o fd_jobs.o fd_log.o fd_penalty.o fd_money.o \
	  fd_data_tables.o fd_data_options.o fd_data_const.o fd_conf.o \
	  fd_data_property.o $(O_FILES) \

# all source files that are generated automagically.
GFILES  = fd_config.h fd_skilltable.h fd_grouptable.h fd_skills_gsn.h \
	  fd_skills_gsn.c fd_spells.h fd_data_property.c

all: rom playerutil msgd

nolink: generated $(OFILES)

subdirs:
	@for i in $(SUBDIRS); do \
		echo "==> $$i"; \
		cd $$i; \
		make $(.TARGETS); \
		echo "==< $$i"; \
	done

.c.o:
	$(CC) $(C_FLAGS) -c $< -o $@

generated: $(GFILES)

fd_skilltable.h fd_grouptable.h fd_skills_gsn.h fd_skills_gsn.c fd_spells.h: ../tools/compilecst fataldimensions.cst
	../tools/compilecst

fd_config.h: Makefile configure
	./configure config $(C_FLAGS) $(TUNABLES)

fd_data_property.c: property.txt
	../tools/makeprops.pl

rom! generated subdirs $(OFILES)
	$(RM) -f rom
	$(CC) $(L_FLAGS) -o rom $(OFILES) $(LIBS)

playerutil: playerutil.o
	rm -f playerutil
	$(CC) $(C_FLAGS) $(L_FLAGS) -o playerutil playerutil.o

msgd: msgd.o
	$(RM) -f msgd
	$(CC) $(C_FLAGS) $(L_FLAGS) -o msgd msgd.o

clean: subdirs
	-$(RM) *.o playerutil $(GFILES)

dist-i3:
	$(RM) -f fd_i3.tar.gz
	tar zcvf fd_i3.tar.gz intermud3/*

depend: generated subdirs
	$(CC) -E -MM $(C_FLAGS) *.c > .depend
