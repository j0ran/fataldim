/* $Id: update.c,v 1.160 2008/05/01 19:21:42 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

/*
 * 19971214 EG Player is notified when his hp/mana points are full
 * 19980125 EG Player won't be noticed about the weather when he's blind
 * 19980222 EG Players are now dreaming...
 */

#include "merc.h"
#include "interp.h"

/*
 * Local functions.
 */
int	hit_gain	( CHAR_DATA *ch );
int	mana_gain	( CHAR_DATA *ch );
int	move_gain	( CHAR_DATA *ch );
void	mobile_update	( int start, int step );
void	weather_update	( void );
void	char_update	( int start, int step );
void	obj_update	( int start, int step );
void	aggr_update	( void );
void	tag_update	( void );
void	descriptor_update	( void );
void	water_update	( void );
void	quest_update	( void );	/* Quest-stuff */

/*
 * Advancement stuff.
 */
void advance_level( CHAR_DATA *ch, bool hide )
{
    char buf[MAX_STRING_LENGTH];
    int add_hp;
    int add_mana;
    int add_move;
    int add_prac;
    OBJ_DATA *obj,*objnext;
    int levelmin,levelmax,i;

    ch->pcdata->last_level =
            ( ch->played + (int) (current_time - ch->logon) ) / 3600;

    if(!STR_IS_SET(ch->strbit_act,PLR_FREEZETITLE))
    {
        sprintf( buf, "the %s",
                 title_table [ch->class] [ch->level] [show_sex(ch)==SEX_FEMALE ? 1:0] );
        set_title( ch, buf );
    }

    add_hp	= con_app[get_curr_stat(ch,STAT_CON)].hitp + number_range(
                class_table[ch->class].hp_min,
            class_table[ch->class].hp_max );
    add_hp = (add_hp * pc_race_table[ch->race].hp_mult)/100;

    add_mana = (number_range(
                    class_table[ch->class].mana_min,
                class_table[ch->class].mana_max)*
            (2*get_curr_stat(ch,STAT_INT)+get_curr_stat(ch,STAT_WIS)))/75;
    add_mana = (add_mana * pc_race_table[ch->race].mana_mult)/100;

    add_move	= number_range( 1, (get_curr_stat(ch,STAT_CON)
                                    + get_curr_stat(ch,STAT_DEX))/6 );
    add_move = (add_move * pc_race_table[ch->race].move_mult)/100;

    add_prac	= wis_app[get_curr_stat(ch,STAT_WIS)].practice;

    add_hp = (add_hp * 9)/10;
    add_move = (add_move * 9)/10;

    add_hp	= UMAX(  2, add_hp   );
    add_mana	= UMAX(  2, add_mana );
    add_move	= UMAX(  6, add_move );

    ch->max_hit 	+= add_hp;
    ch->max_mana	+= add_mana;
    ch->max_move	+= add_move;
    ch->practice	+= add_prac;
    ch->train		+= 1;

    ch->pcdata->perm_hit	+= add_hp;
    ch->pcdata->perm_mana	+= add_mana;
    ch->pcdata->perm_move	+= add_move;

    for (obj=ch->carrying;obj;obj=objnext) {
        objnext=obj->next_content;

        levelmin=levelmax=-1;
        GetObjectProperty(obj,PROPERTY_INT,"levelhavemin",&levelmin);
        GetObjectProperty(obj,PROPERTY_INT,"levelhavemax",&levelmax);
        if ((levelmin>=0 && ch->level<levelmin) ||
                (levelmax>=0 && ch->level>levelmax))
            zap_char_obj(obj,TRUE);
    }

    if (!hide) {
        sprintf(buf,
                "You gain %d hit point%s, %d mana, %d move, and %d practice%s.\n\r",
                add_hp, add_hp == 1 ? "" : "s", add_mana, add_move,
                add_prac, add_prac == 1 ? "" : "s");
        send_to_char( buf, ch );

        for (i=1;i<MAX_CHANNELS;i++)
            if (ch->level==mud_data.newbie_channel_level[i])
                sprintf_to_char(ch,
                                "Congratulations, you're now allowed to use the %s.\n\r",
                                channel_table[i].name);
        if (ch->level==mud_data.newbie_room_level)
            send_to_char(
                        "Well done, you're no longer a newbie. This means\n\r"
                        "you can't enter mudschool anymore.\n\r",ch);
    }
    return;
}



void gain_exp( CHAR_DATA *ch, int gain, bool noisy ) {
    if (IS_NPC(ch) || ch->level >= LEVEL_HERO )
        return;

    if (ch->level >= class_table[ch->class].spec_level) {
        send_to_char("{rWARNING!!!!{x\n\rYou can not gain XP until you specialize at your guildmaster.\n\r", ch);
        return;
    }

    if (gain>0 && STR_IS_SET(ch->strbit_act,PLR_HARDCORE)) {
        sprintf_to_char(ch,"You receive %d xp hardcore bonus.\n\r",gain);
        gain*=2;
    }

    ch->exp = UMAX( exp_per_level(ch,ch->pcdata->points), ch->exp + gain );
    while ( ch->level < LEVEL_HERO && ch->exp >=
            exp_per_level(ch,ch->pcdata->points) * (ch->level+1) ) {
        send_to_char( "You raise a level!!  ", ch );
        ch->level += 1;
        if (ch->level==UPPER_CLAN_LEVEL && ch->clan==NULL)
            send_to_char("Last chance to join a clan or become a loner!\n\r",ch);
        if (ch->level >= class_table[ch->class].spec_level) {
            send_to_char( "You have advanced enough to specialize.\n\r"
                          "You can not gain new XP until you specialize at your guildmaster.\n\r",
                          ch);
        }
        logf("[%d] %s gained level %d",GET_DESCRIPTOR(ch),ch->name,ch->level);
        wiznet(WIZ_LEVELS,0,ch,NULL,"$N has attained level %d!",ch->level);
        if (noisy) {
            announce(ch,"$N has attained level %d!",ch->level);
        }
        advance_level(ch,FALSE);
        save_char_obj(ch);
    }

    return;
}



/*
 * Regeneration stuff.
 */
int hit_gain( CHAR_DATA *ch )
{
    int gain;
    int room_multiplyer;
    EFFECT_DATA *eff;

    if (ch->in_room == NULL)
        return 0;

    if ( IS_NPC(ch) )
    {
        gain =  5 + ch->level;
        if (IS_AFFECTED(ch,EFF_REGENERATION))
            gain *= 2;

        switch(ch->position)
        {
        default : 		gain /= 2;			break;
        case POS_SLEEPING: 	gain = 3 * gain/2;		break;
        case POS_RESTING:  					break;
        case POS_FIGHTING:	gain /= 3;		 	break;
        }


    }
    else
    {
        gain = UMAX(3,get_curr_stat(ch,STAT_CON) - 3 + ch->level/2);
        gain += class_table[ch->class].hp_max - 10;
        if (skillcheck(ch,gsn_fast_healing)) {
            gain += number_percent() * gain / 100;
            if (ch->hit < ch->max_hit)
                check_improve(ch,gsn_fast_healing,TRUE,8);
        }

        switch ( ch->position )
        {
        default:	   	gain /= 4;			break;
        case POS_SLEEPING: 					break;
        case POS_RESTING:  	gain /= 2;			break;
        case POS_FIGHTING: 	gain /= 6;			break;
        }

        if ( ch->pcdata->condition[COND_HUNGER]   == 0 )
            gain /= 2;

        if ( ch->pcdata->condition[COND_THIRST] == 0 )
            gain /= 2;

    }

    if ((eff=effect_find(ch->affected,TO_EFFECTS,gsn_bandage))!=NULL)
        gain+=eff->modifier;

    if (!GetRoomProperty(ch->in_room,PROPERTY_INT,"healrate",&room_multiplyer))
        room_multiplyer=100;

    if (room_multiplyer<0 && IS_NPC(ch) && STR_IS_SET(ch->strbit_act,ACT_IGNORENEGHEALING))
        room_multiplyer=100;

    gain = gain * room_multiplyer / 100;

    if (ch->on != NULL && ch->on->item_type == ITEM_FURNITURE)
        gain = gain * ch->on->value[3] / 100;

    if ( IS_AFFECTED(ch, EFF_POISON) )
        gain /= 4;

    if (IS_AFFECTED(ch, EFF_PLAGUE))
        gain /= 8;

    if (IS_AFFECTED(ch,EFF_HASTE) || IS_AFFECTED(ch,EFF_SLOW))
        gain /=2 ;

    return(gain);
}



int mana_gain( CHAR_DATA *ch )
{
    int gain;
    int room_multiplyer;

    if (ch->in_room == NULL)
        return 0;

    if ( IS_NPC(ch) )
    {
        gain = 5 + ch->level;
        switch (ch->position)
        {
        default:		gain /= 2;		break;
        case POS_SLEEPING:	gain = 3 * gain/2;	break;
        case POS_RESTING:				break;
        case POS_FIGHTING:	gain /= 3;		break;
        }
    }
    else
    {
        gain = (get_curr_stat(ch,STAT_WIS)
                + get_curr_stat(ch,STAT_INT) + ch->level) / 2;
        if (skillcheck(ch,gsn_meditation)) {
            gain += number_percent() * gain / 100;
            if (ch->mana < ch->max_mana)
                check_improve(ch,gsn_meditation,TRUE,8);
        }
        if (!class_table[ch->class].fMana)
            gain /= 2;

        switch ( ch->position )
        {
        default:		gain /= 4;			break;
        case POS_SLEEPING: 					break;
        case POS_RESTING:	gain /= 2;			break;
        case POS_FIGHTING:	gain /= 6;			break;
        }

        if ( ch->pcdata->condition[COND_HUNGER]   == 0 )
            gain /= 2;

        if ( ch->pcdata->condition[COND_THIRST] == 0 )
            gain /= 2;

    }

    if (!GetRoomProperty(ch->in_room,PROPERTY_INT,"manarate",&room_multiplyer))
        room_multiplyer=100;

    if (room_multiplyer<0 && IS_NPC(ch) && STR_IS_SET(ch->strbit_act,ACT_IGNORENEGHEALING))
        room_multiplyer=100;

    gain = gain * room_multiplyer / 100;

    if (ch->on != NULL && ch->on->item_type == ITEM_FURNITURE)
        gain = gain * ch->on->value[4] / 100;

    if ( IS_AFFECTED( ch, EFF_POISON ) )
        gain /= 4;

    if (IS_AFFECTED(ch, EFF_PLAGUE))
        gain /= 8;

    if (IS_AFFECTED(ch,EFF_HASTE) || IS_AFFECTED(ch,EFF_SLOW))
        gain /=2 ;

    return(gain);
}



int move_gain( CHAR_DATA *ch )
{
    int gain;
    int room_multiplyer;

    if (ch->in_room == NULL)
        return 0;

    if ( IS_NPC(ch) )
    {
        gain = ch->level;
    }
    else
    {
        gain = UMAX( 15, ch->level );

        switch ( ch->position )
        {
        case POS_SLEEPING: gain += get_curr_stat(ch,STAT_DEX);		break;
        case POS_RESTING:  gain += get_curr_stat(ch,STAT_DEX) / 2;	break;
        }

        if ( ch->pcdata->condition[COND_HUNGER]   == 0 )
            gain /= 2;

        if ( ch->pcdata->condition[COND_THIRST] == 0 )
            gain /= 2;
    }

    if (!GetRoomProperty(ch->in_room,PROPERTY_INT,"moverate",&room_multiplyer) &&
            !GetRoomProperty(ch->in_room,PROPERTY_INT,"healrate",&room_multiplyer))
        room_multiplyer=100;

    if (room_multiplyer<0 && IS_NPC(ch) && STR_IS_SET(ch->strbit_act,ACT_IGNORENEGHEALING))
        room_multiplyer=100;

    gain = gain * room_multiplyer / 100;

    if (ch->on != NULL && ch->on->item_type == ITEM_FURNITURE)
        gain = gain * ch->on->value[3] / 100;

    if ( IS_AFFECTED(ch, EFF_POISON) )
        gain /= 4;

    if (IS_AFFECTED(ch, EFF_PLAGUE))
        gain /= 8;

    if (IS_AFFECTED(ch,EFF_HASTE) || IS_AFFECTED(ch,EFF_SLOW))
        gain /=2 ;

    return(gain);
}



void gain_condition( CHAR_DATA *ch, int iCond, int value )
{
    int condition;

    if (value==0) return;
    if (IS_NPC(ch)) return;
    if (ch->level>=LEVEL_IMMORTAL && iCond!=COND_ADRENALINE) return;

    condition = ch->pcdata->condition[iCond];
    ch->pcdata->condition[iCond] = URANGE( 0, condition + value, 48 );

    // just bail out in case of negative values, don't worry about them
    if (condition < 0)
        return;

    switch ( iCond )
    {
    case COND_ADRENALINE:
        if (ch->pcdata->condition[COND_ADRENALINE]==0 && condition!=0)
            send_to_char(
                        "You've caught your breath after your last fight.\n\r",ch);
        break;

    case COND_HUNGER:
        if (ch->desc && ch->desc->pStringBegin==NULL &&
                (ch->level!=LEVEL_HERO || !STR_IS_SET(ch->strbit_comm,COMM_SHOW_NOHUNGER)))
            switch (ch->pcdata->condition[COND_HUNGER]) {
            case 0:
                if (IS_AFFECTED(ch,EFF_HALLUCINATION))
                    send_to_char("The munchies are interfering with your motor capabilities.\n\r",ch);
                else
                    send_to_char("You are hungry.\n\r",ch);
                break;
            case 1:
                if (IS_AFFECTED(ch,EFF_HALLUCINATION))
                    send_to_char("You are getting the munchies.\n\r",ch);
                else
                    send_to_char("You feel hungry.\n\r",ch);
                break;
            case 2:
                if (IS_AFFECTED(ch,EFF_HALLUCINATION))
                    send_to_char("You start getting the munchies.\n\r",ch);
                else
                    send_to_char("You start to feel hungry.\n\r",ch);
                break;
            case 3:
                if (IS_AFFECTED(ch,EFF_HALLUCINATION))
                    send_to_char("You feel like getting the munchies.\n\r",ch);
                else
                    send_to_char("Your stomach feels empty.\n\r",ch);
                break;
            }
        break;

    case COND_THIRST:
        if (ch->desc && ch->desc->pStringBegin==NULL &&
                (ch->level!=LEVEL_HERO || !STR_IS_SET(ch->strbit_comm,COMM_SHOW_NOHUNGER)))
            switch (ch->pcdata->condition[COND_THIRST]) {
            case 0: send_to_char("You are thirsty.\n\r",ch);break;
            case 1: send_to_char("You feel thirsty.\n\r",ch);break;
            case 2: send_to_char("You start to feel thirsty.\n\r",ch);break;
            case 3: send_to_char("Your mouth feels dry.\n\r",ch);break;
            }
        break;

    case COND_DRUNK:
        if (condition!=0)
            switch (ch->pcdata->condition[COND_DRUNK]) {
            case 0: send_to_char("You are sober.\n\r",ch);break;
            case 1: send_to_char("You're still not steady.\n\r",ch);break;
            case 2: send_to_char("The world stops spinning.\n\r",ch);break;
            case 3: send_to_char("The world is not that much blurred anymore.\n\r",ch);break;
            }
        break;
    }

    return;
}

void phased_wandering(CHAR_DATA *ch) {
    EXIT_DATA *pexit=ch->phasedskill_target;
    EXIT_DATA *pexit_rev;
    int dir=ch->phasedskill_step;
    ROOM_DATA *from_room=ch->in_room;
    ROOM_DATA *to_room;

    if (from_room->exit[dir]!=pexit && from_room!=pexit->to_room) {
        set_phased_skill(ch,NULL,NULL,0,PHASED_NONE);
        return;
    }
    if (from_room!=pexit->to_room) {
        // unlock
        if (IS_SET(pexit->exit_info, EX_LOCKED)) {
            if (!STR_IS_SET(ch->strbit_act, ACT_LOCK_WANDERER)) {
                set_phased_skill(ch,NULL,NULL,0,PHASED_NONE);
                return;
            }
            REMOVE_BIT(pexit->exit_info, EX_LOCKED);
            send_to_char( "*Click*\n\r", ch );
            act( "$n unlocks the $d.", ch, NULL, pexit->keyword, TO_ROOM );

            /* unlock the other side */
            if ( ( to_room   = pexit->to_room            ) != NULL
                 &&   ( pexit_rev = to_room->exit[rev_dir[dir]] ) != NULL
                 &&   pexit_rev->to_room == ch->in_room )
            {
                REMOVE_BIT( pexit_rev->exit_info, EX_LOCKED );
            }
            return;
        }

        // open
        if (IS_SET(pexit->exit_info, EX_CLOSED)) {
            REMOVE_BIT(pexit->exit_info, EX_CLOSED);
            act( "$n opens the $d.", ch, NULL, pexit->keyword, TO_ROOM );

            /* unlock the other side */
            if ( ( to_room   = pexit->to_room            ) != NULL
                 &&   ( pexit_rev = to_room->exit[rev_dir[dir]] ) != NULL
                 &&   pexit_rev->to_room == ch->in_room )
            {
                REMOVE_BIT( pexit_rev->exit_info, EX_CLOSED );
            }
            return;
        }

        // move
        move_char(ch,dir,FALSE,FALSE);
    } else {
        // close
        if (!IS_SET(pexit->exit_info, EX_CLOSED)) {
            SET_BIT(pexit->exit_info, EX_CLOSED);
            act( "$n opens the $d.", ch, NULL, pexit->keyword, TO_ROOM );

            /* unlock the other side */
            if ( ( to_room   = pexit->to_room            ) != NULL
                 &&   ( pexit_rev = to_room->exit[rev_dir[dir]] ) != NULL
                 &&   pexit_rev->to_room->exit[dir] == pexit )
            {
                SET_BIT( pexit_rev->exit_info, EX_CLOSED );
            }
            return;
        }

        // lock
        if (!IS_SET(pexit->exit_info, EX_LOCKED) && IS_SET(pexit->rs_flags, EX_LOCKED)) {
            SET_BIT(pexit->exit_info, EX_LOCKED);

            /* unlock the other side */
            if ( ( to_room   = pexit->to_room            ) != NULL
                 &&   ( pexit_rev = to_room->exit[rev_dir[dir]] ) != NULL
                 &&   pexit_rev->to_room->exit[dir] == pexit )
            {
                SET_BIT( pexit_rev->exit_info, EX_LOCKED );
            }
            return;
        }

        // cleanup
        set_phased_skill(ch,NULL,NULL,0,PHASED_NONE);
    }
}

/*
 * Mob autonomous action.
 * This function takes 25% to 35% of ALL Merc cpu time.
 * -- Furey
 */
void mobile_update(int start, int step)
{
    CHAR_DATA *ch;
    CHAR_DATA *ch_next;
    EXIT_DATA *pexit;
    int door;

    /* Examine all mobs. */
    for ( ch = char_list; ch != NULL; ch = ch_next )
    {
        ch_next = ch->next;
        if (--start>0) continue;
        start=step;

        if ( !IS_NPC(ch) || ch->in_room == NULL || IS_AFFECTED(ch,EFF_CHARM))
            continue;

        if (ch->in_room->area->empty && !STR_IS_SET(ch->strbit_act,ACT_UPDATE_ALWAYS))
            continue;

        /* Examine call for special procedure */
        if ( ch->spec_fun != 0 )
        {
            if ( (*ch->spec_fun) ( ch ) )
                continue;
        }

        if (ch->pIndexData->pShop != NULL) /* give him some gold */
            if ((ch->gold * 100 + ch->silver) < ch->pIndexData->wealth)
            {
                ch->gold += ch->pIndexData->wealth * number_range(1,20)/5000000;
                ch->silver += ch->pIndexData->wealth * number_range(1,20)/50000;
            }

        /* Here remember a player in this room and if you havn't finished
         * your last fight with him. Finish it now.
     */

        {
            int ool_timeout;
            if (GetCharProperty(ch,PROPERTY_INT,"ool_timeout",&ool_timeout)) {
                if (ool_timeout--) {
                    SetCharProperty(ch,PROPERTY_INT,"ool_timeout",&ool_timeout);
                } else {
                    DeleteCharProperty(ch,PROPERTY_INT,"ool_level");
                    DeleteCharProperty(ch,PROPERTY_STRING,"ool_char");
                    DeleteCharProperty(ch,PROPERTY_INT,"ool_timeout");
                }
            }
        }
        /* That's all for sleeping / busy monster, and empty zones */
        if ( ch->position != POS_STANDING )
            continue;

        /* Scavenge */
        if ( STR_IS_SET(ch->strbit_act, ACT_SCAVENGER)
             &&   ch->in_room->contents != NULL
             &&   number_bits( 6 ) == 0
             &&   !(IS_AFFECTED(ch,EFF_BLIND)||is_affected(ch,gsn_blindness)))
        {
            OBJ_DATA *obj;
            OBJ_DATA *obj_best;
            int max;
            CHAR_DATA *gch;
            bool in_use;

            max         = 1;
            obj_best    = 0;
            for ( obj = ch->in_room->contents; obj; obj = obj->next_content )
            {
                if ( CAN_WEAR(obj, ITEM_TAKE)
                     && can_loot(ch, obj)
                     && can_see_obj(ch, obj)
                     && obj->cost > max  && obj->cost > 0) {
                    in_use=FALSE;
                    if (obj->in_room != NULL) {
                        for (gch = obj->in_room->people; gch != NULL; gch = gch->next_in_room) {
                            if (gch->on == obj) {
                                in_use=TRUE;
                                break;
                            }
                        }
                    }
                    if (in_use) continue;

                    obj_best    = obj;
                    max         = obj->cost;
                }
            }

            if ( obj_best )
            {
                obj_from_room( obj_best );
                obj_to_char( obj_best, ch );
                act( "$n gets $p.", ch, obj_best, NULL, TO_ROOM );
                if (mob_compare(ch,obj_best)>0)
                    wear_obj(ch,obj_best,TRUE,TRUE);
            }
        }

        /* Wander */
        if ( !STR_IS_SET(ch->strbit_act, ACT_SENTINEL)
             &&   !STR_IS_SET(ch->strbit_act, ACT_PET)
             && ch->phasedskill_proc==NULL
             && number_bits(3) == 0
             && ( door = number_bits( 5 ) ) < MAX_DIR
             && ( pexit = ch->in_room->exit[door] ) != NULL
             &&   pexit->to_room != NULL
             &&   !STR_IS_SET(pexit->to_room->strbit_room_flags, ROOM_NO_MOB)
             &&   !STR_IS_SET(pexit->to_room->strbit_room_flags, ROOM_DEATHTRAP)
             && ( !STR_IS_SET(ch->strbit_act, ACT_STAY_AREA)
                  ||   pexit->to_room->area == ch->in_room->area )
             && ( !STR_IS_SET(ch->strbit_act, ACT_OUTDOORS)
                  ||   !STR_IS_SET(pexit->to_room->strbit_room_flags,ROOM_INDOORS))
             && ( !STR_IS_SET(ch->strbit_act, ACT_INDOORS)
                  ||   STR_IS_SET(pexit->to_room->strbit_room_flags,ROOM_INDOORS)))
        {
            if ( !IS_SET(pexit->exit_info, EX_CLOSED) ||
                 IS_AFFECTED(ch,EFF_PASS_DOOR))
                move_char( ch, door, FALSE, FALSE );
            else
                if (STR_IS_SET(ch->strbit_act, ACT_DOOR_WANDERER) ||
                        STR_IS_SET(ch->strbit_act, ACT_LOCK_WANDERER))
                    set_phased_skill(ch,phased_wandering,pexit,door,PHASED_NONE);
        }
    }

    return;
}


static void weather_message(char *message) {
    CHAR_DATA *player;

    for ( player=player_list;player!=NULL;player=player->next_player ) {
        if ( IS_OUTSIDE(player)
             &&   player->in_room->sector_type!=SECT_WATER_BELOW
             &&   IS_AWAKE(player)
             &&   check_blind(player,FALSE)
             &&   !STR_IS_SET(player->strbit_comm,COMM_QUIET))
            send_to_char( message, player );
    }
}

static void weather_change(int new_cloud_state) {
    int old_cloud_state;

    old_cloud_state=weather_info.sky;
    weather_info.sky=new_cloud_state;

    mp_weather_trigger(old_cloud_state,new_cloud_state);
    op_weather_trigger(old_cloud_state,new_cloud_state);
    rp_weather_trigger(old_cloud_state,new_cloud_state);
    ap_weather_trigger(old_cloud_state,new_cloud_state);
}

/*
 * Update the weather.
 */
void weather_update( void )
{
    int diff;

    switch ( ++time_info.hour )
    {
    case  5:
        weather_info.sunlight = SUN_LIGHT;
        weather_message("The day has begun.\n\r" );
        mp_daystart_trigger();
        op_daystart_trigger();
        rp_daystart_trigger();
        ap_daystart_trigger();
        break;

    case  6:
        weather_info.sunlight = SUN_RISE;
        weather_message("The sun rises in the east.\n\r" );
        mp_sunrise_trigger();
        op_sunrise_trigger();
        rp_sunrise_trigger();
        ap_sunrise_trigger();
        break;

    case 19:
        weather_info.sunlight = SUN_SET;
        weather_message("The sun slowly disappears in the west.\n\r" );
        mp_sunset_trigger();
        op_sunset_trigger();
        rp_sunset_trigger();
        ap_sunset_trigger();
        break;

    case 20:
        weather_info.sunlight = SUN_DARK;
        weather_message("The night has begun.\n\r" );
        mp_dayend_trigger();
        op_dayend_trigger();
        rp_dayend_trigger();
        ap_dayend_trigger();
        break;

    case 24:
        time_info.hour = 0;
        time_info.day++;
        break;
    }

    if ( time_info.day   >= 35 )
    {
        time_info.day = 0;
        time_info.month++;
    }

    if ( time_info.month >= 17 )
    {
        time_info.month = 0;
        time_info.year++;
    }

    /*
     * Weather change.
     */
    if ( time_info.month >= 9 && time_info.month <= 16 )
        diff = weather_info.mmhg >  985 ? -2 : 2;
    else
        diff = weather_info.mmhg > 1015 ? -2 : 2;

    weather_info.change   += diff * dice(1, 4) + dice(2, 6) - dice(2, 6);
    weather_info.change    = UMAX(weather_info.change, -12);
    weather_info.change    = UMIN(weather_info.change,  12);

    weather_info.mmhg += weather_info.change;
    weather_info.mmhg  = UMAX(weather_info.mmhg,  960);
    weather_info.mmhg  = UMIN(weather_info.mmhg, 1040);

    switch ( weather_info.sky )
    {
    default:
        bugf( "Weather_update: bad sky %d.", weather_info.sky );
        weather_info.sky = SKY_CLOUDLESS;
        break;

    case SKY_CLOUDLESS:
        if ( weather_info.mmhg <  990
             || ( weather_info.mmhg < 1010 && number_bits( 2 ) == 0 ) )
        {
            weather_message("The sky is getting cloudy.\n\r" );
            weather_change(SKY_CLOUDY);
        }
        break;

    case SKY_CLOUDY:
        if ( weather_info.mmhg <  970
             || ( weather_info.mmhg <  990 && number_bits( 2 ) == 0 ) )
        {
            weather_message("It starts to rain.\n\r" );
            weather_change(SKY_RAINING);
        }

        if ( weather_info.mmhg > 1030 && number_bits( 2 ) == 0 )
        {
            weather_message("The clouds disappear.\n\r" );
            weather_change(SKY_CLOUDLESS);
        }
        break;

    case SKY_RAINING:
        if ( weather_info.mmhg <  970 && number_bits( 2 ) == 0 )
        {
            weather_message("Lightning flashes in the sky.\n\r" );
            weather_change(SKY_LIGHTNING);
        }

        if ( weather_info.mmhg > 1030
             || ( weather_info.mmhg > 1010 && number_bits( 2 ) == 0 ) )
        {
            weather_message("The rain stopped.\n\r" );
            weather_change(SKY_CLOUDY);
        }
        break;

    case SKY_LIGHTNING:
        if ( weather_info.mmhg > 1010
             || ( weather_info.mmhg >  990 && number_bits( 2 ) == 0 ) )
        {
            weather_message("The lightning has stopped.\n\r" );
            weather_change(SKY_RAINING);
            break;
        }
        break;
    }


    return;
}


/*
 * Update all chars, including mobs.
*/
void char_update(int start, int step)
{
    CHAR_DATA *ch;
    CHAR_DATA *ch_next;
    CHAR_DATA *ch_quit;

    ch_quit	= NULL;

    for ( ch = char_list; ch != NULL; ch = ch_next )
    {
        EFFECT_DATA *pef;
        EFFECT_DATA *pef_next;

        ch_next = ch->next;
        if (--start>0) continue;
        start=step;

#ifdef TEST
        if ( IS_HERO(ch)?((ch->desc==NULL) && (ch->timer>12)):(ch->timer>3) )
            ch_quit = ch;
#else
        if ( IS_HERO(ch)?((ch->desc==NULL) && (ch->timer>120)):(ch->timer>30) )
            ch_quit = ch;
#endif

        if (ch->position == POS_SLEEPING)
            update_dream(ch);

        if ( ch->position >= POS_STUNNED )
        {
            /* check to see if we need to go home */
            if (IS_NPC(ch)
                    && ((ch->zone != NULL && ch->zone != ch->in_room->area) ||
                        (ch->zone == NULL && STR_IS_SET((ch)->strbit_act, ACT_PET) && ch->master==NULL) )
                    && ch->desc == NULL &&  ch->fighting == NULL
                    && !IS_AFFECTED(ch,EFF_CHARM) && !STR_IS_SET((ch)->strbit_act, ACT_MUDWANDERER)
                    && number_percent() < 5)
            {
                act("$n wanders on home.",ch,NULL,NULL,TO_ROOM);
                extract_char(ch,TRUE);
                continue;
            }

            if (ch->hit<ch->max_hit) {
                int max_hp;
                ch->hit=UMIN(ch->max_hit,ch->hit+hit_gain(ch));
                if (GetCharProperty(ch,PROPERTY_INT,"max_hp",&max_hp)) {
                    ch->hit=UMIN(ch->hit,max_hp);
                }
                if (ch->hit==ch->max_hit) {
                    send_to_char("You are in perfect condition again!\n\r",ch);
                }
            }
            if (ch->mana<ch->max_mana) {
                ch->mana=UMIN(ch->max_mana,ch->mana+mana_gain(ch));
                ch->mana=UMAX(ch->mana,0);
                if (ch->mana==ch->max_mana)
                    send_to_char("Your mana pool is full again!\n\r",ch);
            }
            if ( ch->move < ch->max_move )
                ch->move=UMIN(ch->max_move,ch->move+move_gain(ch));
        }

        if (IS_PC(ch)) {
            int log_timeout;
            int ool_penalty;
            if (STR_IS_SET(ch->strbit_act,PLR_LOG) &&
                    GetCharProperty(ch,PROPERTY_INT,"log-timeout",&log_timeout)) {
                if (log_timeout==0) {
                    // log is over
                    DeleteCharProperty(ch,PROPERTY_INT,"log-timeout");
                    DeleteCharProperty(ch,PROPERTY_STRING,"log-reason");
                    STR_REMOVE_BIT(ch->strbit_act,PLR_LOG);
                } else {
                    log_timeout--;
                    SetCharProperty(ch,PROPERTY_INT,"log-timeout",&log_timeout);
                }
            }
            if (GetCharProperty(ch,PROPERTY_INT,"ool_penalty",&ool_penalty) &&
                    (ool_penalty<(time(NULL)-(3*24*60*60))))
                DeleteCharProperty(ch,PROPERTY_INT,"ool_penalty");

        }

        if (IS_PC(ch) && ch->fighting)
            berserker_rage(ch);

        if ( ch->position == POS_STUNNED )
            update_pos( ch );

        // taken out of the loop because it was never checked
        // for immortals.
        if (!IS_NPC(ch)) {
            int afktimeout=12;

            GetCharProperty(ch,PROPERTY_INT,"afk-timeout",&afktimeout);
            ch->timer++;		// normal timer

            if ( ch->timer >= afktimeout )
                if (!STR_IS_SET(ch->strbit_comm,COMM_AFK)) {
                    sprintf_to_char(ch,"%d ticks and you're {WOUT{x!\n\r",afktimeout);
                    set_afk_state(ch,TRUE,FALSE);
                }

            if ( (ch->position>POS_DEAD) &&
                 (IS_IMMORTAL(ch)?((ch->desc==NULL)&&(ch->timer>=48)):(ch->timer>=12))) {
                if ( ch->was_in_room == NULL && ch->in_room != NULL ) {
                    ch->was_in_room = ch->in_room;
                    if ( ch->fighting != NULL )
                        stop_fighting( ch, TRUE );
                    act( "$n disappears into the void.",
                         ch, NULL, NULL, TO_ROOM );
                    send_to_char( "You disappear into the void.\n\r", ch );
                    if (ch->level > 1)
                        save_char_obj( ch );
                    char_from_room( ch );
                    char_to_room( ch, get_room_index( ROOM_VNUM_LIMBO ) );
                }
            }
        }

        if ( !IS_NPC(ch) && ch->level < LEVEL_IMMORTAL )
        {
            OBJ_DATA *obj;

            // check the light-source
            if ( ( obj = get_eq_char( ch, WEAR_LIGHT ) ) != NULL
                 &&   obj->item_type == ITEM_LIGHT
                 &&   obj->value[2] > 0 )
            {
                if ( --obj->value[2] == 0 && ch->in_room != NULL )
                {
                    --ch->in_room->light;
                    act( "$p goes out.", ch, obj, NULL, TO_ROOM );
                    act( "$p flickers and goes out.", ch, obj, NULL, TO_CHAR );
                    extract_obj( obj );
                }
                else if ( obj->value[2] <= 5 && ch->in_room != NULL) {
                    act("$p flickers.",ch,obj,NULL,TO_CHAR);
                    if (obj->carried_by && IS_AFFECTED(obj->carried_by,EFF_HALLUCINATION))
                        send_to_char(
                                    "Batteries have not been invented yet.\n\r",
                                    obj->carried_by);
                }
            }

            // check the floating disc
            if (( obj = get_eq_char(ch,WEAR_FLOAT)) != NULL)
                if (obj->pIndexData->vnum==OBJ_VNUM_DISC && obj->timer<3)
                    send_to_char("Your floating disc flashes for a moment.\n\r",ch);


            if (ch->position == POS_DEAD) continue;

            // changed the conditions
            gain_condition( ch, COND_DRUNK,  -1 );
            gain_condition( ch, COND_FULL, ch->size > SIZE_MEDIUM ? -4 : -2 );
            if (IS_AFFECTED(ch,EFF_HASTE)) {
                gain_condition( ch, COND_THIRST, -2 );
                gain_condition( ch, COND_HUNGER, ch->size>SIZE_MEDIUM ? -4:-2);
            } else if (IS_AFFECTED(ch,EFF_SLOW)) {
                if (number_percent()<50) {
                    gain_condition( ch, COND_THIRST, -1 );
                    gain_condition( ch, COND_HUNGER,
                                    ch->size>SIZE_MEDIUM ? -2:-1);
                }
            } else {
                gain_condition( ch, COND_THIRST, -1 );
                gain_condition( ch, COND_HUNGER, ch->size>SIZE_MEDIUM ? -2:-1);
            }

            // build up some excitement during a fight, remove it afterwards
            if (ch->position==POS_FIGHTING) {
                gain_condition( ch, COND_ADRENALINE, 1 );

                if (IS_AFFECTED(ch,EFF_BERSERK) ||
                        is_affected(ch,gsn_berserk) ||
                        is_affected(ch,gsn_frenzy)) {
                    // one additional point increased
                    gain_condition( ch, COND_ADRENALINE, 1 );
                }

            } else {
                // let's party!
                if (IS_AFFECTED(ch,EFF_BERSERK) ||
                        is_affected(ch,gsn_berserk) ||
                        is_affected(ch,gsn_frenzy)) {
                    // if affected by berserk/frenzy, then no decreasing
                } else if (skillcheck(ch,gsn_meditation)) {
                    gain_condition( ch, COND_ADRENALINE, -2 );
                } else {
                    gain_condition( ch, COND_ADRENALINE, -1 );
                }
            }
        }

        for ( pef = ch->affected; pef != NULL; pef = pef_next )
        {
            pef_next	= pef->next;
            if ( pef->duration > 0 )
            {
                pef->duration--;
                if (number_range(0,4) == 0 && pef->level > 0)
                    pef->level--;  /* spell strength fades with time */
            }
            else if ( pef->duration < 0 )
                ;
            else
            {
                if ( pef_next == NULL
                     ||   pef_next->type != pef->type
                     ||   pef_next->duration > 0 )
                {
                    char *msgoff;
                    WEAROFF_FUN *wearoff_fun;

                    if ((wearoff_fun=skill_wearoff_fun(pef->type,ch))!=NULL)
                        wearoff_fun(ch);

                    if ( pef->type > 0 && (msgoff=skill_msg_off(pef->type,ch))) {
                        send_to_char( msgoff, ch );
                        send_to_char( "\n\r", ch );
                    }
                    if ((msgoff=skill_msg_off_room(pef->type,ch))) {
                        act(msgoff,ch,NULL,NULL,TO_ROOM);
                    }
                }
                effect_remove( ch, pef );
            }
        }

        if (IS_PC(ch)) update_skill_decay(ch);

        /*
     * Careful with the damages here,
     *   MUST NOT refer to ch after damage taken,
     *   as it may be lethal damage (on NPC).
     */

        if (is_affected(ch, gsn_plague) && ch != NULL)
        {
            EFFECT_DATA *ef, plague;
            CHAR_DATA *vch;
            int dam;
            int prev_pos;

            if (ch->in_room == NULL)
                continue;

            act("$n writhes in agony as plague sores erupt from $s skin.",
                ch,NULL,NULL,TO_ROOM);
            send_to_char("You writhe in agony from the plague.\n\r",ch);
            for ( ef = ch->affected; ef != NULL; ef = ef->next )
            {
                if (ef->type == gsn_plague)
                    break;
            }

            if (ef == NULL)
            {
                STR_REMOVE_BIT(ch->strbit_affected_by,EFF_PLAGUE);
                continue;
            }

            if (ef->level == 1)
                continue;

            ZEROVAR(&plague,EFFECT_DATA);
            plague.where	= TO_EFFECTS;
            plague.type 	= gsn_plague;
            plague.level 	= ef->level - 1;
            plague.duration 	= number_range(1,2 * plague.level);
            plague.location	= APPLY_STR;
            plague.modifier 	= -5;
            plague.bitvector 	= EFF_PLAGUE;

            for ( vch = ch->in_room->people; vch != NULL; vch = vch->next_in_room)
            {
                if (!saves_spell(plague.level - 2,vch,DAM_DISEASE)
                        &&  !IS_IMMORTAL(vch)
                        &&  !IS_AFFECTED(vch,EFF_PLAGUE) && number_bits(4) == 0)
                {
                    send_to_char("You feel hot and feverish.\n\r",vch);
                    act("$n shivers and looks very ill.",vch,NULL,NULL,TO_ROOM);
                    effect_join(vch,&plague);
                }
            }

            dam = UMIN(ch->level,ef->level/5+1);
            ch->mana -= dam;
            ch->move -= dam;
            prev_pos=ch->position;
            damage( ch, ch, dam, gsn_plague,DAM_DISEASE,FALSE, NULL);
            if (prev_pos==POS_SLEEPING)
                ch->position=POS_RESTING;
        }
        else if ( IS_AFFECTED(ch, EFF_POISON) && ch != NULL
                  &&   !IS_AFFECTED(ch,EFF_SLOW))

        {
            EFFECT_DATA *poison;

            poison = effect_find(ch->affected,TO_EFFECTS,gsn_poison);

            if (poison != NULL)
            {
                act( "$n shivers and suffers.", ch, NULL, NULL, TO_ROOM );
                send_to_char( "You shiver and suffer.\n\r", ch );
                damage(ch,ch,poison->level/10 + 1,gsn_poison,
                       DAM_POISON,FALSE, NULL);
            }
        }

        else if ( ch->position == POS_INCAP && number_range(0,1) == 0)
        {
            damage( ch, ch, 1, TYPE_UNDEFINED, DAM_NONE,FALSE, NULL);
        }
        else if ( ch->position == POS_MORTAL )
        {
            damage( ch, ch, 1, TYPE_UNDEFINED, DAM_NONE,FALSE, NULL);
        }

        /*
     * Autosave.
     */
        if (ch->desc != NULL)
            save_char_obj(ch);
    }

    if ( ch_quit )
        do_function(ch_quit,&do_quit,"");

    return;
}




/*
 * Update all objs.
 * This function is performance sensitive.
 */
void obj_update(int start, int step)
{
    OBJ_DATA *obj;
    OBJ_DATA *obj_next;
    EFFECT_DATA *pef, *pef_next;

    for ( obj = object_list; obj != NULL; obj = obj_next?obj_next:obj->next )
    {
        CHAR_DATA *rch;
        char *message;

        obj_next = NULL;
        if (--start>0) continue;
        start=step;

        if (!IS_VALID(obj)) {
            bugf("obj_update(): Trying to update invalid object.");
            return;
        }

        /* go through effects and decrement */
        for ( pef = obj->affected; pef != NULL; pef = pef_next )
        {
            pef_next    = pef->next;
            if ( pef->duration > 0 )
            {
                pef->duration--;
                if (number_range(0,4) == 0 && pef->level > 0)
                    pef->level--;  /* spell strength fades with time */
            }
            else if ( pef->duration < 0 )
                ;
            else
            {
                if ( pef_next == NULL
                     ||   pef_next->type != pef->type
                     ||   pef_next->duration > 0 )
                {
                    if ( pef->type > 0 && skill_msg_obj(pef->type,NULL))
                    {
                        if (obj->carried_by != NULL)
                        {
                            rch = obj->carried_by;
                            act(skill_msg_obj(pef->type,rch),
                                rch,obj,NULL,TO_CHAR);
                        }
                        if (obj->in_room != NULL
                                && obj->in_room->people != NULL)
                        {
                            rch = obj->in_room->people;
                            act(skill_msg_obj(pef->type,NULL),
                                rch,obj,NULL,TO_ALL);
                        }
                    }
                }

                effect_remove_obj( obj, pef );
            }
        }

        if (obj->item_type==ITEM_CLAN &&
                IS_OBJ_STAT(obj,ITEM_INSTALLED) &&
                (number_bits(11)==0)) {
            if ((--obj->condition)==0) {
                if ((rch=obj->in_room->people)) {
                    char msg[MSL];
                    sprintf(msg,"%s has been worn to the pole.",obj->short_descr);
                    act( msg, rch, obj, NULL, TO_ROOM );
                    act( msg, rch, obj, NULL, TO_CHAR );
                }
                extract_obj(obj);
                continue;
            } else {
                if ((rch=obj->in_room->people) && obj->condition<25) {
                    char msg[MSL];
                    sprintf(msg,"%s looks pretty worn.",obj->short_descr);
                    act( msg, rch, obj, NULL, TO_ROOM );
                    act( msg, rch, obj, NULL, TO_CHAR );
                }
            }
        }

        if ( obj->timer <= 0 || --obj->timer > 0 ) {
            if (obj->in_room &&
                    obj->wear_flags & ITEM_TAKE) {
                if (obj->in_room->sector_type == SECT_AIR)
                    object_drop_air(obj);
                if (obj->in_room->sector_type == SECT_WATER_SWIM ||
                        obj->in_room->sector_type == SECT_WATER_NOSWIM ||
                        obj->in_room->sector_type == SECT_WATER_BELOW)
                    object_drop_water(obj);
            }
            continue;
        }

        // empty vials don't dissolve
        if (obj->item_type==ITEM_POTION) {
            if (obj->value[1]==0 && obj->value[2]==0 &&
                    obj->value[3]==0 && obj->value[4]==0)
                continue;
        }

        if (STR_IS_SET(obj->strbit_extra_flags,ITEM_INSURED)) {
            ROOM_DATA *room;

            room=get_room_index(ROOM_VNUM_INSURANCE);
            if (!room) {
                bugf("Insurance room [%d] doesn't exist.",ROOM_VNUM_INSURANCE);
                continue;
            }
            if ( obj->in_room != NULL ) {
                obj_from_room( obj );
            } else if ( obj->carried_by != NULL ) {
                obj_from_char( obj );
            } else if ( obj->in_obj != NULL ) {
                obj_from_obj( obj );
            }
            STR_REMOVE_BIT(obj->strbit_extra_flags,ITEM_INSURED);
            logf("Insurance invoked for [%d] %s owned by %s",obj->pIndexData->vnum,obj->short_descr,obj->owner_name);
            obj_to_room(obj,room);
            continue;
        }

        // dissolving of items
        switch ( obj->item_type )
        {
        default:              message = "$p crumbles into dust.";  break;
        case ITEM_FOUNTAIN:   message = "$p dries up.";         break;
        case ITEM_CORPSE_NPC: message = "$p decays into dust."; break;
        case ITEM_CORPSE_PC:  message = "$p decays into dust."; break;
        case ITEM_FOOD:       message = "$p decomposes.";	break;
        case ITEM_POTION:     message = "$p has evaporated from disuse.";
            break;
        case ITEM_PORTAL:     message = "$p fades out of existence."; break;
        case ITEM_CONTAINER:
            if (CAN_WEAR(obj,ITEM_WEAR_FLOAT))
                if (obj->contains)
                    message =
                            "$p flickers and vanishes, spilling its contents on the floor.";
                else
                    message = "$p flickers and vanishes.";
            else
                message = "$p crumbles into dust.";
            break;
        }

        if ( obj->carried_by != NULL )
        {
            if (IS_NPC(obj->carried_by)
                    &&  obj->carried_by->pIndexData->pShop != NULL)
                obj->carried_by->silver += obj->cost/5;
            else
            {
                act( message, obj->carried_by, obj, NULL, TO_CHAR );
                if ( obj->wear_loc == WEAR_FLOAT)
                    act(message,obj->carried_by,obj,NULL,TO_ROOM);
            }
        }
        else if ( obj->in_room != NULL
                  &&      ( rch = obj->in_room->people ) != NULL )
        {
            if (! (obj->in_obj && obj->in_obj->pIndexData->vnum == OBJ_VNUM_PIT
                   && !CAN_WEAR(obj->in_obj,ITEM_TAKE)))
            {
                act( message, rch, obj, NULL, TO_ROOM );
                act( message, rch, obj, NULL, TO_CHAR );
            }
        }

        if ((obj->item_type == ITEM_CORPSE_PC || obj->wear_loc == WEAR_FLOAT)
                &&  obj->contains)
        {   /* save the contents */
            OBJ_DATA *t_obj;

            while ((t_obj = obj->contains)) {
                obj_from_obj(t_obj);

                if (obj->in_obj) /* in another object */
                    obj_to_obj(t_obj,obj->in_obj);

                else if (obj->carried_by)  /* carried */
                    if (obj->wear_loc == WEAR_FLOAT)
                        if (obj->carried_by->in_room == NULL)
                            extract_obj(t_obj);
                        else
                            obj_to_room(t_obj,obj->carried_by->in_room);
                    else
                        obj_to_char(t_obj,obj->carried_by);

                else if (obj->in_room == NULL)  /* destroy it */
                    extract_obj(t_obj);

                else /* to a room */
                    obj_to_room(t_obj,obj->in_room);
            }
        }

        while (obj->contains) extract_obj(obj->contains);

        obj_next=obj->next;

        if(obj->carried_by && obj->wear_loc==WEAR_WIELD)
        {
            OBJ_DATA *sec;
            CHAR_DATA *victim=obj->carried_by;

            extract_obj( obj );
            sec=get_eq_char(victim,WEAR_SECONDARY);
            if(sec)
            {
                unequip_char( victim, sec);
                equip_char( victim, sec, WEAR_WIELD );
                act( "$n now uses $p as primary weapon.",victim,sec,NULL,TO_ROOM);
                act( "You now use $p as primary weapon.",victim,sec,NULL,TO_CHAR);
            }
        }
        else extract_obj( obj );
    }

    return;
}

/*
 * Update all room.
 * This function is performance sensitive.
 * I hope it won't take to mutch cpu. Else I have to split the rooms
 * up into two hash tables. I hope i have not to do that.
 */
void room_update(int start, int step)
{
    ROOM_INDEX_DATA *room;
    ROOM_INDEX_DATA *room_next;
    EFFECT_DATA *pef, *pef_next;
    int i;

    for(i=start-1;i<MAX_KEY_HASH;i+=step)
        for ( room = room_index_hash[i]; room != NULL; room = room_next )
        {
            CHAR_DATA *rch;

            room_next = room->next;

            /* go through effects and decrement */
            for ( pef = room->eff_temp; pef != NULL; pef = pef_next )
            {
                pef_next    = pef->next;
                if ( pef->duration > 0 )
                {
                    pef->duration--;
                    if (number_range(0,4) == 0 && pef->level > 0)
                        pef->level--;  /* spell strength fades with time */
                }
                else if ( pef->duration < 0 )
                    ;
                else
                {
                    if ( pef_next == NULL
                         ||   pef_next->type != pef->type
                         ||   pef_next->duration > 0 )
                    {
                        if ( pef->type > 0 && skill_msg_obj(pef->type,NULL) )
                        {
                            if (room->people != NULL)
                            {
                                rch = room->people;
                                act(skill_msg_obj(pef->type,NULL),
                                    rch,NULL,NULL,TO_ALL);
                            }
                        }
                    }

                    effect_remove_room( room, pef );
                }
            }
        }

    return;
}


void aggr_update( void )
{
    CHAR_DATA *wch,*next_wch;
    CHAR_DATA *ch;
    CHAR_DATA *ch_next;
    CHAR_DATA *vch;
    CHAR_DATA *vch_next;
    CHAR_DATA *victim;

    for( wch = player_list; wch != NULL; wch = next_wch )
    {
        next_wch = wch->next_player;

        if ( !wch
             ||   wch->level >= LEVEL_IMMORTAL
             ||   wch->in_room == NULL
             ||   wch->in_room->area->empty)
            continue;

        for ( ch = wch->in_room->people; ch != NULL; ch = ch_next )
        {
            int count;

            ch_next	= ch->next_in_room;

            if ( !IS_NPC(ch)
                 ||   !STR_IS_SET(ch->strbit_act, ACT_AGGRESSIVE)
                 ||   STR_IS_SET(ch->in_room->strbit_room_flags,ROOM_SAFE)
                 ||   IS_AFFECTED(ch,EFF_CALM)
                 ||   ch->fighting != NULL
                 ||   IS_AFFECTED(ch, EFF_CHARM)
                 ||   !IS_AWAKE(ch)
                 ||   ( STR_IS_SET(ch->strbit_act, ACT_WIMPY) && IS_AWAKE(wch) )
                 ||   !can_see( ch, wch )
                 ||   number_bits(1) == 0)
                continue;

            /*
         * Ok we have a 'wch' player character and a 'ch' npc aggressor.
         * Now make the aggressor fight a RANDOM pc victim in the room,
         *   giving each 'vch' an equal chance of selection.
         */
            count	= 0;
            victim	= NULL;
            for ( vch = wch->in_room->people; vch != NULL; vch = vch_next )
            {
                vch_next = vch->next_in_room;

                if ( !IS_NPC(vch)
                     &&   vch->level < LEVEL_IMMORTAL
                     &&   ch->level >= vch->level - 5
                     &&   ( !STR_IS_SET(ch->strbit_act, ACT_WIMPY) || !IS_AWAKE(vch))
                     &&   can_see( ch, vch ) )
                {
                    if ( number_range( 0, count ) == 0 )
                        victim = vch;
                    count++;
                }
            }

            if ( victim == NULL )
                continue;

            /* Don't attack at once */
            WAIT_STATE( ch, number_range(1,3) );
            /* If character is in a group, then attack the leader */
            if(victim->leader!=NULL && victim->leader->in_room==ch->in_room)
                set_fighting(ch,victim->leader);
            else
                set_fighting(ch,victim);
        }
    }

    return;
}

void pre_reboot_insurance_update(void) {
    OBJ_DATA *obj;
    OBJ_DATA *cobj;
    ROOM_DATA *room;

    room=get_room_index(ROOM_VNUM_INSURANCE);
    if (!room) {
        bugf("Insurance room [%d] doesn't exist.",ROOM_VNUM_INSURANCE);
        return;
    }

    for (obj=object_list; obj!=NULL; obj=obj->next) {
        if (!STR_IS_SET(obj->strbit_extra_flags,ITEM_INSURED))
            continue;

        cobj=obj;
        while (cobj->in_obj) cobj=cobj->in_obj;
        if (cobj->carried_by && IS_PC(cobj->carried_by))
            continue;
        if (cobj->in_room && STR_IS_SET(cobj->in_room->strbit_room_flags,ROOM_SAVEOBJ))
            continue;

        if ( obj->in_room != NULL ) {
            obj_from_room( obj );
        } else if ( obj->carried_by != NULL ) {
            obj_from_char( obj );
        } else if ( obj->in_obj != NULL ) {
            obj_from_obj( obj );
        }
        STR_REMOVE_BIT(obj->strbit_extra_flags,ITEM_INSURED);
        logf("Insurance invoked for [%d] %s owned by %s",obj->pIndexData->vnum,obj->short_descr,obj->owner_name);
        obj_to_room(obj,room);
    }
    
}

void reboot_update(void) {

    if (mud_data.reboot_type==REBOOT_NONE)
        return;

    mud_data.reboot_timer--;

    // reboot now
    if (mud_data.reboot_timer==0) {
        CHAR_DATA *vch;
        char s[MAX_STRING_LENGTH];

        sprintf(s,"}r%s now.{x\n\r",reboot_state());
        send_to_all_char(s);

        if (mud_data.reboot_type==REBOOT_SHUTDOWN) {
            FILE *f;

            send_to_all_char("}rShutting down now.{x\n\r");
            if((f=fopen(mud_data.shutdown_file,"w"))) fclose(f);
        }

        mud_data.reboot_now=TRUE;
        wiznet(WIZ_REBOOT,0,NULL,NULL,"Saving changed areas.");
        do_function(NULL,&do_asave,"");
        wiznet(WIZ_REBOOT,0,NULL,NULL,"Saving clans.");
        save_clans();
        wiznet(WIZ_REBOOT,0,NULL,NULL,"Saving insured objects.");
        pre_reboot_insurance_update();
        wiznet(WIZ_REBOOT,0,NULL,NULL,"Saving all rooms.");
        //save_all_room_obj();
        new_save_all_room_obj();
        wiznet(WIZ_REBOOT,0,NULL,NULL,"Saving all jobs.");
        save_jobs();

        if (mud_data.reboot_type==REBOOT_COPYOVER) {
            unlink(WATCHDOG_FILE);

            copyover_execute();

            // in case copyover fails (eeks!)
            mud_data.reboot_now=FALSE;
            mud_data.reboot_type=REBOOT_NONE;
            mud_data.reboot_timer=0;
            mud_data.reboot_pulse=0;

            // create the watchdog file
            {
                FILE *wd;

                wd=fopen(WATCHDOG_FILE,"w");
                fprintf(wd,"%d",getpid());
                fclose(wd);
            }

            return;
        }

        for (vch=player_list;vch!=NULL;vch=vch->next_player) {
            save_char_obj(vch);
            if (vch->desc)
                close_socket(vch->desc);
        }
        return;
    }

    // counting down
    if (mud_data.reboot_timer<=4) {
        char buf[MAX_STRING_LENGTH];

        switch(mud_data.reboot_timer) {
        case 1:
            sprintf(buf,"}r%s in half a minute.{x\n\r",reboot_state());
            send_to_all_char(buf);
            break;
        case 2:
            sprintf(buf,"}r%s in one minute.{x\n\r",reboot_state());
            send_to_all_char(buf);
            break;
        case 3:
            sprintf(buf,"}r%s in one and a half minute.{x\n\r",reboot_state());
            send_to_all_char(buf);
            break;
        case 4:
            sprintf(buf,"}r%s in two minutes.{x\n\r",reboot_state());
            send_to_all_char(buf);
            break;
        }
        return;
    }

    if (mud_data.reboot_timer%2==0) {
        char buf[MAX_STRING_LENGTH];

        sprintf(buf,"}r%s in %d minutes.{x\n\r",
                reboot_state(),
                mud_data.reboot_timer/2);
        send_to_all_char(buf);
        return;
    }
}


//
// mob/room/area/object programs updates
//
void mprog_pulse_update(int start, int step) {
    CHAR_DATA *ch,*ch_next;

    for (ch=char_list;ch!=NULL;ch=ch_next) {
        ch_next=ch->next;
        if (--start>0) continue;
        start=step;

        // it sometimes happens that a mob further on in the list gets invalid.
        // this is not a bug. mobprograms can do these things.
        if (!IS_VALID(ch))
            return;

        /*
     * Check triggers only if mobile still in default position
     */
        if (IS_PC(ch))
            continue;
        if (ch->in_room==NULL)
            continue;
        if (IS_AFFECTED(ch,EFF_CHARM))
            continue;
        if ( ch->position != ch->pIndexData->default_pos &&
             !STR_IS_SET(ch->strbit_act,ACT_MOBPROG_IGNOREPOS))
            continue;

        /* Delay */
        if ( MOB_HAS_TRIGGER( ch, MTRIG_DELAY)
             &&   ch->mprog_delay > 0 ) {
            if ( --ch->mprog_delay <= 0 ) {
                mp_delay_trigger(ch);
                if (!IS_VALID(ch))
                    continue;
            }
        }
        if( MOB_HAS_TRIGGER( ch, MTRIG_TIMER)
                && ch->mprog_timer>=0 ) {
            ch->mprog_timer++;
            mp_timer_trigger(ch);
            if (!IS_VALID(ch))
                continue;
        }
        if ( MOB_HAS_TRIGGER( ch, MTRIG_RANDOM) ) {
            mp_random_trigger(ch);
            if (!IS_VALID(ch))
                continue;
        }
    }
}

void oprog_pulse_update(int start, int step) {
    OBJ_DATA *obj,*obj_next;

    for (obj=object_list;obj!=NULL;obj=obj_next) {
        obj_next=obj->next;
        if (--start>0) continue;
        start=step;

        // it sometimes happens that an object further on in the list gets
        // invalid. this is not a bug. objprograms can do these things.
        if (!IS_VALID(obj))
            return;

        if ( OBJ_HAS_TRIGGER( obj, OTRIG_DELAY)
             &&   obj->oprog_delay > 0 )
            if ( --obj->oprog_delay <= 0 ) {
                op_delay_trigger( obj );
                if (!IS_VALID(obj))
                    continue;
            }
        if ( OBJ_HAS_TRIGGER( obj, OTRIG_TIMER)
             && obj->oprog_timer>=0 ) {
            obj->oprog_timer++;
            op_timer_trigger(obj);
            if (!IS_VALID(obj))
                continue;
        }
        if ( OBJ_HAS_TRIGGER( obj, OTRIG_RANDOM) ) {
            op_random_trigger( obj );
            if (!IS_VALID(obj))
                continue;
        }
    }
}

void rprog_pulse_update(int start, int step) {
    ROOM_INDEX_DATA *room;
    int i;

    for(i=start-1;i<MAX_KEY_HASH;i+=step)
        for ( room = room_index_hash[i]; room != NULL; room = room->next )
        {
            // timer-thingies
            if ( ROOM_HAS_TRIGGER( room, RTRIG_DELAY)
                 &&   room->rprog_delay > 0 )
                if ( --room->rprog_delay <= 0 )
                    rp_delay_trigger( room );
            if( ROOM_HAS_TRIGGER( room, RTRIG_TIMER)
                    && room->rprog_timer>=0 ) {
                room->rprog_timer++;
                rp_timer_trigger(room);
            }
            if ( ROOM_HAS_TRIGGER( room, RTRIG_RANDOM) )
                rp_random_trigger(room);
        }
}

void aprog_pulse_update(int start, int step) {
    AREA_DATA *pArea;

    pArea=area_first;
    while (pArea!=NULL) {
        if (--start==0) {
            start=step;

            if ( AREA_HAS_TRIGGER( pArea, ATRIG_DELAY)
                 &&   pArea->aprog_delay > 0 ) {
                if ( --pArea->aprog_delay <= 0 ) {
                    ap_delay_trigger( pArea );
                }
            }
            if( AREA_HAS_TRIGGER( pArea, ATRIG_TIMER)
                    && pArea->aprog_timer>=0 ) {
                pArea->aprog_timer++;
                ap_timer_trigger(pArea);
            }
            if ( AREA_HAS_TRIGGER( pArea, ATRIG_RANDOM) )
                ap_random_trigger(pArea);
        }
        pArea=pArea->next;
    }
}

void mprog_tick_update(void) {
    CHAR_DATA *ch,*ch_next;

    for (ch=char_list;ch!=NULL;ch=ch_next) {
        ch_next=ch->next;

        // it sometimes happens that a mob further on in the list gets invalid.
        // this is not a bug. mobprograms can do these things.
        if (!IS_VALID(ch))
            return;

        /*
     * Check triggers only if mobile still in default position
     */
        if (IS_PC(ch))
            continue;
        if (ch->position != ch->pIndexData->default_pos &&
                !STR_IS_SET(ch->strbit_act,ACT_MOBPROG_IGNOREPOS))
            continue;

        if (MOB_HAS_TRIGGER( ch, MTRIG_HOUR )) {
            mp_hour_trigger(ch,time_info.hour);
            if (!IS_VALID(ch))
                continue;
        }
    }
}

void oprog_tick_update() {
    OBJ_DATA *obj,*obj_next;

    for (obj=object_list;obj!=NULL;obj=obj_next) {
        obj_next=obj->next;

        // it sometimes happens that an object further on in the list gets
        // invalid. this is not a bug. objprograms can do these things.
        if (!IS_VALID(obj))
            return;

        if (OBJ_HAS_TRIGGER( obj, OTRIG_HOUR )) {
            op_hour_trigger(obj,time_info.hour);
            if (!IS_VALID(obj))
                continue;
        }
    }
}

void rprog_tick_update() {
    ROOM_INDEX_DATA *room;
    int i;

    for(i=0;i<MAX_KEY_HASH;i++)
        for ( room = room_index_hash[i]; room != NULL; room = room->next )
        {
            if (ROOM_HAS_TRIGGER( room, RTRIG_HOUR ))
                rp_hour_trigger(room,time_info.hour);
        }
}

void aprog_tick_update() {
    AREA_DATA *pArea;

    pArea=area_first;
    while (pArea!=NULL) {
        if (AREA_HAS_TRIGGER( pArea, ATRIG_HOUR ))
            ap_hour_trigger(pArea,time_info.hour);

        pArea=pArea->next;
    }
}

void save_stats(void) {
    FILE *fout;

    if ((fout=fopen(mud_data.statistics_file,"at"))==NULL) {
        logf( "save_stats(): fopen(%s): %s",mud_data.statistics_file,ERROR);
        bugf( "save_stats: couldn't open 'statistics'.");
        return;
    }

    /* Format of the output file:
     * - unix-time
     * - number of players
     * - number of tcp-sessions
     * - new players
     * - deleted players
     * - HTML violations
     * - POP3 violations
     * - MUD violations
     * - player-violations
     * - site-violations
     * - new player-violations
     * - newlock-violations
     * - wizlock-violations
     * - http-who-requests
     * - http-help-requests
     * - http-failed-requests
     * - pop3-requests
     */

    fprintf(fout,"%d %d %d %d %d "
        #ifdef HAS_HTTP
                 "%d "
        #endif
        #ifdef HAS_POP3
                 "%d "
        #endif
                 "%d %d %d %d %d %d "
        #ifdef HAS_HTTP
                 "%d %d %d "
        #endif
        #ifdef HAS_POP3
                 "%d"
        #endif
                 "\n",
            (int)time(NULL)
            ,mud_data.players_total
            ,mud_data.connections
            ,mud_data.players_new
            ,mud_data.players_deleted
        #ifdef HAS_HTTP
            ,mud_data.banhttp_violations
        #endif
        #ifdef HAS_POP3
            ,mud_data.banpop3_violations
        #endif
            ,mud_data.banmud_violations
            ,mud_data.banplayer_violations
            ,mud_data.bansite_violations
            ,mud_data.bannew_violations
            ,mud_data.newlock_violations
            ,mud_data.wizlock_violations
        #ifdef HAS_HTTP
            ,mud_data.http_who_connections
            ,mud_data.http_help_connections
            ,mud_data.http_errors
        #endif
        #ifdef HAS_POP3
            ,mud_data.pop3_connections
        #endif
            );

    fclose(fout);
}

void save_wholist(void) {
    FILE *fout;
    CHAR_DATA *ch;

    if ((fout=fopen(mud_data.wholist_file,"w"))==NULL) {
        logf( "save_wholist(): fopen(%s): %s",mud_data.wholist_file,ERROR);
        bugf( "save_wholist: couldn't open '%s'.",mud_data.wholist_file);
        return;
    }
    for (ch=player_list;ch!=NULL;ch=ch->next_player) {

        if(IS_AFFECTED(ch,EFF_INVISIBLE) ||
                IS_AFFECTED2(ch,EFF_IMPINVISIBLE) ||
                ch->invis_level ||
                ch->incog_level)
            continue;

        fprintf(fout,"%d|%s|",ch->level,
                ch->race < MAX_PC_RACE ? pc_race_table[ch->race].who_name : "");
        switch ( ch->level ) {
        default: 		fprintf(fout,"%s|",class_table[ch->class].who_name); break;
        case MAX_LEVEL-0 :	fputs("IMP|",fout); break;
        case MAX_LEVEL-1 :	fputs("CRE|",fout); break;
        case MAX_LEVEL-2 :	fputs("SUP|",fout); break;
        case MAX_LEVEL-3 :	fputs("DEI|",fout); break;
        case MAX_LEVEL-4 :	fputs("GOD|",fout); break;
        case MAX_LEVEL-5 :	fputs("IMM|",fout); break;
        case MAX_LEVEL-6 :	fputs("DEM|",fout); break;
        case MAX_LEVEL-7 :	fputs("ANG|",fout); break;
        case MAX_LEVEL-8 :	fputs("AVA|",fout); break;
        }
        fprintf(fout,"%s|",ch->pcdata->whoname);
        if (ch->clan==NULL) {
            fputs("||",fout);
        } else {
            fprintf(fout,"%s|%s|",
                    ch->clan->clan_info->who_name,
                    ch->clan->rank_info->who_name);
        }
        fprintf(fout,"%s%s%s|",
                STR_IS_SET(ch->strbit_comm,COMM_AFK) ? "[AFK] ":"",
                STR_IS_SET(ch->strbit_act,PLR_KILLER) ? "(KILLER) ":"",
                STR_IS_SET(ch->strbit_act,PLR_THIEF)  ? "(THIEF) ":"");
        fprintf(fout,"%s|",ch->name);
        fprintf(fout,"%s\n",get_afk_title(ch));
    }
    fclose(fout);
}

void underwater_update( void ) {
    CHAR_DATA *ch;
    CHAR_DATA *ch_next;

    for ( ch = char_list; ch != NULL; ch = ch_next) {
        ch_next = ch->next;

        if (ch->in_room==NULL)		// we don't support people/mobs
            continue;			// who aren't in the game yet

        if (!IS_IMMORTAL(ch)
                && ch->in_room->sector_type==SECT_WATER_BELOW) {

            if (is_affected(ch,gsn_waterbreathing))
                continue;
            if (IS_AFFECTED(ch,EFF_WATERBREATHING))
                continue;

            if ( ch->hit > 20) {
                ch->position = POS_RESTING;
                ch->hit /= 2;
                send_to_char("You're drowning!!!\n\r", ch);
                if (ch->hit<=20) {
                    act("$ns face turns purple, bubbles of air are escaping "
                        "through $s mouth!",ch,NULL,NULL,TO_ROOM);
                } else if (ch->hit<=50) {
                    act("$ns face turns red, $e starts panicing!",
                        ch,NULL,NULL,TO_ROOM);
                } else {
                    act("$n is drowing!",ch,NULL,NULL,TO_ROOM);
                }
            } else {
                char log_buf[MSL];

                sprintf(log_buf,"$N died under water at %s [room %d]",
                        ch->in_room->name, ch->in_room->vnum);
                if (IS_NPC(ch))
                    wiznet(WIZ_MOBDEATHS,0,ch,NULL,"%s",log_buf);
                else
                    wiznet(WIZ_DEATHS,0,ch,NULL,"%s",log_buf);

                act("$ns body stops moving, it starts to float slowly to "
                    "the surface.",ch,NULL,NULL,TO_ROOM);
                send_to_char("You didn't reach the surface, "
                             "your vision blurs and turns black!!\n\r", ch );
                if (IS_AFFECTED(ch,EFF_HALLUCINATION))
                    send_to_char("You sink like the Titanic.\n\r",ch);
                else
                    send_to_char("You sink like a rock.\n\r",ch);
                ch->hit = 1;
                raw_kill(ch,NULL);
            }
        }
    }
}

void phasedskill_update(void) {
    CHAR_DATA *ch,*ch_next;

    for (ch=char_list;ch;ch=ch_next) {
        ch_next=ch->next;

        if (ch->phasedskill_proc==NULL) {
            continue;
        }
        if (ch->position==POS_FIGHTING) {
            continue;
        }
        (ch->phasedskill_proc)(ch);
    }
}

/*
* Handle all kinds of updates.
* Called once per pulse from game loop.
* Random times to defeat tick-timing clients and players.
*/

void update_handler( void )
{
    static  int     pulse_area;
    static  int     pulse_mobile;
    static  int     pulse_violence;
    static  int     pulse_point;
#ifdef HAS_JUKEBOX
    static  int	    pulse_music;
#endif
    static  int	    pulse_asave;
    static  int	    pulse_hour;
    static  int	    pulse_underwater;

    if ( --mud_data.reboot_pulse <=0 )
    {
        mud_data.reboot_pulse = PULSE_REBOOT;
        reboot_update();
    }

    if ( --pulse_asave <=0 )
    {
        pulse_asave = PULSE_ASAVE;
        do_function(NULL,&do_asave,"");
        //save_all_room_obj();
        new_save_all_room_obj();
        save_clans();
        save_jobs();
    }

    if ( --pulse_area     <= 0 )
    {
        pulse_area	= PULSE_AREA;
        /* number_range( PULSE_AREA / 2, 3 * PULSE_AREA / 2 ); */
        area_update	( );
        quest_update	( );
        ban_update	( );
        lockout_update	( );
    }

    if ( --pulse_hour	  <= 0 )
    {
        pulse_hour	= PULSE_HOUR;
        save_stats();
        descriptor_update();
    }

#ifdef HAS_JUKEBOX
    if ( --pulse_music	  <= 0 )
    {
        pulse_music	= PULSE_MUSIC;
        song_update();
    }
#endif

    if ( --pulse_mobile   <= 0 )
    {
        pulse_mobile	= PULSE_MOBILE;
    }
    mobile_update(pulse_mobile,PULSE_MOBILE);
    mprog_pulse_update(pulse_mobile,PULSE_MOBILE);
    oprog_pulse_update(pulse_mobile,PULSE_MOBILE);
    rprog_pulse_update(pulse_mobile,PULSE_MOBILE);
    aprog_pulse_update(pulse_mobile,PULSE_MOBILE);

    if ( --pulse_violence <= 0 )
    {
        pulse_violence	= PULSE_VIOLENCE;
        violence_update	( );
        phasedskill_update ( );
    }

    if ( --pulse_underwater <= 0 ) {
        pulse_underwater= PULSE_UNDERWATER;
        wiznet(WIZ_TICKS,0,NULL,NULL,"TICK (water)!");
        underwater_update ( );
    }

    if ( --pulse_point    <= 0 )
    {
        extern int nAllocPerm,sAllocPerm;

        wiznet(WIZ_TICKS,0,NULL,NULL,"TICK!");
        wiznet(WIZ_MEMORY,0,NULL,NULL,
               "Perms %d blocks of %d bytes.",nAllocPerm,sAllocPerm);

        pulse_point     = PULSE_TICK;
        weather_update	( );
        save_wholist();

        mprog_tick_update();
        oprog_tick_update();
        rprog_tick_update();
        aprog_tick_update();
    }
    obj_update(pulse_point,PULSE_TICK);
    room_update(pulse_point,PULSE_TICK);
    char_update(pulse_point,PULSE_TICK);

    aggr_update( );
    tag_update();
    tail_chain( );
    return;
}

void descriptor_update(void) {
    DESCRIPTOR_DATA *d,*d_next;

    for ( d = descriptor_list; d != NULL; d = d_next ) {
        d_next=d->next;

        // more than an hour for logging in, enough!
        if (d->connected != CON_PLAYING
                && time(NULL)-d->idle>3600) {
            close_socket(d);
            continue;
        }

        // not immortal, idle for more than an hour? enough!
        if (d->connected==CON_PLAYING
                && !IS_HERO(d->character)
                && time(NULL)-d->idle>3600) {
            close_socket(d);
            continue;
        }
    }
}
