//
// $Id: fd_conf.c,v 1.16 2004/03/16 18:22:19 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "merc.h"

void init_config(void);
void init_newbie(void);

void InitMudStatus(char *arg,int argc,char **argv) {
    char buf[MSL];
    int i;
    char ch,*p;

    mud_data.newlock_violations=0;
    mud_data.wizlock_violations=0;
#ifdef HAS_HTTP
    mud_data.banhttp_violations=0;
#endif
#ifdef HAS_POP3
    mud_data.banpop3_violations=0;
#endif
    mud_data.banmud_violations=0;
    mud_data.banplayer_violations=0;
    mud_data.bansite_violations=0;
    mud_data.bannew_violations=0;
    mud_data.players_total=0;
    mud_data.players_new=0;
    mud_data.players_deleted=0;
    mud_data.connections=0;
#ifdef HAS_HTTP
    mud_data.http_who_connections=0;
    mud_data.http_help_connections=0;
    mud_data.http_errors=0;
#endif
#ifdef HAS_POP3
    mud_data.pop3_connections=0;
#endif

    if (!strcmp(arg,"init")) {

	mud_data.mud_port=4000;
	mud_data.http_port=4001;
	mud_data.pop3_port=4002;
	mud_data.copyover=FALSE;
	mud_data.nogame=FALSE;
	mud_data.no_timestamp_log=FALSE;

	mud_data.argc=argc;
	mud_data.argv=argv;
	asprintf(&mud_data.startup_dir,"%s",getcwd(buf,sizeof(buf)));
	asprintf(&mud_data.exe_file,"%s",argv[0]);
	if ((p=strrchr(mud_data.exe_file,'/'))==NULL)
	    asprintf(&mud_data.exe_name,"%s",argv[0]);
	else
	    asprintf(&mud_data.exe_name,"%s",p+1);

	while ((ch=getopt(argc,argv,"d:H:p:P:qt"))!=-1) {
	    switch (ch) {

	    case 'd':
		if (chdir(optarg)!=0) {
		    bugf("Cannot chdir to %s",optarg);
		    abort();
		}
		logf("[0] Setting directory to '%s'",optarg);
		break;

#ifdef HAS_HTTP
	    case 'H':
		mud_data.http_port=atoi(optarg);
		logf("[0] Setting HTTP port to '%s'",optarg);
		break;
#endif

	    case 'p':
		mud_data.mud_port=atoi(optarg);
		mud_data.http_port=atoi(optarg)+1;
		mud_data.pop3_port=atoi(optarg)+2;
		logf("[0] Setting MUD port to '%s'",optarg);
		break;

#ifdef HAS_POP3
	    case 'P':
		mud_data.pop3_port=atoi(optarg);
		logf("[0] Setting POP3 port to '%s'",optarg);
		break;
#endif

	    case 't':
		mud_data.no_timestamp_log=TRUE;
		logf("[0] Don't put timestamps on stderr");
		break;

	    case 'q':
		mud_data.nogame=TRUE;
		break;

	    default:
		bugf("Unknown option: '%c'",ch);
		bugf("Usage: %0 [-p port] [-H port] [-P port] [-d dir] [-q]");
		bugf("-p port    Define what is the baseport (default: 4000)");
#ifdef HAS_HTTP
		bugf("-H port    Define what is the HTTP-port (default: baseport+1)");
#endif
#ifdef HAS_POP3
		bugf("-P port    Define what is the POP3-port (default: baseport+2)");
#endif
		bugf("-d dir     Startup directory (default: .)");
		bugf("-t         Don't put timestamps on the logging lines. (eg with syslog)");
		bugf("-q         Don't run the game, only load the database and quit.");
		exit(0);
	    }
	}

	asprintf(&mud_data.work_dir,"%s",getcwd(buf,sizeof(buf)));

	asprintf(&mud_data.player_dir,"%s",PLAYER_DIR);
	asprintf(&mud_data.hardcore_dir,"%s",HARDCORE_DIR);
	asprintf(&mud_data.area_dir,"%s",AREA_DIR);
	asprintf(&mud_data.room_dir,"%s",ROOM_DIR);
	asprintf(&mud_data.log_dir,"%s",LOG_DIR);
	asprintf(&mud_data.notesroot_dir,"%s",NOTESROOT_DIR);
	asprintf(&mud_data.area_import,"%s/import",AREA_DIR);
	asprintf(&mud_data.config_dir,"%s",CONFIG_DIR);

	asprintf(&mud_data.area_lst,"%s/%s",mud_data.area_dir,AREA_LIST);
	asprintf(&mud_data.copyover_file,"%s/%s",mud_data.config_dir,COPYOVER_FILE);
	asprintf(&mud_data.shutdown_file,"%s/%s",mud_data.config_dir,SHUTDOWN_FILE);
	asprintf(&mud_data.newbie_file,"%s/%s",mud_data.config_dir,NEWBIE_FILE);
	asprintf(&mud_data.social_file,"%s/%s",mud_data.area_dir,SOCIAL_FILE);
	asprintf(&mud_data.ban_file,"%s/%s",mud_data.config_dir,BAN_FILE);
	asprintf(&mud_data.badname_file,"%s/%s",mud_data.config_dir,BADNAMES_FILE);
	asprintf(&mud_data.clan_file,"%s/%s",mud_data.config_dir,CLAN_FILE);
	asprintf(&mud_data.job_file,"%s/%s",mud_data.config_dir,JOB_FILE);
	asprintf(&mud_data.property_file,"%s/%s",mud_data.config_dir,PROPERTY_FILE);
	asprintf(&mud_data.swear_file,"%s/%s",mud_data.config_dir,SWEAR_FILE);
	asprintf(&mud_data.music_file,"%s/%s",mud_data.config_dir,MUSIC_FILE);
	asprintf(&mud_data.poll_file,"%s/%s",mud_data.config_dir,POLL_FILE);
	asprintf(&mud_data.pinky_file,"%s/%s",mud_data.config_dir,PINKY_FILE);
	asprintf(&mud_data.statistics_file,"%s/%s",mud_data.log_dir,STATS_FILE);
	asprintf(&mud_data.wholist_file,"%s/%s",mud_data.log_dir,WHOLIST_FILE);
	asprintf(&mud_data.i3_config,"%s/%s",mud_data.config_dir,I3_FILE);
	asprintf(&mud_data.mud_config,"%s/%s",mud_data.config_dir,CONFIG_FILE);

	asprintf(&mud_data.notes_dir[NOTE_NOTE],
				"%s/%s",mud_data.notesroot_dir,NOTE_DIR);
	asprintf(&mud_data.notes_dir[NOTE_IDEA],
				"%s/%s",mud_data.notesroot_dir,IDEA_DIR);
	asprintf(&mud_data.notes_dir[NOTE_PENALTY],
				"%s/%s",mud_data.notesroot_dir,PENALTY_DIR);
	asprintf(&mud_data.notes_dir[NOTE_NEWS],
				"%s/%s",mud_data.notesroot_dir,NEWS_DIR);
	asprintf(&mud_data.notes_dir[NOTE_CHANGES],
				"%s/%s",mud_data.notesroot_dir,CHANGES_DIR);
	asprintf(&mud_data.notes_dir[NOTE_BUG],
				"%s/%s",mud_data.notesroot_dir,BUG_DIR);
	asprintf(&mud_data.notes_dir[NOTE_TYPO],
				"%s/%s",mud_data.notesroot_dir,TYPO_DIR);
	asprintf(&mud_data.notes_dir[NOTE_MCNOTE],
				"%s/%s",mud_data.notesroot_dir,MCNOTE_DIR);

	//
	// if the copyover file is older than 10 seconds, then it's not a
	// copyover reboot.
	//
	{
	    struct stat sb;

	    if (stat(mud_data.copyover_file,&sb)==0) {
		if (sb.st_mtime+10>time(NULL)) {
		    logf("[0] Found copyover datafile");
		    mud_data.copyover=TRUE;
		} else {
		    logf("[0] Removing stale copyover datafile");
		    unlink(mud_data.copyover_file);
		}
	    }
	}

	mud_data.newlock=0;
	mud_data.newlock_by=str_dup("(startup)");
	mud_data.newlock_since=time(NULL);

	mud_data.wizlock=0;
	mud_data.wizlock_by=str_dup("(startup)");
	mud_data.wizlock_since=time(NULL);

	mud_data.dnslock=0;
	mud_data.dnslock_by=str_dup("(startup)");
	mud_data.dnslock_since=time(NULL);

	mud_data.clearcounter_by=str_dup("(startup)");
	mud_data.clearcounter_since=time(NULL);

	mud_data.reboot_now=FALSE;
	mud_data.reboot_type=REBOOT_NONE;
	mud_data.reboot_timer=0;
	mud_data.reboot_pulse=0;
	mud_data.reboot_by=str_dup("(startup)");

	for (i=0;i<MAX_TIMEZONES;i++) {
	    mud_data.time_zones[i]=str_dup("");
	    mud_data.time_zonenames[i]=str_dup("");
	    mud_data.time_offsets[i]=0;
	}

        mud_data.max_string=MAX_STRING;

	init_config();
	init_newbie();
    }
}


void init_config(void) {
    FILE *	fin;
    int		version;
    char *	word;

    if ((fin=fopen(mud_data.mud_config,"rt"))==NULL) {
	bugf("init_config(): Couldn't open %s: %s.",mud_data.mud_config,ERROR);
	abort();
    }

    word=fread_word(fin);
    if (str_cmp(word,"Version")) {
	bugf(
	    "init_config(): Couldn't found version of %s, aborted (found: '%s')",
	    mud_data.mud_config,word);
	abort();
    }

    version=fread_number(fin);
    logf("[0] init_config(): Reading %s, version %d.",
	mud_data.mud_config,version);

    while (!feof(fin)) {
	word=fread_word(fin);

	if (word[0]==0) continue;

	if (word[0]=='#') {
	    if (!str_cmp(word,"#END")) {
		fclose(fin);
		return;
	    }

	    if (!str_cmp(word,"#TIME")) {
		int	offset;
		char *	zone,*tzname;
		int	timecounter=0;

		while (!feof(fin)) {
		    offset=fread_number(fin);
		    zone=fread_string_temp(fin);

		    if (zone[0]=='0') break;

		    mud_data.time_offsets[timecounter]=offset;
		    mud_data.time_zones[timecounter]=str_dup(zone);
		    if (version>1) {
			tzname=fread_string_temp(fin);
			mud_data.time_zonenames[timecounter]=str_dup(tzname);
		    } else
			mud_data.time_zonenames[timecounter]=NULL;
		    timecounter++;
		}
		continue;
	    }

	    if (!str_cmp(word,"#MAX_STRING")) {
		int size;

		size=fread_number(fin);

		if (size<MAX_STRING) 
		    bugf("Configured MAX_STRING(%d) lower than default(%d)",MAX_STRING,size);
		else
		    mud_data.max_string=size;

		continue;
	    }
	}

	bugf("init_config(): Found unknown keyword '%s'",word);
	abort();
    }

    bugf("Unexpected end of file for %s",mud_data.mud_config,version);
    abort();
}

void init_newbie(void) {
    FILE *	fin;
    int		version;
    char *	word;

    if ((fin=fopen(mud_data.newbie_file,"rt"))==NULL) {
	bugf("init_newbie(): Couldn't open %s: %s.",mud_data.newbie_file,ERROR);
	abort();
    }

    word=fread_word(fin);
    if (str_cmp(word,"Version")) {
	bugf(
	    "init_newbie(): Couldn't found version of %s, aborted (found: '%s')",
	    mud_data.newbie_file,word);
	abort();
    }

    version=fread_number(fin);
    logf("[0] init_newbie(): Reading %s, version %d.",
	mud_data.newbie_file,version);

    while (!feof(fin)) {
	word=fread_word(fin);

	if (word[0]==0) continue;

	if (word[0]=='#') {
	    if (!str_cmp(word,"#END")) {
		fclose(fin);
		return;
	    }

	    if (!str_cmp(word,"#ROOMS")) {
		mud_data.newbie_room_level=fread_number(fin);
		mud_data.newbie_room_since=fread_number(fin);
		mud_data.newbie_room_by=str_dup(fread_word(fin));
		continue;
	    }

	    if (!str_cmp(word,"#POLL")) {
		mud_data.poll_level=fread_number(fin);
		mud_data.poll_since=fread_number(fin);
		mud_data.poll_by=str_dup(fread_word(fin));
		continue;
	    }

	    if (!str_cmp(word,"#BOARDS")) {
		char *		type;
		int		i;

		while (!feof(fin)) {
		    type=fread_word(fin);

		    if (type[0]=='0') break;

		    for (i=0;i<MAX_NOTES;i++)
			if (!str_cmp(notes_table[i].name,type))
			    break;
		    if (i==MAX_NOTES) {
			bugf("init_newbie(): found unknown board '%s'",type);
			abort();
		    }

		    mud_data.newbie_notes_level[i]=fread_number(fin);
		    mud_data.newbie_notes_since[i]=fread_number(fin);
		    mud_data.newbie_notes_by[i]=str_dup(fread_word(fin));
		}
		continue;
	    }

	    if (!str_cmp(word,"#CHANNELS")) {
		char *		type;
		int		i;

		while (!feof(fin)) {
		    type=fread_word(fin);

		    if (type[0]=='0') break;

		    for (i=0;i<MAX_CHANNELS;i++)
			if (!str_cmp(channel_table[i].name,type))
			    break;
		    if (i==MAX_CHANNELS) {
			bugf("init_newbie(): found unknown channel '%s'",type);
			abort();
		    }

		    mud_data.newbie_channel_level[i]=fread_number(fin);
		    mud_data.newbie_channel_since[i]=fread_number(fin);
		    mud_data.newbie_channel_by[i]=str_dup(fread_word(fin));
		}
		continue;
	    }
	}

	bugf("init_newbie(): Found unknown keyword '%s'",word);
	abort();
    }

    bugf("Unexpected end of file for %s",mud_data.newbie_file,version);
    abort();
}


void save_config(void) {
    FILE *	fout;
    int		i;

    if ((fout=fopen(mud_data.mud_config,"wt"))==NULL) {
	bugf("save_newbie(): Couldn't open %s for writing: %s",
	    mud_data.mud_config,ERROR);
	return;
    }

    fprintf(fout,"Version 2\n\n");

    fprintf(fout,"#TIME\n");
    for (i=0;i<MAX_TIMEZONES;i++) {
	fprintf(fout,"%d %s~ %s~\n",
	    mud_data.time_offsets[i],
	    mud_data.time_zones[i],
	    mud_data.time_zonenames[i]?mud_data.time_zonenames[i]:""
	);
    }
    fprintf(fout,"0 0~\n\n");

    fprintf(fout,"#MAX_STRING %d\n\n",mud_data.max_string);

    fprintf(fout,"#END\n");
    fclose(fout);
}


void save_newbie(void) {
    FILE *	fout;
    int		i;

    if ((fout=fopen(mud_data.newbie_file,"wt"))==NULL) {
	bugf("save_newbie(): Couldn't open %s for writing: %s",
	    mud_data.newbie_file,ERROR);
	return;
    }

    fprintf(fout,"Version 1\n\n");

    fprintf(fout,"#CHANNELS\n");
    for (i=0;i<MAX_CHANNELS;i++) {
	fprintf(fout,"%s	%d %d %s\n",
	    channel_table[i].name,
	    mud_data.newbie_channel_level[i],
	    (int)mud_data.newbie_channel_since[i],
	    mud_data.newbie_channel_by[i]
	);
    }
    fprintf(fout,"0\n\n");

    fprintf(fout,"#BOARDS\n");
    for (i=0;i<MAX_NOTES;i++) {
	fprintf(fout,"%s	%d %d %s\n",
	    notes_table[i].name,
	    mud_data.newbie_notes_level[i],
	    (int)mud_data.newbie_notes_since[i],
	    mud_data.newbie_notes_by[i]
	);
    }
    fprintf(fout,"0\n\n");

    fprintf(fout,"#ROOMS\n");
    fprintf(fout,"%d %d %s\n",	mud_data.newbie_room_level,
				(int)mud_data.newbie_room_since,
				mud_data.newbie_room_by);
    fprintf(fout,"\n");

    fprintf(fout,"#POLL\n");
    fprintf(fout,"%d %d %s\n",	mud_data.poll_level,
				(int)mud_data.poll_since,
				mud_data.poll_by);
    fprintf(fout,"\n");

    fprintf(fout,"#END\n");
    fclose(fout);
}


void set_newbie(CHAR_DATA *ch,char *argument) {
    char	arg1[MAX_INPUT_LENGTH];
    char	arg2[MAX_INPUT_LENGTH];
    char	arg3[MAX_INPUT_LENGTH];
    int		arg2i,arg3i;
    int		i;

    argument=one_argument(argument,arg1);
    argument=one_argument(argument,arg2);
    argument=one_argument(argument,arg3);
    arg2i=atoi(arg2);
    arg3i=atoi(arg3);

    if (arg1[0]==0) {
	send_to_char("Usage: {Wset newbie channel <channel> <level>{x\n\r",ch);
	send_to_char("       {Wset newbie board <board> <level>{x\n\r",ch);
	send_to_char("       {Wset newbie room <level>{x\n\r",ch);
	send_to_char("       {Wset newbie poll <level>{x\n\r",ch);
	return;
    }

    switch (which_keyword(arg1,"channel","board","room","poll",NULL)) {
    default:
	set_newbie(ch,NULL);
	return;

    case 1:	// channel
	for (i=0;i<MAX_CHANNELS;i++)
	    if (!str_cmp(channel_table[i].name,arg2))
		break;
	if (i==MAX_CHANNELS) {
	    send_to_char("Unknown channel.\n\r",ch);
	    return;
	}
	mud_data.newbie_channel_level[i]=arg3i;
	mud_data.newbie_channel_since[i]=time(NULL);
	free_string(mud_data.newbie_channel_by[i]);
	mud_data.newbie_channel_by[i]=str_dup(ch->name);
	send_to_char("Ok.\n\r",ch);
	break;

    case 2:	// board
	for (i=0;i<MAX_NOTES;i++)
	    if (!str_cmp(notes_table[i].name,arg2))
		break;
	if (i==MAX_NOTES) {
	    send_to_char("Unknown board.\n\r",ch);
	    return;
	}
	mud_data.newbie_notes_level[i]=arg3i;
	free_string(mud_data.newbie_notes_by[i]);
	mud_data.newbie_notes_by[i]=str_dup(ch->name);
	send_to_char("Ok.\n\r",ch);
	break;

    case 3:	// room
	mud_data.newbie_room_level=arg2i;
	mud_data.newbie_room_since=time(NULL);
	free_string(mud_data.newbie_room_by);
	mud_data.newbie_room_by=str_dup(ch->name);
	send_to_char("Ok.\n\r",ch);
	break;

    case 4:	// poll
	mud_data.poll_level=arg2i;
	mud_data.poll_since=time(NULL);
	free_string(mud_data.poll_by);
	mud_data.poll_by=str_dup(ch->name);
	send_to_char("Ok.\n\r",ch);
	break;
    }

    save_newbie();
}

void set_mud( CHAR_DATA *ch, char *argument ) {
    char	arg1[MAX_INPUT_LENGTH];
    char	arg2[MAX_INPUT_LENGTH];
    char	arg3[MAX_INPUT_LENGTH];
    int		arg2i,arg3i;

    if (argument==NULL) {
	send_to_char("Usage: {Wset mud <field>{x\n\r",ch);
	send_to_char("where <field is one of the following:\n\r",ch);
	send_to_char("dnslock, wizlock, newlock, clearcounter\n\r",ch);
	send_to_char("Usage: {Wset mud time <slot> <offset> <name>{x\n",ch);
	send_to_char("Usage: {Wset mud time <slot> <tzfile> <name>{x\n",ch);
	return;
    }

    argument=one_argument(argument,arg1);
    argument=one_argument(argument,arg2);
    argument=one_argument(argument,arg3);
    arg2i=atoi(arg2);
    arg3i=atoi(arg3);

    switch (which_keyword(arg1,"time","clearcounter",
		"dnslock","wizlock","newlock",NULL)) {
    default:
	set_mud(ch,NULL);
	return;

    case 1:	// time
	if (arg2i<0 || arg2i>=MAX_TIMEZONES) {
	    sprintf_to_char(ch,"Slot should be 0..%d\n\r",MAX_TIMEZONES-1);
	    return;
	}
	mud_data.time_offsets[arg2i]=arg3i;
	free_string(mud_data.time_zonenames[arg2i]);
	if (!is_number(arg3)) {
	    mud_data.time_zonenames[arg2i]=str_dup(arg3);
	} else {
	    mud_data.time_zonenames[arg2i]=NULL;
	}
	free_string(mud_data.time_zones[arg2i]);
	mud_data.time_zones[arg2i]=str_dup(argument);
	send_to_char("Okay.\n\r",ch);
	break;

    case 2: // clearcounter
	mud_data.clearcounter_since=time(NULL);
	free_string(mud_data.clearcounter_by);
	mud_data.clearcounter_by=str_dup(ch->name);
	wiznet(WIZ_NONE,0,ch,NULL,
	    "$N has cleared the mud status counters.");
	send_to_char("Mud status counters cleared.\n\r",ch);

	InitMudStatus("reset",0,NULL);
	return;

    case 3: // dnslock
	mud_data.dnslock=!mud_data.dnslock;
	mud_data.dnslock_since=time(NULL);
	free_string(mud_data.dnslock_by);
	mud_data.dnslock_by=str_dup(ch->name);

	if ( mud_data.dnslock ) {
	    wiznet(WIZ_NONE,0,ch,NULL,"$N has dnslocked the game.");
	    send_to_char( "Game dnslocked.\n\r", ch );
	} else {
	    wiznet(WIZ_NONE,0,ch,NULL,"$N removes dnslock.");
	    send_to_char( "Game un-dnslocked.\n\r", ch );
	}
	return;

    case 4: // wizlock
	mud_data.wizlock=!mud_data.wizlock;
	mud_data.wizlock_since=time(NULL);
	mud_data.wizlock_violations=0;
	free_string(mud_data.wizlock_by);
	mud_data.wizlock_by=str_dup(ch->name);

	if ( mud_data.wizlock ) {
	    wiznet(WIZ_NONE,0,ch,NULL,"$N has wizlocked the game.");
	    send_to_char( "Game wizlocked.\n\r", ch );
	} else {
	    wiznet(WIZ_NONE,0,ch,NULL,"$N removes wizlock.");
	    send_to_char( "Game un-wizlocked.\n\r", ch );
	}
	return;

    case 5: // newlock
	mud_data.newlock=!mud_data.newlock;
	mud_data.newlock_since=time(NULL);
	mud_data.newlock_violations=0;
	free_string(ch->name);
	mud_data.newlock_by=str_dup(ch->name);

	if ( mud_data.newlock ) {
	    wiznet(WIZ_NONE,0,ch,NULL,"$N has newlocked the game.");
	    send_to_char( "Game newlocked.\n\r", ch );
	} else {
	    wiznet(WIZ_NONE,0,ch,NULL,"$N removes newlock.");
	    send_to_char( "Game un-newlocked.\n\r", ch );
	}
	return;
    }

    save_config();
}
