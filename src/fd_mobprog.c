//
// $Id: fd_mobprog.c,v 1.78 2006/10/18 19:41:57 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#define NEEDS_TCL
#include "merc.h"
#include "interp.h"
#include "olc.h"

/*
 *   Misc thingies
 */
int mp_error(CHAR_DATA *ch) {
    char *errormsg;

    bugf("MP: TCL error in namespace %s in trigger %s on line %d: %s",
	namespace_char(ch),
	progData==NULL?"(null)":progData->trigName,
    interp->errorLineDontUse,
	Tcl_GetStringResult(interp));

    errormsg=str_dup(Tcl_GetVar(interp,"errorInfo",TCL_GLOBAL_ONLY));
    while (errormsg) {
	char *line=strsep(&errormsg,"\n\r");
	logf("MP: errorInfo: %s",line);
	wiznet(WIZ_TCL_DEBUG,0,NULL,NULL,"MP: errorInfo: %s",line);
    }
    free_string(errormsg);
    Tcl_ResetResult(interp);
    return TCL_ERROR;
}


/*
 * ---------------------------------------------------------------------
 * Trigger handlers. These are called from various parts of the code
 * when an event is triggered.
 * ---------------------------------------------------------------------
 */

/*
 * general trigger for "normal" character/mob interaction
 */

/*
 * In case of a return value (boolean always), a TRUE means
 * "okay, go on" and a FALSE means "this is wrong"
 */
bool mp_general_trigger_bool_all( CHAR_DATA *mob, int trig_type,
                                  CHAR_DATA *ch, ROOM_DATA *room,char *buf,
                                  OBJ_DATA *obj,OBJ_DATA *obj2) {
    TCLPROG_LIST *prg;
    int boolResult;

// Don't think we need these.
//    if (buf==NULL)
//        Tcl_SetVar(interp,"t","",0);
//    else
//        Tcl_SetVar(interp,"t",buf,0);

    if (room==NULL) room=mob->in_room;

    for (prg=mob->mprogs;prg!=NULL;prg=prg->next ) {
        if (prg->trig_type==trig_type) {
            Tcl_Obj *result;

            if (tcl_start(prg->name,buf,mob,ch,obj,NULL,obj2,room,NULL))
		break;// if one start fails they tend to all fail

	    if (prg->trig_phrase && str_cmp("",prg->trig_phrase)) {
		if (tcl_run_mp_code(mob,prg->trig_phrase)==TCL_ERROR)
		    goto error;

		result=Tcl_GetObjResult(interp);
		boolResult=FALSE;
		if (Tcl_GetBooleanFromObj(interp,result,&boolResult)==TCL_ERROR)
		    goto error;
	    } else
		boolResult=TRUE;

            if (boolResult) {
                if (tcl_run_mp_code(mob,prg->code)==TCL_ERROR)
		    goto error;
                result=Tcl_GetObjResult(interp);
                boolResult=FALSE;
                if (Tcl_GetBooleanFromObj(interp,result,&boolResult)==TCL_ERROR)
		    goto error;
                if (tcl_stop()) return FALSE;
                return boolResult;
            }

            if(tcl_stop()) return FALSE;
        }
    }
    return TRUE;

error:
    mp_error(mob);
    if(tcl_stop()) return FALSE;
    return FALSE;
}

void mp_general_trigger_all( CHAR_DATA *mob,int trig_type,
                             CHAR_DATA *ch, ROOM_DATA *room,char *buf,
                             OBJ_DATA *obj,OBJ_DATA *obj2)
{
    TCLPROG_LIST *prg;
    int boolResult;

// Don't think we need these.
//    if (buf==NULL)
//        Tcl_SetVar(interp,"t","",0);
//    else
//        Tcl_SetVar(interp,"t",buf,0);

    if (room==NULL) room=mob->in_room;

    for (prg=mob->mprogs;prg!=NULL;prg=prg->next ) {
        if(prg->trig_type==trig_type) {
            Tcl_Obj *result;

            if (tcl_start(prg->name,buf,mob,ch,obj,NULL,obj2,room,NULL)) return;

	    if (prg->trig_phrase && str_cmp("",prg->trig_phrase)) {
		if (tcl_run_mp_code(mob,prg->trig_phrase)==TCL_ERROR)
		    goto error;

		result=Tcl_GetObjResult(interp);
		boolResult=FALSE;
		if (Tcl_GetBooleanFromObj(interp,result,&boolResult)==TCL_ERROR)
		    goto error;
	    } else
		boolResult=TRUE;

            if (boolResult) {
                if (tcl_run_mp_code(mob,prg->code)==TCL_ERROR)
		    goto error;
                if(tcl_stop()) return;
                break;
            }

            if(tcl_stop()) return;
        }
    }
    return;

error:
    mp_error(mob);
    if(tcl_stop()) return;
    return;
}

//
// by making these wrappers it's easier to add new fields to the
// rp_general_trigger() function.
//
// trigger1: mob
// trigger2: mob + ch
// trigger3: mob + ch + obj
// trigger4: mob + ch + text
// trigger5: mob + text
//
bool mp_general_trigger1_bool(int trig_type,CHAR_DATA *mob)
{ return mp_general_trigger_bool_all(mob,trig_type,NULL,NULL,NULL,NULL,NULL); }
bool mp_general_trigger2_bool(int trig_type,CHAR_DATA *mob,CHAR_DATA *ch)
{ return mp_general_trigger_bool_all(mob,trig_type,ch,NULL,NULL,NULL,NULL); }
bool mp_general_trigger3_bool(int trig_type,CHAR_DATA *mob,CHAR_DATA *ch,OBJ_DATA *obj)
{ return mp_general_trigger_bool_all(mob,trig_type,ch,NULL,NULL,obj,NULL); }
bool mp_general_trigger4_bool(int trig_type,CHAR_DATA *mob,CHAR_DATA *ch,char *txt)
{ return mp_general_trigger_bool_all(mob,trig_type,ch,NULL,txt,NULL,NULL); }
bool mp_general_trigger5_bool(int trig_type,CHAR_DATA *mob,char *txt)
{ return mp_general_trigger_bool_all(mob,trig_type,NULL,NULL,txt,NULL,NULL); }



void mp_general_trigger1(int trig_type,CHAR_DATA *mob)
    { mp_general_trigger_all(mob,trig_type,NULL,NULL,NULL,NULL,NULL); }
void mp_general_trigger2(int trig_type,CHAR_DATA *mob,CHAR_DATA *ch)
    { mp_general_trigger_all(mob,trig_type,ch,NULL,NULL,NULL,NULL); }
void mp_general_trigger3(int trig_type,CHAR_DATA *mob,CHAR_DATA *ch,OBJ_DATA *obj)
    { mp_general_trigger_all(mob,trig_type,ch,NULL,NULL,obj,NULL); }
void mp_general_trigger4(int trig_type,CHAR_DATA *mob,CHAR_DATA *ch,char *txt)
    { mp_general_trigger_all(mob,trig_type,ch,NULL,txt,NULL,NULL); }
void mp_general_trigger5(int trig_type,CHAR_DATA *mob,char *txt)
    { mp_general_trigger_all(mob,trig_type,NULL,NULL,txt,NULL,NULL); }





/*
 * A general purpose string trigger. Matches argument to a string trigger
 * phrase.
 */
void mp_act_trigger(
	char *argument, CHAR_DATA *mob, CHAR_DATA *ch,
	const void *arg1, const void *arg2, int type )
{
    TCLPROG_LIST *prg;
    int boolResult;

    Tcl_SetVar(interp,"t",argument,0);

    for ( prg = mob->mprogs; prg != NULL; prg = prg->next )
    {
    	if ( prg->trig_type == type )
    	{
    	    Tcl_Obj *result;

    	    if(tcl_start(prg->name,argument,mob,ch,NULL,NULL,NULL,NULL,NULL)) return;
    	    if(prg->trig_type==MTRIG_ACT)
    	    {
		int carg1,carg2;

		tcl_parse_act(argument,&carg1,&carg2);

		switch(carg1) {
		case 1: progData->text1=(char *)arg1;break;
		case 2: break;
		case 4: progData->obj=tcl_check_obj(arg1);break;
		}
		switch(carg2) {
		case 1: progData->text2=(char *)arg2;break;
		case 2: progData->ch2=tcl_check_ch(arg2);break;
		case 4: progData->obj2=tcl_check_obj(arg2);break;
		case 8: progData->text2=arg2?(char *)arg2:"door";break;
		}
	    }

	    if (prg->trig_phrase && str_cmp("",prg->trig_phrase)) {
		if (tcl_run_mp_code(mob,prg->trig_phrase)==TCL_ERROR)
		    goto error;

		result=Tcl_GetObjResult(interp);
		boolResult=FALSE;
		if (Tcl_GetBooleanFromObj(interp,result,&boolResult)==TCL_ERROR)
		    goto error;
	    } else
		boolResult=TRUE;

            if(boolResult) {
		if (tcl_run_mp_code(mob,prg->code)==TCL_ERROR)
		    goto error;
		if(tcl_stop()) return;
		break;
            }

            if(tcl_stop()) return;
	}
    }

    return;

error:
    mp_error(mob);
    if(tcl_stop()) return;
}

//
// Time related
//
void mp_delay_trigger(CHAR_DATA *mob) {
    mp_general_trigger1(MTRIG_DELAY,mob);
}
void mp_random_trigger(CHAR_DATA *mob) {
    mp_general_trigger1(MTRIG_RANDOM,mob);
}
void mp_hour_trigger(CHAR_DATA *mob,int hour) {
    char buf[INT_STRING_BUF_SIZE];
    itoa_r(hour,buf,sizeof(buf));
    mp_general_trigger5(MTRIG_HOUR,mob,buf);
}
void mp_timer_trigger( CHAR_DATA *mob ) {
    char buf[LONG_STRING_BUF_SIZE];
    ltoa_r(mob->mprog_timer,buf,sizeof(buf));
    mp_general_trigger5(MTRIG_TIMER,mob,buf);
}

//
// Fighting related
//

bool mp_preattack_trigger(CHAR_DATA *mob,CHAR_DATA *attacker,char *means) {
    return mp_general_trigger4_bool(MTRIG_PREATTACK,mob,attacker,means);
}

void mp_fight_trigger(CHAR_DATA *mob,CHAR_DATA *victim) {
    mp_general_trigger2(MTRIG_FIGHT,mob,victim);
}
void mp_kill_trigger(CHAR_DATA *mob,CHAR_DATA *attacker) {
    mp_general_trigger2(MTRIG_KILL,mob,attacker);
}
void mp_death_trigger(CHAR_DATA *mob,CHAR_DATA *killer) {
    mp_general_trigger2(MTRIG_DEATH,mob,killer);
}
void mp_killed_trigger(CHAR_DATA *mob,CHAR_DATA *victim) {
    mp_general_trigger2(MTRIG_KILLED,mob,victim);
}
bool mp_surrender_trigger(CHAR_DATA *mob,CHAR_DATA *ch) {
    return mp_general_trigger2_bool(MTRIG_SURRENDER,mob,ch);
}
void mp_flee_trigger(CHAR_DATA *ch) {
    if (ch->fighting==NULL) return;
    mp_general_trigger2(MTRIG_FLEE,ch->fighting,ch);
}
void mp_hpcnt_trigger(CHAR_DATA *mob,CHAR_DATA *ch) {
    mp_general_trigger2(MTRIG_HITPOINTCOUNT,mob,ch);
}
void mp_walked_trigger(CHAR_DATA *mob) {
    char id[20];
    CHAR_DATA *ch;
    switch (mob->hunt_type) {
	case HUNT_FIND:
	case HUNT_GFIND:
	case HUNT_KILL:
	    snprintf(id,20,"g:%ld",mob->hunt_id);
	    ch=tcl_charid_char(mob,id,NULL);
	    mp_general_trigger2(MTRIG_WALKED,mob,ch);
	    break;
	case HUNT_ROOM:
	    mp_general_trigger1(MTRIG_WALKED,mob);
	    break;
    }
}
void mp_didnotfind_trigger(CHAR_DATA *mob, int resultcode) {
    char reason[10];
    itoa_r(resultcode,reason,sizeof(reason));
    mp_general_trigger5(MTRIG_DIDNOTFIND,mob,reason);
}

//
// Load, exit, entrance, leaving
//
void mp_load_trigger(CHAR_DATA *mob,CHAR_DATA *loader) {
    mp_general_trigger2(MTRIG_LOAD,mob,loader);
}

void mp_entry_trigger(CHAR_DATA *mob) {
    mp_general_trigger1(MTRIG_ENTRY,mob);
}

bool mp_exit_trigger(CHAR_DATA *ch,int dir) {
    CHAR_DATA *mob,*mob_next;
    char buf[INT_STRING_BUF_SIZE];

    itoa_r(dir,buf,sizeof(buf));

    mob=ch->in_room->people;
    while (mob!=NULL) {
	mob_next=mob->next_in_room;

	if (!IS_VALID(mob)) return TRUE;	// these things happen sometimes

	if (ch==mob) { mob=mob_next; continue; }
	if (!IS_NPC(mob)) { mob=mob_next; continue; }

	if (MOB_HAS_TRIGGER(mob,MTRIG_EXIT)) {
	    if ( can_see( mob, ch ) )
		if (mp_general_trigger4_bool(MTRIG_EXIT,mob,ch,buf)==FALSE)
		    return FALSE;
	}
	if (MOB_HAS_TRIGGER(mob,MTRIG_EXALL) &&
	    (get_trust(mob) >= ch->invis_level)) {
	    if (mp_general_trigger4_bool(MTRIG_EXALL,mob,ch,buf)==FALSE)
		return FALSE;
	}
	mob=mob_next;
    }
    return TRUE;
}

void mp_leave_trigger(CHAR_DATA *ch,int dir) {
    CHAR_DATA *mob,*mob_next;
    char buf[INT_STRING_BUF_SIZE];

    itoa_r(dir,buf,sizeof(buf));

    mob=ch->in_room->people;
    while (mob!=NULL) {
	mob_next=mob->next_in_room;

	if (!IS_VALID(mob)) return;	// these things happen sometimes

	if (ch==mob) { mob=mob_next; continue; }
	if (!IS_NPC(mob)) { mob=mob_next; continue; }

	if (MOB_HAS_TRIGGER(mob,MTRIG_LEAVE)) {
	    if ( can_see( mob, ch ) )
		mp_general_trigger4(MTRIG_LEAVE,mob,ch,buf);
	}
	if (MOB_HAS_TRIGGER(mob,MTRIG_LEALL) &&
	    (get_trust(mob) >= ch->invis_level)) {
	    mp_general_trigger4(MTRIG_LEALL,mob,ch,buf);
	}

	mob=mob_next;
    }
}

void mp_greet_trigger(CHAR_DATA *ch,int dir) {
    CHAR_DATA *mob,*mob_next;
    char buf[INT_STRING_BUF_SIZE];

    if (dir>=0 && dir < DIR_MAX)
	itoa_r(rev_dir[dir],buf,sizeof(buf));
    else {
	bugf("mp_greet_trigger(): direction is not 0 <= i < DIR_MAX in "
	     "room #%d for %s",ch->in_room->vnum,NAME(ch));
	return;
    }

    mob=ch->in_room->people;
    while (mob!=NULL) {
	mob_next=mob->next_in_room;

	if (!IS_VALID(mob)) return;	// these things happen sometimes

	if (ch==mob) { mob=mob_next; continue; }
	if (!IS_NPC(mob)) { mob=mob_next; continue; }

	if (MOB_HAS_TRIGGER(mob,MTRIG_GREET)) {
	    if ( can_see( mob, ch ) )
		mp_general_trigger4(MTRIG_GREET,mob,ch,buf);
	}
	if (MOB_HAS_TRIGGER(mob,MTRIG_GRALL) &&
	    (get_trust(mob) >= ch->invis_level)) {
	    mp_general_trigger4(MTRIG_GRALL,mob,ch,buf);
	}

	mob=mob_next;
    }
}

bool	mp_prerecall_trigger(CHAR_DATA *player) {
    CHAR_DATA *mob,*mob_next;

    mob=player->in_room->people;
    while (mob!=NULL) {
	mob_next=mob->next_in_room;

	if (!IS_VALID(mob)) return TRUE;	// these things happen sometimes

	if (IS_NPC(mob) && MOB_HAS_TRIGGER(mob,MTRIG_PRERECALL))
	    if (mp_general_trigger2_bool(MTRIG_PRERECALL,mob,player)==FALSE)
		return FALSE;

	mob=mob_next;
    }
    return TRUE;
}
void	mp_recall_trigger(CHAR_DATA *player) {
    CHAR_DATA *mob,*mob_next;

    mob=player->in_room->people;
    while (mob!=NULL) {
	mob_next=mob->next_in_room;

	if (!IS_VALID(mob)) return;	// these things happen sometimes

	if (IS_NPC(mob) && MOB_HAS_TRIGGER(mob,MTRIG_RECALL))
	    mp_general_trigger2(MTRIG_RECALL,mob,player);
	mob=mob_next;
    }
}
void	mp_recallto_trigger(CHAR_DATA *player) {
    CHAR_DATA *mob,*mob_next;

    mob=player->in_room->people;
    while (mob!=NULL) {
	mob_next=mob->next_in_room;

	if (!IS_VALID(mob)) return;	// these things happen sometimes

	if (IS_NPC(mob) && MOB_HAS_TRIGGER(mob,MTRIG_RECALL_TO))
	    mp_general_trigger2(MTRIG_RECALL_TO,mob,player);
	mob=mob_next;
    }
}

//
// give triggers
//
void mp_give_trigger(CHAR_DATA *mob,CHAR_DATA *ch,OBJ_DATA *obj) {
    mp_general_trigger3(MTRIG_GIVE,mob,ch,obj);
}
void mp_bribe_trigger(CHAR_DATA *mob,CHAR_DATA *ch,int amount) {
    char buf[INT_STRING_BUF_SIZE];
    itoa_r(amount,buf,sizeof(buf));
    mp_general_trigger4(MTRIG_BRIBE,mob,ch,buf);
}

//
// look triggers
//
bool mp_prelookat_trigger(CHAR_DATA *mob, CHAR_DATA *ch) {
    return mp_general_trigger2_bool(MTRIG_PRELOOKAT,mob,ch);
}
void mp_lookat_trigger(CHAR_DATA *mob, CHAR_DATA *ch) {
    mp_general_trigger2(MTRIG_LOOKAT,mob,ch);
}

//
// speech triggers
//
void mp_speech_trigger(CHAR_DATA *ch,char *text) {
    CHAR_DATA *mob,*mob_next;

    mob=ch->in_room->people;
    while (mob!=NULL) {
	mob_next=mob->next_in_room;

	if (!IS_VALID(mob)) return;	// these things happen sometimes

	if (IS_NPC(mob)
	&&  MOB_HAS_TRIGGER(mob,MTRIG_SPEECH))
	    mp_general_trigger4(MTRIG_SPEECH,mob,ch,text);
	mob=mob_next;
    }
}
void mp_tell_trigger(CHAR_DATA *mob,CHAR_DATA *player,char *text) {
    mp_general_trigger4(MTRIG_SPEECH,mob,player,text);
}

//
// Buy triggers, better leave them like this
//
bool mp_prebuy_trigger(CHAR_DATA *ch, CHAR_DATA *shopkeeper) {
    TCLPROG_LIST *prg;
    int boolResult;

    Tcl_SetVar(interp,"t","",0);

    if(IS_NPC(shopkeeper) && MOB_HAS_TRIGGER(shopkeeper,MTRIG_PREBUY)) {
	for ( prg = shopkeeper->mprogs; prg != NULL; prg = prg->next ) {
	    if(prg->trig_type==MTRIG_PREBUY) {
		Tcl_Obj *result;

		if(tcl_start(prg->name,NULL,shopkeeper,ch,NULL,NULL,NULL,NULL,NULL)) return FALSE;

		if (prg->trig_phrase && str_cmp("",prg->trig_phrase)) {
		    if (tcl_run_mp_code(shopkeeper,prg->trig_phrase)==TCL_ERROR)
			goto error;

		    result=Tcl_GetObjResult(interp);
		    boolResult=FALSE;
		    if (Tcl_GetBooleanFromObj(interp,result,&boolResult)==TCL_ERROR)
			goto error;
		} else
		    boolResult=TRUE;

		if(boolResult) {
		    if (tcl_run_mp_code(shopkeeper,prg->code)==TCL_ERROR)
			goto error;

		    result=Tcl_GetObjResult(interp);
		    boolResult=FALSE;
		    if(Tcl_GetBooleanFromObj(interp,result,&boolResult)==TCL_ERROR) goto error;

		    if(tcl_stop()) return FALSE;
		    return boolResult;
		}
		if (tcl_stop()) return FALSE;
	    }
	}
    }

    return TRUE;	// no triggers? always allowed

error:
    if(tcl_stop()) return FALSE;
    mp_error(shopkeeper);
    return FALSE;
}

int mp_buy_trigger(CHAR_DATA *ch, CHAR_DATA *shopkeeper, OBJ_DATA *obj) {
    TCLPROG_LIST *prg;
    int intResult;
    int boolResult;

    Tcl_SetVar(interp,"t","",0);

    if(IS_NPC(shopkeeper) && MOB_HAS_TRIGGER(shopkeeper,MTRIG_BUY)) {
	for ( prg = shopkeeper->mprogs; prg != NULL; prg = prg->next ) {
	    if(prg->trig_type==MTRIG_BUY) {
		Tcl_Obj *result;

		if(tcl_start(prg->name,NULL,shopkeeper,ch,obj,NULL,NULL,NULL,NULL)) return MP_BUY_NO_TRIGGER;

		if (prg->trig_phrase && str_cmp("",prg->trig_phrase)) {
		    if (tcl_run_mp_code(shopkeeper,prg->trig_phrase)==TCL_ERROR)
			goto error;

		    result=Tcl_GetObjResult(interp);
		    boolResult=FALSE;
		    if (Tcl_GetBooleanFromObj(interp,result,&boolResult)==TCL_ERROR)
			goto error;
		} else
		    boolResult=TRUE;

		if(boolResult) {
		    if (tcl_run_mp_code(shopkeeper,prg->code)==TCL_ERROR)
			goto error;

		    result=Tcl_GetObjResult(interp);
		    intResult=FALSE;
		    if(Tcl_GetIntFromObj(interp,result,&intResult)==TCL_ERROR) goto error;

		    if(tcl_stop()) return MP_BUY_NO_TRIGGER;
		    switch (intResult) {
			case 0: return MP_BUY_NOT_ALLOWED;
			case 1: return MP_BUY_ALLOWED;
			case 2: return MP_BUY_I_GAVE_IT;
		    }
		}
		if (tcl_stop()) return MP_BUY_NO_TRIGGER;
	    }
	}
    }

    return MP_BUY_NO_TRIGGER;

error:
    if(tcl_stop()) return MP_BUY_NO_TRIGGER;
    mp_error(shopkeeper);
    return MP_BUY_NO_TRIGGER;
}

void mp_postbuy_trigger(CHAR_DATA *ch, CHAR_DATA *shopkeeper) {
    mp_general_trigger2(MTRIG_POSTBUY,shopkeeper,ch);
}


//
// Interpret triggers
//
void mp_interpret(int TRIGTYPE,CHAR_DATA *ch,char *command) {
    CHAR_DATA *mob,*mob_next;

    mob=ch->in_room->people;
    while (mob!=NULL) {
	mob_next=mob->next_in_room;

	if (!IS_VALID(mob)) return;	// these things happen sometimes

	if (IS_NPC(mob) && MOB_HAS_TRIGGER(mob,TRIGTYPE))
	    mp_general_trigger4(TRIGTYPE,mob,ch,command);

	mob=mob_next;
    }
}
bool mp_interpret_bool(int TRIGTYPE,CHAR_DATA *ch,char *command) {
    CHAR_DATA *mob,*mob_next;

    mob=ch->in_room->people;
    while (mob!=NULL) {
	mob_next=mob->next_in_room;

	if (!IS_VALID(mob)) return TRUE;	// these things happen sometimes

	if (IS_NPC(mob) && MOB_HAS_TRIGGER(mob,TRIGTYPE))
	    if (mp_general_trigger4_bool(TRIGTYPE,mob,ch,command)==FALSE)
		return FALSE;

	mob=mob_next;
    }
    return TRUE;
}

bool mp_interpret_unknown( CHAR_DATA *ch, char *command) {
    return !mp_interpret_bool(MTRIG_INTERPRET_UNKNOWN,ch,command);
}
bool mp_interpret_preknown( CHAR_DATA *ch, char *command) {
    return !mp_interpret_bool(MTRIG_INTERPRET_PREKNOWN,ch,command);
}
void mp_interpret_postknown( CHAR_DATA *ch, char *command) {
    mp_interpret(MTRIG_INTERPRET_POSTKNOWN,ch,command);
}

void mp_social_trigger(CHAR_DATA *mob,char *string,CHAR_DATA *ch) {
    mp_general_trigger4(MTRIG_SOCIAL,mob,ch,string);
}
bool mp_presocial_trigger(CHAR_DATA *mob,char *string,CHAR_DATA *ch) {
    if (IS_NPC(mob) && MOB_HAS_TRIGGER(mob,MTRIG_PRESOCIAL))
	if (mp_general_trigger4_bool(MTRIG_PRESOCIAL,mob,ch,string)==FALSE)
	    return TRUE;
    return FALSE;
}

//
// day / sun triggers
//
void	mp_doallmobs(int trigger, char *text) {
    CHAR_DATA *mob=char_list;
    CHAR_DATA *mob_next;

    while (mob!=NULL) {
	mob_next=mob->next;

	if (!IS_VALID(mob)) return;	// these things happen sometimes

	if (IS_NPC(mob) && MOB_HAS_TRIGGER(mob,trigger))
	    mp_general_trigger5(trigger,mob,text);

	mob=mob_next;
    }
}
void    mp_sunset_trigger(void) {
    mp_doallmobs(MTRIG_SUNSET,NULL);
}
void    mp_sunrise_trigger(void) {
    mp_doallmobs(MTRIG_SUNRISE,NULL);
}
void    mp_dayend_trigger(void) {
    mp_doallmobs(MTRIG_DAYEND,NULL);
}
void    mp_daystart_trigger(void) {
    mp_doallmobs(MTRIG_DAYSTART,NULL);
}

void	mp_weather_trigger(int old_state, int new_state) {
    char *old_descr,*new_descr;
    char *text;

    old_descr=table_find_value(old_state,weather_sky_table);
    new_descr=table_find_value(new_state,weather_sky_table);
    text=malloc(strlen(old_descr)+strlen(new_descr)+2);

    strcpy(text,new_descr);
    strcat(text," ");
    strcat(text,old_descr);
    mp_doallmobs(MTRIG_WEATHER,text);
    free(text);
}

//
// special prog triggers
//
bool mp_prespec_trigger(CHAR_DATA *mob,CHAR_DATA *victim,OBJ_DATA *obj) {
    return mp_general_trigger3_bool(MTRIG_PRESPEC,mob,victim,obj);
}

void mp_spec_trigger(CHAR_DATA *mob,CHAR_DATA *victim,OBJ_DATA *obj){
    mp_general_trigger3(MTRIG_SPEC,mob,victim,obj);
}
