/* $Id: alias.c,v 1.19 2002/09/21 20:35:09 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,	   *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *									   *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael	   *
 *  Chastain, Michael Quan, and Mitchell Tse.				   *
 *									   *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc	   *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.						   *
 *									   *
 *  Much time and thought has gone into this software and you are	   *
 *  benefitting.  We hope that you share your changes too.  What goes	   *
 *  around, comes around.						   *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

#include "merc.h"

/* does aliasing and other fun stuff */
void substitute_alias(DESCRIPTOR_DATA *d, char *argument)
{
    CHAR_DATA *ch;
    char buf[MAX_STRING_LENGTH],prefix[MAX_INPUT_LENGTH],name[MAX_INPUT_LENGTH];
    char *point;
    ALIAS_TYPE *alias;

    ch = d->original ? d->original : d->character;

    /* check for prefix */
    if (ch->prefix[0] != '\0' && str_prefix("prefix",argument))
    {
	if (strlen(ch->prefix) + strlen(argument) > MAX_INPUT_LENGTH-2)
	    send_to_char("Line to long, prefix not processed.\r\n",ch);
	else {
	    sprintf(prefix,"%s %s",ch->prefix,argument);
	    argument = prefix;
	}
    }

    if (IS_NPC(ch) || ch->pcdata->aliases == NULL
    ||	!str_prefix("alias",argument) || !str_prefix("una",argument)
    ||  !str_prefix("prefix",argument))
    {
	interpret(d->character,argument);
	return;
    }

    strcpy(buf,argument);

    for (alias = ch->pcdata->aliases; alias ; alias=alias->next) {

	if (!str_prefix(alias->alias,argument))
	{
	    point = one_argument(argument,name);
	    if (!str_cmp(alias->alias,name))
	    {
		buf[0] = '\0';
		strcat(buf,alias->command);
		if (point[0]) {
		    strcat(buf," ");
		    strcat(buf,point);
		}
		strcpy(buf, get_multi_command( d, buf));

		if (strlen(buf) > MAX_INPUT_LENGTH-1) {
		    send_to_char(
			"Alias substitution too long. Truncated.\r\n",ch);
		    buf[MAX_INPUT_LENGTH -1] = '\0';
		}
		break;
	    }
	}
    }
    interpret(d->character,buf);
}

void do_alia(CHAR_DATA *ch, char *argument)
{
    send_to_char("I'm sorry, alias must be entered in full.\n\r",ch);
    return;
}

void do_alias(CHAR_DATA *ch, char *argument)
{
    CHAR_DATA *rch;
    char arg[MAX_INPUT_LENGTH],buf[MAX_STRING_LENGTH];
    ALIAS_TYPE *alias;

    smash_tilde(argument);

    if (ch->desc == NULL)
	rch = ch;
    else
	rch = ch->desc->original ? ch->desc->original : ch;

    if (IS_NPC(rch))
	return;

    argument = one_argument(argument,arg);


    if (arg[0] == '\0') {		// show all aliases
	ALIAS_TYPE *aliases[512];	// at most 512 aliases.
	int top=0,i,j;

	if (rch->pcdata->aliases == NULL) {
	    send_to_char("You have no aliases defined.\n\r",ch);
	    return;
	}
	send_to_char("Your current aliases are:\n\r",ch);

	// sort them a little.
	for (alias=rch->pcdata->aliases;alias;alias=alias->next) {
	    for ( i = 0; i < top; i++ )
		if ( strcmp(aliases[i]->alias,alias->alias)>0)
		    break;
	    for ( j = top; j > i; j--)
		aliases[j]=aliases[j-1];
	    aliases[i]=alias;
	    top++;
	    if (top==512) break;	// silently break
	}

	for (i=0;i<top;i++) {
	    sprintf(buf,"    %s:  %s\n\r",aliases[i]->alias,aliases[i]->command);
	    send_to_char(buf,ch);
	}
	return;
    }

    if (!str_prefix("una",arg) || !str_cmp("alias",arg)) {
	send_to_char("Sorry, that word is reserved.\n\r",ch);
	return;
    }

    if (strchr(arg,' ')||strchr(arg,'"')||strchr(arg,'\'')) {
	send_to_char("The word to be aliased should not contain a space, "
	    "a tick or a double-quote.\n\r",ch);
	return;
    }

    if (argument[0] == '\0') {		// show one alias
	for (alias=rch->pcdata->aliases;alias;alias=alias->next) {
	    if (!str_cmp(arg,alias->alias)) {
		sprintf(buf,"%s aliases to '%s'.\n\r",
		    alias->alias,alias->command);
		send_to_char(buf,ch);
		return;
	    }
	}

	send_to_char("That alias is not defined.\n\r",ch);
	return;
    }

    if (!str_prefix(argument,"delete") || !str_prefix(argument,"prefix")) {
	send_to_char("That shall not be done!\n\r",ch);
	return;
    }

    for (alias=rch->pcdata->aliases;alias;alias=alias->next) {
	if (!str_cmp(arg,alias->alias)) {	// redefine an alias

	    free_string(alias->command);
	    alias->command=str_dup(argument);
	    sprintf(buf,"%s is now realiased to '%s'.\n\r",
		alias->alias,alias->command);
	    send_to_char(buf,ch);
	    return;
	}
    }

    /* make a new alias */
    alias=new_alias();
    alias->alias=str_dup(arg);
    alias->command=str_dup(argument);
    alias->next=rch->pcdata->aliases;
    rch->pcdata->aliases=alias;
    sprintf(buf,"%s is now aliased to '%s'.\n\r",
	alias->alias,alias->command);
    send_to_char(buf,ch);
}


void do_unalias(CHAR_DATA *ch, char *argument)
{
    CHAR_DATA *rch;
    char arg[MAX_INPUT_LENGTH];
    ALIAS_TYPE *alias,*prevalias=NULL;
    bool found = FALSE;

    if (ch->desc == NULL)
	rch = ch;
    else
	rch = ch->desc->original ? ch->desc->original : ch;

    if (IS_NPC(rch))
	return;

    argument = one_argument(argument,arg);

    if (arg[0] == '\0') {
	send_to_char("Unalias what?\n\r",ch);
	return;
    }

    for (alias=rch->pcdata->aliases;alias;alias=alias->next) {
	if (!str_cmp(arg,alias->alias)) {
	    sprintf_to_char(ch,"Alias '%s' removed.\n\r",alias->alias);
	    if (prevalias) {
		prevalias->next=alias->next;
	    } else {
		rch->pcdata->aliases=alias->next;
	    }
	    free_alias(alias);
	    return;
	}
	prevalias=alias;
    }

    if (!found)
	send_to_char("No alias of that name to remove.\n\r",ch);
}

