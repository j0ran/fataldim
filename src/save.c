/* $Id: save.c,v 1.160 2008/03/26 14:37:25 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

/*
 * 19980106 EG  do_finger() has been modified to show the idle time
        when a user is visible on and the Last on time when
        the user is invisible or not logged on.
 */

#define NEEDS_TCL
#include "merc.h"
#include "fd_mxp.h"
#ifdef I3
#include "intermud3/fd_i3.h"
#endif


int rename(const char *oldfname, const char *newfname);

char *print_flags(long flag)
{
    int count, pos = 0;
    static char buf[52];


    for (count = 0; count < 32;  count++)
    {
        if (IS_SET(flag,1<<count))
        {
            if (count < 26)
                buf[pos] = 'A' + count;
            else
                buf[pos] = 'a' + (count - 26);
            pos++;
        }
    }

    if (pos == 0)
    {
        buf[pos] = '0';
        pos++;
    }

    buf[pos] = '\0';

    return buf;
}


/*
 * Array of containers read for proper re-nesting of objects.
 */
#define MAX_NEST	100
static	OBJ_DATA *	rgObjNest	[MAX_NEST];



/*
 * Local functions.
 */
void	fwrite_char	( CHAR_DATA *ch,  FILE *fp);
void	fwrite_objs	( OBJ_DATA  *obj, FILE *fp, int iNest, int level);
void	fwrite_obj	( OBJ_DATA  *obj, FILE *fp, int iNest, int level);
void	fwrite_pet	( CHAR_DATA *pet, FILE *fp);
void    fwrite_notebook	( CHAR_DATA *ch,  FILE *fp);
void	fread_char	( CHAR_DATA *ch,  FILE *fp);
void    fread_pet	( CHAR_DATA *ch,  FILE *fp);
void	fread_obj	( void *ch_room,  FILE *fp, bool charload);
void    fread_notebook	( CHAR_DATA *ch,  FILE *fp);


/* Save the inventory of a room.
 */
void save_room_obj( ROOM_INDEX_DATA *room )
{
    char strsave[MAX_INPUT_LENGTH];
    FILE *fp;

    fclose(fpReserve);
    sprintf(strsave,"%s/r%d.roo",mud_data.room_dir,room->vnum);
    if((fp=fopen(strsave,"w"))==NULL)
    {
        bugf( "Couldn't save the objects in room '%s' (vnum %d)",
              room->name,room->vnum);
        logf( "[0] save_room_obj(): fopen(%s): %s",strsave,ERROR);
        fpReserve = fopen( NULL_FILE, "r" );
        return;
    }

    if (room->contents)
        fwrite_objs(room->contents,fp,0,0);
    fprintf( fp, "#END\n" );
    fclose( fp );
    fpReserve = fopen( NULL_FILE, "r" );
    return;
}

void save_all_room_obj(void)
{
    ROOM_INDEX_DATA *room;
    int i;

    for(i=0;i<MAX_KEY_HASH;i++)
        for(room=room_index_hash[i];room;room=room->next)
        {
            if(STR_IS_SET(room->strbit_room_flags,ROOM_SAVEOBJ))
                save_room_obj(room);
        }

    return;
}

int start_room(bool *room_init, FILE *fp, ROOM_INDEX_DATA *room) {
    if (*room_init) return 1;

    fprintf(fp,"#ROOM %d\r\n",room->vnum);

    *room_init=TRUE;
    return 1;
}

void new_save_all_room_obj(void) {
    AREA_DATA *area;
    ROOM_INDEX_DATA *room;
    OBJ_DATA *obj;
    int i;
    FILE *fp;
    char *strsave;
    bool room_init;

    for (area=area_first;area;area=area->next) {
        bool failed=FALSE;

        asprintf(&strsave,"%s/%s.roo",mud_data.room_dir,area->filename);
        if (!strncmp(".are",strsave+strlen(strsave)-8,4))
            strcpy(strsave+strlen(strsave)-8,".roo");

        fclose(fpReserve);
        if((fp=fopen(strsave,"w"))==NULL) {
            bugf( "Couldn't save the objects in area '%s' (vnum %d)",
                  area->name,area->vnum);
            logf( "[0] save_all_room_obj(): fopen(%s): %s",strsave,ERROR);
            fpReserve = fopen( NULL_FILE, "r" );
            continue;
        }

        if (area->area_vars[0]!=0) {
            char varname[MSL];
            char *tail;
            char *buf;
            const char *value;
            fprintf(fp,"#AREA_VARS\n");

            strcpy(varname,namespace_darea(area));
            tail=varname+strlen(varname);
            *(tail++)=':';
            *(tail++)=':';

            buf=one_argument(area->area_vars,tail);
            while (tail[0]!=0) {

                value=Tcl_GetVar(interp,varname,0);
                if (value) {
                    fprintf(fp,"%s~ %s~\n",tail,value);
                } else
                    bugf("Can't save persistent area variable: %s",varname);

                buf=one_argument(buf,tail);
            }
            fprintf(fp,"$\n");
        }

        for(i=0;i<MAX_KEY_HASH && !failed;i++)
            for(room=room_index_hash[i];room;room=room->next) {
                if (room->vnum<area->lvnum || room->vnum>area->uvnum) continue;
                if (!room->contents) continue;
                room_init=FALSE;

                if(STR_IS_SET(room->strbit_room_flags,ROOM_SAVEOBJ)) {
                    if (!start_room(&room_init, fp, room)) {failed=TRUE; break;}
                    fwrite_objs(room->contents,fp,0,0);
                } else {
                    for (obj=room->contents;obj;obj=obj->next_content)
                        if (STR_IS_SET(obj->strbit_extra_flags,ITEM_SAVEOBJ) ||
                                STR_IS_SET(obj->strbit_extra_flags,ITEM_INSTALLED)) {
                            if (!start_room(&room_init, fp, room)) {failed=TRUE; break;}
                            fwrite_obj(obj,fp,0,0);
                        }
                }
                if (room_init) {
                    fprintf(fp, "#END\n");
                }
            }
        fprintf( fp, "#END_OF_AREA\n" );
        fclose( fp );
        fpReserve = fopen( NULL_FILE, "r" );
        free(strsave);
    }
}

/*
 * Save a character and inventory.
 * Would be cool to save NPC's too for quest purposes,
 *   some of the infrastructure is provided.
 */
void save_char_obj( CHAR_DATA *ch )
{
    char strsave[MAX_INPUT_LENGTH];
    FILE *fp;

    if (IS_NPC(ch))
        return;
    if (STR_IS_SET(ch->strbit_act,PLR_NOSAVE))
        return;

    //
    // Don't save if the character is invalidated.
    // This might happen during the auto-logoff of players.
    // (or other places not yet found out)
    //
    if ( !IS_VALID(ch)) {
        bugf("save_char_obj: Trying to save an invalidated character.\n");
        return;
    }

    if ( ch->desc != NULL && ch->desc->original != NULL )
        ch = ch->desc->original;

    fclose( fpReserve );
    sprintf( strsave, "%s/%s", mud_data.player_dir, capitalize( ch->name ) );
    if ( ( fp = fopen( TEMP_FILE, "w" ) ) == NULL )
    {
        bugf( "Couldn't write playerfile for %s.",ch->name);
        logf("[0] save_char_obj(): fopen(%s): %s",TEMP_FILE,ERROR);
        fpReserve = fopen( NULL_FILE, "r" );
        return;
    }

    fwrite_char( ch, fp );
    if ( ch->carrying != NULL )
        fwrite_objs( ch->carrying, fp, 0, ch->level );
    /* save the pets */
    if (ch->pet != NULL && (
                ch->pet->in_room == ch->in_room || ch->was_in_room) )
        fwrite_pet(ch->pet,fp);
    if (ch->pcdata->notebook != NULL)
        fwrite_notebook(ch,fp);
    fprintf( fp, "#END\n" );
    fclose( fp );

    rename(TEMP_FILE,strsave);
    fpReserve = fopen( NULL_FILE, "r" );
    return;
}

/*
 *  2 : added rom-basic skills
 *  3 :
 *  4 : roam gold (/100)
 *  5 : fix levels
 *  6 :
 *  7 : fix time for playerfiles (into seconds)
 *  8 : Added adrealine to condition fields
 *  9 : conversion of wiznet into string-bits (first try)
 * 10 : conversion of grant, wiznet into string-bits
 * 11 : conversion of effects into string-bits: AfBy, Aff2, ExtF, Ex2F
 * 12 : conversion of effects into string-bits: AffD, Affc, Af2c
 * 13 : conversion of ch->comm into ch->strbit_comm
 * 14 : notebook added flagsfield
 * 15 : added knight to ranks in clans
 * 16 : no-sell moved from property to OBJ_NOSELL
 * 17 : conversion of acts into string-bits
 * 18 : removal of arg2 from EFFECT_DATA
 * 19 : changed layout of properties
 *
 * 23 : new note spool
 */


/*
 * Write the char.
 */
void fwrite_char( CHAR_DATA *ch, FILE *fp )
{
    EFFECT_DATA *pef;
    ALIAS_TYPE *alias;
    int sn, gn;
    char temp[2*MAX_VNUMS/8+10];	// for beeninroom
    char strbit[MAX_FLAGS];
    char buf[MSL];

    if (STR_IS_SET(ch->strbit_act,PLR_NOSAVE))
        return;

    fprintf( fp, "#%s\n", IS_NPC(ch) ? "MOB" : "PLAYER"	);

    fprintf( fp, "Name %s~\n",	ch->name		);
#ifdef HAS_ALTS
    if (ch->pcdata->uberalt[0])
        fprintf( fp, "UberAlt %s~\n",	ch->pcdata->uberalt	);
    if (ch->pcdata->alts[0])
        fprintf( fp, "Alts %s~\n",	ch->pcdata->alts	);
#endif
    fprintf( fp, "Id   %ld\n",	ch->id			);
    fprintf( fp, "LogO %d\n",	(int)current_time	);
    fprintf( fp, "Vers %d\n",   23			);
    if (ch->short_descr[0] != '\0')
        fprintf( fp, "ShD  %s~\n",	ch->short_descr	);
    if( ch->long_descr[0] != '\0')
        fprintf( fp, "LnD  %s~\n",	ch->long_descr	);
    if (ch->description[0] != '\0')
        fprintf( fp, "Desc %s~\n",	ch->description	);
    if (ch->prompt != NULL || !str_cmp(ch->prompt,"<%hhp %mm %vmv> "))
        fprintf( fp, "Prom %s~\n",      ch->prompt  	);
    fprintf( fp, "Race %s~\n", pc_race_table[ch->race].name );
    //  if (ch->clan_clan) {
    //    	fprintf( fp, "Clan %s~\n",ch->clan_clan->name);
    //	fprintf( fp, "Rank %d\n",ch->clan_member->rank);
    //  }
    fprintf( fp, "Sex  %d\n",	show_sex(ch)		);
    fprintf( fp, "Cla  %d\n",	ch->class		);
    fprintf( fp, "Levl %d\n",	ch->level		);
    jobs_to_str(buf,MSL,ch);
    fprintf( fp, "Jobs %s~\n",	buf);
    fprintf( fp, "Killed %d\n",	ch->pcdata->killed	);
    fprintf( fp, "Grnd %s~\n",	stringtohex(ch->pcdata->strbitgrand,
                                            temp,sizeof(ch->pcdata->strbitgrand)));
    fprintf( fp, "Been %s~\n",	stringtohex(ch->pcdata->beeninroom,
                                            temp,sizeof(ch->pcdata->beeninroom)));
    fprintf( fp, "KilledThatMob %s~\n",	stringtohex(ch->pcdata->killedthatmob,
                                                    temp,sizeof(ch->pcdata->killedthatmob)));
    fprintf( fp, "SeenThatMob %s~\n",	stringtohex(ch->pcdata->seenthatmob,
                                                    temp,sizeof(ch->pcdata->seenthatmob)));
    fprintf( fp, "UsedThatObject %s~\n",stringtohex(ch->pcdata->usedthatobject,
                                                    temp,sizeof(ch->pcdata->usedthatobject)));
    fprintf( fp, "SeenThatObject %s~\n",stringtohex(ch->pcdata->seenthatobject,
                                                    temp,sizeof(ch->pcdata->seenthatobject)));
    if (ch->recall != 0)
        fprintf( fp, "Reca %d\n",	ch->recall	);
    if (ch->trust != 0)
        fprintf( fp, "Tru  %d\n",	ch->trust	);
    fprintf( fp, "Plyd %d\n",
             ch->played + (int) (current_time - ch->logon)	);
    fprintf( fp, "Secu %d\n",ch->pcdata->security	);
    fprintf( fp, "Not  %d %d %d %d %d %d\n",
             (int)ch->pcdata->last_notes[NOTE_NOTE],
             (int)ch->pcdata->last_notes[NOTE_IDEA],
             (int)ch->pcdata->last_notes[NOTE_PENALTY],
             (int)ch->pcdata->last_notes[NOTE_NEWS],
             (int)ch->pcdata->last_notes[NOTE_CHANGES],
             (int)ch->pcdata->last_notes[NOTE_MCNOTE]);
    fprintf( fp, "NotP %d %d\n",
             (int)ch->pcdata->last_notes[NOTE_BUG],
             (int)ch->pcdata->last_notes[NOTE_TYPO]);
    fprintf( fp, "Scro %d\n", 	ch->pcdata->default_lines);
    fprintf( fp, "Room %d\n",
             ( ch->was_in_room != NULL )
             ? ch->was_in_room->vnum
             : ch->in_room == NULL ? ROOM_VNUM_TEMPLE : ch->in_room->vnum );

    fprintf( fp, "HMV  %d %d %d %d %d %d\n",
             ch->hit, ch->max_hit, ch->mana, ch->max_mana, ch->move, ch->max_move );
    if (ch->gold > 0)
        fprintf( fp, "Gold %ld\n",	ch->gold		);
    else
        fprintf( fp, "Gold %d\n", 0			);
    if (ch->silver > 0)
        fprintf( fp, "Silv %ld\n",ch->silver		);
    else
        fprintf( fp, "Silv %d\n",0			);
    if(ch->pcdata->bank>0)
        fprintf( fp, "Bank %ld\n", ch->pcdata->bank );
    fprintf( fp, "Exp  %d\n",	ch->exp			);

    // copy them, we need them on the player!
    STR_COPY_STR(strbit,ch->strbit_act,MAX_FLAGS);
    STR_REMOVE_BIT(strbit,PLR_ORDERED);
    STR_REMOVE_BIT(strbit,PLR_PUEBLO);
    STR_REMOVE_BIT(strbit,PLR_MXP);
    STR_REMOVE_BIT(strbit,PLR_SWITCHED);
#ifdef HAS_MCCP
    STR_REMOVE_BIT(strbit,PLR_MCCP2);
    STR_REMOVE_BIT(strbit,PLR_MCCP);
#endif
    if (!STR_SAME_STR(strbit,ZERO_FLAG,MAX_FLAGS))
        fprintf(fp, "Act %s~\n",stringtohex(strbit,temp,MAX_FLAGS));

    if (!STR_SAME_STR(ch->strbit_affected_by,ZERO_FLAG,MAX_FLAGS))
        fprintf(fp, "AfBy %s~\n",
                stringtohex(ch->strbit_affected_by,temp,MAX_FLAGS));
    if (!STR_SAME_STR(ch->strbit_affected_by2,ZERO_FLAG,MAX_FLAGS))
        fprintf(fp, "Aff2 %s~\n",
                stringtohex(ch->strbit_affected_by2,temp,MAX_FLAGS));

    if (!STR_SAME_STR(ch->strbit_vuln_flags,ZERO_FLAG,MAX_FLAGS))
        fprintf(fp, "Vuln %s~\n",
                stringtohex(ch->strbit_vuln_flags,temp,MAX_FLAGS));
    if (!STR_SAME_STR(ch->strbit_res_flags,ZERO_FLAG,MAX_FLAGS))
        fprintf(fp, "Resi %s~\n",
                stringtohex(ch->strbit_res_flags,temp,MAX_FLAGS));
    if (!STR_SAME_STR(ch->strbit_imm_flags,ZERO_FLAG,MAX_FLAGS))
        fprintf(fp, "Immu %s~\n",
                stringtohex(ch->strbit_imm_flags,temp,MAX_FLAGS));

    fprintf( fp, "Comm %s~\n",
             stringtohex(ch->strbit_comm,temp,MAX_FLAGS));
    fprintf( fp, "Wizn %s~\n",  stringtohex(ch->strbit_wiznet,
                                            temp,sizeof(ch->strbit_wiznet)));
    if (ch->pcdata->pueblo_picture[0])
        fprintf( fp, "Picture %s~\n",	ch->pcdata->pueblo_picture);
    if (ch->invis_level)
        fprintf( fp, "Invi %d\n", 	ch->invis_level	);
    if (ch->incog_level)
        fprintf(fp,"Inco %d\n",ch->incog_level);
    fprintf( fp, "Pos  %d\n",
             ch->position == POS_FIGHTING ? POS_STANDING : ch->position );
    if (ch->on)
        fprintf( fp, "On   %d %ld\n", ch->on->pIndexData->vnum, ch->on->id);
    if (ch->practice != 0)
        fprintf( fp, "Prac %d\n",	ch->practice	);
    if (ch->train != 0)
        fprintf( fp, "Trai %d\n",	ch->train	);
    if (ch->saving_throw != 0)
        fprintf( fp, "Save  %d\n",	ch->saving_throw);
    fprintf( fp, "Alig  %d\n",	ch->alignment		);
    if (ch->hitroll != 0)
        fprintf( fp, "Hit   %d\n",	ch->hitroll	);
    if (ch->damroll != 0)
        fprintf( fp, "Dam   %d\n",	ch->damroll	);
    fprintf( fp, "ACs %d %d %d %d\n",
             ch->armor[AC_PIERCE],ch->armor[AC_BASH],
             ch->armor[AC_SLASH],ch->armor[AC_EXOTIC]);
    if (ch->wimpy !=0 )
        fprintf( fp, "Wimp  %d\n",	ch->wimpy	);
    fprintf( fp, "Attr %d %d %d %d %d\n",
             ch->perm_stat[STAT_STR],
             ch->perm_stat[STAT_INT],
             ch->perm_stat[STAT_WIS],
             ch->perm_stat[STAT_DEX],
             ch->perm_stat[STAT_CON] );

    fprintf (fp, "AMod %d %d %d %d %d\n",
             ch->mod_stat[STAT_STR],
             ch->mod_stat[STAT_INT],
             ch->mod_stat[STAT_WIS],
             ch->mod_stat[STAT_DEX],
             ch->mod_stat[STAT_CON] );

    if ( IS_NPC(ch) )
    {
        fprintf( fp, "Vnum %d\n",	ch->pIndexData->vnum	);
    }
    else
    {
        fprintf( fp, "Pass %s~\n",	ch->pcdata->pwd		);
        if (ch->pcdata->bamfin[0] != '\0')
            fprintf( fp, "Bin  %s~\n",	ch->pcdata->bamfin);
        if (ch->pcdata->bamfout[0] != '\0')
            fprintf( fp, "Bout %s~\n",	ch->pcdata->bamfout);
        fprintf( fp, "Titl %s~\n",	ch->pcdata->title	);
        if (ch->pcdata->whoname[0])
            fprintf(fp,"WhoN %s~\n",	ch->pcdata->whoname	);
        fprintf( fp, "Pnts %d\n",   	ch->pcdata->points      );
        fprintf( fp, "TSex %d\n",	ch->pcdata->true_sex	);
        fprintf( fp, "LLev %d\n",	ch->pcdata->last_level	);
        fprintf( fp, "HMVP %d %d %d\n", ch->pcdata->perm_hit,
                 ch->pcdata->perm_mana,
                 ch->pcdata->perm_move);
        fprintf( fp, "Cnd  %d %d %d %d %d\n",
                 ch->pcdata->condition[COND_DRUNK],
                 ch->pcdata->condition[COND_FULL],
                 ch->pcdata->condition[COND_THIRST],
                 ch->pcdata->condition[COND_HUNGER],
                 ch->pcdata->condition[COND_ADRENALINE] );

        if(ch->pcdata->questpoints)
            fprintf( fp, "QPnt %d\n",	ch->pcdata->questpoints );

        /* quest-stuff */
        //	fprintf(fp,"Quest %d %d %d %d\n",
        //	    ch->pcdata->nextquest,
        //	    ch->pcdata->countdown,
        //	    ch->pcdata->questobj,
        //	    ch->pcdata->questmob);

        /* write alias */
        for (alias=ch->pcdata->aliases;alias;alias=alias->next) {
            fprintf(fp,"Alias %s %s~\n",alias->alias,alias->command);
        }

        for ( sn = 0; sn < MAX_SKILL; sn++ )
        {
            if ( skill_exists(sn) && has_gained_skill(ch,sn) )
            {
                fprintf( fp, "Sk %d %d %d '%s'\n",
                         ch->pcdata->skill[sn].learned,
                         ch->pcdata->skill[sn].forgotten,
                         ch->pcdata->skill[sn].counter,
                         skill_name(sn,ch) );
            }
        }

        for ( gn = 0; gn < MAX_GROUP; gn++ )
        {
            if ( group_exists(gn) && ch->pcdata->group_known[gn])
            {
                fprintf( fp, "Gr '%s'\n",group_name(gn,NULL));
            }
        }
    }

    for ( pef = ch->affected; pef != NULL; pef = pef->next )
    {
        if (pef->type < 0 || pef->type>= MAX_SKILL)
            continue;

        fprintf( fp, "Af2c '%s' %3d %3d %3d %3d %3d %10d %d\n",
                 skill_name(pef->type,ch),
                 pef->where,
                 pef->level,
                 pef->duration,
                 pef->modifier,
                 pef->location,
                 pef->bitvector,
                 pef->arg1
                 );
    }

    save_property(fp,"Prop",ch->property,"fwrite_char",ch->name);

#ifdef I3
    {
        I3_CHANNEL *channel;
        for (channel=I3_channel_list;channel;channel=channel->next) {
            if (find_i3_listener_by_char(channel,ch)) {
                fprintf(fp,"I3-channel %s~\n",channel->local_name);
            }
        }
    }
#endif

    fprintf( fp, "End\n\n" );
    return;
}

/* write a pet */
void fwrite_pet( CHAR_DATA *pet, FILE *fp)
{
    EFFECT_DATA *pef;
    char temp[MSL];

    fprintf(fp,"#PET\n");

    fprintf(fp,"Vnum %d\n",pet->pIndexData->vnum);

    fprintf(fp,"Name %s~\n", pet->name);
    fprintf(fp,"LogO %d\n", (int)current_time);
    if (pet->short_descr != pet->pIndexData->short_descr)
        fprintf(fp,"ShD  %s~\n", pet->short_descr);
    if (pet->long_descr != pet->pIndexData->long_descr)
        fprintf(fp,"LnD  %s~\n", pet->long_descr);
    if (pet->description != pet->pIndexData->description)
        fprintf(fp,"Desc %s~\n", pet->description);
    if (pet->race != pet->pIndexData->race)
        fprintf(fp,"Race %s~\n", race_table[pet->race].name);
    //  if (pet->clan_clan) {
    //  	fprintf( fp, "Clan %s~\n",pet->clan_clan->name);
    //  }
    fprintf(fp,"Sex  %d\n", show_sex(pet));
    if (pet->level != pet->pIndexData->level)
        fprintf(fp,"Levl %d\n", pet->level);
    fprintf(fp, "HMV  %d %d %d %d %d %d\n",
            pet->hit, pet->max_hit, pet->mana, pet->max_mana, pet->move, pet->max_move);
    if (pet->gold > 0)
        fprintf(fp,"Gold %ld\n",pet->gold);
    if (pet->silver > 0)
        fprintf(fp,"Silv %ld\n",pet->silver);
    if (pet->exp > 0)
        fprintf(fp, "Exp  %d\n", pet->exp);

    if (!STR_SAME_STR(pet->strbit_act,pet->pIndexData->strbit_act,MAX_FLAGS))
        fprintf(fp, "Act %s~\n",stringtohex(pet->strbit_act,temp,MAX_FLAGS));

    if (!STR_SAME_STR(pet->strbit_affected_by,
                      pet->pIndexData->strbit_affected_by,MAX_FLAGS))
        fprintf(fp, "AfBy %s~\n",
                stringtohex(pet->strbit_affected_by,temp,MAX_FLAGS));
    if (!STR_SAME_STR(pet->strbit_affected_by2,
                      pet->pIndexData->strbit_affected_by2,MAX_FLAGS))
        fprintf(fp, "Aff2 %s~\n",
                stringtohex(pet->strbit_affected_by2,temp,MAX_FLAGS));
    fprintf(fp, "Comm %s~\n", stringtohex(pet->strbit_comm,temp,MAX_FLAGS));
    fprintf(fp,"Pos  %d\n", pet->position = POS_FIGHTING ? POS_STANDING : pet->position);
    if (pet->saving_throw != 0)
        fprintf(fp, "Save %d\n", pet->saving_throw);
    if (pet->alignment != pet->pIndexData->alignment)
        fprintf(fp, "Alig %d\n", pet->alignment);
    if (pet->hitroll != pet->pIndexData->hitroll)
        fprintf(fp, "Hit  %d\n", pet->hitroll);
    if (pet->damroll != pet->pIndexData->damage[DICE_BONUS])
        fprintf(fp, "Dam  %d\n", pet->damroll);
    fprintf(fp, "ACs  %d %d %d %d\n",
            pet->armor[AC_PIERCE],pet->armor[AC_BASH],
            pet->armor[AC_SLASH],pet->armor[AC_EXOTIC]);
    fprintf(fp, "Attr %d %d %d %d %d\n",
            pet->perm_stat[STAT_STR], pet->perm_stat[STAT_INT],
            pet->perm_stat[STAT_WIS], pet->perm_stat[STAT_DEX],
            pet->perm_stat[STAT_CON]);
    fprintf(fp, "AMod %d %d %d %d %d\n",
            pet->mod_stat[STAT_STR], pet->mod_stat[STAT_INT],
            pet->mod_stat[STAT_WIS], pet->mod_stat[STAT_DEX],
            pet->mod_stat[STAT_CON]);

    for ( pef = pet->affected; pef != NULL; pef = pef->next )
    {
        if (pef->type < 0 || pef->type >= MAX_SKILL)
            continue;

        fprintf( fp, "Af2c '%s' %3d %3d %3d %3d %3d %10d %d\n",
                 skill_name(pef->type,NULL),
                 pef->where,
                 pef->level,
                 pef->duration,
                 pef->modifier,
                 pef->location,
                 pef->bitvector,
                 pef->arg1
                 );
    }

    save_property(fp,"Prop",pet->property,"fwrite_pet",pet->name);

    fprintf(fp,"End\n\n");
    return;
}

/*
 * Write all objects (starting with obj) and its contents.
 * If level!=0 then the objects will be saved as if they where in the
 * inventory of an player.
 */

void fwrite_objs( OBJ_DATA *obj, FILE *fp, int iNest, int level ) {
    /*
     * Slick recursion to write lists backwards,
     *   so loading them will load in forwards order.
     */
    //    if ( obj->next_content != NULL )
    //	fwrite_obj( obj->next_content, fp, iNest, level );

    // set prev_content
    obj->prev_content=NULL;
    while (obj->next_content) {
        obj->next_content->prev_content=obj;
        obj=obj->next_content;
    }
    do {
        fwrite_obj(obj,fp,iNest,level);
    } while ((obj=obj->prev_content));

    return;
}


void fwrite_obj( OBJ_DATA *obj, FILE *fp, int iNest, int level )
{
    EXTRA_DESCR_DATA *ed;
    EFFECT_DATA *pef;
    char s[MAX_STRING_LENGTH];

    /*
     * Castrate storage characters.
     */
    if (level) {

        // don't save objects two higher in level
        if (level < obj->level - 2 && obj->item_type != ITEM_CONTAINER)
            return;

        // don't save non-private keys
        if (obj->item_type == ITEM_KEY &&
                !obj_is_owned(obj))
            return;

        // don't save maps. (removed by EG)
        // if (obj->item_type == ITEM_MAP && !obj->value[0]))
        //     return;
    } else {
        // don't save keys in rooms
        if (obj->item_type == ITEM_KEY)
            return;
    }

    fprintf( fp, "#O\n" );
    fprintf( fp, "Vnum %d\n",   obj->pIndexData->vnum        );
    if (obj->enchanted)
        fprintf( fp,"Enchanted\n");
    fprintf( fp, "Nest %d\n",	iNest	  	     );

    /* these data are only used if they do not match the defaults */

    if ( obj->name != obj->pIndexData->name)
        fprintf( fp, "Name %s~\n",	obj->name		     );
    if ( obj->short_descr != obj->pIndexData->short_descr)
        fprintf( fp, "ShD  %s~\n",	obj->short_descr	     );
    if ( obj->description != obj->pIndexData->description)
        fprintf( fp, "Desc %s~\n",	obj->description	     );
    if (!STR_SAME_STR(obj->strbit_extra_flags,
                      obj->pIndexData->strbit_extra_flags,MAX_FLAGS))
        fprintf( fp, "ExtF %s~\n",
                 stringtohex(obj->strbit_extra_flags,s,MAX_FLAGS));
    if (!STR_SAME_STR(obj->strbit_extra_flags2,
                      obj->pIndexData->strbit_extra_flags2,MAX_FLAGS))
        fprintf( fp, "Ex2F %s~\n",
                 stringtohex(obj->strbit_extra_flags2,s,MAX_FLAGS));
    if ( obj->wear_flags != obj->pIndexData->wear_flags)
        fprintf( fp, "WeaF %ld\n",	obj->wear_flags		     );
    if ( obj->item_type != obj->pIndexData->item_type)
        fprintf( fp, "Ityp %d\n",	obj->item_type		     );
    if ( obj->weight != obj->pIndexData->weight)
        fprintf( fp, "Wt   %d\n",	obj->weight		     );
    if ( obj->condition != obj->pIndexData->condition)
        fprintf( fp, "Cond %d\n",	obj->condition		     );
    if ( obj_is_owned(obj)) {
        fprintf( fp, "Owner %s~\n",	obj->owner_name		     );
        fprintf( fp, "OwnerId %ld\n",	obj->owner_id	     );
    }

    /* variable data */

    fprintf( fp, "Wear %d\n",   obj->wear_loc                );
    if (obj->level != obj->pIndexData->level)
        fprintf( fp, "Lev  %d\n",	obj->level		     );
    if (obj->timer != 0)
        fprintf( fp, "Time %d\n",	obj->timer	     );
    fprintf( fp, "Cost %d\n",	obj->cost		     );
    if (obj->value[0] != obj->pIndexData->value[0]
            ||  obj->value[1] != obj->pIndexData->value[1]
            ||  obj->value[2] != obj->pIndexData->value[2]
            ||  obj->value[3] != obj->pIndexData->value[3]
            ||  obj->value[4] != obj->pIndexData->value[4])
        fprintf( fp, "Val  %d %d %d %d %d\n",
                 obj->value[0], obj->value[1], obj->value[2], obj->value[3],
                obj->value[4]	     );

    switch ( obj->item_type )
    {
    case ITEM_POTION:
    case ITEM_SCROLL:
    case ITEM_PILL:
        if ( obj->value[1] > 0 )
        {
            fprintf( fp, "Spell 1 '%s'\n",
                     skill_name(obj->value[1],NULL));
        }

        if ( obj->value[2] > 0 )
        {
            fprintf( fp, "Spell 2 '%s'\n",
                     skill_name(obj->value[2],NULL));
        }

        if ( obj->value[3] > 0 )
        {
            fprintf( fp, "Spell 3 '%s'\n",
                     skill_name(obj->value[3],NULL));
        }

        break;

    case ITEM_STAFF:
    case ITEM_WAND:
        if ( obj->value[3] > 0 )
        {
            fprintf( fp, "Spell 3 '%s'\n",
                     skill_name(obj->value[3],NULL));
        }

        break;
    }

    for ( pef = obj->affected; pef != NULL; pef = pef->next )
    {
        if (pef->type < 0 || pef->type >= MAX_SKILL)
            continue;

        fprintf( fp, "Af2c '%s' %3d %3d %3d %3d %3d %10d %d\n",
                 skill_name(pef->type,NULL),
                 pef->where,
                 pef->level,
                 pef->duration,
                 pef->modifier,
                 pef->location,
                 pef->bitvector,
                 pef->arg1
                 );
    }

    for ( ed = obj->extra_descr; ed != NULL; ed = ed->next )
    {
        fprintf( fp, "ExDe %s~ %s~\n",
                 ed->keyword, ed->description );
    }

    save_property(fp,"Prop",obj->property,"fwrite_obj",obj->name);

    fprintf( fp, "End\n\n" );

    if ( obj->contains != NULL )
        fwrite_objs( obj->contains, fp, iNest + 1, level );


    return;
}


//
// write on notebook page (recursive)
//
void fwrite_notebookpage(CHAR_DATA *ch,FILE *fp,NOTEBK_DATA *page) {
    if (page==NULL)
        return;

    fwrite_notebookpage(ch,fp,page->next);

    smash_tilde(page->subject);
    smash_tilde(page->text);
    fprintf(fp,"%d\n", (int)page->created);
    fprintf(fp,"%d\n", (int)page->modified);
    fprintf(fp,"%s\n", print_flags(page->flags));
    fprintf(fp,"%s~\n", page->subject);
    fprintf(fp,"%s~\n", page->text);
}

//
// write a whole notebook
//
void fwrite_notebook( CHAR_DATA *ch, FILE *fp)
{
    fprintf(fp,"#NOTEBOOK\n");
    fwrite_notebookpage(ch,fp,ch->pcdata->notebook);
    fprintf(fp,"End\n\n" );
}

/*
 * Load a char and inventory into a new ch structure.
 */
bool load_char_obj( DESCRIPTOR_DATA *d, char *name )
{
    char strsave[MAX_INPUT_LENGTH];
    char buf[100];
    CHAR_DATA *ch;
    FILE *fp;
    bool found;
    int stat;

    ch = new_char();
    ch->pcdata = new_pcdata();

    if(d) d->character			= ch;
    ch->desc				= d;
    ch->name				= str_dup( name );
    ch->id				= get_pc_id();
    ch->race				= race_lookup("human");
    STR_ZERO_BIT(ch->strbit_act,sizeof(ch->strbit_act));
    STR_SET_BIT(ch->strbit_act,PLR_NOSUMMON);
    STR_ZERO_BIT(ch->strbit_comm,sizeof(ch->strbit_comm));
    STR_SET_BIT(ch->strbit_comm,COMM_COMBINE);
    ch->prompt 				=str_dup("%h/%Hhp %m/%Mma %vmv | %e >");
    ch->pcdata->confirm_delete		= FALSE;
    ch->pcdata->clan_recruit		= 0;
    ch->pcdata->pwd			= str_dup( "" );
    ch->pcdata->bamfin			= str_dup( "" );
    ch->pcdata->bamfout			= str_dup( "" );
    ch->pcdata->title			= str_dup( "" );
    ch->pcdata->whoname			= str_dup( "" );
    ch->pcdata->pueblo_picture		= str_dup( "" );
#ifdef HAS_ALTS
    ch->pcdata->uberalt			= str_dup( "" );
    ch->pcdata->alts			= str_dup( "" );
#endif
    ch->pcdata->idle			= time(NULL);
    for (stat =0; stat < MAX_STATS; stat++)
        ch->perm_stat[stat]		= 13;
    ch->pcdata->condition[COND_THIRST]	= 48;
    ch->pcdata->condition[COND_FULL]	= 48;
    ch->pcdata->condition[COND_HUNGER]	= 48;
    ch->pcdata->condition[COND_ADRENALINE]= 0;
    ch->pcdata->security		= 0;
    STR_ZERO_BIT(ch->pcdata->strbitgrand,sizeof(ch->pcdata->strbitgrand));
    STR_ZERO_BIT(ch->strbit_wiznet,sizeof(ch->strbit_wiznet));

    found = FALSE;
    fclose( fpReserve );

#ifdef UNIX
    // decompress if .gz file exists
    sprintf( strsave, "%s/%s.gz", mud_data.player_dir, capitalize(name));
    if ( ( fp = fopen( strsave, "r" ) ) != NULL ) {
        fclose(fp);
        sprintf(buf,"gzip -dfq %s",strsave);
        system(buf);
    }
#endif

    sprintf( strsave, "%s/%s", mud_data.player_dir, capitalize( name ) );
    if ( ( fp = fopen( strsave, "r" ) ) != NULL )
    {
        int iNest;

        for ( iNest = 0; iNest < MAX_NEST; iNest++ )
            rgObjNest[iNest] = NULL;

        found = TRUE;
        for ( ; ; )
        {
            char letter;
            char *word;

            letter = fread_letter( fp );
            if ( letter == '*' )
            {
                fread_to_eol( fp );
                continue;
            }

            if ( letter != '#' )
            {
                bugf( "Load_char_obj: # not found for %s.", name);
                break;
            }

            word = fread_word( fp );
            if      ( !str_cmp( word, "PLAYER" ) ) fread_char ( ch, fp );
            else if ( !str_cmp( word, "OBJECT" ) ) fread_obj  ( ch, fp, TRUE );
            else if ( !str_cmp( word, "O"      ) ) fread_obj  ( ch, fp, TRUE );
            else if ( !str_cmp( word, "PET"    ) ) fread_pet  ( ch, fp );
            else if ( !str_cmp( word, "NOTEBOOK") ) fread_notebook ( ch,fp );
            else if ( !str_cmp( word, "END"    ) ) break;
            else
            {
                bugf( "Load_char_obj: bad section for %s: %s.",name,word );
                break;
            }
        }
        fclose( fp );
    }

    fpReserve = fopen( NULL_FILE, "r" );

    if (!found) return FALSE;

    /* initialize race */
    {
        int i;

        if (ch->race == 0)
            ch->race = race_lookup("human");

        ch->size = pc_race_table[ch->race].size;
        ch->dam_type = 17; /*punch */

        for (i = 0; i < 5; i++)
        {
            if (pc_race_table[ch->race].skills[i] == NULL)
                break;
            group_add(ch,pc_race_table[ch->race].skills[i],FALSE);
        }
        STR_OR_STR(ch->strbit_affected_by,
                   race_table[ch->race].strbit_eff,MAX_FLAGS);
        STR_OR_STR(ch->strbit_imm_flags,
                   race_table[ch->race].strbit_imm,MAX_FLAGS);
        STR_OR_STR(ch->strbit_res_flags,
                   race_table[ch->race].strbit_res,MAX_FLAGS);
        STR_OR_STR(ch->strbit_vuln_flags,
                   race_table[ch->race].strbit_vuln,MAX_FLAGS);
        ch->form	= race_table[ch->race].form;
        ch->parts	= race_table[ch->race].parts;
    }


    /* RT initialize skills */

#ifdef notdef
    if (ch->version < 2)  /* need to add the new skills */
    {
        group_add(ch,"rom basics",FALSE);
        group_add(ch,class_table[ch->class].base_group,FALSE);
        group_add(ch,class_table[ch->class].default_group,TRUE);
        ch->pcdata->learned[gsn_recall] = 50;
    }
#endif

    /* fix levels */
    if (ch->version == 5 && (ch->level > 52 || ch->trust > 52))
    {
        if(ch->level>52) ch->level+=40;
        if(ch->trust>52) ch->trust+=40;
    }

    if (ch->version < 7)
    {
        ch->played=ch->level*3*3600;
    }

    /* strip not usefull flags */
    STR_REMOVE_BIT(ch->strbit_act,PLR_QUITTING);
    STR_REMOVE_BIT(ch->strbit_act,PLR_NOSAVE);

    if ((ch->clan=get_clanmember_by_charname(ch->name))!=NULL) {
        //This function is also called before an invalid login, so don't set lastonline here
        //ch->clan->lastonline=time(NULL);
        ch->clan->player=ch;
    }

    if (STR_IS_SET(ch->strbit_comm,COMM_AFK)) {
        char buf[MAX_STRING_LENGTH];
        buf[0]=0;
        free_string(ch->pcdata->afk_text);
        if (!GetCharProperty(ch,PROPERTY_STRING,"afk-message",buf) ||
                (buf[0]==0))
            ch->pcdata->afk_text=NULL;
        else {
            if(buf[0]!=',' && buf[0]!='.' &&
                    buf[0]!='!' && buf[0]!='?') {
                int p;
                buf[40]=0;
                for (p=strlen(buf);p>=0;p--)
                    buf[p+1]=buf[p];
                buf[0]=' ';
            }
            buf[41]=0;
            ch->pcdata->afk_text=buf[0]?str_dup(buf):NULL;
        }
    }

    return TRUE;
}

/*
 * Load the contenst of a room.
 */
bool load_room_obj( ROOM_INDEX_DATA *room)
{
    char strsave[MAX_INPUT_LENGTH];
    FILE *fp;
    bool found;
    int iNest;

    found = FALSE;
    fclose( fpReserve );
    sprintf( strsave, "%s/r%d.roo", mud_data.room_dir,room->vnum);
    if ( ( fp = fopen( strsave, "r" ) ) != NULL ) {

        for ( iNest = 0; iNest < MAX_NEST; iNest++ )
            rgObjNest[iNest] = NULL;

        found = TRUE;
        for ( ; ; )
        {
            char letter;
            char *word;

            letter = fread_letter( fp );
            if ( letter == '*' )
            {
                fread_to_eol( fp );
                continue;
            }

            if ( letter != '#' )
            {
                bugf("Load_room_obj: # not found in r%d.roo.",room->vnum);
                break;
            }

            word = fread_word( fp );
            if      ( !str_cmp( word, "OBJECT" ) ) fread_obj( room, fp, FALSE );
            else if ( !str_cmp( word, "O"      ) ) fread_obj( room, fp, FALSE );
            else if ( !str_cmp( word, "END"    ) ) break;
            else {
                bugf("Load_room_obj: bad section in r%d.roo, found %s.",
                     room->vnum,word);
                break;
            }
        }
        fclose( fp );
    }

    fpReserve = fopen( NULL_FILE, "r" );

    return found;
}

void load_all_room_obj(void)
{
    ROOM_INDEX_DATA *room;
    int i;

    for(i=0;i<MAX_KEY_HASH;i++)
        for(room=room_index_hash[i];room;room=room->next)
        {
            if(STR_IS_SET(room->strbit_room_flags,ROOM_SAVEOBJ))
                load_room_obj(room);
        }

    return;
}

int new_load_room_obj(ROOM_DATA *room,FILE *fp) {
    int iNest;

    for ( iNest = 0; iNest < MAX_NEST; iNest++ )
        rgObjNest[iNest] = NULL;

    for ( ; ; ) {
        char letter;
        char *word;

        letter = fread_letter( fp );
        if ( letter == '*' ) {
            fread_to_eol( fp );
            continue;
        }

        if ( letter != '#' ) {
            bugf("Load_room_obj: # not found in r%d.roo.",room->vnum);
            return -1;
        }

        word = fread_word( fp );
        if      ( !str_cmp( word, "OBJECT" ) ) fread_obj( room, fp, FALSE );
        else if ( !str_cmp( word, "O"      ) ) fread_obj( room, fp, FALSE );
        else if ( !str_cmp( word, "END"    ) ) break;
        else {
            bugf("Load_room_obj: bad section in r%d.roo, found %s.",
                 room->vnum,word);
            return -1;
        }
    }
    return 0;
}

void new_load_all_room_obj(void) {
    AREA_DATA *area;
    char *strsave;
    FILE *fp;

    for (area=area_first;area;area=area->next) {
        asprintf(&strsave,"%s/%s.roo",mud_data.room_dir,area->filename);
        if (!strncmp(".are",strsave+strlen(strsave)-8,4))
            strcpy(strsave+strlen(strsave)-8,".roo");

        if (access(strsave,F_OK)==0) { // does the file exist?
            if ((fp=fopen(strsave,"r"))!=NULL) {
                for ( ; ; ) {
                    char letter;
                    char *word;

                    letter = fread_letter( fp );
                    if ( letter == '*' ) {
                        fread_to_eol( fp );
                        continue;
                    }

                    if ( letter != '#' ) {
                        bugf("Load_room_obj: # not found in %s.",strsave);
                        free(strsave);
                        return;
                    }

                    word = fread_word( fp );
                    if        (!str_cmp(word,"ROOM"       )) {
                        ROOM_DATA *room;
                        int vnum=fread_number(fp);
                        room=get_room_index(vnum);

                        if (room==NULL) {
                            bugf("Not existing room[%d] found in rooms file (%s)",vnum,strsave);
                            break;
                        }

                        new_load_room_obj(room,fp);

                    } else if (!str_cmp(word,"AREA_VARS")) {
                        char varname[MSL];
                        char *buf;

                        snprintf(varname,sizeof(varname),"namespace eval %s { }",namespace_darea(area));
                        Tcl_EvalEx(interp,varname,-1,0);

                        while ((letter=fread_letter(fp))!='$') {
                            buf=fread_string_temp(fp);
                            snprintf(varname,sizeof(varname),"%s::%c%s",namespace_darea(area),letter,buf);
                            buf=fread_string_temp(fp);

                            if (Tcl_SetVar(interp,varname,buf,TCL_LEAVE_ERR_MSG)==NULL) {
                                Tcl_Obj *result;

                                result=Tcl_GetObjResult(interp);
                                bugf("%s",Tcl_GetString(result));
                            }

                        }

                    } else if (!str_cmp(word,"END_OF_AREA")) break;
                }
                fclose(fp);
            } else {
                bugf("Can't open rooms file %s. [%s]",strsave,strerror(errno));
            }
        } else {
            // area.roo doesn't exist. lest see if there are per-room files.
            int vnum;
            for (vnum=area->lvnum;vnum<=area->uvnum;vnum++) {
                ROOM_DATA *room;

                room=get_room_index(vnum);

                if (room && STR_IS_SET(room->strbit_room_flags,ROOM_SAVEOBJ))
                    load_room_obj(room);
            }

        }
        free(strsave);
    }
}

/*
 * Read in a char.
 */
void fread_char( CHAR_DATA *ch, FILE *fp )
{
    char buf[MAX_STRING_LENGTH];
    char *word;
    bool fMatch;
    int lastlogoff = current_time;
    int percent;

#ifdef THREADED_DNS
    word=ch->desc!=NULL?dns_gethostname(ch->desc):"nohost";
#else
    word=(ch->desc==NULL || IS_NULL_STRING(ch->desc->Host))?"nohost":
                                                            dns_gethostname(ch->desc);
#endif
    logf("[%d] Loading %s from %s.",
         ch->desc!=NULL?ch->desc->descriptor:-1,
         ch->name,
         word);

    for ( ; ; )
    {
        word   = feof( fp ) ? "End" : fread_word( fp );
        fMatch = FALSE;

        switch ( UPPER(word[0]) )
        {
        case '*':
            fMatch = TRUE;
            fread_to_eol( fp );
            break;

        case 'A':
            //	    KEY( "Act",		ch->act,		fread_flag( fp ) );
            //	    KEY( "AffectedBy",	ch->affected_by,	fread_flag( fp ) );
            //	    KEY( "EfBy",	ch->affected_by,	fread_flag( fp ) );
            //	    KEY( "Eff2",	ch->affected_by2,	fread_flag( fp ) );
            if ( !str_cmp( word, "Act" ) ) {
                if (ch->version<17) {
                    int i;
                    long l;

                    l=fread_flag( fp );
                    STR_ZERO_BIT(ch->strbit_act,MAX_FLAGS);
                    for (i=1;i<32;i++) {
                        if (l%2)
                            STR_SET_BIT(ch->strbit_act,i);
                        l>>=1;
                    }
                } else {
                    char *s;

                    s=fread_string_temp(fp);
                    hextostring(s,ch->strbit_act,strlen(s));
                }
                fMatch=TRUE;
                break;
            }
            if ( !str_cmp( word, "AfBy" ) ) {
                if (ch->version<11) {
                    int i;
                    long l;

                    l=fread_flag( fp );
                    STR_ZERO_BIT(ch->strbit_affected_by,MAX_FLAGS);
                    for (i=1;i<32;i++) {
                        if (l%2)
                            STR_SET_BIT(ch->strbit_affected_by,i);
                        l>>=1;
                    }
                } else {
                    char *s;

                    s=fread_string_temp(fp);
                    hextostring(s,ch->strbit_affected_by,strlen(s));
                }
                fMatch=TRUE;
                break;
            }
            if ( !str_cmp( word, "Aff2" ) ) {
                if (ch->version<11) {
                    long l;
                    int i;

                    l=fread_flag( fp );
                    STR_ZERO_BIT(ch->strbit_affected_by2,MAX_FLAGS);
                    for (i=1;i<32;i++) {
                        if (l%2)
                            STR_SET_BIT(ch->strbit_affected_by2,i);
                        l>>=1;
                    }
                } else {
                    char *s;

                    s=fread_string_temp(fp);
                    hextostring(s,ch->strbit_affected_by2,strlen(s));
                }
                fMatch=TRUE;
                break;
            }
            //	    KEY( "Alignment",	ch->alignment,		fread_number( fp ) );
            KEY( "Alig",	ch->alignment,		fread_number( fp ) );

            /*	    if (!str_cmp( word, "Alia")) {	// old aliases
        ALIAS_TYPE *alias;

        alias=new_alias();
        alias->alias=str_dup(fread_word(fp));
        alias->command=str_dup(fread_word(fp));
        alias->next=ch->pcdata->aliases;
        ch->pcdata->aliases=alias;
        fMatch = TRUE;
        break;
        }
*/
            if (!str_cmp( word, "Alias")) {	// new aliases
                ALIAS_TYPE *alias;

                alias=new_alias();
                alias->alias=str_dup(fread_word(fp));
                alias->command=fread_string(fp);
                alias->next=ch->pcdata->aliases;
                ch->pcdata->aliases=alias;
                fMatch = TRUE;
                break;
            }
#ifdef HAS_ALTS
            KEYS( "Alts",	ch->pcdata->alts,	fread_string( fp ) );
#endif

            /*	    if (!str_cmp( word, "AC") || !str_cmp(word,"Armor"))
        {
        fread_to_eol(fp);
        fMatch = TRUE;
        break;
        }
*/
            if (!str_cmp(word,"ACs"))
            {
                int i;

                for (i = 0; i < 4; i++)
                    ch->armor[i] = fread_number(fp);
                fMatch = TRUE;
                break;
            }

            if (!str_cmp(word, "AffD"))
            {
                EFFECT_DATA *pef;
                int sn;
                char s[100];

                pef = new_effect();

                strcpy(s,fread_word(fp));
                sn = skill_lookup(s);
                if (sn < 0) {
                    bugf("Fread_char: AffD: unknown skill (%s).",s);
                } else
                    pef->type = sn;

                pef->level	= fread_number( fp );
                pef->duration	= fread_number( fp );
                pef->modifier	= fread_number( fp );
                pef->location	= fread_number( fp );
                if (ch->version<12) {
                    int i;
                    long l;
                    l=fread_number( fp );
                    for (i=1;i<=32;i++) {
                        if (l%2) {
                            pef->bitvector=i;
                            break;
                        }
                        l>>=1;
                    }
                } else {
                    pef->bitvector	= fread_number( fp );
                }
                pef->next	= ch->affected;
                ch->affected	= pef;
                fMatch = TRUE;
                break;
            }

            if (!str_cmp(word, "Affc"))
            {
                EFFECT_DATA *pef;
                int sn;
                char s[100];

                pef = new_effect();

                strcpy(s,fread_word(fp));
                sn = skill_lookup(s);
                if (sn < 0) {
                    bugf("Fread_char: Affc: unknown skill (%s).",s);
                } else
                    pef->type = sn;

                pef->where  = fread_number(fp);
                pef->level      = fread_number( fp );
                pef->duration   = fread_number( fp );
                pef->modifier   = fread_number( fp );
                pef->location   = fread_number( fp );
                if (ch->version<12) {
                    int i;
                    long l;
                    l=fread_number( fp );
                    for (i=1;i<=32;i++) {
                        if (l%2) {
                            pef->bitvector=i;
                            break;
                        }
                        l>>=1;
                    }
                } else {
                    pef->bitvector	= fread_number( fp );
                }
                pef->next       = ch->affected;
                ch->affected    = pef;
                fMatch = TRUE;
                break;
            }

            if (!str_cmp(word, "Af2c"))
            {
                EFFECT_DATA *pef;
                int sn;
                char s[100];

                pef = new_effect();

                strcpy(s,fread_word(fp));
                sn = skill_lookup(s);
                if (sn < 0) {
                    bugf("Fread_char: Af2c: unknown skill (%s).",s);
                } else
                    pef->type = sn;

                pef->where  = fread_number(fp);
                pef->level      = fread_number( fp );
                pef->duration   = fread_number( fp );
                pef->modifier   = fread_number( fp );
                pef->location   = fread_number( fp );
                if (ch->version<12) {
                    int i;
                    long l;
                    l=fread_number( fp );
                    for (i=1;i<=32;i++) {
                        if (l%2) {
                            pef->bitvector=i;
                            break;
                        }
                        l>>=1;
                    }
                } else {
                    pef->bitvector	= fread_number( fp );
                }
                pef->arg1	= fread_number( fp );
                if (ch->version<18) fread_string( fp );
                pef->next       = ch->affected;
                ch->affected    = pef;
                fMatch = TRUE;
                break;
            }

            if ( !str_cmp( word, "AttrMod"  ) || !str_cmp(word,"AMod"))
            {
                int stat;
                for (stat = 0; stat < MAX_STATS; stat ++)
                    ch->mod_stat[stat] = fread_number(fp);
                fMatch = TRUE;
                break;
            }

            if ( !str_cmp( word, "AttrPerm" ) || !str_cmp(word,"Attr"))
            {
                int stat;

                for (stat = 0; stat < MAX_STATS; stat++)
                    ch->perm_stat[stat] = fread_number(fp);
                fMatch = TRUE;
                break;
            }
            break;

        case 'B':
            //	    KEY( "Bamfin",	ch->pcdata->bamfin,	fread_string( fp ) );
            //	    KEY( "Bamfout",	ch->pcdata->bamfout,	fread_string( fp ) );
            KEYS( "Bin",	ch->pcdata->bamfin,	fread_string( fp ) );
            KEYS( "Bout",	ch->pcdata->bamfout,	fread_string( fp ) );
            KEY( "Bank",	ch->pcdata->bank,	fread_number( fp ) );

            if (!str_cmp(word,"Been")) {
                char *s = fread_string_temp(fp);
                hextostring(s,ch->pcdata->beeninroom,strlen(s));
                fMatch = TRUE;
                break;
            }
            break;

        case 'C':
            //	    KEY( "Class",	ch->class,		fread_number( fp ) );
            KEY( "Cla",		ch->class,		fread_number( fp ) );

            /* Stop memory-leak in reading of the Clan */
            //	    KEY( "Clan",	ch->clan,	clan_lookup(fread_string(fp))->vnum);
            if ( !str_cmp( word, "Clan" ) ) {
                char *tmp = fread_string(fp);
                //		ch->clan_clan = clan_lookup(tmp);
                free_string(tmp);
                fMatch = TRUE;
                break;
            }

            if ( !str_cmp( word, "Condition" ) || !str_cmp(word,"Cond"))
            {
                ch->pcdata->condition[COND_DRUNK] = fread_number( fp );
                ch->pcdata->condition[COND_FULL] = fread_number( fp );
                ch->pcdata->condition[COND_THIRST] = fread_number( fp );
                fMatch = TRUE;
                break;
            }
            if (!str_cmp(word,"Cnd"))
            {
                ch->pcdata->condition[COND_DRUNK] = fread_number( fp );
                ch->pcdata->condition[COND_FULL] = fread_number( fp );
                ch->pcdata->condition[COND_THIRST] = fread_number( fp );
                ch->pcdata->condition[COND_HUNGER] = fread_number( fp );
                if (ch->version>7)
                    ch->pcdata->condition[COND_ADRENALINE] = fread_number( fp );
                fMatch = TRUE;
                break;
            }
            if (!str_cmp(word,"Comm")) {
                if (ch->version<13) {
                    long l;
                    int i;

                    l=fread_flag( fp );
                    STR_ZERO_BIT(ch->strbit_comm,MAX_FLAGS);
                    for (i=1;i<32;i++) {
                        if (l%2)
                            STR_SET_BIT(ch->strbit_comm,i);
                        l>>=1;
                    }
                } else {
                    char *s;

                    s=fread_string_temp(fp);
                    hextostring(s,ch->strbit_comm,strlen(s));
                }
                fMatch=TRUE;
                break;
            }

            break;

        case 'D':
            //	    KEY( "Damroll",	ch->damroll,		fread_number( fp ) );
            KEY( "Dam",		ch->damroll,		fread_number( fp ) );
            //	    KEY( "Description",	ch->description,	fread_string( fp ) );
            KEYS( "Desc",	ch->description,	fread_string( fp ) );
            break;

        case 'E':
            if ( !str_cmp( word, "End" ) )
            {
                /* adjust hp mana move up  -- here for speed's sake */
                percent = (current_time - lastlogoff) * 25 / ( 2 * 60 * 60);

                percent = UMIN(percent,100);

                if (percent > 0 && !IS_AFFECTED(ch,EFF_POISON)
                        &&  !IS_AFFECTED(ch,EFF_PLAGUE))
                {
                    ch->hit	+= (ch->max_hit - ch->hit) * percent / 100;
                    ch->mana    += (ch->max_mana - ch->mana) * percent / 100;
                    ch->move    += (ch->max_move - ch->move)* percent / 100;
                }

                {
                    // get rid of old raw-colours-property
                    bool bvalue=FALSE;
                    if (GetCharProperty(ch,PROPERTY_BOOL,"rawcolours",&bvalue)) {
                        logf("[-1] Removing raw-colours from %s",ch->name);
                        DeleteCharProperty(ch,PROPERTY_BOOL,"rawcolours");
                        if (bvalue)
                            STR_SET_BIT(ch->strbit_act,PLR_WANTS_RAW_COLOUR);
                    }
                }

                return;
            }
            KEY( "Exp",		ch->exp,		fread_number( fp ) );
            break;

        case 'G':
            KEY( "Gold",	ch->gold,		fread_number( fp ) );
            if ( !str_cmp( word, "Group" )  || !str_cmp(word,"Gr"))
            {
                int gn;
                char *temp;

                temp = fread_word( fp ) ;
                gn = group_lookup(temp);
                /* gn    = group_lookup( fread_word( fp ) ); */
                if ( gn < 0 )
                {
                    bugf("fread_char(): Group: unknown group '%s'. ",temp );
                } else
                    gn_add(ch,gn);
                fMatch = TRUE;
            }

            if ( !str_cmp( word, "Grnd" ) )
            {
                if (ch->version<10) {
                    char *s;
                    int i;

                    s=fread_string( fp );
                    STR_ZERO_BIT(ch->pcdata->strbitgrand,
                                 sizeof(ch->pcdata->strbitgrand));
                    for (i=1;i<108;i++) {
                        if (isstrbitset(s,i)) {
                            STR_SET_BIT(ch->pcdata->strbitgrand,i);
                        }
                    }
                    free_string(s);
                } else {
                    char *s;

                    s=fread_string_temp(fp);
                    hextostring(s,ch->pcdata->strbitgrand,strlen(s));
                }
                fMatch=TRUE;
                break;
            }
            break;

        case 'H':
            //	    KEY( "Hitroll",	ch->hitroll,		fread_number( fp ) );
            KEY( "Hit",		ch->hitroll,		fread_number( fp ) );

            if ( !str_cmp( word, "HpManaMove" ) || !str_cmp(word,"HMV"))
            {
                ch->hit		= fread_number( fp );
                ch->max_hit	= fread_number( fp );
                ch->mana	= fread_number( fp );
                ch->max_mana	= fread_number( fp );
                ch->move	= fread_number( fp );
                ch->max_move	= fread_number( fp );
                fMatch = TRUE;
                break;
            }

            if ( !str_cmp( word, "HpManaMovePerm" ) || !str_cmp(word,"HMVP"))
            {
                ch->pcdata->perm_hit	= fread_number( fp );
                ch->pcdata->perm_mana   = fread_number( fp );
                ch->pcdata->perm_move   = fread_number( fp );
                fMatch = TRUE;
                break;
            }

            break;

        case 'I':
            KEY( "Id",		ch->id,			fread_number( fp ) );
            //	    KEY( "InvisLevel",	ch->invis_level,	fread_number( fp ) );
            KEY( "Inco",	ch->incog_level,	fread_number( fp ) );
            KEY( "Invi",	ch->invis_level,	fread_number( fp ) );

            if (!str_cmp(word,"Immu")) {
                char *s;

                s=fread_string_temp(fp);
                hextostring(s,ch->strbit_imm_flags,strlen(s));
                fMatch=TRUE;
                break;
            }
#ifdef I3
            if (!str_cmp(word,"I3-channel")) {
                char *s;
                s=fread_string(fp);
                i3_listen_channel(ch->desc,s,TRUE);
                free_string(s);
                fMatch=TRUE;
                break;
            }
#endif
            // old IMC2 stuff, stays here for prevention of false negatives
            if (!str_cmp(word,"IMC")) {
                fread_flag(fp);fMatch=TRUE;break; }
            if (!str_cmp(word,"IMCAllow")) {
                fread_flag(fp);fMatch=TRUE;break; }
            if (!str_cmp(word,"IMCDeny")) {
                fread_flag(fp);fMatch=TRUE;break; }
            if (!str_cmp(word,"ICEListen")) {
                char *s=fread_string(fp);free_string(s);fMatch=TRUE;break; }
            //
            break;
        case 'J':
            if (!str_cmp(word,"Jobs")) {
                char *s = fread_string(fp);
                jobs_from_str(ch,s);
                free_string(s);
                fMatch = TRUE;
                break;
            }

        case 'K':
            if (!str_cmp(word,"KilledThatMob")) {
                char *s = fread_string_temp(fp);
                hextostring(s,ch->pcdata->killedthatmob,strlen(s));
                fMatch = TRUE;
                break;
            }
            KEY( "Killed",	ch->pcdata->killed,	fread_number( fp ) );

        case 'L':
            //	    KEY( "LastLevel",	ch->pcdata->last_level,	fread_number( fp ) );
            KEY( "LLev",	ch->pcdata->last_level,	fread_number( fp ) );
            //	    KEY( "Level",	ch->level,		fread_number( fp ) );
            KEY( "Lev",		ch->level,		fread_number( fp ) );
            KEY( "Levl",	ch->level,		fread_number( fp ) );
            KEY( "LogO",	lastlogoff,		fread_number( fp ) );
            //	    KEY( "LongDescr",	ch->long_descr,		fread_string( fp ) );
            KEYS( "LnD",	ch->long_descr,		fread_string( fp ) );
            break;

        case 'M':
            if (!str_cmp(word,"Mail")) {
                char *s;
                s=fread_string(fp);
                SetCharProperty(ch,PROPERTY_STRING,"email",s);
                free_string(s);
                fMatch=TRUE;
                break;
            }
            break;

        case 'N':
            KEYS( "Name",	ch->name,		fread_string( fp ) );
            if (!str_cmp(word,"Not"))
            {
                ch->pcdata->last_notes[NOTE_NOTE]	= fread_number(fp);
                ch->pcdata->last_notes[NOTE_IDEA]	= fread_number(fp);
                ch->pcdata->last_notes[NOTE_PENALTY]	= fread_number(fp);
                ch->pcdata->last_notes[NOTE_NEWS]	= fread_number(fp);
                ch->pcdata->last_notes[NOTE_CHANGES]	= fread_number(fp);
                if (ch->version>=23)
                    ch->pcdata->last_notes[NOTE_MCNOTE]	= fread_number(fp);
                fMatch = TRUE;
                break;
            }
            if (!str_cmp(word,"NotP"))
            {
                ch->pcdata->last_notes[NOTE_BUG]	= fread_number(fp);
                ch->pcdata->last_notes[NOTE_TYPO]	= fread_number(fp);
                fMatch = TRUE;
                break;
            }
            break;
        case 'O':
            if (!str_cmp(word,"On")) {
                int  vnum;
                long id;
                OBJ_DATA *obj, *best_match;

                vnum=fread_number(fp);
                id=fread_long(fp);
                best_match=NULL;

                obj=ch->in_room->contents;
                for (;obj;obj=obj->next_content) {
                    if (obj->pIndexData->vnum == vnum) {
                        best_match=obj;
                        if (obj->id == id) break;
                    }
                }

                switch (ch->position) {
                case POS_RESTING :
                    rest_obj(ch,best_match,TRUE);
                    break;
                case POS_SITTING :
                    sit_obj(ch,best_match,TRUE);
                    break;
                }

                fMatch = TRUE;
                break;
            }
            break;

        case 'P':
            //	    KEY( "Password",	ch->pcdata->pwd,	fread_string( fp ) );
            KEYS( "Pass",	ch->pcdata->pwd,	fread_string( fp ) );
            KEY("Picture",	ch->pcdata->pueblo_picture,fread_string( fp ) );
            //	    KEY( "Played",	ch->played,		fread_number( fp ) );
            KEY( "Plyd",	ch->played,		fread_number( fp ) );
            //	    KEY( "Points",	ch->pcdata->points,	fread_number( fp ) );
            KEY( "Pnts",	ch->pcdata->points,	fread_number( fp ) );
            //	    KEY( "Position",	ch->position,		fread_number( fp ) );
            KEY( "Pos",		ch->position,		fread_number( fp ) );
            //	    KEY( "Practice",	ch->practice,		fread_number( fp ) );
            KEY( "Prac",	ch->practice,		fread_number( fp ) );
            //	    KEYS( "Prompt",     ch->prompt,             fread_string( fp ) );
            KEYS( "Prom",	ch->prompt,		fread_string( fp ) );
            if (!str_cmp(word,"Prop")) {
                char *key;
                char *type;
                char *value;
                char old_key[MAX_STRING_LENGTH];
                char old_type[MAX_STRING_LENGTH];
                char old_value[MAX_STRING_LENGTH];
                int i;
                bool b;
                char c;
                long l;

                if (ch->version<19) {
                    char *temp;
                    temp=fread_string_eol(fp);
                    temp=one_argument(temp,old_key);
                    temp=one_argument(temp,old_type);
                    temp=one_argument(temp,old_value);
                    key=old_key;
                    type=old_type;
                    value=old_value;
                } else {
                    key=fread_string(fp);
                    type=fread_string(fp);
                    value=fread_string(fp);
                }

                switch (which_keyword(type,"int","bool","string",
                                      "char","long",NULL)) {
                case 1:
                    i=atoi(value);
                    SetCharProperty(ch,PROPERTY_INT,key,&i);
                    fMatch = TRUE;
                    break;
                case 2:
                    switch (which_keyword(value,"true","false",NULL)) {
                    case 1:
                        b=TRUE;
                        SetCharProperty(ch,PROPERTY_BOOL,key,&b);
                        fMatch = TRUE;
                        break;
                    case 2:
                        b=FALSE;
                        SetCharProperty(ch,PROPERTY_BOOL,key,&b);
                        fMatch = TRUE;
                        break;
                    default:
                        break;
                    }
                    break;
                case 3:
                    SetCharProperty(ch,PROPERTY_STRING,key,value);
                    fMatch = TRUE;
                    break;
                case 4:
                    c=value[0];
                    SetCharProperty(ch,PROPERTY_CHAR,key,&c);
                    fMatch = TRUE;
                    break;
                case 5:
                    l=atol(value);
                    SetCharProperty(ch,PROPERTY_LONG,key,&l);
                    fMatch = TRUE;
                    break;
                default:
                    break;
                }
                if (ch->version>=19) {
                    free_string(key);
                    free_string(type);
                    free_string(value);
                }

            }
            break;

        case 'Q':
            KEY( "QPnt",	ch->pcdata->questpoints, fread_number( fp ) );

            if (!str_cmp(word,"Quest")) {
                //		ch->pcdata->nextquest = fread_number( fp );
                //		ch->pcdata->countdown = fread_number( fp );
                //		ch->pcdata->questobj  = fread_number( fp );
                //		ch->pcdata->questmob  = fread_number( fp );
                //		ch->pcdata->questgiver= NULL;
                fread_number( fp );
                fread_number( fp );
                fread_number( fp );
                fread_number( fp );
                fMatch = TRUE;
                break;
            }

            break;
        case 'R':
            //	    KEY( "Race",        ch->race,
            //				race_lookup(fread_string( fp )) );
            if (!str_cmp( word, "Race" )) {
                char *tmp = fread_string(fp);
                ch->race = race_lookup(tmp);
                free_string(tmp);
                fMatch = TRUE;
                break;
            }

            KEY( "Reca",	ch->recall,		fread_number( fp ) );
            if (!str_cmp(word, "Rank")) {
                fread_number( fp );
                //		if (ch->version<15 && rankread==3) rankread=4;
                //		ch->rank=get_rank_by_vnum(rankread);
                fMatch = TRUE;
                break;
            }
            //	    KEY( "Rank",	ch->rank,		fread_number( fp ) );

            if (!str_cmp(word,"Resi")) {
                char *s;

                s=fread_string_temp(fp);
                hextostring(s,ch->strbit_res_flags,strlen(s));
                fMatch=TRUE;
                break;
            }

            if ( !str_cmp( word, "Room" ) )
            {
                ch->in_room = get_room_index( fread_number( fp ) );
                if ( ch->in_room == NULL )
                    ch->in_room = get_room_index( ROOM_VNUM_LIMBO );
                fMatch = TRUE;
                break;
            }

            break;
        case 'S':
            //	    KEY( "SavingThrow",	ch->saving_throw,	fread_number( fp ) );
            KEY( "Save",	ch->saving_throw,	fread_number( fp ) );
            //	    KEY( "Scro",	ch->pcdata->default_lines,
            //							fread_number( fp ) );
            KEY( "Sex",		ch->Sex,		fread_number( fp ) );
            //	    KEY( "ShortDescr",	ch->short_descr,	fread_string( fp ) );
            KEYS( "ShD",	ch->short_descr,	fread_string( fp ) );
            KEY( "Secu",	ch->pcdata->security,	fread_number( fp ) );
            //	    KEY( "Security",	ch->pcdata->security,	fread_number( fp ) );
            KEY( "Silv",        ch->silver,             fread_number( fp ) );

            if (!str_cmp(word,"Scro")) {
                if (ch->version<20)
                    ch->pcdata->default_lines=-fread_number( fp );
                else
                    ch->pcdata->default_lines=fread_number( fp );
                fMatch=TRUE;
                break;
            }
            if (!str_cmp(word,"SeenThatObject")) {
                char *s = fread_string_temp(fp);
                hextostring(s,ch->pcdata->seenthatobject,strlen(s));
                fMatch = TRUE;
                break;
            }

            if (!str_cmp(word,"SeenThatMob")) {
                char *s = fread_string_temp(fp);
                hextostring(s,ch->pcdata->seenthatmob,strlen(s));
                fMatch = TRUE;
                break;
            }

            if ( !str_cmp( word, "Skill" ) || !str_cmp(word,"Sk"))
            {
                int sn;
                int value,forgotten,counter;
                char *temp;

                value = fread_number( fp );
                // skill decay was introduced @21, but a full reset was called for @22
                if (ch->version>=21) {
                    forgotten = fread_number( fp );
                    counter = fread_number( fp );
                }
                if (ch->version<22) {
                    forgotten=0;
                    counter=0;
                }
                temp = fread_word( fp ) ;
                sn = skill_lookup(temp);
                /* sn    = skill_lookup( fread_word( fp ) ); */
                if ( sn < 0 )
                    bugf("Fread_char(): Skill: unknown skill (%s).",temp);
                else {
                    ch->pcdata->skill[sn].learned = value;
                    ch->pcdata->skill[sn].forgotten = forgotten;
                    ch->pcdata->skill[sn].counter = counter;
                }
                fMatch = TRUE;
            }

            break;

        case 'T':
            //          KEY( "TrueSex",     ch->pcdata->true_sex,  	fread_number( fp ) );
            KEY( "TSex",	ch->pcdata->true_sex,   fread_number( fp ) );
            KEY( "Trai",	ch->train,		fread_number( fp ) );
            //	    KEY( "Trust",	ch->trust,		fread_number( fp ) );
            KEY( "Tru",		ch->trust,		fread_number( fp ) );

            if ( !str_cmp( word, "Title" )  || !str_cmp( word, "Titl"))
            {
                ch->pcdata->title = fread_string( fp );
                if (ch->pcdata->title[0] != '.' && ch->pcdata->title[0] != ','
                        &&  ch->pcdata->title[0] != '!' && ch->pcdata->title[0] != '?')
                {
                    sprintf( buf, " %s", ch->pcdata->title );
                    free_string( ch->pcdata->title );
                    ch->pcdata->title = str_dup( buf );
                }
                fMatch = TRUE;
                break;
            }

            break;

        case 'U':
#ifdef HAS_ALTS
            KEYS( "UberAlt",	ch->pcdata->uberalt,	fread_string( fp ) );
#endif
            if (!str_cmp(word,"UsedThatObject")) {
                char *s = fread_string_temp(fp);
                hextostring(s,ch->pcdata->usedthatobject,strlen(s));
                fMatch = TRUE;
                break;
            }
            break;

        case 'V':
            //	    KEY( "Version",     ch->version,		fread_number ( fp ) );
            KEY( "Vers",	ch->version,		fread_number ( fp ) );
            if ( !str_cmp( word, "Vnum" ) )
            {
                ch->pIndexData = get_mob_index( fread_number( fp ) );
                fMatch = TRUE;
                break;
            }
            if (!str_cmp(word,"Vuln")) {
                char *s;

                s=fread_string_temp(fp);
                hextostring(s,ch->strbit_vuln_flags,strlen(s));
                fMatch=TRUE;
                break;
            }
            break;

        case 'W':
            //	    KEY( "Wimpy",	ch->wimpy,		fread_number( fp ) );
            KEY( "Wimp",	ch->wimpy,		fread_number( fp ) );
            if ( !str_cmp( word, "Wizn" ) ) {
                if (ch->version<9) {
                    long l;
                    int i;

                    l=fread_flag(fp);
                    // doe er nu nog iets mee
                    for (i=1;i<=32;i++) {
                        if (l%2)
                            STR_SET_BIT(ch->strbit_wiznet,i);
                        l>>=1;
                    }
                } else if (ch->version<10) {
                    char *s;
                    int i;

                    s=fread_string( fp );
                    STR_ZERO_BIT(ch->strbit_wiznet,sizeof(ch->strbit_wiznet));
                    for (i=1;i<40;i++) {
                        if (isstrbitset(s,i)) {
                            STR_SET_BIT(ch->strbit_wiznet,i);
                        }
                    }
                    free_string(s);
                } else {
                    char *s;

                    s=fread_string_temp( fp );
                    hextostring(s,ch->strbit_wiznet,strlen(s));
                }
                fMatch=TRUE;
            }
            KEY( "WhoN",	ch->pcdata->whoname,	fread_string( fp ) );
            break;
        }

        if ( !fMatch )
        {
            bugf("Fread_char: keyword not matched (%s)",word);
            fread_to_eol( fp );
        }

    }
}

/* load a pet from the forgotten reaches */
void fread_pet( CHAR_DATA *ch, FILE *fp )
{
    char *word;
    CHAR_DATA *pet;
    bool fMatch;
    int lastlogoff = current_time;
    int percent;

    /* first entry had BETTER be the vnum or we barf */
    word = feof(fp) ? "END" : fread_word(fp);
    if (!str_cmp(word,"Vnum"))
    {
        int vnum;

        vnum = fread_number(fp);
        if (get_mob_index(vnum) == NULL)
        {
            bugf("Fread_pet: bad vnum %d, mob does not exist.",vnum);
            pet = create_mobile(get_mob_index(MOB_VNUM_FIDO));
        }
        else
            pet = create_mobile(get_mob_index(vnum));
    }
    else
    {
        bugf("Fread_pet: no vnum in file, creating fido.");
        pet = create_mobile(get_mob_index(MOB_VNUM_FIDO));
    }

    for ( ; ; )
    {
        word 	= feof(fp) ? "END" : fread_word(fp);
        fMatch = FALSE;

        switch (UPPER(word[0]))
        {
        case '*':
            fMatch = TRUE;
            fread_to_eol(fp);
            break;

        case 'A':
            //	    KEY( "Act",		pet->act,		fread_flag(fp));
            //	    KEY( "EfBy",	pet->affected_by,	fread_flag(fp));
            //	    KEY( "Eff2",	pet->affected_by2,	fread_flag( fp ) );
            if ( !str_cmp( word, "Act" ) ) {
                if (ch->version<17) {
                    int i;
                    long l;

                    l=fread_flag( fp );
                    STR_ZERO_BIT(pet->strbit_act,MAX_FLAGS);
                    for (i=1;i<32;i++) {
                        if (l%2)
                            STR_SET_BIT(pet->strbit_act,i);
                        l>>=1;
                    }
                } else {
                    char *s;

                    s=fread_string_temp(fp);
                    hextostring(s,pet->strbit_act,strlen(s));
                }
                fMatch=TRUE;
                break;
            }
            if ( !str_cmp( word, "AfBy" ) ) {
                if (ch->version<11) {
                    long l;
                    int i;

                    l=fread_flag( fp );
                    STR_ZERO_BIT(pet->strbit_affected_by,MAX_FLAGS);
                    for (i=1;i<32;i++) {
                        if (l%2)
                            STR_SET_BIT(pet->strbit_affected_by,i);
                        l>>=1;
                    }
                } else {
                    char *s;

                    s=fread_string_temp(fp);
                    hextostring(s,pet->strbit_affected_by,strlen(s));
                }
                fMatch=TRUE;
                break;
            }
            if ( !str_cmp( word, "Aff2" ) ) {
                if (ch->version<11) {
                    long l;
                    int i;

                    l=fread_flag( fp );
                    STR_ZERO_BIT(pet->strbit_affected_by2,MAX_FLAGS);
                    for (i=1;i<32;i++) {
                        if (l%2)
                            STR_SET_BIT(pet->strbit_affected_by2,i);
                        l>>=1;
                    }
                } else {
                    char *s;

                    s=fread_string_temp(fp);
                    hextostring(s,pet->strbit_affected_by2,strlen(s));
                }
                fMatch=TRUE;
                break;
            }
            KEY( "Alig",	pet->alignment,		fread_number(fp));

            if (!str_cmp(word,"ACs"))
            {
                int i;

                for (i = 0; i < 4; i++)
                    pet->armor[i] = fread_number(fp);
                fMatch = TRUE;
                break;
            }

            if (!str_cmp(word,"AffD"))
            {
                EFFECT_DATA *pef;
                int sn;
                char s[100];

                pef = new_effect();

                strcpy(s,fread_word(fp));
                sn = skill_lookup(s);
                if (sn < 0) {
                    bugf("Fread_pet: AffD: unknown effect (%s).",s);
                } else
                    pef->type = sn;

                pef->level	= fread_number(fp);
                pef->duration	= fread_number(fp);
                pef->modifier	= fread_number(fp);
                pef->location	= fread_number(fp);
                if (ch->version<12) {
                    int i;
                    long l;
                    l=fread_number( fp );
                    for (i=1;i<=32;i++) {
                        if (l%2) {
                            pef->bitvector=i;
                            break;
                        }
                        l>>=1;
                    }
                } else {
                    pef->bitvector	= fread_number( fp );
                }
                pef->next	= pet->affected;
                pet->affected	= pef;
                fMatch		= TRUE;
                break;
            }

            if (!str_cmp(word,"Affc"))
            {
                EFFECT_DATA *pef;
                int sn;
                char s[100];

                pef = new_effect();

                strcpy(s,fread_word(fp));
                sn = skill_lookup(s);
                if (sn < 0) {
                    bugf("Fread_pet: Affc: unknown effect (%s).",s);
                } else
                    pef->type = sn;

                pef->where	= fread_number(fp);
                pef->level      = fread_number(fp);
                pef->duration   = fread_number(fp);
                pef->modifier   = fread_number(fp);
                pef->location   = fread_number(fp);
                if (ch->version<12) {
                    int i;
                    long l;
                    l=fread_number( fp );
                    for (i=1;i<=32;i++) {
                        if (l%2) {
                            pef->bitvector=i;
                            break;
                        }
                        l>>=1;
                    }
                } else
                    pef->bitvector	= fread_number( fp );
                pef->next       = pet->affected;
                pet->affected   = pef;
                fMatch          = TRUE;
                break;
            }

            if (!str_cmp(word, "Af2c"))
            {
                EFFECT_DATA *pef;
                int sn;
                char s[100];

                pef = new_effect();

                strcpy(s,fread_word(fp));
                sn = skill_lookup(s);
                if (sn < 0) {
                    bugf("Fread_pet: Af2c: unknown effect (%s).",s);
                } else
                    pef->type = sn;

                pef->where  = fread_number(fp);
                pef->level      = fread_number( fp );
                pef->duration   = fread_number( fp );
                pef->modifier   = fread_number( fp );
                pef->location   = fread_number( fp );
                if (ch->version<12) {
                    int i;
                    long l;
                    l=fread_number( fp );
                    for (i=1;i<=32;i++) {
                        if (l%2) {
                            pef->bitvector=i;
                            break;
                        }
                        l>>=1;
                    }
                } else
                    pef->bitvector	= fread_number( fp );
                pef->arg1	= fread_number( fp );
                if (ch->version<18) fread_string( fp );
                pef->next       = pet->affected;
                pet->affected    = pef;
                fMatch = TRUE;
                break;
            }

            if (!str_cmp(word,"AMod"))
            {
                int stat;

                for (stat = 0; stat < MAX_STATS; stat++)
                    pet->mod_stat[stat] = fread_number(fp);
                fMatch = TRUE;
                break;
            }

            if (!str_cmp(word,"Attr"))
            {
                int stat;

                for (stat = 0; stat < MAX_STATS; stat++)
                    pet->perm_stat[stat] = fread_number(fp);
                fMatch = TRUE;
                break;
            }
            break;

        case 'C':
            //	    /* Stop memory-leak in reading of the Clan */
            //	    KEY( "Clan",	pet->clan,	clan_lookup(fread_string(fp))->vnum);
            if ( !str_cmp( word, "Clan" ) ) {
                char *tmp = fread_string(fp);
                //		 pet->clan = clan_lookup(tmp)->vnum;
                free_string(tmp);
                fMatch = TRUE;
                break;
            }

            if (!str_cmp(word,"Comm")) {
                if (ch->version<13) {
                    long l;
                    int i;

                    l=fread_flag( fp );
                    STR_ZERO_BIT(pet->strbit_comm,MAX_FLAGS);
                    for (i=1;i<32;i++) {
                        if (l%2)
                            STR_SET_BIT(pet->strbit_comm,i);
                        l>>=1;
                    }
                } else {
                    char *s;

                    s=fread_string_temp(fp);
                    hextostring(s,pet->strbit_comm,strlen(s));
                }
                fMatch=TRUE;
                break;
            }

        case 'D':
            KEY( "Dam",	pet->damroll,		fread_number(fp));
            KEYS( "Desc",	pet->description,	fread_string(fp));
            break;

        case 'E':
            if (!str_cmp(word,"End"))
            {
                pet->leader = ch;
                pet->master = ch;
                ch->pet = pet;
                /* adjust hp mana move up  -- here for speed's sake */
                percent = (current_time - lastlogoff) * 25 / ( 2 * 60 * 60);

                if (percent > 0 && !IS_AFFECTED(pet,EFF_POISON)
                        &&  !IS_AFFECTED(pet,EFF_PLAGUE))
                {
                    percent = UMIN(percent,100);
                    pet->hit	+= (pet->max_hit - pet->hit) * percent / 100;
                    pet->mana   += (pet->max_mana - pet->mana) * percent / 100;
                    pet->move   += (pet->max_move - pet->move)* percent / 100;
                }
                return;
            }
            KEY( "Exp",	pet->exp,		fread_number(fp));
            break;

        case 'G':
            KEY( "Gold",	pet->gold,		fread_number(fp));
            break;

        case 'H':
            KEY( "Hit",	pet->hitroll,		fread_number(fp));

            if (!str_cmp(word,"HMV"))
            {
                pet->hit	= fread_number(fp);
                pet->max_hit	= fread_number(fp);
                pet->mana	= fread_number(fp);
                pet->max_mana	= fread_number(fp);
                pet->move	= fread_number(fp);
                pet->max_move	= fread_number(fp);
                fMatch = TRUE;
                break;
            }
            break;

        case 'L':
            KEY( "Levl",	pet->level,		fread_number(fp));
            KEYS( "LnD",	pet->long_descr,	fread_string(fp));
            KEY( "LogO",	lastlogoff,		fread_number(fp));
            break;

        case 'N':
            KEYS( "Name",	pet->name,		fread_string(fp));
            break;

        case 'P':
            KEY( "Pos",	pet->position,		fread_number(fp));
            if (!str_cmp(word,"Prop")) {
                char old_key[MAX_STRING_LENGTH];
                char old_type[MAX_STRING_LENGTH];
                char old_value[MAX_STRING_LENGTH];
                char *key;
                char *type;
                char *value;
                int i;
                bool b;
                char c;
                long l;

                if (ch->version<19) {
                    char *temp;
                    temp=fread_string_eol(fp);
                    temp=one_argument(temp,old_key);
                    temp=one_argument(temp,old_type);
                    temp=one_argument(temp,old_value);
                    key=old_key;
                    type=old_type;
                    value=old_value;
                } else {
                    key=fread_string(fp);
                    type=fread_string(fp);
                    value=fread_string(fp);
                }

                switch (which_keyword(type,"int","bool","string",
                                      "char","long",NULL)) {
                case 1:
                    i=atoi(value);
                    SetCharProperty(pet,PROPERTY_INT,key,&i);
                    fMatch = TRUE;
                    break;
                case 2:
                    switch (which_keyword(value,"true","false",NULL)) {
                    case 1:
                        b=TRUE;
                        SetCharProperty(pet,PROPERTY_BOOL,key,&b);
                        fMatch = TRUE;
                        break;
                    case 2:
                        b=FALSE;
                        SetCharProperty(pet,PROPERTY_BOOL,key,&b);
                        fMatch = TRUE;
                        break;
                    default:
                        break;
                    }
                    break;
                case 3:
                    SetCharProperty(pet,PROPERTY_STRING,key,value);
                    fMatch = TRUE;
                    break;
                case 4:
                    c=value[0];
                    SetCharProperty(pet,PROPERTY_CHAR,key,&c);
                    fMatch = TRUE;
                    break;
                case 5:
                    l=atol(value);
                    SetCharProperty(pet,PROPERTY_LONG,key,&l);
                    fMatch = TRUE;
                    break;
                default:
                    break;
                }
                if (ch->version>=19) {
                    free_string(key);
                    free_string(type);
                    free_string(value);
                }

            }
            break;

        case 'R':
            //    	    KEY( "Race",	pet->race, race_lookup(fread_string(fp)));
            if (!str_cmp( word, "Race" )) {
                char *tmp = fread_string(fp);
                pet->race = race_lookup(tmp);
                free_string(tmp);
                fMatch = TRUE;
                break;
            }
            break;

        case 'S' :
            KEY( "Save",	pet->saving_throw,	fread_number(fp));
            KEY( "Sex",		pet->Sex,		fread_number(fp));
            KEYS( "ShD",	pet->short_descr,	fread_string(fp));
            KEY( "Silv",        pet->silver,            fread_number( fp ) );
            break;
        }

        if ( !fMatch )
        {
            bugf("Fread_pet: no match for %s",word);
            fread_to_eol(fp);
        }
    }
}

/* If charload=TRUE then read objects for characters,
   else read objects for rooms. */
void fread_obj( void *ch_room, FILE *fp, bool charload )
{
    CHAR_DATA *ch=NULL;
    ROOM_INDEX_DATA *room=NULL;
    OBJ_DATA *obj;
    char *word;
    int iNest;
    bool fMatch;
    bool fNest;
    bool fVnum;
    bool first;
    bool make_new;    /* update object */

    if(charload) ch=(CHAR_DATA *)ch_room;
    else room=(ROOM_INDEX_DATA *)ch_room;

    fVnum = FALSE;
    obj = NULL;
    first = TRUE;  /* used to counter fp offset */
    make_new = FALSE;

    word   = feof( fp ) ? "End" : fread_word( fp );
    if (!str_cmp(word,"Vnum" ))
    {
        int vnum;
        first = FALSE;  /* fp will be in right place */

        vnum = fread_number( fp );
        if (  get_obj_index( vnum )  == NULL )
        {
            bugf( "Fread_obj: bad vnum %d.", vnum );
        }
        else
        {
            obj = create_object_data(get_obj_index(vnum),-1);
        }
    }

    if (obj == NULL)  /* either not found or old style */
    {
        obj = new_obj();
        obj->name		= str_dup( "" );
        obj->short_descr	= str_dup( "" );
        obj->description	= str_dup( "" );
    }

    fNest		= FALSE;
    fVnum		= TRUE;
    iNest		= 0;

    for ( ; ; )
    {
        if (first)
            first = FALSE;
        else
            word   = feof( fp ) ? "End" : fread_word( fp );
        fMatch = FALSE;

        switch ( UPPER(word[0]) )
        {
        case '*':
            fMatch = TRUE;
            fread_to_eol( fp );
            break;

        case 'A':
            if (!str_cmp(word,"AffD"))
            {
                EFFECT_DATA *pef;
                int sn;
                char s[100],t[100];

                pef = new_effect();

                strcpy(s,fread_word(fp));
                sn = skill_lookup(s);
                if (sn < 0) {
                    bugf(t,"Fread_obj: AffD: unknown skill (%s).",s);
                } else
                    pef->type = sn;

                pef->level	= fread_number( fp );
                pef->duration	= fread_number( fp );
                pef->modifier	= fread_number( fp );
                pef->location	= fread_number( fp );
                if (charload && ch->version<12) {
                    int i;
                    long l;
                    l=fread_number( fp );
                    for (i=1;i<=32;i++) {
                        if (l%2) {
                            pef->bitvector=i;
                            break;
                        }
                        l>>=1;
                    }
                } else
                    pef->bitvector	= fread_number( fp );
                pef->bitvector	= fread_number( fp );
                pef->next	= obj->affected;
                obj->affected	= pef;
                fMatch		= TRUE;
                break;
            }
            if (!str_cmp(word,"Affc"))
            {
                EFFECT_DATA *pef;
                int sn;
                char s[100];

                pef = new_effect();

                strcpy(s,fread_word(fp));
                sn = skill_lookup(s);
                if (sn < 0) {
                    bugf("Fread_obj: Affc: unknown skill (%s).",s);
                } else
                    pef->type = sn;

                pef->where	= fread_number( fp );
                pef->level      = fread_number( fp );
                pef->duration   = fread_number( fp );
                pef->modifier   = fread_number( fp );
                pef->location   = fread_number( fp );
                if (charload && ch->version<12) {
                    int i;
                    long l;
                    l=fread_number( fp );
                    for (i=1;i<=32;i++) {
                        if (l%2) {
                            pef->bitvector=i;
                            break;
                        }
                        l>>=1;
                    }
                } else
                    pef->bitvector	= fread_number( fp );
                pef->next       = obj->affected;
                obj->affected   = pef;
                fMatch          = TRUE;
                break;
            }
            if (!str_cmp(word, "Af2c"))
            {
                EFFECT_DATA *pef;
                int sn;
                char s[100];

                pef = new_effect();

                strcpy(s,fread_word(fp));
                sn = skill_lookup(s);
                if (sn < 0) {
                    bugf("Fread_obj: Af2c: unknown skill (%s).",s);
                } else
                    pef->type = sn;

                pef->where  = fread_number(fp);
                pef->level      = fread_number( fp );
                pef->duration   = fread_number( fp );
                pef->modifier   = fread_number( fp );
                pef->location   = fread_number( fp );
                if (charload && ch->version<12) {
                    int i;
                    long l;
                    l=fread_number( fp );
                    for (i=1;i<=32;i++) {
                        if (l%2) {
                            pef->bitvector=i;
                            break;
                        }
                        l>>=1;
                    }
                } else
                    pef->bitvector	= fread_number( fp );
                pef->arg1	= fread_number( fp );
                if (charload && (ch->version<18)) fread_string( fp );
                pef->next       = obj->affected;
                obj->affected    = pef;
                fMatch = TRUE;
                break;
            }
            break;

        case 'C':
            KEY( "Cond",	obj->condition,		fread_number( fp ) );
            KEY( "Cost",	obj->cost,		fread_number( fp ) );
            break;

        case 'D':
            KEYS( "Description",obj->description,	fread_string( fp ) );
            KEYS( "Desc",	obj->description,	fread_string( fp ) );
            break;

        case 'E':

            if ( !str_cmp( word, "Enchanted"))
            {
                obj->enchanted = TRUE;
                fMatch 	= TRUE;
                break;
            }

            //	    KEY( "ExtraFlags",	obj->extra_flags,	fread_number( fp ) );
            //	    KEY( "ExtF",	obj->extra_flags,	fread_number( fp ) );
            //	    KEY( "Ex2F",	obj->extra_flags2,	fread_number( fp ) );

            if ( !str_cmp( word, "ExtF" ) ) {
                if (charload && ch->version<11) {
                    long l;
                    int i;

                    l=fread_flag( fp );
                    STR_ZERO_BIT(obj->strbit_extra_flags,MAX_FLAGS);
                    for (i=1;i<32;i++) {
                        if (l%2)
                            STR_SET_BIT(obj->strbit_extra_flags,i);
                        l>>=1;
                    }
                } else {
                    char *s;

                    s=fread_string_temp(fp);
                    hextostring(s,obj->strbit_extra_flags,strlen(s));
                }
                fMatch=TRUE;
                break;
            }
            if ( !str_cmp( word, "Ex2F" ) ) {
                if (charload && ch->version<11) {
                    long l;
                    int i;

                    l=fread_flag( fp );
                    STR_ZERO_BIT(obj->strbit_extra_flags2,MAX_FLAGS);
                    for (i=1;i<1;i++) {
                        if (l%2)
                            STR_SET_BIT(obj->strbit_extra_flags2,i);
                        l>>=1;
                    }
                } else {
                    char *s;

                    s=fread_string_temp(fp);
                    hextostring(s,obj->strbit_extra_flags2,strlen(s));
                }
                fMatch=TRUE;
                break;
            }

            if ( !str_cmp( word, "ExtraDescr" ) || !str_cmp(word,"ExDe"))
            {
                EXTRA_DESCR_DATA *ed;

                ed = new_extra_descr();

                ed->keyword		= fread_string( fp );
                ed->description		= fread_string( fp );
                ed->next		= obj->extra_descr;
                obj->extra_descr	= ed;
                fMatch = TRUE;
            }

            if ( !str_cmp( word, "End" ) )
            {
                if ( !fNest || !fVnum || obj->pIndexData == NULL) {
                    bugf( "Fread_obj: incomplete object." );
                    if(fNest) rgObjNest[iNest] = NULL;
                    free_obj(obj);
                    return;
                } else {
                    if (make_new) {
                        int wear;

                        wear = obj->wear_loc;
                        extract_obj(obj);

                        obj = create_object_data(obj->pIndexData,0);
                        obj->wear_loc = wear;
                    }

                    //
                    // remove old and obsolete properties
                    //
                    {
                        bool bvalue=FALSE;
                        if (GetObjectProperty(obj,PROPERTY_BOOL,"no-sell",&bvalue)) {
                            logf("[-1] Removing no-sell from obj #%d (%s)",
                                 obj->pIndexData->vnum,obj->short_descr);
                            DeleteObjectProperty(obj,PROPERTY_BOOL,"no-sell");
                            if (bvalue)
                                STR_SET_BIT(obj->strbit_extra_flags,ITEM_NOSELL);
                        }
                    }
                    create_object_tcl(obj);

                    if ( iNest == 0 || rgObjNest[iNest-1] == NULL ) {
                        if(ch) obj_to_char( obj, ch );
                        else obj_to_room( obj, room );
                    } else {
                        // Check if the object which is used as a container
                        // really is a container. This is really usefull
                        // for saved rooms which had objects which are gone
                        // but still marked as stuff being nested in.
                        if (rgObjNest[iNest-1]->item_type==ITEM_CONTAINER ||
                                rgObjNest[iNest-1]->item_type==ITEM_CORPSE_NPC ||
                                rgObjNest[iNest-1]->item_type==ITEM_CORPSE_PC ) {
                            obj_to_obj( obj, rgObjNest[iNest-1] );
                        } else {
                            if (room==NULL) {
                                bugf( "Fread_obj: nested object (%s (#%d)) is not put in a container (%s (#%d)), removing it.",
                                      obj->short_descr,
                                      obj->pIndexData->vnum,
                                      rgObjNest[iNest-1]->short_descr,
                                        rgObjNest[iNest-1]->pIndexData->vnum
                                        );
                                free_obj(obj);
                                return;
                            } else {
                                bugf( "Fread_obj: nested object (%s (#%d)) is not put in a container (%s (#%d)), leaving it in room #%d (%s).",
                                      obj->short_descr,
                                      obj->pIndexData->vnum,
                                      rgObjNest[iNest-1]->short_descr,
                                        rgObjNest[iNest-1]->pIndexData->vnum,
                                        room->vnum,
                                        room->name
                                        );
                                obj_to_room( obj, room );
                            }
                        }
                    }
                    return;
                }
            }
            break;

        case 'I':
            KEY( "ItemType",	obj->item_type,		fread_number( fp ) );
            KEY( "Ityp",	obj->item_type,		fread_number( fp ) );
            break;

        case 'L':
            KEY( "Level",	obj->level,		fread_number( fp ) );
            KEY( "Lev",		obj->level,		fread_number( fp ) );
            break;

        case 'N':
            KEYS( "Name",	obj->name,		fread_string( fp ) );

            if ( !str_cmp( word, "Nest" ) )
            {
                iNest = fread_number( fp );
                if ( iNest < 0 || iNest >= MAX_NEST)
                {
                    bugf( "Fread_obj: bad nest %d.", iNest );
                }
                else
                {
                    rgObjNest[iNest] = obj;
                    fNest = TRUE;
                }
                fMatch = TRUE;
            }
            break;

        case 'O':
            KEYS( "Owner",	obj->owner_name,	fread_string( fp ) );
            KEY( "OwnerID",	obj->owner_id,		fread_number( fp ) );
            // I think this part is obsolte now
            if ( !str_cmp( word,"Oldstyle" ) )
            {
                if (obj->pIndexData != NULL)
                    make_new = TRUE;
                fMatch = TRUE;
            }
            break;

        case 'P':
            if (!str_cmp(word,"Prop")) {
                char *temp;
                char key[MAX_STRING_LENGTH];
                char type[MAX_STRING_LENGTH];
                char value[MAX_STRING_LENGTH];
                int i;
                bool b;
                char c;
                long l;

                // try to see the difference between '..' .. '..' and ..~ ..~ ..~
                c=fread_letter(fp);
                funread_letter(fp,c);
                if (c=='\'') {
                    temp=fread_string_eol(fp);
                    temp=one_argument(temp,key);
                    temp=one_argument(temp,type);
                    temp=one_argument(temp,value);
                } else {
                    temp=fread_string(fp); strncpy(key,temp,MAX_STRING_LENGTH);
                    temp=fread_string(fp); strncpy(type,temp,MAX_STRING_LENGTH);
                    temp=fread_string(fp); strncpy(value,temp,MAX_STRING_LENGTH);
                }

                switch (which_keyword(type,"int","bool","string",
                                      "char","long",NULL)) {
                case 1:
                    i=atoi(value);
                    SetObjectProperty(obj,PROPERTY_INT,key,&i);
                    fMatch = TRUE;
                    break;
                case 2:
                    switch (which_keyword(value,"true","false",NULL)) {
                    case 1:
                        b=TRUE;
                        SetObjectProperty(obj,PROPERTY_BOOL,key,&b);
                        fMatch = TRUE;
                        break;
                    case 2:
                        b=FALSE;
                        SetObjectProperty(obj,PROPERTY_BOOL,key,&b);
                        fMatch = TRUE;
                        break;
                    }
                    break;
                case 3:
                    SetObjectProperty(obj,PROPERTY_STRING,key,value);
                    fMatch = TRUE;
                    break;
                case 4:
                    c=value[0];
                    SetObjectProperty(obj,PROPERTY_CHAR,key,&c);
                    fMatch = TRUE;
                    break;
                case 5:
                    l=atol(value);
                    SetObjectProperty(obj,PROPERTY_LONG,key,&l);
                    fMatch = TRUE;
                    break;
                }

            }
            break;

        case 'S':
            KEYS( "ShortDescr",	obj->short_descr,	fread_string( fp ) );
            KEYS( "ShD",	obj->short_descr,	fread_string( fp ) );

            if ( !str_cmp( word, "Spell" ) )
            {
                int iValue;
                int sn;
                char s[100];

                iValue = fread_number( fp );
                strcpy(s,fread_word(fp));
                sn     = skill_lookup(s);
                if ( iValue < 0 || iValue > 4 )
                {
                    bugf( "Fread_obj: bad iValue %d.", iValue );
                }
                else if ( sn < 0 )
                {
                    bugf("Fread_obj: Spell: unknown skill (%s).",s);
                }
                else
                {
                    obj->value[iValue] = sn;
                }
                fMatch = TRUE;
                break;
            }

            break;

        case 'T':
            KEY( "Timer",	obj->timer,		fread_number( fp ) );
            KEY( "Time",	obj->timer,		fread_number( fp ) );
            break;

        case 'V':
            if ( !str_cmp( word, "Values" ) || !str_cmp(word,"Vals"))
            {
                obj->value[0]	= fread_number( fp );
                obj->value[1]	= fread_number( fp );
                obj->value[2]	= fread_number( fp );
                obj->value[3]	= fread_number( fp );
                if (obj->item_type == ITEM_WEAPON && obj->value[0] == 0)
                    obj->value[0] = obj->pIndexData->value[0];
                fMatch		= TRUE;
                break;
            }

            if ( !str_cmp( word, "Val" ) )
            {
                obj->value[0] 	= fread_number( fp );
                obj->value[1]	= fread_number( fp );
                obj->value[2] 	= fread_number( fp );
                obj->value[3]	= fread_number( fp );
                obj->value[4]	= fread_number( fp );
                fMatch = TRUE;
                break;
            }

            if ( !str_cmp( word, "Vnum" ) )
            {
                int vnum;

                vnum = fread_number( fp );
                if ( ( obj->pIndexData = get_obj_index( vnum ) ) == NULL )
                    bugf( "Fread_obj: bad vnum %d.", vnum );
                else
                    fVnum = TRUE;
                fMatch = TRUE;
                break;
            }
            break;

        case 'W':
            KEY( "WearFlags",	obj->wear_flags,	fread_number( fp ) );
            KEY( "WeaF",	obj->wear_flags,	fread_number( fp ) );
            KEY( "WearLoc",	obj->wear_loc,		fread_number( fp ) );
            KEY( "Wear",	obj->wear_loc,		fread_number( fp ) );
            KEY( "Weight",	obj->weight,		fread_number( fp ) );
            KEY( "Wt",		obj->weight,		fread_number( fp ) );
            break;

        }

        if ( !fMatch )
        {
            bugf( "Fread_obj: no match: %s", word );
            fread_to_eol( fp );
        }
    }
}

void fread_notebook(CHAR_DATA *ch, FILE *fp)
{
    NOTEBK_DATA *pnotebook;
    char *test;

    for ( ; ; ) {
        test = fread_word( fp );
        if (!str_cmp("End",test ))
            return;
        pnotebook = new_notebook();
        pnotebook->created = atol(test);
        pnotebook->modified = atol(fread_word(fp));
        pnotebook->flags=0;
        if (ch->version>13)
            pnotebook->flags=fread_flag( fp );
        pnotebook->subject = fread_string( fp );
        pnotebook->text = fread_string( fp );
        pnotebook->next = ch->pcdata->notebook;
        ch->pcdata->notebook = pnotebook;
    }
}

char *find_player(char *name) {
    /* Find the player whose name best matches <name>
    If more players match equally, return NULL
    If no matches where found, a copy of the original string is returned.
    remeber to free the result when done */

    char *best_match=NULL;
    int multiple_match=0;
    DIR *dp;
    struct dirent *de;

    if (!(dp=opendir(mud_data.player_dir))) {
        bugf("Can't access playerdir");
        return NULL;
    }

    while ((de=readdir(dp))) {
        if (de->d_name[0]=='.') continue;
        if (!str_cmp(name,de->d_name)) {
            // exact match!
            if (best_match) free_string(best_match);
            closedir(dp);
            return str_dup(name);
        } else if (!str_prefix(name,de->d_name)) {
            // prefix match
            if (best_match) {
                multiple_match=1;
            } else {
                best_match=str_dup(de->d_name);
            }
        }
    }
    closedir(dp);

    if (!best_match) return str_dup(name);
    if (!multiple_match) return best_match;
    free_string(best_match);
    return NULL;
}

bool getplayerinfo(CHAR_DATA *ch,char *name,PLAYER_INFO *pi) {
    char strsave[MAX_INPUT_LENGTH];
    char buf[MAX_STRING_LENGTH];
    char word[MAX_STRING_LENGTH];
    char *arg;
    char *s;
    FILE *fp;
    DESCRIPTOR_DATA *d;
    bool invis;

    pi->level=-1;
    pi->lastlogin=0;
    pi->email[0]=0;
    pi->yahoo[0]=0;
    pi->icq[0]=0;
    pi->msn[0]=0;
    pi->aim[0]=0;
    pi->xmpp[0]=0;
    pi->homepage[0]=0;
    pi->class = 0;
    pi->race = 0;
    pi->online=FALSE;
    pi->publish=FALSE;
    invis=FALSE;

    /* First check if player is on-line */
    for(d = descriptor_list; d != NULL; d = d->next) {
        if (d->connected != CON_PLAYING)
            continue;

        if (d->original) /* don't finger switched persons! */
            continue;

        if (!str_cmp(d->character->name,name)) {
            /* He is on-line extract some data */
            if(ch && !can_see(ch,d->character)) {
                invis=TRUE;
                break; /* Not visible? He's offline :) */
            }
            pi->level=d->character->level;
            pi->lastlogin=time(NULL);
            pi->idle=d->character->pcdata->idle;
            pi->class=d->character->class;
            pi->race=d->character->race;
            GetCharProperty(d->character,PROPERTY_BOOL,"publish",&pi->publish);
            GetCharProperty(d->character,PROPERTY_STRING,"email",pi->email);
            GetCharProperty(d->character,PROPERTY_STRING,"yahoo",pi->yahoo);
            GetCharProperty(d->character,PROPERTY_STRING,"icq",pi->icq);
            GetCharProperty(d->character,PROPERTY_STRING,"msn",pi->msn);
            GetCharProperty(d->character,PROPERTY_STRING,"aim",pi->aim);
            GetCharProperty(d->character,PROPERTY_STRING,"xmpp",pi->xmpp);
            GetCharProperty(d->character,PROPERTY_STRING,"homepage",pi->homepage);
            return TRUE;
        }
    }

    sprintf( strsave, "%s/%s", mud_data.player_dir, capitalize( name ) );
    if ( name[0]=='.' || ( fp = fopen( strsave, "r" ) ) == NULL )
        return FALSE;

    while(fgets(buf,sizeof(buf),fp)) {
        buf[strlen(buf)-1]=0;
        arg=one_argument( buf, word );

        if(pi->level==-1 && !str_cmp(word,"Levl"))
            pi->level=atoi(arg);
        else if(pi->lastlogin==0 && !str_cmp(word,"LogO")) {
            if (invis) {
                pi->lastlogin=d->character->logon;
            } else {
                pi->lastlogin=atoi(arg);
            }
        } else if(!str_cmp(word,"Mail")) {
            if((s=strchr(arg,'~'))) *s='\0';
            strncpy(pi->email,arg,sizeof(pi->email));
            pi->email[sizeof(pi->email)-1]=0;
            pi->email[strlen(pi->email)-1]=0;
        } else if (!str_cmp(word,"Race")) {
            if((s=strchr(arg,'~'))) *s='\0';
            pi->race = race_lookup(arg);
        } else if (!str_cmp(word,"Cla")) {
            pi->class=atoi(arg);
        } else if (!str_cmp(word,"Prop")) {
            arg=one_argument(arg,word);
            if (!str_cmp(word,"icq~")) {
                arg=one_argument(arg,word);
                strncpy(pi->icq,arg,sizeof(pi->icq));
                pi->icq[sizeof(pi->icq)-1]=0;
                pi->icq[strlen(pi->icq)-1]=0;
            } else if (!str_cmp(word,"aim~")) {
                arg=one_argument(arg,word);
                strncpy(pi->aim,arg,sizeof(pi->aim));
                pi->aim[sizeof(pi->aim)-1]=0;
                pi->aim[strlen(pi->aim)-1]=0;
            } else if (!str_cmp(word,"msn~")) {
                arg=one_argument(arg,word);
                strncpy(pi->msn,arg,sizeof(pi->msn));
                pi->msn[sizeof(pi->msn)-1]=0;
                pi->msn[strlen(pi->msn)-1]=0;
            } else if (!str_cmp(word,"yahoo~")) {
                arg=one_argument(arg,word);
                strncpy(pi->yahoo,arg,sizeof(pi->yahoo));
                pi->yahoo[sizeof(pi->yahoo)-1]=0;
                pi->yahoo[strlen(pi->yahoo)-1]=0;
            } else if (!str_cmp(word,"email~")) {
                arg=one_argument(arg,word);
                strncpy(pi->email,arg,sizeof(pi->email));
                pi->email[sizeof(pi->email)-1]=0;
                pi->email[strlen(pi->email)-1]=0;
            } else if (!str_cmp(word,"xmpp~")) {
                arg=one_argument(arg,word);
                strncpy(pi->xmpp,arg,sizeof(pi->xmpp));
                pi->xmpp[sizeof(pi->xmpp)-1]=0;
                pi->xmpp[strlen(pi->xmpp)-1]=0;
            } else if (!str_cmp(word,"publish~")) {
                arg=one_argument(arg,word);
                if (!strcmp(word,"true"))
                    pi->publish=TRUE;
                else
                    pi->publish=FALSE;
            } else if (!str_cmp(word,"homepage~")) {
                arg=one_argument(arg,word);
                strncpy(pi->homepage,arg,sizeof(pi->homepage));
                pi->homepage[sizeof(pi->homepage)-1]=0;
                pi->homepage[strlen(pi->homepage)-1]=0;
            }
        } else if (!str_cmp(word,"End")) {
            fclose(fp);
            return TRUE;
        }
    }

    fclose(fp);
    return FALSE;
}

int cmp_users(const void *a, const void *b) {
    return strncmp((const char *)a,(const char *)b,15);
}

void do_finger(CHAR_DATA *ch,char *argument)
{
    char	buf[MAX_STRING_LENGTH];
    DIR *	dp;
    struct dirent *de;
    int		i,number;
    PLAYER_INFO pi;
    char *	full_name;
    int		prefix_search;

    prefix_search=(argument[0]!='\0') && !is_number(argument) && ((full_name=find_player(argument))==NULL);

    if(argument[0]=='\0' || is_number(argument) || prefix_search) {
        BUFFER *buffer,*name_buf;
        struct stat sb;
        time_t maxage=time(NULL)-(500*24*60*60); /* 500 days */
        int    too_old=0;

        buffer = new_buf();
        name_buf = new_buf();

        number=atoi(argument);

        dp=opendir(mud_data.player_dir);
        i=0;
        buf[0]='\0';
        while((de=readdir(dp))!=NULL) {
            if(de->d_name[0]=='.')
                continue;
            if(number && IS_IMMORTAL(ch)
                    && getplayerinfo(ch,de->d_name,&pi)
                    && pi.level!=number)
                continue;

            if (prefix_search && str_prefix(argument,de->d_name))
                continue;

            sprintf(buf,"%s/%s",mud_data.player_dir,de->d_name);
            if (!stat(buf,&sb) && (sb.st_mtime<maxage)) {
                too_old++;
                continue;
            }

            sprintf(buf,"%-15s",de->d_name);
            add_buf(name_buf,buf);

            i++;
        }

        closedir(dp);

        if (too_old) {
            sprintf_to_char(ch,"%d character%s left out because of age.\n\r",too_old,too_old==1?" was":"s were");
        }

        if (!prefix_search) {
            send_to_char("Users in this mud:\n\r",ch);
        } else {
            send_to_char("Users in this mud starting with '",ch);
            send_to_char(argument,ch);
            send_to_char("':\n\r",ch);
        }

        if(!i) {
            if(number) {
                sprintf(buf,"No characters found that are level %d.\n\r",number);
                add_buf(buffer,buf);
            }
            else add_buf(buffer,"No characters found.\n\r");
        } else {
            int j;

            qsort(name_buf->string,i,15,cmp_users);

            for (j=0;j<i;j++) {
                strncpy(buf,name_buf->string+(j*15),15);
                buf[15]='\0';
                add_buf(buffer,buf);
                if (j%5==4) add_buf(buffer,"\n\r");
            }
            if (i%5!=0) add_buf(buffer,"\n\r");
        }

        page_to_char(buf_string(buffer),ch);
        free_buf(buffer);
        free_buf(name_buf);
        return;
    }

    if (getplayerinfo(ch,full_name,&pi)) {
        CLANMEMBER_TYPE *pClan;
        CLAN_INFO *plonerclan=get_clan_by_name("Loner");

        sprintf(buf,"Information on player: %s\n\r",capitalize( full_name ) );
        if ((pClan=get_clanmember_by_charname(full_name))!=NULL) {
            sprintf(buf+strlen(buf),
                    "Clan: %s %s\n\r",
                    pClan->clan_info->who_name,
                    pClan->clan_info==plonerclan?"":pClan->rank_info->rank_name);
        }
        if(pi.level!= -1 )
            sprintf(buf+strlen(buf),"[%3d %6s %s]\n\r",pi.level,pc_race_table[pi.race].who_name,class_table[pi.class].who_name);
                if(pi.lastlogin) {
                    int days;
                    int hours;
                    int minutes;
                    int secs;

                    secs=difftime(time(NULL),pi.lastlogin);
                    if (secs) {
                        sprintf(buf+strlen(buf),"Last logoff: %s\r",ctime(&pi.lastlogin));
                        strcat(buf,"\r");

                        days=secs/86400;secs%=86400;
                        hours=secs/3600;secs%=3600;
                        minutes=secs/60;secs%=60;

                        sprintf(buf+strlen(buf),
                                "Last on: %d days, %d hrs, %d min and %d sec ago.\n\r",
                                days,hours,minutes,secs);
                    } else {
                        secs=difftime(time(NULL),pi.idle);
                        days=secs/86400;secs%=86400;
                        hours=secs/3600;secs%=3600;
                        minutes=secs/60;secs%=60;

                        sprintf(buf+strlen(buf),
                                "Idle since: %d days, %d hrs, %d min and %d seconds.\n\r",
                                days,hours,minutes,secs);
                    }
                }
        if (pi.yahoo[0])
            sprintf(buf+strlen(buf),"Yahoo: %s\n\r",pi.yahoo);
        if (pi.icq[0])
            sprintf(buf+strlen(buf),"ICQ: %s\n\r",pi.icq);
        if (pi.msn[0])
            sprintf(buf+strlen(buf),"MSN: %s\n\r",pi.msn);
        if (pi.aim[0])
            sprintf(buf+strlen(buf),"AIM: %s\n\r",pi.aim);
        if (pi.xmpp[0])
            sprintf(buf+strlen(buf),"XMPP/Jabber: %s\n\r",pi.xmpp);
        if (pi.email[0]) {
            if (pi.email[0]=='@' && (IS_TRUSTED(ch,LEVEL_COUNCIL) || !str_cmp(ch->name,full_name))) {
                if (HAS_PUEBLO(ch)) {
                    sprintf(buf+strlen(buf),
                            "Configured email: </xch_mudtext><a href=\"mailto:%s\">"
                            "<tt>%s</tt></a><xch_mudtext>\n\r",pi.email,pi.email);
                    sprintf(buf+strlen(buf),
                            "Visible email: </xch_mudtext><a href=\"mailto:%s@fataldimensions.org\">"
                            "<tt>%s@fataldimensions.org</tt></a><xch_mudtext>\n\r",full_name,full_name);
                } else if (HAS_MXP(ch)) {
                    sprintf(buf+strlen(buf),
                            "Configured email: "MXP"<a href=\"mailto:%s\">"
                                                   "%s</a>\n\r",pi.email,pi.email);
                    sprintf(buf+strlen(buf),
                            "Visible email: "MXP"<a href=\"mailto:%s@fataldimensions.org\">"
                                                "%s@fataldimensions.org</a>\n\r",full_name,full_name);
                } else {
                    sprintf(buf+strlen(buf),"Configured email: %s\n\r",pi.email);
                    sprintf(buf+strlen(buf),"Visible email: %s@fataldimensions.org\n\r",full_name);
                }
            } else {
                if (pi.email[0]=='@') {
                    strcpy(pi.email,full_name);
                    strcat(pi.email,"@fataldimensions.org");
                }
                if (HAS_PUEBLO(ch))
                    sprintf(buf+strlen(buf),
                            "Email: </xch_mudtext><a href=\"mailto:%s\">"
                            "<tt>%s</tt></a><xch_mudtext>\n\r",pi.email,pi.email);
                else if (HAS_MXP(ch))
                    sprintf(buf+strlen(buf),
                            "Email: "MXP"<a href=\"mailto:%s\">"
                                        "%s</a>\n\r",pi.email,pi.email);
                else
                    sprintf(buf+strlen(buf),"Email: %s\n\r",pi.email);

            }
        }
        if (pi.homepage[0]) {
            if (HAS_PUEBLO(ch))
                sprintf(buf+strlen(buf),
                        "Homepage: </xch_mudtext><a href=\"%s\">"
                        "<tt>%s</tt></a><xch_mudtext>\n\r",pi.homepage,pi.homepage);
            else if (HAS_MXP(ch))
                sprintf(buf+strlen(buf),
                        "Homepage: "MXP"<a href=\"%s\">"
                                       "%s</a>\n\r",pi.homepage,pi.homepage);
            else
                sprintf(buf+strlen(buf),"Homepage: %s\n\r",pi.homepage);
        }
        send_to_char( buf, ch );
    } else {
        send_to_char("Player does not exist or no information found.\n\r",ch);
    }
    free_string(full_name);
}
