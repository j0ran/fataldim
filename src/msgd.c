#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <strings.h>
#include <netinet/in.h>
#include <sys/select.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/sendfile.h>
#include <time.h>

char *message_filename=NULL;
int  message_file_handle;
long post_sleep=5;
long process_lifetime=-1;
int  port=4000;

void receive(int client) {
    sendfile(client, message_file_handle, NULL, 0x7ffff000);
    sleep(post_sleep);
    close(client);
    exit(0);
}

void usage() {
    printf("msgd -f <message file> -l <lifetime> -s <sleep time> -p <port>\n"
           "\n"
           "message file      full path to the file that is displayed on\n"
           "                  incoming connections\n"
           "lifetime          time after which this app exits\n"
           "sleeptime         time between the end of messagefile and the\n"
           "                  connection being closed\n"
           "port              port number to listen on\n"
           "\n");
    exit(1);
}

int main(int argc, char *argv[]) {
    int ch;
    int lsnr4,lsnr6,i;
    struct sockaddr_in6 sa6;
    struct sockaddr_in  sa4;
    time_t deadline;
    struct timeval timeout;

    while ((ch = getopt(argc, argv, "f:l:p:s:")) != -1) {
        switch (ch) {
        case 'f': {
            int stat_res;
            struct stat stat_data;

            message_filename=optarg;
            stat_res=stat(message_filename,&stat_data);
            if (stat_res!=0)
                err(1,"while testing message file '%s'",message_filename);
            if ((stat_data.st_mode & S_IFMT) != S_IFREG )
                errx(1,"'%s' is not a regular file",message_filename);
            break;
        }
        case 'l': {
            char *endptr;

            process_lifetime=strtol(optarg,&endptr,10);
            if (*endptr!=0 ||
                    process_lifetime<-1)
                errx(1,"'%s' is not a valid lifetime. (>=0)",optarg);
            break;
        }
        case 'p': {
            char *endptr;
            long lport;

            lport=strtol(optarg,&endptr,10);

            if (*endptr!=0 ||
                    lport<1 ||
                    lport>65535)
                errx(1,"'%s' is not a valid port number. (>=0)",optarg);
            port=lport;
            break;
        }
        case 's': {
            char *endptr;

            post_sleep=strtol(optarg,&endptr,10);
            if (*endptr!=0 ||
                    post_sleep<0)
                errx(1,"'%s' is not a valid wait time. (>=0)",optarg);
            break;
        }
        case '?': usage();
        }
    }
    argc-=optind;
    argv+=optind;

    if (optind==0 || message_filename==NULL) usage();

    signal(SIGCHLD,SIG_IGN);

    message_file_handle=open(message_filename,O_RDONLY);
    if (message_file_handle<0)
        err(1,"While opening the message file");

    lsnr4=socket( PF_INET, SOCK_STREAM, 0 );
    if (lsnr4==-1) err(1,"While creating the ipv4 socket");

    i=1;
    if (setsockopt(lsnr4,SOL_SOCKET,SO_REUSEADDR,&i,sizeof(i)))
        err(1,"While enabling SO_REUSEADDR on the ipv4 socket");
    i=1;
    if (setsockopt(lsnr4,SOL_SOCKET,SO_REUSEPORT,&i,sizeof(i)))
        err(1,"While enabling SO_REUSEPORT on the ipv4 socket");

    bzero(&sa4,sizeof(sa4));
    sa4.sin_family = AF_INET;
    sa4.sin_port   = htons( port );

    if (bind(lsnr4,(struct sockaddr *)&sa4,sizeof(sa4)))
        err(1,"While binding the ipv4 socket");

    lsnr6=socket( PF_INET6, SOCK_STREAM, 0 );
    if (lsnr6==-1) err(1,"While creating the ipv6 socket");

    i=1;
    if (setsockopt(lsnr6,SOL_SOCKET,SO_REUSEADDR,&i,sizeof(i)))
        err(1,"While enabling SO_REUSEADDR on the ipv6 socket");
    i=1;
    if (setsockopt(lsnr6,SOL_SOCKET,SO_REUSEPORT,&i,sizeof(i)))
        err(1,"While enabling SO_REUSEPORT on the ipv6 socket");

    bzero(&sa6,sizeof(sa6));
    sa6.sin6_family = AF_INET6;
    sa6.sin6_port   = htons( port );

    if (bind(lsnr6,(struct sockaddr *)&sa6,sizeof(sa6)))
        err(1,"While binding the ipv4 socket");

    if (listen(lsnr4,3))
        err(1,"While listen on the ipv4 socket");
    if (listen(lsnr6,3))
        err(1,"While listen on the ipv6 socket");

    deadline=time(NULL)+process_lifetime;

    i=lsnr4;
    if (lsnr6>i) i=lsnr6;
    i++;

    timeout.tv_usec=0;
    while (process_lifetime==-1 || (timeout.tv_sec=deadline-time(NULL))>0) {
        fd_set sockets;

        FD_ZERO(&sockets);
        FD_SET(lsnr4,&sockets);
        FD_SET(lsnr6,&sockets);

        select(i,&sockets,NULL,NULL,(process_lifetime==-1)?NULL:&timeout);

        if (FD_ISSET(lsnr4, &sockets)) {
            int client;

            client=accept(lsnr4,NULL,NULL);
            if (fork()==0) {
                close(lsnr4);
                close(lsnr6);
                receive(client);
            }
            close(client);
        }
        if (FD_ISSET(lsnr6, &sockets)) {
            int client;

            client=accept(lsnr6,NULL,NULL);
            if (fork()==0) {
                close(lsnr4);
                close(lsnr6);
                receive(client);
            }
            close(client);
        }
    }

    close(lsnr4);
    close(lsnr6);

    return 0;
}

