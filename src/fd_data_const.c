/* $Id: fd_data_const.c,v 1.17 2006/08/25 09:43:19 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

#include "merc.h"
#include "interp.h"
#include "color.h"


NOTES_DATA	notes_table[]={
    {	NOTE_NOTE,	NULL,	28*24*60*60,	0,	NULL,
	"note",		"a note",	"note",		"notes"
    },
    {	NOTE_IDEA,	NULL,	28*24*60*60,	0,	NULL,
	"idea",		"an idea",	"idea",		"ideas"
    },
    {	NOTE_PENALTY,	NULL,	0,		LEVEL_IMMORTAL,	NULL,
	"penalty",	"a penalty",	"penalty",	"penalties"
    },
    {	NOTE_NEWS,	NULL,	0,		LEVEL_IMMORTAL,	NULL,
	"news",		"a news-article","news article","news articles"
    },
    {	NOTE_CHANGES,	NULL,	0,		LEVEL_COUNCIL,	NULL,
	"change",	"a change-message","change",	"changes"
    },
    {	NOTE_BUG,	NULL,	0,		0,	NULL,
	"bug",		"a bug",	"bug",		"bugs"
    },
    {	NOTE_TYPO,	NULL,	0,		0,	NULL,
	"typo",		"a tyop",	"tyop",		"typso"
    },
    {	NOTE_MCNOTE,	NULL,	28*24*60*60,	0,	"mortal-council council",
	"mcnote",	"a MC Note",	"McNote",	"McNotes"
    },
    {	0,		NULL }
};

const	CHANNEL_DATA	channel_table[]={
    {	"gossip",	"gossip",	"gossips",	"no-gossip",
	C_GOSSIP,	COMM_NOGOSSIP,	FALSE,	FALSE,	FALSE,	FALSE,
	FALSE,	FALSE,	0	},
    {	"music",	"MUSIC:",	"MUSIC:",	"no-music",
	C_MUSIC,	COMM_NOMUSIC,	FALSE,	FALSE,	FALSE,	FALSE,
	FALSE,	FALSE,	0	},
    {	"auction",	"auction",	"auctions",	"no-auction",
	C_AUCTION,	COMM_NOAUCTION,	FALSE,	FALSE,	FALSE,	FALSE,
	FALSE,	FALSE,	0	},
    {	"question",	"question",	"questions",	"no-question",
	C_QUESTION,	COMM_NOQUESTION,FALSE,	FALSE,	FALSE,	FALSE,
	FALSE,	FALSE,	0	},
    {	"answer",	"answer",	"answers",	"no-answer",
	C_ANSWER,	COMM_NOANSWER,	FALSE,	FALSE,	FALSE,	FALSE,
	FALSE,	FALSE,	0	},
    {	"quest",	"quest",	"quests",	"no-quest",
	C_QUEST,	COMM_NOQUEST,	FALSE,	FALSE,	FALSE,	FALSE,
	FALSE,	FALSE,	0	},
    {	"quote",	"quote",	"quotes",	"no-answer",
	C_QUOTE,	COMM_NOQUOTE,	FALSE,	FALSE,	FALSE,	FALSE,
	FALSE,	FALSE,	0	},
    {	"grat",		"grat",		"grats",	"no-grat",
	C_GRATS,	COMM_NOGRATS,	FALSE,	FALSE,	FALSE,	FALSE,
	FALSE,	FALSE,	0	},
    {	"pray",		"pray to your gods","prays",	"no-pray",
	C_PRAY,		COMM_NOPRAY,	FALSE,	FALSE,	FALSE,	FALSE,
	FALSE,	FALSE,	LEVEL_IMMORTAL},
    {	"shout",	"shout",	"shouts",	"no-shout",
	C_SHOUT,	COMM_NOSHOUT,	FALSE,	FALSE,	FALSE,	FALSE,
	FALSE,	TRUE,	0	},
    {	"yell",		"yell",		"yells",	"no-yell",
	C_YELL,		COMM_NOYELL,	TRUE,	FALSE,	FALSE,	FALSE,
	FALSE,	TRUE,	0	},
    {	"announce",	"announce",	"announced",	"no-announce",
	NULL,		0,		FALSE,	FALSE,	FALSE,	TRUE,
	FALSE,	FALSE,	0	},
    {	"grouptell",	"tell the group","tells the group",	NULL,
	C_GTELL,	0,		FALSE,	TRUE,	FALSE,	FALSE,
	FALSE,	FALSE,	0	},
    {	"herotalk",	"yell on herotalk",	"HEROTALK",	"no-herotalk",
	C_HEROTALK,	COMM_NOHEROTALK,FALSE,	FALSE,	FALSE,	FALSE,
	TRUE,	FALSE,	0	},
    {	"clantalk",	"clan",		"clans",	"no-clantalk",
	C_CLAN,		COMM_NOCLAN,	FALSE,	FALSE,	TRUE,	FALSE,
	FALSE,	FALSE,	0	},
    {	"immtalk",	NULL,		NULL,		NULL,
	C_IMMTALK,	COMM_NOIMMTALK,	FALSE,	FALSE,	FALSE,	TRUE,
	FALSE,	FALSE,	0	},
    {	NULL,		NULL,		NULL,		NULL,
	NULL,		0,		FALSE,	FALSE,	FALSE,	FALSE,
	FALSE,	FALSE,	0	}
};

/* weapon selection table */
const	struct	weapon_type	weapon_table	[]	=
{
   { "sword",	OBJ_VNUM_SCHOOL_SWORD,	WEAPON_SWORD,	&gsn_sword	},
   { "mace",	OBJ_VNUM_SCHOOL_MACE,	WEAPON_MACE,	&gsn_mace 	},
   { "dagger",	OBJ_VNUM_SCHOOL_DAGGER,	WEAPON_DAGGER,	&gsn_dagger	},
   { "axe",	OBJ_VNUM_SCHOOL_AXE,	WEAPON_AXE,	&gsn_axe	},
   { "staff",	OBJ_VNUM_SCHOOL_STAFF,	WEAPON_SPEAR,	&gsn_spear	},
   { "flail",	OBJ_VNUM_SCHOOL_FLAIL,	WEAPON_FLAIL,	&gsn_flail	},
   { "whip",	OBJ_VNUM_SCHOOL_WHIP,	WEAPON_WHIP,	&gsn_whip	},
   { "polearm",	OBJ_VNUM_SCHOOL_POLEARM,WEAPON_POLEARM,	&gsn_polearm	},
   { NULL,	0,			0,		NULL		}
};



/* wiznet table and prototype for future flag setting */
const   struct wiznet_type      wiznet_table    []              =
{
   {    "on",           WIZ_ON,         LEVEL_IMMORTAL },
   {    "prefix",	WIZ_PREFIX,	LEVEL_IMMORTAL },

   // please keep this list alphabeticly sorted!

   {	"asave",	WIZ_ASAVE,	MAX_LEVEL4	},
   {    "ban",		WIZ_BAN,        LEVEL_IMMORTAL	},
   {	"bugs",		WIZ_BUGS,	LEVEL_IMMORTAL	},
   {	"cmd-log",	WIZ_CMD_LOG,	LEVEL_IMMORTAL	},
   {    "deaths",       WIZ_DEATHS,     LEVEL_IMMORTAL	},
// {	"debugsummon",	WIZ_DEBUGSUMMON,LEVEL_IMMORTAL	},
   {    "donate",	WIZ_DONATE,	MAX_LEVEL5	},
   {    "flags",	WIZ_FLAGS,	MAX_LEVEL5	},
   {	"help",		WIZ_HELP,	LEVEL_IMMORTAL	},
#ifdef HAS_HTTP
   {	"http",		WIZ_HTTP,	MAX_LEVEL3	},
   {	"http-debug",	WIZ_HTTP_DEBUG,	LEVEL_COUNCIL	},
#endif
#ifdef I3
   {	"i3-errors",	WIZ_I3_ERRORS,	LEVEL_IMMORTAL	},
   {	"i3-info",	WIZ_I3_INFO,	LEVEL_IMMORTAL	},
   {	"i3-debug",	WIZ_I3_DEBUG,	LEVEL_IMMORTAL	},
#endif
   {	"levels",	WIZ_LEVELS,	LEVEL_IMMORTAL	},
   {    "links",        WIZ_LINKS,      MAX_LEVEL7	},
   {	"load",		WIZ_LOAD,	MAX_LEVEL2	},
   {    "logins",       WIZ_LOGINS,     LEVEL_IMMORTAL	},
   {	"plr-logs",	WIZ_PLR_LOGS,	MAX_LEVEL3	},
   {	"memory",	WIZ_MEMORY,	LEVEL_IMMORTAL	},
   {    "mob_deaths",	WIZ_MOBDEATHS,  MAX_LEVEL4	},
   {	"newbies",	WIZ_NEWBIE,	LEVEL_IMMORTAL	},
   {	"note",		WIZ_NOTE,	LEVEL_IMMORTAL	},
   {	"ool-detects",	WIZ_OOL_DETECTS,LEVEL_IMMORTAL	},
   {	"penalties",	WIZ_PENALTIES,	MAX_LEVEL5	},
#ifdef HAS_POP3
   {	"pop3",		WIZ_POP3,	MAX_LEVEL3	},
   {	"pop3-debug",	WIZ_POP3_DEBUG,	LEVEL_COUNCIL	},
#endif
   {	"questinfo",	WIZ_QUESTS,	LEVEL_IMMORTAL	},
   {	"rebootinfo",	WIZ_REBOOT,	LEVEL_IMMORTAL	},
   {    "resets",       WIZ_RESETS,     MAX_LEVEL4	},
   {    "reset-debug",	WIZ_RESET_DEBUG,LEVEL_IMMORTAL	},
   {	"restore",	WIZ_RESTORE,	MAX_LEVEL2	},
   {	"saccing",	WIZ_SACCING,	MAX_LEVEL5	},
// {	"secure",	WIZ_SECURE,	MAX_LEVEL1	},
   {    "sites",        WIZ_SITES,      MAX_LEVEL4	},
   {	"snoops",	WIZ_SNOOPS,	MAX_LEVEL2	},
   {	"spam",		WIZ_SPAM,	MAX_LEVEL5	},
   {	"switches",	WIZ_SWITCHES,	MAX_LEVEL2	},
   {    "telnet",	WIZ_TELNET,	LEVEL_IMMORTAL	},
   {    "telnet-debug",	WIZ_TELNET_DEBUG,	LEVEL_COUNCIL	},
   {    "ticks",	WIZ_TICKS,	LEVEL_IMMORTAL	},
   {	"tcl-debug",	WIZ_TCL_DEBUG,	LEVEL_IMMORTAL	},
   {	NULL,		0,		0		}
};

/* attack table  -- not very organized :( */
const 	struct attack_type	attack_table	[MAX_DAMAGE_MESSAGE]	=
{
    { 	"none",		"hit",		-1		},  /*  0 */
    {	"slice",	"slice", 	DAM_SLASH	},
    {   "stab",		"stab",		DAM_PIERCE	},
    {	"slash",	"slash",	DAM_SLASH	},
    {	"whip",		"whip",		DAM_SLASH	},
    {   "claw",		"claw",		DAM_SLASH	},  /*  5 */
    {	"blast",	"blast",	DAM_BASH	},
    {   "pound",	"pound",	DAM_BASH	},
    {	"crush",	"crush",	DAM_BASH	},
    {   "grep",		"grep",		DAM_SLASH	},
    {	"bite",		"bite",		DAM_PIERCE	},  /* 10 */
    {   "pierce",	"pierce",	DAM_PIERCE	},
    {   "suction",	"suction",	DAM_BASH	},
    {	"beating",	"beating",	DAM_BASH	},
    {   "digestion",	"digestion",	DAM_ACID	},
    {	"charge",	"charge",	DAM_BASH	},  /* 15 */
    { 	"slap",		"slap",		DAM_BASH	},
    {	"punch",	"punch",	DAM_BASH	},
    {	"wrath",	"wrath",	DAM_ENERGY	},
    {	"magic",	"magic",	DAM_ENERGY	},
    {   "divine",	"divine power",	DAM_HOLY	},  /* 20 */
    {	"cleave",	"cleave",	DAM_SLASH	},
    {	"scratch",	"scratch",	DAM_PIERCE	},
    {   "peck",		"peck",		DAM_PIERCE	},
    {   "peckb",	"peck",		DAM_BASH	},
    {   "chop",		"chop",		DAM_SLASH	},  /* 25 */
    {   "sting",	"sting",	DAM_PIERCE	},
    {   "smash",	 "smash",	DAM_BASH	},
    {   "shbite",	"shocking bite",DAM_LIGHTNING	},
    {	"flbite",	"flaming bite", DAM_FIRE	},
    {	"frbite",	"freezing bite", DAM_COLD	},  /* 30 */
    {	"acbite",	"acidic bite", 	DAM_ACID	},
    {	"chomp",	"chomp",	DAM_PIERCE	},
    {  	"drain",	"life drain",	DAM_NEGATIVE	},
    {   "thrust",	"thrust",	DAM_PIERCE	},
    {   "slime",	"slime",	DAM_ACID	},  /* 35 */
    {	"shock",	"shock",	DAM_LIGHTNING	},
    {   "thwack",	"thwack",	DAM_BASH	},
    {   "flame",	"flame",	DAM_FIRE	},
    {   "chill",	"chill",	DAM_COLD	},
    {   "smear",	"smear",	DAM_ACID	},  /* 40 */
    {   "crack",	"crack",	DAM_SOUND	},
    {   "song",		"song",		DAM_SOUND	},
    {   NULL,		NULL,		0		}
};

/* race table */
struct	race_type	race_table	[]		=
{
/*
    {
	name,		pc_race?,
	act bits,	eff_by bits	eff_by2,	off bits,
	imm,		res,		vuln,
	form,		parts
    },
*/
    {
	"unique",		FALSE,
	"",		"",	"",	"",	"",	"",	"",
	0,		0
    },


    {
	"human",		TRUE,
	"",		"",	"",	"",	"", 	"",	"",
	A|H|M|V,	A|B|C|D|E|F|G|H|I|J|K
    },

    {
	"elf",			TRUE,
	"",		"",	"",	"",	"",	"",	"",
	A|H|M|V,	A|B|C|D|E|F|G|H|I|J|K
    },

    {
	"dwarf",		TRUE,
	"",		"",	"",	"",	"",	"",	"",
	A|H|M|V,	A|B|C|D|E|F|G|H|I|J|K
    },

    {
	"giant",		TRUE,
	"",		"",	"",	"",	"",	"",	"",
	A|H|M|V,	A|B|C|D|E|F|G|H|I|J|K
    },

    {
	"halfling",		TRUE,
	"",		"",	"",	"",	"",	"",	"",
	A|H|M|V,	A|B|C|D|E|F|G|H|I|J|K
    },

    {
	"kender",		TRUE,
	"",		"",	"",	"",	"",	"",	"",
	A|H|M|V,	A|B|C|D|E|F|G|H|I|J|K
    },

    {
	"drow",			TRUE,
	"",		"",	"",	"",	"",	"",	"",
	A|H|M|V,	A|B|C|D|E|F|G|H|I|J|K
    },

    {
	"bat",			FALSE,
	"",		"",	"",	"",	"",	"",	"",
	A|G|V,		A|C|D|E|F|H|J|K|P
    },

    {
	"bear",			FALSE,
	"",		"",	"",	"",	"",	"",	"",
	A|G|V,		A|B|C|D|E|F|H|J|K|U|V
    },

    {
	"cat",			FALSE,
	"",		"",	"",	"",	"",	"",	"",
	A|G|V,		A|C|D|E|F|H|J|K|Q|U|V
    },

    {
	"centipede",		FALSE,
	"",		"",	"",	"",	"",	"",	"",
 	A|B|G|O,		A|C|K
    },

    {
	"dog",			FALSE,
	"",		"",	"",	"",	"",	"",	"",
	A|G|V,		A|C|D|E|F|H|J|K|U|V
    },

    {
	"doll",			FALSE,
	"",		"",	"",	"",	"",	"",	"",
	E|J|M|cc,	A|B|C|G|H|K
    },

    { 	"dragon", 		FALSE,
	"",		"",	"",	"",	"",	"",	"",
	A|H|Z,		A|C|D|E|F|G|H|I|J|K|P|Q|U|V|X
    },

    {
	"fido",			FALSE,
	"",		"",	"",	"",	"",	"",	"",
	A|B|G|V,	A|C|D|E|F|H|J|K|Q|V
    },

    {
	"fox",			FALSE,
	"",		"",	"",	"",	"",	"",	"",
	A|G|V,		A|C|D|E|F|H|J|K|Q|V
    },

    {
	"goblin",		FALSE,
	"",		"",	"",	"",	"",	"",	"",
	A|H|M|V,	A|B|C|D|E|F|G|H|I|J|K
    },

    {
	"hobgoblin",		FALSE,
	"",		"",	"",	"",	"",	"",	"",
	A|H|M|V,        A|B|C|D|E|F|G|H|I|J|K|Y
    },

    {
	"kobold",		FALSE,
	"",		"",	"",	"",	"",	"",	"",
	A|B|H|M|V,	A|B|C|D|E|F|G|H|I|J|K|Q
    },

    {
	"lizard",		FALSE,
	"",		"",	"",	"",	"",	"",	"",
	A|G|X|cc,	A|C|D|E|F|H|K|Q|V
    },

    {
	"modron",		FALSE,
	"",		"",	"",	"",	"",	"",	"",
	H,		A|B|C|G|H|J|K
    },

    {
	"orc",			FALSE,
	"",		"",	"",	"",	"",	"",	"",
	A|H|M|V,	A|B|C|D|E|F|G|H|I|J|K
    },

    {
	"pig",			FALSE,
	"",		"",	"",	"",	"",	"",	"",
	A|G|V,	 	A|C|D|E|F|H|J|K
    },

    {
	"rodent",		FALSE,
	"",		"",	"",	"",	"",	"",	"",
	A|G|V,		A|C|D|E|F|H|J|K
    },

    {
	"school monster",	FALSE,
	"",		"",	"",	"",	"",	"",	"",
	A|M|V,		A|B|C|D|E|F|H|J|K|Q|U
    },

    {
	"snake",		FALSE,
	"",		"",	"",	"",	"",	"",	"",
	A|G|X|Y|cc,	A|D|E|F|K|L|Q|V|X
    },

    {
	"song bird",		FALSE,
	"",		"",	"",	"",	"",	"",	"",
	A|G|W,		A|C|D|E|F|H|K|P
    },

    {
	"troll",		FALSE,
	"",		"",	"",	"",	"",	"",	"",
	A|B|H|M|V,		A|B|C|D|E|F|G|H|I|J|K|U|V
    },

    {
	"water fowl",		FALSE,
	"",		"",	"",	"",	"",	"",	"",
	A|G|W,		A|C|D|E|F|H|K|P
    },

    {
	"wolf",			FALSE,
	"",		"",	"",	"",	"",	"",	"",
	A|G|V,		A|C|D|E|F|J|K|Q|V
    },

    {
	"wyvern",		FALSE,
	"",		"",	"",	"",	"",	"",	"",
	A|B|G|Z,		A|C|D|E|F|H|J|K|Q|V|X
    },

    {
	"snail",		FALSE,
	"",		"",	"",	"",	"",	"",	"",
	A|G|X|Y|cc,	A|F|M
    },

    {
	NULL, FALSE, "", "", "", "", "", "", "", 0, 0
    }
};

void init_races(void) {
    // unique
    // human
    // elf
    STR_SET_BIT(race_table[ 2].strbit_eff, EFF_INFRARED);
    STR_SET_BIT(race_table[ 2].strbit_res, RES_CHARM);
    STR_SET_BIT(race_table[ 2].strbit_vuln,VULN_IRON);
    // dwarf
    STR_SET_BIT(race_table[ 3].strbit_eff, EFF_INFRARED);
    STR_SET_BIT(race_table[ 3].strbit_res, RES_POISON);
    STR_SET_BIT(race_table[ 3].strbit_res, RES_DISEASE);
    STR_SET_BIT(race_table[ 3].strbit_vuln,VULN_DROWNING);
    // giant
    STR_SET_BIT(race_table[ 4].strbit_res, RES_FIRE);
    STR_SET_BIT(race_table[ 4].strbit_res, RES_COLD);
    STR_SET_BIT(race_table[ 4].strbit_vuln,VULN_MENTAL);
    STR_SET_BIT(race_table[ 4].strbit_vuln,VULN_LIGHTNING);
    // halfling
    STR_SET_BIT(race_table[ 5].strbit_eff, EFF_SNEAK);
    STR_SET_BIT(race_table[ 5].strbit_res, RES_MAGIC);
    STR_SET_BIT(race_table[ 5].strbit_res, RES_POISON);
    // kender
    STR_SET_BIT(race_table[ 6].strbit_res, RES_MAGIC);
    STR_SET_BIT(race_table[ 6].strbit_res, RES_POISON);
    // drow
    STR_SET_BIT(race_table[ 7].strbit_eff, EFF_INFRARED);
    STR_SET_BIT(race_table[ 7].strbit_res, RES_MAGIC);
    STR_SET_BIT(race_table[ 7].strbit_vuln,VULN_SILVER);
    STR_SET_BIT(race_table[ 7].strbit_vuln,VULN_LIGHT);
    // bat
    STR_SET_BIT(race_table[ 8].strbit_eff, EFF_FLYING);
    STR_SET_BIT(race_table[ 8].strbit_eff, EFF_DARK_VISION);
    STR_SET_BIT(race_table[ 8].strbit_off, OFF_DODGE);
    STR_SET_BIT(race_table[ 8].strbit_off, OFF_FAST);
    STR_SET_BIT(race_table[ 8].strbit_vuln,VULN_LIGHT);
    // bear
    STR_SET_BIT(race_table[ 9].strbit_eff, EFF_SWIM);
    STR_SET_BIT(race_table[ 9].strbit_off, OFF_CRUSH);
    STR_SET_BIT(race_table[ 9].strbit_off, OFF_DISARM);
    STR_SET_BIT(race_table[ 9].strbit_off, OFF_BERSERK);
    STR_SET_BIT(race_table[ 9].strbit_res, RES_BASH);
    STR_SET_BIT(race_table[ 9].strbit_res, RES_COLD);
    // cat
    STR_SET_BIT(race_table[10].strbit_eff, EFF_SWIM);
    STR_SET_BIT(race_table[10].strbit_eff, EFF_DARK_VISION);
    STR_SET_BIT(race_table[10].strbit_off, OFF_FAST);
    STR_SET_BIT(race_table[10].strbit_off, OFF_DODGE);
    // centipede
    STR_SET_BIT(race_table[11].strbit_eff, EFF_DARK_VISION);
    STR_SET_BIT(race_table[11].strbit_res, RES_PIERCE);
    STR_SET_BIT(race_table[11].strbit_res, RES_COLD);
    STR_SET_BIT(race_table[11].strbit_vuln,VULN_BASH);
    // dog
    STR_SET_BIT(race_table[12].strbit_eff, EFF_SWIM);
    STR_SET_BIT(race_table[12].strbit_off, OFF_FAST);
    // doll
    STR_SET_BIT(race_table[13].strbit_imm, IMM_COLD);
    STR_SET_BIT(race_table[13].strbit_imm, IMM_POISON);
    STR_SET_BIT(race_table[13].strbit_imm, IMM_HOLY);
    STR_SET_BIT(race_table[13].strbit_imm, IMM_NEGATIVE);
    STR_SET_BIT(race_table[13].strbit_imm, IMM_MENTAL);
    STR_SET_BIT(race_table[13].strbit_imm, IMM_DISEASE);
    STR_SET_BIT(race_table[13].strbit_imm, IMM_DROWNING);
    STR_SET_BIT(race_table[13].strbit_res, RES_BASH);
    STR_SET_BIT(race_table[13].strbit_res, RES_LIGHT);
    STR_SET_BIT(race_table[13].strbit_vuln,VULN_SLASH);
    STR_SET_BIT(race_table[13].strbit_vuln,VULN_FIRE);
    STR_SET_BIT(race_table[13].strbit_vuln,VULN_ACID);
    STR_SET_BIT(race_table[13].strbit_vuln,VULN_LIGHTNING);
    STR_SET_BIT(race_table[13].strbit_vuln,VULN_ENERGY);
    // dragon
    STR_SET_BIT(race_table[14].strbit_eff, EFF_INFRARED);
    STR_SET_BIT(race_table[14].strbit_eff, EFF_FLYING);
    STR_SET_BIT(race_table[14].strbit_res, RES_FIRE);
    STR_SET_BIT(race_table[14].strbit_res, RES_BASH);
    STR_SET_BIT(race_table[14].strbit_res, RES_CHARM);
    STR_SET_BIT(race_table[14].strbit_vuln,VULN_PIERCE);
    STR_SET_BIT(race_table[14].strbit_vuln,VULN_COLD);
    // fido
    STR_SET_BIT(race_table[15].strbit_eff, EFF_SWIM);
    STR_SET_BIT(race_table[15].strbit_off, OFF_DODGE);
    STR_SET_BIT(race_table[15].strbit_off, ASSIST_RACE);
    STR_SET_BIT(race_table[15].strbit_vuln,VULN_MAGIC);
    // fox
    STR_SET_BIT(race_table[16].strbit_eff, EFF_SWIM);
    STR_SET_BIT(race_table[16].strbit_eff, EFF_DARK_VISION);
    STR_SET_BIT(race_table[16].strbit_off, OFF_FAST);
    STR_SET_BIT(race_table[15].strbit_off, OFF_DODGE);
    // goblin
    STR_SET_BIT(race_table[17].strbit_eff, EFF_INFRARED);
    STR_SET_BIT(race_table[17].strbit_res, RES_DISEASE);
    STR_SET_BIT(race_table[17].strbit_vuln,VULN_MAGIC);
    // hobgoblin
    STR_SET_BIT(race_table[18].strbit_eff, EFF_INFRARED);
    STR_SET_BIT(race_table[18].strbit_res, RES_DISEASE);
    STR_SET_BIT(race_table[18].strbit_res, RES_POISON);
    // kobold
    STR_SET_BIT(race_table[19].strbit_eff, EFF_INFRARED);
    STR_SET_BIT(race_table[19].strbit_res, RES_POISON);
    STR_SET_BIT(race_table[19].strbit_vuln,VULN_MAGIC);
    // lizard
    STR_SET_BIT(race_table[20].strbit_res, RES_POISON);
    STR_SET_BIT(race_table[20].strbit_vuln,VULN_COLD);
    // modron
    STR_SET_BIT(race_table[21].strbit_eff, EFF_INFRARED);
    STR_SET_BIT(race_table[21].strbit_off, ASSIST_RACE);
    STR_SET_BIT(race_table[21].strbit_off, ASSIST_ALIGN);
    STR_SET_BIT(race_table[21].strbit_imm, IMM_CHARM);
    STR_SET_BIT(race_table[21].strbit_imm, IMM_DISEASE);
    STR_SET_BIT(race_table[21].strbit_imm, IMM_MENTAL);
    STR_SET_BIT(race_table[21].strbit_imm, IMM_HOLY);
    STR_SET_BIT(race_table[21].strbit_imm, IMM_NEGATIVE);
    STR_SET_BIT(race_table[21].strbit_res, RES_FIRE);
    STR_SET_BIT(race_table[21].strbit_res, RES_COLD);
    STR_SET_BIT(race_table[21].strbit_res, RES_ACID);
    // orc
    STR_SET_BIT(race_table[22].strbit_eff, EFF_INFRARED);
    STR_SET_BIT(race_table[22].strbit_res, RES_DISEASE);
    STR_SET_BIT(race_table[22].strbit_vuln,VULN_LIGHT);
    // pig
    // rodent
    STR_SET_BIT(race_table[24].strbit_off, OFF_DODGE);
    STR_SET_BIT(race_table[24].strbit_off, OFF_FAST);
    // schoolmonster
    STR_SET_BIT(race_table[25].strbit_act, ACT_NOALIGN);
    STR_SET_BIT(race_table[25].strbit_imm, IMM_CHARM);
    STR_SET_BIT(race_table[25].strbit_imm, IMM_SUMMON);
    STR_SET_BIT(race_table[25].strbit_vuln,VULN_MAGIC);
    // snake
    STR_SET_BIT(race_table[26].strbit_eff, EFF_SWIM);
    STR_SET_BIT(race_table[26].strbit_res, RES_POISON);
    STR_SET_BIT(race_table[26].strbit_vuln,VULN_COLD);
    // songbird
    STR_SET_BIT(race_table[27].strbit_eff, EFF_FLYING);
    STR_SET_BIT(race_table[27].strbit_off, OFF_FAST);
    STR_SET_BIT(race_table[27].strbit_off, OFF_DODGE);
    // troll
    STR_SET_BIT(race_table[28].strbit_eff, EFF_REGENERATION);
    STR_SET_BIT(race_table[28].strbit_eff, EFF_INFRARED);
    STR_SET_BIT(race_table[28].strbit_eff, EFF_DETECT_HIDDEN);
    STR_SET_BIT(race_table[28].strbit_off, OFF_BERSERK);
    STR_SET_BIT(race_table[28].strbit_res, RES_CHARM);
    STR_SET_BIT(race_table[28].strbit_res, RES_BASH);
    STR_SET_BIT(race_table[28].strbit_vuln,VULN_FIRE);
    STR_SET_BIT(race_table[28].strbit_vuln,VULN_ACID);
    // water fowl
    STR_SET_BIT(race_table[29].strbit_eff, EFF_SWIM);
    STR_SET_BIT(race_table[29].strbit_eff, EFF_FLYING);
    STR_SET_BIT(race_table[29].strbit_res, RES_DROWNING);
    // wolf
    STR_SET_BIT(race_table[30].strbit_eff, EFF_SWIM);
    STR_SET_BIT(race_table[30].strbit_eff, EFF_DARK_VISION);
    STR_SET_BIT(race_table[30].strbit_off, OFF_FAST);
    STR_SET_BIT(race_table[30].strbit_off, OFF_DODGE);
    // wyvern
    STR_SET_BIT(race_table[31].strbit_eff, EFF_FLYING);
    STR_SET_BIT(race_table[31].strbit_eff, EFF_DETECT_INVIS);
    STR_SET_BIT(race_table[31].strbit_eff, EFF_DETECT_HIDDEN);
    STR_SET_BIT(race_table[31].strbit_off, OFF_BASH);
    STR_SET_BIT(race_table[31].strbit_off, OFF_FAST);
    STR_SET_BIT(race_table[31].strbit_off, OFF_DODGE);
    STR_SET_BIT(race_table[31].strbit_imm, IMM_POISON);
    STR_SET_BIT(race_table[31].strbit_vuln,VULN_LIGHT);
    // snail
    STR_SET_BIT(race_table[32].strbit_eff, EFF_SLOW);
    STR_SET_BIT(race_table[32].strbit_vuln,VULN_BASH);
    STR_SET_BIT(race_table[32].strbit_vuln,VULN_PIERCE);
    STR_SET_BIT(race_table[32].strbit_vuln,VULN_SLASH);
    STR_SET_BIT(race_table[32].strbit_vuln,VULN_FIRE);
    STR_SET_BIT(race_table[32].strbit_vuln,VULN_COLD);

}

const	struct	pc_race_type	pc_race_table	[]	=
{
    { "null race", "", 0, { 100, 100, 100, 100 },
    100,100,100,
    { "" }, { 13, 13, 13, 13, 13 }, { 18, 18, 18, 18, 18 }, 0
    },

/*
    {
	"race name", 	short name, 	points,	{ class multipliers },
	mana,hp,move mult, { bonus skills },
	{ base stats },		{ max stats },		size
    },
*/
    {
	"human",	"Human",	0,	{ 100, 100, 100, 100 },
	100, 100, 100,
	{ "" },
	{ 13, 13, 13, 13, 13 },	{ 18, 18, 18, 18, 18 },	SIZE_MEDIUM
    },

    {
	"elf",		" Elf ",	5,	{ 100, 125,  100, 120 },
	100, 100, 100,
	{ "sneak", "hide" },
	{ 12, 14, 13, 15, 11 },	{ 16, 20, 18, 21, 15 }, SIZE_SMALL
    },

    {
	"dwarf",	"Dwarf",	8,	{ 150, 100, 125, 100 },
	100, 100, 100,
	{ "berserk" },
	{ 14, 12, 14, 10, 15 },	{ 20, 16, 19, 14, 21 }, SIZE_MEDIUM
    },

    {
	"giant",	"Giant",	6,	{ 200, 150, 150, 105 },
	100, 100, 100,
	{ "bash", "fast healing" },
	{ 16, 11, 13, 11, 14 },	{ 22, 15, 18, 15, 20 }, SIZE_LARGE
    },

    {
	"halfling",	"Halfl",	5,	{ 200, 120, 105, 105 },
	100, 100, 100,
	{ "haggle", "hide" },
	{ 10, 13, 11, 15, 14 }, { 17, 18, 17, 20, 18 }, SIZE_SMALL
    },

    {
	"kender",	"Kendr",	7,	{ 200, 125, 100, 110 },
	100, 100, 100,
	{ "haggle", "hide", "sneak" },
	{ 10, 13, 11, 16, 14 }, { 16, 18, 17, 22, 18 }, SIZE_SMALL
    },

    {
	"drow",		"Drow ",	4,	{ 100, 125,  100, 120 },
	100, 100, 100,
	{ "sneak" },
	{ 13, 13, 12, 15, 12 },	{ 17, 19, 17, 21, 18 }, SIZE_MEDIUM
    }
};




/*
 * Class table.
 *
 *   if you change order here, make sure you also change the CLASS_* defines.
 */
const	struct	class_type	class_table	[MAX_CLASS]	=
{
    {
	"mage", "Mag",
	0, 40, -1, MAX_LEVEL, STAT_INT,
	20, 6,  6,  8, 9, 13, TRUE,
	"mage basics", "mage default",
	"dagger"
    },

    {
	"cleric", "Cle",
	0, 40, -1, MAX_LEVEL, STAT_WIS,
	20, 2,  7, 10, 9, 13, TRUE,
	"cleric basics", "cleric default",
	"staff"
    },

    {
	"thief", "Thi",
	0, 40, -1, MAX_LEVEL, STAT_DEX,
	20,  -4,  8, 13, 4, 6, FALSE,
	"thief basic", "thief default",
	"dagger"
    },

    {
	"warrior", "War",
	0, 40, -1, MAX_LEVEL, STAT_STR,
	20,  -10,  11, 15, 4, 6, FALSE,
	"warrior basics", "warrior default",
	"sword"
    }
};


/*
 * Titles.
 */
char *	const			title_table	[MAX_CLASS][MAX_LEVEL+1][2] =
{
    {
	{ "Man",			"Woman"				},

	{ "Apprentice of Magic",	"Apprentice of Magic"		},
	{ "Spell Student",		"Spell Student"			},
	{ "Scholar of Magic",		"Scholar of Magic"		},
	{ "Delver in Spells",		"Delveress in Spells"		},
	{ "Medium of Magic",		"Medium of Magic"		},

	{ "Scribe of Magic",		"Scribess of Magic"		},
	{ "Seer",			"Seeress"			},
	{ "Sage",			"Sage"				},
	{ "Illusionist",		"Illusionist"			},
	{ "Abjurer",			"Abjuress"			},

	{ "Invoker",			"Invoker"			},
	{ "Enchanter",			"Enchantress"			},
	{ "Conjurer",			"Conjuress"			},
	{ "Magician",			"Witch"				},
	{ "Creator",			"Creator"			},

	{ "Savant",			"Savant"			},
	{ "Magus",			"Craftess"			},
	{ "Wizard",			"Wizard"			},
	{ "Warlock",			"War Witch"			},
	{ "Sorcerer",			"Sorceress"			},

	{ "Elder Sorcerer",		"Elder Sorceress"		},
	{ "Grand Sorcerer",		"Grand Sorceress"		},
	{ "Great Sorcerer",		"Great Sorceress"		},
	{ "Golem Maker",		"Golem Maker"			},
	{ "Greater Golem Maker",	"Greater Golem Maker"		},

	{ "Maker of Stones",		"Maker of Stones",		},
	{ "Maker of Potions",		"Maker of Potions",		},
	{ "Maker of Scrolls",		"Maker of Scrolls",		},
	{ "Maker of Wands",		"Maker of Wands",		},
	{ "Maker of Staves",		"Maker of Staves",		},

	{ "Demon Summoner",		"Demon Summoner"		},
	{ "Greater Demon Summoner",	"Greater Demon Summoner"	},
	{ "Dragon Charmer",		"Dragon Charmer"		},
	{ "Greater Dragon Charmer",	"Greater Dragon Charmer"	},
	{ "Master of all Magic",	"Master of all Magic"		},

 	{ "Master Mage",		"Master Mage"			},
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },

        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },

        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },

        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },

        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },

        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },

        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },

        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },

        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },

        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },

        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },
        { "Master Mage",                "Master Mage"                   },

	{ "Mage Hero",			"Mage Heroine"			},
	{ "Avatar of Magic",		"Avatar of Magic"		},
	{ "Angel of Magic",		"Angel of Magic"		},
	{ "Demigod of Magic",		"Demigoddess of Magic"		},
	{ "Immortal of Magic",		"Immortal of Magic"		},
	{ "God of Magic",		"Goddess of Magic"		},
	{ "Deity of Magic",		"Deity of Magic"		},
	{ "Supremity of Magic",		"Supremity of Magic"		},
	{ "Creator",			"Creator"			},
	{ "Implementor",		"Implementress"			}
    },

    {
	{ "Man",			"Woman"				},

	{ "Believer",			"Believer"			},
	{ "Attendant",			"Attendant"			},
	{ "Acolyte",			"Acolyte"			},
	{ "Novice",			"Novice"			},
	{ "Missionary",			"Missionary"			},

	{ "Adept",			"Adept"				},
	{ "Deacon",			"Deaconess"			},
	{ "Vicar",			"Vicaress"			},
	{ "Priest",			"Priestess"			},
	{ "Minister",			"Lady Minister"			},

	{ "Canon",			"Canon"				},
	{ "Levite",			"Levitess"			},
	{ "Curate",			"Curess"			},
	{ "Monk",			"Nun"				},
	{ "Healer",			"Healess"			},

	{ "Chaplain",			"Chaplain"			},
	{ "Expositor",			"Expositress"			},
	{ "Bishop",			"Bishop"			},
	{ "Arch Bishop",		"Arch Lady of the Church"	},
	{ "Patriarch",			"Matriarch"			},

	{ "Elder Patriarch",		"Elder Matriarch"		},
	{ "Grand Patriarch",		"Grand Matriarch"		},
	{ "Great Patriarch",		"Great Matriarch"		},
	{ "Demon Killer",		"Demon Killer"			},
	{ "Greater Demon Killer",	"Greater Demon Killer"		},

	{ "Cardinal of the Sea",	"Cardinal of the Sea"		},
	{ "Cardinal of the Earth",	"Cardinal of the Earth"		},
	{ "Cardinal of the Air",	"Cardinal of the Air"		},
	{ "Cardinal of the Ether",	"Cardinal of the Ether"		},
	{ "Cardinal of the Heavens",	"Cardinal of the Heavens"	},

	{ "Avatar of an Immortal",	"Avatar of an Immortal"		},
	{ "Avatar of a Deity",		"Avatar of a Deity"		},
	{ "Avatar of a Supremity",	"Avatar of a Supremity"		},
	{ "Avatar of an Implementor",	"Avatar of an Implementor"	},
	{ "Master of all Divinity",	"Mistress of all Divinity"	},

	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},

	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},

	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},

	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},

	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},

	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},

	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},

	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},

	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},

	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},

	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},
	{ "Master Cleric",		"Master Cleric"			},

	{ "Holy Hero",			"Holy Heroine"			},
	{ "Holy Avatar",		"Holy Avatar"			},
	{ "Angel",			"Angel"				},
	{ "Demigod",			"Demigoddess",			},
	{ "Immortal",			"Immortal"			},
	{ "God",			"Goddess"			},
	{ "Deity",			"Deity"				},
	{ "Supreme Master",		"Supreme Mistress"		},
        { "Creator",                    "Creator"                       },
	{ "Implementor",		"Implementress"			}
    },

    {
	{ "Man",			"Woman"				},

	{ "Pilferer",			"Pilferess"			},
	{ "Footpad",			"Footpad"			},
	{ "Filcher",			"Filcheress"			},
	{ "Pick-Pocket",		"Pick-Pocket"			},
	{ "Sneak",			"Sneak"				},

	{ "Pincher",			"Pincheress"			},
	{ "Cut-Purse",			"Cut-Purse"			},
	{ "Snatcher",			"Snatcheress"			},
	{ "Sharper",			"Sharpress"			},
	{ "Rogue",			"Rogue"				},

	{ "Robber",			"Robber"			},
	{ "Magsman",			"Magswoman"			},
	{ "Highwayman",			"Highwaywoman"			},
	{ "Burglar",			"Burglaress"			},
	{ "Thief",			"Thief"				},

	{ "Knifer",			"Knifer"			},
	{ "Quick-Blade",		"Quick-Blade"			},
	{ "Killer",			"Murderess"			},
	{ "Brigand",			"Brigand"			},
	{ "Cut-Throat",			"Cut-Throat"			},

	{ "Spy",			"Spy"				},
	{ "Grand Spy",			"Grand Spy"			},
	{ "Master Spy",			"Master Spy"			},
	{ "Assassin",			"Assassin"			},
	{ "Greater Assassin",		"Greater Assassin"		},

	{ "Master of Vision",		"Mistress of Vision"		},
	{ "Master of Hearing",		"Mistress of Hearing"		},
	{ "Master of Smell",		"Mistress of Smell"		},
	{ "Master of Taste",		"Mistress of Taste"		},
	{ "Master of Touch",		"Mistress of Touch"		},

	{ "Crime Lord",			"Crime Mistress"		},
	{ "Infamous Crime Lord",	"Infamous Crime Mistress"	},
	{ "Greater Crime Lord",		"Greater Crime Mistress"	},
	{ "Master Crime Lord",		"Master Crime Mistress"		},
	{ "Godfather",			"Godmother"			},

        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },

        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },

        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },

       { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },

       { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },

       { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },

       { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },

       { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },

       { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },

       { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },

       { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },
        { "Master Thief",               "Master Thief"                  },

	{ "Assassin Hero",		"Assassin Heroine"		},
	{ "Avatar of Death",		"Avatar of Death",		},
	{ "Angel of Death",		"Angel of Death"		},
	{ "Demigod of Assassins",	"Demigoddess of Assassins"	},
	{ "Immortal Assasin",		"Immortal Assassin"		},
	{ "God of Assassins",		"God of Assassins",		},
	{ "Deity of Assassins",		"Deity of Assassins"		},
	{ "Supreme Master",		"Supreme Mistress"		},
        { "Creator",                    "Creator"                       },
	{ "Implementor",		"Implementress"			}
    },

    {
	{ "Man",			"Woman"				},

	{ "Swordpupil",			"Swordpupil"			},
	{ "Recruit",			"Recruit"			},
	{ "Sentry",			"Sentress"			},
	{ "Fighter",			"Fighter"			},
	{ "Soldier",			"Soldier"			},

	{ "Warrior",			"Warrior"			},
	{ "Veteran",			"Veteran"			},
	{ "Swordsman",			"Swordswoman"			},
	{ "Fencer",			"Fenceress"			},
	{ "Combatant",			"Combatess"			},

	{ "Hero",			"Heroine"			},
	{ "Myrmidon",			"Myrmidon"			},
	{ "Swashbuckler",		"Swashbuckleress"		},
	{ "Mercenary",			"Mercenaress"			},
	{ "Swordmaster",		"Swordmistress"			},

	{ "Lieutenant",			"Lieutenant"			},
	{ "Champion",			"Lady Champion"			},
	{ "Dragoon",			"Lady Dragoon"			},
	{ "Cavalier",			"Lady Cavalier"			},
	{ "Knight",			"Lady Knight"			},

	{ "Grand Knight",		"Grand Knight"			},
	{ "Master Knight",		"Master Knight"			},
	{ "Paladin",			"Paladin"			},
	{ "Grand Paladin",		"Grand Paladin"			},
	{ "Demon Slayer",		"Demon Slayer"			},

	{ "Greater Demon Slayer",	"Greater Demon Slayer"		},
	{ "Dragon Slayer",		"Dragon Slayer"			},
	{ "Greater Dragon Slayer",	"Greater Dragon Slayer"		},
	{ "Underlord",			"Underlord"			},
	{ "Overlord",			"Overlord"			},

	{ "Baron of Thunder",		"Baroness of Thunder"		},
	{ "Baron of Storms",		"Baroness of Storms"		},
	{ "Baron of Tornadoes",		"Baroness of Tornadoes"		},
	{ "Baron of Hurricanes",	"Baroness of Hurricanes"	},
	{ "Baron of Meteors",		"Baroness of Meteors"		},

	{ "Master Warrior",		"Master Warrior"		},
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },

        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },

        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
       { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },

       { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },

       { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },

       { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },

       { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },

       { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },

       { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },

       { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },
        { "Master Warrior",             "Master Warrior"                },


	{ "Knight Hero",		"Knight Heroine"		},
	{ "Avatar of War",		"Avatar of War"			},
	{ "Angel of War",		"Angel of War"			},
	{ "Demigod of War",		"Demigoddess of War"		},
	{ "Immortal Warlord",		"Immortal Warlord"		},
	{ "God of War",			"God of War"			},
	{ "Deity of War",		"Deity of War"			},
	{ "Supreme Master of War",	"Supreme Mistress of War"	},
        { "Creator",                    "Creator"                       },
	{ "Implementor",		"Implementress"			}
    }
};



/*
 * Attribute bonus tables.
 */
const	struct	str_app_type	str_app		[26]		=
{
    { -5, -4,   0,  0 },  /* 0  */
    { -5, -4,   3,  1 },  /* 1  */
    { -3, -2,   3,  2 },
    { -3, -1,  10,  3 },  /* 3  */
    { -2, -1,  25,  4 },
    { -2, -1,  55,  5 },  /* 5  */
    { -1,  0,  80,  6 },
    { -1,  0,  90,  7 },
    {  0,  0, 100,  8 },
    {  0,  0, 100,  9 },
    {  0,  0, 115, 10 }, /* 10  */
    {  0,  0, 115, 11 },
    {  0,  0, 130, 12 },
    {  0,  0, 130, 13 }, /* 13  */
    {  0,  1, 140, 14 },
    {  1,  1, 150, 15 }, /* 15  */
    {  1,  2, 165, 16 },
    {  2,  3, 180, 22 },
    {  2,  3, 200, 25 }, /* 18  */
    {  3,  4, 225, 30 },
    {  3,  5, 250, 35 }, /* 20  */
    {  4,  6, 300, 40 },
    {  4,  6, 350, 45 },
    {  5,  7, 400, 50 },
    {  5,  8, 450, 55 },
    {  6,  9, 500, 60 }  /* 25   */
};



const	struct	int_app_type	int_app		[26]		=
{
    {  3 },	/*  0 */
    {  5 },	/*  1 */
    {  7 },
    {  8 },	/*  3 */
    {  9 },
    { 10 },	/*  5 */
    { 11 },
    { 12 },
    { 13 },
    { 15 },
    { 17 },	/* 10 */
    { 19 },
    { 22 },
    { 25 },
    { 28 },
    { 31 },	/* 15 */
    { 34 },
    { 37 },
    { 40 },	/* 18 */
    { 44 },
    { 49 },	/* 20 */
    { 55 },
    { 60 },
    { 70 },
    { 80 },
    { 85 }	/* 25 */
};



const	struct	wis_app_type	wis_app		[26]		=
{
    { 0 },	/*  0 */
    { 0 },	/*  1 */
    { 0 },
    { 0 },	/*  3 */
    { 0 },
    { 1 },	/*  5 */
    { 1 },
    { 1 },
    { 1 },
    { 1 },
    { 1 },	/* 10 */
    { 1 },
    { 1 },
    { 1 },
    { 1 },
    { 2 },	/* 15 */
    { 2 },
    { 2 },
    { 3 },	/* 18 */
    { 3 },
    { 3 },	/* 20 */
    { 3 },
    { 4 },
    { 4 },
    { 4 },
    { 5 }	/* 25 */
};



const	struct	dex_app_type	dex_app		[26]		=
{
    {   60 },   /* 0 */
    {   50 },   /* 1 */
    {   50 },
    {   40 },
    {   30 },
    {   20 },   /* 5 */
    {   10 },
    {    0 },
    {    0 },
    {    0 },
    {    0 },   /* 10 */
    {    0 },
    {    0 },
    {    0 },
    {    0 },
    { - 10 },   /* 15 */
    { - 15 },
    { - 20 },
    { - 30 },
    { - 40 },
    { - 50 },   /* 20 */
    { - 60 },
    { - 75 },
    { - 90 },
    { -105 },
    { -120 }    /* 25 */
};


const	struct	con_app_type	con_app		[26]		=
{
    { -4, 20 },   /*  0 */
    { -3, 25 },   /*  1 */
    { -2, 30 },
    { -2, 35 },	  /*  3 */
    { -1, 40 },
    { -1, 45 },   /*  5 */
    { -1, 50 },
    {  0, 55 },
    {  0, 60 },
    {  0, 65 },
    {  0, 70 },   /* 10 */
    {  0, 75 },
    {  0, 80 },
    {  0, 85 },
    {  0, 88 },
    {  1, 90 },   /* 15 */
    {  2, 95 },
    {  2, 97 },
    {  3, 99 },   /* 18 */
    {  3, 99 },
    {  4, 99 },   /* 20 */
    {  4, 99 },
    {  5, 99 },
    {  6, 99 },
    {  7, 99 },
    {  8, 99 }    /* 25 */
};



/*
 * Liquid properties.
 * Used in world.obj.
 */
const	struct	liq_type	liq_table	[]	=
{
/*    name			color	proof, full, thirst, food, ssize */
    { "water",			"clear",	{   0, 1, 10, 0, 16 }	},
    { "beer",			"amber",	{  12, 1,  8, 1, 12 }	},
    { "red wine",		"burgundy",	{  30, 1,  8, 1,  5 }	},
    { "ale",			"brown",	{  15, 1,  8, 1, 12 }	},
    { "dark ale",		"dark",		{  16, 1,  8, 1, 12 }	},

    { "whisky",			"golden",	{ 120, 1,  5, 0,  2 }	},
    { "lemonade",		"pink",		{   0, 1,  9, 2, 12 }	},
    { "firebreather",		"boiling",	{ 190, 0,  4, 0,  2 }	},
    { "local specialty",	"clear",	{ 151, 1,  3, 0,  2 }	},
    { "slime mold juice",	"green",	{   0, 2, -8, 1,  2 }	},

    { "milk",			"white",	{   0, 2,  9, 3, 12 }	},
    { "tea",			"tan",		{   0, 1,  8, 0,  6 }	},
    { "coffee",			"black",	{   0, 1,  8, 0,  6 }	},
    { "blood",			"red",		{   0, 2, -1, 2,  6 }	},
    { "salt water",		"clear",	{   0, 1, -2, 0,  1 }	},

    { "coke",			"brown",	{   0, 2,  9, 2, 12 }	},
    { "root beer",		"brown",	{   0, 2,  9, 2, 12 }   },
    { "elvish wine",		"green",	{  35, 2,  8, 1,  5 }   },
    { "white wine",		"golden",	{  28, 1,  8, 1,  5 }   },
    { "champagne",		"golden",	{  32, 1,  8, 1,  5 }   },

    { "mead",			"honey-colored",{  34, 2,  8, 2, 12 }   },
    { "rose wine",		"pink",		{  26, 1,  8, 1,  5 }	},
    { "benedictine wine",	"burgundy",	{  40, 1,  8, 1,  5 }   },
    { "vodka",			"clear",	{ 130, 1,  5, 0,  2 }   },
    { "cranberry juice",	"red",		{   0, 1,  9, 2, 12 }	},

    { "orange juice",		"orange",	{   0, 2,  9, 3, 12 }   },
    { "absinthe",		"green",	{ 200, 1,  4, 0,  2 }	},
    { "brandy",			"golden",	{  80, 1,  5, 0,  4 }	},
    { "aquavit",		"clear",	{ 140, 1,  5, 0,  2 }	},
    { "schnapps",		"clear",	{  90, 1,  5, 0,  2 }   },

    { "icewine",		"purple",	{  50, 2,  6, 1,  5 }	},
    { "amontillado",		"burgundy",	{  35, 2,  8, 1,  5 }	},
    { "sherry",			"red",		{  38, 2,  7, 1,  5 }   },
    { "framboise",		"red",		{  50, 1,  7, 1,  5 }   },
    { "rum",			"amber",	{ 151, 1,  4, 0,  2 }	},

    { "cordial",		"clear",	{ 100, 1,  5, 0,  2 }   },
    { "tomato juice",		"red",		{   0, 2,  9, 3, 12 }   },
    { "strawberry kiwi juice",	"light pink",	{   0, 2,  9, 3, 12 }   },
    { "alcohol-free beer",	"amber",	{   0, 1,  8, 1, 12 }   },
    { "soup",			"brown",	{   0, 2,  9, 3, 12 }	},

    { "sake",			"clear",	{ 151, 1,  4, 0,  2 }	},
    { "liquid blood",		"red",		{   0, 2,  9, 3, 12 }	},
    { "milk-n-honey",		"mostly white",	{   0, 2,  9, 4, 12 }	},

    { NULL,			NULL,		{   0, 0,  0, 0,  0 }	}
};

const char *  day_name        [] =
{
    "the Moon", "the Bull", "Deception", "Thunder", "Freedom",
    "the Great Gods", "the Sun"
};

const char *  month_name      [] =
{
    "Winter", "the Winter Wolf", "the Frost Giant", "the Old Forces", "the Grand Struggle",
    "the Spring", "Nature", "Futility", "the Dragon",
    "the Sun", "the Heat", "the Battle", "the Dark Shades",
    "the Shadows", "the Long Shadows", "the Ancient Darkness", "the Great Evil"
};

char * const he_she  [] = { "it",  "he",  "she" };
char * const him_her [] = { "it",  "him", "her" };
char * const his_her [] = { "its", "his", "her" };

char * const dir_name[] = { "north","east","south","west","up","down" };
char * const dir_name_short[] = { "N","E","S","W","U","D" };
const int rev_dir[]	= { DIR_SOUTH, DIR_WEST, DIR_NORTH, DIR_EAST,
			    DIR_DOWN, DIR_UP };


const char *attribute_flags[] = {
    "Strength",
    "Intelligence",
    "Wisdom",
    "Dexterity",
    "Constitution"
};

const char *target_flags[] = {
    "Ignored",
    "Character offensive",
    "Character defensive",
    "Character self",
    "Object inventory",
    "Object character defensive",
    "Object character offensive",
    "Character cancel"
};

/* for hunt */
const struct hunt_type hunt_table[] = {
   {	"none"	},
   {	"kill"	},
   {	"room"	},
   {	"find"	},
   {	"gfind"	},
   {	NULL	}
};

/* for sizes */
const struct size_type size_table[]={
    {	"tiny"		},
    {	"small"		},
    {	"medium"	},
    {	"large"		},
    {	"huge"		},
    {	"giant"		},
    {	NULL		}
};

/* for position */
const struct position_type position_table[] = {
    {	"dead",			"dead"	},
    {	"mortally wounded",	"mort"	},
    {	"incapacitated",	"incap"	},
    {	"stunned",		"stun"	},
    {	"sleeping",		"sleep"	},
    {	"resting",		"rest"	},
    {	"sitting",		"sit"	},
    {	"fighting",		"fight"	},
    {	"standing",		"stand"	},
    {	NULL,			NULL	}
};



/*
 * The skill and spell table.
 */

#include "fd_skilltable.h"
#include "fd_grouptable.h"

