//
// $Id: fd_job.h,v 1.5 2004/02/15 10:22:22 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef INCLUDED_FD_JOBS_H
#define INCLUDED_FD_JOBS_H

void	save_jobs		(void);
bool	is_command_allowed	(CHAR_DATA *ch,int command);
bool	is_channel_allowed	(CHAR_DATA *ch,int channel);
bool	is_job_note_target	(char *category);
bool	char_has_job		(CHAR_DATA *ch,char *jobname);
bool	char_has_jobtarget_in_list	(CHAR_DATA *ch,char *category);
void	job_grant		(CHAR_DATA *ch,CHAR_DATA *victim,char *jobname);
void	job_revoke		(CHAR_DATA *ch,CHAR_DATA *victim,char *jobname);
void	job_show		(CHAR_DATA *ch,CHAR_DATA *victim);
void	jobs_to_str		(char *buf, int size, CHAR_DATA *ch);
void	jobs_from_str		(CHAR_DATA *ch, char *jobstr);
void	mortalcouncil_update	(CHAR_DATA *ch);

#endif	// INCLUDED_FD_JOBS_H
