/* $Id: act_move.c,v 1.148 2008/04/06 19:44:24 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

/*
 * 19980316 EG  Modifed do_open() and do_close() to stop working
		for objects if the level of the object is higher
		than the players' level.
 */

#include "merc.h"
#include "interp.h"
#include "olc.h"	/* For recommended damage values */


const	int	movement_loss	[SECT_MAX]	=
{
    1, 2, 2, 3, 4, 6, 4, 4, 5, 10, 6
};



/*
 * Local functions.
 */
int	find_door	( CHAR_DATA *ch, char *arg );
bool	has_key		( CHAR_DATA *ch, int key );

ROOM_INDEX_DATA *exit_target(CHAR_DATA *ch, EXIT_DATA *pexit) {
    int to_vnum;
    if (IS_SET(pexit->exit_info, EX_GATE_RECALL) &&
	GetCharProperty(ch,PROPERTY_INT,"exit_store",&to_vnum)) {
	return get_room_index(to_vnum);
    }
    return pexit->to_room;
}

bool is_char_allowed_in_room(CHAR_DATA *ch, ROOM_INDEX_DATA *room, bool loud) {

    if ( !is_room_owner(ch,room) &&
	 room_is_private( room ) &&
	 !is_allowed_in_room(ch,room)) {
	if (loud) send_to_char( "That room is private right now.\n\r", ch );
	return FALSE;
    }

    if ( !IS_NPC(ch) )
    {
	char s[MAX_STRING_LENGTH];

	if (!IS_IMMORTAL(ch) &&
	    GetRoomProperty(room,PROPERTY_STRING,"clan",s)) {
	    CLAN_INFO *pClan;
	    char claninvited[MAX_STRING_LENGTH];
	    bool is_invited=FALSE;

	    if (IS_AFFECTED(ch,EFF_INVITED))
		if (GetCharProperty(ch,PROPERTY_STRING,"invited_for",
							    claninvited)
		&& !str_cmp(s,claninvited))
		    is_invited=TRUE;

	    if (!is_invited) {
		if (ch->clan==NULL || ch->clan->clan_info->independent) {
		    if (loud) send_to_char("That room does belong to a clan!\n\r",ch);
		    return FALSE;
		}
		pClan=ch->clan->clan_info;
		if (!is_exact_name(pClan->name,s)) {
		    if (loud) sprintf_to_char(ch,"That room belongs to the %s clan, "
			"so you're not allowed to enter!\n\r",s);
		    return FALSE;
		}
	    }
	}

	/* property: classonly */
	if (GetRoomProperty(room,PROPERTY_STRING,"classonly",s)) {
	    if (!is_exact_name(class_table[ch->class].name,s)) {
		if (loud) send_to_char( "Your kind isn't allowed in there.\n\r", ch );
		return FALSE;
	    }
	}

	/* property: classanti */
	if (GetRoomProperty(room,PROPERTY_STRING,"classanti",s)) {
	    if (is_exact_name(class_table[ch->class].name,s)) {
		if (loud) send_to_char( "Your kind isn't allowed in there.\n\r", ch );
		return FALSE;
	    }
	}

	/* property: raceonly */
	if (GetRoomProperty(room,PROPERTY_STRING,"raceonly",s)) {
	    if (!is_exact_name(race_table[ch->race].name,s)) {
		if (loud) send_to_char( "Your race isn't allowed in there.\n\r", ch );
		return FALSE;
	    }
	}

	/* property: raceanti */
	if (GetRoomProperty(room,PROPERTY_STRING,"raceanti",s)) {
	    if (is_exact_name(race_table[ch->race].name,s)) {
		if (loud) send_to_char( "Your race isn't allowed in there.\n\r", ch );
		return FALSE;
	    }
	}
    }
    return TRUE;
}


void move_char( CHAR_DATA *ch, int door, bool follow, bool ignorecharm )
{
    CHAR_DATA *fch;
    CHAR_DATA *fch_next;
    ROOM_INDEX_DATA *in_room;
    ROOM_INDEX_DATA *to_room;
    EXIT_DATA *pexit;
    int move;

    if ( door < 0 || door >= DIR_MAX )
    {
	bugf( "Do_move: bad door %d for %s.", door,NAME(ch));
	return;
    }

    //
    // hack to make shoving of boats working with people sitten
    //
    if (ch->position==POS_FIGHTING) {
	send_to_char( "No way!  You are still fighting!\n\r", ch);
	return;
    }

    if (!IS_NPC(ch)) {
	/* Uh oh, another drunk Frenchman on the loose! :) */
	if (ch->pcdata->condition[COND_DRUNK] > 10) {
	    if (ch->pcdata->condition[COND_DRUNK] > number_percent()) {
		act("You feel a little drunk.. not to mention kind of lost..",
		    ch,NULL,NULL,TO_CHAR);
		act("$n looks a little drunk.. not to mention kind of lost..",
		    ch,NULL,NULL,TO_ROOM);
		door = number_range(0,DIR_MAX-1);
	    } else {
		act("You feel a little.. drunk..",ch,NULL,NULL,TO_CHAR);
		act("$n looks a little.. drunk..",ch,NULL,NULL,TO_ROOM);
	    }
	}
    }

    in_room = ch->in_room;
    if ( ( pexit   = in_room->exit[door] ) == NULL
    ||   ( to_room = exit_target(ch,pexit) ) == NULL
    ||	 !can_see_room(ch,to_room) )
    {
	if ((IS_NPC(ch) && number_percent() < 10)
	|| (!IS_NPC(ch) && number_percent() < (10 +
	    ch->pcdata->condition[COND_DRUNK]))) {
	    if (!IS_NPC(ch) && ch->pcdata->condition[COND_DRUNK] > 10) {
		act("You drunkenly slam face-first into the 'exit' on your"
		    " way $T.",ch,NULL,dir_name[door],TO_CHAR);
		act("$n drunkenly slams face-first into the 'exit' on $s"
		    " way $T.",ch,NULL,dir_name[door],TO_ROOM);
		damage(ch,ch,3,0,DAM_BASH,FALSE,NULL);
	    } else if (!IS_NPC(ch) && ch->pcdata->condition[COND_DRUNK] > 0) {
		act("You drunkenly face-first into the 'exit' on your way"
		    " $T. WHAM!",ch,NULL,dir_name[door],TO_CHAR);
		act("$n slams face-first into the 'exit' on $s way $T."
		    " WHAM!",ch,NULL,dir_name[door],TO_ROOM);
		damage(ch,ch,3,0,DAM_BASH,FALSE,NULL);
	    } else {
		send_to_char( "Alas, you cannot go that way.\n\r", ch );
	    }
	} else {
	    if (!IS_NPC(ch) && ch->pcdata->condition[COND_DRUNK] > 10) {
		act("You stumble about aimlessly and fall down drunk.",
		    ch,NULL,dir_name[door],TO_CHAR);
		act("$n stumbles about aimlessly and falls down drunk.",
		    ch,NULL,dir_name[door],TO_ROOM);
	    } else {
		send_to_char( "Alas, you cannot go that way.\n\r", ch );
	    }
	}
	return;
    }

    //
    // entanglement
    //
    if (is_affected(ch,gsn_entangle)) {
	OBJ_INDEX_DATA *obj_index;
	OBJ_DATA *obj;
	if (number_percent() <
	    50-8*(ch->perm_stat[STAT_STR]-get_curr_stat(ch,STAT_STR))) {

	    send_to_char("You break the twines, you're free!\n\r",ch);
	    act("$n breaks the twines which are holding $m.\n\r", // \n\r intentionally
		ch,NULL,NULL,TO_ROOM);
	    ch->move=(ch->move*50)/100;
	    obj_index=get_obj_index( OBJ_VNUM_TENDRILS );
	    obj = create_object( obj_index, 0 );
	    obj_to_room(obj,ch->in_room);
	    obj->timer=number_range(3,5);
	    effect_strip(ch,gsn_entangle);
	    op_load_trigger(obj,NULL);

	} else {
	    send_to_char(
		"You try to move, but the twines seem to hold you!\n\r",ch);
	    act("$n tries to move, but is held by the twines.",
		ch,NULL,NULL,TO_ROOM);
	    ch->move=(ch->move*20)/100;
	    WAIT_STATE(ch,8);
	    return;
	}
    }

    // dirty hack to prevent people sitting to leave the room.
    // it's normally done in interp.c but that won't work for boats.
    if (ch->position==POS_SITTING) {
	if (ch->on==NULL || ch->on->item_type!=ITEM_BOAT) {
	    send_to_char("Better stand up first.\n\r",ch);
	    return;
	}
	if (ch->on && ch->on->item_type==ITEM_BOAT &&
	    in_room->sector_type!=SECT_WATER_SWIM &&
	    in_room->sector_type!=SECT_WATER_NOSWIM &&
	    to_room->sector_type!=SECT_WATER_SWIM &&
	    to_room->sector_type!=SECT_WATER_NOSWIM) {
	    act("That would damage $p.",ch,ch->on,NULL,TO_CHAR);
	    return;
	}
    }

    /*
     * Exit trigger, if activated, bail out. Only PCs are triggered.
     */
    if (!IS_NPC(ch) && rp_leave_trigger(ch->in_room,ch,door)==FALSE )
	return;
    if (!IS_VALID(ch)) return;
    if (!IS_NPC(ch) && mp_exit_trigger(ch,door)==FALSE )
	return;
    if (!IS_VALID(ch)) return;
    if (!follow) mp_leave_trigger( ch, door );
    if (!IS_VALID(ch)) return;

    if (IS_SET(pexit->exit_info, EX_CLOSED)
    &&  (!IS_AFFECTED(ch, EFF_PASS_DOOR) || IS_SET(pexit->exit_info,EX_NOPASS))
    &&   !IS_TRUSTED(ch,LEVEL_IMMORTAL))
    {
        if(IS_SET(pexit->exit_info,EX_SECRET))
            send_to_char( "Alas, you cannot go that way.\n\r", ch );
        else
            act( "The $d is closed.", ch, NULL, pexit->keyword, TO_CHAR );
	return;
    }

    //
    // charming!
    // - pets can be send away
    // - mobs can be send away
    // - players can not be send away
    //
    if ( !ignorecharm && IS_AFFECTED(ch, EFF_CHARM)
    &&   ch->master != NULL
    &&  !STR_IS_SET(ch->strbit_act, ACT_PET)
    &&   IS_PC(ch)
    &&   in_room == ch->master->in_room )
    {
	act( "$N looks at you with adoring eyes.", ch->master,NULL,ch,TO_CHAR);
	send_to_char( "What?  And leave your beloved master?\n\r", ch );
	return;
    }

    if (!is_char_allowed_in_room(ch,to_room,TRUE))
	return;

    if ( in_room->sector_type == SECT_AIR
    ||   to_room->sector_type == SECT_AIR ) {
	if ( !IS_AFFECTED(ch, EFF_FLYING) && !IS_IMMORTAL(ch))
	{
	    send_to_char( "You can't fly.\n\r", ch );
	    return;
	}
	if (IS_AFFECTED(ch,EFF_HALLUCINATION))
	    send_to_char("Up, up, and awaaaay! You're walking on air!\n\r",ch);
    }

    // swimming below water
    if ((in_room->sector_type==SECT_WATER_BELOW ||
	 to_room->sector_type==SECT_WATER_BELOW)
    && ((!IS_AFFECTED(ch,EFF_SWIM))
    && !IS_IMMORTAL(ch))) {
	send_to_char("You can't swim!\n\r",ch);
	return;
    }

    if (!IS_IMMORTAL(ch))
    if ((( in_room->sector_type == SECT_WATER_NOSWIM    // no swim and
    ||    to_room->sector_type == SECT_WATER_NOSWIM )   //
    &&   !is_affected(ch,gsn_water_walk)		// not waterwalk and
    &&   !is_affected(ch,gsn_fly)			// not flying and
    &&    !IS_AFFECTED(ch,EFF_FLYING))		        // not flying
    || (( in_room->sector_type == SECT_WATER_SWIM       //
    ||    to_room->sector_type == SECT_WATER_SWIM )	// swim and
    &&   !IS_AFFECTED(ch,EFF_FLYING)		        // not flying and
    &&   !is_affected(ch,gsn_fly)			// not flying and
    &&   !is_affected(ch,gsn_water_walk)		// not waterwalk and
    &&   !IS_AFFECTED(ch,EFF_SWIM))) 			// not swimming
    {
	if (ch->on && ch->on->item_type==ITEM_BOAT) {
	    if (in_room->sector_type==SECT_WATER_NOSWIM ||
		in_room->sector_type==SECT_WATER_SWIM)
		do_function(ch,&do_paddle,dir_name[door]);
	    else
		do_function(ch,&do_shove,dir_name[door]);
	    return;
	}
	send_to_char("You need a boat to go there.\n\r",ch);
	return;
    }

    move = movement_loss[UMIN(SECT_MAX-1, in_room->sector_type)]
	 + movement_loss[UMIN(SECT_MAX-1, to_room->sector_type)]
	 ;

    move /= 2;  /* i.e. the average */

    if ( ch->move < 0 ) {
	send_to_char( "You are too exhausted.\n\r", ch );
	return;
    }

    /* conditional effects */
    if (IS_AFFECTED(ch,EFF_FLYING) || IS_AFFECTED(ch,EFF_HASTE))
	move /= 2;

    if (IS_AFFECTED(ch,EFF_SLOW))
	move *= 2;

    WAIT_STATE( ch, 1 );
    ch->move -= move;

    if ( IS_NPC(ch) && ch->move <= 0 ) {
	send_to_char( "You are too exhausted.\n\r", ch );
	if (ch->master)
	    act("$N is too exhausted to follow you.\n\r",
		ch->master,NULL,ch,TO_CHAR);
	return;
    }

    if ( !IS_AFFECTED(ch, EFF_SNEAK) && ch->invis_level < LEVEL_HERO) {
	if (!IS_NPC(ch) && ch->pcdata->condition[COND_DRUNK] > 10)
	    act("$n stumbles off drunkenly on $s way $T.",
		ch,NULL,dir_name[door],TO_ROOM);
	else
	    if (follow)
		act( "$n follows $N.",ch,NULL,ch->master,TO_ROOM );
	    else {
		if (IS_AFFECTED(ch,EFF_FLYING))
		    act( "$n flies $T.", ch, NULL, dir_name[door], TO_ROOM );
		else
		    act( "$n leaves $T.", ch, NULL, dir_name[door], TO_ROOM );
	    }
    } else {
	// show the sneaking/invis to all holylighters
	for ( fch = in_room->people; fch != NULL; fch = fch->next_in_room )
	  if (IS_PC(fch) && IS_IMMORTAL(fch) &&
	      STR_IS_SET(fch->strbit_act,PLR_HOLYLIGHT_PLUSPLUS) &&
	      ch->invis_level < get_trust(fch)) {
	      act( "$n sneaks $t.", ch, dir_name[door], fch, TO_VICT );
	  }
    }

    if (IS_SET(pexit->exit_info, EX_GATE_RECALL))
	DeleteCharProperty(ch,PROPERTY_INT,"exit_store");
    ap_leave_trigger(ch,to_room);
    char_from_room( ch );
    char_to_room( ch, to_room );

    if ( !IS_AFFECTED(ch, EFF_SNEAK) && ch->invis_level < LEVEL_HERO) {
	if (!IS_NPC(ch) && ch->pcdata->condition[COND_DRUNK] > 10)
	    act("$n stumbles in drunkenly, looking all nice and French.",
		ch,NULL,NULL,TO_ROOM);
	else
	    if (follow) {
		act("$n has arrived, following $N.",
		    ch,NULL,ch->master,TO_NOTVICT );
		act("$n has followed you.",
		    ch,NULL,ch->master,TO_VICT );
	    } else
		act( "$n has arrived.", ch, NULL, NULL, TO_ROOM );
	if(IS_AFFECTED2(ch, EFF_CANTRIP))
	    show_cantrip(NULL,ch,TO_ROOM);
        show_ool_cantrip(NULL,ch,TO_ROOM);

	if (is_affected(ch, gsn_plague))
	    act("$n has the scent of death around $m.",ch,NULL,NULL,TO_ROOM);

    } else {
	// show the sneaking/invis to all holylighters
	for ( fch = to_room->people; fch != NULL; fch = fch->next_in_room )
	  if (IS_PC(fch) && IS_IMMORTAL(fch) &&
	      STR_IS_SET(fch->strbit_act,PLR_HOLYLIGHT_PLUSPLUS) &&
	      ch->invis_level < get_trust(fch)) {
	      act( "$n sneaks in.", ch, NULL, fch, TO_VICT );
	  }
    }
    ap_enter_trigger(ch,in_room);

    look_room(ch,ch->in_room,TRUE);

    /* I think the death trap should be included here */
    if( STR_IS_SET(to_room->strbit_room_flags,ROOM_DEATHTRAP) && !IS_IMMORTAL(ch)) {
	char buf[MSL];

	sprintf( buf, "$N died in a deathtrap at %s [room %d]",
	    ch->in_room->name, ch->in_room->vnum);
	if (IS_NPC(ch))
	    wiznet(WIZ_MOBDEATHS,0,ch,NULL,"%s",buf);
	else
	    wiznet(WIZ_DEATHS,0,ch,NULL,"%s",buf);

	if (IS_PC(ch))
	    announce(ch,"%s died in %s.",ch->name,ch->in_room->name);

	act("You are killed!",ch,NULL,NULL,TO_CHAR);
	act("$n is killed!",ch,NULL,NULL,TO_ROOM);
	raw_kill( ch, NULL );
    }

    if (in_room == to_room) /* no circular follows */
	return;

    for ( fch = in_room->people; fch != NULL; fch = fch_next )
    {
	int align_margin;
	fch_next = fch->next_in_room;

	if (fch->master!=ch) continue;

	if ( !IS_IMMORTAL(ch) &&
	     GetCharProperty(fch,PROPERTY_INT,"pet_align_margin",&align_margin) &&
	     ( ch->alignment > fch->alignment+align_margin ||
	       ch->alignment < fch->alignment-align_margin )) {
	    act("$N refuses to follow you.",ch,NULL,fch,TO_CHAR);
            continue;
        }

	/* forget sleeping charmed pets */
	if ( IS_AFFECTED(fch,EFF_CHARM) &&
	     fch->position < POS_STANDING && fch->position > POS_SLEEPING)
	    do_function(fch, &do_stand, "");

	if ( can_see_room(fch,to_room)) {
	    if (fch->position == POS_STANDING) {

		if (STR_IS_SET(ch->in_room->strbit_room_flags,ROOM_LAW)
		&&  (IS_NPC(fch) && (STR_IS_SET(fch->strbit_act,ACT_AGGRESSIVE) ||
				     STR_IS_SET(fch->strbit_act,ACT_TCL_AGGRESSIVE))))
		{
		    act("You can't bring $N into the city.",
			ch,NULL,fch,TO_CHAR);
		    act("You aren't allowed in the city.",
			fch,NULL,NULL,TO_CHAR);
		    continue;
		}

		act( "You follow $N.", fch, NULL, ch, TO_CHAR );
		move_char( fch, door, TRUE, ignorecharm );
	    } else {
		if (fch->position==POS_SLEEPING) {
		    send_to_char(
			"You have the feeling you've forgotten someone...\n\r",
			fch->master);
		}
	    }
	}
    }

    /*
     * If someone is following the char, these triggers get activated
     * for the followers before the char, but it's safer this way...
     */
    if ( IS_NPC( ch ) && MOB_HAS_TRIGGER( ch, MTRIG_ENTRY ) )
	mp_entry_trigger(ch);
    if (!IS_VALID(ch))
	return;
    if (IS_PC(ch)) {
	rp_enter_trigger(ch->in_room,ch,rev_dir[door]);
    	mp_greet_trigger(ch,rev_dir[door]);
    }

    return;
}



void do_north( CHAR_DATA *ch, char *argument )
{
    move_char( ch, DIR_NORTH, FALSE, FALSE );
    return;
}



void do_east( CHAR_DATA *ch, char *argument )
{
    move_char( ch, DIR_EAST, FALSE, FALSE );
    return;
}



void do_south( CHAR_DATA *ch, char *argument )
{
    move_char( ch, DIR_SOUTH, FALSE, FALSE );
    return;
}



void do_west( CHAR_DATA *ch, char *argument )
{
    move_char( ch, DIR_WEST, FALSE, FALSE );
    return;
}



void do_up( CHAR_DATA *ch, char *argument )
{
    move_char( ch, DIR_UP, FALSE, FALSE );
    return;
}



void do_down( CHAR_DATA *ch, char *argument )
{
    move_char( ch, DIR_DOWN, FALSE, FALSE );
    return;
}



int find_door( CHAR_DATA *ch, char *arg )
{
    EXIT_DATA *pexit;
    int door;
    bool direction=TRUE;

    door = get_direction(arg);
    if ((door<0) || ((ch->in_room->exit[door]) && 
		     (IS_SET(ch->in_room->exit[door]->exit_info,EX_SECRET)) &&
		     (ch->in_room->exit[door]->keyword) &&
		     (ch->in_room->exit[door]->keyword[0])))
    {
	int i;
	char arg1[MSL];
	i=number_argument(arg,arg1);
	direction=FALSE;
	for ( door = 0; door < DIR_MAX; door++ )
	{
	    if ( ( pexit = ch->in_room->exit[door] ) != NULL
	    &&   IS_SET(pexit->exit_info, EX_ISDOOR)
	    &&   pexit->keyword != NULL
	    &&   is_name( arg1, pexit->keyword ) )
		if ((--i)==0)
		    return door;
	}
	act( "I see no $T here.", ch, NULL, arg, TO_CHAR );
	return -1;
    }

    if ( ( pexit = ch->in_room->exit[door] ) == NULL )
    {
	if(direction)
	  send_to_char( "There is no door in that direction.\n\r", ch );
	else
	  act( "I see no door $T here.", ch, NULL, arg, TO_CHAR );
	return -1;
    }

    if ( !IS_SET(pexit->exit_info, EX_ISDOOR) )
    {
	send_to_char( "There is no door in that direction.\n\r", ch );
	return -1;
    }

    return door;
}

static bool handle_trap(CHAR_DATA *ch,OBJ_DATA *obj,ROOM_INDEX_DATA *room,int door) {
    enum {OBJ,EXIT,PORTAL} type;
    enum {TRAP,DARTS,POISON,EXPLODING,SLEEPGAS,DEATH};

    int flag[6][3]= {
    // -> type
    // \/ flag
	{	CONT_TRAP,		EX_TRAP,		EX_TRAP			},
	{	CONT_TRAP_DARTS,	EX_TRAP_DARTS,		EX_TRAP_DARTS		},
	{	CONT_TRAP_POISON,	EX_TRAP_POISON,		EX_TRAP_POISON		},
	{	CONT_TRAP_EXPLODING,	EX_TRAP_EXPLODING,	EX_TRAP_EXPLODING	},
	{	CONT_TRAP_SLEEPGAS,	EX_TRAP_SLEEPGAS,	EX_TRAP_SLEEPGAS	},
	{	CONT_TRAP_DEATH,	EX_TRAP_DEATH,		EX_TRAP_DEATH		}
    };

    char *name;
    int flags;
    int level;
    EXIT_DATA *exit;

    if (room) { 
	type=EXIT;
	exit=room->exit[door];
	name=exit->keyword;
	flags=exit->exit_info;
	level=room->level;
    } else if (obj) {
	switch (obj->item_type) {
	    case ITEM_CONTAINER:
		type=OBJ;
		name=obj->short_descr;
		flags=obj->value[1];
		level=obj->level;
		room=NULL;
		break;
	    case ITEM_PORTAL:
		type=PORTAL;
		name=obj->short_descr;
		flags=obj->value[1];
		level=obj->level;
		room=NULL;
		break;
	    default:
	      bugf("handle_trap() called for non-portal/container obj");
	      return FALSE;
	}
    } else {
	bugf("handle_trap() called without obj or exit");
	return FALSE;
    }

    if ((type==OBJ || type==PORTAL) && OBJ_HAS_TRIGGER(obj,OTRIG_PRETRAP)) {
	if (!op_pretrap_trigger(obj,ch))
	    return FALSE;
    } else
    if (type==EXIT && ROOM_HAS_TRIGGER(room,RTRIG_PRETRAP)) {
	bugf("PRETRAP TRIGGER");
	if (!rp_pretrap_trigger(room,ch,door))
	    return FALSE;
    } else
    if(number_percent()<20) {
	act("You hear a contraption click, but nothing happens.",ch,NULL,NULL,TO_ALL);
	return FALSE;
    }

    /* The trap springs, determine the effects */
    if (IS_SET(flags,flag[DARTS][type])) {
	int thelevel=URANGE(0,level,90);
	int dam=dice(
	    olc_mob_recommended[thelevel].hit[0],
	    olc_mob_recommended[thelevel].hit[1])+
	    olc_mob_recommended[thelevel].hit[2];

	dam/=2;

	if(damage(ch,ch,dam,TYPE_UNDEFINED,DAM_PIERCE,FALSE, NULL))
	{
	    act("A dart springs from $T and hits you.",ch,NULL,name,TO_CHAR);
	    act("A dart springs from $T and hits $n.",ch,NULL,name,TO_ROOM);
	} else {
	    act("A dart springs from $T but missed you.",ch,NULL,name,TO_CHAR);
	    act("A dart springs from $T but misses $n.",ch,NULL,name,TO_ROOM);
	}
    }

    if (IS_SET(flags,flag[POISON][type])) {
	act("Something in $T stings you.",ch,NULL,name,TO_CHAR);
	act("Something in $T stings $n.",ch,NULL,name,TO_ROOM);
	spell_poison(gsn_poison,level,NULL,ch,TAR_CHAR_SELF);
    }

    if (IS_SET(flags,flag[EXPLODING][type])) {
	CHAR_DATA *c,*c_next;
	int thelevel=URANGE(0,level+5,90);
	int dam=dice(
	    olc_mob_recommended[thelevel].hit[0],
	    olc_mob_recommended[thelevel].hit[1])+
	    olc_mob_recommended[thelevel].hit[2];

	dam/=3;

	act("Something in $T explodes!!!.",ch,NULL,name,TO_ALL);
	for(c=ch->in_room->people;c;c=c_next)
	{
	    c_next=c->next_in_room;

	    if( damage(c,c,dam,TYPE_UNDEFINED,DAM_FIRE,FALSE, NULL) )
	    {
		act("You are hit by the explosion.",c,NULL,NULL,TO_CHAR);
	    	act("$n is hit by the explosion.",c,NULL,NULL,TO_ROOM);
	    }
	}
    }

    if (IS_SET(flags,flag[SLEEPGAS][type])) {
	CHAR_DATA *c,*c_next;
        EFFECT_DATA ef;

	act("A gas is flowing from $T.",ch,NULL,name,TO_ALL);

	for(c=ch->in_room->people;c;c=c_next)
        {
	    c_next=c->next_in_room;

	    if ( IS_AFFECTED(c, EFF_SLEEP)
	    ||   (IS_NPC(c) && STR_IS_SET(c->strbit_act,ACT_UNDEAD))
	    ||   (level + 2) < c->level
	    ||   saves_spell( level-4, c,DAM_CHARM) )
		continue;

	    ZEROVAR(&ef,EFFECT_DATA);
	    ef.where     = TO_EFFECTS;
	    ef.type      = gsn_sleep;
	    ef.level     = level;
	    ef.duration  = 4 + level;
	    ef.location  = APPLY_NONE;
	    ef.modifier  = 0;
	    ef.bitvector = EFF_SLEEP;
	    effect_join( c, &ef );

	    if ( IS_AWAKE(c) )
	    {
		send_to_char( "You feel very sleepy ..... zzzzzz.\n\r", c );
		act( "$n goes to sleep.", c, NULL, NULL, TO_ROOM );
		c->position = POS_SLEEPING;
	    }
      	}
    }

    if (IS_SET(flags,flag[DEATH][type])) {
	act("You are killed by the contraption on $T.",ch,NULL,name,TO_CHAR);
	act("$n is killed by the contraption on $T.",ch,NULL,name,TO_ROOM);
	if(IS_IMMORTAL(ch))
	{
	    act("Oops... You are an immortal... So you may live.",ch,NULL,NULL,TO_CHAR);
	    act("Oops... $n is an immortal... So $e may live.",ch,NULL,NULL,TO_ROOM);
	} else {
	    char buf[MSL];

	    sprintf( buf, "$N died in a deathtrap-exit at %s [room %d]",
		ch->in_room->name, ch->in_room->vnum);
	    if (IS_NPC(ch))
		wiznet(WIZ_MOBDEATHS,0,ch,NULL,"%s",buf);
	    else
		wiznet(WIZ_DEATHS,0,ch,NULL,"%s",buf);
	    raw_kill( ch, NULL );
	}
    }

    if ((type==OBJ || type==PORTAL) && OBJ_HAS_TRIGGER(obj,OTRIG_TRAP)) {
	op_trap_trigger(obj,ch);
    }
    if (type==EXIT && ROOM_HAS_TRIGGER(room,RTRIG_TRAP)) {
	rp_trap_trigger(room,ch,door);
    }

    return TRUE;
}

bool handle_portal_trap(CHAR_DATA *ch,OBJ_DATA *obj) {
    if (handle_trap(ch,obj,NULL,0)) {
	if (IS_SET(obj->value[1], CONT_TRAP_EXTRACT)) {
	    act("$T is destroyed.",ch,NULL,obj->short_descr,TO_ALL);
	    extract_obj( obj );
	}
	return TRUE;
    }
    return FALSE;
}

bool handle_exit_trap(CHAR_DATA *ch,ROOM_INDEX_DATA *room, int door) {
    return handle_trap(ch,NULL,room,door);
}

bool handle_cont_trap(CHAR_DATA *ch,OBJ_DATA *obj) {

    if (handle_trap(ch,obj,NULL,0)) {
	if (IS_SET(obj->value[1], CONT_TRAP_EXTRACT)) {
	    act("$T is destroyed.",ch,NULL,obj->short_descr,TO_ALL);
	    extract_obj( obj );
	}
	return TRUE;
    }

    return FALSE;
}

void do_open( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    OBJ_DATA *obj;
    int door;

    one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Open what?\n\r", ch );
	return;
    }

    if ( !IS_DIRECTION(arg) && ( obj = get_obj_here( ch, arg ) ) != NULL )
    {
 	/* open portal */
	if (obj->item_type == ITEM_PORTAL)
	{
	    if (!IS_SET(obj->value[1], EX_ISDOOR))
	    {
		send_to_char("You can't do that.\n\r",ch);
		return;
	    }

	    if (!IS_SET(obj->value[1], EX_CLOSED))
	    {
		send_to_char("It's already open.\n\r",ch);
		return;
	    }

	    if (IS_SET(obj->value[1], EX_LOCKED))
	    {
		send_to_char("It's locked.\n\r",ch);
		return;
	    }

	    if (IS_SET(obj->value[1], EX_TRAP))
	    {
		if(handle_portal_trap(ch,obj)) return;
	    }

	    REMOVE_BIT(obj->value[1], EX_CLOSED);
	    act("You open $p.",ch,obj,NULL,TO_CHAR);
	    act("$n opens $p.",ch,obj,NULL,TO_ROOM);
	    return;
 	}

	/* 'open object' */
	if ( obj->item_type != ITEM_CONTAINER)
	    { send_to_char( "That's not a container.\n\r", ch ); return; }
	if ( !IS_SET(obj->value[1], CONT_CLOSED) )
	    { send_to_char( "It's already open.\n\r",      ch ); return; }
	if ( !IS_NPC(ch) && obj->level>ch->level) {
	    act("The lock of the $d keeps closing itself.",
		ch,NULL,obj->name,TO_CHAR);
	    return;
	}
	if ( !IS_SET(obj->value[1], CONT_CLOSEABLE) )
	    { send_to_char( "You can't open it.\n\r",     ch ); return; }
	if ( IS_SET(obj->value[1], CONT_LOCKED) )
	    { send_to_char( "It's locked.\n\r",            ch ); return; }

	if ( IS_SET(obj->value[1], CONT_TRAP) ) {
	    if(handle_cont_trap(ch,obj)) return;
	}

	// pre drop-check, if it is true don't drop it.
	if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREOPEN ) )
	    if (op_preopen_trigger( obj, ch )==FALSE)
		return;
	if (!IS_VALID(obj))
	    return;

	REMOVE_BIT(obj->value[1], CONT_CLOSED);
	act("You open $p.",ch,obj,NULL,TO_CHAR);
	act( "$n opens $p.", ch, obj, NULL, TO_ROOM );

	if (OBJ_HAS_TRIGGER(obj,OTRIG_OPEN))
	    op_open_trigger(obj,ch);
	return;
    }

    if ( ( door = find_door( ch, arg ) ) >= 0 )
    {
	/* 'open door' */
	ROOM_INDEX_DATA *to_room;
	EXIT_DATA *pexit;
	EXIT_DATA *pexit_rev;
	CHAR_DATA *rch;

	pexit = ch->in_room->exit[door];
	if ( !IS_SET(pexit->exit_info, EX_CLOSED) )
	    { send_to_char( "It's already open.\n\r",      ch ); return; }
	if (  IS_SET(pexit->exit_info, EX_LOCKED) )
	    { send_to_char( "It's locked.\n\r",            ch ); return; }

	if(IS_SET(pexit->exit_info, EX_TRAP))
	{
	    if(handle_exit_trap(ch,ch->in_room,door)) return;
	    REMOVE_BIT(pexit->exit_info, EX_TRAP);
	}

	REMOVE_BIT(pexit->exit_info, EX_CLOSED);
	act( "$n opens the $d.", ch, NULL, pexit->keyword, TO_ROOM );
	send_to_char( "Ok.\n\r", ch );

	/* open the other side */
	if ( ( to_room   = pexit->to_room            ) != NULL
	&&   ( pexit_rev = to_room->exit[rev_dir[door]] ) != NULL
	&&   pexit_rev->to_room == ch->in_room )
	{
	    REMOVE_BIT( pexit_rev->exit_info, EX_CLOSED );
	    for ( rch = to_room->people; rch != NULL; rch = rch->next_in_room )
		act( "The $d opens.", rch, NULL, pexit_rev->keyword, TO_CHAR );
	}

    }

    return;
}



void do_close( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    OBJ_DATA *obj;
    int door;

    one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Close what?\n\r", ch );
	return;
    }

    if ( !IS_DIRECTION(arg) && ( obj = get_obj_here( ch, arg ) ) != NULL )
    {
	/* portal stuff */
	if (obj->item_type == ITEM_PORTAL)
	{

	    if (!IS_SET(obj->value[1],EX_ISDOOR)
	    ||   IS_SET(obj->value[1],EX_NOCLOSE))
	    {
		send_to_char("You can't do that.\n\r",ch);
		return;
	    }

	    if (IS_SET(obj->value[1],EX_CLOSED))
	    {
		send_to_char("It's already closed.\n\r",ch);
		return;
	    }

	    SET_BIT(obj->value[1],EX_CLOSED);
	    act("You close $p.",ch,obj,NULL,TO_CHAR);
	    act("$n closes $p.",ch,obj,NULL,TO_ROOM);
	    return;
	}

	/* 'close object' */
	if ( obj->item_type != ITEM_CONTAINER )
	    { send_to_char( "That's not a container.\n\r", ch ); return; }
	if ( IS_SET(obj->value[1], CONT_CLOSED) )
	    { send_to_char( "It's already closed.\n\r",    ch ); return; }
	if (obj->level>ch->level) {
	    act("The lock of the $d keeps on popping open.",
		ch,NULL,obj->name,TO_CHAR);
	    return;
	}
	if ( !IS_SET(obj->value[1], CONT_CLOSEABLE) )
	    { send_to_char( "You can't do that.\n\r",      ch ); return; }

	// pre drop-check, if it is true don't drop it.
	if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRECLOSE ) )
	    if (op_preclose_trigger( obj, ch )==FALSE)
		return;
	if (!IS_VALID(obj))
	    return;

	SET_BIT(obj->value[1], CONT_CLOSED);
	act("You close $p.",ch,obj,NULL,TO_CHAR);
	act( "$n closes $p.", ch, obj, NULL, TO_ROOM );

	if (OBJ_HAS_TRIGGER(obj,OTRIG_CLOSE))
	    op_close_trigger(obj,ch);
	return;
    }

    if ( ( door = find_door( ch, arg ) ) >= 0 )
    {
	/* 'close door' */
	ROOM_INDEX_DATA *to_room;
	EXIT_DATA *pexit;
	EXIT_DATA *pexit_rev;

	pexit	= ch->in_room->exit[door];
	if ( IS_SET(pexit->exit_info, EX_CLOSED) )
	    { send_to_char( "It's already closed.\n\r",    ch ); return; }

	SET_BIT(pexit->exit_info, EX_CLOSED);
	act( "$n closes the $d.", ch, NULL, pexit->keyword, TO_ROOM );
	send_to_char( "Ok.\n\r", ch );

	/* close the other side */
	if ( ( to_room   = pexit->to_room            ) != NULL
	&&   ( pexit_rev = to_room->exit[rev_dir[door]] ) != 0
	&&   pexit_rev->to_room == ch->in_room )
	{
	    CHAR_DATA *rch;

	    SET_BIT( pexit_rev->exit_info, EX_CLOSED );
	    for ( rch = to_room->people; rch != NULL; rch = rch->next_in_room )
		act( "The $d closes.", rch, NULL, pexit_rev->keyword, TO_CHAR );
	}
    }

    return;
}



bool has_key( CHAR_DATA *ch, int key )
{
    OBJ_DATA *obj;

    for ( obj = ch->carrying; obj != NULL; obj = obj->next_content )
    {
	if ( obj->pIndexData->vnum == key )
	    return TRUE;
    }

    return FALSE;
}



void do_lock( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    OBJ_DATA *obj;
    int door;

    one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Lock what?\n\r", ch );
	return;
    }

    if ( !IS_DIRECTION(arg) && ( obj = get_obj_here( ch, arg ) ) != NULL )
    {
	/* portal stuff */
	if (obj->item_type == ITEM_PORTAL)
	{
	    if (!IS_SET(obj->value[1],EX_ISDOOR)
	    ||  IS_SET(obj->value[1],EX_NOCLOSE))
	    {
		send_to_char("You can't do that.\n\r",ch);
		return;
	    }
	    if (!IS_SET(obj->value[1],EX_CLOSED))
	    {
		send_to_char("It's not closed.\n\r",ch);
	 	return;
	    }

	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_CANLOCK ) ) {
		if (op_canlock_trigger( obj, ch )==FALSE)
		    return;
	    } else {
		if (obj->value[4] < 0 || IS_SET(obj->value[1],EX_NOLOCK))
		{
		    send_to_char("It can't be locked.\n\r",ch);
		    return;
		}
		if (!has_key(ch, obj->value[4])) {
		    send_to_char("You lack the key.\n\r", ch);
		    return;
		}
	    }

	    if (IS_SET(obj->value[1],EX_LOCKED))
	    {
		send_to_char("It's already locked.\n\r",ch);
		return;
	    }

	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRELOCK ) )
		if (op_prelock_trigger( obj, ch )==FALSE)
		    return;
	    if (!IS_VALID(obj))
		return;

	    SET_BIT(obj->value[1],EX_LOCKED);
	    act("You lock $p.",ch,obj,NULL,TO_CHAR);
	    act("$n locks $p.",ch,obj,NULL,TO_ROOM);

	    if (OBJ_HAS_TRIGGER(obj,OTRIG_LOCK))
		op_lock_trigger(obj,ch);

	    return;
	}

	/* 'lock object' */
	if ( obj->item_type != ITEM_CONTAINER )
	    { send_to_char( "That's not a container.\n\r", ch ); return; }
	if ( !IS_SET(obj->value[1], CONT_CLOSED) )
	    { send_to_char( "It's not closed.\n\r",        ch ); return; }
	if ( OBJ_HAS_TRIGGER( obj, OTRIG_CANLOCK ) ) {
	    if (op_canlock_trigger( obj, ch )==FALSE)
		return;
	} else {
	    if ( obj->value[2] < 0 )
		{ send_to_char( "It can't be locked.\n\r",     ch ); return; }
	    if ( !has_key( ch, obj->value[2] ) )
		{ send_to_char( "You lack the key.\n\r",       ch ); return; }
	}
	if ( IS_SET(obj->value[1], CONT_LOCKED) )
	    { send_to_char( "It's already locked.\n\r",    ch ); return; }

	if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRELOCK ) )
	    if (op_prelock_trigger( obj, ch )==FALSE)
		return;
	if (!IS_VALID(obj))
	    return;

	SET_BIT(obj->value[1], CONT_LOCKED);
	act("You lock $p.",ch,obj,NULL,TO_CHAR);
	act( "$n locks $p.", ch, obj, NULL, TO_ROOM );

	if (OBJ_HAS_TRIGGER(obj,OTRIG_LOCK))
	    op_lock_trigger(obj,ch);
	return;
    }

    if ( ( door = find_door( ch, arg ) ) >= 0 )
    {
	/* 'lock door' */
	ROOM_INDEX_DATA *to_room;
	EXIT_DATA *pexit;
	EXIT_DATA *pexit_rev;

	pexit	= ch->in_room->exit[door];
	if ( !IS_SET(pexit->exit_info, EX_CLOSED) )
	    { send_to_char( "It's not closed.\n\r",        ch ); return; }
	if ( IS_SET(pexit->exit_info, EX_TCLLOCK) &&
	     ROOM_HAS_TRIGGER( ch->in_room, RTRIG_CANLOCK ) ) {
	    if (rp_canlock_trigger( ch->in_room, ch, door )==FALSE)
		return;
	} else {
	    if ( pexit->key < 0 )
		{ send_to_char( "It can't be locked.\n\r",     ch ); return; }
	    if ( !has_key( ch, pexit->key ) )
		{ send_to_char( "You lack the key.\n\r",       ch ); return; }
	}
	if ( IS_SET(pexit->exit_info, EX_LOCKED) )
	    { send_to_char( "It's already locked.\n\r",    ch ); return; }

	if ( ROOM_HAS_TRIGGER( ch->in_room, RTRIG_PRELOCK ) )
	    if (rp_prelock_trigger( ch->in_room, ch, door )==FALSE)
		return;

	SET_BIT(pexit->exit_info, EX_LOCKED);
	send_to_char( "*Click*\n\r", ch );
	act( "$n locks the $d.", ch, NULL, pexit->keyword, TO_ROOM );

	/* lock the other side */
	if ( ( to_room   = pexit->to_room            ) != NULL
	&&   ( pexit_rev = to_room->exit[rev_dir[door]] ) != 0
	&&   pexit_rev->to_room == ch->in_room )
	{
	    SET_BIT( pexit_rev->exit_info, EX_LOCKED );
	}
	if (ROOM_HAS_TRIGGER(ch->in_room,RTRIG_LOCK))
	    rp_lock_trigger(ch->in_room,ch,door);
    }

    return;
}



void do_unlock( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    OBJ_DATA *obj;
    int door;

    one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Unlock what?\n\r", ch );
	return;
    }

    if ( !IS_DIRECTION(arg) && ( obj = get_obj_here( ch, arg ) ) != NULL )
    {
 	/* portal stuff */
	if (obj->item_type == ITEM_PORTAL)
	{
	    if (!IS_SET(obj->value[1],EX_ISDOOR))
	    {
		send_to_char("You can't do that.\n\r",ch);
		return;
	    }

	    if (!IS_SET(obj->value[1],EX_CLOSED))
	    {
		send_to_char("It's not closed.\n\r",ch);
		return;
	    }

	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_CANUNLOCK ) ) {
		if (op_canunlock_trigger( obj, ch )==FALSE)
		    return;
	    } else {
		// no key found for this door, assume it's magically locked
		if (obj->value[4] <= 0)
		{
		    send_to_char("You can't find the lock.\n\r",ch);
		    return;
		}

		if (!has_key(ch, obj->value[4])) {
		    send_to_char( "You lack the key.\n\r",       ch );
		    return;
		}
	    }

	    if (!IS_SET(obj->value[1],EX_LOCKED)) {
		send_to_char("It's already unlocked.\n\r",ch);
		return;
	    }

	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREUNLOCK ) )
		if (op_preunlock_trigger( obj, ch )==FALSE)
		    return;
	    if (!IS_VALID(obj))
		return;

	    REMOVE_BIT(obj->value[1],EX_LOCKED);
	    act("You unlock $p.",ch,obj,NULL,TO_CHAR);
	    act("$n unlocks $p.",ch,obj,NULL,TO_ROOM);

	    if (OBJ_HAS_TRIGGER(obj,OTRIG_UNLOCK))
		op_unlock_trigger(obj,ch);

	    return;
	}

	/* 'unlock object' */
	if ( obj->item_type != ITEM_CONTAINER )
	    { send_to_char( "That's not a container.\n\r", ch ); return; }
	if ( !IS_SET(obj->value[1], CONT_CLOSED) )
	    { send_to_char( "It's not closed.\n\r",        ch ); return; }
	if ( OBJ_HAS_TRIGGER( obj, OTRIG_CANUNLOCK ) ) {
	    if (op_canunlock_trigger( obj, ch )==FALSE)
		return;
	} else {
	    if ( obj->value[2] < 0 )
		{ send_to_char( "It can't be unlocked.\n\r",   ch ); return; }
	    if ( !has_key( ch, obj->value[2] ) )
		{ send_to_char( "You lack the key.\n\r",       ch ); return; }
	}
	if ( !IS_SET(obj->value[1], CONT_LOCKED) )
	    { send_to_char( "It's already unlocked.\n\r",  ch ); return; }

	if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREUNLOCK ) )
	    if (op_preunlock_trigger( obj, ch )==FALSE)
		return;
	if (!IS_VALID(obj))
	    return;

	REMOVE_BIT(obj->value[1], CONT_LOCKED);
	act("You unlock $p.",ch,obj,NULL,TO_CHAR);
	act( "$n unlocks $p.", ch, obj, NULL, TO_ROOM );

	if (OBJ_HAS_TRIGGER(obj,OTRIG_UNLOCK))
	    op_unlock_trigger(obj,ch);
	return;
    }

    if ( ( door = find_door( ch, arg ) ) >= 0 )
    {
	/* 'unlock door' */
	ROOM_INDEX_DATA *to_room;
	EXIT_DATA *pexit;
	EXIT_DATA *pexit_rev;

	pexit = ch->in_room->exit[door];
	if ( !IS_SET(pexit->exit_info, EX_CLOSED) )
	    { send_to_char( "It's not closed.\n\r",        ch ); return; }
        if ( IS_SET(pexit->exit_info, EX_TCLLOCK) &&
	     ROOM_HAS_TRIGGER( ch->in_room, RTRIG_CANUNLOCK ) ) {
            if (rp_canunlock_trigger( ch->in_room, ch, door )==FALSE)
                return;
        } else {
	    if ( pexit->key <= 0 )
		{ send_to_char( "You can't find the lock.\n\r",ch ); return; }
            if ( !has_key( ch, pexit->key ) )
                { send_to_char( "You lack the key.\n\r",       ch ); return; }
	}
	if ( !IS_SET(pexit->exit_info, EX_LOCKED) )
	    { send_to_char( "It's already unlocked.\n\r",  ch ); return; }

        if ( ROOM_HAS_TRIGGER( ch->in_room, RTRIG_PREUNLOCK ) )
            if (rp_preunlock_trigger( ch->in_room, ch, door )==FALSE)
                return;

	REMOVE_BIT(pexit->exit_info, EX_LOCKED);
	send_to_char( "*Click*\n\r", ch );
	act( "$n unlocks the $d.", ch, NULL, pexit->keyword, TO_ROOM );

	/* unlock the other side */
	if ( ( to_room   = pexit->to_room            ) != NULL
	&&   ( pexit_rev = to_room->exit[rev_dir[door]] ) != NULL
	&&   pexit_rev->to_room == ch->in_room )
	{
	    REMOVE_BIT( pexit_rev->exit_info, EX_LOCKED );
	}
        if (ROOM_HAS_TRIGGER(ch->in_room,RTRIG_UNLOCK))
            rp_lock_trigger(ch->in_room,ch,door);

    }

    return;
}



void do_pick( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *gch;
    OBJ_DATA *obj;
    int door;
    float chance;

    one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Pick what?\n\r", ch );
	return;
    }

    WAIT_STATE( ch, skill_beats(gsn_pick_lock,ch) );

    /* look for guards */
    for ( gch = ch->in_room->people; gch; gch = gch->next_in_room )
    {
	if ( IS_NPC(gch) && IS_AWAKE(gch) && ch->level + 5 < gch->level  &&
	     !IS_AFFECTED2(gch, EFF_IMPINVISIBLE)
	     )
	{
	    act( "$N is standing too close to the lock.",
		ch, NULL, gch, TO_CHAR );
	    return;
	}
    }

    if ( !IS_DIRECTION(arg) && ( obj = get_obj_here( ch, arg ) ) != NULL )
    {
	/* portal stuff */
	if (obj->item_type == ITEM_PORTAL)
	{
	    if (!IS_SET(obj->value[1],EX_ISDOOR))
	    {
		send_to_char("You can't do that.\n\r",ch);
		return;
	    }

	    if (!IS_SET(obj->value[1],EX_CLOSED))
	    {
		send_to_char("It's not closed.\n\r",ch);
		return;
	    }

	    if (obj->value[4] < 0)
	    {
		send_to_char("It can't be unlocked.\n\r",ch);
		return;
	    }

	    chance=100;
	    if (IS_SET(obj->value[1],EX_EASY)) chance=150;
	    if (IS_SET(obj->value[1],EX_HARD)) chance=50;
	    if (IS_SET(obj->value[1],EX_INFURIATING)) chance=25;
	    if ( !IS_NPC(ch) && !skillcheck2(ch,gsn_pick_lock,chance))
	    {
		send_to_char( "You failed.\n\r", ch);
		check_improve(ch,gsn_pick_lock,FALSE,2);
		if(IS_SET(obj->value[1],EX_TRAP))
		    handle_portal_trap(ch,obj);
		return;
    	    }

	    if (IS_SET(obj->value[1],EX_PICKPROOF))
	    {
		send_to_char("You failed.\n\r",ch);
		if(IS_SET(obj->value[1],EX_TRAP))
		    handle_portal_trap(ch,obj);
		return;
	    }

	    if (IS_PC(ch))
		STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);

	    REMOVE_BIT(obj->value[1],EX_LOCKED);
	    act("You pick the lock on $p.",ch,obj,NULL,TO_CHAR);
	    act("$n picks the lock on $p.",ch,obj,NULL,TO_ROOM);
	    check_improve(ch,gsn_pick_lock,TRUE,2);
	    if(IS_SET(obj->value[1],EX_TRAP))
		handle_portal_trap(ch,obj);
	    return;
	}


	/* 'pick object' */
	if ( obj->item_type != ITEM_CONTAINER )
	    { send_to_char( "That's not a container.\n\r", ch ); return; }
	if ( !IS_SET(obj->value[1], CONT_CLOSED) )
	    { send_to_char( "It's not closed.\n\r",        ch ); return; }
	if ( obj->value[2] < 0 )
	    { send_to_char( "It can't be unlocked.\n\r",   ch ); return; }
	if ( !IS_SET(obj->value[1], CONT_LOCKED) )
	    { send_to_char( "It's already unlocked.\n\r",  ch ); return; }

	if ( !IS_NPC(ch) && !skillcheck(ch,gsn_pick_lock))
	{
	    send_to_char( "You failed.\n\r", ch);
	    check_improve(ch,gsn_pick_lock,FALSE,2);
	    if(IS_SET(obj->value[1],CONT_TRAP))
	    	handle_cont_trap(ch,obj);
	    return;
    	}

	if ( IS_SET(obj->value[1], CONT_PICKPROOF) )
	{
	    send_to_char( "You failed.\n\r",             ch );
	    if(IS_SET(obj->value[1],CONT_TRAP))
	    	handle_cont_trap(ch,obj);
	    return;
	}

	if (IS_PC(ch))
	    STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);
	REMOVE_BIT(obj->value[1], CONT_LOCKED);
        act("You pick the lock on $p.",ch,obj,NULL,TO_CHAR);
        act("$n picks the lock on $p.",ch,obj,NULL,TO_ROOM);
	check_improve(ch,gsn_pick_lock,TRUE,2);
	if(IS_SET(obj->value[1],CONT_TRAP))
	    handle_cont_trap(ch,obj);
	return;
    }

    if ( ( door = find_door( ch, arg ) ) >= 0 )
    {
	/* 'pick door' */
	ROOM_INDEX_DATA *to_room;
	EXIT_DATA *pexit;
	EXIT_DATA *pexit_rev;

	pexit = ch->in_room->exit[door];
	if ( !IS_SET(pexit->exit_info, EX_CLOSED) && !IS_IMMORTAL(ch))
	    { send_to_char( "It's not closed.\n\r",        ch ); return; }
	if ( pexit->key < 0 && !IS_IMMORTAL(ch))
	    { send_to_char( "It can't be picked.\n\r",     ch ); return; }
	if ( !IS_SET(pexit->exit_info, EX_LOCKED) )
	    { send_to_char( "It's already unlocked.\n\r",  ch ); return; }
	chance=100;
	if (IS_SET(pexit->exit_info,EX_EASY)) chance=150;
	if (IS_SET(pexit->exit_info,EX_HARD)) chance=50;
	if (IS_SET(pexit->exit_info,EX_INFURIATING)) chance=25;
	if ( !IS_NPC(ch) && !skillcheck2(ch,gsn_pick_lock,chance))
	{
	    send_to_char( "You failed.\n\r", ch);
	    check_improve(ch,gsn_pick_lock,FALSE,2);
	    if(IS_SET(pexit->exit_info,EX_TRAP))
		handle_exit_trap(ch,ch->in_room,door);
	    return;
	}

	if ( IS_SET(pexit->exit_info, EX_PICKPROOF) && !IS_IMMORTAL(ch))
	{
	    send_to_char( "You failed.\n\r",             ch );
	    if(IS_SET(pexit->exit_info,EX_TRAP))
		handle_exit_trap(ch,ch->in_room,door);
	    return;
	}

	REMOVE_BIT(pexit->exit_info, EX_LOCKED);
	send_to_char( "*Click*\n\r", ch );
	act( "$n picks the $d.", ch, NULL, pexit->keyword, TO_ROOM );
	check_improve(ch,gsn_pick_lock,TRUE,2);
        if(IS_SET(pexit->exit_info,EX_TRAP))
	{
	    handle_exit_trap(ch,ch->in_room,door);
	    REMOVE_BIT(pexit->exit_info,EX_TRAP);
	}


	/* pick the other side */
	if ( ( to_room   = pexit->to_room            ) != NULL
	&&   ( pexit_rev = to_room->exit[rev_dir[door]] ) != NULL
	&&   pexit_rev->to_room == ch->in_room )
	{
	    REMOVE_BIT( pexit_rev->exit_info, EX_LOCKED );
	}
    }

    return;
}

void do_removetrap( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *gch;
    OBJ_DATA *obj;
    int door;

    one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Remove what trap?\n\r", ch );
	return;
    }

    WAIT_STATE( ch, skill_beats(gsn_removetrap,ch) );

    /* look for guards */
    for ( gch = ch->in_room->people; gch; gch = gch->next_in_room )
    {
	if ( IS_NPC(gch) && IS_AWAKE(gch) && ch->level + 5 < gch->level
	  && !IS_AFFECTED2(gch,EFF_IMPINVISIBLE))
	{
	    act( "$N is standing too close.",
		ch, NULL, gch, TO_CHAR );
	    return;
	}
    }

    if ( !IS_DIRECTION(arg) && ( obj = get_obj_here( ch, arg ) ) != NULL )
    {
	/* portal stuff */
	if (obj->item_type == ITEM_PORTAL)
	{
	    if (!IS_SET(obj->value[1],EX_TRAP))
	    {
		send_to_char("You can't do that.\n\r",ch);
		return;
	    }

	    if ( !IS_NPC(ch) && !skillcheck2(ch,gsn_removetrap,100+ch->level-obj->level)) {
		send_to_char( "You failed.\n\r", ch);
		check_improve(ch,gsn_removetrap,FALSE,2);
		if(IS_SET(obj->value[1],EX_TRAP))
		    handle_portal_trap(ch,obj);
		return;
    	    }

	    if (IS_PC(ch))
		STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);
	    REMOVE_BIT(obj->value[1],EX_TRAP);
	    act("You remove the trap from the $p.",ch,obj,NULL,TO_CHAR);
	    act("$n removes the trap from the $p.",ch,obj,NULL,TO_ROOM);
	    check_improve(ch,gsn_removetrap,TRUE,2);
	    return;
	}


	/* 'pick object' */
	if ( obj->item_type != ITEM_CONTAINER )
	    { send_to_char( "That's not a container.\n\r", ch ); return; }
	if ( !IS_SET(obj->value[1], CONT_TRAP) )
	    { send_to_char( "You failed.\n\r",        ch ); return; }

	if ( !IS_NPC(ch) && !skillcheck2(ch,gsn_removetrap,100+ch->level-obj->level)) {
	    send_to_char( "You failed.\n\r", ch);
	    check_improve(ch,gsn_removetrap,FALSE,2);
	    if(IS_SET(obj->value[1],CONT_TRAP))
	    	handle_cont_trap(ch,obj);
	    return;
    	}

	if (IS_PC(ch))
	    STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);
	REMOVE_BIT(obj->value[1], CONT_TRAP);
        act("You remove the trap from the $p.",ch,obj,NULL,TO_CHAR);
        act("$n removes the trap from the $p.",ch,obj,NULL,TO_ROOM);
	check_improve(ch,gsn_removetrap,TRUE,2);
	return;
    }

    if ( ( door = find_door( ch, arg ) ) >= 0 )
    {
	/* 'pick door' */
	EXIT_DATA *pexit;

	pexit = ch->in_room->exit[door];
	if ( !IS_SET(pexit->exit_info, EX_TRAP) && !IS_IMMORTAL(ch))
	    { send_to_char( "You failed.\n\r",        ch ); return; }


	if ( !IS_NPC(ch) && !skillcheck2(ch,gsn_removetrap,100+ch->level-ch->in_room->level)) {
	    send_to_char( "You failed.\n\r", ch);
	    check_improve(ch,gsn_removetrap,FALSE,2);
	    if(IS_SET(pexit->exit_info,EX_TRAP))
		handle_exit_trap(ch,ch->in_room,door);
	    return;
	}

	REMOVE_BIT(pexit->exit_info, EX_TRAP);
	act( "You remove the trap from the $d.", ch, NULL, pexit->keyword, TO_CHAR);
	act( "$n removes the trap from the $d.", ch, NULL, pexit->keyword, TO_ROOM );
	check_improve(ch,gsn_removetrap,TRUE,2);

	/* The trap is still on the other side grin */
    }

    return;
}

void do_findtrap( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *gch;
    OBJ_DATA *obj;
    int door;
    int traplevel;
    int trapflags;
    char *trapname;
    bool trapexitflag;
    int penalty;

    one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Find trap on what?\n\r", ch );
	return;
    }

    WAIT_STATE( ch, skill_beats(gsn_findtrap,ch) );

    /* look for guards */
    for ( gch = ch->in_room->people; gch; gch = gch->next_in_room )
    {
	if ( IS_NPC(gch) && IS_AWAKE(gch) && ch->level + 5 < gch->level
	  && !IS_AFFECTED2(gch,EFF_IMPINVISIBLE))
	{
	    act( "$N is standing too close.",
		ch, NULL, gch, TO_CHAR );
	    return;
	}
    }

    if ( !IS_DIRECTION(arg) && ( obj = get_obj_here( ch, arg ) ) != NULL )
    {
        trapname=obj->short_descr;
	traplevel=obj->level;
	trapflags=obj->value[1];

	if (obj->item_type == ITEM_PORTAL) trapexitflag=TRUE;
	else if(obj->item_type == ITEM_CONTAINER) trapexitflag=FALSE;
	else
	{
	    send_to_char( "That's not a container.\n\r", ch );
	    return;
	}
	if (IS_PC(ch))
	    STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);
    }
    else if ( ( door = find_door( ch, arg ) ) >= 0 )
    {
	/* 'pick door' */
	EXIT_DATA *pexit;

	pexit = ch->in_room->exit[door];
	trapname=pexit->keyword;
	trapflags=pexit->exit_info;
	traplevel=ch->in_room->level;
	trapexitflag=TRUE;
    }
    else
    {
	send_to_char("You can't find it.\n\r",ch);
	return;
    }

    if (trapname || trapname[0]==0) 
	trapname="door";

    /* Do the find trap stuff */
    if (traplevel<ch->level) penalty=100; else penalty=100+ch->level-traplevel;

    if(skillcheck2(ch,gsn_findtrap,penalty)) {
        if(skillcheck2(ch,gsn_findtrap,66)) {
	    /* Je weet niet of er een val op zit. */
	    act("You are not sure if there's a trap on the $T.",ch,NULL,trapname,TO_CHAR);
	    check_improve(ch,gsn_findtrap,FALSE,1);
	} else {
	    /* Er zit nooit een val op... Denkt ie. */
	    act("There is no trap on the $T.",ch,NULL,trapname,TO_CHAR);
	}
    } else {
	int transflags=0;
	int num=0;

	check_improve(ch,gsn_findtrap,TRUE,1);

	if(trapexitflag) transflags=trapflags;
	else
	{
	    if( IS_SET( trapflags , CONT_TRAP) ) SET_BIT( transflags , EX_TRAP );
	    if( IS_SET( trapflags , CONT_TRAP_DARTS) ) SET_BIT( transflags , EX_TRAP_DARTS );
	    if( IS_SET( trapflags , CONT_TRAP_POISON) ) SET_BIT( transflags , EX_TRAP_POISON );
	    if( IS_SET( trapflags , CONT_TRAP_EXPLODING) ) SET_BIT( transflags , EX_TRAP_EXPLODING );
	    if( IS_SET( trapflags , CONT_TRAP_SLEEPGAS) ) SET_BIT( transflags , EX_TRAP_SLEEPGAS );
	    if( IS_SET( trapflags , CONT_TRAP_DEATH) ) SET_BIT( transflags , EX_TRAP_DEATH );
	}

	if(!IS_SET(transflags,EX_TRAP))
	{
	    act("There is no trap on the $T.",ch,NULL,trapname,TO_CHAR);
	    return;
	}

	act("There is a trap on the $T.",ch,NULL,trapname,TO_CHAR);
	if((IS_IMMORTAL(ch) || traplevel<ch->level+5) && IS_SET(transflags,EX_TRAP_DARTS))
	{
	    send_to_char("You see some small holes.\n\r", ch );
	    num++;
	}
	if((IS_IMMORTAL(ch) || traplevel<ch->level+4) && IS_SET(transflags,EX_TRAP_EXPLODING))
	{
	    send_to_char("You see an exploding charge.\n\r", ch );
	    num++;
	}
	if((IS_IMMORTAL(ch) || traplevel<ch->level+10) && IS_SET(transflags,EX_TRAP_POISON))
	{
	    send_to_char("You see a pocket with poison.\n\r", ch );
	    num++;
	}
	if((IS_IMMORTAL(ch) || traplevel<ch->level+14) && IS_SET(transflags,EX_TRAP_SLEEPGAS))
	{
	    send_to_char("You see a vial with some gas in it.\n\r", ch );
	    num++;
	}
	if((IS_IMMORTAL(ch) || traplevel<ch->level+1) && IS_SET(transflags,EX_TRAP_SLEEPGAS))
	{
	    send_to_char("It looks like a very deadly trap!\n\r", ch );
	    num++;
	}
	if(num==0)
	{
	    send_to_char("You look closer at the trap but you see nothing special.\n\r",ch);
	}
    }

    return;
}

void do_stand( CHAR_DATA *ch, char *argument )
{
    OBJ_DATA *obj = NULL;

    if (argument[0] != '\0')
    {
	if (ch->position == POS_FIGHTING)
	{
	    send_to_char("Maybe you should finish fighting first?\n\r",ch);
	    return;
	}
	obj = get_obj_list(ch,argument,ch->in_room->contents);
	if (obj == NULL)
	{
	    send_to_char("You don't see that here.\n\r",ch);
	    return;
	}
	if ((obj->item_type != ITEM_FURNITURE && obj->item_type != ITEM_BOAT)
	||  (!IS_SET(obj->value[2],STAND_AT)
	&&   !IS_SET(obj->value[2],STAND_ON)
	&&   !IS_SET(obj->value[2],STAND_IN)))
	{
	    send_to_char("You can't seem to find a place to stand.\n\r",ch);
	    return;
	}
	if (ch->on != obj && count_users(obj) >= obj->value[0])
	{
	    act_new("There's no room to stand on $p.",
		ch,obj,NULL,TO_CHAR,POS_DEAD);
	    return;
	}
    } else if (ch->on) {
	if (ch->on->item_type==ITEM_BOAT &&
	    (ch->in_room->sector_type==SECT_WATER_SWIM||
	     ch->in_room->sector_type==SECT_WATER_NOSWIM)) {
	    obj=ch->on;
	}
    }

    if (IS_PC(ch) && obj)
	STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);
    switch ( ch->position )
    {
    case POS_SLEEPING:
	if ( IS_AFFECTED(ch, EFF_SLEEP) ) {
	    send_to_char( "You can't wake up!\n\r", ch );
	    return;
	}

	if (obj == NULL) {
	    send_to_char( "You wake and stand up.\n\r", ch );
	    act( "$n wakes and stands up.", ch, NULL, NULL, TO_ROOM );
	    ch->on = NULL;
	}
	else if (IS_SET(obj->value[2],STAND_AT)) {
	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRESTAND ) )
		if (op_prestand_trigger( obj, ch )==FALSE)
		    return;
	    if (!IS_VALID(obj))
		return;
	    act_new("You wake and stand at $p.",ch,obj,NULL,TO_CHAR,POS_DEAD);
	    act("$n wakes and stands at $p.",ch,obj,NULL,TO_ROOM);
	}
	else if (IS_SET(obj->value[2],STAND_ON))
	{
	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRESTAND ) )
		if (op_prestand_trigger( obj, ch )==FALSE)
		    return;
	    if (!IS_VALID(obj))
		return;
	    act_new("You wake and stand on $p.",ch,obj,NULL,TO_CHAR,POS_DEAD);
	    act("$n wakes and stands on $p.",ch,obj,NULL,TO_ROOM);
	}
	else
	{
	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRESTAND ) )
		if (op_prestand_trigger( obj, ch )==FALSE)
		    return;
	    if (!IS_VALID(obj))
		return;
	    act_new("You wake and stand in $p.",ch,obj,NULL,TO_CHAR,POS_DEAD);
	    act("$n wakes and stands in $p.",ch,obj,NULL,TO_ROOM);
	}
	ch->position = POS_STANDING;
	if (obj && OBJ_HAS_TRIGGER(obj,OTRIG_STAND))
	    op_stand_trigger(obj,ch);
	look_room(ch,ch->in_room,TRUE);
	break;

    case POS_STANDING:
	if (obj == ch->on) {
	    send_to_char( "You are already standing.\n\r", ch );
	    break;
	}
	/* Note: fallthrough! */

    case POS_RESTING:
    case POS_SITTING:
	if (obj == NULL)
	{
	    send_to_char( "You stand up.\n\r", ch );
	    act( "$n stands up.", ch, NULL, NULL, TO_ROOM );
	    ch->on = NULL;
	}
	else if (IS_SET(obj->value[2],STAND_AT))
	{
	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRESTAND ) )
		if (op_prestand_trigger( obj, ch )==FALSE)
		    return;
	    if (!IS_VALID(obj))
		return;
	    act("You stand at $p.",ch,obj,NULL,TO_CHAR);
	    act("$n stands at $p.",ch,obj,NULL,TO_ROOM);
	}
	else if (IS_SET(obj->value[2],STAND_ON))
	{
	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRESTAND ) )
		if (op_prestand_trigger( obj, ch )==FALSE)
		    return;
	    if (!IS_VALID(obj))
		return;
	    act("You stand on $p.",ch,obj,NULL,TO_CHAR);
	    act("$n stands on $p.",ch,obj,NULL,TO_ROOM);
	}
	else
	{
	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRESTAND ) )
		if (op_prestand_trigger( obj, ch )==FALSE)
		    return;
	    if (!IS_VALID(obj))
		return;
	    act("You stand in $p.",ch,obj,NULL,TO_CHAR);
	    act("$n stands on $p.",ch,obj,NULL,TO_ROOM);
	}
	ch->position = POS_STANDING;
	if (obj && OBJ_HAS_TRIGGER(obj,OTRIG_STAND)) {
	    op_stand_trigger(obj,ch);
	    if (!IS_VALID(obj))
		return;
	}
	break;

    case POS_FIGHTING:
	send_to_char( "You are already fighting!\n\r", ch );
	break;
    }
    if (ch->position == POS_STANDING)
 	ch->on = obj;

    /* if player is going to wake and can see player, pet will also stand up */
    if ((ch->position==POS_STANDING) && ch->pet
     && (ch->pet->in_room==ch->in_room)) {
	CHAR_DATA *pet=ch->pet;
	if (pet->position!=POS_SLEEPING)
	    do_function(pet,&do_wake,"");
    }

    return;
}



void do_rest( CHAR_DATA *ch, char *argument )
{
    OBJ_DATA *obj = NULL;

    if (ch->position == POS_FIGHTING)
    {
	send_to_char("You are already fighting!\n\r",ch);
	return;
    }

    /* okay, now that we know we can rest, find an object to rest on */
    if (argument[0] != '\0')
    {
	obj = get_obj_list(ch,argument,ch->in_room->contents);
	if (obj == NULL)
	{
	    send_to_char("You don't see that here.\n\r",ch);
	    return;
	}
    }
    else obj = ch->on;

    rest_obj(ch,obj,FALSE);

    /* if player is going to rest, pet will also sit */
    if ((ch->position==POS_RESTING) && ch->pet
     && (ch->pet->in_room==ch->in_room)) {
	CHAR_DATA *pet=ch->pet;
	if (pet->position==POS_STANDING)
	    do_function(pet,&do_sit,"");
    }

}

void rest_obj( CHAR_DATA *ch, OBJ_DATA *obj, int quiet) {
    if (obj != NULL)
    {
        if (obj->item_type != ITEM_FURNITURE
    	||  (!IS_SET(obj->value[2],REST_ON)
    	&&   !IS_SET(obj->value[2],REST_IN)
    	&&   !IS_SET(obj->value[2],REST_AT)))
    	{
	    if (!quiet) send_to_char("You can't rest on that.\n\r",ch);
	    return;
    	}

        if (obj != NULL && ch->on != obj && count_users(obj) >= obj->value[0])
        {
	    if (!quiet) act_new("There's no more room on $p.",ch,obj,NULL,TO_CHAR,POS_DEAD);
	    return;
    	}
    }

    if (IS_PC(ch) && obj)
	STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);

    switch ( ch->position )
    {
    case POS_SLEEPING:
	if (IS_AFFECTED(ch,EFF_SLEEP))
	{
	    if (!quiet) send_to_char("You can't wake up!\n\r",ch);
	    return;
	}

	if (obj == NULL)
	{
	    if (!quiet) {
		send_to_char( "You wake up and start resting.\n\r", ch );
		act ("$n wakes up and starts resting.",ch,NULL,NULL,TO_ROOM);
	    }
	}
	else if (IS_SET(obj->value[2],REST_AT))
	{
	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREREST ) )
		if (op_prerest_trigger( obj, ch )==FALSE)
		    return;
	    if (!IS_VALID(obj))
		return;
	    if (!quiet) {
		act_new("You wake up and rest at $p.",
			ch,obj,NULL,TO_CHAR,POS_SLEEPING);
		act("$n wakes up and rests at $p.",ch,obj,NULL,TO_ROOM);
	    }
	}
        else if (IS_SET(obj->value[2],REST_ON))
        {
	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREREST ) )
		if (op_prerest_trigger( obj, ch )==FALSE)
		    return;
	    if (!IS_VALID(obj))
		return;
	    if (!quiet) {
		act_new("You wake up and rest on $p.",
			ch,obj,NULL,TO_CHAR,POS_SLEEPING);
		act("$n wakes up and rests on $p.",ch,obj,NULL,TO_ROOM);
	    }
        }
        else
        {
	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREREST ) )
		if (op_prerest_trigger( obj, ch )==FALSE)
		    return;
	    if (!IS_VALID(obj))
		return;
	    if (!quiet) {
		act_new("You wake up and rest in $p.",
			ch,obj,NULL,TO_CHAR,POS_SLEEPING);
		act("$n wakes up and rests in $p.",ch,obj,NULL,TO_ROOM);
	    }
        }
	ch->position = POS_RESTING;
	if (obj && OBJ_HAS_TRIGGER(obj,OTRIG_REST)) {
	    op_rest_trigger(obj,ch);
	    if (!IS_VALID(obj))
		return;
	}
	break;

    case POS_STANDING:
	if (obj == NULL)
	{
	    if (!quiet) {
		send_to_char( "You rest.\n\r", ch );
		act( "$n sits down and rests.", ch, NULL, NULL, TO_ROOM );
	    }
	}
        else if (IS_SET(obj->value[2],REST_AT))
        {
	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREREST ) )
		if (op_prerest_trigger( obj, ch )==FALSE)
		    return;
	    if (!IS_VALID(obj))
		return;
	    if (!quiet) {
		act("You sit down at $p and rest.",ch,obj,NULL,TO_CHAR);
		act("$n sits down at $p and rests.",ch,obj,NULL,TO_ROOM);
	    }
        }
        else if (IS_SET(obj->value[2],REST_ON))
        {
	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREREST ) )
		if (op_prerest_trigger( obj, ch )==FALSE)
		    return;
	    if (!IS_VALID(obj))
		return;
	    if (!quiet) {
		act("You sit on $p and rest.",ch,obj,NULL,TO_CHAR);
		act("$n sits on $p and rests.",ch,obj,NULL,TO_ROOM);
	    }
        }
        else
        {
	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREREST ) )
		if (op_prerest_trigger( obj, ch )==FALSE)
		    return;
	    if (!IS_VALID(obj))
		return;
	    if (!quiet) {
		act("You rest in $p.",ch,obj,NULL,TO_CHAR);
		act("$n rests in $p.",ch,obj,NULL,TO_ROOM);
	    }
        }
	ch->position = POS_RESTING;
	if (obj && OBJ_HAS_TRIGGER(obj,OTRIG_REST)) {
	    op_rest_trigger(obj,ch);
	    if (!IS_VALID(obj))
		return;
	}
	break;

    case POS_RESTING:
	if (obj == ch->on) {
	    if (!quiet) send_to_char( "You are already resting.\n\r", ch );
	    break;
	}
	/* Note: fallthrough! */

    case POS_SITTING:
	if (obj == NULL)
	{
	    if (!quiet) {
		send_to_char("You rest.\n\r",ch);
		act("$n rests.",ch,NULL,NULL,TO_ROOM);
	    }
	}
        else if (IS_SET(obj->value[2],REST_AT))
        {
	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREREST ) )
		if (op_prerest_trigger( obj, ch )==FALSE)
		    return;
	    if (!IS_VALID(obj))
		return;
	    if (!quiet) {
		act("You rest at $p.",ch,obj,NULL,TO_CHAR);
		act("$n rests at $p.",ch,obj,NULL,TO_ROOM);
	    }
        }
        else if (IS_SET(obj->value[2],REST_ON))
        {
	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREREST ) )
		if (op_prerest_trigger( obj, ch )==FALSE)
		    return;
	    if (!IS_VALID(obj))
		return;
	    if (!quiet) {
		act("You rest on $p.",ch,obj,NULL,TO_CHAR);
		act("$n rests on $p.",ch,obj,NULL,TO_ROOM);
	    }
        }
        else
        {
	    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREREST ) )
		if (op_prerest_trigger( obj, ch )==FALSE)
		    return;
	    if (!IS_VALID(obj))
		return;
	    if (!quiet) {
		act("You rest in $p.",ch,obj,NULL,TO_CHAR);
		act("$n rests in $p.",ch,obj,NULL,TO_ROOM);
	    }
	}
	ch->position = POS_RESTING;
	if (obj && OBJ_HAS_TRIGGER(obj,OTRIG_REST)) {
	    op_rest_trigger(obj,ch);
	    if (!IS_VALID(obj))
		return;
	}
	break;
    }
    if (ch->position == POS_RESTING)
	ch->on = obj;

    return;
}


void do_sit (CHAR_DATA *ch, char *argument )
{
    OBJ_DATA *obj = NULL;

    if (ch->position == POS_FIGHTING)
    {
	send_to_char("Maybe you should finish this fight first?\n\r",ch);
	return;
    }

    /* okay, now that we know we can sit, find an object to sit on */
    if (argument[0] != '\0')
    {
	obj = get_obj_list(ch,argument,ch->in_room->contents);
	if (obj == NULL)
	{
	    send_to_char("You don't see that here.\n\r",ch);
	    return;
	}
    }
    else obj = ch->on;

    sit_obj(ch,obj,FALSE);

    /* if player is going to sit, pet will also sit */
    if ((ch->position==POS_SITTING) && ch->pet
     && (ch->pet->in_room==ch->in_room)) {
	CHAR_DATA *pet=ch->pet;
	if (pet->position==POS_STANDING) {
	    if (obj && obj->item_type==ITEM_BOAT)
		do_function(pet,&do_sit,argument);
	    else
		do_function(pet,&do_sit,"");
	}
    }

}

void sit_obj(CHAR_DATA *ch, OBJ_DATA *obj, int quiet) {

    if (obj != NULL) {
	if ((obj->item_type != ITEM_FURNITURE && obj->item_type!=ITEM_BOAT)
	||  (!IS_SET(obj->value[2],SIT_ON)
	&&   !IS_SET(obj->value[2],SIT_IN)
	&&   !IS_SET(obj->value[2],SIT_AT)))
	{
	    if (!quiet) send_to_char("You can't sit on that.\n\r",ch);
	    return;
	}

	if (obj != NULL && ch->on != obj && count_users(obj) >= obj->value[0])
	{
	    if (!quiet) act_new("There's no more room on $p.",ch,obj,NULL,TO_CHAR,POS_DEAD);
	    return;
	}
    }

    if (IS_PC(ch) && obj)
	STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);

    switch (ch->position)
    {
	case POS_SLEEPING:
	    if (IS_AFFECTED(ch,EFF_SLEEP))
	    {
		if (!quiet) send_to_char("You can't wake up!\n\r",ch);
		return;
	    }

            if (obj == NULL) {
		if (!quiet) {
		    send_to_char( "You wake and sit up.\n\r", ch );
		    act( "$n wakes and sits up.", ch, NULL, NULL, TO_ROOM );
		}
            }
            else if (IS_SET(obj->value[2],SIT_AT))
            {
		if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRESIT ) )
		    if (op_presit_trigger( obj, ch )==FALSE)
			return;
		if (!IS_VALID(obj))
		    return;
		if (!quiet) {
		    act_new("You wake and sit at $p.",ch,obj,NULL,TO_CHAR,POS_DEAD);
		    act("$n wakes and sits at $p.",ch,obj,NULL,TO_ROOM);
		}
            }
            else if (IS_SET(obj->value[2],SIT_ON))
            {
		if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRESIT ) )
		    if (op_presit_trigger( obj, ch )==FALSE)
			return;
		if (!IS_VALID(obj))
		    return;
		if (!quiet) {
		    act_new("You wake and sit on $p.",ch,obj,NULL,TO_CHAR,POS_DEAD);
		    act("$n wakes and sits at $p.",ch,obj,NULL,TO_ROOM);
		}
            }
            else
            {
		if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRESIT ) )
		    if (op_presit_trigger( obj, ch )==FALSE)
			return;
		if (!IS_VALID(obj))
		    return;
		if (!quiet) {
		    act_new("You wake and sit in $p.",ch,obj,NULL,TO_CHAR,POS_DEAD);
		    act("$n wakes and sits in $p.",ch,obj,NULL,TO_ROOM);
		}
            }

	    ch->position = POS_SITTING;
	    if (obj && OBJ_HAS_TRIGGER(obj,OTRIG_SIT)) {
		op_sit_trigger(obj,ch);
		if (!IS_VALID(obj))
		    return;
	    }
	    break;
	case POS_RESTING:
	    if (obj == NULL) {
		if (!quiet) send_to_char("You stop resting.\n\r",ch);
	    } else if (IS_SET(obj->value[2],SIT_AT)) {
		if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRESIT ) )
		    if (op_presit_trigger( obj, ch )==FALSE)
			return;
		if (!IS_VALID(obj))
		    return;
		if (!quiet) {
		    act("You sit at $p.",ch,obj,NULL,TO_CHAR);
		    act("$n sits at $p.",ch,obj,NULL,TO_ROOM);
		}
	    }

	    else if (IS_SET(obj->value[2],SIT_ON))
	    {
		if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRESIT ) )
		    if (op_presit_trigger( obj, ch )==FALSE)
			return;
		if (!IS_VALID(obj))
		    return;
		if (!quiet) {
		    act("You sit on $p.",ch,obj,NULL,TO_CHAR);
		    act("$n sits on $p.",ch,obj,NULL,TO_ROOM);
		}
	    }
	    ch->position = POS_SITTING;
	    if (obj && OBJ_HAS_TRIGGER(obj,OTRIG_SIT)) {
		op_sit_trigger(obj,ch);
		if (!IS_VALID(obj))
		    return;
	    }
	    break;
	case POS_SITTING:
	    if (obj == ch->on) {
		if (!quiet) send_to_char("You are already sitting down.\n\r",ch);
		break;
	    }
	    /* Note: fallthrough! */
	case POS_STANDING:
	    if (obj == NULL)
    	    {
		if (!quiet) {
		  send_to_char("You sit down on the ground.\n\r",ch);
		  act("$n sits down on the ground.",ch,NULL,NULL,TO_ROOM);
		}
	    }
	    else if (IS_SET(obj->value[2],SIT_AT))
	    {
		if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRESIT ) )
		    if (op_presit_trigger( obj, ch )==FALSE)
			return;
		if (!IS_VALID(obj))
		    return;
		if (!quiet) {
		    act("You sit down at $p.",ch,obj,NULL,TO_CHAR);
		    act("$n sits down at $p.",ch,obj,NULL,TO_ROOM);
		}
	    }
	    else if (IS_SET(obj->value[2],SIT_ON))
	    {
		if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRESIT ) )
		    if (op_presit_trigger( obj, ch )==FALSE)
			return;
		if (!IS_VALID(obj))
		    return;
		if (!quiet) {
		    act("You sit on $p.",ch,obj,NULL,TO_CHAR);
		    act("$n sits on $p.",ch,obj,NULL,TO_ROOM);
		}
	    }
	    else
	    {
		if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRESIT ) )
		    if (op_presit_trigger( obj, ch )==FALSE)
			return;
		if (!IS_VALID(obj))
		    return;
		if (!quiet) {
		    act("You sit down in $p.",ch,obj,NULL,TO_CHAR);
		    act("$n sits down in $p.",ch,obj,NULL,TO_ROOM);
		}
	    }
    	    ch->position = POS_SITTING;
	    if (obj && OBJ_HAS_TRIGGER(obj,OTRIG_SIT)) {
		op_sit_trigger(obj,ch);
		if (!IS_VALID(obj))
		    return;
	    }

    	    break;
    }
    if (ch->position == POS_SITTING)
	ch->on = obj;

    return;
}


void do_sleep( CHAR_DATA *ch, char *argument )
{
    OBJ_DATA *obj = NULL;

    if (ch->position == POS_FIGHTING)
    {
	send_to_char("Maybe you should finish this fight first?\n\r",ch);
	return;
    }

    if (IS_PC(ch) && obj)
	STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);

    switch ( ch->position )
    {
    case POS_SLEEPING:
	send_to_char( "You are already sleeping.\n\r", ch );
	break;

    case POS_RESTING:
    case POS_SITTING:
    case POS_STANDING:
	if (!IS_NPC(ch) && ch->pcdata->condition[COND_ADRENALINE]>0) {
	    send_to_char(
		"The excitement of your previous fight still flashes through your head.\n\r"
		"You can't seem to get yourself enough rest.\n\r",ch);
	    break;
	} else if (argument[0] == '\0' && ch->on == NULL)
	{
	    send_to_char( "You go to sleep.\n\r", ch );
	    act( "$n goes to sleep.", ch, NULL, NULL, TO_ROOM );
	    ch->position = POS_SLEEPING;
	}
	else  /* find an object and sleep on it */
	{
	    if (argument[0] == '\0')
		obj = ch->on;
	    else
	    	obj = get_obj_list( ch, argument,  ch->in_room->contents );

	    if (obj == NULL)
	    {
		send_to_char("You don't see that here.\n\r",ch);
		return;
	    }
	    if (obj->item_type != ITEM_FURNITURE
	    ||  (!IS_SET(obj->value[2],SLEEP_ON)
	    &&   !IS_SET(obj->value[2],SLEEP_IN)
	    &&	 !IS_SET(obj->value[2],SLEEP_AT)))
	    {
		send_to_char("You can't sleep on that!\n\r",ch);
		return;
	    }

	    if (ch->on != obj && count_users(obj) >= obj->value[0])
	    {
		act_new("There is no room on $p for you.",
		    ch,obj,NULL,TO_CHAR,POS_DEAD);
		return;
	    }

	    ch->on = obj;
	    if (IS_SET(obj->value[2],SLEEP_AT))
	    {
		if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRESLEEP ) )
		    if (op_presleep_trigger( obj, ch )==FALSE)
			return;
		if (!IS_VALID(obj))
		    return;
		act("You go to sleep at $p.",ch,obj,NULL,TO_CHAR);
		act("$n goes to sleep at $p.",ch,obj,NULL,TO_ROOM);
	    }
	    else if (IS_SET(obj->value[2],SLEEP_ON))
	    {
		if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRESLEEP ) )
		    if (op_presleep_trigger( obj, ch )==FALSE)
			return;
		if (!IS_VALID(obj))
		    return;
	        act("You go to sleep on $p.",ch,obj,NULL,TO_CHAR);
	        act("$n goes to sleep on $p.",ch,obj,NULL,TO_ROOM);
	    }
	    else
	    {
		if ( OBJ_HAS_TRIGGER( obj, OTRIG_PRESLEEP ) )
		    if (op_presleep_trigger( obj, ch )==FALSE)
			return;
		if (!IS_VALID(obj))
		    return;
		act("You go to sleep in $p.",ch,obj,NULL,TO_CHAR);
		act("$n goes to sleep in $p.",ch,obj,NULL,TO_ROOM);
	    }
	    ch->position = POS_SLEEPING;
	    if (obj && OBJ_HAS_TRIGGER(obj,OTRIG_SLEEP)) {
		op_sleep_trigger(obj,ch);
		if (!IS_VALID(obj))
		    return;
	    }
	}
	break;

    }

    /* if player is going to sleep, pet will also rest */
    if ((ch->position==POS_SLEEPING) && ch->pet
     && (ch->pet->in_room==ch->in_room)) {
	CHAR_DATA *pet=ch->pet;
	if (pet->position==POS_STANDING || pet->position==POS_SITTING)
	    do_function(pet,&do_rest,"");
    }

    return;
}



void do_wake( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;

    one_argument( argument, arg );
    if ( arg[0] == '\0' )
	{ do_function(ch, &do_stand, ""); return; }

    if ( !IS_AWAKE(ch) )
	{ send_to_char( "You are asleep yourself!\n\r",       ch ); return; }

    if ( ( victim = get_char_room( ch, arg ) ) == NULL )
	{ send_to_char( "They aren't here.\n\r",              ch ); return; }

    if ( IS_AWAKE(victim) )
	{ act( "$N is already awake.", ch, NULL, victim, TO_CHAR ); return; }

    if ( IS_AFFECTED(victim, EFF_SLEEP) )
	{ act( "You can't wake $M!",   ch, NULL, victim, TO_CHAR );  return; }

    do_function(victim, &do_stand, "");
    act_new( "$n wakes you.", ch, NULL, victim, TO_VICT,POS_SLEEPING );
    return;
}



void do_sneak( CHAR_DATA *ch, char *argument )
{
    EFFECT_DATA ef;

    send_to_char( "You attempt to move silently.\n\r", ch );
    effect_strip( ch, gsn_sneak );

    if (IS_AFFECTED(ch,EFF_SNEAK))
	return;

    if (skillcheck(ch,gsn_sneak)) {
	check_improve(ch,gsn_sneak,TRUE,3);
	ZEROVAR(&ef,EFFECT_DATA);
	ef.where     = TO_EFFECTS;
	ef.type      = gsn_sneak;
	ef.level     = ch->level;
	ef.duration  = ch->level;
	ef.location  = APPLY_NONE;
	ef.modifier  = 0;
	ef.bitvector =EFF_SNEAK;
	effect_to_char( ch, &ef );
    } else
	check_improve(ch,gsn_sneak,FALSE,3);

    return;
}



// something comparable with do_visible except for the
// sneak-ability only
void do_stealth( CHAR_DATA *ch, char *argument) {
    struct race_type *race;

    if (IS_NPC(ch))
	return;

    race=&race_table[ch->race];

    // if the race has sneak on by default either enable or disable it
    if (STR_IS_SET(race->strbit_eff,EFF_SNEAK)) {
	if (STR_IS_SET(ch->strbit_affected_by,EFF_SNEAK)) {
	    STR_REMOVE_BIT(ch->strbit_affected_by,EFF_SNEAK);
	    send_to_char("You are no longer sneaking.\n\r",ch);
	} else {
	    STR_SET_BIT(ch->strbit_affected_by,EFF_SNEAK);
	    send_to_char("You are moving silently.\n\r",ch);
	}
    } else {
	if (STR_IS_SET(ch->strbit_affected_by,EFF_SNEAK)) {
	    do_function(ch,&do_visible,"");
	} else {
	    do_function(ch,&do_sneak,"");
	}
    }
}



void do_hide( CHAR_DATA *ch, char *argument )
{
    send_to_char( "You attempt to hide.\n\r", ch );

    if ( IS_AFFECTED(ch, EFF_HIDE) )
	STR_REMOVE_BIT(ch->strbit_affected_by, EFF_HIDE);

    effect_check(ch,TO_EFFECTS,EFF_HIDE);
    if( IS_AFFECTED( ch, EFF_HIDE ) ) return;	/* Is permenent affect */

    if (skillcheck(ch,gsn_hide)) {
	STR_SET_BIT(ch->strbit_affected_by, EFF_HIDE);
	check_improve(ch,gsn_hide,TRUE,3);
    }
    else
	check_improve(ch,gsn_hide,FALSE,3);

    return;
}



/*
 * Contributed by Alander.
 */
void do_visible( CHAR_DATA *ch, char *argument ) {
    effect_strip ( ch, gsn_invisibility			);
    effect_strip ( ch, gsn_mass_invis			);
    effect_strip ( ch, gsn_sneak			);
    STR_REMOVE_BIT   ( ch->strbit_affected_by, EFF_HIDE	);
    effect_check ( ch, TO_EFFECTS, EFF_HIDE		);
    send_to_char( "Ok.\n\r", ch );
    return;
}



void event_recall(EVENT_DATA *event) {
    CHAR_DATA *		ch=event->ch;
    int	*		pvnum=(int *)event->data;
    ROOM_INDEX_DATA *	location=get_room_index(*pvnum);
    ROOM_INDEX_DATA *	old_room=ch->in_room;

    if (!IS_VALID(ch))
	return;

    if ( ch->fighting != NULL ) {
	int lose;

	lose = (ch->desc != NULL) ? 25 : 50;
	gain_exp( ch, 0 - lose, TRUE );
	check_improve(ch,gsn_recall,TRUE,4);
	sprintf_to_char(ch,
	    "You recall from combat! You lose %d exps.\n\r",lose);
	stop_fighting( ch, TRUE );
    }

    ch->move /= 2;
    send_to_char("You suddenly are in a different room.\n\r",ch);
    act( "$n disappears.", ch, NULL, NULL, TO_ROOM );

    if ( IS_PC( ch ) ) {
	if (ROOM_HAS_TRIGGER(ch->in_room,RTRIG_RECALL))
	    rp_recall_trigger(ch->in_room,ch);
	mp_recall_trigger(ch);
    }

    ap_leave_trigger(ch,location);
    char_from_room( ch );
    char_to_room( ch, location );
    act( "$n appears in the room.", ch, NULL, NULL, TO_ROOM );
    if( IS_AFFECTED2( ch, EFF_CANTRIP ) )
	show_cantrip( NULL, ch, TO_ROOM );
    show_ool_cantrip(NULL,ch,TO_ROOM);
    ap_enter_trigger(ch,old_room);
    look_room(ch,ch->in_room,TRUE);

    if (ch->pet != NULL && ch->pet->position>=POS_FIGHTING) {
	int *pvnum;
	pvnum=(int *)calloc(1,sizeof(int));
	*pvnum=location->vnum;
	create_event(2,ch->pet,NULL,NULL,event_recall,pvnum);
    }

    if ( IS_PC( ch ) ) {
	mp_recallto_trigger(ch);
	if (ROOM_HAS_TRIGGER(location,RTRIG_RECALL_TO))
	    rp_recallto_trigger(location,ch);
    }

    return;
}

void do_recall( CHAR_DATA *ch, char *argument ) {
    char	buf[MAX_STRING_LENGTH];
    ROOM_INDEX_DATA *location;
    int		vnum_room;
    int		*pvnum;

    if (IS_NPC(ch) && !STR_IS_SET(ch->strbit_act,ACT_PET)) {
	send_to_char("Only players can recall.\n\r",ch);
	return;
    }

    act( "$n prays for transportation!", ch, 0, 0, TO_ROOM );
    send_to_char("You pray for transportation!\n\r",ch);

    //
    // You can recall to
    // - Area recall		(highest prio)
    // - Personall recall
    // - Clanhall
    // - Temple of Fatal	(lowest prio)
    //
    vnum_room=ROOM_VNUM_TEMPLE;
    one_argument(argument,buf);
    switch (which_keyword(buf,"temple","clan","personal",NULL)) {
    case 1:
	vnum_room=ch->in_room->area->recall;
	break;
    case 2:
	if (ch->clan &&
	    !ch->clan->clan_info->independent &&
	    ch->clan->clan_info->recall)
	    vnum_room=ch->clan->clan_info->recall;
	break;
    case 3:
	if (ch->recall)
	    vnum_room=ch->recall;
	break;
    default:
	if (ch->in_room && ch->in_room->area )
	    vnum_room=ch->in_room->area->recall;

	if (vnum_room==ROOM_VNUM_TEMPLE && ch->recall)
	    vnum_room=ch->recall;

	if (vnum_room==ROOM_VNUM_TEMPLE &&
	    ch->clan && !ch->clan->clan_info->independent &&
	    ch->clan->clan_info->recall)
	    vnum_room=ch->clan->clan_info->recall;
	break;
    }

    // if there is a area-recall, go to there. No questions asked.
    if (ch->in_room &&
	ch->in_room->area &&
	    ch->in_room->area->recall!=ROOM_VNUM_TEMPLE)
	vnum_room=ch->in_room->area->recall;

    if( ( location = get_room_index( vnum_room ) ) == NULL
    &&    vnum_room != ROOM_VNUM_TEMPLE) {
	bugf("Recall for %s is unknown (from %d to %d)",ch->name,ch->in_room->vnum,vnum_room);
	vnum_room=ROOM_VNUM_TEMPLE;
	location = get_room_index( vnum_room );
    }

    if ( location == NULL ) {
	send_to_char( "You are completely lost.\n\r", ch );
	return;
    }

    if ( ch->in_room == location )
	return;

    if ( STR_IS_SET(ch->in_room->strbit_room_flags, ROOM_NO_RECALL)
    ||   IS_AFFECTED(ch, EFF_CURSE))
    {
        act("$t has forsaken you.",ch,players_god(ch),NULL,TO_CHAR);
	return;
    }

    if (is_affected(ch,gsn_entangle)) {
	send_to_char("Your feet are entangled to the floor!\n\r",ch);
	return;
    }

    if ( ch->fighting != NULL ) {
	if (!skillcheck2(ch,gsn_recall,80)) {
	    check_improve(ch,gsn_recall,FALSE,6);
	    WAIT_STATE( ch, 4 );
	    send_to_char( "You failed!\n\r", ch );
	    return;
	}
    }

    if (IS_PC(ch)) {
	if (ROOM_HAS_TRIGGER(ch->in_room,RTRIG_PRERECALL))
	if (rp_prerecall_trigger(ch->in_room,ch)==FALSE)
	    return;
	if (mp_prerecall_trigger(ch)==FALSE)
	    return;
    }

    pvnum=(int *)calloc(1,sizeof(int));
    *pvnum=location->vnum;
    create_event(2,ch,NULL,NULL,event_recall,pvnum);
}




void do_train( CHAR_DATA *ch, char *argument ) {
    char buf[MAX_STRING_LENGTH];
    CHAR_DATA *mob;
    int stat = - 1;
    char *pOutputSelf = NULL;
    char *pOutputRoom = NULL;
    char *pOutputType = NULL;

    if ( IS_NPC(ch) )
	return;

    /*
     * Check for trainer.
     */
    for ( mob = ch->in_room->people; mob; mob = mob->next_in_room )
	if ( IS_NPC(mob) && STR_IS_SET(mob->strbit_act, ACT_TRAIN) )
	    break;

    if ( mob == NULL ) {
	send_to_char( "You can't do that here.\n\r", ch );
	return;
    }

    if ( argument[0] ==0) {
	sprintf( buf, "You have %d training sessions.\n\r", ch->train );
	send_to_char( buf, ch );
	argument = "foo";
    }

    if ( ch->train < 1 && str_cmp(argument,"foo")) {
	send_to_char( "You don't have enough training sessions.\n\r", ch );
	return;
    }

    switch (which_keyword(argument,"hp","mana","move",
				"str","int","wis","dex","con",NULL)) {
    case 1:	// hp
	ch->train--;
        ch->pcdata->perm_hit += 10;
        ch->max_hit += 10;
        ch->hit +=10;
        act( "Your durability increases!",ch,NULL,NULL,TO_CHAR);
        act( "$n's durability increases!",ch,NULL,NULL,TO_ROOM);
        return;

    case 2:	// mana
	ch->train--;
        ch->pcdata->perm_mana += 10;
        ch->max_mana += 10;
        ch->mana += 10;
        act( "Your magical powers increases!",ch,NULL,NULL,TO_CHAR);
        act( "$n's magical powers increases!",ch,NULL,NULL,TO_ROOM);
        return;

    case 3:	// move
	ch->train--;
        ch->pcdata->perm_move += 10;
        ch->max_move += 10;
        ch->move += 10;
        act( "Your mobility increases!",ch,NULL,NULL,TO_CHAR);
        act( "$n's mobility increases!",ch,NULL,NULL,TO_ROOM);
        return;

    case 4:	// str
	stat        = STAT_STR;
	pOutputSelf = "You must have been exercising.";
	pOutputRoom = "$n looks stronger.";
	pOutputType = "strength";
	break;

    case 5:	// int
	stat	    = STAT_INT;
	pOutputSelf = "You have been very smart.";
	pOutputRoom = "$n acts smarter.";
	pOutputType = "intelligence";
	break;

    case 6:	// wis
	stat	    = STAT_WIS;
	pOutputSelf = "You have been very observant.";
	pOutputRoom = "$n acts wiser.";
	pOutputType = "wisdom";
	break;

    case 7:	// dex
	stat  	    = STAT_DEX;
	pOutputSelf = "You have been working on your reflexes.";
	pOutputRoom = "$n acts quicker.";
	pOutputType = "dexterity";
	break;

    case 8:	// con
	stat	    = STAT_CON;
	pOutputSelf = "You have lead a healthy life-style.";
	pOutputRoom = "$n has lead a healthy life-style.";
	pOutputType = "constitution";
	break;

    default:
	strcpy( buf, "You can train:" );
	if ( ch->perm_stat[STAT_STR] < get_max_train(ch,STAT_STR))
	    strcat( buf, " str" );
	if ( ch->perm_stat[STAT_INT] < get_max_train(ch,STAT_INT))
	    strcat( buf, " int" );
	if ( ch->perm_stat[STAT_WIS] < get_max_train(ch,STAT_WIS))
	    strcat( buf, " wis" );
	if ( ch->perm_stat[STAT_DEX] < get_max_train(ch,STAT_DEX))
	    strcat( buf, " dex" );
	if ( ch->perm_stat[STAT_CON] < get_max_train(ch,STAT_CON))
	    strcat( buf, " con" );
	strcat( buf, " hp mana move.\n\r");
	send_to_char( buf, ch );
	return;
    }

    if ( ch->perm_stat[stat]  >= get_max_train(ch,stat) ) {
	act( "Your $T is already at maximum.", ch, NULL, pOutputType, TO_CHAR );
	return;
    }

    ch->train--;
    ch->perm_stat[stat]++;
    act( pOutputSelf, ch, NULL, NULL, TO_CHAR );
    act( pOutputRoom, ch, NULL, NULL, TO_ROOM );
    return;
}


// shove while sitting on a boat to a cardinal direction
void do_shove(CHAR_DATA *ch,char *argument) {
    char arg[MAX_STRING_LENGTH];
    OBJ_DATA *boat;
    CHAR_DATA *person,*person_next;
    ROOM_INDEX_DATA *to_room=ch->in_room;
    ROOM_INDEX_DATA *from_room=ch->in_room;
    int to_exit;
    bool b;

    one_argument(argument,arg);
    if (!arg[0]) {
	send_to_char("Shove in which direction?\n\r",ch);
	return;
    }

    to_exit = get_direction(arg);
    if (to_exit<0)
    {
	send_to_char("In which direction you said?\n\r",ch);
	return;
    }

    if (to_exit==DIR_UP)
    {
	send_to_char("You can't do that.\n\r",ch);
	return;
    }

    if (ch->in_room->sector_type==SECT_WATER_SWIM ||
	ch->in_room->sector_type==SECT_WATER_NOSWIM ||
	ch->in_room->sector_type==SECT_WATER_BELOW) {
	send_to_char("You must shove something *in* the water.\n\r",ch);
	return;
    }

    if (ch->in_room->exit[to_exit]==NULL) {
	send_to_char("There is no exit to that direction.\n\r",ch);
	return;
    }
    to_room=ch->in_room->exit[to_exit]->to_room;

    boat=ch->on;
    if (boat==NULL) {
	if (IS_IMMORTAL(ch)) {
	    ap_leave_trigger(ch,to_room);
	    char_from_room( ch );
	    char_to_room( ch, to_room );
	    ap_enter_trigger(ch,from_room);
	    look_room(ch,ch->in_room,TRUE);
	} else {
	    send_to_char("You're not standing or sitting on a boat.\n\r",ch);
	}
	return;
    }

    if (to_room->sector_type!=SECT_WATER_SWIM &&
	to_room->sector_type!=SECT_WATER_NOSWIM) {
	act("That would damage $p.",ch,boat,NULL,TO_CHAR);
	return;
    }

    if (boat->item_type!=ITEM_BOAT) {
	act("It's not a good idea to shove $p in the water.",
	    ch,boat,NULL,TO_CHAR);
	return;
    }

    /*
     * Exit trigger, if activated, bail out. Only PCs are triggered.
     */
    if ( !IS_NPC(ch) && mp_exit_trigger(ch,to_exit)==FALSE )
	return;

    if (ch->move<0) {
	send_to_char("You are too exhausted.\n\r",ch);
	return;
    }
    ch->move-=movement_loss[to_room->sector_type];

    act("You shove $p into the water.",ch,boat,NULL,TO_CHAR);
    act("$n shoves $p into the water.",ch,boat,NULL,TO_ROOM);

    for (person=ch->in_room->people;person;person=person_next) {
	person_next=person->next_in_room;

	if (person->on!=boat)
	    continue;

	if (ch!=person) {
	    act("$n pushes $p and it disappears into the water.",
		person,boat,NULL,TO_ROOM);
	    act("You firmly hold $p when $N goes down in the water.",
		person,boat,ch,TO_CHAR);
	}

	ap_leave_trigger(person,to_room);
	char_from_room(person);
	char_to_room(person,to_room);
	ap_enter_trigger(person,from_room);
	act("$n firmly holds $p.",person,boat,NULL,TO_ROOM);

	b=TRUE;
	SetCharProperty(person,PROPERTY_BOOL,"sat_in_boat",&b);
    }

    for (person=ch->in_room->people;person;person=person->next_in_room) {
	b=FALSE;
	GetCharProperty(person,PROPERTY_BOOL,"sat_in_boat",&b);
	if (b) {
	    person->on=boat;
	    DeleteCharProperty(person,PROPERTY_BOOL,"sat_in_boat");
	}
    }

    obj_from_room(boat);
    for (person=ch->in_room->people;person;person=person->next_in_room)
	if (person->on==boat) {
	    send_to_char("\n\r",person);
	    look_room(person,person->in_room,TRUE);
	}
    obj_to_room(boat,to_room);

    /*
     * If someone is following the char, these triggers get activated
     * for the followers before the char, but it's safer this way...
     */
    if ( IS_NPC( ch ) && MOB_HAS_TRIGGER( ch, MTRIG_ENTRY ) )
	mp_entry_trigger(ch);
    if (!IS_VALID(ch))
	return;
    if ( !IS_NPC( ch ) )
	mp_greet_trigger( ch, to_exit );
}


void do_paddle(CHAR_DATA *ch,char *argument) {
    OBJ_DATA *boat;
    CHAR_DATA *person,*person_next;
    char arg[MAX_STRING_LENGTH];
    ROOM_INDEX_DATA *to_room;
    ROOM_INDEX_DATA *from_room=ch->in_room;
    char msg_char[MAX_STRING_LENGTH];
    char msg_room[MAX_STRING_LENGTH];
    int to_exit;
    bool b;

    one_argument(argument,arg);
    if (!arg[0]) {
	send_to_char("Paddle in which direction?\n\r",ch);
	return;
    }

    to_exit = get_direction(arg);
    if (to_exit<0) {
	send_to_char("In which direction you said?\n\r",ch);
	return;
    }

    if (to_exit==DIR_UP) {
	send_to_char("You can't do that.\n\r",ch);
	return;
    }

    if (ch->in_room->sector_type!=SECT_WATER_SWIM &&
	ch->in_room->sector_type!=SECT_WATER_NOSWIM) {
	send_to_char("Paddling should be done in water...\n\r",ch);
	return;
    }

    if (ch->in_room->exit[to_exit]==NULL) {
	send_to_char("There is no exit to that direction.\n\r",ch);
	return;
    }
    to_room=ch->in_room->exit[to_exit]->to_room;

    boat=ch->on;
    if (boat==NULL) {
	if (IS_IMMORTAL(ch)) {
	    ap_leave_trigger(ch,to_room);
	    char_from_room( ch );
	    char_to_room( ch, to_room );
	    ap_enter_trigger(ch,from_room);
	    look_room(ch,ch->in_room,TRUE);
	} else {
	    send_to_char("You're not standing or sitting on a boat.\n\r",ch);
	}
	return;
    }

    /*
     * Exit trigger, if activated, bail out. Only PCs are triggered.
     */
    if ( !IS_NPC(ch) && mp_exit_trigger(ch,to_exit)==FALSE )
	return;

    if (ch->move<0) {
	send_to_char("You are too exhausted.\n\r",ch);
	return;
    }
    ch->move-=movement_loss[to_room->sector_type];

    if (IS_AFFECTED(ch,EFF_HALLUCINATION))
	send_to_char("Row! Row! Row your boat, gently down the stream!\n\r",ch);
    else
	act("You paddle $p through the water.",ch,boat,NULL,TO_CHAR);
    act("$n paddles $p through the water.",ch,boat,NULL,TO_ROOM);

    sprintf(msg_room,
	"$n gets moved to the %s when $p gets moved.",dir_name[to_exit]);
    sprintf(msg_char,
	"You move to the %s when $p gets moved.",dir_name[to_exit]);

    for (person=ch->in_room->people;person;person=person_next) {
	person_next=person->next_in_room;

	if (person->on!=boat)
	    continue;

	if (ch!=person) {
	    act(msg_room,person,boat,NULL,TO_ROOM);
	    act(msg_char,person,boat,NULL,TO_CHAR);
	}

	ap_leave_trigger(person,to_room);
	char_from_room(person);
	char_to_room(person,to_room);
	ap_enter_trigger(person,from_room);

	act("$n has entered the room in $p.",person,boat,NULL,TO_ROOM);

	b=TRUE;
	SetCharProperty(person,PROPERTY_BOOL,"sat_in_boat",&b);
    }

    for (person=ch->in_room->people;person;person=person->next_in_room) {
	b=FALSE;
	GetCharProperty(person,PROPERTY_BOOL,"sat_in_boat",&b);
	if (b) {
	    person->on=boat;
	    DeleteCharProperty(person,PROPERTY_BOOL,"sat_in_boat");
	}
    }
    obj_from_room(boat);
    for (person=ch->in_room->people;person;person=person->next_in_room)
	if (person->on==boat) {
	    send_to_char("\n\r",person);
	    look_room(person,person->in_room,TRUE);
	}
    obj_to_room(boat,to_room);

    /*
     * If someone is following the char, these triggers get activated
     * for the followers before the char, but it's safer this way...
     */
    if ( IS_NPC( ch ) && MOB_HAS_TRIGGER( ch, MTRIG_ENTRY ) )
	mp_entry_trigger(ch);
    if (!IS_VALID(ch))
	return;
    if ( !IS_NPC( ch ) )
	mp_greet_trigger( ch, to_exit );
}


void object_drop_air(OBJ_DATA *obj) {
    ROOM_INDEX_DATA *new_room = NULL;
    CHAR_DATA *to;
    CHAR_DATA *gch;

    if (!obj->in_room->exit[DIR_DOWN])
	return;
    if (!obj->in_room->exit[DIR_DOWN]->to_room)
	return;
    if (IS_SET(obj->in_room->exit[DIR_DOWN]->exit_info, EX_CLOSED))
	return;

    new_room = obj->in_room->exit[DIR_DOWN]->to_room;

    for ( to=obj->in_room->people; to != NULL; to = to->next_in_room ) {
	if ( can_see_obj(to,obj)) {
	    act( "$p falls away.", to, obj, NULL, TO_CHAR );
	}
    }

    for (gch = obj->in_room->people; gch != NULL; gch = gch->next_in_room)
	if (gch->on == obj) {
	    do_function(gch,&do_stand,"");
	    act("Wow! Your $p just falls away!",gch,obj,NULL,TO_CHAR);
	}

    obj_from_room(obj);
    obj_to_room(obj, new_room);
    for ( to=obj->in_room->people; to != NULL; to = to->next_in_room ) {
	switch (number_percent()%3) {
	    case 0:
		if (can_see_obj(to,obj)) {
		    act( "$p falls from above.", to, obj, NULL, TO_CHAR );
		    break;
		}
		/* Note: fallthrough! */
	    case 1:
		act( "*cloink*", to, NULL, NULL, TO_CHAR );
		break;
	    case 2:
		act( "*dang!*", to, NULL, NULL, TO_CHAR );
		break;
	}
    }
}


void object_drop_water(OBJ_DATA *obj) {
    ROOM_INDEX_DATA *new_room = NULL;
    CHAR_DATA *to;

    if (!obj->in_room->exit[DIR_DOWN])
	return;
    if (!obj->in_room->exit[DIR_DOWN]->to_room)
	return;

    // hmmm... they should stay floating, shouldn't it?
    if (obj->item_type==ITEM_BOAT)
	return;

    new_room = obj->in_room->exit[DIR_DOWN]->to_room;

    for ( to=obj->in_room->people; to != NULL; to = to->next_in_room )
	if ( can_see_obj(to,obj))
	    act( "$p slowly sinks beneath the surface.",to,obj,NULL,TO_CHAR );

    obj_from_room(obj);
    obj_to_room(obj, new_room);
    for ( to=obj->in_room->people; to != NULL; to = to->next_in_room )
	if (can_see_obj(to,obj))
	    act( "$p slowly sinks down.",to,obj,NULL,TO_CHAR );
}


void object_drop_fall(OBJ_DATA *obj) {
    ROOM_INDEX_DATA *new_room = NULL;
    CHAR_DATA *to;

    if (!obj->in_room->exit[DIR_DOWN])
	return;
    if (!obj->in_room->exit[DIR_DOWN]->to_room)
	return;
    if (IS_SET(obj->in_room->exit[DIR_DOWN]->exit_info, EX_CLOSED))
	return;

    new_room = obj->in_room->exit[DIR_DOWN]->to_room;

    for ( to=obj->in_room->people; to != NULL; to = to->next_in_room ) {
	if ( can_see_obj(to,obj)) {
	    act( "$p falls on the floor and rolls away.",to,obj,NULL,TO_CHAR );
	}
    }

    obj_from_room(obj);
    obj_to_room(obj, new_room);
    for ( to=obj->in_room->people; to != NULL; to = to->next_in_room ) {
	switch (number_percent()%3) {
	    case 0:
		if (can_see_obj(to,obj)) {
		    act( "A $p drops from above on the floor.", to, obj, NULL, TO_ROOM );
		break;
		}
		/* Note: fallthrough! */
	    case 1:
		act( "*cloink*", to,NULL,NULL,TO_CHAR);
		break;
	    case 2:
		act( "*dang!*", to,NULL,NULL,TO_CHAR);
		break;
	}
    }
}
