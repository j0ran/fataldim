//
// $Id: wiz_stat.h,v 1.6 2006/03/14 20:15:34 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef INCLUDED_WIZ_STAT_H
#define INCLUDED_WIZ_STAT_H

void	stat_area		(CHAR_DATA *ch,char *arg);
void	stat_channels		(CHAR_DATA *ch);
void	stat_clan		(CHAR_DATA *ch,char *arg);
void	stat_class		(CHAR_DATA *ch,char *arg);
void	stat_descriptor		(CHAR_DATA *ch,char *arg);
void	stat_equipment		(CHAR_DATA *ch,char *arg);
void	stat_files		(CHAR_DATA *ch);
void	stat_group		(CHAR_DATA *ch,char *arg);
void	stat_mobile		(CHAR_DATA *ch,char *arg);
void	stat_mrace		(CHAR_DATA *ch,char *arg);
void	stat_mud		(CHAR_DATA *ch);
void	stat_newbie		(CHAR_DATA *ch);
void	stat_notes		(CHAR_DATA *ch);
void	stat_object		(CHAR_DATA *ch,char *arg);
void	stat_prace		(CHAR_DATA *ch,char *arg);
void	stat_room		(CHAR_DATA *ch,char *arg);
void	stat_rlinks		(CHAR_DATA *ch, char *argument);
void	stat_alinks		(CHAR_DATA *ch);
void	stat_skill		(CHAR_DATA *ch,char *arg);
void	stat_badnames		(CHAR_DATA *ch);
void	stat_social		(CHAR_DATA *ch, char *argument);
void	stat_help		(CHAR_DATA *ch, char *argument);
void	stat_mprog		(CHAR_DATA *ch, char *argument);
void	stat_oprog		(CHAR_DATA *ch, char *argument);
void	stat_rprog		(CHAR_DATA *ch, char *argument);
void	stat_aprog		(CHAR_DATA *ch, char *argument);

#endif	// INCLUDED_WIZ_STAT_H
