//
// $Id: fd_output.c,v 1.19 2006/08/25 09:43:20 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

//
// This part handles all the output to the player, like sending prompts,
// page a long piece of text, send a like, act-function.
//

#include "merc.h"
#include "color.h"
#include "fd_mxp.h"

//
// Write a prompt to the player
//
// Originally by Morgenes for Aldara Mud
//
char *prompt_exists(CHAR_DATA *ch, char *doors, char *buf, bool pueblo)
{
    int door;
    bool found = FALSE;
    EXIT_DATA *pexit;

    doors[0] = '\0';

    if (ch->position>POS_SLEEPING) {
        if (!check_blind(ch,FALSE) && !STR_IS_SET(ch->strbit_act,PLR_HOLYLIGHT)) {
            found=TRUE;
            if (pueblo) {
                strcpy(doors,
                       "<a xch_cmd=\"pueblo north\">north</a> "
                       "<a xch_cmd=\"pueblo east\">east</a> "
                       "<a xch_cmd=\"pueblo south\">south</a> "
                       "<a xch_cmd=\"pueblo west\">west</a> "
                       "<a xch_cmd=\"pueblo up\">up</a> "
                       "<a xch_cmd=\"pueblo down\">down</a>");
            }
            else strcpy(doors,"blinded");
        } else {
            for (door = 0; door < DIR_MAX; door++) {
                if ((pexit = ch->in_room->exit[door]) != NULL
                        && pexit ->to_room != NULL
                        && (can_see_room(ch,pexit->to_room) || IS_AFFECTED(ch,EFF_INFRARED))
                        && !IS_AFFECTED(ch,EFF_BLIND)
                        && !IS_SET(pexit->exit_info,EX_CLOSED))
                {
                    found = TRUE;
                    if (pueblo)
                    {
                        sprintf(buf,"<a xch_cmd=\"pueblo %s\">%s</a> ", dir_name[door],dir_name[door]);
                        strcat(doors,buf);
                    }
                    else
                    {
                        strcat(doors, dir_name_short[door]);
                    }
                }
                else if (pueblo) {
                    sprintf(buf, "%s ", dir_name[door]);
                    strcat(doors, buf);
                }
            }
        }
    } else {
        if (pueblo) strcpy(doors, "north east south west up down");
        else {
            if (ch->position==POS_DEAD) strcpy(doors,"dead");
            else strcpy(doors,"sleeping");
        }
        found=TRUE;
    }

    if (!found) {
        if (pueblo) strcpy(doors, "north east south west up down");
        else strcpy(doors, "none");

    }

    return doors;
}

void send_prompt( CHAR_DATA *ch ) {
    char buf[MAX_STRING_LENGTH];
    char buf2[MAX_STRING_LENGTH];
    const char *str;
    const char *i;
    char *point;
    char doors[MAX_INPUT_LENGTH];

    strcpy(buf,"{x");
    point = strchr(buf,0);
    str = ch->prompt;
    if (str == NULL || str[0] == '\0')
    {
        sprintf_to_char( ch, "{x<%dhp %dm %dmv> %s",
                         ch->hit,ch->mana,ch->move,ch->prefix);
        return;
    }

    if (STR_IS_SET(ch->strbit_comm,COMM_AFK)) {
        char text1[]="AFK";
        char text2[]="AUTO-AFK";
        char *text=text1;

        if (STR_IS_SET(ch->strbit_comm,COMM_AFK_BY_IDLE)) text=text2;
        if (IS_NPC(ch)) {
            sprintf_to_char(ch,"{x<%s> ",text);
            return;
        }

        // Players might have stocked tells.
        if (ch->pcdata->missedtells==0)
            sprintf_to_char(ch,"{x<%s> ",text);
        else
            sprintf_to_char(ch,"{x<%s (%d tell%s)> ",
                            text,
                            ch->pcdata->missedtells,
                            ch->pcdata->missedtells==1?"":"s");
        return;
    }

    if (HAS_MXP(ch))
        send_to_char(MXP"<Prompt>",ch);

    while( *str != '\0' ) {
        if( *str != '%' ) {
            *point++ = *str++;
            continue;
        }

        ++str;
        switch( *str ) {
        default : i = " "; break;
        case 'e':
            i = prompt_exists(ch, doors, buf2, FALSE);
            break;
        case 'E': // which editor
            if( IS_IMMORTAL( ch ) && ch->in_room != NULL )
                strcpy(buf2,olc_ed_name(ch));
            else
                strcpy(buf2,"");
            i=buf2;
            break;
        case 't' :
            strcpy(buf2,"\n\r");
            i = buf2; break;
        case 'T' :
            sprintf(buf2,"%2d%s",
                    (time_info.hour % 12 == 0) ? 12 : time_info.hour %12,
                    time_info.hour >= 12 ? "pm" : "am");
            i = buf2; break;
        case 'h' :
            if (HAS_MXP(ch))
                sprintf( buf2, "<Hp>%d</Hp>", ch->hit );
            else
                itoa_r(ch->hit,buf2,sizeof(buf2));
            i = buf2; break;
        case 'H' :
            if (HAS_MXP(ch))
                sprintf( buf2, "<MaxHp>%d</MaxHp>", ch->max_hit );
            else
                itoa_r(ch->max_hit,buf2,sizeof(buf2));
            i = buf2; break;
        case 'm' :
            if (HAS_MXP(ch))
                sprintf( buf2, "<Mana>%d</Mana>", ch->mana );
            else
                itoa_r(ch->mana,buf2,sizeof(buf2));
            i = buf2; break;
        case 'M' :
            if (HAS_MXP(ch))
                sprintf( buf2, "<MaxMana>%d</MaxMana>", ch->max_mana );
            else
                itoa_r(ch->max_mana,buf2,sizeof(buf2));
            i = buf2; break;
        case 'v' :
            if (HAS_MXP(ch))
                sprintf( buf2, "<Move>%d</Move>", ch->move );
            else
                itoa_r(ch->move,buf2,sizeof(buf2));
            i = buf2; break;
        case 'V' :
            if (HAS_MXP(ch))
                sprintf( buf2, "<MaxMove>%d</MaxMove>", ch->max_move );
            else
                itoa_r(ch->max_move,buf2,sizeof(buf2));
            i = buf2; break;
        case 'q' :
            if (IS_PC(ch))
                itoa_r(ch->pcdata->questpoints,buf2,sizeof(buf2));
            else
                strcpy(buf2,"n/a");
            i = buf2; break;
        case 'Q' : {
            int qp;
            if (IS_PC(ch) &&
                    GetCharProperty(ch,PROPERTY_INT,"quest_countdown",&qp)) {
                itoa_r(qp,buf2,sizeof(buf2));
            } else
                strcpy(buf2,"n/a");
            i = buf2; break;
        }
        case 'x' :
            itoa_r(ch->exp,buf2,sizeof(buf2));
            i = buf2; break;
        case 'X' :
            itoa_r(exp_to_level(ch),buf2,sizeof(buf2));
            i = buf2; break;
        case 'g' :
            ltoa_r(ch->gold,buf2,sizeof(buf2));
            i = buf2; break;
        case 's' :
            ltoa_r(ch->silver,buf2,sizeof(buf2));
            i = buf2; break;
        case 'a' :
            if( ch->level > 9 )
                itoa_r(ch->alignment,buf2,sizeof(buf2));
            else
                sprintf( buf2, "%s", IS_GOOD(ch) ? "good" : IS_EVIL(ch) ?
                                                       "evil" : "neutral" );
            i = buf2; break;
        case 'r' :
            if( ch->in_room != NULL )
                sprintf( buf2, "%s",
                         ((!IS_NPC(ch) && STR_IS_SET(ch->strbit_act,PLR_HOLYLIGHT)) ||
                          (!IS_AFFECTED(ch,EFF_BLIND) && !room_is_dark( ch->in_room )))
                         ? ch->in_room->name : "darkness");
            else
                sprintf( buf2, " " );
            i = buf2; break;
        case 'R' :
            if( IS_IMMORTAL( ch ) && ch->in_room != NULL )
                itoa_r(ch->in_room->vnum,buf2,sizeof(buf2));
            else
                sprintf( buf2, " " );
            i = buf2; break;
        case 'z' :
            if( IS_IMMORTAL( ch ) && ch->in_room != NULL )
                sprintf( buf2, "%s", ch->in_room->area->name );
            else
                sprintf( buf2, " " );
            i = buf2; break;
        case '%' :
            sprintf( buf2, "%%" );
            i = buf2; break;
        case 'c' :		/* OLC */
            i = olc_ed_name( ch );
            if (i[0]==' ') i="";
            break;
        case 'C' :		/* OLC */
            i = olc_ed_vnum( ch );
            if (i[0]==' ') i="";
            break;
        case 'i' : {
            int l=0;
            buf2[l]=0;
            if (ch->invis_level>0&&IS_IMMORTAL(ch)) {
                snprintf(buf2+l,sizeof(buf2)-l-1,"Wizi %d",ch->invis_level);
            } else if (IS_AFFECTED(ch,EFF_INVISIBLE)) {
                strncat(buf2+l,"Invis",sizeof(buf2)-l-1);
            }
            l=strlen(buf2);
            if (ch->incog_level>0&&IS_IMMORTAL(ch)) {
                if (l) { buf2[l++]=' '; buf2[l]=0; }
                l+=snprintf(buf2+l,sizeof(buf2)-l-1,"Incog %d",ch->incog_level);
            }
            i=buf2;
            break;
        }
        }
        ++str;
        while( (*point = *i) != '\0' )
            ++point, ++i;
    }

    if (HAS_PUEBLO(ch)) {
        sprintf(buf2,"</xch_mudtext><xch_pane action=redirect "
                     "name=exits options=\"internal fit nonsizeable\" "
                     "align=bottom><xch_page clear=text>Exits: %s"
                     "<xch_pane action=redirect name=_previous>"
                     "<xch_mudtext>", prompt_exists(ch, doors, buf2, TRUE));
        i=buf2;
        while( (*point = *i) != '\0' )
            ++point, ++i;
    }

    /* Do colour stuff */
    buf[point - buf]='\0';
    colourbuf(ch,buf2,buf);
    write_to_buffer( ch->desc, buf2, 0 );

    if (HAS_MXP(ch))
        send_to_char("</Prompt>",ch);

    if (ch->prefix[0] != '\0')
        write_to_buffer(ch->desc,ch->prefix,0);
    return;
}


//
// Write to all characters.
//
void send_to_all_char( const char *text ) {
    CHAR_DATA *player;

    if (IS_NULL_STRING(text))
        return;
    for (player=player_list; player!=NULL; player=player->next_player )
        send_to_char(text,player);

    return;
}

//
// Write to all characters in an area.
//
void send_to_zone( const char *text, AREA_DATA *area) {
    CHAR_DATA *player;

    if (IS_NULL_STRING(text))
        return;
    if (area==NULL)
        return;

    for (player=player_list; player!=NULL; player=player->next_player)
        if (player->in_room != NULL
                && player->in_room->area == area)
            send_to_char(text,player);
}

//
// sprintf to one char.
//
void sprintf_to_char(CHAR_DATA *ch, const char *fmt, ...) {
    char buf[MAX_STRING_LENGTH];
    va_list ap;

    if (IS_NULL_STRING(fmt))
        return;
    if (ch==NULL)
        return;
    if (ch->desc==NULL)
        return;

    va_start(ap,fmt);
    vsprintf(buf,fmt,ap);
    va_end(ap);
    send_to_char(buf,ch);
}



//
// send a string to one char.
//
void send_to_char( const char *txt, CHAR_DATA *ch ) {
    const char	*point;
    char 	*point2;
    char 	buf[MSL];

    if (IS_NULL_STRING(txt))
        return;
    if (ch==NULL)
        return;
    if (ch->desc==NULL)
        return;

    buf[0]=0;
    point2=buf;

    // with colours
    if (STR_IS_SET(ch->strbit_act,PLR_COLOUR)) {
        for (point=txt ; *point!=0 ; point++ ) {

            if (point2>(buf+sizeof(buf)-MSL)) {
                write_to_buffer(ch->desc,buf,point2-buf);
                point2=buf;
                point2[0]=0;
            }

            // background colour
            if (point[0]=='{') {
                strcat(buf,colour(point[1],ch,TRUE));
                for (point2=buf ; *point2!=0 ; point2++);
                if (point[0]!=0) point++;
                continue;
            }

            // foreground colour
            if (point[0]=='}') {
                strcat(buf,colour(point[1],ch,FALSE));
                for (point2=buf ; *point2!=0 ; point2++);
                if (point[0]!=0) point++;
                continue;
            }

            *point2=*point;
            *++point2=0;
        }
        *point2=0;
        write_to_buffer(ch->desc,buf,point2-buf );
        return;
    }

    // without colours
    for (point=txt ; *point!=0 ; point++ ) {
        if (point2>(buf+sizeof(buf)-MSL)) {
            write_to_buffer(ch->desc,buf,point2-buf);
            point2=buf;
            point2[0]=0;
        }

        // foreground colour
        if( point[0]=='}') {
            if (point[1]=='}' || point[1]==0) {
                *point2=*point;
                *++point2=0;
            }
            if (point[1]!=0) point++;
            continue;
        }

        // background colour
        if (point[0]=='{') {
            if (point[1]=='{' || point[1]==0) {
                *point2=*point;
                *++point2=0;
            }
            if (point[1]!=0) point++;
            continue;
        }
        *point2=*point;
        *++point2=0;
    }
    *point2=0;
    write_to_buffer(ch->desc,buf,point2-buf);
}

//
// Page to one char
//
void page_to_char(const char *txt,CHAR_DATA *ch) {
    const char	*point;
    char	*point2;
    char	buf[MSL];
    BUFFER	*buffer;

    if (IS_NULL_STRING(txt))
        return;
    if (ch==NULL)
        return;
    if (ch->desc==NULL)
        return;

    buf[0]=0;
    point2=buf;

    buffer=new_buf();

    if (STR_IS_SET(ch->strbit_act,PLR_COLOUR)) {
        for (point=txt ; *point!=0 ; point++ ) {
            if (point2>(buf+MSL-64)) { // keep 64 chars open.
                // no space left in buffer -> cleanup
                if (!add_buf(buffer,buf))
                    bugf("page_to_char(): buf overflow for %s on [%d]",
                         ch->name,GET_DESCRIPTOR(ch));
                point2=buf;
                point2[0]=0;
            }

            // foreground colour
            if (point[0]=='{') {
                strcat(buf,colour(point[1],ch,TRUE));
                for (point2=buf;*point2!=0;point2++);
                if (point[1]!=0) point++;
                continue;
            }

            // background colour
            if (point[0]=='}') {
                strcat(buf,colour(point[1],ch,FALSE));
                for (point2=buf;*point2!=0;point2++);
                if (point[1]!=0) point++;
                continue;
            }
            *point2=*point;
            *++point2=0;
        }
        *point2=0;
    } else {
        for (point=txt ; *point!=0 ; point++ ) {
            if (point2>(buf+MSL-64)) {
                // no space left in buffer -> cleanup
                if (!add_buf(buffer,buf))
                    bugf("page_to_char: buf overflow for %s on [%d]",
                         ch->name,GET_DESCRIPTOR(ch));
                point2=buf;
                point2[0]=0;
            }

            // foreground colour
            if( point[0]=='{') {
                if(point[1]=='{'||point[1]==0) {
                    *point2=*point;
                    *++point2=0;
                }
                if (point[1]!=0) point++;
                continue;
            }

            // background colour
            if( point[0]=='}') {
                if(point[1]=='}'||point[1]==0) {
                    *point2=*point;
                    *++point2=0;
                }
                if (point[1]!=0) point++;
                continue;
            }
            *point2 = *point;
            *++point2 = '\0';
        }
        *point2 = '\0';
    }

    add_buf(buffer,buf);

    ch->desc->showstr_head = alloc_mem(strlen(buf_string(buffer)) + 1);
    strcpy(ch->desc->showstr_head,buf_string(buffer));
    free_buf(buffer);
    ch->desc->showstr_point = ch->desc->showstr_head;
    show_string(ch->desc,"");

    return;
}


//
// Sends a semi-formatted string to a player
//
void act_new( const char *format, CHAR_DATA *ch, const void *arg1,
              const void *arg2, int type, int min_pos) {
    CHAR_DATA 	*to;
    CHAR_DATA 	*vch = ( CHAR_DATA * ) arg2;
    OBJ_DATA 	*obj1 = ( OBJ_DATA  * ) arg1;
    OBJ_DATA 	*obj2 = ( OBJ_DATA  * ) arg2;
    const char 	*str;
    char 	*i;
    char 	*point;
    char 	*i2;
    char 	fixed[ MAX_STRING_LENGTH ];
    char 	buf[ MAX_STRING_LENGTH   ];
    char 	fname[ MAX_INPUT_LENGTH  ];
    bool	fColour = FALSE;
    CHAR_DATA	*real_ch,*real_vch;
    OBJ_DATA	*real_obj1,*real_obj2;

    if (IS_NULL_STRING(format))
        return;
    if (ch==NULL)
        return;
    if (!IS_VALID(ch))
        return;
    if (ch->in_room==NULL)
        return;

    to=ch->in_room->people;
    if (type==TO_VICT) {
        if (vch==NULL) {
            bugf("act_new(): type is TO_VICT but there is no victim defined.");
            bugf("act_new(): format='%s'",format);
            bugf("act_new(): ch='%s'",NAME(ch));
            return;
        }

        if (vch->in_room==NULL)
            return;
        to = vch->in_room->people;
    }

    real_ch=ch;
    real_vch=vch;
    real_obj1=obj1;
    real_obj2=obj2;

    for ( ; to!=NULL; to=to->next_in_room ) {
        //
        // restore everything back to normal
        //
        ch=real_ch;
        vch=real_vch;
        obj1=real_obj1;
        obj2=real_obj2;

        //
        // skip actions based on variable-values
        //
        if (!IS_VALID(to))
            continue;
        if (to->position<min_pos)
            continue;
        if (IS_PC(to) && to->desc == NULL )		// linkless people
            continue;
        if ( IS_NPC(to) && !MOB_HAS_TRIGGER(to,MTRIG_ACT) && to->desc==NULL )
            continue;					// npc without logging
        // or act-trigger

        //
        // skip actions based on act-type
        //
        if ( type==TO_CHAR && to!=ch )
            continue;
        if ( type==TO_VICT && ( to!=vch || to==ch ) )
            continue;
        if ( type==TO_ROOM && to==ch )
            continue;
        if ( type==TO_NOTVICT && (to==ch || to==vch) )
            continue;

        //
        // if this player is hallucinating, change things
        //
        if (IS_AFFECTED(to,EFF_HALLUCINATION)) {
            ch=hallucination_char();
            vch=hallucination_char();
            obj1=hallucination_obj();
            obj2=hallucination_obj();
        }

        if (IS_NPC(ch) && STR_IS_SET(ch->strbit_act,ACT_UNSEENSERVANT)) {
            if (!IS_IMMORTAL(to))
                continue;
            strcpy(buf,"(UNSEEN) ");
            point=buf+strlen(buf);
        } else
            point=buf;

        str=format;
        while (*str!=0) {
            if (*str!='$' &&*str!='{' ) {
                *point++=*str++;
                continue;
            }

            i=NULL;
            switch (*str) {
            case '$':
                fColour=TRUE;
                ++str;
                i=" <@@@> ";
                if (arg2==NULL && *str>='A' && *str<='Z' ) {
                    bugf("Act: missing arg2 for code %c.",*str);
                    bugf("Act: format='%s'",format);
                } else {
                    //
                    // Don't think that just checking for
                    // "valid" (ie. non-NULL pointers) will solve
                    // the proble since it is possible to refer
                    // to a structure with the size smaller
                    // than the actual structure. But at least
                    // it's a start.
                    //
                    switch (*str) {
                    default:
                        bugf("Act: bad code %c.",*str);
                        bugf("Act: format='%s'",format);
                        i=" <@@@> ";
                        break;

                        // just a dollar
                    case '$':
                        i="$";
                        break;

                        // door
                    case 'd':
                        if (IS_NULL_STRING((char *)arg2))
                            i = "door";
                        else {
                            one_argument( (char *)arg2, fname );
                            i = fname;
                        }
                        break;

                        // he / she
                    case 'e':
                        if (ch!=NULL)
                            i=he_she[URANGE(0,show_sex(ch),2)];
                        else {
                            bugf("Act: bad code $e for 'ch'");
                            bugf("Act: format='%s'",format);
                        }
                        break;

                    case 'E':
                        if (vch!=NULL)
                            i=he_she[URANGE(0, show_sex(vch),2)];
                        else {
                            bugf("Act: bad code $E for 'vch'");
                            bugf("Act: format='%s'",format);
                        }
                        break;

                        // him / her
                    case 'm':
                        if (ch!=NULL)
                            i=him_her[URANGE(0,show_sex(ch),2)];
                        else {
                            bugf("Act: bad code $m for 'ch'");
                            bugf("Act: format='%s'",format);
                        }
                        break;

                    case 'M':
                        if (vch!=NULL)
                            i=him_her[URANGE(0,show_sex(vch),2)];
                        else {
                            bugf("Act: bad code $M for 'vch'");
                            bugf("Act: format='%s'",format);
                        }
                        break;

                        // name of character
                    case 'n':
                        if (ch!=NULL && to!=NULL)
                            i=PERS(ch,to);
                        else {
                            bugf("Act: bad code $n for 'ch' or 'to'");
                            bugf("Act: format='%s'",format);
                        }
                        break;

                    case 'N':
                        if (vch!=NULL && to!=NULL )
                            i=PERS(vch,to);
                        else {
                            bugf("Act: bad code $N for 'vch' or 'to'");
                            bugf("Act: format='%s'",format);
                        }
                        break;

                        // name of object
                    case 'p':
                        if ( to!=NULL && obj1!=NULL )
                            i=can_see_obj( to, obj1 )
                                    ? obj1->short_descr
                                    : "something";
                        else {
                            bugf("Act: bad code $p for 'to' or 'obj1'.");
                            bugf("Act: format='%s'",format);
                        }
                        break;

                    case 'P':
                        if ( to!=NULL && obj2!=NULL )
                            i=can_see_obj(to,obj2)
                                    ? obj2->short_descr
                                    : "something";
                        else {
                            bugf("Act: bad code $P for 'to' or 'obj2'.");
                            bugf("Act: format='%s'",format);
                        }
                        break;

                        // his / her
                    case 's':
                        if (ch!=NULL)
                            i=his_her[URANGE(0,show_sex(ch),2)];
                        else {
                            bugf("Act: bad code $s for 'ch'.");
                            bugf("Act: format='%s'",format);
                        }
                        break;

                    case 'S':
                        if (vch!=NULL)
                            i=his_her[URANGE(0,show_sex(vch),2)];
                        else {
                            bugf("Act: bad code $S for 'vch'.");
                            bugf("Act: format='%s'",format);
                        }
                        break;

                        // plain text
                    case 't':
                        if (arg1!=NULL)
                            i=(char *)arg1;
                        else {
                            bugf("Act: bad code $t for 'arg1'");
                            bugf("Act: format='%s'",format);
                        }
                        break;

                    case 'T':
                        if (arg2!=NULL)
                            i=(char *)arg2;
                        else {
                            bugf("Act: bad code $T for 'arg2'");
                            bugf("Act: format='%s'",format);
                        }
                        break;
                    }
                }
                break;

            case '{':
                fColour=FALSE;
                i=NULL;
                if (STR_IS_SET(to->strbit_act,PLR_COLOUR))
                    i=colour(str[1],to,TRUE);
                else if (str[1]=='{' || str[1]==0)
                    i="{";

                if (str[1]!=0) str++;
                break;

            case '}':
                fColour=FALSE;
                i=NULL;
                if (STR_IS_SET(to->strbit_act,PLR_COLOUR))
                    i=colour(str[1],to,FALSE);
                else if (str[1]=='{' || str[1]==0)
                    i="{";

                if (str[1]!=0) str++;
                break;

            default:
                fColour = FALSE;
                *point++ = *str++;
                break;
            }
            ++str;

            if (fColour && i!=NULL) {
                fixed[0]=0;
                i2=fixed;

                if (STR_IS_SET(to->strbit_act,PLR_COLOUR)) {
                    for( i2=fixed ; *i!=0 ; i++ ) {
                        if (i[0]=='{') {
                            strcat(fixed,colour(i[1],to,TRUE));
                            for (i2=fixed ; *i2!=0 ; i2++);

                            if (i[1]!=0) i++;
                            continue;
                        }
                        if (i[0]=='}') {
                            strcat(fixed,colour(i[1],to,FALSE));
                            for (i2=fixed ; *i2!=0 ; i2++ );

                            if (i[1]!=0) i++;
                            continue;
                        }
                        *i2=*i;
                        *++i2=0;
                    }
                    *i2=0;
                    i=&fixed[0];
                } else {
                    for( i2=fixed ; *i!=0 ; i++) {
                        if (*i=='{') {
                            if (i[1]=='{' || i[1]==0) {
                                *i2=*i;
                                *++i2=0;
                            }
                            if (i[1]!=0) i++;
                            continue;
                        }
                        *i2=*i;
                        *++i2=0;
                    }
                    *i2=0;
                    i=&fixed[0];
                }
            }


            if (i!=NULL) {
                while ((*point=*i)!=0) {
                    ++point;
                    ++i;
                }
            }
        }

        *point++='\n';
        *point++='\r';
        *point=0;
        buf[0]=UPPER(buf[0]);
        if (buf[0]==27) {	// escape
            if (buf[2]<'2')	// HACK! should be fixed (once)
                buf[7]=UPPER(buf[7]);
            else
                buf[5]=UPPER(buf[5]);
        }

        if (to->desc!=NULL)
            write_to_buffer(to->desc,buf,point-buf);
        if (IS_NPC(to) && MOBtrigger && MOB_HAS_TRIGGER(to,MTRIG_ACT))
            mp_act_trigger((char *)format,to,ch,arg1,arg2,MTRIG_ACT);
    }

    return;
}


//
// colour conversions.
//
// Original by Lope, hacked beyond recognition.
//
// XXX - Should move info fd_colour.h
//
char *colour(char type,CHAR_DATA *ch,bool foreground) {
    static char	code[MSL];	// will be returned to the calling function

    if (ch==NULL)
        return "";
    if (IS_NPC(ch))
        return "";

    if (STR_IS_SET(ch->strbit_act,PLR_HAS_RAW_COLOUR)) {
        if (foreground)
            sprintf(code,"{%c",type);
        else
            sprintf(code,"}%c",type);
        return code;
    }

    // MXP colours
    if (HAS_MXP(ch)) {
        char *where;

        if (foreground)
            where="fore";
        else
            where="back";

        switch (type) {
        default : sprintf( code, "<color %s>"		,where);break;
        case 'x': sprintf( code, "<color %s>"		,where);break;
        case 'b': sprintf( code, "<color %s=#000080>"	,where);break;
        case 'c': sprintf( code, "<color %s=#008080>"	,where);break;
        case 'g': sprintf( code, "<color %s=#008000>"	,where);break;
        case 'm': sprintf( code, "<color %s=#800080>"	,where);break;
        case 'r': sprintf( code, "<color %s=#800000>"	,where);break;
        case 'w': sprintf( code, "<color %s=#800000>"	,where);break;
        case 'y': sprintf( code, "<color %s=#808000>"	,where);break;
        case 'B': sprintf( code, "<color %s=#0000ff>"	,where);break;
        case 'C': sprintf( code, "<color %s=#00ffff>"	,where);break;
        case 'G': sprintf( code, "<color %s=#00ff00>"	,where);break;
        case 'M': sprintf( code, "<color %s=#ff00ff>"	,where);break;
        case 'R': sprintf( code, "<color %s=#ff0000>"	,where);break;
        case 'W': sprintf( code, "<color %s=#ffffff>"	,where);break;
        case 'Y': sprintf( code, "<color %s=#ffff00>"	,where);break;
        case 'D': sprintf( code, "<color %s=#696969>"	,where);break;
        case '_': sprintf( code, "<u>"			);break;
        case '*': strcpy ( code, ""			);break;
        case '#': strcpy ( code, ""			);break;
        case '-': sprintf( code, "<s>"			);break;
        case '{': sprintf( code, "{"			);break;
        case '>':
            switch (number_range(0,15)) {
            case 0 : sprintf(code, "<color %s=#000080>"	,where);break;
            case 1 : sprintf(code, "<color %s=#008080>"	,where);break;
            case 2 : sprintf(code, "<color %s=#008000>"	,where);break;
            case 3 : sprintf(code, "<color %s=#800080>"	,where);break;
            case 4 : sprintf(code, "<color %s=#800000>"	,where);break;
            case 5 : sprintf(code, "<color %s=#800000>"	,where);break;
            case 6 : sprintf(code, "<color %s=#808000>"	,where);break;
            case 7 : sprintf(code, "<color %s=#0000ff>"	,where);break;
            case 8 : sprintf(code, "<color %s=#00ffff>"	,where);break;
            case 9 : sprintf(code, "<color %s=#00ff00>"	,where);break;
            case 10: sprintf(code, "<color %s=#ff00ff>"	,where);break;
            case 11: sprintf(code, "<color %s=#ff0000>"	,where);break;
            case 12: sprintf(code, "<color %s=#ffffff>"	,where);break;
            case 13: sprintf(code, "<color %s=#ffff00>"	,where);break;
            case 14: sprintf(code, "<color %s=#696969>"	,where);break;
            case 15: sprintf(code, "<color %s>"		,where);break;
            }
            break;
        case 0: code[0]='{'; code[1]=0;	break;
        }
        return code;
    }

    // normal ANSI colours, foreground
    if (foreground) {
        switch (type) {
        default : strcpy( code, CLEAR		);break;
        case 'x': strcpy( code, CLEAR		);break;
        case 'b': strcpy( code, C_BLUE		);break;
        case 'c': strcpy( code, C_CYAN		);break;
        case 'g': strcpy( code, C_GREEN		);break;
        case 'm': strcpy( code, C_MAGENTA	);break;
        case 'r': strcpy( code, C_RED		);break;
        case 'w': strcpy( code, C_WHITE		);break;
        case 'y': strcpy( code, C_YELLOW	);break;
        case 'B': strcpy( code, C_B_BLUE	);break;
        case 'C': strcpy( code, C_B_CYAN	);break;
        case 'G': strcpy( code, C_B_GREEN	);break;
        case 'M': strcpy( code, C_B_MAGENTA	);break;
        case 'R': strcpy( code, C_B_RED		);break;
        case 'W': strcpy( code, C_B_WHITE	);break;
        case 'Y': strcpy( code, C_B_YELLOW	);break;
        case 'D': strcpy( code, C_D_GREY	);break;
        case '_': strcpy( code, C_UNDERLINE	);break;
        case '*': strcpy( code, C_BLINK		);break;
        case '#': strcpy( code, C_INVERSE	);break;
        case '-': strcpy( code, C_STRIKETHROUGH	);break;
        case '{': strcpy( code, "{"		);break;
        case '>':
            switch (number_range(0,15)) {
            case 0 : strcpy(code, C_BLUE	); break;
            case 1 : strcpy(code, C_CYAN	); break;
            case 2 : strcpy(code, C_GREEN	); break;
            case 3 : strcpy(code, C_MAGENTA	); break;
            case 4 : strcpy(code, C_RED		); break;
            case 5 : strcpy(code, C_WHITE	); break;
            case 6 : strcpy(code, C_YELLOW	); break;
            case 7 : strcpy(code, C_B_BLUE	); break;
            case 8 : strcpy(code, C_B_CYAN	); break;
            case 9 : strcpy(code, C_B_GREEN	); break;
            case 10 : strcpy(code, C_B_MAGENTA	); break;
            case 11 : strcpy(code, C_B_RED	); break;
            case 12 : strcpy(code, C_B_WHITE	); break;
            case 13 : strcpy(code, C_B_YELLOW	); break;
            case 14 : strcpy(code, C_D_GREY	); break;
            case 15 : strcpy(code, CLEAR	); break;
            }
            break;
        case 0: code[0]='{'; code[1]=0;	break;
        }
        return code;
    }

    // normal ANSI colours, background
    switch (type) {
    default : strcpy( code, CLEAR	);break;
    case 'x': strcpy( code, CLEAR	);break;
    case 'b': strcpy( code, B_BLUE	);break;
    case 'c': strcpy( code, B_CYAN	);break;
    case 'g': strcpy( code, B_GREEN	);break;
    case 'm': strcpy( code, B_MAGENTA	);break;
    case 'r': strcpy( code, B_RED	);break;
    case 'w': strcpy( code, B_WHITE	);break;
    case 'y': strcpy( code, B_YELLOW	);break;
    case '}': strcpy( code, "}"		);break;
    case '>':
        switch (number_range(0,7)) {
        case 0 : strcpy(code, CLEAR	); break;
        case 1 : strcpy(code, B_BLUE	); break;
        case 2 : strcpy(code, B_CYAN	); break;
        case 3 : strcpy(code, B_GREEN	); break;
        case 4 : strcpy(code, B_MAGENTA	); break;
        case 5 : strcpy(code, B_RED	); break;
        case 6 : strcpy(code, B_WHITE	); break;
        case 7 : strcpy(code, B_YELLOW	); break;
        }
        break;
    case 0: code[0]='}'; code[1]=0;	break;
    }

    return code;
}


//
// XXX - Should move info fd_colour.h
//
char *colourbuf(CHAR_DATA *ch,char *to,char *from) {
    char *remto=to;

    while (from[0]!=0) {
        if (from[0]=='{') {
            if (IS_PC(ch) && STR_IS_SET(ch->strbit_act,PLR_COLOUR))
                strcpy(to,colour(from[1],ch,TRUE));
            else if (from[1]=='{' || from[1]==0)
                strcpy(to,"{");
            else
                to[0]=0;

            to+=strlen(to);
            from++;
        } else if (from[0]=='}') {
            if (IS_PC(ch) && STR_IS_SET(ch->strbit_act,PLR_COLOUR))
                strcpy(to,colour(from[1],ch,FALSE));
            else if(from[1]=='}' || from[1]==0)
                strcpy(to,"}");
            else
                to[0]=0;

            to+=strlen(to);
            from++;
        } else {
            *(to++)=from[0];
        }

        if(from[0]) from++;
    }

    to[0]=0;

    return remto;
}

