/* $Id: db2.c,v 1.42 2006/08/26 15:18:47 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

#include "merc.h"
#include "db.h"


/* values for db2.c */
SOCIAL_TYPE	*social_hash[MAX_SOCIAL_HASH];
int		social_count;

/* snarf a socials file */
void load_socials( FILE *fp) {
    SOCIAL_TYPE *social;
    int		i;

    for (i=0;i<MAX_SOCIAL_HASH;i++)
	social_hash[i]=NULL;

    for ( ; ; )
    {
    	char *temp;

        /* clear social */

    	temp = fread_word(fp);
    	if (!strcmp(temp,"#0"))
	    return;  /* done */

        social=new_social();
	social->name=str_dup(temp);
    	fread_to_eol(fp);

        for (i=0;i<8;i++) {
	    temp=fread_string_eol(fp);

	    if (!strcmp(temp,"#")) break; // no more data for this social

	    if (!strcmp(temp,"$")) temp=NULL;

	    switch (i) {
		case 0: social->char_no_arg = temp;	break;
		case 1: social->others_no_arg = temp;	break;
		case 2: social->char_found = temp;	break;
		case 3: social->others_found = temp;	break;
		case 4: social->vict_found = temp;	break;
		case 5: social->char_not_found = temp;	break;
		case 6: social->char_auto = temp;	break;
		case 7: social->others_auto = temp;	break;
	    }
	}

	add_social(social);
    	social_count++;
   }
   return;
}


/*
 * Snarf a mob section.  new style
 */
void load_mobiles( FILE *fp )
{
    MOB_INDEX_DATA *pMobIndex;
    char *shex;
    char s[MSL];

    if ( !area_last )	/* OLC */
    {
	bugf( "Load_mobiles: no #AREA seen yet." );
	abort();
    }

    for ( ; ; )
    {
        int vnum;
        char letter;
        int iHash;

        letter                          = fread_letter( fp );
        if ( letter != '#' ) {
            bugf( "Load_mobiles: # not found." );
            abort();
        }

        vnum                            = fread_number( fp );
        if ( vnum == 0 )
            break;

        fBootDb = FALSE;
        if ( get_mob_index( vnum ) != NULL ) {
            bugf( "Load_mobiles: vnum %d duplicated.", vnum );
            abort();
        }
        fBootDb = TRUE;

        pMobIndex                       = new_mob_index();
        pMobIndex->vnum                 = vnum;
	pMobIndex->area			= area_last;		/* OLC */
	newmobs++;
        pMobIndex->player_name          = fread_string( fp );
        pMobIndex->short_descr          = fread_string( fp );
        pMobIndex->long_descr           = fread_string( fp );
        pMobIndex->description          = fread_string( fp );
	pMobIndex->race		 	= race_lookup(fread_string_temp(fp));
	area_last->vnum_mobs_in_use++;

        pMobIndex->long_descr[0]        = UPPER(pMobIndex->long_descr[0]);
        pMobIndex->description[0]       = UPPER(pMobIndex->description[0]);

	if (area_last->version<1) {
	    long l;
	    int i;

	    STR_ZERO_BIT(pMobIndex->strbit_act_read, MAX_FLAGS);
	    STR_ZERO_BIT(pMobIndex->strbit_act_saved, MAX_FLAGS);
	    STR_ZERO_BIT(pMobIndex->strbit_act_read, MAX_FLAGS);

	    l=fread_flag(fp);
	    for (i=1;i<=32;i++) {
		if (l%2)
		    STR_SET_BIT(pMobIndex->strbit_act_read,i);
		l>>=1;
	    }
	    STR_COPY_STR(pMobIndex->strbit_act,
		       pMobIndex->strbit_act_read,MAX_FLAGS);
	    STR_OR_STR(pMobIndex->strbit_act,
			race_table[pMobIndex->race].strbit_act,MAX_FLAGS);
	    STR_SET_BIT(pMobIndex->strbit_act,ACT_IS_NPC);

	    STR_ZERO_BIT(pMobIndex->strbit_affected_by, MAX_FLAGS);
	    STR_ZERO_BIT(pMobIndex->strbit_affected_saved, MAX_FLAGS);
	    STR_ZERO_BIT(pMobIndex->strbit_affected_read, MAX_FLAGS);

	    l=fread_flag(fp);
	    for (i=1;i<=32;i++) {
		if (l%2)
		    STR_SET_BIT(pMobIndex->strbit_affected_read,i);
		l>>=1;
	    }
	    STR_COPY_STR(pMobIndex->strbit_affected_by,
		   pMobIndex->strbit_affected_read,MAX_FLAGS);
	    STR_OR_STR(pMobIndex->strbit_affected_by,
			race_table[pMobIndex->race].strbit_eff,MAX_FLAGS);

	    pMobIndex->alignment            = fread_number( fp );
	    pMobIndex->group                = fread_number( fp );
	} else {
	    char *shex;

	    if (area_last->version<3) {
		long l;int i;

		STR_ZERO_BIT(pMobIndex->strbit_act_read, MAX_FLAGS);
		STR_ZERO_BIT(pMobIndex->strbit_act_saved, MAX_FLAGS);
		STR_ZERO_BIT(pMobIndex->strbit_act_read, MAX_FLAGS);

		l=fread_flag(fp);
		for (i=1;i<=32;i++) {
		    if (l%2)
			STR_SET_BIT(pMobIndex->strbit_act_read,i);
		    l>>=1;
		}
		STR_COPY_STR(pMobIndex->strbit_act,
			   pMobIndex->strbit_act_read,MAX_FLAGS);
		STR_OR_STR(pMobIndex->strbit_act,
			    race_table[pMobIndex->race].strbit_act,MAX_FLAGS);
		STR_SET_BIT(pMobIndex->strbit_act,ACT_IS_NPC);
	    } else {
		shex=fread_string_temp(fp);
		hextostring(shex,pMobIndex->strbit_act_read,strlen(shex));
		STR_COPY_STR(pMobIndex->strbit_act,
			   pMobIndex->strbit_act_read,MAX_FLAGS);
		STR_OR_STR(pMobIndex->strbit_act,
			    race_table[pMobIndex->race].strbit_act,MAX_FLAGS);
	    }

	    pMobIndex->alignment            = fread_number( fp );
	    pMobIndex->group                = fread_number( fp );

	    shex=fread_string_temp(fp);
	    hextostring(shex,pMobIndex->strbit_affected_read,strlen(shex));
	    STR_COPY_STR(pMobIndex->strbit_affected_by,
		       pMobIndex->strbit_affected_read,MAX_FLAGS);
	    STR_OR_STR(pMobIndex->strbit_affected_by,
			race_table[pMobIndex->race].strbit_eff,MAX_FLAGS);
	}

	pMobIndex->pShop                = NULL;

        pMobIndex->level                = fread_number( fp );
        pMobIndex->hitroll              = fread_number( fp );

	/* read hit dice */
        pMobIndex->hit[DICE_NUMBER]     = fread_number( fp );
        /* 'd'          */                fread_letter( fp );
        pMobIndex->hit[DICE_TYPE]   	= fread_number( fp );
        /* '+'          */                fread_letter( fp );
        pMobIndex->hit[DICE_BONUS]      = fread_number( fp );

 	/* read mana dice */
	pMobIndex->mana[DICE_NUMBER]	= fread_number( fp );
					  fread_letter( fp );
	pMobIndex->mana[DICE_TYPE]	= fread_number( fp );
					  fread_letter( fp );
	pMobIndex->mana[DICE_BONUS]	= fread_number( fp );

	/* read damage dice */
	pMobIndex->damage[DICE_NUMBER]	= fread_number( fp );
					  fread_letter( fp );
	pMobIndex->damage[DICE_TYPE]	= fread_number( fp );
					  fread_letter( fp );
	pMobIndex->damage[DICE_BONUS]	= fread_number( fp );
	pMobIndex->dam_type		= attack_lookup(fread_word(fp));

	/* read armor class */
	pMobIndex->ac[AC_PIERCE]	= fread_number( fp ) * 10;
	pMobIndex->ac[AC_BASH]		= fread_number( fp ) * 10;
	pMobIndex->ac[AC_SLASH]		= fread_number( fp ) * 10;
	pMobIndex->ac[AC_EXOTIC]	= fread_number( fp ) * 10;

	/* read flags and add in data from the race table */
	STR_ZERO_BIT(pMobIndex->strbit_off_flags, MAX_FLAGS);
	STR_ZERO_BIT(pMobIndex->strbit_imm_flags, MAX_FLAGS);
	STR_ZERO_BIT(pMobIndex->strbit_res_flags, MAX_FLAGS);
	STR_ZERO_BIT(pMobIndex->strbit_vuln_flags,MAX_FLAGS);
	STR_ZERO_BIT(pMobIndex->strbit_off_read, MAX_FLAGS);
	STR_ZERO_BIT(pMobIndex->strbit_imm_read, MAX_FLAGS);
	STR_ZERO_BIT(pMobIndex->strbit_res_read, MAX_FLAGS);
	STR_ZERO_BIT(pMobIndex->strbit_vuln_read,MAX_FLAGS);
	STR_ZERO_BIT(pMobIndex->strbit_off_saved, MAX_FLAGS);
	STR_ZERO_BIT(pMobIndex->strbit_imm_saved, MAX_FLAGS);
	STR_ZERO_BIT(pMobIndex->strbit_res_saved, MAX_FLAGS);
	STR_ZERO_BIT(pMobIndex->strbit_vuln_saved,MAX_FLAGS);
	if (area_last->version<1) {
	    long l;
	    int i;

	    l=fread_flag(fp);
	    for (i=1;i<=32;i++) {
		if (l%2)
		    STR_SET_BIT(pMobIndex->strbit_off_read,i);
		l>>=1;
	    }
	    STR_COPY_STR(pMobIndex->strbit_off_flags,
		       pMobIndex->strbit_off_read,MAX_FLAGS);
	    STR_OR_STR(pMobIndex->strbit_off_flags,
			race_table[pMobIndex->race].strbit_off,MAX_FLAGS);

	    l=fread_flag(fp);
	    for (i=1;i<=32;i++) {
		if (l%2)
		    STR_SET_BIT(pMobIndex->strbit_imm_read,i);
		l>>=1;
	    }
	    STR_COPY_STR(pMobIndex->strbit_imm_flags,
		       pMobIndex->strbit_imm_read,MAX_FLAGS);
	    STR_OR_STR(pMobIndex->strbit_imm_flags,
			race_table[pMobIndex->race].strbit_imm,MAX_FLAGS);

	    l=fread_flag(fp);
	    for (i=1;i<=32;i++) {
		if (l%2)
		    STR_SET_BIT(pMobIndex->strbit_res_read,i);
		l>>=1;
	    }
	    STR_COPY_STR(pMobIndex->strbit_res_flags,
		       pMobIndex->strbit_res_read,MAX_FLAGS);
	    STR_OR_STR(pMobIndex->strbit_res_flags,
			race_table[pMobIndex->race].strbit_res,MAX_FLAGS);

	    l=fread_flag(fp);
	    for (i=1;i<=32;i++) {
		if (l%2)
		    STR_SET_BIT(pMobIndex->strbit_vuln_read,i);
		l>>=1;
	    }
	    STR_COPY_STR(pMobIndex->strbit_vuln_flags,
		       pMobIndex->strbit_vuln_read,MAX_FLAGS);
	    STR_OR_STR(pMobIndex->strbit_vuln_flags,
			race_table[pMobIndex->race].strbit_vuln,MAX_FLAGS);

	} else {
	    shex=fread_string_temp(fp);
	    hextostring(shex,pMobIndex->strbit_off_read,strlen(shex));
	    shex=fread_string_temp(fp);
	    hextostring(shex,pMobIndex->strbit_imm_read,strlen(shex));
	    shex=fread_string_temp(fp);
	    hextostring(shex,pMobIndex->strbit_res_read,strlen(shex));
	    shex=fread_string_temp(fp);
	    hextostring(shex,pMobIndex->strbit_vuln_read,strlen(shex));

	    STR_COPY_STR(pMobIndex->strbit_off_flags,
			pMobIndex->strbit_off_read,MAX_FLAGS);
	    STR_OR_STR(pMobIndex->strbit_off_flags,
			race_table[pMobIndex->race].strbit_off,MAX_FLAGS);
	    STR_COPY_STR(pMobIndex->strbit_imm_flags,
			pMobIndex->strbit_imm_read,MAX_FLAGS);
	    STR_OR_STR(pMobIndex->strbit_imm_flags,
			race_table[pMobIndex->race].strbit_imm,MAX_FLAGS);
	    STR_COPY_STR(pMobIndex->strbit_res_flags,
			pMobIndex->strbit_res_read,MAX_FLAGS);
	    STR_OR_STR(pMobIndex->strbit_res_flags,
			race_table[pMobIndex->race].strbit_res,MAX_FLAGS);
	    STR_COPY_STR(pMobIndex->strbit_vuln_flags,
			pMobIndex->strbit_vuln_read,MAX_FLAGS);
	    STR_OR_STR(pMobIndex->strbit_vuln_flags,
			race_table[pMobIndex->race].strbit_vuln,MAX_FLAGS);
	}

	/* vital statistics */
	pMobIndex->start_pos		= position_lookup(fread_word(fp));
	pMobIndex->default_pos		= position_lookup(fread_word(fp));
	pMobIndex->sex			= sex_lookup(fread_word(fp));

	pMobIndex->wealth		= fread_number( fp );

	pMobIndex->form_mem[0]		= fread_flag( fp );
	pMobIndex->form			= pMobIndex->form_mem[0] | race_table[pMobIndex->race].form;
	pMobIndex->parts_mem[0]		= fread_flag( fp );
	pMobIndex->parts		= pMobIndex->parts_mem[0] | race_table[pMobIndex->race].parts;

	/* size */
	pMobIndex->size			= size_lookup(fread_word(fp));
	pMobIndex->material		= str_dup(fread_word( fp ));
	if(pMobIndex->material[0]=='0' && pMobIndex->material[1]=='\0')
	    pMobIndex->material[0]='\0';

	pMobIndex->pueblo_picture	= str_dup("");

//	pMobIndex->act_mem[1]=0;
//	pMobIndex->affected_mem[1]=0;
//	pMobIndex->affected2_mem[1]=0;
//	pMobIndex->off_mem[1]=0;
//	pMobIndex->imm_mem[1]=0;
//	pMobIndex->res_mem[1]=0;
//	pMobIndex->vuln_mem[1]=0;
	pMobIndex->form_mem[1]=0;
	pMobIndex->parts_mem[1]=0;

	for ( ; ; )
        {
            letter = fread_letter( fp );

            if ( letter == 'E' )
            {
                EXTRA_DESCR_DATA *ed;

                ed                      = new_extra_descr();
                ed->keyword             = fread_string( fp );
                ed->description         = fread_string( fp );
                ed->next                = pMobIndex->extra_descr;
                pMobIndex->extra_descr  = ed;
            }

            else if (letter == 'F')
            {
		char *word;
		long vector,l;
		int i;

                word                    = fread_word(fp);

		if (!str_cmp(word,"act"))
		{
		    if (area_last->version<3) {
			l= fread_flag(fp);
			for (i=1;i<=32;i++) {
			    if (l%2) {
				STR_REMOVE_BIT(pMobIndex->strbit_act,i);
				STR_SET_BIT(pMobIndex->strbit_act_saved,i);
			    }
			    l>>=1;
			}
		    } else {
			shex = fread_string_temp(fp);
			hextostring(shex,s,strlen(shex));
			STR_OR_STR(pMobIndex->strbit_act_saved,s,MAX_FLAGS);
			STR_NOT_STR(s,MAX_FLAGS);
			STR_AND_STR(pMobIndex->strbit_act,s,MAX_FLAGS);
		    }
		}
		else if (!str_cmp(word,"for"))
		{
		    vector		= fread_flag(fp);
		    REMOVE_BIT(pMobIndex->form,vector);
		    SET_BIT(pMobIndex->form_mem[1],vector);
		}
		else if (!str_cmp(word,"par"))
		{
		    vector		= fread_flag(fp);
		    REMOVE_BIT(pMobIndex->parts,vector);
		    SET_BIT(pMobIndex->parts_mem[1],vector);
		}
		else if (!str_cmp(word,"aff2"))
		{
		    if (area_last->version<1) {
			l= fread_flag(fp);
			for (i=1;i<=32;i++) {
			    if (l%2) {
				STR_REMOVE_BIT(pMobIndex->strbit_affected_by2,i);
				STR_SET_BIT(pMobIndex->strbit_affected_saved2,i);
			    }
			    l>>=1;
			}
		    } else {
			shex = fread_string_temp(fp);
			hextostring(shex,s,strlen(shex));
			STR_OR_STR(pMobIndex->strbit_affected_saved2,s,MAX_FLAGS);
			STR_NOT_STR(s,MAX_FLAGS);
			STR_AND_STR(pMobIndex->strbit_affected_by2,s,MAX_FLAGS);
		    }
		}
		else if (!str_cmp(word,"aff"))
		{
		    if (area_last->version<1) {
			l= fread_flag(fp);
			for (i=1;i<=32;i++) {
			    if (l%2) {
				STR_REMOVE_BIT(pMobIndex->strbit_affected_by,i);
				STR_SET_BIT(pMobIndex->strbit_affected_saved,i);
			    }
			    l>>=1;
			}
		    } else {
			shex = fread_string_temp(fp);
			hextostring(shex,s,strlen(shex));
			STR_OR_STR(pMobIndex->strbit_affected_saved,s,MAX_FLAGS);
			STR_NOT_STR(s,MAX_FLAGS);
			STR_AND_STR(pMobIndex->strbit_affected_by,s,MAX_FLAGS);
		    }
		}
		else if (!str_cmp(word,"off"))
		{
		    if (area_last->version<1) {
			l= fread_flag(fp);
			for (i=1;i<=32;i++) {
			    if (l%2) {
				STR_REMOVE_BIT(pMobIndex->strbit_off_flags,i);
				STR_SET_BIT(pMobIndex->strbit_off_saved,i);
			    }
			    l>>=1;
			}
		    } else {
			shex = fread_string_temp(fp);
			hextostring(shex,s,strlen(shex));
			STR_OR_STR(pMobIndex->strbit_off_saved,s,MAX_FLAGS);
			STR_NOT_STR(s,MAX_FLAGS);
			STR_AND_STR(pMobIndex->strbit_off_flags,s,MAX_FLAGS);
		    }
		}
		else if (!str_cmp(word,"imm"))
		{
		    if (area_last->version<1) {
			l= fread_flag(fp);
			for (i=1;i<=32;i++) {
			    if (l%2) {
				STR_REMOVE_BIT(pMobIndex->strbit_imm_flags,i);
				STR_SET_BIT(pMobIndex->strbit_imm_saved,i);
			    }
			    l>>=1;
			}
		    } else {
			shex = fread_string_temp(fp);
			hextostring(shex,s,strlen(shex));
			STR_OR_STR(pMobIndex->strbit_imm_saved,s,MAX_FLAGS);
			STR_NOT_STR(s,MAX_FLAGS);
			STR_AND_STR(pMobIndex->strbit_imm_flags,s,MAX_FLAGS);
		    }
		}
		else if (!str_cmp(word,"res"))
		{
		    if (area_last->version<1) {
			l= fread_flag(fp);
			for (i=1;i<=32;i++) {
			    if (l%2) {
				STR_REMOVE_BIT(pMobIndex->strbit_res_flags,i);
				STR_SET_BIT(pMobIndex->strbit_res_saved,i);
			    }
			    l>>=1;
			}
		    } else {
			shex = fread_string_temp(fp);
			hextostring(shex,s,strlen(shex));
			STR_OR_STR(pMobIndex->strbit_res_saved,s,MAX_FLAGS);
			STR_NOT_STR(s,MAX_FLAGS);
			STR_AND_STR(pMobIndex->strbit_res_flags,s,MAX_FLAGS);
		    }
		}
		else if (!str_cmp(word,"vul"))
		{
		    if (area_last->version<1) {
			l= fread_flag(fp);
			for (i=1;i<=32;i++) {
			    if (l%2) {
				STR_REMOVE_BIT(pMobIndex->strbit_vuln_flags,i);
				STR_SET_BIT(pMobIndex->strbit_vuln_saved,i);
			    }
			    l>>=1;
			}
		    } else {
			shex = fread_string_temp(fp);
			hextostring(shex,s,strlen(shex));

			STR_OR_STR(pMobIndex->strbit_vuln_saved,s,MAX_FLAGS);
			STR_NOT_STR(s,MAX_FLAGS);
			STR_AND_STR(pMobIndex->strbit_vuln_flags,s,MAX_FLAGS);
		    }
		}
		else
		{
		    bugf("Flag remove: flag '%s' not found.",word);
		    abort();
		}
	     }
	     else if ( letter == 'M' )
	     {
	        bugf("MOBProgs: Old mobprogs detected.");
	        fread_to_eol(fp);
	     }
	     else if (letter == 'N' ) /* new fields */
	     {
		char *word;

                word                    = fread_word(fp);

		if(!str_cmp(word,"eff2") || !str_cmp(word,"aff2"))
		{
		    if (area_last->version<1) {
			long l;
			int i;

			l=fread_flag(fp);
			for (i=1;i<=32;i++) {
			    if (l%2)
				STR_SET_BIT(pMobIndex->strbit_affected_read2,i);
			    l>>=1;
			}
			STR_COPY_STR(pMobIndex->strbit_affected_by2,
				   pMobIndex->strbit_affected_read2,MAX_FLAGS);
			STR_OR_STR(pMobIndex->strbit_affected_by2,
				    race_table[pMobIndex->race].strbit_eff2,MAX_FLAGS);
		    } else {
			shex=fread_string_temp(fp);
			hextostring(shex,pMobIndex->strbit_affected_read2,strlen(shex));
			STR_COPY_STR(pMobIndex->strbit_affected_by2,
				   pMobIndex->strbit_affected_read2,MAX_FLAGS);
			STR_OR_STR(pMobIndex->strbit_affected_by2,
				    race_table[pMobIndex->race].strbit_eff2,MAX_FLAGS);
		    }
		}
		else
		{
		    bugf("New field: unknown fieldname '%s'",word);
		    abort();
		}
	     }
	     else if (letter == 'L') { /* New mob progs */
		pMobIndex->mprog_vnum =  fread_number(fp);
	     }
	     else if (letter == 'Q') { // pueblo-picture
		pMobIndex->pueblo_picture=fread_string(fp);
	     }
	     else if (letter=='P') {	// property
		char key[MAX_STRING_LENGTH];
		char type[MAX_STRING_LENGTH];
		char value[MAX_STRING_LENGTH];
		int i;
		bool b;
		char c;
		long l;

		if (area_last->version<5) {
		    char *temp;
		    temp=fread_string_eol(fp);
		    temp=one_argument(temp,key);
		    temp=one_argument(temp,type);
		    temp=one_argument(temp,value);
		} else {
		    strcpy(key,fread_string_temp(fp));
		    strcpy(type,fread_string_temp(fp));
		    strcpy(value,fread_string_temp(fp));
		}

		switch (which_keyword(type,"int","bool","string",
			"char","long",NULL)) {
		    case 1:
			i=atoi(value);
			SetDCharProperty(pMobIndex,PROPERTY_INT,key,&i);
			break;
		    case 2:
			switch (which_keyword(value,"true","false",NULL)) {
			    case 1:
				b=TRUE;
				SetDCharProperty(pMobIndex,PROPERTY_BOOL,key,&b);
				break;
			    case 2:
				b=FALSE;
				SetDCharProperty(pMobIndex,PROPERTY_BOOL,key,&b);
				break;
			    default:
				;
			}
			break;
		    case 3:
			SetDCharProperty(pMobIndex,PROPERTY_STRING,key,value);
			break;
		    case 4:
			c=value[0];
			SetDCharProperty(pMobIndex,PROPERTY_CHAR,key,&c);
			break;
		    case 5:
			l=atol(value);
			SetDCharProperty(pMobIndex,PROPERTY_LONG,key,&l);
			break;
		    default:
			bugf("Property: unknown keyword '%s'",type);
                }

	     } else {
		funread_letter(fp,letter);
		break;
	     }
	}

	STR_COPY_STR(s,race_table[pMobIndex->race].strbit_act,MAX_FLAGS);
	STR_NOT_STR(s,MAX_FLAGS);
	STR_AND_STR(pMobIndex->strbit_act_read,s,MAX_FLAGS);
	STR_COPY_STR(s,race_table[pMobIndex->race].strbit_off,MAX_FLAGS);
	STR_NOT_STR(s,MAX_FLAGS);
	STR_AND_STR(pMobIndex->strbit_off_read,s,MAX_FLAGS);
	STR_COPY_STR(s,race_table[pMobIndex->race].strbit_imm,MAX_FLAGS);
	STR_NOT_STR(s,MAX_FLAGS);
	STR_AND_STR(pMobIndex->strbit_imm_read,s,MAX_FLAGS);
	STR_COPY_STR(s,race_table[pMobIndex->race].strbit_res,MAX_FLAGS);
	STR_NOT_STR(s,MAX_FLAGS);
	STR_AND_STR(pMobIndex->strbit_res_read,s,MAX_FLAGS);
	STR_COPY_STR(s,race_table[pMobIndex->race].strbit_vuln,MAX_FLAGS);
	STR_NOT_STR(s,MAX_FLAGS);
	STR_AND_STR(pMobIndex->strbit_vuln_read,s,MAX_FLAGS);
	pMobIndex->form_mem[0]		= (~race_table[pMobIndex->race].form) & pMobIndex->form_mem[0];
	pMobIndex->parts_mem[0]		= (~race_table[pMobIndex->race].parts) & pMobIndex->parts_mem[0];
	STR_COPY_STR(s,race_table[pMobIndex->race].strbit_eff,MAX_FLAGS);
	STR_NOT_STR(s,MAX_FLAGS);
	STR_AND_STR(pMobIndex->strbit_affected_read,s,MAX_FLAGS);
	STR_COPY_STR(s,race_table[pMobIndex->race].strbit_eff2,MAX_FLAGS);
	STR_NOT_STR(s,MAX_FLAGS);
	STR_AND_STR(pMobIndex->strbit_affected_read2,s,MAX_FLAGS);

	STR_AND_STR(pMobIndex->strbit_act_saved,
		    race_table[pMobIndex->race].strbit_act,MAX_FLAGS);
	STR_AND_STR(pMobIndex->strbit_affected_saved,
		    race_table[pMobIndex->race].strbit_eff,MAX_FLAGS);
	STR_AND_STR(pMobIndex->strbit_affected_saved2,
		    race_table[pMobIndex->race].strbit_eff2,MAX_FLAGS);
	STR_AND_STR(pMobIndex->strbit_off_saved,
		    race_table[pMobIndex->race].strbit_off,MAX_FLAGS);
	STR_AND_STR(pMobIndex->strbit_imm_saved,
		    race_table[pMobIndex->race].strbit_imm,MAX_FLAGS);
	STR_AND_STR(pMobIndex->strbit_res_saved,
		    race_table[pMobIndex->race].strbit_res,MAX_FLAGS);
	STR_AND_STR(pMobIndex->strbit_vuln_saved,
		    race_table[pMobIndex->race].strbit_vuln,MAX_FLAGS);
	pMobIndex->form_mem[1]		= race_table[pMobIndex->race].form & pMobIndex->form_mem[1];
	pMobIndex->parts_mem[1]		= race_table[pMobIndex->race].parts & pMobIndex->parts_mem[1];

        iHash                   = vnum % MAX_KEY_HASH;
        pMobIndex->next         = mob_index_hash[iHash];
        mob_index_hash[iHash]   = pMobIndex;
	max_vnum_mob = max_vnum_mob < vnum ? vnum : max_vnum_mob;  /* OLC */
	assign_area_vnum( vnum );				   /* OLC */
    }

    return;
}

/*
 * Snarf an obj section. new style
 */
void load_objects( FILE *fp )
{
    OBJ_INDEX_DATA *pObjIndex;

    if ( !area_last )	/* OLC */
    {
	bugf( "Load_objects: no #AREA seen yet." );
	abort();
    }

    for ( ; ; )
    {
        int vnum;
        char letter;
        int iHash;

        letter                          = fread_letter( fp );
        if ( letter != '#' )
        {
            bugf( "Load_objects: # not found." );
            abort();
        }

        vnum                            = fread_number( fp );
        if ( vnum == 0 )
            break;

        fBootDb = FALSE;
        if ( get_obj_index( vnum ) != NULL )
        {
            bugf( "Load_objects: vnum %d duplicated.", vnum );
            abort();
        }
        fBootDb = TRUE;

        pObjIndex                       = new_obj_index();
        pObjIndex->vnum                 = vnum;
        pObjIndex->area			= area_last;		/* OLC */
	pObjIndex->reset_num		= 0;
	newobjs++;
        pObjIndex->name                 = fread_string( fp );
        pObjIndex->short_descr          = fread_string( fp );
        pObjIndex->description          = fread_string( fp );
        pObjIndex->material		= fread_string( fp );

	area_last->vnum_objects_in_use++;

        pObjIndex->item_type            = table_find_name(fread_word(fp),item_table);
	STR_ZERO_BIT(pObjIndex->strbit_extra_flags,MAX_FLAGS);
	if (area_last->version<1) {
	    long l;
	    int i;

	    l=fread_flag(fp);
	    for (i=1;i<=32;i++) {
		if (l%2)
		    STR_SET_BIT(pObjIndex->strbit_extra_flags,i);
		l>>=1;
	    }
	} else {
	    char *s;

	    s=fread_string_temp(fp);
	    hextostring(s,pObjIndex->strbit_extra_flags,strlen(s));
	}
        pObjIndex->wear_flags           = fread_flag( fp );
	switch(pObjIndex->item_type)
	{
	case ITEM_WEAPON:
	    pObjIndex->value[0]		= weapon_type(fread_word(fp));
	    pObjIndex->value[1]		= fread_number(fp);
	    pObjIndex->value[2]		= fread_number(fp);
	    pObjIndex->value[3]		= attack_lookup(fread_word(fp));
	    pObjIndex->value[4]		= fread_flag(fp);
	    break;
	case ITEM_CONTAINER:
	    pObjIndex->value[0]		= fread_number(fp);
	    pObjIndex->value[1]		= fread_flag(fp);
	    pObjIndex->value[2]		= fread_number(fp);
	    pObjIndex->value[3]		= fread_number(fp);
	    pObjIndex->value[4]		= fread_number(fp);
	    break;
        case ITEM_DRINK_CON:
	case ITEM_FOUNTAIN:
            pObjIndex->value[0]         = fread_number(fp);
            pObjIndex->value[1]         = fread_number(fp);
            pObjIndex->value[2]         = liq_lookup(fread_word(fp));
            pObjIndex->value[3]         = fread_number(fp);
            pObjIndex->value[4]         = fread_number(fp);
            break;
	case ITEM_WAND:
	case ITEM_STAFF:
	    pObjIndex->value[0]		= fread_number(fp);
	    pObjIndex->value[1]		= fread_number(fp);
	    pObjIndex->value[2]		= fread_number(fp);
	    pObjIndex->value[3]		= skill_lookup(fread_word(fp));
	    pObjIndex->value[4]		= fread_number(fp);
	    break;
	case ITEM_POTION:
	case ITEM_PILL:
	case ITEM_SCROLL:
 	    pObjIndex->value[0]		= fread_number(fp);
	    pObjIndex->value[1]		= skill_lookup(fread_word(fp));
	    pObjIndex->value[2]		= skill_lookup(fread_word(fp));
	    pObjIndex->value[3]		= skill_lookup(fread_word(fp));
	    pObjIndex->value[4]		= skill_lookup(fread_word(fp));
	    break;
	default:
            pObjIndex->value[0]             = fread_flag( fp );
            pObjIndex->value[1]             = fread_flag( fp );
            pObjIndex->value[2]             = fread_flag( fp );
            pObjIndex->value[3]             = fread_flag( fp );
	    pObjIndex->value[4]		    = fread_flag( fp );
	    break;
	}
	pObjIndex->level		= fread_number( fp );
        pObjIndex->weight               = fread_number( fp );
        pObjIndex->cost                 = fread_number( fp );

        /* condition */
        letter 				= fread_letter( fp );
	switch (letter)
 	{
	    case ('P') :		pObjIndex->condition = 100; break;
	    case ('G') :		pObjIndex->condition =  90; break;
	    case ('A') :		pObjIndex->condition =  75; break;
	    case ('W') :		pObjIndex->condition =  50; break;
	    case ('D') :		pObjIndex->condition =  25; break;
	    case ('B') :		pObjIndex->condition =  10; break;
	    case ('R') :		pObjIndex->condition =   0; break;
	    default:			pObjIndex->condition = 100; break;
	}

	pObjIndex->pueblo_picture	= str_dup("");

        for ( ; ; )
        {
            char letter;

            letter = fread_letter( fp );

            if ( letter == 'A' )
            {
                EFFECT_DATA *pef;

                pef                     = new_effect();
		pef->where		= TO_OBJECT;
                pef->type               = -1;
                pef->level              = pObjIndex->level;
                pef->duration           = -1;
                pef->location           = fread_number( fp );
                pef->modifier           = fread_number( fp );
                pef->bitvector		= EFF_NONE;
		pef->arg1		= 0;
                pef->next               = pObjIndex->affected;
                pObjIndex->affected     = pef;
            }

            else if ( letter == 'B' )
            {
                EFFECT_DATA *pef;

                pef                     = new_effect();
		pef->where		= TO_OBJECT2;
                pef->type               = -1;
                pef->level              = pObjIndex->level;
                pef->duration           = -1;
                pef->location           = fread_number( fp );
                pef->modifier           = fread_number( fp );
		if (area_last->version<1) {
		    long l;
		    int i;

		    l=fread_flag(fp);
		    for (i=1;i<=32;i++) {
			if (l%2) {
			    pef->bitvector = i;
			    break;
			}
			l>>=1;
		    }
		} else
		    pef->bitvector      = fread_number( fp );
		pef->arg1		= fread_number( fp );
		// old arg2 for effects
		if (area_last->version<4) fread_string( fp );
                pef->next               = pObjIndex->affected;
                pObjIndex->affected     = pef;

		/* If the flag wasn't set yet. */
		STR_SET_BIT(pObjIndex->strbit_extra_flags2,pef->bitvector);
            }

	    else if (letter == 'F')
            {
                EFFECT_DATA *pef;

                pef                     = new_effect();
		letter 			= fread_letter(fp);
		switch (letter)
	 	{
		case 'A':
                    pef->where          = TO_EFFECTS;
		    break;
		case 'B':
		    pef->where		= TO_EFFECTS2;
		    break;
		case 'I':
		    pef->where		= TO_IMMUNE;
		    break;
		case 'R':
		    pef->where		= TO_RESIST;
		    break;
		case 'S':
		    pef->where		= TO_SKILLS;
		    break;
		case 'V':
		    pef->where		= TO_VULN;
		    break;
		default:
            	    bugf( "Load_objects: Bad where on flag set '%c'.",letter );
		    abort();
		}
                pef->type               = -1;
                pef->level              = pObjIndex->level;
                pef->duration           = -1;
                pef->location           = fread_number(fp);
                pef->modifier           = fread_number(fp);
		if (area_last->version<1) {
		    long l;
		    int i;

		    l=fread_flag(fp);
		    for (i=1;i<=32;i++) {
			if (l%2) {
			    pef->bitvector = i;
			    break;
			}
			l>>=1;
		    }
		} else
		    pef->bitvector      = fread_number( fp );

		if(pef->where!=TO_EFFECTS2)
		    pef->arg1		= 0;
		else {
		    pef->arg1		= fread_number(fp);
		    // old arg2 for effects
		    if (area_last->version<4) fread_string(fp);
		}
                pef->next               = pObjIndex->affected;
                pObjIndex->affected     = pef;
            }

            else if ( letter == 'E' )
            {
                EXTRA_DESCR_DATA *ed;

                ed                      = new_extra_descr();
                ed->keyword             = fread_string( fp );
                ed->description         = fread_string( fp );
                ed->next                = pObjIndex->extra_descr;
                pObjIndex->extra_descr  = ed;
            }

            else if ( letter == 'N' ) /* new fields */
            {
		char *word;

		word=fread_word( fp );

		if(!str_prefix(word,"extra2"))
		{
		    STR_ZERO_BIT(pObjIndex->strbit_extra_flags2,MAX_FLAGS);
		    if (area_last->version<1) {
			long l;
			int i;

			l=fread_flag(fp);
			for (i=1;i<=32;i++) {
			    if (l%2)
				STR_SET_BIT(pObjIndex->strbit_extra_flags2,i);
			    l>>=1;
			}
		    } else {
			char *s;

			s=fread_string_temp(fp);
			hextostring(s,pObjIndex->strbit_extra_flags2,strlen(s));
		    }
		}
		else
		{
		    bugf("New field: unknown fieldname '%s'",word);
		    abort();
		}
	    } else if (letter == 'L') { /* New obj progs */
		pObjIndex->oprog_vnum =  fread_number(fp);
	    } else if (letter=='Q') {	// pueblo picture
		pObjIndex->pueblo_picture=fread_string(fp);
	    } else if (letter=='P') {	// property
		char key[MAX_STRING_LENGTH];
		char type[MAX_STRING_LENGTH];
		char value[MAX_STRING_LENGTH];
		int i;
		bool b;
		char c;
		long l;

		if (area_last->version<5) {
		    char *temp;
		    temp=fread_string_eol(fp);
		    temp=one_argument(temp,key);
		    temp=one_argument(temp,type);
		    temp=one_argument(temp,value);
		} else {
		    strcpy(key,fread_string_temp(fp));
		    strcpy(type,fread_string_temp(fp));
		    strcpy(value,fread_string_temp(fp));
		}

		switch (which_keyword(type,"int","bool","string",
			"char","long",NULL)) {
		    case 1:
			i=atoi(value);
			SetDObjectProperty(pObjIndex,PROPERTY_INT,key,&i);
			break;
		    case 2:
			switch (which_keyword(value,"true","false",NULL)) {
			    case 1:
				b=TRUE;
				SetDObjectProperty(pObjIndex,PROPERTY_BOOL,key,&b);
				break;
			    case 2:
				b=FALSE;
				SetDObjectProperty(pObjIndex,PROPERTY_BOOL,key,&b);
				break;
			    default:
				;
			}
			break;
		    case 3:
			SetDObjectProperty(pObjIndex,PROPERTY_STRING,key,value);
			break;
		    case 4:
			c=value[0];
			SetDObjectProperty(pObjIndex,PROPERTY_CHAR,key,&c);
			break;
		    case 5:
			l=atol(value);
			SetDObjectProperty(pObjIndex,PROPERTY_LONG,key,&l);
			break;
		    default:
			bugf("Property: unknown keyword '%s'",type);
                }
	    } else {
                funread_letter(fp, letter );
                break;
            }
        }

        iHash                   = vnum % MAX_KEY_HASH;
        pObjIndex->next         = obj_index_hash[iHash];
        obj_index_hash[iHash]   = pObjIndex;
	max_vnum_obj = max_vnum_obj < vnum ? vnum : max_vnum_obj;  /* OLC */
	assign_area_vnum( vnum );				   /* OLC */
    }

    return;
}

