//
// $Id: fd_http.c,v 1.37 2001/12/21 11:05:41 mavetju Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

///////////////////////////////////////////////////////////////////////////////
//
// Commands implemented
// ====================
//
// /who		Displays all players currently in the mud:
//		level|race|class|clan|flags|name|title
//		whoname|clan|flags|name|title
//
// /help	Displays help-index
//   /help_<?>	Displays help-index for everything with a certain character
//   /<item>	Displays help for <item>.
//
// /pmemory	Displays the output of the memory command
//
// /pclan	Displays the list of clans
// /pclan/<vnum>
//		Display the info of the clan with that vnum 
//
// /pareas	Displays all areas in the format:
//		vnum|name|creator|lvnum|uvnum|llevel|ulevel
// /parea/<vnum>
//		Display area info for area <vnum>
//
// /pics_o	Show all objects with a picture
// /pics_m	Show all objects with a picture
// /pics_r	Show all objects with a picture
//
///////////////////////////////////////////////////////////////////////////////

/*
 * 19971227 EG	Modified html_who() to give cleaner HTML code
 * 19971227 EG  Modified html_help() to work the same was as the
		help in the mud does.
 * 19971228 EG	Added HTMLheader() to remove mess of HTML headers
 */

#include "merc.h"
#include "color.h"
#include "olc.h"

#ifdef HAS_HTTP

DECLARE_HTML_FUN(	html_help	);
DECLARE_HTML_FUN(	html_parea	);
DECLARE_HTML_FUN(	html_pareas	);
DECLARE_HTML_FUN(	html_pclan	);
DECLARE_HTML_FUN(	html_pics_m	);
DECLARE_HTML_FUN(	html_pics_o	);
DECLARE_HTML_FUN(	html_pics_r	);
DECLARE_HTML_FUN(	html_pmemory	);
DECLARE_HTML_FUN(	html_root	);
DECLARE_HTML_FUN(	html_who	);

void HTTPheader(DESCRIPTOR_DATA *d,char *returncode,char *mime);

static char *bad_request=
  "<HTML>\n"
  "<HEAD><TITLE>400 Bad Request</TITLE></HEAD>\n"
  "<BODY>\n"
  "<H1>400 Bad Request</H1>\n"
  "<P>Your client sent a query that this server could not understand.</P>\n"
  "<P>Reason: Invalid or unsupported method.</P>\n"
  "</BODY></HTML>\n";
static char *not_found=
  "<HTML>\n"
  "<HEAD><TITLE>404 Not Found</TITLE></HEAD>\n"
  "<BODY>\n"
  "<H1>404 Not Found</H1>\n"
  "<P>The requested URL was not found on this server.</P>\n"
  "</BODY></HTML>\n";

static struct html_page {
    char *name;
    HTML_FUN *html_fun;
} html_page[]={
    {	"help",		html_help	},
    {	"parea",	html_parea	},
    {	"pareas",	html_pareas	},
    {	"pclan",	html_pclan	},
    {	"pics_m",	html_pics_m	},
    {	"pics_o",	html_pics_o	},
    {	"pics_r",	html_pics_r	},
    {	"pmemory",	html_pmemory	},
    {	"who",		html_who	},
    {	"",		html_root	},
    {	NULL,		NULL		}
};

char *get_http_command(char *argument,char *arg) {
    while (*argument=='/') argument++;
    while (*argument!='\0' && *argument!='/') *(arg++)=*(argument++);
    if (*argument=='/') argument++;
    *arg='\0';
    return argument;
}

void http_get(DESCRIPTOR_DATA *d,char *argument) {
    char command[MAX_INPUT_LENGTH];
    struct html_page *page;

    argument=get_http_command(argument,command);
    wiznet(WIZ_HTTP_DEBUG,0,NULL,NULL,"HTTP-debug: %s requests '%s %s'",
	dns_gethostname(d),command,argument);

    for (page=html_page;page->name;page++) {
	if (!str_cmp(command,page->name)) {
	    (*page->html_fun)(d,argument);
	    return;
	}
    }

    HTTPheader(d,"404 Not Found","text/html");
    write_to_buffer(d, not_found, 0);
    mud_data.http_errors++;
}

/*
>GET /who HTTP/1.0<
>Connection: Keep-Alive<
>User-Agent: Mozilla/3.0 (X11; I; FreeBSD 2.2.1-RELEASE i386)<
>Host: localhost:4001<
>Accept: image/gif, image/x-xbitmap, image/jpeg, image/pjpeg<
>Authorization: Basic dGVzdDp0ZXN0amU=<
*/

void run_http_server( DESCRIPTOR_DATA *d )
{
    char cmd[MAX_INPUT_LENGTH],*argument;
    char arg[MAX_INPUT_LENGTH];

    argument=d->incomm;
    logf("[%d] HTTP-command: %s",d->descriptor,argument);
    argument=one_argument(argument,cmd);

    if(*cmd)
    {
        one_argument(argument,arg);

        if(!str_cmp("GET",cmd)) http_get(d,arg);
        else {
	    write_to_buffer( d, bad_request, 0 );
	    mud_data.http_errors++;
	}

        close_socket(d);
    }
}



bool can_see_html(CHAR_DATA *wch)
{
    if(IS_AFFECTED(wch,EFF_INVISIBLE)) return FALSE;
    if(wch->invis_level) return FALSE;
    if(wch->incog_level) return FALSE;
    return TRUE;
}

/* HTML goodies */
void HTTPheader(DESCRIPTOR_DATA *output,char *returncode,char *mime) {
    write_to_buffer(output,"HTTP/1.0 ",0);
    write_to_buffer(output,returncode,0);
    write_to_buffer(output,
	"\n"
	"Content-Type: ",0);
    write_to_buffer(output,mime,0);
    write_to_buffer(output,
	"\n"
	"Server: Fatal Dimensions WebServer\n"
	"\n",0);
}

/* The html commands */

void html_root(DESCRIPTOR_DATA *output,char *argument) {
    HTTPheader(output,"200 OK","text/html");
    write_to_buffer(output,
	"<TITLE>Fatal Dimensions</TITLE>\n"
	"<H1>Fatal Dimensions</H1>\n"
	"This is not the Fatal Dimensions website. The website can be "
	"found <a href='"URLBASE"'>here</a>.\n"
	"<p>\n"
	"If you're interested in this webbrowser, you can do the following"
	"things here: <ul>\n"
	"<li>See the <a href='/who'>who-list</a>\n"
	"<li>See the <a href='/help'>help</a>\n"
	"<li>See the <a href='/pareas'>areas</a>\n"
	"<li>See the <a href='/pmemory'>memory overview</a>\n"
	"</ul>\n",
	0);
}


void html_pareas( DESCRIPTOR_DATA *output,char *argument) {
    AREA_DATA *pArea;
    char buf[MSL];

    HTTPheader(output,"200 OK","text/html");
    for(pArea=area_first;pArea;pArea=pArea->next) {
	sprintf(buf,"%d|%s|%s|%d|%d|%d|%d\n",
	    pArea->vnum,
	    pArea->name,
	    pArea->creator,
	    pArea->lvnum,
	    pArea->uvnum,
	    pArea->llevel,
	    pArea->ulevel
	);
	write_to_buffer(output,buf,0);
    }
}

void html_parea( DESCRIPTOR_DATA *output,char *argument) {
    AREA_DATA *pArea;
    char buf[MSL];
    int vnum=atoi(argument);

    HTTPheader(output,"200 OK","text/html");

    if ((pArea=get_area_data(vnum))==NULL) {
	write_to_buffer(output,"No such area",0);
	return;
    }

    sprintf(buf,"%d|%s|%s|%d|%d|%d|%d\n",
	pArea->vnum,
	pArea->name,
	pArea->creator,
	pArea->lvnum,
	pArea->uvnum,
	pArea->llevel,
	pArea->ulevel
    );
    write_to_buffer(output,buf,0);

    write_to_buffer(output,pArea->description,0);
}


void html_who( DESCRIPTOR_DATA *output,char *argument) {
    CHAR_DATA *wch;
    int nMatch;
    char buf[2*MAX_STRING_LENGTH];
    char clanname[MAX_STRING_LENGTH];
    char playername[MAX_STRING_LENGTH];
    char htmlbuf[2*MAX_STRING_LENGTH];

    mud_data.http_who_connections++;

    HTTPheader(output,"200 OK","text/html");

    nMatch = 0;
    for ( wch=player_list; wch!=NULL; wch=wch->next_player ) {
        char const *class;

	if (!can_see_html(wch))
	    continue;

        nMatch++;

        /*
         * Figure out what to print for class.
	 */
	class = class_table[wch->class].who_name;
	switch ( wch->level ) {
	    default: break;
	    case MAX_LEVEL - 0 : class = "IMP";     break;
	    case MAX_LEVEL - 1 : class = "CRE";     break;
	    case MAX_LEVEL - 2 : class = "SUP";     break;
	    case MAX_LEVEL - 3 : class = "DEI";     break;
	    case MAX_LEVEL - 4 : class = "GOD";     break;
	    case MAX_LEVEL - 5 : class = "IMM";     break;
	    case MAX_LEVEL - 6 : class = "DEM";     break;
	    case MAX_LEVEL - 7 : class = "ANG";     break;
	    case MAX_LEVEL - 8 : class = "AVA";     break;
	}

        if (wch->clan==NULL)
	    strcpy(clanname,"&nbsp;");
	else {
	    char buf[MSL];
	    sprintf(buf,"[%s %s]",
		wch->clan->clan_info->who_name,
		wch->clan->rank_info->who_name);
	    ascii_to_html(clanname,buf,0);
	}
	sprintf(buf,"%s|%s",wch->name,get_afk_title(wch));
	ascii_to_html(playername,buf,0);

	/*
	 * Format it up.
	 */
	if (wch->pcdata->whoname[0]) {
	  /* Who name player */
	  ascii_to_html(htmlbuf,wch->pcdata->whoname,0);
	  sprintf(buf,
	    "%s|%s|%s%s%s|%s\n",
	    htmlbuf,
	    clanname,
	    STR_IS_SET(wch->strbit_comm,COMM_AFK) ? "[AFK] ":"",
	    STR_IS_SET(wch->strbit_act,PLR_KILLER) ? "(KILLER) ":"",
	    STR_IS_SET(wch->strbit_act,PLR_THIEF)  ? "(THIEF) ":"",
	    playername);
	} else {
	  /* Normal player */
	  sprintf(buf,
	    "%d|%s|%s|%s|%s%s%s|%s\n",
	    wch->level,
	    wch->race < MAX_PC_RACE ? pc_race_table[wch->race].who_name : "",
	    class,
	    clanname,
	    STR_IS_SET(wch->strbit_comm,COMM_AFK)  ? "[AFK] ":"",
	    STR_IS_SET(wch->strbit_act,PLR_KILLER) ? "(KILLER) ":"",
	    STR_IS_SET(wch->strbit_act,PLR_THIEF)  ? "(THIEF) ":"",
	    playername);
	}

	write_to_buffer(output,buf,0);
    }

    return;
}



void html_help( DESCRIPTOR_DATA *output,char *argument)
{
    char buf[4*MAX_STRING_LENGTH];
    char urlargs[MAX_STRING_LENGTH],str_letter[2];
    char title[MSL];
    char *s;
    HELP_DATA *pHelp;

    mud_data.http_help_connections++;
    if (argument[0]=='\0') argument="summary";

    for(s=argument;*s;s++) if(*s=='+') *s=' ';

    if(!str_prefix("help_",argument) && (strlen(argument)>5)) {
        char letter,*l;

        letter=toupper(argument[5]);

        /* Generate the help index */
	HTTPheader(output,"200 OK","text/html");

	sprintf(str_letter,"%c",letter);
	/* Generate the table */
        for (pHelp=help_first;pHelp!=NULL;pHelp=pHelp->next ) {
	    if (pHelp->keyword[0]=='*'?
		is_exact_name(str_letter,pHelp->keyword+1):
		is_name(str_letter, pHelp->keyword) ) {

		char helpstring[MSL],*phelpstring;

		s=pHelp->keyword;
		if (*s=='*') s++;
		l=s;if (*l=='\'') l++;
		strcpy(urlargs,s);
		strcpy(title,Capitalize(s));
		for (s=urlargs;*s;s++)
		    if (*s==' ')
			*s='+';

		phelpstring=pHelp->keyword;
		while (phelpstring[0]) {
		    phelpstring=one_argument(phelpstring,helpstring);
		    if (toupper(helpstring[0])==letter) {
			sprintf(buf,"%s %s\n",urlargs,helpstring);
			write_to_buffer(output,buf,0);
		    }
		}
	    }
        }

	return;
    }

    /* Find the help!!! */
    for ( pHelp = help_first; pHelp != NULL; pHelp = pHelp->next )
    {
        s=pHelp->keyword;
        if(*s=='*') s++;
        if(!str_cmp(s,argument)) break;
    }

    /* If no help is found, display the help index */
    if(!pHelp)
    {
        HTTPheader(output,"404 Not Found","text/html");
        write_to_buffer(output, not_found, 0);
        mud_data.http_errors++;
        return;
    }

    HTTPheader(output,"200 OK","text/html");
    s=Capitalize(argument);
    ascii_to_html(buf,pHelp->text,0);
    write_to_buffer(output,buf,0);

    return;
}

void html_pmemory(DESCRIPTOR_DATA *output,char *arg) {
    BUFFER *buf;

    buf=new_buf();
    get_memory_overview(buf);
    HTTPheader(output,"200 OK","text/plain");
    write_to_buffer(output,buf_string(buf),0);
    free_buf(buf);
}


void html_pics_m(DESCRIPTOR_DATA *output,char *arg) {
    int i;
    MOB_INDEX_DATA *mid;
    char URL[MSL];
    char line[MSL];
    AREA_DATA *area=NULL;

    if (arg[0]!=0)
	area=get_area_data(atoi(arg));

    HTTPheader(output,"200 OK","text/plain");

    for (i=0;i<MAX_VNUMS;i++) {
	if ((mid=get_mob_index(i))!=NULL) {
	    if (area==NULL || mid->area==area) {
		if (mid->pueblo_picture!=NULL
		&&  mid->pueblo_picture[0]!=0) {
		    URL[0]=0;
		    url(URL,mid->pueblo_picture,mid->area,FALSE);
		    sprintf(line,"%s|%s|%s\n",
			mid->area->name,
			mid->short_descr,
			URL);
		    write_to_buffer(output,line,0);
		}
	    }
	}
    }
}

void html_pics_o(DESCRIPTOR_DATA *output,char *arg) {
    int i;
    OBJ_INDEX_DATA *oid;
    char URL[MSL];
    char line[MSL];
    AREA_DATA *area=NULL;

    if (arg[0]!=0)
	area=get_area_data(atoi(arg));

    HTTPheader(output,"200 OK","text/plain");

    for (i=0;i<MAX_VNUMS;i++) {
	if ((oid=get_obj_index(i))!=NULL) {
	    if (area==NULL || oid->area==area) {
		if (oid->pueblo_picture!=NULL
		&&  oid->pueblo_picture[0]!=0) {
		    URL[0]=0;
		    url(URL,oid->pueblo_picture,oid->area,FALSE);
		    sprintf(line,"%s|%s|%s\n",
			oid->area->name,
			oid->short_descr,
			URL);
		    write_to_buffer(output,line,0);
		}
	    }
	}
    }
}

void html_pics_r(DESCRIPTOR_DATA *output,char *arg) {
    int i;
    ROOM_INDEX_DATA *rid;
    char URL[MSL];
    char line[MSL];
    AREA_DATA *area=NULL;

    if (arg[0]!=0)
	area=get_area_data(atoi(arg));

    HTTPheader(output,"200 OK","text/plain");

    for (i=0;i<MAX_VNUMS;i++) {
	if ((rid=get_room_index(i))!=NULL) {
	    if (area==NULL || rid->area==area) {
		if (rid->pueblo_picture!=NULL
		&&  rid->pueblo_picture[0]!=0) {
		    URL[0]=0;
		    url(URL,rid->pueblo_picture,rid->area,FALSE);
		    sprintf(line,"%s|%s|%s\n",
			rid->area->name,
			rid->name,
			URL);
		    write_to_buffer(output,line,0);
		}
	    }
	}
    }
}

void html_pclan(DESCRIPTOR_DATA *output,char *arg) {
    CLAN_INFO *		clan;
    CLANMEMBER_TYPE *	member;
    RANK_INFO *		rank;
    int			members,i;
    char		s[MSL];

    HTTPheader(output,"200 OK","text/plain");
    if (arg[0]==0) {
	clan=clan_list;
	while (clan!=NULL) {
	    if (!clan->deleted) {
		sprintf(s,"%s|%s\n",
		    clan->name,
		    clan->who_name);
		write_to_buffer(output,s,0);
	    }
	    clan=clan->next;
	}
	return;
    }

    if ((clan=get_clan_by_name(arg))==NULL)
	return;

    write_to_buffer(output,clan->who_name,0);	write_to_buffer(output,"\n",0);
    members=0;
    if (!clan->independent)
	for (i=0;i<MAX_CLAN_RANK;i++) {
	    rank=get_rank_by_name(rank_name[i]);
	    write_to_buffer(output,rank->rank_name,0);
	    write_to_buffer(output,": ",0);
	    for (member=clan->members;member!=NULL;member=member->next_member) {
		if (rank==member->rank_info) {
		    members++;
		    write_to_buffer(output,member->playername,0);
		    write_to_buffer(output," ",0);
		}
	    }
	    write_to_buffer(output,"\n",0);
	}

    write_to_buffer(output,clan->url,0);	write_to_buffer(output,"\n",0);
    write_to_buffer(output,clan->description,0);write_to_buffer(output,"\n",0);
}

#endif


//
// these functions are used in the notes-sytem also, so they are needed anyway.
// maybe could be nicer to move them out of here entirely.
//
static char *colorstart[]={
  "<FONT COLOR='#AA0000'>",        /* red */
  "<FONT COLOR='#0099FF'><B>",     /* cyan */
  "<FONT COLOR='#006600'>",        /* green */
  "<FONT COLOR='#990066'>",	   /* magenta */
  "<FONT COLOR='#000077'>",        /* blue */
  "<FONT COLOR='#777777'>",        /* white */
  "<FONT COLOR='#996633'>",        /* yellow */

  "<FONT COLOR='#FF0000'>",        /* red */
  "<FONT COLOR='#0099FF'><B>",     /* cyan */
  "<FONT COLOR='#008800'>",        /* green */
  "<FONT COLOR='#FF33CC'>",	   /* magenta */
  "<FONT COLOR='#3366FF'>",        /* blue */
  "<FONT COLOR='#FFFFFF'><B>",     /* white */
  "<FONT COLOR='#FFFF66'><B>"      /* yellow */
};

static char *colorstop[]={
  "</FONT>",
  "</B></FONT>",
  "</FONT>",
  "</FONT>",
  "</FONT>",
  "</FONT>",
  "</FONT>",

  "</FONT>",
  "</B></FONT>",
  "</FONT>",
  "</FONT>",
  "</FONT>",
  "</B></FONT>",
  "</B></FONT>",
};


/* If you update this function, please also update the
 * ascii_to_html_length function.
 */
char *ascii_to_html(char *html,char *ascii,int options) {
    char *c=html;
    bool incolor=FALSE;
    int curcol;

    while(*ascii)
    {
        switch(*ascii) {
          case '\r':break;
          case '\n': if(options&1) { strcpy(html,"<BR>\n");html+=5; }
                     else *(html++)=*ascii;
                     break;
          case '<': strcpy(html,"&lt;");html+=4;break;
          case '>': strcpy(html,"&gt;");html+=4;break;
          case '&': strcpy(html,"&amp;");html+=5;break;
          case '}': if(ascii[1]=='}' || ascii[1]=='\0') *(html++)='}';
                    // html doesn't have background colors
                    if(ascii[1]!='\0') ascii++;
                    break;
          case '{': if(ascii[1]=='{' || ascii[1]=='\0')
                    {
                      *(html++)='{';
                      if(ascii[1]!='\0') ascii++;
                      break;
                    }
                    if(ascii[1])
                    {
                       if(incolor)
                       {
                         strcpy(html,colorstop[curcol]);
                         html+=strlen(colorstop[curcol]);
                         incolor=FALSE;
                       }
                       curcol=-1;
                      /* Insert HTML colorcode here */
                      switch(ascii[1]) {
                        case 'x': break;
                        case 'b': curcol=4;break;
                        case 'c': curcol=1;break;
                        case 'g': curcol=2;break;
                        case 'r': curcol=0;break;
                        case 'w': curcol=5;break;
                        case 'y': curcol=6;break;
                        case 'B': curcol=11;break;
                        case 'C': curcol=8;break;
                        case 'G': curcol=9;break;
                        case 'M': curcol=10;break;
                        case 'R': curcol=7;break;
                        case 'W': curcol=12;break;
                        case 'Y': curcol=13;break;
                        case 'D': ;break;
                        case 'm': curcol=3;break;
			case '>': curcol=number_range(0,13);break;
                      }
                      if(curcol>=0)
                      {
                         strcpy(html,colorstart[curcol]);
                         html+=strlen(colorstart[curcol]);
                         incolor=TRUE;
                      }
                      ascii++;
                    }
                    break;
          default : *(html++)=*ascii;break;
        }
        ascii++;
    }

    if(incolor)
    {
      strcpy(html,colorstop[curcol]);
      html+=strlen(colorstop[curcol]);
    }
    *html='\0';

    return c;
}

int ascii_to_html_length(char *ascii,int options)
{
    bool incolor=FALSE;
    int curcol;
    int len=0;

    while(*ascii)
    {
        switch(*ascii) {
          case '\r':break;
          case '\n': if(options&1) len+=5;
                     else len++;
                     break;
          case '<': len+=4;break;
          case '>': len+=4;break;
          case '&': len+=5;break;
          case '}': if(ascii[1]=='}' || ascii[1]=='\0') len++;
                    if(ascii[1]!='\0') ascii++;
                    break;
          case '{': if(ascii[1]=='{' || ascii[1]=='\0')
                    {
                      len++;
                      if(ascii[1]!='\0') ascii++;
                      break;
                    }
                    if(ascii[1])
                    {
                       if(incolor)
                       {
                         len+=strlen(colorstop[curcol]);
                         incolor=FALSE;
                       }
                       curcol=-1;
                      /* Insert HTML colorcode here */
                      switch(ascii[1]) {
                        case 'x': break;
                        case 'b': curcol=4;break;
                        case 'c': curcol=1;break;
                        case 'g': curcol=2;break;
                        case 'r': curcol=0;break;
                        case 'w': curcol=5;break;
                        case 'y': curcol=6;break;
                        case 'B': curcol=11;break;
                        case 'C': curcol=8;break;
                        case 'G': curcol=9;break;
                        case 'M': curcol=10;break;
                        case 'R': curcol=7;break;
                        case 'W': curcol=12;break;
                        case 'Y': curcol=13;break;
                        case 'D': ;break;
                        case 'm': curcol=3;break;
			case '>': curcol=number_range(0,13);break;
                      }
                      if(curcol>=0)
                      {
                         len+=strlen(colorstart[curcol]);
                         incolor=TRUE;
                      }
                      ascii++;
                    }
                    break;
          default : len++;break;
        }
        ascii++;
    }

    if(incolor)
    {
        len+=strlen(colorstop[curcol]);
    }

    return len;
}
