//
// $Id: fd_tcl_mob.c,v 1.41 2008/04/15 19:01:37 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#define NEEDS_TCL
#include "merc.h"
#include "olc.h"
#include "fd_tcl_char.h"

TCL_MOB_CMD(mob_fullname);
TCL_MOB_CMD(mob_short);
TCL_MOB_CMD(mob_delay);
TCL_MOB_CMD(mob_hasdelay);
TCL_MOB_CMD(mob_timer);
TCL_MOB_CMD(mob_hastimer);
TCL_MOB_CMD(mob_remember);
TCL_MOB_CMD(mob_forget);
TCL_MOB_CMD(mob_hastarget);
TCL_MOB_CMD(mob_targethere);
TCL_MOB_CMD(mob_addtrigger);
TCL_MOB_CMD(mob_deltrigger);
TCL_MOB_CMD(mob_triggers);
TCL_MOB_CMD(mob_assist);
TCL_MOB_CMD(mob_kill);
TCL_MOB_CMD(mob_flee);
TCL_MOB_CMD(mob_cast);
TCL_MOB_CMD(mob_at);
TCL_MOB_CMD(mob_extract);
TCL_MOB_CMD(mob_impinvis);
TCL_MOB_CMD(mob_do);
TCL_MOB_CMD(mob_around);
TCL_MOB_CMD(mob_hunt);
TCL_MOB_CMD(mob_drunk);
TCL_MOB_CMD(mob_hasbeenkilledby);
TCL_MOB_CMD(mob_leave);
TCL_MOB_CMD(mob_restore);
TCL_MOB_CMD(mob_walk);
TCL_MOB_CMD(mob_give);
TCL_MOB_CMD(mob_longdescr);
TCL_MOB_CMD(mob_extradescr);
TCL_MOB_CMD(mob_hunt_id);
TCL_MOB_CMD(mob_hunt_type);
TCL_MOB_CMD(mob_descr);
TCL_MOB_CMD(mob_sex);
TCL_MOB_CMD(mob_level);
TCL_MOB_CMD(mob_namespace);
TCL_MOB_CMD(mob_pet);
TCL_MOB_CMD(mob_peace);
//TCL_MOB_CMD(mob_money);
TCL_MOB_CMD(MobCmdNotImpl);


struct tcl_Mob_Command_Struct {
    char *cmd;
    int  (*func)(Tcl_Interp *,CHAR_DATA *,int,Tcl_Obj *CONST []);
} tclMobCommands[] = {
	{ "name",		char_name		},
	{ "id",			char_id			},
	{ "fullname",		mob_fullname		},
	{ "short",		mob_short		},
	{ "delay",		mob_delay		},
	{ "hasdelay",		mob_hasdelay		},
	{ "timer",		mob_timer		},
	{ "hastimer",		mob_hastimer		},
	{ "isactive",		char_isactive		},
	{ "cansee",		char_cansee		},
	{ "remember",		mob_remember		},
	{ "forget",		mob_forget		},
	{ "hastarget",		mob_hastarget		},
	{ "targethere",		mob_targethere		},
	{ "addtrigger",		mob_addtrigger		},
	{ "deltrigger",		mob_deltrigger		},
	{ "triggers",		mob_triggers		},
	{ "he",			char_he			},
	{ "him",		char_him		},
	{ "his",		char_his		},
	{ "echo",		char_echo		},
	{ "oload",		char_oload		},
	{ "hpcnt",		char_hpcnt		},
	{ "goto",		char_goto		},
	{ "assist",		mob_assist		},
	{ "kill",		mob_kill		},
	{ "fighting",		char_fighting		},
	{ "flee",		mob_flee		},
	{ "act",		char_act		},
	{ "cast",		mob_cast		},
	{ "at",			mob_at			},
	{ "remove",		char_remove		},
	{ "extract",		mob_extract		},
	{ "impinvis",		mob_impinvis		},
	{ "do",			mob_do			},
	{ "room",		char_room		},
	{ "around",		mob_around		},
	{ "property",		char_property		},
	{ "position",		char_position		},
	{ "hunt",		mob_hunt		},
	{ "iscarrying",		char_iscarrying		},
	{ "iswearing",		char_iswearing		},
	{ "drunk",		mob_drunk		},
	{ "hasbeenkilledby",	mob_hasbeenkilledby	},
	{ "leave",		mob_leave		},
	{ "restore",		mob_restore		},
	{ "dex",		char_dex		},
	{ "int",		char_int		},
	{ "wis",		char_wis		},
	{ "str",		char_str		},
	{ "con",		char_str		},
	{ "walk",		mob_walk		},
	{ "unequip",		char_unequip		},
	{ "vnum",		char_vnum		},
	{ "give",		mob_give		},
	{ "longdescr",		mob_longdescr		},
	{ "extradescr",		mob_extradescr		},
	{ "hunt_id",		mob_hunt_id		},
	{ "hunt_type",		mob_hunt_type		},
	{ "descr",		mob_descr		},
	{ "sex",		mob_sex			},
	{ "delproperty",	char_delproperty	},
	{ "level",		mob_level		},
	{ "namespace",		mob_namespace		},
	{ "pet",		mob_pet			},
	{ "listeff",		char_listeff		},
	{ "listaff",		char_listeff		},
	{ "peace",		mob_peace		},
	{ "money",		char_money		},
	{ "exists",		MobCmdNotImpl		},
	{ NULL,			MobCmdNotImpl           }
};


int tclMobObjCmd (client, interp, objc, objv)
    ClientData client;			/* Not used. */
    Tcl_Interp *interp;			/* Current interpreter. */
    int objc;				/* Number of arguments. */
    Tcl_Obj *CONST objv[];		/* Argument values. */
{
    CHAR_DATA *ch;
    int index;

    // we know argv[0] is 'mob'          
    objc--; objv++;

    ch=tcl_parse_char_switches(progData->mob,&objc,&objv);

    if (objc==1 && !str_cmp(Tcl_GetString(objv[0]),"exists")) { 
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(ch!=NULL));
	return TCL_OK;
    }

    if (!ch) return TCL_ERROR;

    if (!IS_NPC(ch)) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj(
	    "mob command executed on a player character.",-1));
	return TCL_ERROR;
    }

    if (objc==0) {
	// No arguments given return name.
	return char_name(interp,ch,objc,objv);
    }

    if (Tcl_GetIndexFromObjStruct(interp, objv[0], 
				  (char **)tclMobCommands, sizeof(struct tcl_Mob_Command_Struct), 
				  "mob command", 0, &index) != TCL_OK) {
	return TCL_ERROR;
    }

    return (tclMobCommands[index].func)(interp,ch,objc,objv);
}

TCL_MOB_CMD(MobCmdNotImpl) {
    Tcl_Obj *result;

    result=Tcl_NewStringObj("Mob command not implemented yet: ",-1);
    Tcl_AppendObjToObj(result,objv[0]);
    Tcl_SetObjResult(interp,result);

    return TCL_ERROR;
}

/* fullname [new str]*/
TCL_MOB_CMD(mob_fullname) {
    if (objc==2) {
	char *newname;

	newname=Tcl_GetString(objv[1]);
	free_string(ch->name);
	ch->name=str_dup(newname);
    } else
	Tcl_SetObjResult(interp,Tcl_NewStringObj(ch->name,-1));
    return TCL_OK;
}

/* short [new str]*/
TCL_MOB_CMD(mob_short) {
    if (objc==2) {
	char *newname;

	newname=Tcl_GetString(objv[1]);
	free_string(ch->short_descr);
	ch->short_descr=str_dup(newname);
    } else
	Tcl_SetObjResult(interp,Tcl_NewStringObj(ch->short_descr,-1));
    return TCL_OK;
}

/* delay */
TCL_MOB_CMD(mob_delay) {
    long delay;

    if (objc==1) {
	Tcl_SetObjResult(interp,Tcl_NewLongObj(ch->mprog_delay));
	return TCL_OK;
    }
    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"mob delay number");
	return TCL_ERROR;
    }

    if (Tcl_GetLongFromObj(interp,objv[1],&delay)==TCL_ERROR) {
	if (!str_cmp(Tcl_GetString(objv[1]),"cancel")) delay=-1;
	else return TCL_ERROR;
    }
    ch->mprog_delay=delay;
    Tcl_SetObjResult(interp,Tcl_NewLongObj(delay));
    return TCL_OK;
}

/* hasdelay */
TCL_MOB_CMD(mob_hasdelay) {
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(ch->mprog_delay>0));
    return TCL_OK;
}

/* timer */
TCL_MOB_CMD(mob_timer) {
    long time;

    if (objc==1) { /* zero arguments */
	Tcl_SetObjResult(interp,Tcl_NewLongObj(ch->mprog_timer));
	return TCL_OK;
    }

    if (objc!=2) { /* 1 argument */
	Tcl_WrongNumArgs(interp,0,objv,"mob timer ?number?");
	return TCL_ERROR;
    }

    if (Tcl_GetLongFromObj(interp,objv[1],&time)==TCL_ERROR) {
	if (!str_cmp(Tcl_GetString(objv[1]),"cancel")) time=-1;
	else return TCL_ERROR;
    }
    ch->mprog_timer=time;
    Tcl_SetObjResult(interp,Tcl_NewLongObj(time));
    return TCL_OK;
}

/* hastimer */
TCL_MOB_CMD(mob_hastimer) {
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(ch->mprog_timer>=0));
    return TCL_OK;
}

/* remember */
TCL_MOB_CMD(mob_remember) {
    if (objc>2) {
	Tcl_WrongNumArgs(interp,0,objv,"mob remember ?charid?");
	return TCL_ERROR;
    }

    if (objc==1)
	Tcl_SetObjResult(interp,Tcl_NewLongObj(ch->mprog_target?ch->mprog_target->id:0));
    else {
	CHAR_DATA *target;

	if ((target=tcl_charid_char(ch,Tcl_GetString(objv[1]),NULL))==NULL) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("mob remember: target not found",-1));
	    return TCL_ERROR;
	}

	ch->mprog_target=target;
	Tcl_SetObjResult(interp,Tcl_NewLongObj(target->id));
    }
    return TCL_OK;
}

/* forget */
TCL_MOB_CMD(mob_forget) {
    Tcl_SetObjResult(interp,Tcl_NewLongObj(ch->mprog_target?ch->mprog_target->id:0));
    ch->mprog_target=NULL;
    return TCL_OK;
}

/* hastarget */
TCL_MOB_CMD(mob_hastarget) {
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(ch->mprog_target!=NULL));
    return TCL_OK;
}

/* targethere */
TCL_MOB_CMD(mob_targethere) {
    if (ch->mprog_target && ch->in_room==ch->mprog_target->in_room)
      Tcl_SetObjResult(interp,Tcl_NewBooleanObj(TRUE));
    else
      Tcl_SetObjResult(interp,Tcl_NewBooleanObj(FALSE));
    return TCL_OK;
}

/* addtrigger */
TCL_MOB_CMD(mob_addtrigger) {
    return mp_addtrigger(ch,objc,objv);
}

/* deltrigger */
TCL_MOB_CMD(mob_deltrigger) {
    static tclOptionList *deltriggerSwitches[]={"-all","-prefix", NULL};
    int sindex=-1;
    bool delAll,prefix;
    TCLPROG_LIST *trig,*trig_next;
    char *s,l;

    if (objc<=1) {
	Tcl_WrongNumArgs(interp,0,objv,"mob deltrigger ?-all? ?-prefix? ?name?");
	return TCL_ERROR;
    }

    s=Tcl_GetString(objv[1]);
    if (*s=='-' && Tcl_GetIndexFromObj(interp, objv[1], deltriggerSwitches, "switch", 0, &sindex) != TCL_OK)
	return TCL_ERROR;

    delAll=sindex==0;
    prefix=sindex==1;

    if ((prefix && objc!=3)
    || (!prefix && objc!=2)) {
	Tcl_WrongNumArgs(interp,0,objv,"mob deltrigger ?-all? ?-prefix? ?name?");
	return TCL_ERROR;
    }

    s=Tcl_GetString(objv[objc-1]);
    l=strlen(s);

    for (trig=ch->mprogs;trig;trig=trig_next) {
	trig_next=trig->next;
	if (delAll) mp_delonetrigger(ch,trig);
	else if (prefix && strncmp(trig->name,s,l)==0) mp_delonetrigger(ch,trig);
	else if (strcmp(trig->name,s)==0) mp_delonetrigger(ch,trig);
    }
    return TCL_OK;
}

/* triggers */
TCL_MOB_CMD(mob_triggers) {
    TCLPROG_LIST *trig;
    Tcl_Obj *result;

    result=Tcl_NewListObj(0,NULL);
    for (trig=ch->mprogs;trig;trig=trig->next) {
	Tcl_ListObjAppendElement(interp,result,Tcl_NewStringObj(trig->name,-1));
    }
    Tcl_SetObjResult(interp,result);
    return TCL_OK;
}

/* assist <char> */
TCL_MOB_CMD(mob_assist) {
    CHAR_DATA *target;

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"mob assist charid");
	return TCL_ERROR;
    }

    if ((target=tcl_charid_char(ch,Tcl_GetString(objv[1]),NULL))==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("mob assist: can't find target charid",-1));
	return TCL_ERROR;
    }

    if (ch->in_room!=target->in_room) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("mob assist: target must be in the same room",-1));
	return TCL_ERROR;
    }

    if (target==ch || ch->fighting!=NULL || target->fighting == NULL)
	return TCL_OK;

    multi_hit(ch,target->fighting, TYPE_UNDEFINED);
    return TCL_OK;
}

/* kill <char> */
TCL_MOB_CMD(mob_kill) {
    CHAR_DATA *target;

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"mob kill charid");
	return TCL_ERROR;
    }

    if ((target=tcl_charid_char(ch,Tcl_GetString(objv[1]),NULL))==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("mob kill: can't find target charid",-1));
	return TCL_ERROR;
    }

    if (ch->in_room!=target->in_room) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("mob kill: target must be in the same room",-1));
	return TCL_ERROR;
    }

    if (target==ch || IS_NPC(target) || ch->position==POS_FIGHTING)
	return TCL_OK;

    if (IS_AFFECTED(ch,EFF_CHARM ) && ch->master == target) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("mob kill: charmed mob attacking master",-1));
	return TCL_ERROR;
    }

    multi_hit(ch,target, TYPE_UNDEFINED);
    return TCL_OK;
}

/* flee */
TCL_MOB_CMD(mob_flee) {
    ROOM_INDEX_DATA *was_in;
    EXIT_DATA *pexit;
    int door, attempt;

    if ( ch->fighting != NULL )
	return TCL_OK;

    if ( (was_in = ch->in_room) == NULL )
	return TCL_OK;

    for ( attempt = 0; attempt < DIR_MAX; attempt++ ) {
	door = number_door( );
	if ( ( pexit = was_in->exit[door] ) == 0
	||   pexit->to_room == NULL
	||   IS_SET(pexit->exit_info, EX_CLOSED)
	|| ( IS_NPC(ch)
	&&   STR_IS_SET(pexit->to_room->strbit_room_flags, ROOM_NO_MOB) ) )
	    continue;

	move_char( ch, door, FALSE, TRUE );

	if ( ch->in_room != was_in )
	    break;
    }
    return TCL_OK;
}

/* cast */
TCL_MOB_CMD(mob_cast) {
    CHAR_DATA *vch;
    OBJ_DATA *obj;
    void *vo = NULL;
    char *spell,*s;
    int target;
    int sn;
    int i=1,index;
    bool noisy=FALSE;
    bool speechonly=FALSE;
    static tclOptionList *castSwitches[]={"-noisy","-speechonly",NULL};

    if (objc-i<1) {
	Tcl_WrongNumArgs(interp,0,objv,"mob cast ?-noisy? ?-speechonly? spell ?target?");
	return TCL_ERROR;
    }

    while (objc-i>0) {
	s=Tcl_GetString(objv[i]);
	if (*s!='-') break;
	if (Tcl_GetIndexFromObj(interp, objv[i], castSwitches, "switch", 0, &index) != TCL_OK)
	    return TCL_ERROR;
	if (index==0) noisy=TRUE;
	if (index==1) speechonly=TRUE;
	i++;
    }

    spell=Tcl_GetString(objv[i]);
    i++;

    if (*spell=='\0' || ((sn=skill_lookup(spell))<0)) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("mob spell: no such spell",-1));
	return TCL_ERROR;
    }

    if (speechonly) {
	say_spell(ch,sn);
	return TCL_OK; 
    }

    if (objc-i>1) {
	Tcl_WrongNumArgs(interp,0,objv,"mob cast ?-noisy? ?-speechonly? spell ?target?");
	return TCL_ERROR;
    }

    if (objc-i>0) {
	char *target;

	target=Tcl_GetString(objv[i]);
	i++;

	vch=tcl_charid_char(ch,target,NULL);
	obj=tcl_objid_here(ch,target);
    } else {
	vch=NULL;
	obj=NULL;
    }

    target=TARGET_NONE;
    vo=NULL;
    switch ( skill_target(sn,ch) ) {
	default: return TCL_OK;
	case TAR_IGNORE:
	    break;
	case TAR_CHAR_OFFENSIVE:
	    if ( vch == NULL || vch == ch ) return TCL_OK;
	    vo = ( void * ) vch;
	    target=TARGET_CHAR;
	    break;
	case TAR_CHAR_DEFENSIVE:
	    vo = vch == NULL ? ( void *) ch : (void *) vch;
	    target=TARGET_CHAR;
	    break;
	case TAR_CHAR_SELF:
	    vo = ( void *) ch;
	    target=TARGET_CHAR;
	    break;
	case TAR_OBJ_CHAR_DEF:
	case TAR_OBJ_CHAR_OFF:
	    if ( obj != NULL ) {
		vo = ( void * ) obj;
		target = TARGET_OBJ;
	    } else if ( vch != NULL ) {
		vo = ( void * ) vch;
		target = TARGET_CHAR;
	    } else {
		vo = ( void * ) ch;
		target = TARGET_CHAR;
	    }
	    break;
	case TAR_OBJ_INV:
	case TAR_OBJ_ROOM:
	    if ( obj == NULL ) return TCL_OK;
	    vo = ( void * ) obj;
	    target = TARGET_OBJ;
	    break;
    }
    if (noisy) say_spell(ch,sn);
    (*skill_spell_fun(sn,ch))( sn, ch->level, ch, vo, target );
    return TCL_OK;
}

/* at location program */
TCL_MOB_CMD(mob_at) {
    ROOM_INDEX_DATA *room,*oldroom;
    int error;

    if (objc!=3) {
	Tcl_WrongNumArgs(interp,0,objv,"mob at location tclprogram");
	return TCL_ERROR;
    }

    if ((room=tcl_location(Tcl_GetString(objv[1])))==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("mob at: location not found",-1));
	return TCL_ERROR;
    }

    if (ch->in_room==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("mob at: character isn't in a room",-1));
	return TCL_ERROR;
    }

    oldroom=ch->in_room;

    char_from_room(ch);
    char_to_room(ch,room);

    error=Tcl_EvalObjEx(interp,objv[2],0);

    char_from_room(ch);
    char_to_room(ch,oldroom);

    return error;
}

/* extract */
TCL_MOB_CMD(mob_extract) {
    extract_char(ch,TRUE);
    return TCL_OK;
}

/* impinvis ?boolean? */
TCL_MOB_CMD(mob_impinvis) {
    int i=1;

    if (objc-i>1) {
	Tcl_WrongNumArgs(interp,0,objv,"mob impinvis ?boolean?");
	return TCL_ERROR;
    }

    if (objc-i>0) {
	int onoff;

	if ((Tcl_GetBooleanFromObj(interp,objv[i],&onoff)!=TCL_OK))
	    return TCL_ERROR;
	if (onoff) STR_SET_BIT(ch->strbit_affected_by2,EFF_IMPINVISIBLE);
	else STR_REMOVE_BIT(ch->strbit_affected_by2,EFF_IMPINVISIBLE);
    }

    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(IS_AFFECTED2(ch,EFF_IMPINVISIBLE)));
    return TCL_OK;
}

/* do tcl-commands */
TCL_MOB_CMD(mob_do) {
    int i=1;
    int error;
    CHAR_DATA *old;

    if (objc-i!=1) {
	Tcl_WrongNumArgs(interp,0,objv,"mob do tclscript");
	return TCL_ERROR;
    }

    old=progData->mob;
    progData->mob=ch;

    error=Tcl_EvalObjEx(interp,objv[i],0);

    progData->mob=old;

    return error;
}

/* around */
TCL_MOB_CMD(mob_around) {
    int i=1;
    int error=0;
    ROOM_INDEX_DATA *was_in_room;
    int door;

    if (objc-i!=1) {
	Tcl_WrongNumArgs(interp,0,objv,"mob around tclscript");
	return TCL_ERROR;
    }

    was_in_room=ch->in_room;
    for (door=0;door<DIR_MAX;door++) {
	EXIT_DATA       *pexit;

	if ( ch
	&& ( pexit = was_in_room->exit[door] ) != NULL
	&&   pexit->to_room != NULL
	&&   pexit->to_room != was_in_room ) {
	    ch->in_room = pexit->to_room;
	    MOBtrigger  = FALSE;
	    error=Tcl_EvalObjEx(interp,objv[i],0);
	    ch=tcl_check_ch(ch);
	    MOBtrigger  = TRUE;
	}
    }
    if (ch) ch->in_room=was_in_room;

    return error;
}

// Hunt
TCL_MOB_CMD(mob_hunt) {
    CHAR_DATA *target;
    int direction;

    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"mob hunt charid");
	return TCL_ERROR;
    }

    if ((target=tcl_charid_char(ch,Tcl_GetString(objv[1]),NULL))==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewIntObj(-2));
	// no spamming if not needed, it has a return value
	// Tcl_SetStringObj(resultPtr,"mob hunt: target not found",-1);
	return TCL_ERROR;
    }

    // returns 0-5 for exits, -1 for not found or "here"
    direction=find_path(ch,ch->in_room->vnum,target->in_room->vnum,
	MAX_HUNT_DEPTH,TRUE,FALSE);
    Tcl_SetObjResult(interp,Tcl_NewIntObj(direction));
    return TCL_OK;
}

/* drunk */
TCL_MOB_CMD(mob_drunk) {
    Tcl_Obj *concatObj;
    char cmdbuf[MAX_INPUT_LENGTH],*str;

    if (objc<=1) {
	Tcl_WrongNumArgs(interp,0,objv,"mob drunk text ?text text ...?");
	return TCL_ERROR;
    }

    concatObj=Tcl_ConcatObj(objc-1,objv+1);
    Tcl_IncrRefCount(concatObj);

    strcpy(cmdbuf,Tcl_GetString(concatObj));
    str=makedrunk(ch,cmdbuf,TRUE);
    strcpy(cmdbuf,"say ");
    strcat(cmdbuf,str);
    interpret(ch,cmdbuf);

    Tcl_DecrRefCount(concatObj);
    return TCL_OK;
}

// hasbeenkilledby
TCL_MOB_CMD(mob_hasbeenkilledby) {
    if (progData->ch==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("mob hasbeenkilledby: no character.",-1));
	return TCL_ERROR;
    }
    if (IS_NPC(progData->ch)) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("mob hasbeenkilledby: character is mob.",-1));
	return TCL_ERROR;
    }
    if (IS_PC(ch))
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(FALSE));
    else
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(
	    STR_IS_SET(progData->ch->pcdata->killedthatmob,ch->pIndexData->vnum)));
    return TCL_OK;
}

// leave
TCL_MOB_CMD(mob_leave) {
    ROOM_INDEX_DATA *room;
    EXIT_DATA *exit;
    int doors[MAX_DIR];
    int i,attempt,dir;

    room=ch->in_room;
    for (i=0;i<MAX_DIR;i++) {
	doors[i]=0;

	if ((exit=room->exit[i])==0) continue;
	if (exit->to_room==NULL) continue;
	if (IS_SET(exit->exit_info,EX_CLOSED)) continue;
	if (STR_IS_SET(exit->to_room->strbit_room_flags,ROOM_NO_MOB)) continue;

	doors[i]=1;
    }

    dir=number_range(0,MAX_DIR);
    for (attempt=0;attempt<6;attempt++) {
	if (doors[dir]==1) break;
	dir=(dir+1)%MAX_DIR;
    }

    // mob goes into direction of 'dir' now.

    interpret(ch,dir_name[dir]);
    return TCL_OK;
}


// restore
TCL_MOB_CMD(mob_restore) {
    effect_strip(ch,gsn_plague);
    effect_strip(ch,gsn_poison);
    effect_strip(ch,gsn_blindness);
    effect_strip(ch,gsn_sleep);
    effect_strip(ch,gsn_curse);
    ch->hit =ch->max_hit;
    ch->mana=ch->max_mana;
    ch->move=ch->max_move;
    update_pos(ch);
    return TCL_OK;
}

// walk ?-room? ?-char? ?-mob? ?-global? id
TCL_MOB_CMD(mob_walk) {
    int i=1,index;
    bool to_room=FALSE;
    bool to_char=FALSE;
    bool to_mob=FALSE;
    bool global_find=FALSE;
    char *s;
    static tclOptionList *walkSwitches[]={"-room","-char","-mob","-global",NULL};

    if (objc-i<1) {
	Tcl_WrongNumArgs(interp,0,objv,"mob walk ?-room? ?-char? ?-mob? id");
	return TCL_ERROR;
    }

    while (objc-i>0) {
	s=Tcl_GetString(objv[i]);
	if (*s!='-') break;
	if (Tcl_GetIndexFromObj(interp, objv[i], walkSwitches, "switch", 0, &index) != TCL_OK)
	    return TCL_ERROR;
	if (index==0) to_room=TRUE;
	if (index==1) to_char=TRUE;
	if (index==2) to_mob=TRUE;
	if (index==3) global_find=TRUE;
	i++;
    }

    if ((to_room && ( to_char || to_mob)) ||
	(to_char && ( to_room || to_mob)) ||
	(to_mob && ( to_room || to_char))) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("mob walk: -room, -char and -mob are mutally exclusive.",-1));
	return TCL_ERROR;
    }

    if (!to_room && !to_char && !to_mob)
	to_mob=TRUE;

    if (to_char || to_mob ) {
	CHAR_DATA *target;

	if ((target=tcl_charid_char(ch,Tcl_GetString(objv[i]),NULL))==NULL) {
	    sprintf_to_char(ch,"mob walk: Cannot find mob '%s'",Tcl_GetString(objv[i]));
	    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(FALSE));
	    return TCL_OK;
	}

	if (global_find)
	    ch->hunt_type=HUNT_GFIND;
	else
	    ch->hunt_type=HUNT_FIND;
	ch->hunt_id=target->id;
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(TRUE));
	return TCL_OK;
    }

    if (to_room) {
	ROOM_DATA *target;

	if ((target=tcl_location(Tcl_GetString(objv[i])))==NULL) {
	    sprintf_to_char(ch,"mob walk: Cannot find room '%s'",Tcl_GetString(objv[i]));
	    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(FALSE));
	    return TCL_OK;
	}

	ch->hunt_type=HUNT_ROOM;
	ch->hunt_id=target->vnum;
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(TRUE));
	return TCL_OK;
    }

    return TCL_OK;
}

// give <obj id> <char id>
TCL_MOB_CMD(mob_give) {
    int i=1;
    OBJ_DATA *	obj;
    CHAR_DATA *	target;
    bool loud=FALSE;
    static tclOptionList *giveSwitches[]={"-loud",NULL};

    while (objc-i>0) {
	char *s;
	int index;
	s=Tcl_GetString(objv[i]);
	if (*s!='-') break;
	if (Tcl_GetIndexFromObj(interp, objv[i], giveSwitches, "switch", 0, &index) != TCL_OK)
	    return TCL_ERROR;
	if (index==0) loud=TRUE;
	i++;
    }

    if (objc-i!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"mob give ?-loud? <objid> <charid>");
	return TCL_ERROR;
    }

    if ((obj=tcl_objid(ch->carrying,Tcl_GetString(objv[i]),FALSE))==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("mob give: objid not found",-1));
	return TCL_ERROR;
    }
    if ((target=tcl_charid(ch->in_room,Tcl_GetString(objv[i+1]),NULL))==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("mob give: charid not found",-1));
	return TCL_ERROR;
    }

    obj_from_char(obj);
    if (loud) {
	act( "$n gives $p to $N.", ch, obj, target, TO_NOTVICT );
	act( "$n gives you $p.",   ch, obj, target, TO_VICT    );
	act( "You give $p to $N.", ch, obj, target, TO_CHAR    );
    }
    obj_to_char(obj,target);

    return TCL_OK;
}


// longdescr [new long description]
TCL_MOB_CMD(mob_longdescr) {
    if (objc==2) {
	char *newname;
	char snewname[MSL];

	newname=Tcl_GetString(objv[1]);
	free_string(ch->long_descr);
	strncpy(snewname,newname,MSL);
	snewname[MSL-3]=0;
	strcat(snewname,"\n\r");
	ch->long_descr=str_dup(snewname);
    } else
	Tcl_SetObjResult(interp,Tcl_NewStringObj(ch->long_descr,-1));
    return TCL_OK;
}

// extradescr add|del|mod|list [key [value]] 
TCL_MOB_CMD(mob_extradescr) {
    char *subcmd;
    char *key;
    char *value;  

    if (objc<2) {
        Tcl_WrongNumArgs(interp,0,objv,"mob extradescr add|del|mod|list ?key ?value?? ");      
        return TCL_ERROR;
    }
    subcmd=Tcl_GetString(objv[1]);
    if (objc>2)
        key=Tcl_GetString(objv[2]);  
    if (objc>3)
        value=Tcl_GetString(objv[3]);
    if (!strcasecmp(subcmd,"list")) {
        EXTRA_DESCR_DATA *edd;
        Tcl_Obj *list;

        list=Tcl_NewListObj(0,NULL);
        for (edd=ch->extra_descr;edd;edd=edd->next) {
            Tcl_ListObjAppendElement(interp,list,Tcl_NewStringObj(edd->keyword,-1));
        }
        Tcl_SetObjResult(interp,list);
        return TCL_OK;
    } else if (!strcasecmp(subcmd,"mod") || !strcasecmp(subcmd,"add")) {
        EXTRA_DESCR_DATA *edd;

        if (objc<4) {
            Tcl_WrongNumArgs(interp,0,objv,"mob extradescr add|mod key value ");
            return TCL_ERROR;
        }
        edd=get_extra_descr(key,ch->extra_descr);
        if (edd==NULL) {
            edd=new_extra_descr();
            edd->keyword=str_dup(key);
            edd->description=str_dup(value);
            edd->next=ch->extra_descr;
            ch->extra_descr=edd;
            return TCL_OK;
        }
        free_string(edd->description);
        edd->description=str_dup(value);
        return TCL_OK;
    } else if (!strcasecmp(subcmd,"del")) {
        EXTRA_DESCR_DATA *edd,*pedd;
        if (objc<3) {
            Tcl_WrongNumArgs(interp,0,objv,"mob extradescr del key ");
            return TCL_ERROR;
        }
        edd=get_extra_descr(key,ch->extra_descr);
        if (edd==NULL) {
            Tcl_SetObjResult(interp,Tcl_NewStringObj("mob extradescr del: Extra description doesn't exist ",-1));
            return TCL_ERROR;
        }
        if (ch->extra_descr==edd) {
            ch->extra_descr=edd->next;
        } else {
            for (pedd=ch->extra_descr;pedd->next!=edd;pedd=pedd->next)
                if (pedd==NULL) {
                    Tcl_SetObjResult(interp,Tcl_NewStringObj("mob extradescr del: Extra description exists but not found. Contact coder!",-1));
                    return TCL_ERROR;
                }

            /* pedd->next == edd */
            pedd->next=edd->next;
        }
        free_extra_descr(edd);
        return TCL_OK;
    } else {
        Tcl_SetObjResult(interp,Tcl_NewStringObj("mob extradescr: unknown subcommand (add/del/mod/list) ",-1));
        return TCL_ERROR;
    }

    return TCL_OK;
}

// hunt_id
TCL_MOB_CMD(mob_hunt_id) {
    if (ch->hunt_type==HUNT_NONE)
	Tcl_SetObjResult(interp,Tcl_NewIntObj(0));
    else
	Tcl_SetObjResult(interp,Tcl_NewIntObj(ch->hunt_id));
    return TCL_OK;
}

// hunt_type
TCL_MOB_CMD(mob_hunt_type) {
    Tcl_SetObjResult(interp,Tcl_NewStringObj(hunt_table[ch->hunt_type].name,-1));
    return TCL_OK;
}

// descr [new description]
TCL_MOB_CMD(mob_descr) {
    if (objc==2) {
	char *newname;
	char snewname[MSL];

	newname=Tcl_GetString(objv[1]);
	free_string(ch->description);
	strncpy(snewname,newname,MSL);
	snewname[MSL-3]=0;
	strcat(snewname,"\n\r");
	ch->description=str_dup(snewname);
    } else
	Tcl_SetObjResult(interp,Tcl_NewStringObj(ch->description,-1));
    return TCL_OK;
}

// sex [new sex]
TCL_MOB_CMD(mob_sex) {
    if (objc==2) {
	int gender;
	static tclOptionList *genders[]={ "neutral", "male", "female", "either", (char *)NULL };
	if (Tcl_GetIndexFromObj(interp,objv[1],genders,"option",0,&gender)!=TCL_OK)
	    if (Tcl_GetIntFromObj(interp,objv[1],&gender)!=TCL_OK)
		return TCL_ERROR;
	if ((gender<0) || (gender>3)) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("Gender out of range",-1));
	    return TCL_ERROR;
	}
	ch->Sex=gender;
    } else {
	switch(show_sex(ch)) {
	    case SEX_MALE: Tcl_SetObjResult(interp,Tcl_NewStringObj("male",-1));break;
	    case SEX_FEMALE: Tcl_SetObjResult(interp,Tcl_NewStringObj("female",-1));break;
	    default: Tcl_SetObjResult(interp,Tcl_NewStringObj("it",-1));break;
	}
    }
    return TCL_OK;
}

// level [new level]
TCL_MOB_CMD(mob_level) {
    if (objc==2) {
	int newlevel;
	// get integer arg:
	if (Tcl_GetIntFromObj(interp,objv[1],&newlevel)!=TCL_OK)
	    return TCL_ERROR;
	// check level range:
	if ((newlevel<0) || (newlevel>90)) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("New level must be >=0 and <=90",-1));
	    return TCL_ERROR;
	}
	if (ch->level!=newlevel) {
	    int v, delta, deltalevel;

	    // set level
	    deltalevel = newlevel - ch->level;
	    ch->level = newlevel;

	    // set armor
	    ch->armor[AC_PIERCE] = olc_mob_recommended[newlevel].ac*10;
	    ch->armor[AC_BASH] = olc_mob_recommended[newlevel].ac*10;
	    ch->armor[AC_SLASH] = olc_mob_recommended[newlevel].ac*10;
	    ch->armor[AC_EXOTIC] = (((olc_mob_recommended[newlevel].ac-10)/3)+10)*10;

	    // set hitpoints
	    v = dice(olc_mob_recommended[newlevel].hit[DICE_NUMBER],
	       olc_mob_recommended[newlevel].hit[DICE_TYPE]) + olc_mob_recommended[newlevel].hit[DICE_BONUS];
	    if (v<ch->max_hit && deltalevel>0) v = ch->max_hit+1;
	    if (v>ch->max_hit && deltalevel<0) v = ch->max_hit-1;
	    delta = v - ch->max_hit;
	    ch->max_hit = v;
	    ch->hit += delta;
	    if (ch->hit<1) ch->hit=1; // don't die...

	    // set damage
	    ch->damage[DICE_NUMBER] = olc_mob_recommended[newlevel].damage[DICE_NUMBER];
	    ch->damage[DICE_TYPE] = olc_mob_recommended[newlevel].damage[DICE_TYPE];
	    ch->damage[DICE_BONUS] = olc_mob_recommended[newlevel].damage[DICE_BONUS];
	    ch->damroll = olc_mob_recommended[newlevel].damage[DICE_BONUS];

	    // set mana
	    v = dice(olc_mob_recommended[newlevel].mana[DICE_NUMBER],
	       olc_mob_recommended[newlevel].mana[DICE_TYPE]) + olc_mob_recommended[newlevel].mana[DICE_BONUS];
	    if (v<ch->max_mana && deltalevel>0) v = ch->max_mana+1;
	    if (v>ch->max_mana && deltalevel<0) v = ch->max_mana-1;
	    delta = v - ch->max_mana;
	    ch->max_mana = v;
	    ch->mana += delta;
	    if (ch->mana<0) ch->mana = 0;

	    // set move
	    v = 100+newlevel*10;
	    if (v<ch->max_move && deltalevel>0) v = ch->max_move+1;
	    if (v>ch->max_move && deltalevel<0) v = ch->max_move-1;
	    delta = v - ch->max_move;
	    ch->max_move = v;
	    ch->move += delta;
	    if (ch->move<0) ch->move = 0;
	}
    }
    Tcl_SetObjResult(interp,Tcl_NewIntObj(ch->level));
    return TCL_OK;
}

TCL_MOB_CMD(mob_namespace) {
    if (objc>1 && !str_cmp("-db",Tcl_GetString(objv[1])))
	Tcl_SetObjResult(interp,Tcl_NewStringObj(namespace_dmob(ch->pIndexData),-1));
    else
	Tcl_SetObjResult(interp,Tcl_NewStringObj(namespace_char(ch),-1));             
    return TCL_OK;
}

TCL_MOB_CMD(mob_pet) {
    CHAR_DATA *owner;
    if (objc==1) {
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(STR_IS_SET(ch->strbit_act,ACT_PET)));
	return TCL_OK;
    }

    if ((owner=tcl_charid_char(ch,Tcl_GetString(objv[1]),NULL))==NULL) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("mob pet: can't find would-be owner charid",-1));
	return TCL_ERROR;
    }
    if (owner->pet) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("mob pet: owner already has a pet",-1));
	return TCL_ERROR;
    }
    if (STR_IS_SET(ch->strbit_act,ACT_PET)) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("mob pet: mob already is a pet",-1));
	return TCL_ERROR;
    }
    STR_SET_BIT(ch->strbit_act, ACT_PET);
    STR_SET_BIT(ch->strbit_affected_by, EFF_CHARM);

    add_follower( ch, owner );
    ch->leader = owner;
    owner->pet = ch;
    ch->clan=owner->clan;
    return TCL_OK;
}

TCL_MOB_CMD(mob_peace) {
    int all=TRUE;

    if (objc>1 && !str_cmp("-self",Tcl_GetString(objv[1]))) 
	all=FALSE;

    stop_fighting(ch,all);
    return TCL_OK;
}

/*
TCL_MOB_CMD(mob_money) {
    static tclOptionList *options[]={ "-gold", "-silver", "-total", "-add", "-remove", (char *)NULL };
    int unit=-1;
    int rel=0; // -1 == remove ; +1 == add
    int i=1;
    int amount=0;
    int newamount;

    while (objc-i>0) {
	char *s;
	int index;

	s=Tcl_GetString(objv[i]);
	if (*s!='-') break;
	if (Tcl_GetIndexFromObj(interp, objv[i], options, "option", 0, &index) != TCL_OK)
	    return TCL_ERROR;
	if (index>=0 && index<=2 && unit!=-1) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("mob money: gold, silver, and total are mutualy exclusive",-1));
	    return TCL_ERROR;
	}
	if (index>=3 && index<=4 && rel!=0) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("mob money: you can't add and remove at the same time.",-1));
	    return TCL_ERROR;
	}
	switch (index) {
	    case 0:
	    case 1:
	    case 2: unit=index; break;
	    case 3: rel=1; break;
	    case 4: rel=-1; break;
	}
	i++;
    }

    if (unit==-1) unit=2;

    switch (unit) {
	case 0: amount=ch->gold; break;
	case 1: amount=ch->silver; break;
	case 2: amount=ch->gold*100+ch->silver; break;
    }
    if (objc<=i) { 
	if (rel!=0) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("mob money: you can't add or remove without a value.",-1));
	    return TCL_ERROR;
	}
    } else {
	if (Tcl_GetIntFromObj(interp,objv[i],&newamount)!=TCL_OK)
	    return TCL_ERROR;

	if (amount+rel*newamount<0) {
	    Tcl_SetObjResult(interp,Tcl_NewStringObj("mob money: mob doesn't have enough money. Please catch this",-1));
	    return TCL_ERROR;
	}
	if (rel==0)
	    amount=newamount;
	else
	    amount=amount+rel*newamount;

	switch (unit) {
	    case 0: ch->gold=amount; break;
	    case 1: ch->silver=amount; break;
	    case 2: ch->gold=amount/100;ch->silver=amount%100; break;
	}
    }
    Tcl_SetObjResult(interp,Tcl_NewIntObj(amount));
    return TCL_OK;
}
*/

    

