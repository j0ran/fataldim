//
// $Id: fd_tcl_mud.c,v 1.20 2008/03/06 22:06:13 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#define NEEDS_TCL
#include "merc.h"

#define TCL_MUD_CMD(name) static int name(Tcl_Interp *interp,int objc,Tcl_Obj *CONST objv[])

TCL_MUD_CMD(mud_name);
TCL_MUD_CMD(mud_hour);
TCL_MUD_CMD(mud_charexists);
TCL_MUD_CMD(mud_objexists);
TCL_MUD_CMD(mud_echo);
TCL_MUD_CMD(mud_allmob);
TCL_MUD_CMD(mud_allobj);
TCL_MUD_CMD(mud_sun);
TCL_MUD_CMD(mud_day);
TCL_MUD_CMD(mud_month);
TCL_MUD_CMD(mud_season);
TCL_MUD_CMD(mud_randomchar);
TCL_MUD_CMD(mud_randomroom);
TCL_MUD_CMD(mud_log);
TCL_MUD_CMD(mud_allchar);
TCL_MUD_CMD(mud_weather);
TCL_MUD_CMD(MudCmdNotImpl);

struct tcl_Mud_Command_Struct {
    char *cmd;
    int  (*func)(Tcl_Interp *,int,Tcl_Obj *CONST []);
};

static struct tcl_Mud_Command_Struct tclMudCommands[] = {
	{ "name", 		mud_name },
	{ "hour", 		mud_hour },
	{ "charexists", 	mud_charexists },
	{ "objexists", 		mud_objexists },
	{ "echo", 		mud_echo },
	{ "allmob", 		mud_allmob },
	{ "sun",		mud_sun },
	{ "day", 		mud_day },
	{ "month", 		mud_month },
	{ "season", 		mud_season },
	{ "randomchar", 	mud_randomchar },
	{ "randomroom", 	mud_randomroom },
	{ "log",		mud_log },
	{ "allchar", 		mud_allchar },
	{ "allobj", 		mud_allobj },
	{ "weather", 		mud_weather },
	{ NULL,			MudCmdNotImpl }
};

int tclMudObjCmd (client, interp, objc, objv)
    ClientData client;			/* Not used. */
    Tcl_Interp *interp;			/* Current interpreter. */
    int objc;				/* Number of arguments. */
    Tcl_Obj *CONST objv[];		/* Argument values. */
{
    int index;

    objv++;objc--;

    if (objc==0) {
	return mud_name(interp,objc,objv);
    }

    if (Tcl_GetIndexFromObjStruct(interp, objv[0], 
				  (char **)tclMudCommands, sizeof(struct tcl_Mud_Command_Struct), 
				  "mud command", 0, &index) != TCL_OK) {
	return TCL_ERROR;
    }

    return (tclMudCommands[index].func)(interp,objc,objv);
}

TCL_MUD_CMD(MudCmdNotImpl) {
    Tcl_Obj *result;

    result=Tcl_NewStringObj("Mud command not implemented (yet): ",-1);
    Tcl_AppendObjToObj(result,objv[0]);
    Tcl_SetObjResult(interp,result);

    return TCL_ERROR;
}

/* name */
TCL_MUD_CMD(mud_name) {
    Tcl_SetObjResult(interp,Tcl_NewStringObj("Fatal Dimensions",-1));
    return TCL_OK;
}

/* hour */
TCL_MUD_CMD(mud_hour) {
    Tcl_SetObjResult(interp,Tcl_NewLongObj(time_info.hour));
    return TCL_OK;
}

/* charexists */
TCL_MUD_CMD(mud_charexists) {
    CHAR_DATA *ch;

    bugf("TCL: %s:mud charexists is depricated. Please use char exists.",current_namespace);
    if (objc!=2) {
	Tcl_WrongNumArgs(interp,0,objv,"mud charexists charid");
	return TCL_ERROR;
    }

    ch=tcl_charid(NULL,Tcl_GetString(objv[1]),NULL);
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(ch!=NULL));
    return TCL_OK;
}

/* objexists */
TCL_MUD_CMD(mud_objexists) {
    OBJ_DATA *obj;

    bugf("TCL: %s:mud objexists is depricated. Please use obj exists.",current_namespace);
    if (objc!=2) {
        Tcl_WrongNumArgs(interp,0,objv,"mud objexists charid");
        return TCL_ERROR;
    }

    obj=tcl_objid(NULL,Tcl_GetString(objv[1]),TRUE);
    Tcl_SetObjResult(interp,Tcl_NewBooleanObj(obj!=NULL));
    return TCL_OK;
}

/* echo */
TCL_MUD_CMD(mud_echo) {
    Tcl_Obj *concatObj;
    CHAR_DATA *player;

    if (objc<=1) {
        Tcl_WrongNumArgs(interp,0,objv,"mud echo text ?text text ...?");
        return TCL_ERROR;
    }

    concatObj=Tcl_ConcatObj(objc-1,objv+1);
    Tcl_IncrRefCount(concatObj);
    Tcl_AppendToObj(concatObj,"\n\r",-1);


    for ( player=player_list;player!=NULL;player=player->next_player)
    	if ( player->in_room!=NULL) {
    	    if ( IS_IMMORTAL(player) )
    		send_to_char( "gecho> ", player );
    	    send_to_char( Tcl_GetString(concatObj) , player );
    	}

    Tcl_DecrRefCount(concatObj);
    return TCL_OK;
}

/* allmob <charid> <tclscript> */
TCL_MUD_CMD(mud_allmob) {
    CHAR_DATA *ch;
    CHAR_DATA *old_ch,*next_ch;
    char *charid;
    int error;

    if (objc!=3) {
	Tcl_WrongNumArgs(interp,0,objv,"mud allmob charid tcl-script");
	return TCL_ERROR;
    }

    charid=Tcl_GetString(objv[1]);

    ch=tcl_charid(NULL,charid,NULL);
    old_ch=progData->mob;
    error=TCL_OK;

    while(ch!=NULL) {
	progData->mob=ch;
	next_ch=ch->next;
	error=Tcl_EvalObjEx(interp,objv[2],0);

	if (error!=TCL_OK) break;
	if (!next_ch) break;
	ch=tcl_charid(NULL,charid,next_ch);
    }
    progData->mob=tcl_check_ch(old_ch);

    if (error!=TCL_OK) return TCL_ERROR;
    return TCL_OK;
}

/* allobj <objid> <tclscript> */
TCL_MUD_CMD(mud_allobj) {
    OBJ_DATA *obj;
    OBJ_DATA *old_obj,*next_obj;
    char *objid;
    int error;

    if (objc!=3) {
	Tcl_WrongNumArgs(interp,0,objv,"mud allobj objid tcl-script");
	return TCL_ERROR;
    }

    objid=Tcl_GetString(objv[1]);

    obj=tcl_objid(NULL,objid,TRUE);
    old_obj=progData->obj;
    error=TCL_OK;

    while(obj!=NULL) {
	progData->obj=obj;
	next_obj=obj->next;
	error=Tcl_EvalObjEx(interp,objv[2],0);

	if (error!=TCL_OK) break;
	if (!next_obj) break;
	obj=tcl_objid(next_obj,objid,TRUE);
    }
    progData->obj=tcl_check_obj(old_obj);

    if (error!=TCL_OK) return TCL_ERROR;
    return TCL_OK;
}

/* sun */
TCL_MUD_CMD(mud_sun) {
    if (weather_info.sunlight==SUN_DARK) {
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(FALSE));
    } else
    if (weather_info.sunlight==SUN_LIGHT) {
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(TRUE));
    } else
	// sun rise, sun set, your glass may be half full.
	Tcl_SetObjResult(interp,Tcl_NewBooleanObj(number_percent()%2==0));
    return TCL_OK;
}

// day
TCL_MUD_CMD(mud_day) {
    static tclOptionList *daySwitches[]={"-name",NULL};
    bool name=FALSE;
    char *s;
    int index,i=1;

    while (objc-i>0) {
	s=Tcl_GetString(objv[i]);
	if (*s!='-') break;

	if (Tcl_GetIndexFromObj(interp, objv[i], daySwitches, "switch", 0, &index) != TCL_OK)
	    return TCL_ERROR;

	if (index==0) name=TRUE;
	i++;
    }

    if (name)
	Tcl_SetObjResult(interp,Tcl_NewStringObj((char *)day_name[(time_info.day+1)%7],-1));
    else
	Tcl_SetObjResult(interp,Tcl_NewLongObj(time_info.day+1));
    return TCL_OK;
}

// month
TCL_MUD_CMD(mud_month) {
    static tclOptionList *monthSwitches[]={"-name",NULL};
    bool name=FALSE;
    char *s;
    int index,i=1;

    while (objc-i>0) {
	s=Tcl_GetString(objv[i]);
	if (*s!='-') break;

	if (Tcl_GetIndexFromObj(interp, objv[i], monthSwitches, "switch", 0, &index) != TCL_OK)
	    return TCL_ERROR;

	if (index==0) name=TRUE;
	i++;
    }

    if (name)
	Tcl_SetObjResult(interp,Tcl_NewStringObj((char *)month_name[time_info.month],-1));
    else
	Tcl_SetObjResult(interp,Tcl_NewLongObj(time_info.month));
    return TCL_OK;
}

// season
TCL_MUD_CMD(mud_season) {
    static tclOptionList *seasonSwitches[]={"-name",NULL};
    bool name=FALSE;
    char *s;
    int index,i=1;
    Tcl_Obj *result;

    while (objc-i>0) {
	s=Tcl_GetString(objv[i]);
	if (*s!='-') break;

	if (Tcl_GetIndexFromObj(interp, objv[i], seasonSwitches, "switch", 0, &index) != TCL_OK)
	    return TCL_ERROR;

	if (index==0) name=TRUE;
	i++;
    }

    if (name) {
	if (time_info.month< 5) { result=Tcl_NewStringObj("winter",-1); } else
	if (time_info.month< 9) { result=Tcl_NewStringObj("spring",-1); } else
	if (time_info.month<13) { result=Tcl_NewStringObj("summer",-1); } else
				  result=Tcl_NewStringObj("autumn",-1);
    } else {
	if (time_info.month< 5) { result=Tcl_NewLongObj(0); } else
	if (time_info.month< 9) { result=Tcl_NewLongObj(1); } else
	if (time_info.month<13) { result=Tcl_NewLongObj(2); } else
				{ result=Tcl_NewLongObj(3); }
    }
    Tcl_SetObjResult(interp,result);
    return TCL_OK;
}

// randomchar
TCL_MUD_CMD(mud_randomchar) {
    static tclOptionList *randomcharSwitches[]={"-mob","-char",NULL};
    int index,i,j;
    bool findnpc=FALSE;	// you should specify something
    bool findpc=FALSE;
    CHAR_DATA *target;

    i=1;

    while (objc-i>0) {
	char *s;

	s=Tcl_GetString(objv[i]);
	if (*s!='-') break;

	if (Tcl_GetIndexFromObj(interp, objv[i], randomcharSwitches, "switch", 0, &index) != TCL_OK)
	    return TCL_ERROR;

	if (index==0) { findnpc=TRUE; }
	if (index==1) { findpc=TRUE; }
	i++;
    }

    if (objc-i>1) {
	Tcl_WrongNumArgs(interp,0,objv,"mud randomchar ?-mob? ?-char?");
	return TCL_ERROR;
    }

    if (!findnpc && !findpc) {
	Tcl_SetObjResult(interp,Tcl_NewStringObj("mud randomchar: not searcing for anything",-1));
	return TCL_ERROR;
    }

    // if there are no players and there is not looking for NPC's, return 0
    if (pcdata_inuse==0 && !findnpc) {
	Tcl_SetObjResult(interp,Tcl_NewLongObj(0));
	return TCL_OK;
    }

    if (findnpc)
	while (TRUE) {
	    j=number_range(0,char_inuse-1);
	    target=char_list;
	    while (--j>0) {
		target=target->next;
		if (target==NULL)
		    break;
	    }
	    if (target==NULL)	// woops, we ran out of the list
		continue;
	    if (IS_NPC(target) && findnpc)
		break;
	    if (IS_PC(target) && findpc)
		break;
	}
    else
	while (TRUE) {
	    j=number_range(0,pcdata_inuse-1);
	    target=char_list;
	    while (--j>0) {
		while (target && !IS_PC(target)) target=target->next;
		if (target==NULL) break;
		else target=target->next;
	    }
	    while (target && !IS_PC(target)) target=target->next;
	    if (target==NULL)	// woops, we ran out of the list
		continue;
	    if (IS_NPC(target) && findnpc)
		break;
	    if (IS_PC(target) && findpc)
		break;
	}
    Tcl_SetObjResult(interp,Tcl_NewLongObj(target->id));
    return TCL_OK;
}

// randomroom
TCL_MUD_CMD(mud_randomroom) {
    ROOM_DATA *room=get_random_room(NULL);

    if (objc>1) {
	Tcl_WrongNumArgs(interp,0,objv,"mud randomroom");
	return TCL_ERROR;
    }
    Tcl_SetObjResult(interp,Tcl_NewLongObj(room->vnum));
    return TCL_OK;
}

// log
TCL_MUD_CMD(mud_log) {
    Tcl_Obj *concatObj;

    if (objc<=1) {
	Tcl_WrongNumArgs(interp,0,objv,"mud log text ?text text ...?");
	return TCL_ERROR;
    }

    concatObj=Tcl_ConcatObj(objc-1,objv+1);
    Tcl_IncrRefCount(concatObj);

    logf("[-1] %s",Tcl_GetString(concatObj));
    Tcl_DecrRefCount(concatObj);
    return TCL_OK;
}

// allchar [-name]
TCL_MUD_CMD(mud_allchar) {
    static tclOptionList *charsSwitches[]={"-name",NULL};
    bool name=FALSE;
    CHAR_DATA *pc;
    char *s;
    Tcl_Obj *result;

    if (objc>1) {
	s=Tcl_GetString(objv[1]);
	if (*s=='-') {
	    int sindex;
	    if (Tcl_GetIndexFromObj(interp, objv[1], charsSwitches, "switch", 0, &sindex) != TCL_OK)
		return TCL_ERROR;
	    if (sindex==0) name=TRUE;
	}
    }
    result=Tcl_NewListObj(0,NULL);
    for (pc=char_list;pc;pc=pc->next) {
	if (IS_NPC(pc)) continue;
	if (name) {
	    Tcl_ListObjAppendElement(interp,result,Tcl_NewStringObj(pc->name,-1));
	} else {
	    Tcl_ListObjAppendElement(interp,result,Tcl_NewLongObj(pc->id));
	}
    }
    Tcl_SetObjResult(interp,result);
    return TCL_OK;
}

TCL_MUD_CMD(mud_weather) {
    static tclOptionList *items[]={"pressure","sky","sunlight",NULL};
    Tcl_Obj *result;
    int index;

    if (objc>1) {
	if (Tcl_GetIndexFromObj(interp, objv[1], items, "weather item", 0, &index) != TCL_OK)
	    return TCL_ERROR;
    } else 
	index=-1;

    switch (index) {
	case -1:
	    result=Tcl_NewListObj(0,NULL);
	    Tcl_ListObjAppendElement(interp,result,Tcl_NewIntObj(weather_info.mmhg));
	    Tcl_ListObjAppendElement(interp,result,Tcl_NewStringObj(table_find_value(weather_info.sky,weather_sky_table),-1));
	    Tcl_ListObjAppendElement(interp,result,Tcl_NewStringObj(table_find_value(weather_info.sunlight,weather_sun_table),-1));
	    break;
	case 0:
	    result=Tcl_NewIntObj(weather_info.mmhg);
	    break;
	case 1:
	    result=Tcl_NewStringObj(table_find_value(weather_info.sky,weather_sky_table),-1);
	    break;
	case 2:
	    result=Tcl_NewStringObj(table_find_value(weather_info.sunlight,weather_sun_table),-1);
	    break;
    }

    Tcl_SetObjResult(interp,result);
    return TCL_OK;
}
