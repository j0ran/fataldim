//
// $Id: wiz_set.c,v 1.13 2008/05/11 20:50:47 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"
#include "interp.h"


bool check_range_and_complain(CHAR_DATA *ch,int value,int min,int max,char *fmt) {
    if ( value<min || value>max ) {
        sprintf_to_char(ch,fmt,min,max);
        return FALSE;
    }
    return TRUE;
}


void do_set( CHAR_DATA *ch, char *argument ) {
    char arg[MAX_INPUT_LENGTH];

    argument = one_argument(argument,arg);
    switch (which_keyword(arg,"mobile","character","skill","spell",
                          "object","room","clan","mud","newbie",
                          "weapon","effect","logging","command",
                          NULL)) {
    case -1:
    case  0:
        send_to_char("Syntax:\n\r",ch);
        send_to_char("  set mob    <mob> <field> <value>\n\r",ch);
        send_to_char("  set obj    <obj> <field> <value>\n\r",ch);
        send_to_char("  set room   <room> <field> <value>\n\r",ch);
        send_to_char("  set clan   <clan> <field> <value>\n\r",ch);
        send_to_char("  set skill  <char> <spell or skill> <value>\n\r",ch);
        send_to_char("  set mud    <option>\n\r",ch);
        send_to_char("  set newbie <option> <value>\n\r",ch);
        send_to_char("  set weapon <obj> <flags>\n\r",ch);
        send_to_char("  set effect <obj> add <loc> <#mod> [obj|eff|imm|res|vuln] [flags]\n\r",ch);
        send_to_char("  set logging <mob> [filename]\n\r",ch);
        send_to_char("  set effect <obj> del <eff#>\n\r",ch);
        send_to_char("  set command <command> < off | on | [no]log >\n\r",ch);
        return;
    case  1:
    case  2:  set_mob(ch,argument);return;
    case  3:
    case  4:  set_skill(ch,argument);return;
    case  5:  set_object(ch,argument);return;
    case  6:  set_room(ch,argument);return;
    case  7:  set_clan(ch,argument);return;
    case  8:  set_mud(ch,argument);return;
    case  9:  set_newbie(ch,argument);return;
    case 10:  set_weapon(ch,argument);return;
    case 11:  set_effect(ch,argument);return;
    case 12:  set_mud_logging(ch,argument);return;
    case 13:  set_command(ch,argument);return;
    }
}



void set_skill(CHAR_DATA *ch, char *argument ) {
    char arg1 [MAX_INPUT_LENGTH];
    char arg2 [MAX_INPUT_LENGTH];
    char arg3 [MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    int value,decay;
    int sn;
    bool fAll;

    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );
    argument = one_argument( argument, arg3 );

    if ( arg1[0] == '\0' || arg2[0] == '\0' || arg3[0] == '\0' ) {
        send_to_char( "Syntax:\n\r",ch);
        send_to_char( "  set skill <name> <spell or skill> <value> [<decay>]\n\r", ch);
        send_to_char( "  set skill <name> all <value> [<decay>]\n\r",ch);
        send_to_char("   (use the name of the skill, not the number)\n\r",ch);
        return;
    }

    if ( ( victim = get_char_world( ch, arg1 ) ) == NULL ) {
        send_to_char( "They aren't here.\n\r", ch );
        return;
    }

    if ( IS_NPC(victim) ) {
        send_to_char( "Not on NPC's.\n\r", ch );
        return;
    }

    fAll = !str_cmp( arg2, "all" );
    sn   = 0;
    if ( !fAll && ( sn = skill_lookup( arg2 ) ) < 0 ) {
        send_to_char( "No such skill or spell.\n\r", ch );
        return;
    }

    /*
     * Snarf the value.
     */
    if ( !is_number( arg3 ) ) {
        send_to_char( "Value must be numeric.\n\r", ch );
        return;
    }

    value = atoi( arg3 );
    if ( value < 0 || value > 100 ) {
        send_to_char( "Value range is 0 to 100.\n\r", ch );
        return;
    }

    if (argument[0]) {
        if ( !is_number( argument ) ) {
            send_to_char( "Decay must be numeric.\n\r", ch );
            return;
        }

        decay = atoi( argument );
        if ( (value-decay<1) ||
             (value-decay>100)) {
            send_to_char("Invalid value/decay combination.\n\r",ch);
            return;
        }
    } else
        decay=0;

    if ( fAll ) {
        for ( sn = 0; sn < MAX_SKILL; sn++ ) {
            if ( skill_exists(sn) ) {
                victim->pcdata->skill[sn].learned	= value;
                victim->pcdata->skill[sn].forgotten	= decay;
            }
        }
    } else {
        victim->pcdata->skill[sn].learned	= value;
        victim->pcdata->skill[sn].forgotten	= decay;
    }

    return;
}

void set_mob( CHAR_DATA *ch, char *argument ) {
    char arg1 [MAX_INPUT_LENGTH];
    char arg2 [MAX_INPUT_LENGTH];
    char arg3 [MAX_INPUT_LENGTH];
    CHAR_DATA *victim, *target;
    int race, class,value;

    smash_tilde( argument );
    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );
    strcpy( arg3, argument );

    if ( arg1[0] == '\0' || arg2[0] == '\0' || arg3[0] == '\0' ) {
        send_to_char("Syntax:\n\r",ch);
        send_to_char("  set char <name> <field> <value>\n\r",ch);
        send_to_char( "  Field being one of:\n\r",			ch );
        send_to_char( "    str int wis dex con sex class level\n\r",	ch );
        send_to_char( "    race group gold silver hp mana move prac\n\r",ch);
        send_to_char( "    align train thirst hunger drunk full\n\r",	ch );
        send_to_char( "    security recall questpnt hunting\n\r",	ch );
        send_to_char( "    adrenaline property beeninroom killedmob\n\r",ch );
        return;
    }

    if ( ( victim = get_char_world( ch, arg1 ) ) == NULL ) {
        send_to_char( "They aren't here.\n\r", ch );
        return;
    }

    /* clear zones for mobs */
    victim->zone = NULL;

    /*
     * Snarf the value (which need not be numeric).
     */
    value = is_number( arg3 ) ? atoi( arg3 ) : -1;

    /*
     * Set something.
     */

    switch(which_keyword(arg2,
                         "str",     "int",    "wis",   "dex",      "con",
                         "sex",     "level",  "gold",  "silver",   "class",
                         "hp",      "mana",   "move",  "practice", "train",
                         "align",   "thirst", "drunk", "full",     "hunger",
                         "race",    "recall", "group", "security", "questpnt",
                         "hunting", "property","adrenaline","beeninroom","killedmob",
                         NULL)) {

    case 1:  /* str */
        if (check_range_and_complain(ch,value,3,get_max_train(victim,STAT_STR),
                                     "Strength range is %d to %d.\n\r"))
            victim->perm_stat[STAT_STR] = value;
        return;

    case 2: /* int */
        if (check_range_and_complain(ch,value,3,get_max_train(victim,STAT_INT),
                                     "Intelligence range is %d to %d.\n\r"))
            victim->perm_stat[STAT_INT] = value;
        return;

    case 3: /* wis */
        if (check_range_and_complain(ch,value,3,get_max_train(victim,STAT_WIS),
                                     "Wisdom range is %d to %d.\n\r"))
            victim->perm_stat[STAT_WIS] = value;
        return;

    case 4: /*dex */
        if (check_range_and_complain(ch,value,3,get_max_train(victim,STAT_DEX),
                                     "Dexterity range is %d to %d.\n\r"))
            victim->perm_stat[STAT_DEX] = value;
        return;

    case 5: /* con */
        if (check_range_and_complain(ch,value,3,get_max_train(victim,STAT_CON),
                                     "Constitution range is %d to %d.\n\r"))
            victim->perm_stat[STAT_CON] = value;
        return;

    case 6: /* sex */
        if (check_range_and_complain(ch,value,0,2,"Sex range is %d to %d.\n\r")) {
            victim->Sex = value;
            if (!IS_NPC(victim))
                victim->pcdata->true_sex = value;
        }
        return;

    case  7: /* level */
        if ( !IS_NPC(victim) )
            send_to_char( "Not on PC's.\n\r", ch );
        else if (check_range_and_complain(ch,value,0,MAX_LEVEL,"Level range is %d to %d\n\r."))
            victim->level = value;
        return;

    case  8: /* gold */
        victim->gold = value;
        return;

    case  9: /* silver */
        victim->silver = value;
        return;

    case 10: /* class */
        if (IS_NPC(victim)) {
            send_to_char("Mobiles have no class.\n\r",ch);
            return;
        }

        class = class_lookup(arg3);
        if ( class == -1 ) {
            char buf[MAX_STRING_LENGTH];

            strcpy( buf, "Possible classes are: " );
            for ( class = 0; class < MAX_CLASS; class++ ) {
                if ( class > 0 )
                    strcat( buf, " " );
                strcat( buf, class_table[class].name );
            }
            strcat( buf, ".\n\r" );

            send_to_char(buf,ch);
            return;
        }
        victim->class = class;
        return;

    case 11: /* hp */
        if (check_range_and_complain(ch,value,-10,30000,"Hp range is %d to %d hit points.\n\r")) {
            victim->max_hit = value;
            if (!IS_NPC(victim))
                victim->pcdata->perm_hit = value;
        }
        return;

    case 12: /* mana */
        if (check_range_and_complain(ch,value,0,30000,"Mana range is %d to %d mana points.\n\r")) {
            victim->max_mana = value;
            if (!IS_NPC(victim))
                victim->pcdata->perm_mana = value;
        }
        return;

    case 13: /* move */
        if (check_range_and_complain(ch,value,0,30000,"Move range is %d to %d move points.\n\r")) {
            victim->max_move = value;
            if (!IS_NPC(victim))
                victim->pcdata->perm_move = value;
        }
        return;

    case 14: /* practice */
        if (check_range_and_complain(ch,value,0,250,"Practice range is %d to %d sessions.\n\r"))
            victim->practice = value;
        return;

    case 15: /* train */
        if (check_range_and_complain(ch,value,0,50,"Training session range is %d to %d sessions.\n\r"))
            victim->train = value;
        return;

    case 16: /* align */
        if (check_range_and_complain(ch,value,-1000,1000,"Alignment range is %d to %d.\n\r"))
            victim->alignment = value;
        return;

    case 17: /* Thirst */
        if ( IS_NPC(victim) ) {
            send_to_char( "Not on NPC's.\n\r", ch );
            return;
        }
        if (check_range_and_complain(ch,value,-1,100,"Thirst range is %d to %d sessions.\n\r"))
            victim->pcdata->condition[COND_THIRST] = value;
        return;

    case 18: /* drunk */
        if ( IS_NPC(victim) ) {
            send_to_char( "Not on NPC's.\n\r", ch );
            return;
        }
        if (check_range_and_complain(ch,value,-1,100,"Drunk range is %d to %d sessions.\n\r"))
            victim->pcdata->condition[COND_DRUNK] = value;
        return;

    case 19: /* full */
        if ( IS_NPC(victim) ) {
            send_to_char( "Not on NPC's.\n\r", ch );
            return;
        }
        if (check_range_and_complain(ch,value,-1,100,"Full range is %d to %d sessions.\n\r"))
            victim->pcdata->condition[COND_FULL] = value;
        return;

    case 20: /* hunger */
        if ( IS_NPC(victim) )
        {
            send_to_char( "Not on NPC's.\n\r", ch );
            return;
        }
        if (check_range_and_complain(ch,value,-1,100,"Hunger range is %d to %d sessions.\n\r"))
            victim->pcdata->condition[COND_HUNGER] = value;
        return;

    case 21: /* race */
        race = race_lookup(arg3);
        if ( race == 0) {
            send_to_char("That is not a valid race.\n\r",ch);
            return;
        }

        if (!IS_NPC(victim) && !race_table[race].pc_race) {
            send_to_char("That is not a valid player race.\n\r",ch);
            return;
        }
        victim->race = race;
        return;

    case 22: /* recall */
        victim->recall = atoi(arg3);
        return;

    case 23: /* group */
        if (!IS_NPC(victim)) {
            send_to_char("Only on NPCs.\n\r",ch);
            return;
        }
        victim->group = value;
        return;

    case 24: /* security */
        if ( IS_NPC( victim ) ) {
            send_to_char( "Not on NPC's.\n\r", ch );
            return;
        }

        if ( !IS_TRUSTED(ch,LEVEL_COUNCIL)) {
            send_to_char("Only Council members and higher can do this.\n\r",ch);
            return;
        }

        if (value>ch->pcdata->security || value>9) {
            sprintf_to_char(ch, "Valid security is 0-%d.\n\r",
                            ch->pcdata->security );
            return;
        }

        victim->pcdata->security = value;
        sprintf_to_char(ch,"Security set to %d\n",value);
        return;

    case 25: /*questpnt */
        if( IS_NPC(victim) )
            send_to_char( "Not on NPC's.\n\r", ch );
        else if ( value < 0  )
            send_to_char( "Quest points must be >0\n\r", ch );
        else
            victim->pcdata->questpoints = value;
        return;

    case 26: /* hunting */
        if( !IS_NPC(victim) )
            send_to_char("Only on NPC's.\n\r", ch );
        else if(!(target=get_char_world(ch,arg3)))
            send_to_char("That character could not be found.\n\r",ch);
        else {
            victim->hunt_type=HUNT_KILL;
            victim->hunt_id=target->id;
        }
        return;

    case 27: /* property */
        set_mob_property(ch,victim,arg3);
        break;

    case 28: /* adrenaline */
        if( IS_NPC(victim) )
            send_to_char("Only on PC's.\n\r", ch );
        else {
            if (check_range_and_complain(ch,value,-1,100,
                                         "Adrenaline range is %d to %d sessions.\n\r"))
                victim->pcdata->condition[COND_ADRENALINE]=value;
        }
        return;

    case 29: // beeninroom
        if( IS_NPC(victim) ) {
            send_to_char("Only on PC's.\n\r", ch );
            return;
        }
        STR_TOGGLE_BIT(victim->pcdata->beeninroom,value);
        sprintf_to_char(ch,"Room #%d is now %svisited.\n\r",
                        value,STR_IS_SET(victim->pcdata->beeninroom,value)?"":"un");
        return;

    case 30: // killedmob
        if( IS_NPC(victim) ) {
            send_to_char("Only on PC's.\n\r", ch );
            return;
        }
        STR_TOGGLE_BIT(victim->pcdata->killedthatmob,value);
        sprintf_to_char(ch,"Mob #%d is now %smarked as killed.\n\r",
                        value,
                        STR_IS_SET(victim->pcdata->killedthatmob,value)?"":"not anymore ");
        return;

    default:
        set_mob(ch,NULL);
        return;
    }
}

void do_string( CHAR_DATA *ch, char *argument )
{
    char type [MAX_INPUT_LENGTH];
    char arg1 [MAX_INPUT_LENGTH];
    char arg2 [MAX_INPUT_LENGTH];
    char arg3 [MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    OBJ_DATA *obj;

    smash_tilde( argument );
    argument = one_argument( argument, type );
    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );
    strcpy( arg3, argument );

    if ( type[0] == '\0' || arg1[0] == '\0' || arg2[0] == '\0' || arg3[0] == '\0' )
    {
        send_to_char("Syntax:\n\r",ch);
        send_to_char("  string char <name> <field> <string>\n\r",ch);
        send_to_char("    fields: name short long desc title spec whoname password\n\r",ch);
        send_to_char("  string obj  <name> <field> <string>\n\r",ch);
        send_to_char("    fields: name short long extended\n\r",ch);
        return;
    }

    if (!str_prefix(type,"character") || !str_prefix(type,"mobile"))
    {
        if ( ( victim = get_char_world( ch, arg1 ) ) == NULL )
        {
            send_to_char( "They aren't here.\n\r", ch );
            return;
        }

        /* clear zone for mobs */
        victim->zone = NULL;

        /* string something */

        if ( !str_prefix( arg2, "name" ) )
        {
            if ( !IS_NPC(victim) )
            {
                send_to_char( "Not on PC's.\n\r", ch );
                return;
            }
            free_string( victim->name );
            victim->name = str_dup( arg3 );
            return;
        }

        if ( !str_prefix( arg2, "description" ) )
        {
            free_string(victim->description);
            victim->description = str_dup(arg3);
            return;
        }

        if ( !str_prefix( arg2, "short" ) )
        {
            free_string( victim->short_descr );
            victim->short_descr = str_dup( arg3 );
            return;
        }

        if ( !str_prefix( arg2, "long" ) )
        {
            free_string( victim->long_descr );
            strcat(arg3,"\n\r");
            victim->long_descr = str_dup( arg3 );
            return;
        }

        if ( !str_prefix( arg2, "password" ) )
        {
            if ( IS_NPC(victim) )
            {
                send_to_char( "Not on NPC's.\n\r", ch );
                return;
            }
            if ( get_trust(ch)<LEVEL_COUNCIL) {
                send_to_char( "Do you feel lucky, punk?\n\r", ch );
                return;
            }
            free_string( victim->pcdata->pwd );
            victim->pcdata->pwd = str_dup( crypt(arg3,victim->name));
            send_to_char( "Ok, password is resetted.\n\r", ch );
            return;
        }

        if ( !str_prefix( arg2, "title" ) )
        {
            if ( IS_NPC(victim) )
            {
                send_to_char( "Not on NPC's.\n\r", ch );
                return;
            }

            set_title( victim, arg3 );
            return;
        }

        if ( !str_prefix( arg2, "whoname" ) )
        {
            if( IS_NPC(victim) )
            {
                send_to_char( "Not on NPC's.\n\r", ch);
                return;
            }

            free_string( victim->pcdata->whoname);
            if(!str_cmp(arg3,"none")) victim->pcdata->whoname=str_dup("");
            else victim->pcdata->whoname = str_dup( arg3 );
            return;
        }

        if ( !str_prefix( arg2, "spec" ) )
        {
            if ( !IS_NPC(victim) )
            {
                send_to_char( "Not on PC's.\n\r", ch );
                return;
            }

            if ( ( victim->spec_fun = spec_lookup( arg3 ) ) == 0 )
            {
                send_to_char( "No such spec fun.\n\r", ch );
                return;
            }

            return;
        }
    }

    if (!str_prefix(type,"object"))
    {
        /* string an obj */

        if ( ( obj = get_obj_world( ch, arg1 ) ) == NULL )
        {
            send_to_char( "Nothing like that in heaven or earth.\n\r", ch );
            return;
        }

        if ( !str_prefix( arg2, "name" ) )
        {
            free_string( obj->name );
            obj->name = str_dup( arg3 );
            return;
        }

        if ( !str_prefix( arg2, "short" ) )
        {
            free_string( obj->short_descr );
            obj->short_descr = str_dup( arg3 );
            return;
        }

        if ( !str_prefix( arg2, "long" ) )
        {
            free_string( obj->description );
            obj->description = str_dup( arg3 );
            return;
        }

        if ( !str_prefix( arg2, "ed" ) || !str_prefix( arg2, "extended"))
        {
            EXTRA_DESCR_DATA *ed;

            argument = one_argument( argument, arg3 );
            if ( argument == NULL )
            {
                send_to_char( "Syntax: oset <object> ed <keyword> <string>\n\r",
                              ch );
                return;
            }

            strcat(argument,"\n\r");

            ed = new_extra_descr();

            ed->keyword		= str_dup( arg3     );
            ed->description	= str_dup( argument );
            ed->next		= obj->extra_descr;
            obj->extra_descr	= ed;
            return;
        }
    }


    /* echo bad use message */
    do_string(ch,NULL);
}



void set_object( CHAR_DATA *ch, char *argument ) {
    char arg1 [MAX_INPUT_LENGTH];
    char arg2 [MAX_INPUT_LENGTH];
    char arg3 [MAX_INPUT_LENGTH];
    OBJ_DATA *obj;
    int value;

    smash_tilde( argument );
    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );
    strcpy( arg3, argument );

    if ( arg1[0] == '\0' || arg2[0] == '\0' || arg3[0] == '\0' )
    {
        send_to_char("Syntax:\n\r",ch);
        send_to_char("  set obj <object> <field> <value>\n\r",ch);
        send_to_char("  Field being one of:\n\r",ch );
        send_to_char("    v0 v1 v2 v3 v4\n\r",ch );
        send_to_char("    extra wear level weight cost timer\n\r",ch );
        send_to_char("    property owner\n\r",ch);
        return;
    }

    if ( ( obj = get_obj_world( ch, arg1 ) ) == NULL )
    {
        send_to_char( "Nothing like that in heaven or earth.\n\r", ch );
        return;
    }

    /*
     * Snarf the value (which need not be numeric).
     */
    value = atoi( arg3 );

    /*
     * Set something.
     */

    switch(which_keyword(arg2, "value0", "v0", "value1", "v1",
                         "value2", "v2", "value3", "v3",
                         "value4", "v4", "extra", "wear",
                         "level",  "weight", "cost", "timer",
                         "property","owner",NULL))
    {
    case  1:
    case  2: obj->value[0] = UMIN(50,value); return;
    case  3:
    case  4: obj->value[1] = value; return;
    case  5:
    case  6: obj->value[2] = value; return;
    case  7:
    case  8: obj->value[3] = value; return;
    case  9:
    case 10: obj->value[4] = value; return;
    case 11: set_object_extra(ch,obj,argument);return;
    case 12: set_object_wear(ch,obj,argument); return;
    case 13: obj->level = value; return;
    case 14: obj->weight = value; return;
    case 15: obj->cost = value; return;
    case 16: obj->timer = value; return;
    case 17: set_object_property(ch,obj,argument); return;
    case 18: set_object_owner(ch,obj,arg3); return;
    default: set_object(ch,NULL);
    }
}



void set_room( CHAR_DATA *ch, char *argument )
{
    char arg1 [MAX_INPUT_LENGTH];
    char arg2 [MAX_INPUT_LENGTH];
    char arg3 [MAX_INPUT_LENGTH];
    ROOM_INDEX_DATA *location;

    smash_tilde( argument );
    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );
    strcpy( arg3, argument );

    if ( arg1[0] == '\0' || arg2[0] == '\0' || arg3[0] == '\0' ) {
        send_to_char( "Syntax:\n\r",ch);
        send_to_char( "  set room <location> <field> <value>\n\r",ch);
        send_to_char( "  <location> can be also \"this\"\n\r",ch);
        send_to_char( "  Field being one of:\n\r",			ch );
        send_to_char( "    flags sector property\n\r",			ch );
        return;
    }

    if (!str_cmp(arg1,"this")) {
        location=ch->in_room;
    } else if ( ( location = find_location( ch, arg1 ) ) == NULL ) {
        send_to_char( "No such location.\n\r", ch );
        return;
    }

    if (!is_room_owner(ch,location) &&
            ch->in_room != location &&
            !is_allowed_in_room(ch,location) &&
            room_is_private(location) && !IS_TRUSTED(ch,LEVEL_COUNCIL))
    {
        send_to_char("That room is private right now.\n\r",ch);
        return;
    }

    switch(which_keyword(arg2,"flags","sector","property",NULL)) {
    case  1:
    {
        int value;

        if ((value=option_find_name(arg3,room_options,TRUE))==0) {
            send_to_char("No such room-option found.\n\r",ch);
            olc_help_options(ch,"Available room-options",room_options);
            return;
        }
        STR_TOGGLE_BIT(location->strbit_room_flags,value);

        sprintf_to_char(ch,"Room has %s the flag '%s'.\n\r",
                        STR_IS_SET(location->strbit_room_flags,value)
                        ?"now":"not longer",
                        option_find_value(value,room_options));

        return;
    }

    case  2:
    {
        int value;

        if ((value=table_find_name(arg3,sector_table))==0) {
            send_to_char("No such sector found.\n\r",ch);
            olc_help_table(ch,"Available sector-types",sector_table);
            return;
        }
        location->sector_type=value;

        sprintf_to_char(ch,"Room is now of sector '%s'.\n\r",
                        table_find_value(value,sector_table));
        return;
    }

    case  3:
        set_room_property(ch,location,argument);
        return;
    default:
        set_room(ch,NULL);
    }
}

void set_clan( CHAR_DATA *ch, char *argument ) {
    char arg1 [MAX_INPUT_LENGTH];
    char arg2 [MAX_INPUT_LENGTH];
    char arg3 [MAX_INPUT_LENGTH];
    CLAN_INFO *clan;
    int value;
    char *ps;

    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );
    strcpy( arg3, argument );

    if (arg1[0]==0 || arg2[0]==0 ) {
        send_to_char("Syntax:\n\r",ch);
        send_to_char("  set clan <clan> <field> [value]\n\r",ch);
        send_to_char("  Field being one of:\n\r",ch);
        send_to_char("    recall hall description name whoname\n\r",ch);
        send_to_char("    deleted new independent url\n\r",ch);
        return;
    }

    if ((clan=get_clan_by_name(arg1))==NULL) {
        send_to_char( "No such clan could not be found.\n\r", ch );
        return;
    }

    switch (which_keyword(arg2,"recall","hall","description",
                          "name","whoname","deleted","new","independent","url",
                          NULL)) {
    case 0:
    case -1:
        set_clan(ch,NULL);
        return;

    case  1:
        if (!is_number(arg3))
            send_to_char( "Value must be numeric.\n\r", ch );
        else {
            value = atoi( arg3 );
            if (get_room_index(value)==NULL)
                send_to_char("That room does not exist.\n\r",ch);
            else
                clan->recall = value;
        }
        return;

    case  2:
        if (!is_number(arg3))
            send_to_char( "Value must be numeric.\n\r", ch );
        else {
            value = atoi( arg3 );
            if (get_room_index(value)==NULL)
                send_to_char("That room does not exist.\n\r",ch);
            else
                clan->hall = value;
        }
        return;

    case 3:
        sprintf_to_char(ch,"Enter clan description for %s:\n\r",clan->name);
        editor_start(ch,&clan->description);
        return;

    case 4:
        if (arg3[0]==0) {
            send_to_char("What will be the new name?\n\r",ch);
        } else {
            free_string(clan->name);
            smash_tilde(arg3);
            clan->name=str_dup(arg3);
        }
        return;

    case 5: /* whoname */
        if (arg3[0]==0) {
            send_to_char("What will be the new whoname?\n\r",ch);
            send_to_char(
                        "It should start with a [ and be 8 characters long.\n\r",
                        ch);
        } else {
            free_string(clan->who_name);
            smash_tilde(arg3);
            clan->who_name=str_dup(arg3);
        }
        return;

    case 6: /* deleted */
        if (clan->deleted) {
            clan->deleted=FALSE;
            sprintf_to_char(ch,"%s has been resurrected.\n\r",clan->name);
        } else {
            clan->deleted=TRUE;
            sprintf_to_char(ch,"%s has been terminated.\n\r",clan->name);
        }
        return;

    case 7:	/* new */
        clan               = new_clan();
        clan->name         = str_dup("newclan");
        clan->who_name     = str_dup("whoname");
        clan->hall         = ROOM_VNUM_ALTAR;
        clan->recall       = ROOM_VNUM_TEMPLE;
        clan->next         = clan_list;
        clan_list          = clan;

        return;
    case 8: /* independent */
        clan->independent=clan->independent?FALSE:TRUE;
        sprintf_to_char(ch,"%s is %s independent.\n\r",
                        clan->name,clan->independent?"now":"not anymore");
        return;

    case 9: // url
        if (arg3[0]==0) {
            send_to_char("URL deleted\n\r",ch);
            free_string(clan->url);
            clan->url=str_dup("");
            return;
        }
        free_string(clan->url);
        while ((ps=strchr(arg3,'~'))!=NULL) {
            strcpy(arg2,ps+1);
            ps[0]='%';
            ps[1]='7';
            ps[2]='e';
            ps[3]=0;
            strcat(arg3,arg2);
        }
        clan->url=str_dup(arg3);
        sprintf_to_char(ch,"New url set to %s.\n\r",clan->url);
        return;
    }
}

void set_weapon(CHAR_DATA *ch, char *argument) {
    char arg1 [MAX_INPUT_LENGTH];
    char arg2 [MAX_INPUT_LENGTH];
    OBJ_DATA *obj;
    int i;

    smash_tilde( argument );
    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );

    if ( arg1[0] == '\0' || arg2[0] == '\0' ) {
        send_to_char("Syntax:\n\r",ch);
        send_to_char("  set weapon <obj> <flags>\n\r",ch);
        send_to_char("  Flags being one of:\n\r",ch);
        olc_help_options(ch,NULL,weaponflag_options);
        return;
    }

    if ( ( obj = get_obj_world( ch, arg1 ) ) == NULL ) {
        send_to_char( "Nothing like that in heaven or earth.\n\r", ch );
        return;
    }

    if ((i=option_find_name(arg2,weaponflag_options,TRUE))!=NO_FLAG) {
        sprintf_to_char(ch,"%s is %s %s.\n\r",
                        obj->short_descr,
                        IS_SET(obj->value[4],i)?"not longer":"now",
                option_find_value(i,weaponflag_options));
        obj->value[4]^=i;
        return;
    }

    send_to_char("No such flag found.\n\r",ch);
    olc_help_options(ch,NULL,weaponflag_options);
    return;
}

void set_effect(CHAR_DATA *ch, char *argument) {
    char obj[MAX_INPUT_LENGTH];
    char loc[MAX_INPUT_LENGTH];
    char mod[MAX_INPUT_LENGTH];
    char cmd[MAX_INPUT_LENGTH];
    char action[MAX_INPUT_LENGTH];
    OBJ_DATA *pobj;
    int type=TO_OBJECT;
    EFFECT_DATA *pef,*pef_next;
    int bitvector=0;
    int loc_flag,value;

    smash_tilde( argument );
    argument = one_argument( argument, obj );
    argument = one_argument( argument, action );
    argument = one_argument( argument, loc );
    argument = one_argument( argument, mod );
    argument = one_argument( argument, cmd );

    if (!str_cmp(action,"add")) {
        if (loc[0]==0 || mod[0]==0 || !is_number(mod)) {
            send_to_char("Syntax: set eff <obj> add <loc> <#mod> [<obj|eff|imm|res|vuln> <flags>]\n\r",ch);
            send_to_char("Syntax: set eff <obj> del <eff#>\n\r",ch);
            send_to_char("Where:\n\r",ch);
            olc_help_options(ch,"loc",apply_options);

            send_to_char("{ymod{x: a number\n\r",ch);
            olc_help_options(ch,"flags for eff",effect_options);
            olc_help_options(ch,"flags for imm",imm_options);
            olc_help_options(ch,"flags for res",res_options);
            olc_help_options(ch,"flags for vuln",vuln_options);

            return;
        }

        if ((loc_flag=option_find_name(loc,apply_options,TRUE))==NO_FLAG) {
            olc_help_options(ch,"flags for apply",apply_options);
            return;
        }

        if (cmd[0]) {
            if (!str_prefix(cmd,"effect")) {
                type=TO_EFFECTS;
                if ((bitvector=option_find_name(argument,effect_options,TRUE))==NO_FLAG) {
                    olc_help_options(ch,"flags for eff",effect_options);
                    return;
                }
            } else if (!str_prefix(cmd,"object")) {
                type=TO_OBJECT;
                bitvector=EFF_NONE;
            } else if (!str_prefix(cmd,"immune")) {
                type=TO_IMMUNE;
                if ((bitvector=option_find_name(argument,imm_options,TRUE))==NO_FLAG) {
                    olc_help_options(ch,"flags for imm",imm_options);
                    return;
                }
            } else if (!str_prefix(cmd,"resist")) {
                type=TO_RESIST;
                if ((bitvector=option_find_name(argument,res_options,TRUE))==NO_FLAG) {
                    olc_help_options(ch,"flags for res",res_options);
                    return;
                }
            } else if (!str_prefix(cmd,"vulnerability")) {
                type=TO_VULN;
                if ((bitvector=option_find_name(argument,vuln_options,TRUE))==NO_FLAG) {
                    olc_help_options(ch,"flags for vuln",vuln_options);
                    return;
                }
            } else if (!str_prefix(cmd,"skill")) {
                type=TO_SKILLS;
                if ((bitvector=skill_lookup(argument))==-1) {
                    send_to_char("Not a known skill/spell\n\r",ch);
                    return;
                }
            } else {
                send_to_char("Unknown flag-type, try obj, eff, imm, res or vuln.\n\r",ch);
                return;
            }
        }

        if ( ( pobj = get_obj_world( ch, obj ) ) == NULL ) {
            send_to_char( "Nothing like that in heaven or earth.\n\r", ch );
            return;
        }

        pef             =   new_effect();
        pef->where      =   type;
        pef->location   =   loc_flag;
        pef->modifier   =   atoi( mod );
        pef->level      =   pobj->level;
        pef->type       =   0;
        pef->duration   =   -1;
        pef->bitvector  =   bitvector;
        pef->next       =   pobj->affected;
        pobj->affected  =   pef;

        send_to_char("Effect added.\n\r",ch);
        return;
    }

    if (!str_cmp(action,"del")) {
        if ( ( pobj = get_obj_world( ch, obj ) ) == NULL ) {
            send_to_char( "Nothing like that in heaven or earth.\n\r", ch );
            return;
        }

        if (loc[0]==0 || !is_number(loc) || (value=atoi(loc))<0 ) {
            set_effect(ch,"x add");
            return;
        }

        pef=pobj->affected;
        if (!pef) {
            send_to_char("Effect not found, sorry.\n\r",ch);
            return;
        }
        if (value==0) {
            pobj->affected=pef->next;
            if (pef->where==TO_OBJECT2 &&
                    pef->bitvector&(ITEM_ANTI_CLASS|ITEM_ANTI_RACE|
                                    ITEM_CLASS_ONLY|ITEM_RACE_ONLY)) {
                STR_REMOVE_BIT(pobj->strbit_extra_flags2,ITEM_ANTI_CLASS);
                STR_REMOVE_BIT(pobj->strbit_extra_flags2,ITEM_ANTI_RACE);
                STR_REMOVE_BIT(pobj->strbit_extra_flags2,ITEM_CLASS_ONLY);
                STR_REMOVE_BIT(pobj->strbit_extra_flags2,ITEM_RACE_ONLY);
            }
            sprintf_to_char(ch,"Removing %s.\n\r",effect_loc_name(pef));
            free_effect(pef);
            return;
        }
        while ((pef_next=pef->next)&&--value)
            pef=pef_next;
        if (pef_next==NULL) {
            send_to_char("Effect not found, sorry.\n\r",ch);
            return;
        }
        pef->next = pef_next->next;
        if (pef->where==TO_OBJECT2 &&
                pef->bitvector&(ITEM_ANTI_CLASS|ITEM_ANTI_RACE|
                                ITEM_CLASS_ONLY|ITEM_RACE_ONLY)) {
            STR_REMOVE_BIT(pobj->strbit_extra_flags2,ITEM_ANTI_CLASS);
            STR_REMOVE_BIT(pobj->strbit_extra_flags2,ITEM_ANTI_RACE);
            STR_REMOVE_BIT(pobj->strbit_extra_flags2,ITEM_CLASS_ONLY);
            STR_REMOVE_BIT(pobj->strbit_extra_flags2,ITEM_RACE_ONLY);
        }
        sprintf_to_char(ch,"Removing %s.\n\r",effect_loc_name(pef_next));
        free_effect(pef_next);
        return;
    }

    set_effect(ch,"x add");
}


void set_object_property(CHAR_DATA *ch,OBJ_DATA *obj,char *argument) {
    char stype[MAX_STRING_LENGTH];
    char key[MAX_STRING_LENGTH];
    char svalue[MAX_STRING_LENGTH];
    long l;
    int  i;
    bool b;
    char c;

    argument=one_argument(argument,key);
    argument=one_argument(argument,stype);
    argument=one_argument(argument,svalue);

    if (svalue[0] == '\0') {
        send_to_char("Usage: set obj property <key> <type> <value>\n\r",ch);
        send_to_char("  Where <type> is \"bool\", \"int\", \"long\", \"char\","
                     "\"string\n\r",ch);
        return;
    }

    if (!does_property_exist_s(key,stype)) {
        send_to_char(
                    "Unknown property, see `{Wpropertylist{x` for overview.\n\r",ch);
        return;
    }

    switch (which_keyword(stype,"bool","int","long","char","string",NULL)) {
    case 1:
        switch (which_keyword(svalue,"true","false","delete",NULL)) {
        case 1:
            b=TRUE;
            break;
        case 2:
            b=FALSE;
            break;
        case 3:
            DeleteObjectProperty(obj,PROPERTY_BOOL,key);
            return;
        default:
            set_object_property(ch,obj,"");
            return;
        }
        SetObjectProperty(obj,PROPERTY_BOOL,key,&b);
        break;
    case 2:
        if (!strcmp(svalue,"delete"))
            DeleteObjectProperty(obj,PROPERTY_INT,key);
        else {
            i=atoi(svalue);
            SetObjectProperty(obj,PROPERTY_INT,key,&i);
        }
        break;
    case 3:
        if (!strcmp(svalue,"delete"))
            DeleteObjectProperty(obj,PROPERTY_LONG,key);
        else {
            l=atol(svalue);
            SetObjectProperty(obj,PROPERTY_LONG,key,&l);
        }
        break;
    case 4:
        if (!strcmp(svalue,"delete"))
            DeleteObjectProperty(obj,PROPERTY_CHAR,key);
        else {
            c=svalue[0];
            SetObjectProperty(obj,PROPERTY_CHAR,key,&c);
        }
        break;
    case 5:
        if (!strcmp(svalue,"delete"))
            DeleteObjectProperty(obj,PROPERTY_STRING,key);
        else
            SetObjectProperty(obj,PROPERTY_STRING,key,svalue);
        break;
    default:
        set_object_property(ch,obj,"");
        return;
    }

    send_to_char("Done.\n\r",ch);
}

void set_room_property(CHAR_DATA *ch,ROOM_INDEX_DATA *room,char *argument) {
    char stype[MAX_STRING_LENGTH];
    char key[MAX_STRING_LENGTH];
    char svalue[MAX_STRING_LENGTH];
    long l;
    int  i;
    bool b;
    char c;

    argument=one_argument(argument,key);
    argument=one_argument(argument,stype);
    argument=one_argument(argument,svalue);

    if (svalue[0] == '\0') {
        send_to_char("Usage: set room property <key> <type> <value>\n\r",ch);
        send_to_char("  Where <type> is \"bool\", \"int\", \"long\", \"char\","
                     "\"string\n\r",ch);
        return;
    }

    if (!does_property_exist_s(key,stype)) {
        send_to_char(
                    "Unknown property, see `{Wpropertylist{x` for overview.\n\r",ch);
        return;
    }

    switch (which_keyword(stype,"bool","int","long","char","string",NULL)) {
    case 1:
        switch (which_keyword(svalue,"true","false","delete",NULL)) {
        case 1:
            b=TRUE;
            break;
        case 2:
            b=FALSE;
            break;
        case 3:
            DeleteRoomProperty(room,PROPERTY_BOOL,key);
            return;
        default:
            set_room_property(ch,room,"");
            return;
        }
        SetRoomProperty(room,PROPERTY_BOOL,key,&b);
        break;
    case 2:
        if (!strcmp(svalue,"delete"))
            DeleteRoomProperty(room,PROPERTY_INT,key);
        else {
            i=atoi(svalue);
            SetRoomProperty(room,PROPERTY_INT,key,&i);
        }
        break;
    case 3:
        if (!strcmp(svalue,"delete"))
            DeleteRoomProperty(room,PROPERTY_LONG,key);
        else {
            l=atol(svalue);
            SetRoomProperty(room,PROPERTY_LONG,key,&l);
        }
        break;
    case 4:
        if (!strcmp(svalue,"delete"))
            DeleteRoomProperty(room,PROPERTY_CHAR,key);
        else {
            c=svalue[0];
            SetRoomProperty(room,PROPERTY_CHAR,key,&c);
        }
        break;
    case 5:
        if (!strcmp(svalue,"delete"))
            DeleteRoomProperty(room,PROPERTY_STRING,key);
        else
            SetRoomProperty(room,PROPERTY_STRING,key,svalue);
        break;
    default:
        set_room_property(ch,room,"");
        return;
    }

    send_to_char("Done.\n\r",ch);
}

void set_mob_property(CHAR_DATA *ch,CHAR_DATA *victim,char *argument) {
    char stype[MAX_STRING_LENGTH];
    char key[MAX_STRING_LENGTH];
    char svalue[MAX_STRING_LENGTH];
    long l;
    int  i;
    bool b;
    char c;

    argument=one_argument(argument,key);
    argument=one_argument(argument,stype);
    argument=one_argument(argument,svalue);

    if (svalue[0] == '\0') {
        send_to_char("Usage: set char property <key> <type> <value>\n\r",ch);
        send_to_char("  Where <type> is \"bool\", \"int\", \"long\", \"char\","
                     "\"string\n\r",ch);
        return;
    }

    if (!does_property_exist_s(key,stype)) {
        send_to_char(
                    "Unknown property, see `{Wpropertylist{x` for overview.\n\r",ch);
        return;
    }

    switch (which_keyword(stype,"bool","int","long","char","string",NULL)) {
    case 1:
        switch (which_keyword(svalue,"true","false","delete",NULL)) {
        case 1:
            b=TRUE;
            break;
        case 2:
            b=FALSE;
            break;
        case 3:
            DeleteCharProperty(victim,PROPERTY_BOOL,key);
            return;
        default:
            set_mob_property(ch,victim,"");
            return;
        }
        SetCharProperty(victim,PROPERTY_BOOL,key,&b);
        break;
    case 2:
        if (!strcmp(svalue,"delete"))
            DeleteCharProperty(victim,PROPERTY_INT,key);
        else {
            i=atoi(svalue);
            SetCharProperty(victim,PROPERTY_INT,key,&i);
        }
        break;
    case 3:
        if (!strcmp(svalue,"delete"))
            DeleteCharProperty(victim,PROPERTY_LONG,key);
        else {
            l=atol(svalue);
            SetCharProperty(victim,PROPERTY_LONG,key,&l);
        }
        break;
    case 4:
        if (!strcmp(svalue,"delete"))
            DeleteCharProperty(victim,PROPERTY_CHAR,key);
        else {
            c=svalue[0];
            SetCharProperty(victim,PROPERTY_CHAR,key,&c);
        }
        break;
    case 5:
        if (!strcmp(svalue,"delete"))
            DeleteCharProperty(victim,PROPERTY_STRING,key);
        else
            SetCharProperty(victim,PROPERTY_STRING,key,svalue);
        break;
    default:
        set_mob_property(ch,victim,"");
        return;
    }

    send_to_char("Done.\n\r",ch);
}

void set_object_extra(CHAR_DATA *ch,OBJ_DATA *obj,char *argument) {
    char arg1[MAX_STRING_LENGTH];
    int i;

    smash_tilde(argument);
    one_argument(argument,arg1);

    if ((i=option_find_name(arg1,extra_options,TRUE))==NO_FLAG) {
        send_to_char("No such flag found.\n\r",ch);
        olc_help_options(ch,NULL,extra_options);
        return;
    }

    sprintf_to_char(ch,"%s has %s the extra-bit '%s'.\n\r",
                    obj->short_descr,
                    STR_IS_SET(obj->strbit_extra_flags,i)?"not longer":"now",
                    option_find_value(i,extra_options));
    STR_TOGGLE_BIT(obj->strbit_extra_flags,i);
}

void set_object_wear(CHAR_DATA *ch,OBJ_DATA *obj,char *argument) {
    char arg1[MAX_STRING_LENGTH];
    int i;

    smash_tilde(argument);
    one_argument(argument,arg1);

    if ((i=option_find_name(arg1,wear_options,TRUE))==NO_FLAG) {
        send_to_char("No such flag found.\n\r",ch);
        olc_help_options(ch,NULL,wear_options);
        return;
    }

    sprintf_to_char(ch,"%s has %s the wear-bit '%s'.\n\r",
                    obj->short_descr,
                    IS_SET(obj->wear_flags,i)?"not longer":"now",
                    option_find_value(i,wear_options));
    TOGGLE_BIT(obj->wear_flags,i);
}

void set_object_owner(CHAR_DATA *ch,OBJ_DATA *obj, char *newowner) {
    CHAR_DATA *victim;

    obj_is_owned(obj); // collapse old style properties

    if (!str_cmp(newowner,"none")) {
        if (obj->owner_name)
            free_string(obj->owner_name);
        obj->owner_name=NULL;
        obj->owner_id=-1;
        return;
    }

    if (!player_exist(newowner)) {
        sprintf_to_char(ch,"%s is not a player.\n\r",newowner);
        return;
    }

    free_string(obj->owner_name);
    obj->owner_name=str_dup(newowner);

    victim=get_char_world(ch,newowner);

    if (victim && IS_PC(victim) && !str_cmp(victim->name,newowner)) { // better safe than sorry
        obj->owner_id=victim->id;
    } else {
        obj->owner_id=-1;
    }

}

void set_mud_logging(CHAR_DATA *ch,char *argument) {
    CHAR_DATA *victim;
    char arg1[MSL],arg2[MSL];

    if (ch->level<99) {
        send_to_char("Only level 99 and higher!\n\r",ch);
        return;
    }

    argument=one_argument(argument,arg1);
    argument=one_argument(argument,arg2);

    if ( arg1[0]==0 || ( victim = get_char_world( ch, arg1 ) ) == NULL ) {
        send_to_char( "Put log on who?\n\r", ch );
        return;
    }

    if( IS_PC(victim) ) {
        send_to_char("Only on NPC's.\n\r",ch);
        return;
    }

    if (victim->desc==NULL) {
        DESCRIPTOR_DATA *logging_descriptor;
        char filename[MSL];
        int desc;

        if (arg2[0]==0) {
            sprintf(filename,"logging.%d",victim->pIndexData->vnum);
        } else {
            if (strchr(arg2,'/')) {
                send_to_char("Invalid filename, should not contain a /\n\r",ch);
                return;
            }
            sprintf(filename,"logging.%s",arg2);
        }


        if ((desc=open(filename,O_RDWR|O_CREAT|O_TRUNC,0644))==-1) {
            sprintf_to_char(ch,"Couldn't open logging to %s, reason %s\n\r",filename,ERROR);
            return;
        }
        // create a logging descriptor
        logging_descriptor=new_descriptor(DESTYPE_MUD);
        logging_descriptor->descriptor=desc;
        lseek(logging_descriptor->descriptor,0,SEEK_END);
        logging_descriptor->Host[0]=0;
        logging_descriptor->Host[1]=0;
        logging_descriptor->Host[2]=0;
        logging_descriptor->Host[3]=0;
        logging_descriptor->connected=CON_LOGGING;
        logf( "[%d] Created logging character, logging to %s", logging_descriptor->descriptor,filename);
        logging_descriptor->next    = descriptor_list;
        descriptor_list             = logging_descriptor;

        logging_descriptor->character=victim;
        victim->desc=logging_descriptor;
        send_to_char("Logging enabled.\n\r",ch);
    } else if (victim->desc->connected==CON_LOGGING) {
        logf( "[%d] Removing logging character", victim->desc->descriptor);
        close(victim->desc->descriptor);
        if (victim->desc==descriptor_list) {
            descriptor_list=descriptor_list->next;
        } else {
            DESCRIPTOR_DATA *d;
            for ( d=descriptor_list; d && d->next!=victim->desc; d=d->next ) ;
            if ( d != NULL )
                d->next = victim->desc->next;
        }
        free_descriptor(victim->desc);
        victim->desc=NULL;
        send_to_char("Logging removed.\n\r",ch);
    } else {
        send_to_char("Cannot put a log on this one.\n\r",ch);
    }
}
