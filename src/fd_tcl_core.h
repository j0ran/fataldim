//
// $Id: fd_tcl_core.h,v 1.45 2008/05/01 18:47:06 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef INCLUDED_FD_TCLPROG_H
#define INCLUDED_FD_TCLPROG_H

extern char current_namespace[];

//
// Functions in fd_tcl_core.c which are needed everywhere
//
void 	tcl_init			(void);

char *	namespace_darea			(AREA_DATA *area);
char *	namespace_area			(AREA_DATA *area);
char *	namespace_dmob			(MOB_INDEX_DATA *dmob);
char *	namespace_char			(CHAR_DATA *ch);
char *	namespace_dobj			(OBJ_INDEX_DATA *dobj);
char *	namespace_obj			(OBJ_DATA *obj);
char *	namespace_room			(ROOM_DATA *obj);

void	tcl_extracted_obj		(OBJ_DATA *obj);
void	tcl_extracted_char		(CHAR_DATA *ch);
void	tcl_extracted_room		(ROOM_DATA *room);
void	tcl_extracted_area		(AREA_DATA *area);

void 	tcl_create_char_instance	(CHAR_DATA *ch);
void 	tcl_delete_char_instance	(CHAR_DATA *ch);

void 	tcl_create_obj_instance		(OBJ_DATA *obj);
void 	tcl_delete_obj_instance		(OBJ_DATA *obj);

void 	tcl_create_room_instance	(ROOM_DATA *room);
void 	tcl_delete_room_instance	(ROOM_DATA *room);

void 	tcl_create_area_instance	(AREA_DATA *area);
void 	tcl_delete_area_instance	(AREA_DATA *area);

bool	tcl_start			(char *name,char *trig,CHAR_DATA *mob,
					 CHAR_DATA *ch,OBJ_DATA *obj,
					 CHAR_DATA *ch2,OBJ_DATA *obj2,
					 ROOM_INDEX_DATA *room,AREA_DATA *area);
bool	tcl_stop			(void);

bool		tcl_parse_act		(char *format,int *arg1,int *arg2);
OBJ_DATA *	tcl_check_obj		(const OBJ_DATA *cobj);
CHAR_DATA *	tcl_check_ch		(const CHAR_DATA *cch);
ROOM_DATA *	tcl_check_room		(const ROOM_DATA *croom);
AREA_DATA *	tcl_check_area		(const AREA_DATA *carea);
int		tcl_run_mp_code		(CHAR_DATA *ch,char *code);
int		tcl_run_op_code		(OBJ_DATA *obj,char *code);
int		tcl_run_rp_code		(ROOM_DATA *room,char *code);
int		tcl_run_ap_code		(AREA_DATA *area,char *code);


char * 		tcl_AddSlashes		(const char *src);


//
// Define some tcl version dependant stuff
//
#if TCL_MAJOR_VERSION==8 && TCL_MINOR_VERSION<=3
  typedef char tclOptionList;
#else
  typedef const char tclOptionList;
#endif

//
// Structures for the TCL programs
//
struct fs_entry {
    bool	valid;
    FS_ENTRY *	next;
    char *	filename;
    char *	data;		// pointer to data, somewhere in memory
    int		descriptor;	// don't change it, don't free it...
    long	offset;
    long	size;
};

struct tclprog_list
{
    bool		valid;
    char *		name;
    int			trig_type;
    char *		trig_phrase;
    char *		code;
    TCLPROG_LIST * 	next;
};

struct tclprog_code
{
    bool		valid;
    int			vnum;
    AREA_DATA *		area;
    char *		code;
    int			edit;	/* bool, Is the code being edited? */
    char *		title;
    TCLPROG_CODE *	next;
    bool		deleted;
    int			version;
};

//
// Mob-program-triggers
// right now there is room for 64 triggers (8 bytes),
// as defined in MPROG_MAX_TRIGGERS
//
#define MPROG_MAX_TRIGGERS	64/8

#define MTRIG_NONE			(0)
#define MTRIG_ACT			(1)
#define MTRIG_BRIBE			(2)
#define MTRIG_DEATH			(3)
#define MTRIG_ENTRY			(4)
#define MTRIG_FIGHT			(5)
#define MTRIG_GIVE			(6)
#define MTRIG_GREET			(7)
#define MTRIG_GRALL			(8)
#define MTRIG_KILL			(9)
#define MTRIG_HITPOINTCOUNT		(10)
#define MTRIG_RANDOM			(11)
#define MTRIG_SPEECH			(12)
#define MTRIG_EXIT			(13)
#define MTRIG_EXALL			(14)
#define MTRIG_DELAY			(15)
#define MTRIG_SURRENDER			(16)
#define MTRIG_LEAVE			(17)
#define MTRIG_LEALL			(18)
#define MTRIG_TIMER			(19)
#define MTRIG_HOUR			(20)
#define MTRIG_FLEE			(21)
#define MTRIG_SPECIAL			(22)
#define MTRIG_INTERPRET_UNKNOWN		(23)
#define MTRIG_PREBUY			(24)
#define MTRIG_BUY			(25)
#define MTRIG_POSTBUY			(26)
#define MTRIG_INTERPRET_PREKNOWN	(27)
#define MTRIG_INTERPRET_POSTKNOWN	(28)
#define MTRIG_PRELOOKAT			(29)
#define MTRIG_LOOKAT			(30)
#define MTRIG_KILLED			(31)
#define MTRIG_PRERECALL			(32)
#define MTRIG_RECALL			(33)
#define MTRIG_RECALL_TO			(34)
#define MTRIG_WALKED			(35)
#define MTRIG_DIDNOTFIND		(36)
#define MTRIG_SUNRISE			(37)
#define MTRIG_SUNSET			(38)
#define MTRIG_DAYSTART			(39)
#define MTRIG_DAYEND			(40)
#define MTRIG_SOCIAL			(41)
#define MTRIG_PRESOCIAL			(42)
#define MTRIG_LOAD			(43)
#define MTRIG_PRESPEC			(44)
#define MTRIG_PREATTACK			(45)
#define MTRIG_SPEC			(46)
#define MTRIG_WEATHER			(47)

//
// Room-Program-triggers
// right now there is room for 128 triggers (16 bytes)
// as defined in RPROG_MAX_TRIGGERS
//
#define RPROG_MAX_TRIGGERS	128/8

#define RTRIG_NONE			(0)
#define RTRIG_PRELOOK			(1)
#define RTRIG_LOOK			(2)
#define RTRIG_PRESPEECH			(3)
#define RTRIG_SPEECH			(4)
#define	RTRIG_PRELOOK_ED		(5)
#define	RTRIG_LOOK_ED			(6)
#define RTRIG_PRERECALL			(7)
#define RTRIG_RECALL			(8)
#define RTRIG_RECALL_TO			(9)
#define RTRIG_PRERESET			(10)
#define RTRIG_RESET			(11)

#define RTRIG_ENTER			(12)
#define RTRIG_LEAVE			(13)

#define RTRIG_SUNRISE			(14)
#define RTRIG_SUNSET			(15)
#define RTRIG_DAYSTART			(16)
#define RTRIG_DAYEND			(17)
#define RTRIG_WEATHER			(18)

#define RTRIG_CANLOCK			(19)
#define RTRIG_PRELOCK			(20)
#define RTRIG_LOCK			(21)
#define RTRIG_CANUNLOCK			(22)
#define RTRIG_PREUNLOCK			(23)
#define RTRIG_UNLOCK			(24)
#define RTRIG_PRETRAP			(25)
#define RTRIG_TRAP			(26)

#define RTRIG_PREATTACK			(27)
#define RTRIG_KILL			(28)
#define RTRIG_DEATH			(29)

#define RTRIG_TIMER			(100)
#define RTRIG_HOUR			(101)
#define RTRIG_RANDOM			(102)
#define RTRIG_DELAY			(103)
#define RTRIG_INTERPRET_PREKNOWN	(104)
#define RTRIG_INTERPRET_POSTKNOWN	(105)
#define RTRIG_INTERPRET_UNKNOWN		(106)
#define RTRIG_SOCIAL			(107)
#define RTRIG_PRESOCIAL			(108)

//
// Area-Program-triggers
// right now there is room for 128 triggers (16 bytes)
// as defined in APROG_MAX_TRIGGERS
//
#define APROG_MAX_TRIGGERS	128/8

#define ATRIG_NONE			(0)
#define ATRIG_PRERESET			(1)
#define ATRIG_RESET			(2)

#define ATRIG_SUNRISE			(3)
#define ATRIG_SUNSET			(4)
#define ATRIG_DAYSTART			(5)
#define ATRIG_DAYEND			(6)
#define ATRIG_WEATHER			(7)

#define ATRIG_ENTER			(8)
#define ATRIG_LEAVE			(9)

#define ATRIG_TIMER			(100)
#define ATRIG_HOUR			(101)
#define ATRIG_RANDOM			(102)
#define ATRIG_DELAY			(103)

//
// Object-Program-trigger
// right now there is room for 128 triggers (16 bytes),
// as defined in OPROG_MAX_TRIGGERS
//
#define OPROG_MAX_TRIGGERS	128/8

#define OTRIG_NONE			(0)
#define OTRIG_PREDROP			(1)
#define OTRIG_DROP			(2)
#define OTRIG_PREOPEN			(3)
#define OTRIG_OPEN			(4)
#define OTRIG_PRECLOSE			(5)
#define OTRIG_CLOSE			(6)
#define OTRIG_PREEAT			(7)
#define OTRIG_EAT			(8)
#define OTRIG_PREDRINK			(9)
#define OTRIG_DRINK			(10)
#define OTRIG_PRELOOKAT			(11)
#define OTRIG_LOOKAT			(12)
#define OTRIG_PRELOOKIN			(13)
#define OTRIG_LOOKIN			(14)
#define OTRIG_PREZAP			(15)
#define OTRIG_ZAP			(16)
#define OTRIG_PREWEAR			(17)
#define OTRIG_WEAR			(18)
#define OTRIG_PREREMOVE			(19)
#define OTRIG_REMOVE			(20)
#define OTRIG_PRELOCK			(21)
#define OTRIG_LOCK			(22)
#define OTRIG_PREUNLOCK			(23)
#define OTRIG_UNLOCK			(24)
#define OTRIG_PREGET			(25)
#define OTRIG_GET			(26)
#define OTRIG_PREPUT			(27)
#define OTRIG_PUT			(29)
#define OTRIG_PREGIVE			(30)
#define OTRIG_GIVE			(31)
#define OTRIG_PRESELL			(32)
#define OTRIG_SELL			(33)
#define OTRIG_PREPUTIN			(34)
#define OTRIG_PUTIN			(35)
#define OTRIG_PREGETOUT			(36)
#define OTRIG_GETOUT			(37)
#define OTRIG_LOAD			(38)
#define OTRIG_PREQUAFF			(39)
#define OTRIG_QUAFF			(40)
#define OTRIG_PRERECITE			(41)
#define OTRIG_RECITE			(42)
#define OTRIG_BUY			(43)
#define OTRIG_CANLOCK			(44)
#define OTRIG_CANUNLOCK			(45)
#define OTRIG_PRETRAP			(46)
#define OTRIG_TRAP			(47)
#define OTRIG_PRELOOKAT_ED		(48)
#define OTRIG_LOOKAT_ED			(49)

#define OTRIG_PREENTER			(50)
#define OTRIG_ENTER			(51)
#define OTRIG_PREHIT			(52)
#define OTRIG_HIT			(53)
#define OTRIG_PREKEY			(54)
#define OTRIG_KEY			(55)
#define OTRIG_PREBRANDISH		(54)
#define OTRIG_BRANDISH			(55)
#define OTRIG_PRESIT			(56)
#define OTRIG_SIT			(57)
#define OTRIG_PRESTAND			(58)
#define OTRIG_STAND			(59)
#define OTRIG_PREREST			(60)
#define OTRIG_REST			(61)
#define OTRIG_PRESLEEP			(62)
#define OTRIG_SLEEP			(63)
#define OTRIG_PRESPEECH			(64)
#define OTRIG_SPEECH			(65)
#define OTRIG_PREATTACK			(66)
#define OTRIG_READ			(67)
#define OTRIG_PREREAD			(68)
#define OTRIG_USE			(69)
#define OTRIG_PREUSE			(70)
#define OTRIG_NOHIT			(71)
#define OTRIG_PREPLAY			(72)
#define OTRIG_PLAY			(73)
#define OTRIG_PLAYFILTER		(74)

#define OTRIG_SUNRISE			(80)
#define OTRIG_SUNSET			(81)
#define OTRIG_DAYSTART			(82)
#define OTRIG_DAYEND			(83)
#define OTRIG_WEATHER			(84)

#define OTRIG_TIMER			(100)
#define OTRIG_HOUR			(101)
#define OTRIG_RANDOM			(102)
#define OTRIG_DELAY			(103)
#define OTRIG_INTERPRET_PREKNOWN	(104)
#define OTRIG_INTERPRET_POSTKNOWN	(105)
#define OTRIG_INTERPRET_UNKNOWN		(106)

extern const TABLE_TYPE	roomtrigger_table[];
extern const TABLE_TYPE	areatrigger_table[];
extern const TABLE_TYPE	objtrigger_table[];
extern const TABLE_TYPE	mobtrigger_table[];

//
// progData is the global variable of the TCL programs, it contains all the
// references needed by the trigger.
//
#define PROGTYPE_NONE 0
#define PROGTYPE_CHAR 1
#define PROGTYPE_OBJECT 2
#define PROGTYPE_ROOM 3
#define PROGTYPE_AREA 4
#define PROGDATA_SLOTS 5

struct _progData {
    CHAR_DATA *mob;
    CHAR_DATA *ch;
    OBJ_DATA *obj;
    CHAR_DATA *ch2;
    OBJ_DATA *obj2;
    AREA_DATA *area;
    CHAR_DATA *lastCh;
    OBJ_DATA *lastObj;
    ROOM_INDEX_DATA *room;
    ROOM_INDEX_DATA *lastRoom;
    CHAR_DATA *lastRoomCh;
    AREA_DATA *lastArea;
    char *text1;
    char *text2;
    char *trigValue;
    char *trigName;
    CHAR_DATA *char_store[PROGDATA_SLOTS];
    OBJ_DATA *obj_store[PROGDATA_SLOTS];
};

#ifdef NEEDS_TCL
//
// Functions in fd_tcl_core which are needed in the functions of fd_tcl_xxx.c
//
int	tclAreaObjCmd		(ClientData,Tcl_Interp *,int,Tcl_Obj *CONST []);
int	tclCharObjCmd		(ClientData,Tcl_Interp *,int,Tcl_Obj *CONST []);
int	tclClanObjCmd		(ClientData,Tcl_Interp *,int,Tcl_Obj *CONST []);
int	tclDataObjCmd		(ClientData,Tcl_Interp *,int,Tcl_Obj *CONST []);
int	tclDirectionNameObjCmd	(ClientData,Tcl_Interp *,int,Tcl_Obj *CONST []);
int	tclInterpretObjCmd	(ClientData,Tcl_Interp *,int,Tcl_Obj *CONST []);
int	tclMobObjCmd		(ClientData,Tcl_Interp *,int,Tcl_Obj *CONST []);
int	tclMudObjCmd		(ClientData,Tcl_Interp *,int,Tcl_Obj *CONST []);
int	tclObjObjCmd		(ClientData,Tcl_Interp *,int,Tcl_Obj *CONST []);
int	tclProgTypeObjCmd	(ClientData,Tcl_Interp *,int,Tcl_Obj *CONST []);
int	tclRandomObjCmd		(ClientData,Tcl_Interp *,int,Tcl_Obj *CONST []);
int	tclFormatObjCmd		(ClientData,Tcl_Interp *,int,Tcl_Obj *CONST []);
int	tclRoomObjCmd		(ClientData,Tcl_Interp *,int,Tcl_Obj *CONST []);
int	tclSourceObjCmd		(ClientData,Tcl_Interp *,int,Tcl_Obj *CONST []);
int	tclTrigObjCmd		(ClientData,Tcl_Interp *,int,Tcl_Obj *CONST []);
int	tclDebugObjCmd		(ClientData,Tcl_Interp *,int,Tcl_Obj *CONST []);
int	tclStringChangeCost	(ClientData,Tcl_Interp *,int,Tcl_Obj *CONST []);

int	tcl_remove		(CHAR_DATA *ch,int objc,Tcl_Obj *CONST objv[]);
int	tcl_act			(CHAR_DATA *ch,int objc,Tcl_Obj *CONST objv[]);
int	mp_addtrigger		(CHAR_DATA *ch,int objc,Tcl_Obj *CONST objv[]);
int	op_addtrigger		(OBJ_DATA *obj,int objc,Tcl_Obj *CONST objv[]);
int	rp_addtrigger		(ROOM_DATA *room,int objc,Tcl_Obj *CONST objv[]);
int	ap_addtrigger		(AREA_DATA *area,int objc,Tcl_Obj *CONST objv[]);

AREA_DATA *tcl_parse_area_switches	(AREA_DATA *,int *,Tcl_Obj *CONST **);
CHAR_DATA *tcl_parse_char_switches	(CHAR_DATA *,int *,Tcl_Obj *CONST **);
OBJ_DATA *tcl_parse_obj_switches	(OBJ_DATA *,int *,Tcl_Obj *CONST **);
ROOM_INDEX_DATA *tcl_parse_room_switches(ROOM_INDEX_DATA *,CHAR_DATA **,
					 int *,Tcl_Obj *CONST **);


char 		*tcl_firstname		(CHAR_DATA *ch);
char 		*tcl_firstobjname	(OBJ_DATA *obj);
bool		tcl_findword		(char *word,char *str);

ROOM_INDEX_DATA *tcl_location		(char *id);
int		tcl_count_people_room	(ROOM_INDEX_DATA *room,
					 CHAR_DATA *mob,int iFlag,int vnum);
int		tcl_count_people_area	(AREA_DATA *area,
					 CHAR_DATA *mob,int iFlag,int vnum);
CHAR_DATA 	*tcl_get_random_char	(ROOM_INDEX_DATA *room,CHAR_DATA *mob,int type);

OBJ_DATA *	tcl_objid		(OBJ_DATA *obj_list,char *id,
					 bool isworldlist);
OBJ_DATA *	tcl_objid_here		(CHAR_DATA *ch,char *arg);
bool		tcl_isobjid		(OBJ_DATA *obj,char *id);

CHAR_DATA *	tcl_charid		(ROOM_DATA *room,char *id,
					 CHAR_DATA *start_pos);
CHAR_DATA *	tcl_charid_char		(CHAR_DATA *ch,char *id,
					 CHAR_DATA *start_pos);
CHAR_DATA *	tcl_charid_obj		(OBJ_DATA *obj,char *id,
					 CHAR_DATA *start_pos);
bool		tcl_ischarid		(CHAR_DATA *ch,char *id);

void		mp_delonetrigger	(CHAR_DATA *ch,TCLPROG_LIST *trig);
void		ap_delonetrigger	(AREA_DATA *area,TCLPROG_LIST *trig);
void		op_delonetrigger	(OBJ_DATA *obj,TCLPROG_LIST *trig);
void		rp_delonetrigger	(ROOM_DATA *room,TCLPROG_LIST *trig);


extern struct _progData *progData;
extern int progType;

Tcl_Obj *	Effect_to_ListObj	(EFFECT_DATA *aff);
#endif

#endif	// INCLUDED_FD_TCLPROG_H
