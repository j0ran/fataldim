/* $Id: act_obj.c,v 1.196 2006/08/25 09:43:19 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

/*
 * 19971214 EG Modified donate() so that objects in corpses of mobiles
           get the value of 0. BWAHAHAHA ;-)
 * 19971214 EG Modified do_sell() to support the statement "sell all"
           and "sell all.object"
 * 19971220 EG	Modified do_deposit() to allow the player to keep
        n silver left instead of depositing m silver.
 * 19980104 EG	Syntax for get : "get  [<mul>*][<num>.]<object> [source]"
        Syntax for drop: "drop [<mul>*][<num>.]<object>"
        Syntax for put : "put  [<mul>*][<num>.]<object> <dest>"
        Syntax for give: "give [<mul>*][<num>.]<object> <dest>"
        Syntax for sac : "sac  [<mul>*][<num>.]<object>"
        Syntax for tap : "tap  [<mul>*][<num>.]<object>"
        Syntax for buy : "buy  [<mul>*][<num>.]<object>"
        Syntax for sell: "sell [<mul>*][<num>.]<object>"
 * 19980128 EG  Modified do_deposit() and do_withdrawn() to support
        clan-accounts
 * 19980316 EG  Mofified do_put(), do_get() and do_pour() to refuse
        to work if the level of the container is higher
        than the level of the player.
 * 19980324 EG	Modified do_second() to check for a wieldable weapon
        instead of a sleepingback to have as second weapon.
 */

#include "merc.h"
#include "interp.h"
#include "color.h"
#include "fd_mxp.h"

/* RT part of the corpse looting code */

bool can_loot(CHAR_DATA *ch, OBJ_DATA *obj)
{
    CHAR_DATA *owner, *wch;

    if (IS_IMMORTAL(ch))
        return TRUE;

    if (!obj_is_owned_by_another(ch,obj))
        return TRUE;

    owner = NULL;
    for ( wch = char_list; wch != NULL ; wch = wch->next )
        if (!str_cmp(wch->name,obj->owner_name))
            owner = wch;

    if (owner == NULL)
        return TRUE;

    if (!IS_NPC(owner) && STR_IS_SET(owner->strbit_act,PLR_CANLOOT))
        return TRUE;

    if (is_same_group(ch,owner))
        return TRUE;

    return FALSE;
}

bool obj_is_owned(OBJ_DATA *obj) {
    char owner[MSL];

    if (GetObjectProperty(obj,PROPERTY_STRING,"owner",owner)) {
        free_string(obj->owner_name);
        obj->owner_name=str_dup(owner);
        obj->owner_id=-1;
        DeleteObjectProperty(obj,PROPERTY_STRING,"owner");
    }

    if (!obj->owner_name || obj->owner_name[0] == 0)
        return FALSE;

    if (obj->owner_id==-1) {
        CHAR_DATA *wch;
        for ( wch = char_list; wch != NULL ; wch = wch->next )
            if (IS_PC(wch) && !str_cmp(wch->name,obj->owner_name))
                obj->owner_id=wch->id;
    }

    return TRUE;
}

bool obj_is_owned_by_another(CHAR_DATA *ch, OBJ_DATA *obj) {

    return obj_is_owned(obj) && (str_cmp(ch->name,obj->owner_name) || ch->id!=obj->owner_id);
}

void get_obj( CHAR_DATA *ch, OBJ_DATA *obj, OBJ_DATA *container )
{
    /* variables for AUTOSPLIT */
    CHAR_DATA *gch;
    int members;
    char buf[MSL];
    int levelmin,levelmax;
    bool carryingtoomuch;

    if ( !CAN_WEAR(obj, ITEM_TAKE) )
    {
        send_to_char( "You can't take that.\n\r", ch );
        return;
    }

    if (!IS_IMMORTAL(ch)) {
        if (obj_is_owned_by_another(ch,obj)) {
            act("$p is owned by someone else.",ch,obj,NULL,TO_CHAR);
            return;
        }
        if (container && obj_is_owned_by_another(ch,container)) {
            act("$p is owned by someone else.",ch,container,NULL,TO_CHAR);
            return;
        }
    }

    if ( ch->carry_number + get_obj_number( obj ) > can_carry_n( ch ) ) {
        act( "$d: you can't carry that many items.",
             ch, NULL, obj->name, TO_CHAR );
        return;
    }

    carryingtoomuch=FALSE;
    if (get_carry_weight(ch) >= can_carry_w(ch)) carryingtoomuch=TRUE;

    if ((!obj->in_obj || obj->in_obj->carried_by != ch)
            &&  (get_carry_weight(ch) + get_obj_weight(obj) > can_carry_w(ch))) {
        if (obj->item_type==ITEM_MONEY) {
            int carry_left=can_carry_w(ch)-get_carry_weight(ch);
            int gold_coins=0,silver_coins=0;
            bool is_getting_all=TRUE;

            if (carryingtoomuch) {
                sprintf_to_char(ch,"%s: You are already carrying too much.\n\r",
                                obj->short_descr);
                return;
            }

            if (obj->value[1]) {
                if (obj->value[1]*2/5 > carry_left) {
                    // more goldpieces than able to carry
                    gold_coins=carry_left*5/2;
                    is_getting_all=FALSE;
                } else {
                    gold_coins=obj->value[1];
                }
                carry_left-=gold_coins*2/5;
            }
            if (obj->value[0]) {
                if (obj->value[0]/10 > carry_left) {
                    // more silverpieces than able to carry
                    silver_coins=carry_left*10;
                    is_getting_all=FALSE;
                } else {
                    silver_coins=obj->value[0];
                }
            }

            if (is_getting_all) {
                if (container) {
                    sprintf_to_char(ch,"You get %s from %s.\n\r",
                                    money_string(gold_coins,silver_coins),container->short_descr);
                    act("$n gets the coins from $P.",
                        ch,NULL,container,TO_ROOM);
                } else {
                    sprintf_to_char(ch,"You get %s.\n\r",
                                    money_string(gold_coins,silver_coins));
                    act("$n gets the coins.",ch,NULL,NULL,TO_ROOM);
                }
            } else if (gold_coins || silver_coins) {
                if (container) {
                    sprintf_to_char(ch,"You get only %s from %s.\n\r",
                                    money_string(gold_coins,silver_coins),container->short_descr);
                    act("$n gets some of the coins from $P.",
                        ch,NULL,container,TO_ROOM);
                } else {
                    sprintf_to_char(ch,"You get only %s.\n\r",
                                    money_string(gold_coins,silver_coins));
                    act("$n gets some of the coins.",ch,NULL,NULL,TO_ROOM);
                }
            } else {
                sprintf_to_char(ch,"You can't get any coins.\n\r");
            }
            obj->value[1]-=gold_coins; ch->gold+=gold_coins;
            obj->value[0]-=silver_coins; ch->silver+=silver_coins;

            // one could recalculate everything here, but it's much easier to create a new pile.
            if (obj->value[0]!=0 || obj->value[1]!=0) {
                OBJ_DATA *nobj;

                nobj=create_money(obj->value[1],obj->value[0]);
                if (obj->in_room) obj_to_room(nobj,obj->in_room);
                else if (obj->in_obj) obj_to_obj(nobj,obj->in_obj);
                else if (obj->carried_by) obj_to_char(nobj,obj->carried_by);
            }

            extract_obj(obj);

        } else {
            act( "$d: you can't carry that much weight.",
                 ch, NULL, obj->name, TO_CHAR );
        }
        return;
    }


    if (!can_loot(ch,obj)) {
        act("Corpse looting is not permitted.",ch,NULL,NULL,TO_CHAR );
        return;
    }

    if (obj->in_room != NULL) {
        for (gch = obj->in_room->people; gch != NULL; gch = gch->next_in_room)
            if (gch->on == obj) {
                if (ch==gch)
                    act("You appear to be using $p.",ch,obj,gch,TO_CHAR);
                else
                    act("$N appears to be using $p.",ch,obj,gch,TO_CHAR);
                return;
            }
    }

    if((obj->item_type==ITEM_CLAN) && !IS_IMMORTAL(ch)) {
        CLAN_INFO *clan;

        // mobs can't take them
        if (IS_NPC(ch)) return;

        if (ch->clan==NULL) {
            send_to_char("That's a clan-item, you're not in a clan, please leave it alone.\n\r",ch);
            return;
        }

        /* Check on clan object */
        if (GetObjectProperty(obj,PROPERTY_STRING,"clanitem",buf)==FALSE) {
            bugf("get_obj: clanitem without clanname (vnum: %d)",obj->pIndexData->vnum);
            sprintf_to_char(ch,"This item is broken, please report a bug with number %d.\n\r",obj->pIndexData->vnum);
            return;
        }

        if ((clan=get_clan_by_name(buf))!=NULL && clan!=ch->clan->clan_info) {
            OBJ_DATA *pObj;

            /* It is a clan object of another clan. */
            for(pObj=ch->carrying;pObj;pObj=pObj->next_content)
                if(pObj->wear_loc!=WEAR_NONE) {
                    send_to_char("You can only take objects of other clans without wearing any equipment.\n\r", ch );
                    return;
                }
        }
    }

    if (GetObjectProperty(obj,PROPERTY_STRING,"clan",buf) && !IS_IMMORTAL(ch))
        if (ch->clan==NULL || str_cmp(buf,ch->clan->clan_info->name)) {
            sprintf_to_char(ch,
                            "You see a mark on %s: this item belongs to the %s clan.\n\r",
                            obj->short_descr,buf);
            return;
        }

    if ( (container != NULL) && (container->item_type!=ITEM_PORTAL)) {
        if (container->pIndexData->vnum == OBJ_VNUM_PIT
                &&  get_trust(ch) < obj->level) {
            send_to_char("You are not powerful enough to use it.\n\r",ch);
            return;
        }

        if ( OBJ_HAS_TRIGGER( container, OTRIG_PREGETOUT ) )
            if (op_pregetout_trigger( container, ch, obj )==FALSE)
                return;
        if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREGET ) )
            if (op_preget_trigger( obj, ch, container )==FALSE)
                return;
        if (!IS_VALID(obj))
            return;

        if (container->pIndexData->vnum == OBJ_VNUM_PIT
                &&  !CAN_WEAR(container, ITEM_TAKE)
                &&  !IS_OBJ_STAT(obj,ITEM_HAD_TIMER))
            obj->timer = 0;
        if (STR_IS_SET(obj->strbit_extra_flags,ITEM_ROT_DEATH))
            obj->timer=0;
        STR_REMOVE_BIT(obj->strbit_extra_flags,ITEM_HAD_TIMER);
        obj_from_obj( obj );
        if (container->carried_by==ch) {
            if (carryingtoomuch) {
                act("You get $p from $P but lose your grip on it.",
                    ch,obj,container,TO_CHAR);
                act("$n gets $p from $P but loses $s grip on it.",
                    ch,obj,container,TO_ROOM);
                obj_to_room(obj,ch->in_room);
                return;
            }
        } else {
            if (carryingtoomuch) {
                act("$d: You are already carrying too much.",
                    ch,NULL,obj->name,TO_CHAR);
                return;
            }
        }
        act( "You get $p from $P.", ch, obj, container, TO_CHAR );
        act( "$n gets $p from $P.", ch, obj, container, TO_ROOM );

        if (IS_PC(ch)) {
            STR_SET_BIT(ch->pcdata->usedthatobject,container->pIndexData->vnum);
            STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);
        }

        if ( OBJ_HAS_TRIGGER( container, OTRIG_GETOUT ) )
            op_getout_trigger( container, ch, obj );
        if ( OBJ_HAS_TRIGGER( obj, OTRIG_GET ) )
            op_get_trigger( obj, ch, container );
        if (!IS_VALID(obj))
            return;

    } else {
        if (carryingtoomuch) {
            act("$d: You are already carrying too much.",
                ch,NULL,obj->name,TO_CHAR);
            return;
        }

        if ( container && OBJ_HAS_TRIGGER( container, OTRIG_PREGETOUT ) )
            if (op_pregetout_trigger( container, ch, obj )==FALSE)
                return;
        if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREGET ) )
            if (op_preget_trigger( obj, ch,NULL )==FALSE)
                return;
        if (!IS_VALID(obj))
            return;

        if (container) {
            // not a real container but a portal
            act( "You get $p from $P.", ch, obj, container, TO_CHAR);
            act( "$n's hand appears and gets $p.", ch, obj, NULL, TO_ROOM);
        } else {
            act( "You get $p.", ch, obj, container, TO_CHAR );
            act( "$n gets $p.", ch, obj, container, TO_ROOM );
        }
        obj_from_room( obj );

        if ( container && OBJ_HAS_TRIGGER( container, OTRIG_GETOUT ) )
            op_getout_trigger( container, ch, obj );
        if ( OBJ_HAS_TRIGGER( obj, OTRIG_GET ) )
            op_get_trigger( obj, ch, NULL);
        if (!IS_VALID(obj))
            return;
    }

    if ( obj->item_type == ITEM_MONEY)
    {
        ch->silver += obj->value[0];
        ch->gold += obj->value[1];
        if (STR_IS_SET(ch->strbit_act,PLR_AUTOSPLIT))
        { /* AUTOSPLIT code */
            members = 0;
            for (gch = ch->in_room->people; gch != NULL; gch = gch->next_in_room )
            {
                if (!IS_AFFECTED(gch,EFF_CHARM) && is_same_group( gch, ch ) )
                    members++;
            }

            if ( members > 1 && (obj->value[0] > 1 || obj->value[1]))
            {
                sprintf(buf,"auto %d %d",obj->value[0],obj->value[1]);
                do_function(ch, &do_split, buf);
            }
        }

        extract_obj( obj );
    }
    else
    {
        obj_to_char( obj, ch );

        levelmin=levelmax=-1;
        GetObjectProperty(obj,PROPERTY_INT,"levelhavemin",&levelmin);
        GetObjectProperty(obj,PROPERTY_INT,"levelhavemax",&levelmax);
        if ((levelmin>=0 && ch->level<levelmin) ||
                (levelmax>=0 && ch->level>levelmax)) {
            zap_char_obj(obj,TRUE);
            return;
        }
    }

    return;
}


void do_get( CHAR_DATA *ch, char *argument )
{
    char arg1[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    char argn[MAX_INPUT_LENGTH];
    OBJ_DATA *obj;
    OBJ_DATA *obj_next;
    OBJ_DATA *container;
    int number,i;
    bool found;

    argument = one_argument( argument, argn );
    argument = one_argument( argument, arg2 );

    number = mult_argument(argn,arg1);

    if (!str_cmp(arg2,"from"))
        argument = one_argument(argument,arg2);

    /* Get type. */
    if ( arg1[0] == '\0' ) {
        send_to_char( "Get what?\n\r", ch );
        return;
    }

    if ( arg2[0] == '\0' )
    {
        if ( str_cmp( arg1, "all" ) && str_prefix( "all.", arg1 ) )
        {
            /* 'get obj' */
            for (i=0;i<number;i++) {
                if ((obj=get_obj_list(ch,arg1,ch->in_room->contents))==NULL) {
                    if (i==0)
                        sprintf_to_char(ch,"I see no %s here.\n\r",arg1);
                    else
                        sprintf_to_char(ch,"There %s only %d %s%s here.\n\r",
                                        i==1?"was":"were",i,arg1,i==1?"":"s");
                    return;
                }
                get_obj( ch, obj, NULL );
            }

            return;
        }

        /* 'get all' or 'get all.obj' */
        if (number!=1) {
            send_to_char("You greedy bastard.\n\r",ch);
            return;
        }
        found = FALSE;
        for ( obj = ch->in_room->contents; obj != NULL; obj = obj_next )
        {
            obj_next = obj->next_content;
            if ( ( arg1[3] == '\0' || is_name( &arg1[4], obj->name ) )
                 &&   can_see_obj( ch, obj ) )
            {
                found = TRUE;
                get_obj( ch, obj, NULL );
                if (STR_IS_SET(ch->in_room->strbit_room_flags,ROOM_NOGETALL))
                    break;
            }
        }

        if ( !found )
        {
            if ( arg1[3] == '\0' || arg1[4]==0 )
                send_to_char( "I see nothing here.\n\r", ch );
            else
                act( "I see no $T here.", ch, NULL, &arg1[4], TO_CHAR );
        }
        return;
    }

    /* 'get ... container' */
    if ( !str_cmp( arg2, "all" ) || !str_prefix( "all.", arg2 ) )
    {
        send_to_char( "You can't do that.\n\r", ch );
        return;
    }

    container = get_obj_here( ch, arg2 );
    if (  container != NULL ) {
        get_from_container(ch,container,number,arg1);
        return;
    }

    if (IS_IMMORTAL(ch)) {
        CHAR_DATA *victim;

        victim=get_char_room(ch,arg2);

        if (victim && (IS_NPC(victim) || get_trust(ch)>get_trust(victim))) {
            obj=get_obj_carry(victim,arg1,ch);

            if (obj && (STR_IS_SET(obj->strbit_extra_flags,ITEM_UNUSABLE) ||
                        IS_TRUSTED(ch,LEVEL_COUNCIL))) {
                act( "$n takes $p from you.", ch, obj, victim, TO_VICT    );
                act( "$n takes $p from $N.",  ch, obj, victim, TO_NOTVICT );
                act( "You take $p from $N.",  ch, obj, victim, TO_CHAR );
                obj_from_char(obj);
                obj_to_char(obj,ch);
            } else {
                sprintf_to_char(ch,"I see no %s on %s.\n\r",arg1,NAME(victim));
            }
            return;
        }
    }

    act( "I see no $T here.", ch, NULL, arg2, TO_CHAR );
}

void get_from_container(CHAR_DATA *ch, OBJ_DATA *container, int amount, char *arg) {
    ROOM_INDEX_DATA *local,*target;
    OBJ_DATA *obj;
    OBJ_DATA *obj_next;
    char clan[MAX_INPUT_LENGTH];
    int i;
    bool found;

    switch ( container->item_type )
    {
    default:
        send_to_char( "That's not a container.\n\r", ch );
        return;

    case ITEM_PORTAL:
        local = ch->in_room;
        target = get_portal_destination(ch,container);
        if (!target) {
            sprintf_to_char(ch,"%s seems to be broken.\n\r",container->short_descr);
            return;
        }
        if (!is_char_allowed_in_room(ch,target,FALSE)) {
            send_to_char("You can't do that.\n\r", ch );
            return;
        }
        break;
    case ITEM_CONTAINER:
    case ITEM_CORPSE_NPC:
        break;

    case ITEM_CORPSE_PC:
        if (!can_loot(ch,container)) {
            send_to_char( "You can't do that.\n\r", ch );
            return;
        }
        break;
    }

    if ( IS_SET(container->value[1], CONT_CLOSED) ) {
        act( "The $d is closed.", ch, NULL, container->name, TO_CHAR );
        return;
    }

    if (GetObjectProperty(container,PROPERTY_STRING,"clan",clan)) {
        if ((ch->clan!=NULL && str_cmp(ch->clan->clan_info->name,clan)) ||
                (ch->clan==NULL && !IS_IMMORTAL(ch))) {
            sprintf_to_char(ch,"There is label on %s 'This belongs to "
                               "the %s clan'\n\r",container->short_descr,clan);
            return;
        }
    }

    if (!IS_NPC(ch) && container->level>ch->level
            && container->item_type!=ITEM_CORPSE_NPC
            && container->item_type!=ITEM_CORPSE_PC) {
        if (container->item_type!=ITEM_PORTAL) {
            act("The $d refuses to let you reach in.",
                ch, NULL, container->name, TO_CHAR);
        } else {
            act( "The $d refuses to release the object to you.",
                 ch, NULL, container->name, TO_CHAR );
        }
        return;
    }

    if ( str_cmp( arg, "all" ) && str_prefix( "all.", arg ) )
    {
        if (container->item_type==ITEM_PORTAL) {
            bool orig_visited=IS_PC(ch) && STR_IS_SET(ch->pcdata->beeninroom,target->vnum);
            /* 'get obj portal' */
            act("$n reaches into $p.",ch,container,NULL,TO_ROOM);
            char_from_room(ch);
            char_to_room(ch,target);
            for (i=0;i<amount;i++) {
                if ((obj=get_obj_list(ch,arg,target->contents))==NULL) {
                    if (i==0)
                        sprintf_to_char(ch,"I see no %s here.\n\r",arg);
                    else
                        sprintf_to_char(ch,"There %s only %d %s%s here.\n\r",
                                        i==1?"was":"were",i,arg,i==1?"":"s");
                    char_from_room(ch);
                    char_to_room(ch,local);
                    return;
                }
                get_obj( ch, obj, container );
                if (IS_SET(container->value[2], GATE_NOGETALL))
                    break;
            }
            char_from_room(ch);
            char_to_room(ch,local);
            if (IS_PC(ch) && !orig_visited)
                STR_REMOVE_BIT(ch->pcdata->beeninroom,target->vnum);
        } else {
            /* 'get obj container' */
            for (i=0;i<amount;i++) {
                if ((obj = get_obj_list( ch, arg, container->contains ))==NULL) {
                    if (i==0)
                        sprintf_to_char(ch,"I see nothing like that in %s.\n\r",
                                        container->short_descr);
                    else
                        sprintf_to_char(ch,"There %s only %d %s%s in %s.\n\r",
                                        i==1?"was":"were",i,arg,i==1?"":"s",container->short_descr);
                    return;
                }
                get_obj( ch, obj, container );
            }
        }
        return;
    }

    if (container->item_type==ITEM_PORTAL) {
        bool orig_visited=IS_PC(ch) && STR_IS_SET(ch->pcdata->beeninroom,target->vnum);
        /* 'get all portal' or 'get all.obj portal' */
        act("$n reaches into $p.",ch,container,NULL,TO_ROOM);
        found = FALSE;
        char_from_room(ch);
        char_to_room(ch,target);
        for ( obj = target->contents; obj != NULL; obj = obj_next )
        {
            obj_next = obj->next_content;
            if ( ( arg[3] == '\0' || is_name( &arg[4], obj->name ) )
                 &&   can_see_obj( ch, obj ) )
            {
                found = TRUE;
                get_obj( ch, obj, container );
                if (STR_IS_SET(target->strbit_room_flags, ROOM_NOGETALL))
                    break;
            }
        }
        char_from_room(ch);
        char_to_room(ch,local);
        if (IS_PC(ch) && !orig_visited)
            STR_REMOVE_BIT(ch->pcdata->beeninroom,target->vnum);

    } else {
        /* 'get all container' or 'get all.obj container' */
        found = FALSE;
        for ( obj = container->contains; obj != NULL; obj = obj_next )
        {
            obj_next = obj->next_content;
            if ( ( arg[3] == '\0' || is_name( &arg[4], obj->name ) )
                 &&   can_see_obj( ch, obj ) )
            {
                found = TRUE;
                if (container->pIndexData->vnum == OBJ_VNUM_PIT
                        &&  !IS_IMMORTAL(ch))
                {
                    send_to_char("Don't be so greedy!\n\r",ch);
                    return;
                }
                get_obj( ch, obj, container );
                if (IS_SET(container->value[1], CONT_NOGETALL))
                    break;
            }
        }
    }

    if ( !found )
    {
        if ( arg[3] == '\0' )
            act( "I see nothing in the $P.",
                 ch, NULL, container, TO_CHAR );
        else
            act( "I see nothing like that in the $P.",
                 ch, NULL, container, TO_CHAR );
    }

    return;
}



void do_put( CHAR_DATA *ch, char *argument )
{
    char argn[MAX_INPUT_LENGTH];
    char arg1[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    OBJ_DATA *container;
    OBJ_DATA *obj,*moved_obj;
    OBJ_DATA *obj_next;
    ROOM_INDEX_DATA *room;
    int number,i;

    if (check_ordered(ch)) return;

    argument = one_argument( argument, argn );

    if (is_number(argn)) {
        // put N coins/silver/gold [in] container/portal
        int amount, gold=0, silver=0;

        amount = atoi(argn);
        argument = one_argument( argument, arg1);
        if ( (amount <= 0)
             || ( str_cmp( arg1, "coins" ) && str_cmp( arg1, "coin" ) &&
                  str_cmp( arg1, "gold"  ) && str_cmp( arg1, "silver") ) ) {
            send_to_char( "Sorry, you can't do that.\n\r", ch );
            return;
        }
        if ( str_cmp( arg1, "gold") ) {
            if (ch->silver < amount) {
                send_to_char("You don't have that much silver.\n\r",ch);
                return;
            }
            //            ch->silver -= amount;
            silver = amount;
        } else {
            if (ch->gold < amount) {
                send_to_char("You don't have that much gold.\n\r",ch);
                return;
            }
            //            ch->gold -= amount;
            gold = amount;
        }
        argument = one_argument( argument, arg2);
        if (!str_cmp(arg2,"in") || !str_cmp(arg2,"on"))
            argument = one_argument(argument,arg2);

        if ( ( container = get_obj_here( ch, arg2 ) ) == NULL ) {
            act( "I see no $T here.", ch, NULL, arg2, TO_CHAR );
            return;
        }
        if (container->item_type != ITEM_CONTAINER &&
                container->item_type !=ITEM_PORTAL) {
            send_to_char( "That's not a container.\n\r", ch );
            return;
        }
        if ( IS_SET(container->value[1], CONT_CLOSED) ) {
            act( "The $d is closed.", ch, NULL, container->name, TO_CHAR );
            return;
        }

        if (!IS_NPC(ch) && container->level>ch->level) {
            act( "The $d refuses to let you put that in it.",
                 ch, NULL, container->name, TO_CHAR );
            return;
        }

        moved_obj = create_money( gold, silver );

        if (container->item_type!=ITEM_PORTAL &&
                (get_obj_weight(moved_obj)+get_true_weight(container) > (container->value[0] * 10)
                 ||  get_obj_weight(moved_obj) > (container->value[3] * 10))) {
            sprintf_to_char(ch,"%s: It won't fit.\n\r", moved_obj->short_descr );
            extract_obj(moved_obj);
            return;
        }

        if ( OBJ_HAS_TRIGGER( container, OTRIG_PREPUTIN ) )
            if (op_preputin_trigger( container, ch, moved_obj )==FALSE) {
                extract_obj(moved_obj);
                return;
            }

        // check if ch->* is big enough is done already.
        ch->silver-=silver;
        ch->gold-=gold;

        // find old coinstacks
        if (container->item_type == ITEM_PORTAL) {
            room=get_portal_destination(ch,container);
            if (!room || !is_char_allowed_in_room(ch,room,FALSE)) {
                send_to_char("You can't do that.\n\r", ch );
                return;
            }
            obj = room->contents;
        } else {
            obj = container->contains;
        }
        for ( ; obj != NULL; obj = obj_next ) {
            obj_next = obj->next_content;

            if ((container->item_type == ITEM_CONTAINER) &&
                    (get_obj_weight(obj)+get_obj_weight(moved_obj) > (container->value[3] * 10)))
                continue;

            switch ( obj->pIndexData->vnum )
            {
            case OBJ_VNUM_SILVER_ONE:
                silver += 1;
                extract_obj(obj);
                break;

            case OBJ_VNUM_GOLD_ONE:
                gold += 1;
                extract_obj( obj );
                break;

            case OBJ_VNUM_SILVER_SOME:
                silver += obj->value[0];
                extract_obj(obj);
                break;

            case OBJ_VNUM_GOLD_SOME:
                gold += obj->value[1];
                extract_obj( obj );
                break;

            case OBJ_VNUM_COINS:
                silver += obj->value[0];
                gold += obj->value[1];
                extract_obj(obj);
                break;
            }
        }
        obj = create_money( gold, silver );

        if (container->item_type == ITEM_PORTAL) {
            if (room==NULL) {
                act("$n puts $p in $P and sees it disappear.",
                    ch,moved_obj,container,TO_ROOM);
                act("You put $p in $P and see it disappear.",
                    ch,moved_obj,container,TO_CHAR);
                extract_obj(obj);
            } else {
                act("$n puts $p in $P and it disappears.",
                    ch,moved_obj,container,TO_ROOM);
                act("You put $p in $P and it disappears.",
                    ch,moved_obj,container,TO_CHAR);
                obj_to_room(obj,room);
                if (room->people)
                    act("You suddenly see $p appear in the room.",
                        room->people,moved_obj,NULL,TO_ALL);
            }
            if (IS_PC(ch)) {
                STR_SET_BIT(ch->pcdata->usedthatobject,container->pIndexData->vnum);
                STR_SET_BIT(ch->pcdata->usedthatobject,moved_obj->pIndexData->vnum);
            }
        } else {
            obj_to_obj( obj, container );

            if (IS_SET(container->value[1],CONT_PUT_ON)) {
                act("$n puts $p on $P.",ch,moved_obj,container, TO_ROOM);
                act("You put $p on $P.",ch,moved_obj,container, TO_CHAR);
            } else {
                act( "$n puts $p in $P.", ch, moved_obj, container, TO_ROOM );
                act( "You put $p in $P.", ch, moved_obj, container, TO_CHAR );
            }
            if (IS_PC(ch)) {
                STR_SET_BIT(ch->pcdata->usedthatobject,container->pIndexData->vnum);
                STR_SET_BIT(ch->pcdata->usedthatobject,moved_obj->pIndexData->vnum);
            }
        }
        if ( OBJ_HAS_TRIGGER( container, OTRIG_PUTIN ) )
            op_putin_trigger( container, ch, moved_obj );
        extract_obj(moved_obj);
        return;
    }

    argument = one_argument( argument, arg2 );

    number=mult_argument(argn,arg1);

    if (!str_cmp(arg2,"in") || !str_cmp(arg2,"on"))
        argument = one_argument(argument,arg2);

    if ( arg1[0] == '\0' || arg2[0] == '\0' )
    {
        send_to_char( "Put what in what?\n\r", ch );
        return;
    }

    if ( !str_cmp( arg2, "all" ) || !str_prefix( "all.", arg2 ) )
    {
        send_to_char( "You can't do that.\n\r", ch );
        return;
    }

    if ( ( container = get_obj_here( ch, arg2 ) ) == NULL )
    {
        act( "I see no $T here.", ch, NULL, arg2, TO_CHAR );
        return;
    }

    if (container->item_type != ITEM_CONTAINER &&
            container->item_type !=ITEM_PORTAL) {
        send_to_char( "That's not a container.\n\r", ch );
        return;
    }

    if ( IS_SET(container->value[1], CONT_CLOSED) ) {
        act( "The $d is closed.", ch, NULL, container->name, TO_CHAR );
        return;
    }

    if (!IS_NPC(ch) && container->level>ch->level) {
        act( "The $d refuses to let you put that object in it.",
             ch, NULL, container->name, TO_CHAR );
        return;
    }

    if ( str_cmp( arg1, "all" ) && str_prefix( "all.", arg1 ) )
    {
        /* 'put obj container' */

        for (i=0;i<number;i++) {
            if ( ( obj = get_obj_carry( ch, arg1, ch ) ) == NULL ) {
                if (i==0)
                    sprintf_to_char( ch,
                                     "%s: You do not have that item.\n\r",arg1);
                else
                    sprintf_to_char(ch,"You had only %d %s%s to put in %s.\n\r",
                                    i,arg1,i==1?"":"s",container->short_descr);
                return;
            }

            if ( obj == container ) {
                sprintf_to_char(ch,
                                "%s: You can't fold it into itself.\n\r", obj->short_descr);
                return;
            }

            if ( !can_drop_obj( ch, obj ) ) {
                sprintf_to_char(ch,
                                "%s: You can't let go of it.\n\r", obj->short_descr );
                return;
            }

            if (WEIGHT_MULT(obj) != 100) {
                sprintf_to_char(ch,
                                "%s: You have a feeling that would be a bad idea.\n\r",
                                obj->short_descr);
                return;
            }

            if (container->item_type!=ITEM_PORTAL &&
                    (get_obj_weight(obj)+get_true_weight(container) > (container->value[0] * 10)
                     ||  get_obj_weight(obj) > (container->value[3] * 10))) {
                sprintf_to_char(ch,"%s: It won't fit.\n\r", obj->short_descr );
                return;
            }

            if ( OBJ_HAS_TRIGGER( container, OTRIG_PREPUTIN ) )
                if (op_preputin_trigger( container, ch, obj )==FALSE)
                    return;
            if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREPUT ) )
                if (op_preput_trigger( obj, ch, container )==FALSE)
                    return;
            if (!IS_VALID(obj))
                continue;

            if (container->pIndexData->vnum == OBJ_VNUM_PIT
                    &&  !CAN_WEAR(container,ITEM_TAKE)) {
                if (obj->timer)
                    STR_SET_BIT(obj->strbit_extra_flags,ITEM_HAD_TIMER);
                else
                    obj->timer = number_range(100,200);
            }

            if (IS_PC(ch)) {
                STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);
                STR_SET_BIT(ch->pcdata->usedthatobject,container->pIndexData->vnum);
            }

            if (container->item_type==ITEM_PORTAL) {
                ROOM_INDEX_DATA *room;

                //
                // if it has no destination, get rid of it.
                //
                room=get_portal_destination(ch,container);
                if (room==NULL) {
                    sprintf_to_char(ch,"%s seems to be broken.\n\r",container->short_descr);
                    return;
                } else {
                    obj_from_char( obj );

                    act("$n puts $p in $P and it disappears.",
                        ch,obj,container,TO_ROOM);
                    act("You put $p in $P and it disappears.",
                        ch,obj,container,TO_CHAR);
                    obj_to_room(obj,room);
                    if (room->people)
                        act("You suddenly see $p appear in the room.",
                            room->people,obj,NULL,TO_ALL);
                }
            } else {
                obj_from_char( obj );

                obj_to_obj( obj, container );

                if (IS_SET(container->value[1],CONT_PUT_ON)) {
                    act("$n puts $p on $P.",ch,obj,container, TO_ROOM);
                    act("You put $p on $P.",ch,obj,container, TO_CHAR);
                } else {
                    act( "$n puts $p in $P.", ch, obj, container, TO_ROOM );
                    act( "You put $p in $P.", ch, obj, container, TO_CHAR );
                }
            }
            if ( OBJ_HAS_TRIGGER( obj, OTRIG_PUT ) )
                op_put_trigger( obj, ch, container );
            if ( OBJ_HAS_TRIGGER( container, OTRIG_PUTIN ) )
                op_putin_trigger( container, ch, obj );
            if (!IS_VALID(obj))
                continue;
        }
    } else {
        /* 'put all container' or 'put all.obj container' */
        if (number!=1) {
            send_to_char("You can't put everything more than once.\n\r",ch);
            return;
        }
        for ( obj = ch->carrying; obj != NULL; obj = obj_next ) {
            obj_next = obj->next_content;

            if ( ( arg1[3] == '\0' || is_name( &arg1[4], obj->name ) )
                 &&   can_see_obj( ch, obj )
                 &&   WEIGHT_MULT(obj) == 100
                 &&   obj->wear_loc == WEAR_NONE
                 &&   obj != container
                 &&   can_drop_obj( ch, obj )
                 &&   ((container->item_type!=ITEM_PORTAL
                        && get_obj_weight( obj ) + get_true_weight( container )
                        <= (container->value[0] * 10)
                        &&   get_obj_weight(obj) < (container->value[3] * 10))
                       || (container->item_type==ITEM_PORTAL))) {

                if ( OBJ_HAS_TRIGGER( container, OTRIG_PREPUTIN ) )
                    if (op_preputin_trigger( container, ch, obj )==FALSE)
                        continue;
                if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREPUT ) )
                    if (op_preput_trigger( obj, ch, container )==FALSE)
                        continue;
                if (!IS_VALID(obj))
                    continue;

                if (container->pIndexData->vnum == OBJ_VNUM_PIT
                        &&  !CAN_WEAR(obj, ITEM_TAKE) ) {
                    if (obj->timer)
                        STR_SET_BIT(obj->strbit_extra_flags,ITEM_HAD_TIMER);
                    else
                        obj->timer = number_range(100,200);
                }
                if (IS_PC(ch)) {
                    STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);
                    STR_SET_BIT(ch->pcdata->usedthatobject,container->pIndexData->vnum);
                }

                if (container->item_type==ITEM_PORTAL) {
                    ROOM_INDEX_DATA *room;

                    //
                    // if it has no destination, get rid of it.
                    //
                    room=get_portal_destination(ch,container);
                    if (room==NULL) {
                        sprintf_to_char(ch,"%s seems to be broken.\n\r",container->short_descr);
                        return;
                    } else {
                        obj_from_char( obj );
                        act("$n puts $p in $P and it disappears.",
                            ch,obj,container,TO_ROOM);
                        act("You put $p in $P and it disappears.",
                            ch,obj,container,TO_CHAR);
                        obj_to_room(obj,room);
                        if (room->people)
                            act("You suddenly see $p appear in the room.",
                                room->people,obj,NULL,TO_ALL);
                    }
                } else {
                    obj_from_char( obj );
                    obj_to_obj( obj, container );

                    if (IS_SET(container->value[1],CONT_PUT_ON)) {
                        act("$n puts $p on $P.",ch,obj,container, TO_ROOM);
                        act("You put $p on $P.",ch,obj,container, TO_CHAR);
                    } else {
                        act( "$n puts $p in $P.", ch, obj, container, TO_ROOM );
                        act( "You put $p in $P.", ch, obj, container, TO_CHAR );
                    }
                }

                if ( OBJ_HAS_TRIGGER( container, OTRIG_PUTIN ) )
                    op_putin_trigger( container, ch, obj );
                if ( OBJ_HAS_TRIGGER( obj, OTRIG_PUT ) )
                    op_put_trigger( obj, ch, container );
                if (!IS_VALID(obj))
                    continue;
            }
        }
    }

    return;
}



void do_drop( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    char narg[MAX_INPUT_LENGTH];
    OBJ_DATA *obj;
    OBJ_DATA *obj_next;
    bool found;
    int number,i;

    if (check_ordered(ch)) return;

    argument = one_argument( argument, narg );
    number = mult_argument( narg, arg);

    if ( arg[0] == '\0' )
    {
        send_to_char( "Drop what?\n\r", ch );
        return;
    }

    if ( is_number( arg ) )
    {
        /* 'drop NNNN coins' */
        int amount, gold = 0, silver = 0;

        amount   = atoi(arg);
        argument = one_argument( argument, arg );
        if ( amount <= 0
             || ( str_cmp( arg, "coins" ) && str_cmp( arg, "coin" ) &&
                  str_cmp( arg, "gold"  ) && str_cmp( arg, "silver") ) )
        {
            send_to_char( "Sorry, you can't do that.\n\r", ch );
            return;
        }

        if ( !str_cmp( arg, "coins") || !str_cmp(arg,"coin")
             ||   !str_cmp( arg, "silver"))
        {
            if (ch->silver < amount)
            {
                send_to_char("You don't have that much silver.\n\r",ch);
                return;
            }

            ch->silver -= amount;
            silver = amount;
        }

        else
        {
            if (ch->gold < amount)
            {
                send_to_char("You don't have that much gold.\n\r",ch);
                return;
            }

            ch->gold -= amount;
            gold = amount;
        }

        for ( obj = ch->in_room->contents; obj != NULL; obj = obj_next )
        {
            obj_next = obj->next_content;

            switch ( obj->pIndexData->vnum )
            {
            case OBJ_VNUM_SILVER_ONE:
                silver += 1;
                extract_obj(obj);
                break;

            case OBJ_VNUM_GOLD_ONE:
                gold += 1;
                extract_obj( obj );
                break;

            case OBJ_VNUM_SILVER_SOME:
                silver += obj->value[0];
                extract_obj(obj);
                break;

            case OBJ_VNUM_GOLD_SOME:
                gold += obj->value[1];
                extract_obj( obj );
                break;

            case OBJ_VNUM_COINS:
                silver += obj->value[0];
                gold += obj->value[1];
                extract_obj(obj);
                break;
            }
        }

        obj_to_room( create_money( gold, silver ), ch->in_room );
        act( "$n drops some coins.", ch, NULL, NULL, TO_ROOM );
        send_to_char( "OK.\n\r", ch );
        return;
    }

    if ( str_cmp( arg, "all" ) && str_prefix( "all.", arg ) )
    {
        /* 'drop obj' */
        for (i=0;i<number;i++) {
            if ( ( obj = get_obj_carry( ch, arg, ch ) ) == NULL ) {
                if (i==0)
                    send_to_char( "You do not have that item.\n\r", ch );
                else
                    sprintf_to_char(ch,"You were only carrying %d %s%s.\n\r",
                                    i,arg,i==1?"":"s");
                return;
            }

            if ( !can_drop_obj( ch, obj ) ) {
                sprintf_to_char(ch,
                                "%s: You can't let go of it.\n\r",obj->short_descr);
                return;
            }

            // pre drop-check, if it is true don't drop it.
            if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREDROP ) )
                if (op_predrop_trigger( obj, ch )==FALSE)
                    return;	// if you can't drop it, skip the rest
            if (!IS_VALID(obj))
                continue;

            obj_from_char( obj );
            obj_to_room( obj, ch->in_room );
            act( "$n drops $p.", ch, obj, NULL, TO_ROOM );
            act( "You drop $p.", ch, obj, NULL, TO_CHAR );

            if (IS_PC(ch))
                STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);

            if ( OBJ_HAS_TRIGGER( obj, OTRIG_DROP ) )
                op_drop_trigger( obj, ch );
            if (!IS_VALID(obj))
                continue;

            if (IS_OBJ_STAT(obj,ITEM_MELT_DROP)) {
                act("$p dissolves into smoke.",ch,obj,NULL,TO_ROOM);
                act("$p dissolves into smoke.",ch,obj,NULL,TO_CHAR);
                extract_obj(obj);
            } else {
                if (obj->in_room->sector_type==SECT_AIR)
                    object_drop_air(obj);
                else if (obj->in_room->sector_type==SECT_WATER_NOSWIM ||
                         obj->in_room->sector_type==SECT_WATER_SWIM ||
                         obj->in_room->sector_type==SECT_WATER_BELOW )
                    object_drop_water(obj);
                else if (number_percent()<10)
                    object_drop_fall(obj);
            }
        }
    } else {
        /* 'drop all' or 'drop all.obj' */
        if (number!=1) {
            send_to_char("You can't drop everything more than once.\n\r",ch);
            return;
        }
        found = FALSE;
        for ( obj = ch->carrying; obj != NULL; obj = obj_next )
        {
            obj_next = obj->next_content;

            if ( ( arg[3] == '\0' || is_name( &arg[4], obj->name ) )
                 &&   can_see_obj( ch, obj )
                 &&   obj->wear_loc == WEAR_NONE
                 &&   can_drop_obj( ch, obj ) )
            {
                // pre drop-check, if it is true don't drop it.
                if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREDROP ) )
                    if (op_predrop_trigger( obj, ch )==FALSE)
                        continue;
                if (!IS_VALID(obj))
                    continue;

                found = TRUE;
                obj_from_char( obj );
                obj_to_room( obj, ch->in_room );
                act( "$n drops $p.", ch, obj, NULL, TO_ROOM );
                act( "You drop $p.", ch, obj, NULL, TO_CHAR );

                if (IS_PC(ch))
                    STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);

                if ( OBJ_HAS_TRIGGER( obj, OTRIG_DROP ) )
                    op_drop_trigger( obj, ch );
                if (!IS_VALID(obj))
                    continue;

                if (IS_OBJ_STAT(obj,ITEM_MELT_DROP)) {
                    act("$p dissolves into smoke.",ch,obj,NULL,TO_ROOM);
                    act("$p dissolves into smoke.",ch,obj,NULL,TO_CHAR);
                    extract_obj(obj);
                } else
                    if (obj->in_room->sector_type==SECT_AIR)
                        object_drop_air(obj);
                    else if (obj->in_room->sector_type==SECT_WATER_NOSWIM ||
                             obj->in_room->sector_type==SECT_WATER_SWIM ||
                             obj->in_room->sector_type==SECT_WATER_BELOW )
                        object_drop_water(obj);
                    else if (number_percent()<10)
                        object_drop_fall(obj);
            }
        }

        if ( !found )
        {
            if ( arg[3] == '\0' )
                act( "You are not carrying anything.",
                     ch, NULL, arg, TO_CHAR );
            else
                act( "You are not carrying any $T.",
                     ch, NULL, &arg[4], TO_CHAR );
        }
    }

    return;
}


void do_give( CHAR_DATA *ch, char *argument )
{
    char argn [MAX_INPUT_LENGTH];
    char arg1 [MAX_INPUT_LENGTH];
    char arg2 [MAX_INPUT_LENGTH];
    char buf[MAX_STRING_LENGTH];
    CHAR_DATA *victim;
    SPEC_FUN *spec_banker;
    OBJ_DATA  *obj;
    int number,i;
    int levelmin,levelmax;

    if (check_ordered(ch)) return;

    argument = one_argument( argument, argn );
    argument = one_argument( argument, arg2 );
    number = mult_argument (argn,arg1);

    if ( arg1[0] == '\0' || arg2[0] == '\0' )
    {
        send_to_char( "Give what to whom?\n\r", ch );
        return;
    }

    if ( is_number( arg1 ) )
    {
        /* 'give NNNN coins victim' */
        int amount;
        bool silver;

        amount   = atoi(arg1);
        if ( amount <= 0
             || ( str_cmp( arg2, "coins" ) && str_cmp( arg2, "coin" ) &&
                  str_cmp( arg2, "gold"  ) && str_cmp( arg2, "silver")) )
        {
            send_to_char( "Sorry, you can't do that.\n\r", ch );
            return;
        }

        silver = str_cmp(arg2,"gold");

        argument = one_argument( argument, arg2 );
        if ( arg2[0] == '\0' )
        {
            send_to_char( "Give what to whom?\n\r", ch );
            return;
        }

        if ( ( victim = get_char_room( ch, arg2 ) ) == NULL )
        {
            send_to_char( "They aren't here.\n\r", ch );
            return;
        }

        if ( (!silver && ch->gold < amount) || (silver && ch->silver < amount) )
        {
            send_to_char( "You haven't got that much.\n\r", ch );
            return;
        }

        if ((spec_banker=spec_lookup("spec_banker"))==NULL) {
            bugf("do_give(): Couldn't find `spec_banker', no giving of money possible.");
            return;
        }

        if (IS_NPC(victim) && victim->spec_fun==spec_banker) {
            sprintf(buf,"%d %s",amount,silver?"silver":"gold");
            do_function(ch,&do_deposit,buf);
            return;
        }

        if (silver) {
            ch->silver		-= amount;
            victim->silver 	+= amount;
        } else {
            ch->gold		-= amount;
            victim->gold	+= amount;
        }

        sprintf(buf,"$n gives you %s.",
                money_string(silver?0:amount,silver?amount:0));
        act( buf, ch, NULL, victim, TO_VICT    );
        act( "$n gives $N some coins.",  ch, NULL, victim, TO_NOTVICT );
        sprintf(buf,"You give $N %s.",
                money_string(silver?0:amount,silver?amount:0));
        act( buf, ch, NULL, victim, TO_CHAR    );

        /*
     * Bribe trigger
     */
        if ( IS_NPC(victim) && MOB_HAS_TRIGGER( victim, MTRIG_BRIBE ) ) {
            mp_bribe_trigger(victim,ch,silver?amount:amount*COINS_SILVER_IN_GOLD);
            if (!IS_VALID(victim))
                return;
        } else if (IS_NPC(victim) && STR_IS_SET(victim->strbit_act,ACT_IS_CHANGER))
        {
            int change;

            change = (silver ? 95 * amount / 100 / 100 : 95 * amount);

            if (!silver && change > victim->silver)
                victim->silver += change;

            if (silver && change > victim->gold)
                victim->gold += change;

            if (change < 1 && can_see(victim,ch))
            {
                act("$n tells you '"C_TELL"I'm sorry, you did not give me enough to change.{x'"
                    ,victim,NULL,ch,TO_VICT);
                ch->reply = victim;
                sprintf(buf,"%d %s %s",
                        amount, silver ? "silver" : "gold",ch->name);
                do_function(victim, &do_give, buf);
            }
            else if (can_see(victim,ch))
            {
                sprintf(buf,"%d %s %s",
                        change, silver ? "gold" : "silver",ch->name);
                do_function(victim, &do_give, buf);
                if (silver)
                {
                    sprintf(buf,"%d silver %s",
                            (95 * amount / 100 - change * 100),ch->name);
                    do_function(victim, &do_give, buf);
                }
                act("$n tells you '"C_TELL"Thank you, come again.{x'",
                    victim,NULL,ch,TO_VICT);
                ch->reply = victim;
            }
        }
        return;
    }

    if ( ( victim = get_char_room( ch, arg2 ) ) == NULL ) {
        send_to_char( "They aren't here.\n\r", ch );
        return;
    }

    if (IS_NPC(victim) && (victim->pIndexData->pShop != NULL) &&
            !(MOB_HAS_TRIGGER( victim, MTRIG_GIVE ))) {
        act("$N tells you '"C_TELL"Sorry, you'll have to sell that.{x'",
            ch,NULL,victim,TO_CHAR);
        ch->reply = victim;
        return;
    }

    for (i=0;i<number;i++) {

        if ( ( obj = get_obj_carry( ch, arg1, ch ) ) == NULL ) {
            if (i==0)
                send_to_char( "You do not have that item.\n\r", ch );
            else
                sprintf_to_char(ch,"You had only %d %s%s.\n\r",
                                i,arg1,i==1?"":"s");
            return;
        }

        if ( obj->wear_loc != WEAR_NONE ) {
            sprintf_to_char( ch,
                             "%s: You must remove it first.\n\r", obj->short_descr );
            return;
        }

        if ( !can_drop_obj( ch, obj ) ) {
            sprintf_to_char(ch,
                            "%s: You can't let go of it.\n\r",obj->short_descr);
            return;
        }

        if ( victim->carry_number + get_obj_number( obj ) > can_carry_n( victim ) ) {
            act( "$p: $N has $S hands full.", ch, obj, victim, TO_CHAR );
            return;
        }

        if (get_carry_weight(victim) + get_obj_weight(obj) > can_carry_w( victim ) )
        {
            act( "$p: $N can't carry that much weight.", ch, obj, victim, TO_CHAR );
            return;
        }

        if ( !can_see_obj( victim, obj ) )
        {
            act( "$p: $N can't see it.", ch, obj, victim, TO_CHAR );
            return;
        }

        //
        // check for clan items (only when the giver and receiver are not imms)
        //
        if (!IS_IMMORTAL(ch) && !IS_IMMORTAL(victim)
                &&  GetObjectProperty(obj,PROPERTY_STRING,"clan",buf)) {
            if (victim->clan==NULL || !str_cmp(victim->clan->clan_info->name,buf)) {
                send_to_char(
                            "That's a clan-object, you shouldn't give it away to "
                            "somebody who doesn't belong to your clan.\n\r",ch);
                return;
            }
        }

        if (IS_PC(ch) &&
                IS_PC(victim) && !IS_IMMORTAL(victim) &&
                obj_is_owned_by_another(victim,obj)) {
            if (IS_IMMORTAL(ch)) {
                act("WARNING: $p is not owned by $N.",ch,obj,victim,TO_CHAR);
                if (ch->desc->repeat==0) {
                    send_to_char("Repeat command to actually give the object.\n\r",ch);
                    return;
                }
            } else {
                act("$p is your property, don't give it away.",ch,obj,victim, TO_CHAR);
                act("$n tries to give $p to you, but you can't accept $s property.",ch,obj,victim, TO_VICT);
                return;
            }
        }

        if (!op_pregive_trigger(obj,ch,victim))
            continue;

        MOBtrigger=FALSE;
        obj_from_char( obj );
        act( "$n gives $p to $N.", ch, obj, victim, TO_NOTVICT );
        act( "$n gives you $p.",   ch, obj, victim, TO_VICT    );
        act( "You give $p to $N.", ch, obj, victim, TO_CHAR    );
        if (IS_IMMORTAL(victim) && obj_is_owned_by_another(ch,obj))
            act("WARNING: $p is not owned by $n.",ch,obj,victim,TO_VICT);
        obj_to_char( obj, victim );
        if (IS_PC(ch))
            STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);
        if (IS_PC(victim))
            STR_SET_BIT(victim->pcdata->usedthatobject,obj->pIndexData->vnum);

        levelmin=levelmax=-1;
        GetObjectProperty(obj,PROPERTY_INT,"levelhavemin",&levelmin);
        GetObjectProperty(obj,PROPERTY_INT,"levelhavemax",&levelmax);
        if ((levelmin>=0 && victim->level<levelmin) ||
                (levelmax>=0 && victim->level>levelmax)) {
            zap_char_obj(obj,TRUE);
            continue;
        }


        MOBtrigger=TRUE;
        /*
     * Give trigger
     */
        op_give_trigger(obj,ch,victim);
        if (!IS_VALID(obj)) continue;

        if ( IS_NPC(victim) && MOB_HAS_TRIGGER( victim, MTRIG_GIVE ) )
            mp_give_trigger( victim, ch, obj );
        if (!IS_VALID(victim))
            return;
    }

    return;
}


/* for poisoning weapons and food/drink */
void do_envenom(CHAR_DATA *ch, char *argument)
{
    OBJ_DATA *obj;
    EFFECT_DATA ef;
    int percent = 75;

    /* find out what */
    if (argument[0] == '\0')
    {
        send_to_char("Envenom what item?\n\r",ch);
        return;
    }

    obj =  get_obj_list(ch,argument,ch->carrying);

    if (obj== NULL)
    {
        send_to_char("You don't have that item.\n\r",ch);
        return;
    }

    if (!has_skill_available(ch,gsn_envenom))
    {
        send_to_char("Are you crazy? You'd poison yourself!\n\r",ch);
        return;
    }

    if (obj->item_type == ITEM_FOOD || obj->item_type == ITEM_DRINK_CON)
    {
        if (IS_OBJ_STAT(obj,ITEM_BLESS) || IS_OBJ_STAT(obj,ITEM_BURN_PROOF))
        {
            act("You fail to poison $p.",ch,obj,NULL,TO_CHAR);
            return;
        }

        if (skillcheck(ch,gsn_envenom)) {
            act("$n treats $p with deadly poison.",ch,obj,NULL,TO_ROOM);
            act("You treat $p with deadly poison.",ch,obj,NULL,TO_CHAR);
            if (!obj->value[3])
            {
                obj->value[3] = 1;
                check_improve(ch,gsn_envenom,TRUE,4);
            }
            WAIT_STATE(ch,skill_beats(gsn_envenom,ch));
            return;
        }

        act("You fail to poison $p.",ch,obj,NULL,TO_CHAR);
        if (!obj->value[3])
            check_improve(ch,gsn_envenom,FALSE,4);
        WAIT_STATE(ch,skill_beats(gsn_envenom,ch));
        return;
    }

    if (obj->item_type == ITEM_WEAPON)
    {
        if (IS_WEAPON_STAT(obj,WEAPON_FLAMING)) {
            act("The poison evapourates before you can reach $p.",
                ch,obj,NULL,TO_CHAR);
            return;
        }
        if (IS_WEAPON_STAT(obj,WEAPON_FROST)) {
            act("The poison freezes immediatly when you place it on $p.",
                ch,obj,NULL,TO_CHAR);
            return;
        }
        if (IS_WEAPON_STAT(obj,WEAPON_VAMPIRIC)) {
            act("The poison is sucked into $p.",ch,obj,NULL,TO_CHAR);
            return;
        }
        if (IS_WEAPON_STAT(obj,WEAPON_SHARP)) {
            act("The poison drips of from $p.",ch,obj,NULL,TO_CHAR);
            return;
        }
        if (IS_WEAPON_STAT(obj,WEAPON_VORPAL)) {
            act("The poison drips of from $p.",ch,obj,NULL,TO_CHAR);
            return;
        }
        if (IS_WEAPON_STAT(obj,WEAPON_SHOCKING)) {
            act("You aren't able to poison $p because you feel small shocks "
                "when you try to envonom it.",ch,obj,NULL,TO_CHAR);
            return;
        }
        if (IS_OBJ_STAT(obj,ITEM_BLESS) || IS_OBJ_STAT(obj,ITEM_BURN_PROOF))
        {
            act("You can't seem to envenom $p.",ch,obj,NULL,TO_CHAR);
            return;
        }

        if (obj->value[3] < 0
                ||  attack_table[obj->value[3]].damage == DAM_BASH)
        {
            send_to_char("You can only envenom edged weapons.\n\r",ch);
            return;
        }

        if (IS_WEAPON_STAT(obj,WEAPON_POISON))
        {
            act("$p is already envenomed.",ch,obj,NULL,TO_CHAR);
            return;
        }

        if (IS_PC(ch))
            STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);

        if (skillcheck(ch,gsn_envenom)) {
            ZEROVAR(&ef,EFFECT_DATA);
            ef.where     = TO_WEAPON;
            ef.type      = gsn_poison;
            ef.level     = ch->level * percent / 100;
            ef.duration  = ch->level/2 * percent / 100;
            ef.location  = 0;
            ef.modifier  = 0;
            ef.bitvector =WEAPON_POISON;
            ef.casted_by = ch?ch->id:0;
            effect_to_obj(obj,&ef);

            act("$n coats $p with deadly venom.",ch,obj,NULL,TO_ROOM);
            act("You coat $p with venom.",ch,obj,NULL,TO_CHAR);
            check_improve(ch,gsn_envenom,TRUE,3);
            WAIT_STATE(ch,skill_beats(gsn_envenom,ch));
            return;
        }
        else
        {
            act("You fail to envenom $p.",ch,obj,NULL,TO_CHAR);
            check_improve(ch,gsn_envenom,FALSE,3);
            WAIT_STATE(ch,skill_beats(gsn_envenom,ch));
            return;
        }
    }

    act("You can't poison $p.",ch,obj,NULL,TO_CHAR);
    return;
}



void do_steal( CHAR_DATA *ch, char *argument )
{
    char buf  [MAX_STRING_LENGTH];
    char arg1 [MAX_INPUT_LENGTH];
    char arg2 [MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    OBJ_DATA *obj;
    int percent;
    int levelmin,levelmax;

    if (check_ordered(ch)) return;

    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );

    if ( arg1[0] == '\0' || arg2[0] == '\0' ) {
        send_to_char( "Steal what from whom?\n\r", ch );
        return;
    }

    if ( ( victim = get_char_room( ch, arg2 ) ) == NULL ) {
        send_to_char( "They aren't here.\n\r", ch );
        return;
    }

    if ( victim == ch ) {
        send_to_char( "That's pointless.\n\r", ch );
        return;
    }

    if (is_safe(ch,victim))
        return;

    if ( IS_NPC(victim)
         && victim->position == POS_FIGHTING)
    {
        send_to_char(  "Kill stealing is not permitted.\n\r"
                       "You'd better not -- you might get hit.\n\r",ch);
        return;
    }

    WAIT_STATE( ch, skill_beats(gsn_steal,ch));

    if (!IS_AWAKE(victim))
        percent = 110;
    else if (!can_see(victim,ch))
        percent = 75;
    else
        percent = 50;

    percent+=(victim->level-ch->level);

    if ( ((ch->level + 7 < victim->level || ch->level -7 > victim->level)
          && !IS_NPC(victim) && !IS_NPC(ch) )
         || ( !IS_NPC(ch) && !skillcheck2(ch,gsn_steal,percent))
         || ( !IS_NPC(ch) && !IS_NPC(victim) && !is_clan(ch)) )
    {
        /*
     * Failure.
     */
        send_to_char( "Oops.\n\r", ch );
        effect_strip(ch,gsn_sneak);
        /* REMOVE_BIT(ch->affected_by,EFF_SNEAK); */

        act( "$n tried to steal from you.", ch, NULL, victim, TO_VICT    );
        act( "$n tried to steal from $N.",  ch, NULL, victim, TO_NOTVICT );
        switch(number_range(0,3))
        {
        case 0 :
            sprintf( buf, "%s is a lousy thief!", ch->name );
            break;
        case 1 :
            sprintf( buf, "%s couldn't rob %s way out of a paper bag!",
                     ch->name,show_sex(ch) == SEX_FEMALE ? "her" : "his");
            break;
        case 2 :
            sprintf( buf,"%s tried to rob me!",ch->name );
            break;
        case 3 :
            sprintf(buf,"Keep your hands out of there, %s!",ch->name);
            break;
        }
        if (!IS_AWAKE(victim))
            do_function(victim, &do_wake, "");
        if (IS_AWAKE(victim))
            do_function(victim, &do_yell, buf );
        if ( !IS_NPC(ch) )
        {
            if ( IS_NPC(victim) )
            {
                check_improve(ch,gsn_steal,FALSE,2);
                multi_hit( victim, ch, TYPE_UNDEFINED );
            }
            else
            {
                wiznet(WIZ_FLAGS,0,ch,NULL,
                       "$N tried to steal from %s.",victim->name);
                if ( !STR_IS_SET(ch->strbit_act, PLR_THIEF) ) {
                    STR_SET_BIT(ch->strbit_act, PLR_THIEF);
                    send_to_char( "*** You are now a THIEF!! ***\n\r", ch );
                    save_char_obj( ch );
                }
            }
        }

        return;
    }

    if ( !str_cmp( arg1, "coin"  )
         ||   !str_cmp( arg1, "coins" )
         ||   !str_cmp( arg1, "gold"  )
         ||	 !str_cmp( arg1, "silver"))
    {
        int gold, silver;

        gold = victim->gold * number_range(1, ch->level) / MAX_LEVEL;
        silver = victim->silver * number_range(1,ch->level) / MAX_LEVEL;
        if ( gold <= 0 && silver <= 0 )
        {
            send_to_char( "You couldn't get any coins.\n\r", ch );
            return;
        }

        ch->gold     	+= gold;
        ch->silver   	+= silver;
        victim->silver 	-= silver;
        victim->gold 	-= gold;

        sprintf_to_char(ch,"Bingo! You got %s.\n\r",money_string(gold,silver));
        check_improve(ch,gsn_steal,TRUE,2);
        return;
    }

    if ( ( obj = get_obj_carry( victim, arg1, ch ) ) == NULL )
    {
        send_to_char( "You can't find it.\n\r", ch );
        return;
    }

    if ( !can_drop_obj( ch, obj )
         ||   STR_IS_SET(obj->strbit_extra_flags, ITEM_INVENTORY)
         ||   STR_IS_SET(obj->strbit_extra_flags, ITEM_NOSTEAL)
         ||   obj->level > ch->level )
    {
        send_to_char( "You can't pry it away.\n\r", ch );
        return;
    }

    if ( ch->carry_number + get_obj_number( obj ) > can_carry_n( ch ) )
    {
        send_to_char( "You have your hands full.\n\r", ch );
        return;
    }

    if ( ch->carry_weight + get_obj_weight( obj ) > can_carry_w( ch ) )
    {
        send_to_char( "You can't carry that much weight.\n\r", ch );
        return;
    }

    obj_from_char( obj );
    obj_to_char( obj, ch );
    act("You pocket $p.",ch,obj,NULL,TO_CHAR);
    check_improve(ch,gsn_steal,TRUE,2);
    send_to_char( "Got it!\n\r", ch );
    if (IS_PC(ch))
        STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);

    levelmin=levelmax=-1;
    GetObjectProperty(obj,PROPERTY_INT,"levelhavemin",&levelmin);
    GetObjectProperty(obj,PROPERTY_INT,"levelhavemax",&levelmax);
    if ((levelmin>=0 && ch->level<levelmin) ||
            (levelmax>=0 && ch->level>levelmax)) {
        zap_char_obj(obj,TRUE);
    }

    return;
}


void do_sharpen(CHAR_DATA *ch, char *argument)
{
    OBJ_DATA *obj,*stone;
    EFFECT_DATA ef;
    char arg[MAX_INPUT_LENGTH];
    bool b;

    if (ch->position==POS_FIGHTING) {
        send_to_char("No way, you're fighting.",ch);
        return;
    }

    argument = one_argument(argument, arg);
    if (arg[0] == '\0') {
        send_to_char("Sharpen what weapon?\n\r",ch);
        return;
    }

    obj =  get_obj_list(ch,arg,ch->carrying);
    if (obj== NULL) {
        send_to_char("You don't have that item.\n\r",ch);
        return;
    }

    if (!has_skill_available(ch,gsn_sharpen)) {
        send_to_char("You have no idea how to sharpen weapons.\n\r",ch);
        return;
    }

    if (obj->item_type != ITEM_WEAPON) {
        send_to_char("You can only sharpen weapons.\n\r",ch);
        return;
    }

    /* Of course not everything can be sharpened! */
    switch (obj->value[0]) {
    case WEAPON_EXOTIC:
        send_to_char(
                    "You have no idea how to sharpen this extraordinary weapon.\n\r"
                    ,ch);
        return;

    case WEAPON_SWORD:
    case WEAPON_DAGGER:
    case WEAPON_SPEAR:
    case WEAPON_AXE:
        break;

    case WEAPON_POLEARM:
        send_to_char("You can't reach the end of it!\n\r",ch);
        return;

    case WEAPON_MACE:
    case WEAPON_FLAIL:
    case WEAPON_WHIP:
    default:
        send_to_char("You can't sharpen this kind of weapon.\n\r",ch);
        return;
    }

    if (IS_WEAPON_STAT(obj,WEAPON_SHARP)) {
        act("$p can't be made sharper.",ch,obj,NULL,TO_CHAR);
        return;
    }

    stone=get_eq_char(ch,WEAR_HOLD);
    if (stone==NULL) {
        send_to_char("You need to hold something to sharpen it with!\n\r",ch);
        return;
    }

    b=FALSE;
    GetObjectProperty(stone,PROPERTY_BOOL,"whetstone",&b);
    if (!b) {
        act("You need a real whetstone, not a $p.",ch,stone,NULL,TO_CHAR);
        return;
    }

    if (IS_PC(ch))
        STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);

    if (skillcheck(ch,gsn_sharpen)) {
        int percent;
        percent=100-(number_percent()*get_skill(ch,gsn_sharpen)/100); // get a number between skill and 100
        ZEROVAR(&ef,EFFECT_DATA);
        ef.where     = TO_WEAPON;
        ef.type      = gsn_sharpen;
        ef.level     = ch->level * percent / 100;
        ef.duration  = ch->level * percent; /* just make it long */
        ef.location  = 0;
        ef.modifier  = 0;
        ef.bitvector = WEAPON_SHARP;
        ef.casted_by = 0;
        effect_to_obj(obj,&ef);

        act("$n pulls out a piece of stone and begins sharpening $p.",
            ch,obj,NULL,TO_ROOM);
        act("You sharpen $p.",ch,obj,NULL,TO_CHAR);
        check_improve(ch,gsn_sharpen,TRUE,3);
        WAIT_STATE(ch,skill_beats(gsn_sharpen,ch));

        if (stone->item_type!=ITEM_GEM) {
            act("The sharpening of $p took the strength out of the whetstone.",
                ch,obj,NULL,TO_CHAR);
            act("$n looks very sad when $e realizes the"
                " current state of $s whetstone.",
                ch,NULL,NULL,TO_ROOM);
            extract_obj(stone);
        }
    }
}


void do_puke( CHAR_DATA *ch, char *argument) {
    OBJ_DATA *puke;
    OBJ_INDEX_DATA *pukeidx=get_obj_index (OBJ_VNUM_PUKE);

    if (pukeidx!=NULL) {
        if ( !IS_NPC(ch) ) {
            gain_condition(ch,COND_DRUNK ,-dice(1,4));
            gain_condition(ch,COND_FULL  ,-dice(1,8));
            gain_condition(ch,COND_THIRST,-dice(1,6));
            gain_condition(ch,COND_HUNGER,-dice(1,6));
        }

        // don't make multiple pukes
        if (get_obj_here_by_objtype(pukeidx,ch->in_room->contents)==NULL) {
            puke = create_object( pukeidx,0);
            puke->timer=number_range(1,4);
            obj_to_room( puke, ch->in_room );
            op_load_trigger(puke,ch);
        }
    } else {
        bugf("Cannot find object for OBJ_VNUM_PUKE (%d) for %s.",
             OBJ_VNUM_PUKE,NAME(ch));
    }
    check_social( ch, "puke", argument );
    return;
}


void do_use(CHAR_DATA *ch,char *argument) {
    char	arg[MSL];
    OBJ_DATA *	obj;

    argument = one_argument(argument, arg);
    if (arg[0]==0) {
        send_to_char("Use what?\n\r",ch);
        return;
    }

    if ((obj=get_obj_list(ch,arg,ch->carrying))==NULL)
        obj=get_obj_list(ch,arg,ch->carrying);
    if (obj==NULL) {
        send_to_char("You don't have or see such an item.\n\r",ch);
        return;
    }

    if (OBJ_HAS_TRIGGER(obj,OTRIG_PREUSE) || OBJ_HAS_TRIGGER(obj,OTRIG_USE)) {
        if (OBJ_HAS_TRIGGER(obj,OTRIG_PREUSE))
            if (op_preuse_trigger(obj,ch,argument)==FALSE)
                return;
        if (!IS_VALID(obj))
            return;
        op_use_trigger(obj,ch,argument);
    } else {
        send_to_char("You don't know how to use this.\n\r",ch);
    }

}
