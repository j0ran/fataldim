//
// $Id: fight_action.c,v 1.25 2008/04/29 18:54:13 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"
#include "interp.h"



void do_berserk( CHAR_DATA *ch, char *argument)
{
    float chance;

    if (!has_skill_available(ch,gsn_berserk)
    ||  (IS_NPC(ch) && !STR_IS_SET(ch->strbit_off_flags,OFF_BERSERK))
    ||  (!IS_NPC(ch)
    &&   ch->level < skill_level(gsn_berserk,ch)))
    {
	send_to_char("You turn red in the face, but nothing happens.\n\r",ch);
	return;
    }

    if (IS_AFFECTED(ch,EFF_BERSERK) || is_affected(ch,gsn_berserk)
    ||  is_affected(ch,gsn_frenzy))
    {
	send_to_char("You get a little madder.\n\r",ch);
	return;
    }

    if (IS_AFFECTED(ch,EFF_CALM))
    {
	send_to_char("You're feeling too mellow to berserk.\n\r",ch);
	return;
    }

    if (ch->mana < 50)
    {
	send_to_char("You can't get up enough energy.\n\r",ch);
	return;
    }

    /* modifiers */

    /* fighting */
    if (ch->position == POS_FIGHTING)
	chance = 110;
    else
	chance = 100;

    /* damage -- below 50% of hp helps, above hurts */
    chance = chance * (125 - (50 * ch->hit/ch->max_hit))*0.01;

    if (skillcheck2(ch,gsn_berserk,chance)) {
	EFFECT_DATA ef;

	WAIT_STATE(ch,PULSE_VIOLENCE);
	ch->mana -= 50;
	ch->move /= 2;

	/* heal a little damage */
	if (ch->hit < ch->max_hit) {
	    ch->hit += ch->level * 2;
	    ch->hit = UMIN(ch->hit,ch->max_hit);
	}

	send_to_char("Your pulse races as you are consumed by rage!\n\r",ch);
	if (!IS_SET(ch->parts,PART_EYE))
	    act("$n gets a wild look in $s face.",ch,NULL,NULL,TO_ROOM);
	else
	    act("$n gets a wild look in $s eyes.",ch,NULL,NULL,TO_ROOM);
	check_improve(ch,gsn_berserk,TRUE,2);

	ZEROVAR(&ef,EFFECT_DATA);
	ef.where	= TO_EFFECTS;
	ef.type		= gsn_berserk;
	ef.level	= ch->level;
	ef.duration	= number_fuzzy(ch->level / 8);
	ef.modifier	= UMAX(1,ch->level/5);
	ef.bitvector    = EFF_BERSERK;
	ef.casted_by	= ch?ch->id:0;
	ef.location	= APPLY_HITROLL;
	effect_to_char(ch,&ef);

	ef.location	= APPLY_DAMROLL;
	effect_to_char(ch,&ef);

	ef.modifier	= UMAX(10,10 * (ch->level/5));
	ef.location	= APPLY_AC;
	effect_to_char(ch,&ef);
    }

    else
    {
	WAIT_STATE(ch,3 * PULSE_VIOLENCE);
	ch->mana -= 25;
	ch->move /= 2;

	send_to_char("Your pulse speeds up, but nothing happens.\n\r",ch);
	check_improve(ch,gsn_berserk,FALSE,2);
    }
}

void do_bash( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    float chance;

    one_argument(argument,arg);

    if ( !has_skill_available(ch,gsn_bash)
    ||	 (IS_NPC(ch) && !STR_IS_SET(ch->strbit_off_flags,OFF_BASH))
    ||	 (!IS_NPC(ch)
    &&	  ch->level < skill_level(gsn_bash,ch)))
    {
	send_to_char("Bashing? What's that?\n\r",ch);
	return;
    }

    if (IS_AFFECTED(ch,EFF_CALM)) {
	send_to_char("You're feeling too mellow to bash.\n\r",ch);
	return;
    }

    if (arg[0] == '\0')
    {
	victim = ch->fighting;
	if (victim == NULL)
	{
	    send_to_char("But you aren't fighting anyone!\n\r",ch);
	    return;
	}
    }

    else if ((victim = get_char_room(ch,arg)) == NULL)
    {
	send_to_char("They aren't here.\n\r",ch);
	return;
    }

    if (victim->position < POS_FIGHTING)
    {
	act("You'll have to let $M get back up first.",ch,NULL,victim,TO_CHAR);
	return;
    }

    if (victim == ch)
    {
	send_to_char("You try to bash your brains out, but fail.\n\r",ch);
	return;
    }

    if (is_safe(ch,victim))
	return;

    if (MOB_HAS_TRIGGER(victim,MTRIG_PREATTACK) &&
	ch->fighting!=victim &&
	!mp_preattack_trigger(victim,ch,"bash"))
	return;

    if (!rp_preattack_trigger(ch,victim,"bash"))
	return;

    if (!allow_killstealing(ch,victim))
	return;

    if (IS_AFFECTED(ch,EFF_CHARM) && ch->master == victim)
    {
	act("But $N is your friend!",ch,NULL,victim,TO_CHAR);
	return;
    }


    chance=100;
    /* modifiers */

    /* size  and weight */
    chance *= 1+ ch->carry_weight*0.00004;
    chance /= 1+ victim->carry_weight*0.00005;

    if (ch->size < victim->size)
	chance *= 1+ (ch->size - victim->size) * 15 *0.01;
    else
	chance *= 1+ (ch->size - victim->size) * 10 *0.01;


    /* stats */
    chance *= 1+ get_curr_stat(ch,STAT_STR)*0.01 ;
    chance /= 1+ (get_curr_stat(victim,STAT_DEX) * 4)*0.003;
    chance /= 1+ GET_AC(victim,AC_BASH)*0.0004;
    /* speed */
    if (STR_IS_SET(ch->strbit_off_flags,OFF_FAST) || IS_AFFECTED(ch,EFF_HASTE))
        chance *= 1.1;
    if (STR_IS_SET(victim->strbit_off_flags,OFF_FAST) || IS_AFFECTED(victim,EFF_HASTE))
        chance *= 0.7;

    /* level */
    chance *= 1+(ch->level - victim->level)*0.01;

    if (!IS_NPC(victim)
	&& skillcheck2(victim,gsn_dodge,10000/chance))
    {	/*
        act("$n tries to bash you, but you dodge it.",ch,NULL,victim,TO_VICT);
        act("$N dodges your bash, you fall flat on your face.",ch,NULL,victim,TO_CHAR);
        WAIT_STATE(ch,skill_beats(gsn_bash,ch));
        return;*/
	chance /= 1+ 3 * (get_skill(victim,gsn_dodge) - chance);
    }

    /* now the attack */
    if (skillcheck2(ch,gsn_bash,chance))
    {
	act("$n sends you sprawling with a powerful bash!",
		ch,NULL,victim,TO_VICT);
	act("You slam into $N, and send $M flying!",ch,NULL,victim,TO_CHAR);
	act("$n sends $N sprawling with a powerful bash.",
		ch,NULL,victim,TO_NOTVICT);
	check_improve(ch,gsn_bash,TRUE,1);

	DAZE_STATE(victim, 3 * PULSE_VIOLENCE);
	WAIT_STATE(ch,skill_beats(gsn_bash,ch));
	victim->position = POS_RESTING;
	damage(ch,victim,number_range(2,2 + 2 * ch->size + chance/20),gsn_bash,
	    DAM_BASH,FALSE, NULL);

    } else {
	damage(ch,victim,0,gsn_bash,DAM_BASH,FALSE, NULL);
	act("You fall flat on your face!",
	    ch,NULL,victim,TO_CHAR);
	act("$n falls flat on $s face.",
	    ch,NULL,victim,TO_NOTVICT);
	act("You evade $n's bash, causing $m to fall flat on $s face.",
	    ch,NULL,victim,TO_VICT);
	check_improve(ch,gsn_bash,FALSE,1);
	ch->position = POS_RESTING;
	WAIT_STATE(ch,skill_beats(gsn_bash,ch) * 3/2);
    }
    check_killer(ch,victim);
}

void do_dirt( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    float chance;

    one_argument(argument,arg);

    if ( !has_skill_available(ch,gsn_dirt)
    ||   (IS_NPC(ch) && !STR_IS_SET(ch->strbit_off_flags,OFF_KICK_DIRT))
    ||   (!IS_NPC(ch)
    &&    ch->level < skill_level(gsn_dirt,ch)))
    {
	send_to_char("You get your feet dirty.\n\r",ch);
	return;
    }

    if (IS_AFFECTED(ch,EFF_CALM)) {
	send_to_char("You're feeling too mellow.\n\r",ch);
	return;
    }

    if (arg[0] == '\0')
    {
	victim = ch->fighting;
	if (victim == NULL)
	{
	    send_to_char("But you aren't in combat!\n\r",ch);
	    return;
	}
    }

    else if ((victim = get_char_room(ch,arg)) == NULL)
    {
	send_to_char("They aren't here.\n\r",ch);
	return;
    }

    if (IS_AFFECTED(victim,EFF_BLIND))
    {
	act("$E's already been blinded.",ch,NULL,victim,TO_CHAR);
	return;
    }

    if (victim == ch)
    {
	send_to_char("Very funny.\n\r",ch);
	return;
    }

    if (is_safe(ch,victim))
	return;

    if (MOB_HAS_TRIGGER(victim,MTRIG_PREATTACK) &&
	ch->fighting!=victim &&
	!mp_preattack_trigger(victim,ch,"dirtkick"))
	return;

    if (!rp_preattack_trigger(ch,victim,"dirtkick"))
	return;

    if (!allow_killstealing(ch,victim))
	return;

    if (IS_AFFECTED(ch,EFF_CHARM) && ch->master == victim)
    {
	act("But $N is such a good friend!",ch,NULL,victim,TO_CHAR);
	return;
    }

    /* modifiers */
    chance=100;

    /* dexterity */
    chance *= 1+ get_curr_stat(ch,STAT_DEX)*0.01;
    chance /= 1+ 2*get_curr_stat(victim,STAT_DEX)*0.01;

    /* speed  */
    if (STR_IS_SET(ch->strbit_off_flags,OFF_FAST) || IS_AFFECTED(ch,EFF_HASTE))
	chance *= 1.1;
    if (STR_IS_SET(victim->strbit_off_flags,OFF_FAST) || IS_AFFECTED(victim,EFF_HASTE))
	chance *= 0.75;

    /* level */
    chance *= 1+ (ch->level - victim->level) * 2*0.01;

    /* terrain */

    switch(ch->in_room->sector_type)
    {
	case(SECT_INSIDE):		chance *= 0.8;	break;
	case(SECT_CITY):		chance *= 0.9;	break;
	case(SECT_FIELD):		chance *= 1.05;	break;
	case(SECT_FOREST):				break;
	case(SECT_HILLS):				break;
	case(SECT_MOUNTAIN):		chance *= 0.9;	break;
	case(SECT_WATER_SWIM):		chance  =  0;	break;
	case(SECT_WATER_NOSWIM):	chance  =  0;	break;
	case(SECT_WATER_BELOW):		chance  =  0;	break;
	case(SECT_AIR):			chance  =  0;  	break;
	case(SECT_DESERT):		chance *= 1.1;   break;
    }

    if (chance == 0)
    {
	send_to_char("There isn't any dirt to kick.\n\r",ch);
	return;
    }

    if (!IS_SET(victim->parts,PART_EYE)) {
	act("You kick dirt in $S face.",ch,NULL,victim,TO_CHAR);
	act("$n kicks dirt in your face.",ch,NULL,victim,TO_VICT);
	return;
    }

    /* now the attack */
    if (skillcheck2(ch,gsn_dirt,chance))
    {
	EFFECT_DATA ef;

	act("$n is blinded by the dirt in $s eyes!",victim,NULL,NULL,TO_ROOM);
	act("$n kicks dirt in your eyes!",ch,NULL,victim,TO_VICT);
        damage(ch,victim,number_range(2,5),gsn_dirt,DAM_NONE,FALSE, NULL);
	send_to_char("You can't see a thing!\n\r",victim);
	check_improve(ch,gsn_dirt,TRUE,2);
	WAIT_STATE(ch,skill_beats(gsn_dirt,ch));

	ZEROVAR(&ef,EFFECT_DATA);
	ef.where	= TO_EFFECTS;
	ef.type 	= gsn_dirt;
	ef.level 	= ch->level;
	ef.duration	= 0;
	ef.location	= APPLY_HITROLL;
	ef.modifier	= -4;
	ef.bitvector    = EFF_BLIND;
	ef.casted_by	= ch?ch->id:0;
	effect_to_char(victim,&ef);
    }
    else
    {
	damage(ch,victim,0,gsn_dirt,DAM_NONE,TRUE, NULL);
	check_improve(ch,gsn_dirt,FALSE,2);
	WAIT_STATE(ch,skill_beats(gsn_dirt,ch));
    }
    check_killer(ch,victim);
}

void do_trip( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    float chance;

    one_argument(argument,arg);

    if ( !has_skill_available(ch,gsn_trip)
    ||   (IS_NPC(ch) && !STR_IS_SET(ch->strbit_off_flags,OFF_TRIP))
    ||   (!IS_NPC(ch)
	  && ch->level < skill_level(gsn_trip,ch)))
    {
	send_to_char("Tripping?  What's that?\n\r",ch);
	return;
    }

    if (IS_AFFECTED(ch,EFF_CALM)) {
	send_to_char("You're feeling too mellow.\n\r",ch);
	return;
    }


    if (arg[0] == '\0')
    {
	victim = ch->fighting;
	if (victim == NULL)
	{
	    send_to_char("But you aren't fighting anyone!\n\r",ch);
	    return;
 	}
    }

    else if ((victim = get_char_room(ch,arg)) == NULL)
    {
	send_to_char("They aren't here.\n\r",ch);
	return;
    }

    if (is_safe(ch,victim))
	return;

    if (MOB_HAS_TRIGGER(victim,MTRIG_PREATTACK) &&
	ch->fighting!=victim &&
	!mp_preattack_trigger(victim,ch,"trip"))
	return;

    if (!rp_preattack_trigger(ch,victim,"trip"))
	return;

    if (!allow_killstealing(ch,victim))
	return;

    if (IS_AFFECTED(victim,EFF_FLYING))
    {
	act("$S feet aren't on the ground.",ch,NULL,victim,TO_CHAR);
	return;
    }

    if (victim->position < POS_FIGHTING)
    {
	act("$N is already down.",ch,NULL,victim,TO_CHAR);
	return;
    }

    if (victim == ch)
    {
	send_to_char("You fall flat on your face!\n\r",ch);
	WAIT_STATE(ch,2 * skill_beats(gsn_trip,ch));
	act("$n trips over $s own feet!",ch,NULL,NULL,TO_ROOM);
	return;
    }

    if (IS_AFFECTED(ch,EFF_CHARM) && ch->master == victim)
    {
	act("$N is your beloved master.",ch,NULL,victim,TO_CHAR);
	return;
    }

    chance=100;
    /* modifiers */

    /* size */
    if (ch->size < victim->size)
        chance *= 1+ (ch->size - victim->size) *0.1;  /* bigger = harder to trip */

    /* dex */
    chance *= 1+ get_curr_stat(ch,STAT_DEX)*0.01;
    chance /= 1+ get_curr_stat(victim,STAT_DEX) * 0.015;

    /* speed */
    if (STR_IS_SET(ch->strbit_off_flags,OFF_FAST) || IS_AFFECTED(ch,EFF_HASTE))
	chance *= 1.1;
    if (STR_IS_SET(victim->strbit_off_flags,OFF_FAST) || IS_AFFECTED(victim,EFF_HASTE))
	chance *= 0.8;

    /* level */
    chance *= 1+ (ch->level - victim->level) * 2*0.01;


    /* now the attack */
    if (skillcheck2(ch,gsn_trip,chance))
    {
	act("$n trips you and you go down!",ch,NULL,victim,TO_VICT);
	act("You trip $N and $E goes down!",ch,NULL,victim,TO_CHAR);
	act("$n trips $N, sending $M to the ground.",ch,NULL,victim,TO_NOTVICT);
	check_improve(ch,gsn_trip,TRUE,1);

	DAZE_STATE(victim,2 * PULSE_VIOLENCE);
        WAIT_STATE(ch,skill_beats(gsn_trip,ch));
	victim->position = POS_RESTING;
	damage(ch,victim,number_range(2, 2 +  2 * victim->size),gsn_trip,
	    DAM_BASH,TRUE, NULL);
    }
    else
    {
	damage(ch,victim,0,gsn_trip,DAM_BASH,TRUE, NULL);
	WAIT_STATE(ch,skill_beats(gsn_trip,ch)*2/3);
	check_improve(ch,gsn_trip,FALSE,1);
    }
	check_killer(ch,victim);
}

void do_tail( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    int dam;
    float chance;

    if(!IS_NPC(ch)) return;   /* Only for NPC at the moment */

    one_argument(argument,arg);

    if(!IS_SET(ch->parts,PART_TAIL))
    {
	send_to_char("You don't have a tail to use.\n\r",ch);
	return;
    }

    if ( !has_skill_available(ch,gsn_tail)
    ||   (IS_NPC(ch) && !STR_IS_SET(ch->strbit_off_flags,OFF_TAIL))
    ||   (!IS_NPC(ch)
	  && ch->level < skill_level(gsn_tail,ch)))
    {
	send_to_char("You don't know how to use your tail?\n\r",ch);
	return;
    }

    if (IS_AFFECTED(ch,EFF_CALM)) {
	send_to_char("You're feeling too mellow.\n\r",ch);
	return;
    }

    if (arg[0] == '\0')
    {
	victim = ch->fighting;
	if (victim == NULL)
	{
	    send_to_char("But you aren't fighting anyone!\n\r",ch);
	    return;
 	}
    }

    else if ((victim = get_char_room(ch,arg)) == NULL)
    {
	send_to_char("They aren't here.\n\r",ch);
	return;
    }

    if (is_safe(ch,victim))
	return;

    if (MOB_HAS_TRIGGER(victim,MTRIG_PREATTACK) &&
	ch->fighting!=victim &&
	!mp_preattack_trigger(victim,ch,"tail"))
	return;

    if (!rp_preattack_trigger(ch,victim,"tail"))
	return;

    if (!allow_killstealing(ch,victim))
	return;

    if (victim == ch)
    {
	send_to_char("You slap your tail into your own face!\n\r",ch);
	WAIT_STATE(ch,2 * skill_beats(gsn_tail,ch));
	act("$n slaps $s tail into $s own face!",ch,NULL,NULL,TO_ROOM);
	return;
    }

    if (IS_AFFECTED(ch,EFF_CHARM) && ch->master == victim)
    {
	act("$N is your beloved master.",ch,NULL,victim,TO_CHAR);
	return;
    }

    /* modifiers */
    chance=100;

    /* dex */
    chance *= 1+ get_curr_stat(ch,STAT_DEX)*0.01;
    chance /= 1+ get_curr_stat(victim,STAT_DEX) *0.015;

    /* speed */
    if (STR_IS_SET(ch->strbit_off_flags,OFF_FAST) || IS_AFFECTED(ch,EFF_HASTE))
	chance *= 1.1;
    if (STR_IS_SET(victim->strbit_off_flags,OFF_FAST) || IS_AFFECTED(victim,EFF_HASTE))
	chance *= 0.8;

    /* level */
    chance *= 1+ (ch->level - victim->level) * 2*0.01;

    /* now the attack */
    if (skillcheck2(ch,gsn_tail,chance)) {
	act("$n slaps you with $s tail!",ch,NULL,victim,TO_VICT);
	act("You slap $N with your tail!",ch,NULL,victim,TO_CHAR);
	act("$n slaps $N with $s tail.",ch,NULL,victim,TO_NOTVICT);
	check_improve(ch,gsn_tail,TRUE,1);

        WAIT_STATE(ch,skill_beats(gsn_trip,ch));

	dam=number_range(4,ch->level+10);
	switch(ch->size) {
	  case SIZE_TINY: dam/=4;break;
	  case SIZE_SMALL: dam/=2;break;
	  case SIZE_MEDIUM: break;
	  case SIZE_LARGE: dam=(dam*3)/2;
	  case SIZE_HUGE: dam*=2;
	  case SIZE_GIANT: dam*=3;
	};

	damage(ch,victim,number_range(1,ch->level+10),gsn_tail,
	    DAM_BASH,TRUE, NULL);
    }
    else
    {
	damage(ch,victim,0,gsn_tail,DAM_BASH,TRUE, NULL);
	WAIT_STATE(ch,skill_beats(gsn_trip,ch)*2/3);
	check_improve(ch,gsn_tail,FALSE,1);
    }

    check_killer(ch,victim);
}

void do_kill( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;

    one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Kill whom?\n\r", ch );
	return;
    }

    if ( ( victim = get_char_room( ch, arg ) ) == NULL )
    {
	send_to_char( "They aren't here.\n\r", ch );
	return;
    }
/*  Allow player killing
    if ( !IS_NPC(victim) )
    {
        if ( !IS_SET(victim->strbit_act, PLR_KILLER)
        &&   !IS_SET(victim->strbit_act, PLR_THIEF) )
        {
            send_to_char( "You must MURDER a player.\n\r", ch );
            return;
        }
    }
*/

    if (IS_AFFECTED(ch,EFF_CALM)) {
	send_to_char("You're feeling too mellow.\n\r",ch);
	return;
    }

    if ( victim == ch )
    {
	send_to_char( "You hit yourself.  Ouch!\n\r", ch );
	multi_hit( ch, ch, TYPE_UNDEFINED );
	return;
    }

    if ( is_safe( ch, victim ) )
	return;

    if (!allow_killstealing(ch,victim))
	return;

    if ( IS_AFFECTED(ch, EFF_CHARM) && ch->master == victim )
    {
	act( "$N is your beloved master.", ch, NULL, victim, TO_CHAR );
	return;
    }

    if ( ch->position == POS_FIGHTING )
    {
	send_to_char( "You do the best you can!\n\r", ch );
	return;
    }

    if (!op_preattack_trigger(ch,victim,"kill") ||
	!rp_preattack_trigger(ch,victim,"kill"))
	return;
    if (MOB_HAS_TRIGGER(victim,MTRIG_PREATTACK) &&
	ch->fighting!=victim &&
	!mp_preattack_trigger(victim,ch,"kill"))
	return;

    if ( IS_PC(ch) && IS_NPC(victim) ) {
	int ool_level,ool_timeout;
	if (GetCharProperty(victim,PROPERTY_INT,"ool_level",&ool_level)) {
	    if (ool_level>ch->level+10) {
		char buf1[MSL];
		if (!GetCharProperty(victim,PROPERTY_STRING,"ool_char",&buf1))
		   strcpy(buf1,"someone");
		else {
		    CHAR_DATA *wch;

		    for (wch = char_list; wch!=NULL; wch=wch->next) {
			if (!strcmp(buf1,wch->name)) {
			    ool_penalize(wch);
			    break;
			}
		    }
		}
		logf("[0] Possible OOL detected: %s[%d] is attacking %s[%d] "
		    "after %s[%d] did.",
		    ch->name,ch->level,
		    victim->pIndexData->short_descr,
		    victim->level,buf1,ool_level);
		wiznet(WIZ_OOL_DETECTS,0,ch,NULL,
		    "Possible OOL detected: $N[%d] is attacking %s[%d] after "
		    "%s[%d] did.",
		    ch->level,
		    victim->pIndexData->short_descr,
		    victim->level,buf1,ool_level);
	    }
	}
	SetCharProperty(victim,PROPERTY_INT,"ool_level",&ch->level);
	SetCharProperty(victim,PROPERTY_STRING,"ool_char",ch->name);
	ool_timeout=100;
	SetCharProperty(victim,PROPERTY_INT,"ool_timeout",&ool_timeout);
    }

    WAIT_STATE( ch, 1 * PULSE_VIOLENCE );
    check_killer( ch, victim );
    multi_hit( ch, victim, TYPE_UNDEFINED );

    return;
}



void do_murde( CHAR_DATA *ch, char *argument )
{
    send_to_char( "If you want to MURDER, spell it out.\n\r", ch );
    return;
}



void do_murder( CHAR_DATA *ch, char *argument )
{
    char buf[MAX_STRING_LENGTH];
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;

    one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Murder whom?\n\r", ch );
	return;
    }

    if (IS_AFFECTED(ch,EFF_CALM)) {
	send_to_char("You're feeling too mellow.\n\r",ch);
	return;
    }

    if (IS_AFFECTED(ch,EFF_CHARM) || (IS_NPC(ch) && STR_IS_SET(ch->strbit_act,ACT_PET)))
	return;

    if ( ( victim = get_char_room( ch, arg ) ) == NULL )
    {
	send_to_char( "They aren't here.\n\r", ch );
	return;
    }

    if ( victim == ch )
    {
	send_to_char( "Suicide is a mortal sin.\n\r", ch );
	return;
    }

    if ( is_safe( ch, victim ) )
	return;

    if (!allow_killstealing(ch,victim))
	return;

    if ( IS_AFFECTED(ch, EFF_CHARM) && ch->master == victim )
    {
	act( "$N is your beloved master.", ch, NULL, victim, TO_CHAR );
	return;
    }

    if ( ch->position == POS_FIGHTING )
    {
	send_to_char( "You do the best you can!\n\r", ch );
	return;
    }

    if (!op_preattack_trigger(ch,victim,"murder") ||
	!rp_preattack_trigger(ch,victim,"murder"))
	return;
    if (MOB_HAS_TRIGGER(victim,MTRIG_PREATTACK) &&
	ch->fighting!=victim &&
	!mp_preattack_trigger(victim,ch,"murder"))
	return;

    WAIT_STATE( ch, 1 * PULSE_VIOLENCE );
    if (IS_NPC(ch))
	sprintf(buf, "Help! I am being attacked by %s!",ch->short_descr);
    else
    	sprintf( buf, "Help!  I am being attacked by %s!", ch->name );
    do_function(victim, &do_yell, buf );
    check_killer( ch, victim );
    multi_hit( ch, victim, TYPE_UNDEFINED );
    return;
}



void do_backstab( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    OBJ_DATA *obj;
    int base_chance;

    one_argument( argument, arg );

    if (arg[0] == '\0') {
        send_to_char("Backstab whom?\n\r",ch);
        return;
    }

    if (IS_AFFECTED(ch,EFF_CALM)) {
	send_to_char("You're feeling too mellow.\n\r",ch);
	return;
    }

    if (ch->fighting != NULL) {
	send_to_char("You're facing the wrong end.\n\r",ch);
	return;
    }

    if ((victim = get_char_room(ch,arg)) == NULL) {
        send_to_char("They aren't here.\n\r",ch);
        return;
    }

    if (victim->fighting==ch) {
	act("You are already fighting with $N.",ch,NULL,victim,TO_CHAR);
	return;
    }

    if ( victim == ch ) {
	send_to_char( "How can you sneak up on yourself?\n\r", ch );
	return;
    }

    if ( is_safe( ch, victim ) )
      return;

    if (!allow_killstealing(ch,victim))
	return;

    if ( ( obj = get_eq_char( ch, WEAR_WIELD ) ) == NULL)
    {
	send_to_char( "You need to wield a weapon to backstab.\n\r", ch );
	return;
    }

    if (obj->value[0]!=WEAPON_EXOTIC &&
	obj->value[0]!=WEAPON_SWORD &&
	obj->value[0]!=WEAPON_DAGGER &&
	obj->value[0]!=WEAPON_SPEAR &&
	obj->value[0]!=WEAPON_AXE ) {
	act("You realize your $p isn't sharp enough for this.\n\r",
	    ch,obj,NULL,TO_CHAR);
	return;
    }

    base_chance=120*victim->hit/victim->max_hit*victim->hit/victim->max_hit;
    if (victim->fighting) base_chance/=2;

    if (!op_preattack_trigger(ch,victim,"backstab") ||
	!rp_preattack_trigger(ch,victim,"backstab"))
	return;
    if (MOB_HAS_TRIGGER(victim,MTRIG_PREATTACK) &&
	ch->fighting!=victim &&
	!mp_preattack_trigger(victim,ch,"backstab"))
	return;

    check_killer( ch, victim );
    WAIT_STATE( ch, skill_beats(gsn_backstab,ch) );
    if ( skillcheck2(ch,gsn_backstab,base_chance)
    || ( get_skill(ch,gsn_backstab) >= 2 && !IS_AWAKE(victim) ) )
    {
	check_improve(ch,gsn_backstab,TRUE,1);
	multi_hit( ch, victim, gsn_backstab );
    }
    else
    {
	check_improve(ch,gsn_backstab,FALSE,1);
	damage( ch, victim, 0, gsn_backstab,DAM_NONE,TRUE, NULL);
    }

    return;
}



void do_flee( CHAR_DATA *ch, char *argument )
{
    ROOM_INDEX_DATA *was_in;
    ROOM_INDEX_DATA *now_in;
    OBJ_INDEX_DATA *obj_index;
    OBJ_DATA *obj;
    CHAR_DATA *victim;
    int attempt;
    char arg[MSL];
    int try_door;

    one_argument(argument,arg);
    if ( ( victim = ch->fighting ) == NULL && str_cmp(arg,"auto")) {
        if ( ch->position == POS_FIGHTING )
            ch->position = POS_STANDING;
	send_to_char( "You aren't fighting anyone.\n\r", ch );
	return;
    }

    if (ch->move==0) {
	act("You're too exhausted from the fight.",ch,NULL,NULL,TO_CHAR);
	act("$n tries to get away from the fight but $e is too exhausted.",
	    ch,NULL,ch->fighting,TO_NOTVICT);
	act("$n tries to escape but $e can't get away!",
	    ch,NULL,ch->fighting,TO_VICT);
	return;
    }

    if (is_affected(ch,gsn_entangle)) {
	if (number_percent() <
	    50-8*(ch->perm_stat[STAT_STR]-get_curr_stat(ch,STAT_STR))) {

	    send_to_char("You break the twines, you're free!\n\r",ch);
	    act("$n breaks the twines which are holding $m.\n\r",
		ch,NULL,NULL,TO_ROOM);
	    ch->move=(ch->move*50)/100;
	    obj_index=get_obj_index( OBJ_VNUM_TENDRILS );
	    obj = create_object( obj_index, 0 );
	    obj_to_room(obj,ch->in_room);
	    obj->timer=number_range(3,5);
	    effect_strip(ch,gsn_entangle);
	    op_load_trigger(obj,NULL);

	} else {
	    send_to_char(
		"You try to flee, but the twines seem to hold you!\n\r",ch);
	    act("$n tries to flee, but is held by the twines.",
		ch,NULL,NULL,TO_ROOM);
	    ch->move=(ch->move*20)/100;
	    WAIT_STATE(ch,8);
	    return;
	}
    }

    // see if person can flee to a certain direction
    // no check_improve btw, up to 75% sounds reasonable.
    try_door=-1;
    if (arg[0])
	try_door=
	    (skillcheck(ch,gsn_flee)) ? -1 : get_direction(arg);

    was_in = ch->in_room;
    for ( attempt = 0; attempt < DIR_MAX; attempt++ )
    {
	EXIT_DATA *pexit;
	int door;

	if (try_door<0)
	    door = number_door( );
	else
	    door = try_door;

	if ( ( pexit = was_in->exit[door] ) == 0
	||   pexit->to_room == NULL
	||   IS_SET(pexit->exit_info, EX_CLOSED)
	||   number_range(0,ch->daze) != 0
	|| ( IS_NPC(ch)
	&&   STR_IS_SET(pexit->to_room->strbit_room_flags, ROOM_NO_MOB) ) )
	    continue;

	mp_flee_trigger(ch);

	stop_fighting( ch, TRUE );
	move_char( ch, door, FALSE, TRUE );
	if ( ( now_in = ch->in_room ) == was_in )
	    continue;

	ch->in_room = was_in;
	act( "$n has fled!", ch, NULL, NULL, TO_ROOM );
	ch->in_room = now_in;

	if ( !IS_NPC(ch) )
	{
	    send_to_char( "You flee from combat!\n\r", ch );
	    if( (ch->class == 2)
		&& (number_percent() < (3*ch->level/2) ) )
		send_to_char( "You snuck away safely.\n\r", ch);
	    else {
		send_to_char( "You lost 10 exp.\n\r", ch);
		gain_exp( ch, -10, TRUE );
	    }
	}

	/* Make NPC hunt PC (check for victim!=NULL first because of autoflee */
	if ( victim && IS_NPC(victim) && !IS_NPC(ch) && STR_IS_SET(victim->strbit_off_flags, OFF_HUNTER) ) {
	   victim->hunt_type = HUNT_KILL;
	   victim->hunt_id = ch->id;
	}
	return;
    }

    send_to_char( "PANIC! You couldn't escape!\n\r", ch );
    return;
}


void rescue( CHAR_DATA *ch, CHAR_DATA *victim ) {
    CHAR_DATA *fch;

    if ( victim == ch )
    {
	send_to_char( "What about fleeing instead?\n\r", ch );
	return;
    }

    // is_same_group is for checking that you can rescue your pet(s)
    if ( !IS_NPC(ch) && IS_NPC(victim) && !is_same_group(ch,victim))
    {
	send_to_char( "Doesn't need your help!\n\r", ch );
	return;
    }

    if ( ch->fighting == victim )
    {
	send_to_char( "Too late.\n\r", ch );
	return;
    }

    if ( ( fch = victim->fighting ) == NULL )
    {
	send_to_char( "That person is not fighting right now.\n\r", ch );
	return;
    }

    if( ch->fighting && ch->fighting!=fch )
    {
	send_to_char( "You are already fighting someone else.\n\r", ch );
	return;
    }

    if (!allow_killstealing(ch,fch))
	return;

    if (!ch->fighting) {
	if (!op_preattack_trigger(ch,victim,"rescue") ||
	    !rp_preattack_trigger(ch,victim,"rescue"))
	    return;
	if (MOB_HAS_TRIGGER(victim,MTRIG_PREATTACK) &&
	    !mp_preattack_trigger(victim,ch,"rescue"))
	    return;
    }

    WAIT_STATE( ch, skill_beats(gsn_rescue,ch) );
    if (!skillcheck(ch,gsn_rescue))
    {
	send_to_char( "You fail the rescue.\n\r", ch );
	check_improve(ch,gsn_rescue,FALSE,1);
	return;
    }

    act( "You rescue $N!",  ch, NULL, victim, TO_CHAR    );
    act( "$n rescues you!", ch, NULL, victim, TO_VICT    );
    act( "$n rescues $N!",  ch, NULL, victim, TO_NOTVICT );
    check_improve(ch,gsn_rescue,TRUE,1);

    if(ch->fighting) stop_fighting( ch, FALSE );
    stop_fighting( fch, FALSE );
    stop_fighting( victim, FALSE );

    check_killer( ch, fch );
    set_fighting( ch, fch );
    set_fighting( fch, ch );
    return;
}

void do_rescue( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;

    one_argument( argument, arg );
    if ( arg[0] == '\0' )
    {
	send_to_char( "Rescue whom?\n\r", ch );
	return;
    }

    if ( ( victim = get_char_room( ch, arg ) ) == NULL )
    {
	send_to_char( "They aren't here.\n\r", ch );
	return;
    }

    rescue(ch,victim);
}



void do_kick( CHAR_DATA *ch, char *argument )
{
    CHAR_DATA *victim;

    if ( !IS_NPC(ch)
    &&   ch->level < skill_level(gsn_kick,ch))
    {
	send_to_char(
	    "You better leave the martial arts to fighters.\n\r", ch );
	return;
    }

    if (IS_NPC(ch) && !STR_IS_SET(ch->strbit_off_flags,OFF_KICK))
	return;

    if ( ( victim = ch->fighting ) == NULL )
    {
	send_to_char( "You aren't fighting anyone.\n\r", ch );
	return;
    }

    if (IS_AFFECTED(ch,EFF_CALM)) {
	send_to_char("You're feeling too mellow.\n\r",ch);
	return;
    }

    WAIT_STATE( ch, skill_beats(gsn_kick,ch));
    if ( skillcheck(ch,gsn_kick))
    {
	damage(ch,victim,number_range( 1, ch->level ), gsn_kick,DAM_BASH,TRUE, NULL);
	check_improve(ch,gsn_kick,TRUE,1);
    }
    else
    {
	damage( ch, victim, 0, gsn_kick,DAM_BASH,TRUE, NULL);
	check_improve(ch,gsn_kick,FALSE,1);
    }
    check_killer(ch,victim);
    return;
}



/*
 * Disarm a creature.
 * Caller must check for successful attack.
 */
void disarm( CHAR_DATA *ch, CHAR_DATA *victim )
{
    OBJ_DATA *obj,*sec;

    if ( ( obj = get_eq_char( victim, WEAR_WIELD ) ) == NULL )
	return;

    if ( IS_OBJ_STAT(obj,ITEM_NOREMOVE))
    {
	act("$S weapon won't budge!",ch,NULL,victim,TO_CHAR);
	act("$n tries to disarm you, but your weapon won't budge!",
	    ch,NULL,victim,TO_VICT);
	act("$n tries to disarm $N, but fails.",ch,NULL,victim,TO_NOTVICT);
	return;
    }

    act( "$n DISARMS you and sends your weapon flying!",
	 ch, NULL, victim, TO_VICT    );
    act( "You disarm $N!",  ch, NULL, victim, TO_CHAR    );
    act( "$n disarms $N!",  ch, NULL, victim, TO_NOTVICT );

    obj_from_char( obj );
    if ( IS_OBJ_STAT(obj,ITEM_NODROP) || IS_OBJ_STAT(obj,ITEM_INVENTORY) )
	obj_to_char( obj, victim );
    else
    {
	obj_to_room( obj, victim->in_room );
	if (IS_NPC(victim) && victim->wait == 0 && can_see_obj(victim,obj)) {
	    get_obj(victim,obj,NULL);
	    wear_obj( victim, obj, FALSE, TRUE);
	    return;
	    /* Don't bother about secondary weapons if the mob has
	       re-equiped himself */
	} else
	    if (obj->in_room->sector_type==SECT_AIR)
		object_drop_air(obj);
	    else if (obj->in_room->sector_type==SECT_WATER_NOSWIM ||
		     obj->in_room->sector_type==SECT_WATER_SWIM ||
		     obj->in_room->sector_type==SECT_WATER_BELOW )
		object_drop_water(obj);
	    else if (number_percent()<10)
		object_drop_fall(obj);
    }

    sec=get_eq_char(victim,WEAR_SECONDARY);
    if(sec)
    {
	unequip_char( victim, sec);
	equip_char( victim, sec, WEAR_WIELD );
	act( "$n now uses $p as primary weapon.",victim,sec,NULL,TO_ROOM);
	act( "You now use $p as primary weapon.",victim,sec,NULL,TO_CHAR);
    }

    return;
}

void do_disarm( CHAR_DATA *ch, char *argument )
{
    CHAR_DATA *victim;
    OBJ_DATA *obj;
    int hth,ch_weapon,vict_weapon,ch_vict_weapon;
    float chance;

    hth = 0;

    if (!has_skill_available(ch,gsn_disarm)) {
	send_to_char( "You don't know how to disarm opponents.\n\r", ch );
	return;
    }

    if ( get_eq_char( ch, WEAR_WIELD ) == NULL
    &&   (!has_skill_available(ch,gsn_hand_to_hand)
    ||    (IS_NPC(ch) && !STR_IS_SET(ch->strbit_off_flags,OFF_DISARM))))
    {
	send_to_char( "You must wield a weapon to disarm.\n\r", ch );
	return;
    }

    if ( ( victim = ch->fighting ) == NULL )
    {
	send_to_char( "You aren't fighting anyone.\n\r", ch );
	return;
    }

    if (!can_see(ch,victim)) {
	send_to_char("You can't see a thing!\n\r",ch);
	return;
    }

    if ( ( obj = get_eq_char( victim, WEAR_WIELD ) ) == NULL )
    {
	send_to_char( "Your opponent is not wielding a weapon.\n\r", ch );
	return;
    }

    /* find weapon skills */
    ch_weapon = get_weapon_skill(ch,get_weapon_sn(ch,FALSE));
    vict_weapon = get_weapon_skill(victim,get_weapon_sn(victim,FALSE));
    ch_vict_weapon = get_weapon_skill(ch,get_weapon_sn(victim,FALSE));
    hth = get_skill(ch,gsn_hand_to_hand);

    /* modifiers */
    chance=100;

    /* skill */
    if ( get_eq_char(ch,WEAR_WIELD) == NULL)
	chance = chance * hth*0.007;
    else
	chance = chance * ch_weapon*0.01;

    chance *= 1+ (ch_vict_weapon/2 - vict_weapon) *0.005;

    /* dex vs. strength */
    chance *= 1+ get_curr_stat(ch,STAT_DEX)*0.01;
    chance /= 1+ 2 * get_curr_stat(victim,STAT_STR)*0.01;

    /* level */
    chance *= 1+ (ch->level - victim->level) *0.02;

    /* and now the attack */
    if (skillcheck2(ch,gsn_disarm,chance)) {
    	WAIT_STATE( ch, skill_beats(gsn_disarm,ch) );
	disarm( ch, victim );
	check_improve(ch,gsn_disarm,TRUE,1);
    }
    else
    {
	WAIT_STATE(ch,skill_beats(gsn_disarm,ch));
	act("You fail to disarm $N.",ch,NULL,victim,TO_CHAR);
	act("$n tries to disarm you, but fails.",ch,NULL,victim,TO_VICT);
	act("$n tries to disarm $N, but fails.",ch,NULL,victim,TO_NOTVICT);
	check_improve(ch,gsn_disarm,FALSE,1);
    }
    check_killer(ch,victim);
    return;
}



void do_sla( CHAR_DATA *ch, char *argument )
{
    send_to_char( "If you want to SLAY, spell it out.\n\r", ch );
    return;
}



void do_slay( CHAR_DATA *ch, char *argument )
{
    CHAR_DATA *victim;
    char arg[MAX_INPUT_LENGTH];

    one_argument( argument, arg );
    if ( arg[0] == '\0' )
    {
	send_to_char( "Slay whom?\n\r", ch );
	return;
    }

    if ( ( victim = get_char_room( ch, arg ) ) == NULL )
    {
	send_to_char( "They aren't here.\n\r", ch );
	return;
    }

    if ( ch == victim )
    {
	send_to_char( "Suicide is a mortal sin.\n\r", ch );
	return;
    }

    if ( !IS_NPC(victim) && victim->level >= get_trust(ch) )
    {
	send_to_char( "You failed.\n\r", ch );
	return;
    }

    act( "You slay $M in cold blood!",  ch, NULL, victim, TO_CHAR    );
    act( "$n slays you in cold blood!", ch, NULL, victim, TO_VICT    );
    act( "$n slays $N in cold blood!",  ch, NULL, victim, TO_NOTVICT );
    raw_kill( victim, ch );
    return;
}

void do_surrender( CHAR_DATA *ch, char *argument ) {
    CHAR_DATA *mob;

    //
    // rules of the game:
    // - surrender of NPC to PC: works
    // - surrender of NPC to NPC: works
    // - surrender of PC to PC: not working
    // - surrender of PC to NPC: works if mob has surrender trigger
    //

    if ( (mob = ch->fighting) == NULL ) {
	send_to_char( "But you're not fighting!\n\r", ch );
	return;
    }

    act( "You surrender to $N!", ch, NULL, mob, TO_CHAR );
    act( "$n surrenders to you!", ch, NULL, mob, TO_VICT );
    act( "$n tries to surrender to $N!", ch, NULL, mob, TO_NOTVICT );
    stop_fighting( ch, TRUE );

    if (IS_NPC(ch) && IS_NPC(mob))
	return;
    if (IS_NPC(ch) && IS_PC(mob))
	return;

    if (IS_PC(ch) && IS_PC(mob)) {
	act("$N seems to ignore your cowardly act!",ch,NULL,mob,TO_CHAR);
	multi_hit(mob,ch,TYPE_UNDEFINED);
	return;
    }

    if (IS_PC(ch) && IS_NPC(mob)) {
	if (!MOB_HAS_TRIGGER(mob,MTRIG_SURRENDER)) {
	    act("$N seems to ignore your cowardly act!",ch,NULL,mob,TO_CHAR);
	    multi_hit( mob, ch, TYPE_UNDEFINED );
	    return;
	}
	if (mp_surrender_trigger(mob,ch))
	    return;
	if (!IS_VALID(mob))
	    return;

	act("$N seems to ignore your cowardly act!",ch,NULL,mob,TO_CHAR);
	multi_hit( mob, ch, TYPE_UNDEFINED );
	return;
    }
}


#ifdef DO_NOT_COMPILE

merc.h:#define EFF_WARCRY               (ee)
merc.h:extern int  gsn_warcry;
const.c:    {
const.c:        "warcry",               { 90, 90, 90, 90 },     { 1, 1, 1, 1},
const.c:        spell_null,             TAR_IGNORE,             POS_STANDING,
const.c:        &gsn_warcry,            SLOT( 0),       0,      2,
const.c:        "warcry",               "Your ears stop buzzing.",""
const.c:    }
db.c:int                     gsn_warcry;
interp.c:    { "warcry",                do_warcry,      POS_STANDING,    0,  LOG_NORMAL, 1 },
tables.c:    {  "warcry",               EFF_WARCRY,             TRUE    },

void do_warcry( CHAR_DATA *ch, char *argument ) {
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim,*next_victim;
    int chance;
    int fleed=0;
    int lastexit,j;
    EXIT_DATA *pexit;
    EFFECT_DATA ef;
    int	sn_warcry=gsn_warcry;

    if (sn_warcry<0) {
	send_to_char("Please note a bug to the immortals complaining that the warcry isn't recognized\n\r",ch);
	return;
    }

    one_argument(argument,arg);

#warning: needs changing to skillcheck and others
    if ( (chance = get_skill(ch,gsn_warcry)) == 0
    ||   (!IS_NPC(ch)
	  && ch->level < skill_level(gsn_warcry,ch))) {
	act("You start a load roar but your voice breaks...",ch,NULL,NULL,TO_CHAR);
	act("$n produces a high-pitched roar.",ch,NULL,NULL,TO_ROOM);
	check_social( ch, "blush","");
	return;
    }

    if (ch->in_room->people->next_in_room==NULL) {
	act("You clear your throat and you roar like hell. Too bad there is nobody in\n\rthis room who could be impressed by it.",ch,NULL,NULL,TO_CHAR);
	return;
    }
    act("You clear your throat and you roar like hell.",ch,NULL,NULL,TO_CHAR);

    lastexit=0;
    next_victim=ch->in_room->people->next_in_room;
    ch->position=POS_FIGHTING;
    for (victim=ch->in_room->people; victim!=NULL; victim=next_victim) {
	next_victim=victim->next_in_room;
	if (ch->in_room!=victim->in_room) continue; /* ever wondered why this */
						    /* keeps going wrong???   */
	if (victim==ch) continue;

	if (victim->position<POS_SLEEPING) {
	    act("This definitly doesn't improve your condition...",victim,NULL,NULL,TO_CHAR);
	    continue;
	}
	if (victim->position==POS_SLEEPING) {
	    act_new("AAAAAAAAAAAARGH! A terrible scream wakes you up...",victim,NULL,NULL,TO_CHAR,POS_SLEEPING);
	    victim->position=POS_STANDING;
	}
	act("$n produces a terrible sound which scares the hell out of you!",ch,NULL,victim,TO_VICT);

	/* How to suffer from a warcry:
	 * - do not save vs sound
	 * - have no clue what the sound is or be too late with recognizing it
	 * - haven't experienced it before
	 */
	if ( saves_spell(ch->level,victim,DAM_SOUND)) continue;
	if ( number_percent() < get_skill(victim,gsn_warcry)) continue;
	if ( IS_AFFECTED(victim,EFF_WARCRY)) continue;

	fleed++;
	if (victim->level<ch->level-15) {
	    for (j=lastexit; j<(lastexit+DIR_MAX); j++) {
		if ((pexit = victim->in_room->exit[j%DIR_MAX]) != NULL
		&&  pexit ->to_room != NULL
		&&  (can_see_room(victim,pexit->to_room)
		||   (IS_AFFECTED(victim,EFF_INFRARED)
		&&    !IS_AFFECTED(victim,EFF_BLIND)))
		&&  !IS_SET(pexit->exit_info,EX_CLOSED)) {
		    ROOM_INDEX_DATA *from_room=victim->in_room;
		    lastexit=j%DIR_MAX;

		    act("$N flees when $E hears the scream of $n!",ch,NULL,victim,TO_NOTVICT);
		    act("$N flees when $E hears your scream!",ch,NULL,victim,TO_CHAR);
		    act("You flee when you hear $s scream!",ch,NULL,victim,TO_VICT);
		    ap_leave_trigger(victim,pexit->to_room);
		    char_from_room(victim);
		    stop_fighting( victim, TRUE );
		    char_to_room(victim,pexit->to_room);
		    ap_enter_trigger(victim,from_room);
		    act( "$n has arrived.", victim, NULL, NULL, TO_ROOM );
		    look_room(victim,victim->in_room,TRUE);
		    break;
		}
	    }
	}

	ZEROVAR(&ef,EFFECT_DATA);
	ef.where     = TO_EFFECTS;
	ef.type      = sn_warcry;
	ef.level     = ch->level;
	ef.modifier  = -ch->level/10;
	ef.duration  = ch->level/10;
	ef.bitvector = EFF_WARCRY;
	ef.casted_by = ch?ch->id:0;
	ef.location  = APPLY_HITROLL;
	effect_to_char( victim, &ef );

	ef.location  = APPLY_DAMROLL;
	effect_to_char( victim, &ef );

	if ((ch->in_room==victim->in_room)&&IS_NPC(victim))
	    multi_hit(victim,ch,TYPE_UNDEFINED);
    }
    ch->position=POS_STANDING;

    if (!fleed) {
	act(" ",ch,NULL,NULL,TO_ALL);

	for (victim=ch->in_room->people; victim!=NULL; victim=victim->next_in_room) {
	    if (victim==ch) continue;
	    switch (number_bits( 2 ) ) {
		case 0:
		    send_to_char("Actually is was very boring...\n\r",victim);
		    check_social(victim, "yawn", "");
		    break;
		default:
		    send_to_char("Actually it was very impressive...\n\r",victim);
		    check_social(victim, "applaud",ch->name);
		    break;
	    }
	}
	act(" ",ch,NULL,NULL,TO_ALL);
	send_to_char("This is not quite what you expected to happen...\n\r",ch);
	check_social(ch,"blush","");
	check_social(ch,"bow","");
    }
}
#endif

void do_attack(CHAR_DATA *ch,char *argument) {
    CHAR_DATA *victim;
    CHAR_DATA *attackedperson;
    char arg[MAX_INPUT_LENGTH];

    one_argument(argument,arg);
    if (arg[0]==0) {
	send_to_char("Whom do you want to attack?\n\r",ch);
	return;
    }
    if ((victim=get_char_room(ch,arg))==NULL) {
	send_to_char("Who is that?\n\r",ch);
	return;
    }

    attackedperson=victim->fighting;
    if (!is_same_group(ch,attackedperson)) {
	act("You're not fighting $M.",ch,NULL,victim,TO_CHAR);
	return;
    }

    if (!ch->fighting) {
	if (!op_preattack_trigger(ch,victim,"attack") ||
	    !rp_preattack_trigger(ch,victim,"attack"))
	    return;
	if (MOB_HAS_TRIGGER(victim,MTRIG_PREATTACK) &&
	    !mp_preattack_trigger(victim,ch,"attack"))
	    return;
    }

    ch->fighting=victim;
    act("You're now concentrating your efforts on $N.",ch,NULL,victim,TO_CHAR);
    act("$n changes $s attacks on $N.",ch,NULL,victim,TO_ROOM);
    act("$n moves towards you!",ch,NULL,victim,TO_VICT);

    return;
}

void do_eyepoke(CHAR_DATA *ch,char *argument) {
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    float chance;
    EFFECT_DATA ef;

    one_argument(argument,arg);

    if ( !has_skill_available(ch,gsn_eyepoke)
    ||   (!IS_NPC(ch)
    &&    ch->level < skill_level(gsn_eyepoke,ch))) {
	send_to_char("No... that's too cruel for you.\n\r",ch);
	return;
    }

    if (IS_AFFECTED(ch,EFF_CALM)) {
	send_to_char("You're feeling too mellow.\n\r",ch);
	return;
    }

    if (arg[0]==0) {
	if ((victim=ch->fighting)==NULL) {
	    send_to_char("Whom do you want to poke in the eyes?\n\r",ch);
	    return;
	}
    } else if ((victim=get_char_room(ch,arg))==NULL) {
	send_to_char("That person is not here.\n\r",ch);
	return;
    }

    if (!IS_SET(victim->parts,PART_EYE)) {
	act("But $N doesn't have any eyes!",ch,NULL,victim,TO_CHAR);
	return;
    }

    if (ch==victim) {
	send_to_char("Ouch! Yes, it still works.\n\r",ch);
	act("$n puts $s fingers before $s eyes, looks at them, and trusts them forward!",
	    ch,NULL,NULL,TO_ROOM);

	if (!is_affected(ch,gsn_eyepoke)) {
	    ZEROVAR(&ef,EFFECT_DATA);
	    ef.where        = TO_EFFECTS;
	    ef.type         = gsn_eyepoke;
	    ef.level        = ch->level;
	    ef.duration     = ch->level/10;
	    ef.location     = APPLY_HITROLL;
	    ef.modifier     = -4;
	    ef.bitvector    = EFF_BLIND;
	    ef.casted_by    = 0;
	    effect_to_char(ch,&ef);
	}
	return;
    }

    if (victim->position<POS_SLEEPING) {
	act("Just wait until $H is dead...",ch,NULL,victim,TO_CHAR);
	return;
    }

    if (is_safe(ch,victim))
	return;

    if (!allow_killstealing(ch,victim))
	return;

    if (IS_AFFECTED(ch,EFF_CHARM) && ch->master==victim) {
	act("But $N is your friend!",ch,NULL,victim,TO_CHAR);
	return;
    }

    if ((ch->size-victim->size)<-2) {	// SIZE_TINY vs SIZE_LARGE
	act("You can't get high enough to reach $S eyes!",
	    ch,NULL,victim,TO_CHAR);
	return;
    }

    if (!ch->fighting) {
	if (!op_preattack_trigger(ch,victim,"eyepoke") ||
	    !rp_preattack_trigger(ch,victim,"eyepoke"))
	    return;
	if (MOB_HAS_TRIGGER(victim,MTRIG_PREATTACK) &&
	    !mp_preattack_trigger(victim,ch,"eyepoke"))
	    return;
    }

    chance=100;
    // do something with size
    chance/= 1+ 20*abs(ch->size-victim->size)*0.01;

    // do something with dex
    chance/= 1+ (get_curr_stat(victim,STAT_DEX) * 4)*0.003;

    if (STR_IS_SET(ch->strbit_off_flags,OFF_FAST) || IS_AFFECTED(ch,EFF_HASTE))
	chance*=1.1;

    if (IS_AFFECTED(ch,EFF_BLIND))
	chance*=0.2;

    chance*=1+(ch->level-victim->level)*0.01;

    if (!IS_NPC(victim)
	&& skillcheck2(victim,gsn_dodge,10000/chance)) {
	chance/=1+ 3*(get_skill(victim,gsn_dodge)-chance)*0.01;
    }

    if (skillcheck2(ch,gsn_eyepoke,chance)) {
	act("$n pokes both $s fingers in the eyes of $N.",
	    ch,NULL,victim,TO_NOTVICT);
	act("$n pokes $s fingers in your eyes!",ch,NULL,victim,TO_VICT);
	act("You brutally push your fingers in the eyes of $N.",
	    ch,NULL,victim,TO_CHAR);
	check_improve(ch,gsn_eyepoke,TRUE,1);

	DAZE_STATE(victim, 3 * PULSE_VIOLENCE);
	WAIT_STATE(ch,skill_beats(gsn_eyepoke,ch));
	damage(ch,victim,number_range(2,2+get_current_skill(ch,gsn_eyepoke)*chance/20000),gsn_eyepoke,
	    DAM_NONE,FALSE, NULL);

	if (!is_affected(victim,gsn_eyepoke)) {
	    ef.where        = TO_EFFECTS;
	    ef.type         = gsn_eyepoke;
	    ef.level        = ch->level;
	    ef.duration     = ch->level/10;
	    ef.location     = APPLY_HITROLL;
	    ef.modifier     = -4;
	    ef.bitvector    = EFF_BLIND;
	    ef.casted_by    = 0;
	    effect_to_char(victim,&ef);
	}

    } else {
	damage(ch,victim,0,gsn_bash,DAM_NONE,FALSE, NULL);
	act("$N evades your action.",ch,NULL,victim,TO_CHAR);
	act("You just can prevent that $n puts $s fingers in your face.",
	    ch,NULL,victim,TO_VICT);
	act("$N barely escapes that $n pushes $s fingers in $S eyes.",
	    ch,NULL,victim,TO_NOTVICT);
	check_improve(ch,gsn_eyepoke,FALSE,1);
	WAIT_STATE(ch,skill_beats(gsn_eyepoke,ch)*3/2);
    }

    check_killer(ch,victim);
}

void do_scare( CHAR_DATA *ch, char *argument)
{
    CHAR_DATA *victim;
    char arg[MAX_INPUT_LENGTH];
    float chance;

    one_argument( argument, arg );
    if ( arg[0] == '\0' ) {
	send_to_char( "Scare whom?\n\r", ch );
	return;
    }

    if ( ( victim = get_char_room( ch, arg ) ) == NULL ) {
	send_to_char( "They aren't here.\n\r", ch );
	return;
    }

    if ( ch == victim ) {
	send_to_char( "Scared of yourself?\n\r", ch );
	return;
    }

    if (IS_AFFECTED(ch,EFF_CALM)) {
	send_to_char("You're feeling too mellow.\n\r",ch);
	return;
    }

    if (!IS_NPC(ch)) {
	if (!has_skill_available(ch,gsn_scare)) {
	    send_to_char( "First learn how to scare someone.\n\r",ch);
	    return;
	}

	if (ch->level < (victim->level - 8)) {
	    send_to_char( "Are you planning to scare something of that power?\n\r",ch);
	    return;
	}
    }

    if ( can_see(victim,ch)) {
	send_to_char( "First make sure your victim doesn't see you.\n\r",ch);
	return;
    }

    if ( IS_AFFECTED(victim, EFF_BLIND)) {
	send_to_char( "You can't scare someone who is blinded.\n\r",ch);
	return;
    }

    if (!is_safe(ch, victim)) {
	/*Uitrekenen wat de kans gaat worden*/
	chance   = 100;
	chance  *= 1+ (ch->level - victim->level) *3*0.01;
	chance  *= 1+ get_curr_stat(ch, STAT_DEX)*0.01;

	/*En nu AANVALLEN!*/
	if (skillcheck2(ch,gsn_scare,chance)) {
	    WAIT_STATE(ch, skill_beats(gsn_scare,ch) );
	    victim->position = POS_STANDING;
	    do_flee( victim, "auto");
	    WAIT_STATE(victim, skill_beats(gsn_scare,ch) );
	    check_improve(ch,gsn_scare,TRUE,1);
	} else {
	    WAIT_STATE(ch,skill_beats(gsn_scare,ch));
	    act("You fail to scare $N.",ch,NULL,victim,TO_CHAR);
	    act("$n tries to scare you, but you are afraid of nothing.",
		ch,NULL,victim,TO_VICT);
	    act("$n tries to scare $N, but fails.",ch,NULL,victim,TO_NOTVICT);
	    check_improve(ch,gsn_scare,FALSE,1);
	    check_social(victim,"bonk",ch->name);
	}
    }
    return;
}


// The Berserker version of kick, the headbutt is far more accurate.
// A headbutt will always inflict damage even when the skill check is
// failed. But if the check is failed the berserker will take half as
// much damage as he inflicts. If the target is wearing a helmet the
// Berserker will take equal damage when they fail their check.
// For damage purposes, the headbutt should be about 25% more harmful
// than kick.
void do_headbutt(CHAR_DATA *ch,char *arg) {
    CHAR_DATA *victim;
    OBJ_DATA *helmet;
    int dmg;

    if ( !IS_NPC(ch)
    &&   ch->level < skill_level(gsn_headbutt,ch))
    {
	send_to_char(
	    "You better leave the martial arts to fighters.\n\r", ch );
	return;
    }

    if (IS_NPC(ch) && !STR_IS_SET(ch->strbit_off_flags,OFF_HEADBUTT))
	return;

    if ( ( victim = ch->fighting ) == NULL )
    {
	send_to_char( "You aren't fighting anyone.\n\r", ch );
	return;
    }

    if (IS_AFFECTED(ch,EFF_CALM)) {
	send_to_char("You're feeling too mellow.\n\r",ch);
	return;
    }

    dmg=number_range(1,ch->level);

    WAIT_STATE( ch, skill_beats(gsn_headbutt,ch) );
    if ( skillcheck(ch,gsn_headbutt)) {
	damage(ch,victim,dmg*5/4, gsn_headbutt,DAM_BASH,TRUE, NULL);
	check_improve(ch,gsn_headbutt,TRUE,1);
    } else {
	damage( ch, victim, dmg, gsn_headbutt,DAM_BASH,TRUE, NULL);
	helmet=get_eq_char(victim,WEAR_HEAD);
	if (helmet==NULL) dmg/=2;
	damage( ch, ch, dmg, gsn_headbutt,DAM_BASH,TRUE, NULL);
	check_improve(ch,gsn_headbutt,FALSE,1);
    }
    check_killer(ch,victim);
}



// The Violent Desire is the pinnacle of a Berserkers combat skills,
// a vicious and contemptuous attack on their foes. The Violent Desire
// is enacted over 5 pulses.
// The Violent desire requires a two-handed weapon, it cannot be bludgeoning.
// Pulse 1: The Berserker impales his foe causing 125% normal hit damage.
// Pulse 2: The Berserker lifts their victim into the air with the
//	    weapon inflicting 135% normal hit damage.
// Pulse 3: The Berserker drives their opponent into the ground,
//	    still impaled, inflicting 135% normal hit damage with a 5% chance
//	    of knockout.
// Pulse 4: The Berserker wrenches their weapon from their opponent,
//	    inflicting 135% normal hit damage.
// Pulse 5: The Berserker cleaves down upon their opponent with
//	    all their weight inflicting 200% normal hit damage, with a 10%
//	    chance of KO.
// Failure at any stage will end the violent desire, the victim managing
// to escape further harm.
// Pulse 1 will drain 15 moves, pulse 2: 20 moves, pulse 3: 15 moves,
// pulse 4: 10 moves and the final, Pulse 5 will cost the Berserker
// 50 moves.  The berserker suffers a 5 pulse buffer delay after the
// end of Violent Desire.

void phase_slaughter(CHAR_DATA *ch) {
    OBJ_DATA *sword=get_eq_char(ch,WEAR_WIELD);
    CHAR_DATA *victim=ch->fighting;

    if (victim==NULL) {
	// eeks. this shouldn't happen
	bugf("Entered phased function without target. "
		  "Function is phase_slaughter, char is %s.",ch->name);
	set_phased_skill(ch,NULL,NULL,0,PHASED_NONE);
	return;
    }

    if (ch->fighting!=ch->phasedskill_target) {
	act("$N has interrupted your fight.",ch,NULL,victim,TO_CHAR);
	set_phased_skill(ch,NULL,NULL,0,PHASED_NONE);
	return;
    }

    if (!skillcheck(ch,gsn_violent_desire) ||
	sword==NULL ||
	ch->move<0 ) {
	act("$N staggers away, escaping further harm...",
	    ch,NULL,victim,TO_CHAR);
	act("You somehow stagger away from $ns onslaught...",
	    ch,NULL,victim,TO_VICT);
	act("$N somehow staggers free of $ns wrath...",
	    ch,NULL,victim,TO_NOTVICT);
	set_phased_skill(ch,NULL,NULL,0,PHASED_NONE);
	WAIT_STATE( ch, skill_beats(gsn_violent_desire,ch) );
	check_improve(ch,gsn_violent_desire,TRUE,1);
	return;
    }

    switch (ch->phasedskill_step) {
	case 1:
	    act("You ram $p deep into $Ns chest!",
		ch,sword,victim,TO_CHAR);
	    act("Agony bursts in your chest as $N stabs $p into your body!",
		ch,sword,victim,TO_VICT);
	    act("$n impales $N upon $p!",
		ch,sword,victim,TO_NOTVICT);

	    ch->move-=15;
	    one_hit_variable(ch,victim,gsn_violent_desire,125,FALSE);
	    break;
	case 2:
	    act("You lift $N into the air with $p, $E screams in agony!",
		ch,sword,victim,TO_CHAR);
	    act("You scream in pain as $n lifts you into the air with $p!",
		ch,sword,victim,TO_VICT);
	    act("$N screams horribly as $n hoists $m into the air!",
		ch,sword,victim,TO_NOTVICT);

	    ch->move-=20;
	    one_hit_variable(ch,victim,gsn_violent_desire,135,FALSE);
	    break;
	case 3:
	    act("You slam $N down again, pinning them to the ground with $p!",
		ch,sword,victim,TO_CHAR);
	    act("$n slams you to the ground, pinning you down with $p!",
		ch,sword,victim,TO_VICT);
	    act("$n nails $N to the ground with $p!",
		ch,sword,victim,TO_NOTVICT);

	    ch->move-=15;
	    one_hit_variable(ch,victim,gsn_violent_desire,135,FALSE);

	    if (number_percent() < 5 ) {
		act("$N writhes briefly, then lies still...",
		    ch,NULL,victim,TO_CHAR);
		act("The pain is too great to bear! Everything goes black...",
		    ch,NULL,victim,TO_VICT);
		act("$N writhes in pain, then abruptly lies still at $ns feet...",
		    ch,NULL,victim,TO_NOTVICT);
		stop_fighting(victim,TRUE);
		victim->position=POS_STUNNED;
		set_phased_skill(ch,NULL,NULL,0,PHASED_NONE);
	    }
	    break;
	case 4:
	    act("You place your foot on $Ns chest, and wrench $p free!",
		ch,sword,victim,TO_CHAR);
	    act("$n wrenches $p from your body, your innards spill onto the ground!",
		ch,sword,victim,TO_VICT);
	    act("$n wrenches $p out of $N, $s entrails spill everywhere!",
		ch,sword,victim,TO_NOTVICT);

	    ch->move-=10;
	    one_hit_variable(ch,victim,gsn_violent_desire,135,FALSE);
	    break;
	case 5:
	    act("You swing your weapon overhead and smash it down on $Ns prone body!",
		ch,sword,victim,TO_CHAR);
	    act("$n swings $s weapon down, you scream as it smashes into you!",
		ch,sword,victim,TO_VICT);
	    act("$N screams in pain as $n smashes $p down on $M!",
		ch,sword,victim,TO_NOTVICT);

	    ch->move-=50;
	    one_hit_variable(ch,victim,gsn_violent_desire,200,FALSE);
	    if (number_percent() < 10 ) {
		act("$N writhes briefly, then lies still...",
		    ch,NULL,victim,TO_CHAR);
		act("The pain is too great to bear! Everything goes black...",
		    ch,NULL,victim,TO_VICT);
		act("$N writhes in pain, then abruptly lies still at $ns feet...",
		    ch,NULL,victim,TO_NOTVICT);
		stop_fighting(victim,TRUE);
		victim->position=POS_STUNNED;
	    }
	    WAIT_STATE( ch, skill_beats(gsn_violent_desire,ch) );
	    check_improve(ch,gsn_violent_desire,FALSE,1);
	    set_phased_skill(ch,NULL,NULL,0,PHASED_NONE);
	    return;
    }

    ch->phasedskill_step++;
    return;
}

void do_slaughter(CHAR_DATA *ch,char *arg) {
    CHAR_DATA *victim;
    OBJ_DATA *sword;

    if ( !IS_NPC(ch)
    &&   ch->level < skill_level(gsn_violent_desire,ch))
    {
	send_to_char(
	    "You better leave the martial arts to fighters.\n\r", ch );
	return;
    }

//    if (IS_NPC(ch) && !STR_IS_SET(ch->strbit_off_flags,OFF_HEADBUTT))
//	return;

    if ( ( victim = ch->fighting ) == NULL )
    {
	send_to_char( "You aren't fighting anyone.\n\r", ch );
	return;
    }

    if (IS_AFFECTED(ch,EFF_CALM)) {
	send_to_char("You're feeling too mellow.\n\r",ch);
	return;
    }

    if ((sword = get_eq_char(ch,WEAR_WIELD))==NULL) {
	send_to_char("You need a weapon to make this work!\n\r",ch);
	return;
    }
    if (sword->value[0]!=WEAPON_SWORD &&
	sword->value[0]!=WEAPON_DAGGER &&
	sword->value[0]!=WEAPON_SPEAR &&
	sword->value[0]!=WEAPON_EXOTIC) {
	act("Your $p shouldn't be bludgeoning.",ch,sword,NULL,TO_CHAR);
	return;
    }
    if (!IS_WEAPON_STAT(sword,WEAPON_TWO_HANDS)) {
	send_to_char("You need a two handed weapon to make this work!\n\r",ch);
	return;
    }


    if (!skillcheck(ch,gsn_violent_desire)) {
	act("$N manages to escape your blow.",ch,NULL,victim,TO_CHAR);
	act("You manage to escape $ns blow.",ch,NULL,victim,TO_VICT);
	act("$N manages to escape $ns blow.",ch,NULL,victim,TO_NOTVICT);
	WAIT_STATE( ch, skill_beats(gsn_violent_desire,ch)/2 );
	return;
    }
    act("You concentrate all your efforts on $N.",ch,NULL,victim,TO_CHAR);
    act("$n looks at you in a wierd way...",ch,NULL,victim,TO_VICT);
    act("$n looks at $N, hatred in $s eyes.",ch,NULL,victim,TO_NOTVICT);

    set_phased_skill(ch,phase_slaughter,victim,1,PHASED_DOESOWNDAMAGE);
}



// This skill is the controlled anger that makes a Berserker a Berserker.
// It differs from the skill "Berserk" in that the Berserker cannot choose
// when to use it, and rather than tiring the berserker it fills them with
// violent energy without clouding their judgement.
// Whenever a Berserker enters combat there is a chance (based upon skill%)
// that they will become "Enraged". While enraged a Berserkers hitroll and
// damroll both increase by 1 per 10 levels (ie +2 at level 20), they gain
// one HP per level (temporarily above maximum if possible), and they gain
// two Moves per level (temporarily above maximum if possible). The downside
// is that the berserker loses one mana per level, and their save vs spells
// is penalised by -2 per 10 levels.
// So a level 30 berserker "enraged" has +3 to hit and dam, will gain 30 hp
// and 60 moves, while suffering -30 mana and -6 save vs spells.
void berserker_rage( CHAR_DATA *ch) {

    // quick byes
    if (IS_NPC(ch))
	return;
    if (ch->fighting==NULL)
	return;

    // don't get madder
    if (is_affected(ch,gsn_berserker_rage) ||
	IS_AFFECTED(ch,EFF_BERSERK) ||
	is_affected(ch,gsn_berserk) ||
	is_affected(ch,gsn_frenzy))
	return;

    if (ch->level < skill_level(gsn_berserker_rage,ch))
	return;

    if (!has_skill_available(ch,gsn_berserker_rage)) {
	send_to_char("You turn red in the face, but nothing happens.\n\r",ch);
	return;
    }

    if (IS_AFFECTED(ch,EFF_CALM)) {
	send_to_char("You're feeling too mellow to berserk.\n\r",ch);
	return;
    }

    if (ch->mana < 50) {
	send_to_char("You can't get up enough energy.\n\r",ch);
	return;
    }

    /* damage -- below 50% of hp helps, above hurts */

    if (skillcheck2(ch,gsn_berserker_rage,125-50*ch->hit/ch->max_hit)) {
	EFFECT_DATA ef;

	WAIT_STATE(ch,PULSE_VIOLENCE);
	ch->mana -= 50;
	ch->move /= 2;

	/* heal a little damage */
	if (ch->hit < ch->max_hit) {
	    ch->hit += ch->level * 2;
	    ch->hit = UMIN(ch->hit,ch->max_hit);
	}

	send_to_char("Violent energy surges through your veins, you feel the urge to kill!\n\r",ch);
	act("$ns face flushes with rage and $e starts foaming at the mouth!",
	    ch,NULL,NULL,TO_ROOM);
	check_improve(ch,gsn_berserker_rage,TRUE,2);

	ZEROVAR(&ef,EFFECT_DATA);
	ef.where	= TO_EFFECTS;
	ef.type		= gsn_berserker_rage;
	ef.level	= ch->level;
	ef.duration	= number_fuzzy(ch->level / 8);
	ef.bitvector    = EFF_BERSERK;
	ef.casted_by	= 0;
	ef.modifier	= UMAX(1,ch->level/10);
	ef.location	= APPLY_HITROLL;
	effect_to_char(ch,&ef);

	ef.location	= APPLY_DAMROLL;
	effect_to_char(ch,&ef);

        ef.location     = APPLY_SAVING_SPELL;
        ef.modifier     = 2*ch->level/10;
	effect_to_char(ch,&ef);

	ch->hit+=ch->level;
	ch->move+=2*ch->level;
	ch->mana-=ch->level;
    }
}


// This is a prayer that a Templar/Reaver recites in battle. They
// must be in battle for it to be effective. While praying the
// Templar/Reaver gains a bonus of +1 to hitroll and +1 to damage per
// 10 levels. The prayer drains the Templar/Reaver of 50 mana when
// started and costs 20 mana per tick untill either the fight ends,
// or mana is "consumed".
void phase_battle_hymn(CHAR_DATA *ch) {
    ch->mana-=20;
    if (ch->mana<0) {
	send_to_char("You can't keep up the singing anymore!\n\r",ch);
	act("$n looks too tired and stops singing now!",
	    ch,NULL,NULL,TO_ROOM);
	effect_strip(ch,gsn_battle_hymn);
	set_phased_skill(ch,NULL,NULL,0,PHASED_NONE);
	return;
    }

    if (ch->position!=POS_FIGHTING) {
	send_to_char("Your fight is over, you stop singing.\n\r",ch);
	act("$n stops singing when your fight is over.",
	    ch,NULL,NULL,TO_ROOM);
	effect_strip(ch,gsn_battle_hymn);
	set_phased_skill(ch,NULL,NULL,0,PHASED_NONE);
	return;
    }
}
void do_battle_hymn(CHAR_DATA *ch,char *arg) {
    EFFECT_DATA ef;

    if (ch->position!=POS_FIGHTING) {
	send_to_char("You aren't fighting anything.\n\r",ch);
	return;
    }

    if (!has_skill_available(ch,gsn_battle_hymn)) {
	check_social(ch,"sing","");
	return;
    }
    if (!skillcheck(ch,gsn_battle_hymn)) {
	send_to_char("You start to sing...\n\r"
		     "...but your voice creaks and you stop.\n\r",ch);
	return;
    }

    if (ch->mana<50) {
	send_to_char("You start to sing...\n\r"
		     "...but your voice creaks and you stop.\n\r",ch);
	ch->mana=0;
	return;
    }

    ch->mana-=50;
    send_to_char("You start to sing...\n\r",ch);
    act("$n starts singing, $e sounds angelic.",ch,NULL,NULL,TO_ROOM);

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = gsn_battle_hymn;
    ef.level     = ch->level;
    ef.duration  = ch->mana/20;
    ef.location  = APPLY_HITROLL;
    ef.modifier  = ch->level / 10;
    ef.bitvector = EFF_BATTLE_HYMN;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( ch, &ef );
    ef.location  = APPLY_DAMROLL;
    effect_to_char( ch, &ef );

    set_phased_skill(ch,phase_battle_hymn,NULL,0,PHASED_TAKEAFTERCARE);
}

void phase_war_dirge(CHAR_DATA *ch) {
    ch->mana-=20;

    if (ch->position!=POS_FIGHTING) {
	send_to_char("Your fight is over, you stop singing.\n\r",ch);
	act("$n stops singing when $s fight is over.",
	    ch,NULL,NULL,TO_ROOM);
	effect_strip(ch,gsn_war_dirge);
	set_phased_skill(ch,NULL,NULL,0,PHASED_NONE);
	return;
    }

    if (ch->mana<0) {
	send_to_char("You can't keep up the singing anymore!\n\r",ch);
	act("$n looks too tired and stops singing now!",
	    ch,NULL,NULL,TO_ROOM);
	effect_strip(ch,gsn_war_dirge);
	set_phased_skill(ch,NULL,NULL,0,PHASED_NONE);
	return;
    }

}
void do_war_dirge(CHAR_DATA *ch,char *arg) {
    EFFECT_DATA ef;

    if (ch->position!=POS_FIGHTING) {
	send_to_char("You aren't fighting anything.\n\r",ch);
	return;
    }

    if (!has_skill_available(ch,gsn_war_dirge)) {
	check_social(ch,"sing","");
	return;
    }
    if (skillcheck(ch,gsn_war_dirge)) {
	send_to_char("You start to sing...\n\r"
		     "...but your voice creaks and you stop.\n\r",ch);
	return;
    }

    if (ch->mana<50 || !skillcheck(ch,gsn_war_dirge)) {
	send_to_char("You start to sing...\n\r"
		     "...but your voice creaks and you stop.\n\r",ch);
	if (ch->mana<50) ch->mana=0;
	return;
    }

    ch->mana-=50;
    send_to_char("You start to sing...\n\r",ch);
    act("$n starts singing, $s voice is filled with hatred!",
	ch,NULL,NULL,TO_ROOM);

    ZEROVAR(&ef,EFFECT_DATA);
    ef.where     = TO_EFFECTS;
    ef.type      = gsn_war_dirge;
    ef.level     = ch->level;
    ef.duration  = ch->mana/20;
    ef.location  = APPLY_HITROLL;
    ef.modifier  = ch->level / 10;
    ef.bitvector = EFF_BATTLE_HYMN;
    ef.casted_by = ch?ch->id:0;
    effect_to_char( ch, &ef );
    ef.location  = APPLY_DAMROLL;
    effect_to_char( ch, &ef );

    set_phased_skill(ch,phase_battle_hymn,NULL,0,PHASED_TAKEAFTERCARE);
}
