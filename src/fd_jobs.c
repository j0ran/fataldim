//
// $Id: fd_jobs.c,v 1.17 2009/03/30 15:17:58 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"
#include "interp.h"

JOB_TYPE	*job_list=NULL;
bool		job_changed;

bool new_job(char *name) {
    JOB_TYPE *job,*j2;

    job=(JOB_TYPE *)calloc(1,sizeof(JOB_TYPE));
    job->name=(char *)calloc(1,strlen(name)+1);
    strcpy(job->name,name);
    STR_ZERO_BIT(job->commands,MAX_FLAGS);
    STR_ZERO_BIT(job->channels,MAX_FLAGS);
    job->note_targets=NULL;

    job->vnum=0;
    for (j2=job_list;j2;j2=j2->next)
	if (j2->vnum>job->vnum) job->vnum=j2->vnum;
    job->vnum++;

    if (job->vnum>MAX_JOBS) {
	bugf("new_job: too many jobs. increase MAX_JOB in merc.h");
	free(job);
	return FALSE;
    }

    job->next=job_list;
    job_list=job;
    return TRUE;
}

void do_job(CHAR_DATA *ch, char *argument) {
    JOB_TYPE *job=job_list;
    char arg[MAX_INPUT_LENGTH];
    char arg1[MAX_INPUT_LENGTH];
    int counter,i;

    if (argument[0]=='\0') {
	send_to_char("options: view, command, channel, new, save\r\n",ch);
	return;
    }

    argument=one_argument(argument,arg);
    argument=one_argument(argument,arg1);

    if (!str_prefix(arg,"view")) {
	if (arg1[0]!='\0') {
	    if (!str_cmp(arg1,"command")) {
		// show which jobs `command` is in
		int cmd_vnum;

		cmd_vnum=command_to_vnum(argument);
		if (cmd_vnum==-1) {
		    sprintf_to_char(ch,"Unknown command: %s\r\n",argument);
		    return;
		}
		if (cmd_vnum==0) {
		    sprintf_to_char(ch,"%s is not a priviledged command.\r\n",argument);
		    return;
		}
		counter=0;
		sprintf_to_char(ch,"%s is found in:\r\n",vnum_to_command(cmd_vnum));
		for (;job;job=job->next) {
		    if (STR_IS_SET(job->commands,cmd_vnum)) {
			sprintf_to_char(ch,"%s%s",counter?", ":"",job->name);
			counter++;
		    }
		}
		if (!counter)
		    send_to_char("<nowhere>",ch);
		send_to_char("\r\n",ch);
		return;
	    }
	    if (!str_cmp(arg1,"channel")) {
		// show which jobs `channel` is in
		int chan_vnum;

		chan_vnum=wiznet_lookup(argument);
		if (chan_vnum==-1) {
		    sprintf_to_char(ch,"Unknown command: %s\r\n",argument);
		    return;
		}
		if (chan_vnum==0) {
		    sprintf_to_char(ch,"%s is not a priviledged command.\r\n",argument);
		    return;
		}
		counter=0;
		sprintf_to_char(ch,"%s is found in:\r\n",vnum_to_command(chan_vnum));
		for (;job;job=job->next) {
		    if (STR_IS_SET(job->channels,chan_vnum)) {
			sprintf_to_char(ch,"%s%s",counter?", ":"",job->name);
			counter++;
		    }
		}
		if (!counter)
		    send_to_char("<nowhere>",ch);
		send_to_char("\r\n",ch);
		return;
	    }
	    if (!str_cmp(arg1,"job"))
		strncpy(arg1,argument,MAX_INPUT_LENGTH);
	    while (job && str_cmp(job->name,arg1)) job=job->next;
	    if (job) {
		sprintf_to_char(ch,"Viewing job '%s':\r\n",job->name);
		if (STR_SAME_STR(job->commands,ONE_FLAG,MAX_FLAGS)) {
		    send_to_char("Job has ALL commands.\r\n",ch);
		} else {
		    send_to_char("Commands:\r\n",ch);
		    i=0;
		    for (counter=1;counter<MAX_FLAGS*8;counter++)
			if (STR_IS_SET(job->commands,counter)) {
			    const char *name;
			    name=vnum_to_command(counter);
			    if (name) {
				sprintf_to_char(ch,"%-14s ",name);
				if (++i % 5 == 0) send_to_char("\r\n",ch);
			    }
			}
		    if (i%5!=0)
			send_to_char("\r\n",ch);
		}
		if (STR_SAME_STR(job->channels,ONE_FLAG,MAX_FLAGS)) {
		    send_to_char("Job has ALL channels.\r\n",ch);
		} else {
		    send_to_char("Channels:\r\n",ch);
		    i=0;
		    // lets skip 'on' and 'prefix' and start at 2
		    for (counter=2;counter<20*8;counter++)
			if (STR_IS_SET(job->channels,counter)) {
			    const char *name;
			    name=wiznet_name_lookup(counter);
			    if (name) {
				sprintf_to_char(ch,"%-14s ",name);
				if (++i % 5 == 0) send_to_char("\r\n",ch);
			    }
			}
		    if (i%5!=0)
			send_to_char("\r\n",ch);
		}
		if (job->note_targets && job->note_targets[0])
		    sprintf_to_char(ch,"Note targets: %s\r\n",job->note_targets);
		else
		    send_to_char("No note targets defined for this job.\r\n",ch);

		return;
	    }
	    sprintf_to_char(ch,"'%s' is not a known job. ",arg1);
	}
	send_to_char("Known jobs are:\r\n",ch);
	for (job=job_list;job;job=job->next)
	    sprintf_to_char(ch,"%s ",job->name);
	send_to_char("\r\n",ch);
	return;
    } else if (!str_prefix(arg,"command")) {
	if (!IS_TRUSTED(ch,MAX_LEVEL)) {
	    sprintf_to_char(ch,"You have to have a trust of %d to alter jobs.\r\n",MAX_LEVEL);
	    return;
	}
	if (arg1[0]==0)
	    send_to_char("Usage: job command <job> add|del <command>\r\n",ch);
	else {
	    bool add;
	    int  vnum;
	    // find the job
	    for (job=job_list;job;job=job->next)
		if (!str_cmp(job->name,arg1)) break;
	    if (!job) {
		send_to_char("No job with that name\r\n",ch);
		return;
	    }
	    // add/del
	    argument=one_argument(argument,arg);
	    if (!str_prefix(arg,"add")) add=TRUE;
	    else if (!str_prefix(arg,"delete")) add=FALSE;
	    else {
		send_to_char("Add or Delete: your choice.\r\n",ch);
		return;
	    }
	    argument=one_argument(argument,arg);
	    if (arg[0]==0) {
		sprintf_to_char(ch,"%s which command(s).\r\n",add?"Add":"Remove");
		return;
	    }
	    while (arg[0]!=0) {
		if (!str_cmp(arg,"all")) {
		    if (add) {
			STR_ONE_BIT(job->commands,MAX_FLAGS);
			sprintf_to_char(ch,"Added ALL commands to job %s\r\n",job->name);
		    } else {
			STR_ZERO_BIT(job->commands,MAX_FLAGS);
			sprintf_to_char(ch,"Removed ALL commands from job %s\r\n",job->name);
		    }
		    job_changed=TRUE;
		} else {
		    // find command
		    vnum=command_to_vnum(arg);
		    if (vnum==0) {
			send_to_char("Mortal commands can't be in jobs\r\n",ch);
			argument=one_argument(argument,arg);
			continue;
		    }
		    if (vnum<0) {
			send_to_char("Unknown command\r\n",ch);
			argument=one_argument(argument,arg);
			continue;
		    }
		    if (add) {
			if (STR_IS_SET(job->commands,vnum))
			    sprintf_to_char(ch,"%s is already in job %s\r\n",vnum_to_command(vnum),job->name);
			else {
			    STR_SET_BIT(job->commands,vnum);
			    sprintf_to_char(ch,"Added %s to job %s\r\n",vnum_to_command(vnum),job->name);
			}
		    } else {
			if (STR_IS_SET(job->commands,vnum)) {
			    STR_REMOVE_BIT(job->commands,vnum);
			    sprintf_to_char(ch,"Removed %s from job %s\r\n",vnum_to_command(vnum),job->name);
			} else
			    sprintf_to_char(ch,"%s isn't in job %s\r\n",vnum_to_command(vnum),job->name);
		    }
		    job_changed=TRUE;
		}
		argument=one_argument(argument,arg);
	    }
	}
    } else if (!str_prefix(arg,"channel")) {
	if (!IS_TRUSTED(ch,MAX_LEVEL)) {
	    sprintf_to_char(ch,"You have to have a trust of %d to alter jobs.\r\n",MAX_LEVEL);
	    return;
	}
	if (argument[0]==0)
	    send_to_char("Usage: job channel <job> add|del <channel>\r\n",ch);
	else {
	    bool add;
	    int  vnum;
	    // find the job
	    for (job=job_list;job;job=job->next)
		if (!str_cmp(job->name,arg1)) break;
	    if (!job) {
		send_to_char("No job with that name\r\n",ch);
		return;
	    }
	    // add/del
	    argument=one_argument(argument,arg);
	    if (!str_prefix(arg,"add")) add=TRUE;
	    else if (!str_prefix(arg,"delete")) add=FALSE;
	    else {
		send_to_char("Add or Delete: your choice.\r\n",ch);
		return;
	    }
	    argument=one_argument(argument,arg);
	    if (arg[0]==0) {
		sprintf_to_char(ch,"%s which channels(s).\r\n",add?"Add":"Remove");
		return;
	    }
	    while (arg[0]!=0) {
               if (!str_cmp(arg,"all")) {
                    if (add) {
                        STR_ONE_BIT(job->channels,MAX_FLAGS);
                        sprintf_to_char(ch,"Added ALL channels to job %s\r\n",job->name);
                    } else {
                        STR_ZERO_BIT(job->channels,MAX_FLAGS);
                        sprintf_to_char(ch,"Removed ALL channels from job %s\r\n",job->name);
                    }
                    job_changed=TRUE;
		} else {
		    // find command
		    vnum=wiznet_lookup(arg);
		    if (vnum<0) {
			send_to_char("Unknown wiznet channel\r\n",ch);
			argument=one_argument(argument,arg);
			continue;
		    }
		    if (add) {
			if (STR_IS_SET(job->channels,vnum))
			    sprintf_to_char(ch,"%s is already in job %s\r\n",wiznet_name_lookup(vnum),job->name);
			else {
			    STR_SET_BIT(job->channels,vnum);
			    sprintf_to_char(ch,"Added %s to job %s\r\n",wiznet_name_lookup(vnum),job->name);
			}
		    } else {
			if (STR_IS_SET(job->channels,vnum)) {
			    STR_REMOVE_BIT(job->channels,vnum);
			    sprintf_to_char(ch,"Removed %s from job %s\r\n",wiznet_name_lookup(vnum),job->name);
			} else
			    sprintf_to_char(ch,"%s isn't in job %s\r\n",wiznet_name_lookup(vnum),job->name);
		    }
		    job_changed=TRUE;
		}
		argument=one_argument(argument,arg);
	    }
	}
    } else if (!str_prefix(arg,"target")) {
	if (!IS_TRUSTED(ch,MAX_LEVEL)) {
	    sprintf_to_char(ch,"You have to have a trust of %d to alter jobs.\r\n",MAX_LEVEL);
	    return;
	}
	for (job=job_list;job;job=job->next)
	    if (!str_cmp(job->name,arg1)) break;
	if (!job) {
	    send_to_char("No job with that name\r\n",ch);
	    return;
	}
	argument=one_argument(argument,arg);
	if (!str_prefix(arg,"add")) {
	    if (job->note_targets)
		strncpy(arg1,job->note_targets,MAX_INPUT_LENGTH);
	    else
		arg1[0]=0;
	    for (;;) {
		
		argument=one_argument(argument,arg);
		if (arg[0]==0) break;

		if (is_exact_name(arg,job->note_targets)) {
		    sprintf_to_char(ch,"%s already has a target of '%s'\n\r",job->name,arg);
		    continue;
		}

		if ((strlen(arg1)+strlen(arg)+2)>MAX_INPUT_LENGTH) {
		    send_to_char("Note target string too long.\n\r",ch);
		    continue;
		}
		if (arg1[0]!=0) strcat(arg1," ");
		strcat(arg1,arg);
	    }
	    free_string(job->note_targets);
	    job->note_targets=str_dup(arg1);
	} else if (!str_prefix(arg,"remove") || !str_prefix(arg,"delete")) {
	    char *orig=job->note_targets;
	    if (!orig) return;
	    arg1[0]=0;
	    for (;;) {
		
		orig=one_argument(orig,arg);
		if (arg[0]==0) break;

		if (is_exact_name(arg,argument))
		    continue;

		if (arg1[0]!=0) strcat(arg1," ");
		strcat(arg1,arg);
	    }
	    free_string(job->note_targets);
	    if (arg1[0])
		job->note_targets=str_dup(arg1);
	    else
		job->note_targets=NULL;
	} else {
	    send_to_char("You can only add or remove/delete note targets\n\r",ch);
	    return;
	}
	job_changed=TRUE;
    } else if (!str_prefix(arg,"new")) {
	if (!IS_TRUSTED(ch,MAX_LEVEL)) {
	    sprintf_to_char(ch,"You have to have a trust of %d to alter jobs.\r\n",MAX_LEVEL);
	    return;
	}
	if (arg1[0]==0)
	    send_to_char("Usage: job new <job>\r\n",ch);
	else {
	    if (new_job(arg1))
		sprintf_to_char(ch,"New job '%s' created.\r\n",arg1);
	    else
		send_to_char("Creation of job failed.\r\n",ch);
	}
	job_changed=TRUE;
    } else if (!str_prefix(arg,"save")) {
	save_jobs();
	send_to_char("Job saved.\r\n",ch);
    } else {
	sprintf_to_char(ch,"Job doesn't know %s.\r\n",arg);
    }

}

void load_jobs(void) {
    FILE *fp;
    char *keyword;
    char *value;

    if ( ( fp = fopen( mud_data.job_file, "r" ) ) == NULL ) {
	logf("[0] load_jobs(): fopen(%s): %s", mud_data.job_file,ERROR);
	return;
    }

    while (strcmp((keyword=fread_word(fp)),"#end")) {
	if (keyword[0]=='#') {
	    fread_to_eol(fp);
	    continue;
	} else if (!strcmp(keyword,"name")) {
	    value=fread_word(fp);
	    if (!new_job(value)) {
		logf("Failed to create job '%s'. Loading aborted",value);
		fclose(fp);
		return;
	    }
	    fread_to_eol(fp);
	} else if (!strcmp(keyword,"commands")) {
	    while ((value=fread_word(fp))[0]!='~') {
		// ok, we got the command in value. scan the table
		int vnum;

                if (!strcmp(value,"all")) {
		    STR_ONE_BIT(job_list->commands,MAX_FLAGS);
		} else {
		    vnum=exact_command_to_vnum(value);
		    if (vnum!=-1)
			STR_SET_BIT(job_list->commands,vnum);
		    else
			bugf("Unknown command '%s' in job '%s'.",value,job_list->name);
		}
	    }
	} else if (!strcmp(keyword,"channels")) {
	    while ((value=fread_word(fp))[0]!='~') {
		// ok, we got the command in value. scan the table
		int vnum;
		if (!strcmp(value,"all")) {
		    STR_ONE_BIT(job_list->channels,MAX_FLAGS);
		} else {
		    vnum=wiznet_lookup(value);
		    if (cmd_table[vnum].name!=NULL)
			STR_SET_BIT(job_list->channels,vnum);
		    else
			bugf("Unknown channel '%s' in job '%s'.",value,job_list->name);
		}
	    }
	} else if (!strcmp(keyword,"notetargets")) {
	    free_string(job_list->note_targets);
	    job_list->note_targets=fread_string_eol(fp);
	} else {
	    bugf("Unknown job keyword '%s'.",keyword);
	    fread_to_eol(fp);
	    continue;
        }
    }

    fclose(fp);
    job_changed=FALSE;
}

void save_job(FILE *fp,JOB_TYPE *job) {
    int i;
    const char *name;

    if (job->next) save_job(fp,job->next);

    fprintf(fp, "name %s\n",job->name);

    fprintf(fp, "commands ");
    if (STR_SAME_STR(job->commands,ONE_FLAG,MAX_FLAGS)) {
	fprintf(fp, "all ");
    } else {
	for (i=0;i<MAX_FLAGS*8;i++)
	    if (STR_IS_SET(job->commands,i)) {
		if ((name=vnum_to_command(i)))
		    fprintf(fp,"%s ",name);
		else
		    break;
	    }
    }
    fprintf(fp,"~\n");

    fprintf(fp, "channels ");
    if (STR_SAME_STR(job->channels,ONE_FLAG,MAX_FLAGS)) {
	fprintf(fp, "all ");
    } else {
	for (i=0;i<MAX_FLAGS*8;i++)
	    if (STR_IS_SET(job->channels,i)) {
		if ((name=wiznet_name_lookup(i)))
		    fprintf(fp,"%s ",name);
		else
		    break;
	    }
    }
    fprintf(fp,"~\n");

    if (job->note_targets && job->note_targets[0])
	fprintf(fp,"notetargets %s\n",job->note_targets);
}

void save_jobs(void) {

    FILE *fp;
    char renamefile[MIL];

    if (!job_changed) return;

    strcpy(renamefile,mud_data.job_file);
    strcat(renamefile,"~");
    rename(mud_data.job_file,renamefile);

    fclose(fpReserve);

    if ( ( fp = fopen( mud_data.job_file, "w" ) ) == NULL ) {
	logf("[0] save_jobs(): fopen(%s): %s", mud_data.job_file,ERROR);
	fpReserve = fopen( NULL_FILE, "r" );
	return;
    }

    save_job(fp,job_list);
    fprintf(fp,"#end\n");

    fclose(fp);
    fpReserve = fopen( NULL_FILE, "r" );

    job_changed=FALSE;
}

bool is_command_allowed(CHAR_DATA *ch,int command) {
    JOB_TYPE *job;
    CHAR_DATA *whotocheck;

    if (command==0) return TRUE; // command is not protected

    if (IS_PC(ch))
	whotocheck=ch;
    else {
	if (ch->desc==NULL)
	    return FALSE;
	if (ch->desc->original==NULL)
	    return FALSE;
	whotocheck=ch->desc->original;
    }

    for (job=job_list;job!=NULL;job=job->next)
	if (STR_IS_SET(job->commands,command) &&
	    STR_IS_SET(whotocheck->pcdata->jobs,job->vnum))
	    return TRUE;

    return FALSE;
}

bool is_channel_allowed(CHAR_DATA *ch,int channel) {
    JOB_TYPE *job;
    CHAR_DATA *whotocheck;

    if (channel<=1) return TRUE; // channel is not protected: 0=On 1=Prefix

    if (IS_PC(ch))
	whotocheck=ch;
    else {
	if (ch->desc==NULL)
	    return FALSE;
	if (ch->desc->original==NULL)
	    return FALSE;
	whotocheck=ch->desc->original;
    }

    for (job=job_list;job;job=job->next)
	if (STR_IS_SET(job->channels,channel) &&
	    STR_IS_SET(whotocheck->pcdata->jobs,job->vnum))
	    return TRUE;

    return FALSE;
}

bool char_has_jobtarget_in_list(CHAR_DATA *ch,char *targets) {
    JOB_TYPE *job;
    CHAR_DATA *whotocheck;
    char target[MSL];

    if (!targets || !targets[0])
	return FALSE;

    if (IS_PC(ch))
	whotocheck=ch;
    else {
	if (ch->desc==NULL)
	    return FALSE;
	if (ch->desc->original==NULL)
	    return FALSE;
	whotocheck=ch->desc->original;
    }

    targets=one_argument(targets,target);
    while (target[0]) {
	for (job=job_list;job;job=job->next)
	    if (STR_IS_SET(whotocheck->pcdata->jobs,job->vnum) &&
		is_exact_name(target,job->note_targets))
		return TRUE;
	targets=one_argument(targets,target);
    }

    return FALSE;
}

bool is_job_note_target(char *target) {
    JOB_TYPE *job;

    for (job=job_list;job;job=job->next)
	if (is_exact_name(target,job->note_targets))
	    return TRUE;

    return FALSE;
}

bool char_has_job(CHAR_DATA *ch,char *jobname) {
    JOB_TYPE *job;

    for (job=job_list;job;job=job->next)
	if (!str_cmp(job->name,jobname))
	    break;

    if (!job) return FALSE;

    return (STR_IS_SET(ch->pcdata->jobs,job->vnum));
}

void job_grant(CHAR_DATA *ch,CHAR_DATA *victim,char *jobname) {
    JOB_TYPE *job;

    for (job=job_list;job;job=job->next)
	if (!str_cmp(job->name,jobname))
	    break;

    if (!job) {
	send_to_char("Job does not exist.\r\n",ch);
	return;
    }

    if (STR_IS_SET(victim->pcdata->jobs,job->vnum)) {
	sprintf_to_char(ch,"%s is already assigned to %s.\r\n",
			   NAME(victim),job->name);
    } else {
	STR_SET_BIT(victim->pcdata->jobs,job->vnum);
	sprintf_to_char(ch,"%s has been assigned to %s.\r\n",
			   NAME(victim),job->name);
    }

}

void job_revoke(CHAR_DATA *ch,CHAR_DATA *victim,char *jobname) {
    JOB_TYPE *job;

    for (job=job_list;job;job=job->next)
	if (!str_cmp(job->name,jobname))
	    break;

    if (!job) {
	send_to_char("Job does not exist.\r\n",ch);
	return;
    }

    if (!STR_IS_SET(victim->pcdata->jobs,job->vnum)) {
	sprintf_to_char(ch,"%s is not assigned to %s.\r\n",
			   NAME(victim),job->name);
    } else {
	STR_REMOVE_BIT(victim->pcdata->jobs,job->vnum);
	sprintf_to_char(ch,"%s has been removed from %s.\r\n",
			   NAME(victim),job->name);
    }

}

void job_show(CHAR_DATA *ch,CHAR_DATA *victim) {
    JOB_TYPE *job;

    if (STR_SAME_STR(victim->pcdata->jobs,ZERO_FLAG,(MAX_JOBS+7)/8)) {
	sprintf_to_char(ch,"%s has no jobs assigned.\r\n",NAME(victim));
	return;
    }

    sprintf_to_char(ch,"Jobs assigned to %s:\r\n",NAME(victim));
    for (job=job_list;job;job=job->next)
	if (STR_IS_SET(victim->pcdata->jobs,job->vnum))
	    sprintf_to_char(ch,"%s ",job->name);

    send_to_char("\r\n",ch);
}

void jobs_to_str(char *buf, int size, CHAR_DATA *ch) {
    JOB_TYPE *job;

    buf[0]=0;
    for (job=job_list;job;job=job->next) {
	if (STR_IS_SET(ch->pcdata->jobs,job->vnum)) {
	    strlcat(buf,job->name,size);
	    if (strlcat(buf," ",size)>size) 
		bugf("Can't save all jobs for %s. increase buffer length",NAME(ch));
	}
    }
}

void jobs_from_str(CHAR_DATA *ch, char *jobstr) {
    char jobname[MSL];

    for (jobstr=one_argument(jobstr,jobname);jobname[0];jobstr=one_argument(jobstr,jobname)) {
	JOB_TYPE *job;

	for (job=job_list;job;job=job->next)
	    if (!str_cmp(job->name,jobname))
		break;

	if (!job) {
	    bugf("jobs_from_str: job doesn't exist. %s",jobname);
	    continue;
	}

	STR_SET_BIT(ch->pcdata->jobs,job->vnum);
    }
}

void mortalcouncil_update(CHAR_DATA *ch) {
    char MC_JOB[] = "mortal-council";
    if (ch->clan && !ch->clan->clan_info->independent && ch->clan->rank_info->rank>=CLAN_KNIGHT) {  
	if (!char_has_job(ch,MC_JOB)) {
	    job_grant(NULL,ch,MC_JOB);
	    logf("auto-granted %s the %s job",NAME(ch),MC_JOB);   
	}
    } else {
	if (char_has_job(ch,MC_JOB)) {
	    job_revoke(NULL,ch,MC_JOB);
	    logf("auto-revoked %s the %s job",NAME(ch),MC_JOB);      
	}
    }
}
