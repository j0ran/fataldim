// $Id: olc_oedit.c,v 1.24 2008/05/01 19:21:42 jodocus Exp $
/***************************************************************************
 *  File: olc.c                                                            *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 *                                                                         *
 *  This code was freely distributed with the The Isles 1.1 source code,   *
 *  and has been used here for OLC - OLC would not be what it is without   *
 *  all the previous coders who released their source code.                *
 *                                                                         *
 ***************************************************************************/

#include "merc.h"
#include "olc.h"
#include "interp.h"
#include "db.h"

/*
 * Object Editor Functions.
 */
void show_obj_values( CHAR_DATA *ch, OBJ_INDEX_DATA *obj )
{
    ROOM_INDEX_DATA *pRoom;

    switch( obj->item_type )
    {
    default:	/* No values. */
        break;

    case ITEM_WEAPON:
        sprintf_to_char( ch,
                         "[v0] Weapon type:    [%s]\n\r"
                         "[v1] Number of Dice: [%d]\n\r"
                         "[v2] Sides of Dice:  [%d]\n\r"
                         "[v3] Damage message: [%s]\n\r",
                         weapon_name( obj->value[0] ),
                obj->value[1],
                obj->value[2],
                table_find_value(obj->value[3], damagetype_table ));
        sprintf_to_char( ch,
                         "[v4] Weapon flags:   [%s]\n\r",
                         option_string_from_long(obj->value[4],weaponflag_options));
        break;

    case ITEM_ARMOR:
        sprintf_to_char( ch,
                         "[v0] Armor Pierce: [%d]\n\r"
                         "[v1] Armor Bash:   [%d]\n\r"
                         "[v2] Armor Slash:  [%d]\n\r"
                         "[v3] Armor Exotic: [%d]\n\r"
                         "[v4] Bulk:         [Not yet implemented]\n\r",
                         obj->value[0],obj->value[1],
                obj->value[2],obj->value[3]);
        break;

    case ITEM_LIGHT:
        if ( obj->value[2] == -1 )
            sprintf_to_char( ch, "[v2] Light:  Infinite[-1]\n\r" );
        else
            sprintf_to_char( ch, "[v2] Light:  [%d]\n\r", obj->value[2] );
        break;

    case ITEM_WAND:
    case ITEM_STAFF:
        sprintf_to_char( ch,
                         "[v0] Level:          [%d]\n\r"
                         "[v1] Charges Total:  [%d]\n\r"
                         "[v2] Charges Left:   [%d]\n\r"
                         "[v3] Spell:          [%s]\n\r",
                         obj->value[0],
                obj->value[1],
                obj->value[2],
                obj->value[3] != -1 ? skill_name(obj->value[3],ch)
            : "none" );
        break;

    case ITEM_SCROLL:
    case ITEM_POTION:
    case ITEM_PILL:
        sprintf_to_char( ch,
                         "[v0] Level:  [%d]\n\r"
                         "[v1] Spell:  [%s]\n\r"
                         "[v2] Spell:  [%s]\n\r"
                         "[v3] Spell:  [%s]\n\r"
                         "[v4] Spell:  [%s]\n\r",
                         obj->value[0],
                obj->value[1] != -1 ? skill_name(obj->value[1],ch)
            : "none",
              obj->value[2] != -1 ? skill_name(obj->value[2],ch)
            : "none",
              obj->value[3] != -1 ? skill_name(obj->value[3],ch)
            : "none" ,
              obj->value[4] != -1 ? skill_name(obj->value[4],ch)
            : "none" );
        break;

    case ITEM_BOAT:
        sprintf_to_char( ch,
                         "[v0] Places players:    [%d]\n\r"
                         "[v2] Funiture flags:    [%s]\n\r",
                         obj->value[0],
                option_string_from_long(obj->value[2],furniture_options));
        break;

    case ITEM_CONTAINER:
        sprintf_to_char( ch,
                         "[v0] Total Weight:      [%d kg]\n\r"
                         "[v1] Flags:             [%s]\n\r"
                         "[v2] Key vnum:          [%5d]\n\r"
                         "[v3] Weight per Item:   [%d kg]\n\r"
                         "[v4] Weight multiplier: [%d]\n\r",
                         obj->value[0],
                option_string_from_long(obj->value[1],container_options ),
                obj->value[2],
                obj->value[3],
                obj->value[4] );
        break;

    case ITEM_DRINK_CON:
        sprintf_to_char( ch,
                         "[v0] Liquid Total: [%d]\n\r"
                         "[v1] Liquid Left:  [%d]\n\r"
                         "[v2] Liquid:       [%s]\n\r"
                         "[v3] Poisoned:     %s\n\r",
                         obj->value[0],
                obj->value[1],
                table_find_value( obj->value[2], liquid_table ),
                obj->value[3] != 0 ? "Yes" : "No" );
        break;

    case ITEM_FOUNTAIN:
        sprintf_to_char( ch,
                         "[v0] Liquid Total: [%d]\n\r"
                         "[v1] Liquid Left:  [%d]\n\r"
                         "[v2] Liquid:       [%s]\n\r"
                         "[v3] Poisoned:     %s\n\r",
                         obj->value[0],
                obj->value[1],
                table_find_value( obj->value[2], liquid_table ),
                obj->value[3] != 0 ? "Yes" : "No" );
        break;

    case ITEM_FOOD:
        sprintf_to_char( ch,
                         "[v0] Food full hours:   [%d]\n\r"
                         "[v1] Food hungry hours: [%d]\n\r"
                         "[v3] Poisoned:          %s\n\r",
                         obj->value[0],
                obj->value[1],
                obj->value[3] != 0 ? "Yes" : "No" );
        break;

    case ITEM_MONEY:
        sprintf_to_char( ch,
                         "[v0] Silver:   [%d]\n\r"
                         "[v1] Gold:     [%d]\n\r",
                         obj->value[0],obj->value[1] );
        break;

    case ITEM_PORTAL:
        pRoom=get_room_index(obj->value[3]);
        sprintf_to_char( ch,
                         "[v0] Charges:    %s[%d]\n\r"
                         "[v1] Exit flags: [%s]\n\r",
                         obj->value[0] == 0 ? "Infinite" : "",
                obj->value[0],
                option_string_from_long(obj->value[1],exit_options));

        sprintf_to_char( ch,
                         "[v2] Gate flags: [%s]\n\r"
                         "[v3] Goto Room:  [%d] %s\n\r"
                         "[v4] Key vnum:   [%d]\n\r",
                         option_string_from_long(obj->value[2],gate_options),
                obj->value[3],
                obj->value[3]<=0 ? "Random" :
                                   pRoom!=NULL ? pRoom->name : "Can't find room!",
                obj->value[4]);
        break;

    case ITEM_FURNITURE:
        sprintf_to_char( ch,
                         "[v0] Sitplaces:      [%d]\n\r"
                         "[v1] Support weight: [%d kg]\n\r"
                         "[v2] Funiture flags: [%s]\n\r"
                         "[v3] Healing bonus:  [%d]\n\r"
                         "[v4] Mana bonus:     [%d]\n\r",
                         obj->value[0],obj->value[1],
                option_string_from_long( obj->value[2],furniture_options ),
                obj->value[3],obj->value[4] );
        break;

    case ITEM_QUEST:
        sprintf_to_char(ch,
                        "[v0] Consumption:  [%s]\n\r",
                        table_find_value(obj->value[0],questconsumption_table));
        sprintf_to_char(ch,
                        "[v1] Effects    :  [%s]\n\r"
                        "[v2] By value   :  [%d]\n\r",
                        table_find_value(obj->value[1],questeffects_table),
                obj->value[2] );
        break;

    case ITEM_JUKEBOX:
        if (obj->value[0]<=0)
            sprintf_to_char(ch,"[V0] Plays all songs [%d]\n\r",obj->value[0]);
        else
            sprintf_to_char(ch,"[V0] Playlist        [%d]\n\r",obj->value[0]);
        break;

    }

    return;
}



bool set_obj_values( CHAR_DATA *ch, OBJ_INDEX_DATA *pObj, int value_num, char *argument)
{
    int value;

    switch( pObj->item_type )
    {
    default:
        break;

    case ITEM_LIGHT:
        switch ( value_num )
        {
        default:
            show_help(ch,"ITEM_LIGHT");
            return FALSE;
        case 2:
            send_to_char( "HOURS OF LIGHT SET.\n\r\n\r", ch );
            pObj->value[2] = atoi( argument );
            break;
        }
        break;

    case ITEM_BOAT:
        switch (value_num) {
        default:
            show_help(ch,"ITEM_BOAT");
            return FALSE;
        case 0:
            send_to_char("Number of places set.\n\r\n\r",ch);
            pObj->value[0]=atoi(argument);
            break;
        case 2:
            if ((value=option_find_name(argument,furniture_options,TRUE))
                    != NO_FLAG )
                TOGGLE_BIT(pObj->value[2], value);
            else {
                show_help(ch,"ITEM_FURNITURE");
                return FALSE;
            }
            send_to_char("FURNITURE FLAG TOGGLED.\n\r\n\r",ch);
            break;
        }
        break;

    case ITEM_WAND:
    case ITEM_STAFF:
        switch ( value_num )
        {
        default:
            show_help(ch,"ITEM_STAFF_WAND");
            return FALSE;
        case 0:
            send_to_char( "SPELL LEVEL SET.\n\r\n\r", ch );
            pObj->value[0] = atoi( argument );
            break;
        case 1:
            send_to_char( "TOTAL NUMBER OF CHARGES SET.\n\r\n\r", ch );
            pObj->value[1] = atoi( argument );
            break;
        case 2:
            send_to_char( "CURRENT NUMBER OF CHARGES SET.\n\r\n\r", ch );
            pObj->value[2] = atoi( argument );
            break;
        case 3:
            send_to_char( "SPELL TYPE SET.\n\r", ch );
            pObj->value[3] = skill_lookup( argument );
            break;
        }
        break;

    case ITEM_SCROLL:
    case ITEM_POTION:
    case ITEM_PILL:
        switch ( value_num )
        {
        default:
            show_help(ch,"ITEM_SCROLL_POTION_PILL");
            return FALSE;
        case 0:
            send_to_char( "SPELL LEVEL SET.\n\r\n\r", ch );
            pObj->value[0] = atoi( argument );
            break;
        case 1:
            send_to_char( "SPELL TYPE 1 SET.\n\r\n\r", ch );
            pObj->value[1] = skill_lookup( argument );
            break;
        case 2:
            send_to_char( "SPELL TYPE 2 SET.\n\r\n\r", ch );
            pObj->value[2] = skill_lookup( argument );
            break;
        case 3:
            send_to_char( "SPELL TYPE 3 SET.\n\r\n\r", ch );
            pObj->value[3] = skill_lookup( argument );
            break;
        case 4:
            send_to_char( "SPELL TYPE 4 SET.\n\r\n\r", ch );
            pObj->value[4] = skill_lookup( argument );
            break;
        }
        break;

    case ITEM_WEAPON:
        switch ( value_num )
        {
        default:
            show_help(ch,"ITEM_WEAPON");
            return FALSE;
        case 0:
            send_to_char( "WEAPON TYPE SET.\n\r\n\r", ch );
            pObj->value[0] = weapon_type( argument );
            break;
        case 1:
            send_to_char( "NUMBER OF DICE SET.\n\r\n\r", ch );
            pObj->value[1] = atoi( argument );
            break;
        case 2:
            send_to_char( "SIDES OF DICE SET.\n\r\n\r", ch );
            pObj->value[2] = atoi( argument );
            break;
        case 3:
            if ((value=table_find_name(argument,damagetype_table))!=NO_FLAG)
                pObj->value[3] = value;
            else {
                show_help(ch,"ITEM_WEAPON");
                return FALSE;
            }
            send_to_char( "DAMAGE MESSAGE SET.\n\r\n\r", ch );
            break;
        case 4:
            if ((value=option_find_name(argument,weaponflag_options,TRUE))
                    != NO_FLAG )
                TOGGLE_BIT(pObj->value[4], value);
            else
            {
                show_help(ch,"ITEM_WEAPON");
                return FALSE;
            }
            send_to_char( "WEAPON FLAGS SET.\n\r\n\r", ch );
            break;
        }
        break;

    case ITEM_CONTAINER:
        switch ( value_num )
        {
        default:
            show_help(ch,"ITEM_CONTAINER");
            return FALSE;
        case 0:
            send_to_char( "WEIGHT CAPACITY SET.\n\r\n\r", ch );
            pObj->value[0] = atoi( argument );
            break;
        case 1:
            if ((value=option_find_name(argument,container_options,TRUE))
                    != NO_FLAG )
                TOGGLE_BIT(pObj->value[1], value);
            else
            {
                show_help(ch,"ITEM_CONTAINER");
                return FALSE;
            }
            send_to_char( "CONTAINER TYPE SET.\n\r\n\r", ch );
            break;
        case 2:
            send_to_char( "KEY VNUM SET.\n\r\n\r", ch );
            pObj->value[2] = atoi( argument );
            break;
        case 3:
            send_to_char( "MAXIMUM WEIGHT PER ITEM SET.\n\r\n\r",ch );
            pObj->value[3]=atoi(argument);
            break;
        case 4:
            send_to_char( "WEIGHT MULTIPLIER SET.\n\r\n\r",ch );
            pObj->value[4]=atoi(argument);
            if(pObj->value[4]<1) pObj->value[4]=1;
            break;
        }
        break;

    case ITEM_DRINK_CON:
        switch ( value_num )
        {
        default:
            show_help(ch,"ITEM_DRINK");
            return FALSE;
        case 0:
            send_to_char( "MAXIMUM AMOUT OF LIQUID SET.\n\r\n\r", ch );
            pObj->value[0] = atoi( argument );
            break;
        case 1:
            send_to_char( "CURRENT AMOUNT OF LIQUID SET.\n\r\n\r", ch );
            pObj->value[1] = atoi( argument );
            break;
        case 2:
            if ((value=table_find_name(argument,liquid_table))!=NO_FLAG)
                pObj->value[2] = value;
            else
            {
                show_help(ch,"ITEM_DRINK");
                return FALSE;
            }
            send_to_char( "LIQUID TYPE SET.\n\r\n\r", ch );
            break;
        case 3:
            send_to_char( "POISON VALUE TOGGLED.\n\r\n\r", ch );
            pObj->value[3] = ( pObj->value[3] == 0 ) ? A : 0;
            break;
        }
        break;

    case ITEM_FOOD:
        switch ( value_num )
        {
        default:
            show_help(ch,"ITEM_FOOD");
            return FALSE;
        case 0:
            send_to_char( "FULL HOURS OF FOOD SET.\n\r\n\r", ch );
            pObj->value[0] = atoi( argument );
            break;
        case 1:
            send_to_char( "HUNGRY HOURS OF FOOD SET.\n\r\n\r", ch );
            pObj->value[1] = atoi( argument );
            break;
        case 3:
            send_to_char( "POISON VALUE TOGGLED.\n\r\n\r", ch );
            pObj->value[3] = ( pObj->value[3] == 0 ) ? A : 0;
            break;
        }
        break;

    case ITEM_MONEY:
        switch ( value_num )
        {
        default:
            show_help(ch,"ITEM_MONEY");
            return FALSE;
        case 0:
            send_to_char( "SILVER AMOUNT SET.\n\r\n\r", ch );
            pObj->value[0] = atoi( argument );
            break;
        case 1:
            send_to_char( "GOLD AMOUNT SET.\n\r\n\r", ch );
            pObj->value[1] = atoi( argument );
            break;
        }
        break;
    case ITEM_ARMOR:
        switch ( value_num )
        {
        default:
            show_help(ch,"ITEM_ARMOR");
            return FALSE;
        case 0:
            send_to_char( "ARMOR VS. PIERCE SET.\n\r\n\r", ch );
            pObj->value[0] = atoi( argument );
            break;
        case 1:
            send_to_char( "ARMOR VS. BASH SET.\n\r\n\r", ch );
            pObj->value[1] = atoi( argument );
            break;
        case 2:
            send_to_char( "ARMOR VS. SLASH SET.\n\r\n\r", ch );
            pObj->value[2] = atoi( argument );
            break;
        case 3:
            send_to_char( "ARMOR VS. EXOTIC SET.\n\r\n\r", ch );
            pObj->value[3] = atoi( argument );
            break;
        case 4:
            send_to_char( "BULK IS NOT IMPLEMENTED YET.\n\r\n\r", ch );
            break;
        }
        break;
    case ITEM_FOUNTAIN:
        switch ( value_num )
        {
        default:
            show_help(ch,"ITEM_FOUNTAIN");
            return FALSE;
        case 0:
            send_to_char( "MAXIMUM AMOUNT OF LIQUID SET.\n\r\n\r", ch );
            pObj->value[0] = atoi( argument );
            break;
        case 1:
            send_to_char( "CURRENT AMOUNT OF LIQUID SET.\n\r\n\r", ch );
            pObj->value[1] = atoi( argument );
            break;
        case 2:
            if ((value=table_find_name(argument,liquid_table))!=NO_FLAG)
                pObj->value[2]=value;
            else
            {
                show_help(ch,"ITEM_FOUNTAIN");
                return FALSE;
            }
            send_to_char( "LIQUID TYPE SET.\n\r\n\r", ch );
            break;
        case 3:
            send_to_char( "POISON VALUE TOGGLED.\n\r\n\r", ch );
            pObj->value[3] = ( pObj->value[3] == 0 ) ? A : 0;
            break;
        }
        break;
    case ITEM_PORTAL:
        switch ( value_num )
        {
        default:
            show_help(ch,"ITEM_PORTAL");
            return FALSE;
        case 0:
            send_to_char( "NUMBER OF CHARGES SET.\n\r\n\r", ch );
            pObj->value[0] = atoi( argument );
            break;
        case 1:
            if ((value=option_find_name(argument,exit_options,TRUE))
                    != NO_FLAG )
                TOGGLE_BIT(pObj->value[1], value);
            else
            {
                show_help(ch,"ITEM_PORTAL");
                return FALSE;
            }
            send_to_char( "EXIT FLAG TOGGLED.\n\r\n\r", ch );
            break;
        case 2:
            if ((value=option_find_name(argument,gate_options,TRUE))
                    != NO_FLAG )
                TOGGLE_BIT(pObj->value[2], value);
            else
            {
                show_help(ch,"ITEM_PORTAL");
                return FALSE;
            }
            send_to_char( "GATE FLAG TOGGLED.\n\r\n\r", ch );
            break;
        case 3:
            send_to_char( "ROOM NUMBER SET.\n\r\n\r", ch );
            pObj->value[3] = atoi( argument );
            break;
        case 4:
            send_to_char( "KEY VNUM SET.\n\r\n\r", ch );
            pObj->value[4] = atoi( argument );
            break;
        }
        break;
    case ITEM_FURNITURE:
        switch ( value_num )
        {
        default:
            show_help(ch,"ITEM_FURNITURE");
            return FALSE;
        case 0:
            send_to_char( "NUMBER OF SITPLACES SET.\n\r\n\r", ch );
            pObj->value[0] = atoi( argument );
            break;
        case 1:
            send_to_char( "SUPPORT WEIGHT SET.\n\r\n\r", ch );
            pObj->value[1] = atoi( argument );
            break;
        case 2:
            if ((value=option_find_name(argument,furniture_options,TRUE))
                    != NO_FLAG )
                TOGGLE_BIT(pObj->value[2], value);
            else
            {
                show_help(ch,"ITEM_FURNITURE");
                return FALSE;
            }
            send_to_char( "FURNITURE FLAG TOGGLED.\n\r\n\r", ch );
            break;
        case 3:
            send_to_char( "HEALING BONUS SET.\n\r\n\r", ch );
            pObj->value[3] = atoi( argument );
            break;
        case 4:
            send_to_char( "MANA BONUS SET.\n\r\n\r", ch );
            pObj->value[4] = atoi( argument );
            break;
        }
        break;
    case ITEM_QUEST:
        switch ( value_num )
        {
        default:
            show_help(ch,"ITEM_QUEST");
            return FALSE;
        case 0:
            if ((value=table_find_name(argument,questconsumption_table))
                    != NO_FLAG )
                pObj->value[0]=value;
            else
            {
                show_help(ch,"ITEM_QUEST");
                return FALSE;
            }
            send_to_char( "QUESTITEM CONSUMPTION TYPE SET.\n\r\n\r", ch );
            break;
        case 1:
            if ((value=table_find_name(argument,questeffects_table))
                    != NO_FLAG )
                pObj->value[1]=value;
            else
            {
                show_help(ch,"ITEM_QUEST");
                return FALSE;
            }
            send_to_char( "QUESTITEM EFFECT TYPE SET.\n\r\n\r", ch );
            break;
        case 2:
            send_to_char( "EFFECT VALUE SET.\n\r\n\r", ch );
            pObj->value[2] = atoi( argument );
            break;
        }
        break;
    case ITEM_JUKEBOX:
        switch (value_num) {
        default:
            show_help(ch,"ITEM_JUKEBOX");
            return FALSE;
        case 0:
            send_to_char( "PLAYLIST VALUE SET.\n\r\n\r", ch );
            pObj->value[0] = atoi( argument );
            break;
        }
    }

    show_obj_values( ch, pObj );

    return TRUE;
}



bool oedit_show( CHAR_DATA *ch, char *argument )
{
    OBJ_INDEX_DATA *pObj;
    char tmp[MAX_STRING_LENGTH],tmp2[MAX_STRING_LENGTH];
    char *t,*f;
    EFFECT_DATA *pef;
    int cnt,i;
    char *arg1,*arg2;

    EDIT_OBJ(ch, pObj);

    sprintf_to_char( ch, "{yName:{x          [%s] %s\n\r{yArea:{x          [%5d] [v%d] %s\n\r",
                     pObj->name,
                     pObj->deleted?"{Rdeleted{x":"",
                     !pObj->area ? -1        : pObj->area->vnum,
                     !pObj->area ? 0		: pObj->area->version,
                     !pObj->area ? "No Area" : pObj->area->name );

    sprintf_to_char( ch, "{yVnum:{x          [%5d] [%d counts]\n\r{yType:{x          [%s]\n\r",
                     pObj->vnum,pObj->count,
                     table_find_value(pObj->item_type,item_table));

    sprintf_to_char( ch, "{yMaterial:{x      [%s]\n\r{yLevel:{x         [%d]\n\r",
                     pObj->material,
                     pObj->level
                     );

    sprintf_to_char( ch, "{yWear flags:{x    [%s]\n\r",
                     option_string_from_long(pObj->wear_flags,wear_options));

    sprintf_to_char( ch, "{yExtra flags:{x   [%s]\n\r",
                     option_string(pObj->strbit_extra_flags,extra_options));

    sprintf_to_char( ch, "{yExtra2 flag:{x   [%s]\n\r",
                     option_string(pObj->strbit_extra_flags2,extra2_options));

    sprintf_to_char( ch, "{yWeight:{x        [%d]\n\r{yCost:{x          [%d]\n\r",
                     pObj->weight, pObj->cost );

    sprintf_to_char( ch, "{yCondition:{x     [%d]\n\r",pObj->condition );

    if(pObj->oprog_vnum!=0)
    {
        TCLPROG_CODE *oProg=get_oprog_index(pObj->oprog_vnum);

        if(oProg) {
            sprintf_to_char( ch, "{yProg instance:{x [%d] %s\n\r",
                             pObj->oprog_vnum,oProg->title);
        }
    }
    else send_to_char("{yProg instance:{x none\n\r",ch);

    if ( pObj->pueblo_picture[0])
        sprintf_to_char(ch, "{yPicture:{x     %s\n\r",
                        url(tmp,pObj->pueblo_picture,pObj->area,TRUE));

    if ( pObj->extra_descr )
    {
        EXTRA_DESCR_DATA *ed;

        send_to_char( "{yEx desc kwd:{x   ", ch );

        for ( ed = pObj->extra_descr; ed; ed = ed->next )
        {
            send_to_char( "[", ch );
            send_to_char( ed->keyword, ch );
            send_to_char( "]", ch );
        }

        send_to_char( "\n\r", ch );
    }

    sprintf_to_char( ch, "{yShort desc:{x    %s\n\r{yLong desc:{x\n\r       %s\n\r",
                     pObj->short_descr, pObj->description );

    show_obj_values( ch, pObj );

    for ( cnt = 0, pef = pObj->affected; pef; pef = pef->next )
    {
        if ( cnt == 0 )
        {
            send_to_char( "Number Effects  Modifier Type   Flags\n\r", ch );
            send_to_char( "------ -------- -------- ------ -----\n\r", ch );
        }

        arg1=arg2="";

        switch(pef->where) {
        default:
        case TO_OBJECT:
            t="object";
            f="not available";
            break;
        case TO_OBJECT2:
            t="obj2";
            f=option_find_value(pef->bitvector,extra2_options);
            switch (pef->bitvector) {
            case ITEM_ANTI_CLASS:
            case ITEM_CLASS_ONLY:

                strcpy(tmp2,"[");
                for(i=0;i<MAX_CLASS;i++)
                {
                    if(pef->arg1&(1<<i))
                    {
                        if(strlen(tmp2)!=1) strcat(tmp2," ");
                        strcat(tmp2,class_table[i].name);
                    }
                }
                strcat(tmp2,"]");
                arg1=tmp2;
                break;

            case ITEM_ANTI_RACE:
            case ITEM_RACE_ONLY:
            case ITEM_RACE_POISON:

                strcpy(tmp2,"[");
                for(i=0;race_table[i].name;i++)
                {
                    if(pef->arg1&(1<<i))
                    {
                        if(strlen(tmp2)!=1) strcat(tmp2," ");
                        strcat(tmp2,race_table[i].name);
                    }
                }
                strcat(tmp2,"]");
                arg1=tmp2;
                break;
            }
            break;
        case TO_IMMUNE:
            t="immune";
            f=option_find_value(pef->bitvector,imm_options);
            break;
        case TO_RESIST:
            t="resist";
            f=option_find_value(pef->bitvector,res_options);
            break;
        case TO_VULN:
            t="vuln";
            f=option_find_value(pef->bitvector,vuln_options);
            break;
        case TO_EFFECTS:
            t="effects";
            f=option_find_value(pef->bitvector,effect_options);
            break;
        case TO_EFFECTS2:
            t="eff2";
            f=option_find_value(pef->bitvector,effect2_options);
            break;
        case TO_SKILLS:
            t="skills";
            f=skill_name(pef->bitvector,NULL);
            break;
        }

        strcpy(tmp,f);

        sprintf_to_char( ch, "[%4d] %-8.8s %-8d %-6.6s %s %s %s\n\r", cnt,
                         option_find_value( pef->location, apply_options),
                         pef->modifier,
                         t,
                         tmp,
                         arg1,arg2 );
        cnt++;
    }

    show_properties(ch,pObj->property,"perm");

    return FALSE;
}


/*
 * Need to issue warning if flag isn't valid.
 */
bool oedit_addeffect( CHAR_DATA *ch, char *argument )
{
    OBJ_INDEX_DATA *pObj;
    EFFECT_DATA *pef;
    char loc[MAX_INPUT_LENGTH];
    char mod[MAX_INPUT_LENGTH];
    char cmd[MAX_INPUT_LENGTH];
    int type=TO_OBJECT;
    int bitvector=0;

    EDIT_OBJ(ch, pObj);

    argument = one_argument( argument, loc );
    argument = one_argument( argument, mod );
    argument = one_argument( argument, cmd );

    if ( loc[0] == '\0' || mod[0] == '\0' || !is_number( mod ) )
    {
        send_to_char( "Syntax:  addeffect <location> <#mod> [eff|imm|res|vuln|skill] [flags]\n\r", ch );
        return FALSE;
    }

    if (option_find_name(loc,apply_options,TRUE)==NO_FLAG) {
        send_to_char("OEdit: Unknown location flag, type ? LOCATION for help.\n\r", ch );
        return FALSE;
    }

    if(cmd[0]!='\0')
    {
        if(!str_prefix(cmd,"effect"))
        {
            type=TO_EFFECTS;
            if ((bitvector=option_find_name(argument,effect_options,TRUE))==NO_FLAG) {
                send_to_char("OEdit: Unknown effect, type ? EFFECT for help.\n\r", ch );
                return FALSE;
            }
        }
        else if(!str_prefix(cmd,"immune"))
        {
            type=TO_IMMUNE;
            if ((bitvector=option_find_name(argument,imm_options,TRUE))==NO_FLAG) {
                send_to_char("OEdit: Unknown effect, type ? IMMRESVULN for help.\n\r", ch );
                return FALSE;
            }
        }
        else if(!str_prefix(cmd,"resist"))
        {
            type=TO_RESIST;
            if((bitvector=option_find_name(argument,res_options,TRUE))==NO_FLAG)
            {
                send_to_char("OEdit: Unknown effect, type ? IMMRESVULN for help.\n\r", ch );
                return FALSE;
            }
        }
        else if(!str_prefix(cmd,"vulnerability"))
        {
            type=TO_VULN;
            if((bitvector=option_find_name(argument,vuln_options,TRUE))==NO_FLAG)
            {
                send_to_char("OEdit: Unknown effect, type ? IMMRESVUL for help.\n\r", ch );
                return FALSE;
            }
        }
        else if(!str_prefix(cmd,"skill"))
        {
            type=TO_SKILLS;
            if ((bitvector=skill_lookup(argument))==-1) {
                send_to_char("Not a known skill/spell\n\r",ch);
                return FALSE;
            }
        }
        else
        {
            send_to_char("OEdit: Unknown flag type.\n\r", ch );
            return FALSE;
        }
    }

    pef             =   new_effect();
    pef->where	    =   type;
    pef->location   =   option_find_name(loc,apply_options,TRUE);
    pef->modifier   =   atoi( mod );
    pef->level	    =	pObj->level;
    pef->type       =   -1;
    pef->duration   =   -1;
    pef->bitvector  =   bitvector;
    pef->next       =   pObj->affected;
    pObj->affected  =   pef;

    send_to_char( "Effect added.\n\r", ch);
    return TRUE;
}


/*
 * My thanks to Hans Hvidsten Birkeland and Noam Krendel(Walker)
 * for really teaching me how to manipulate pointers.
 */
bool oedit_deleffect( CHAR_DATA *ch, char *argument )
{
    OBJ_INDEX_DATA *pObj;
    EFFECT_DATA *pef;
    EFFECT_DATA *pef_next;
    char effect[MAX_STRING_LENGTH];
    int  value;
    int  cnt = 0;

    EDIT_OBJ(ch, pObj);

    one_argument( argument, effect );

    if ( !is_number( effect ) || effect[0] == '\0' )
    {
        send_to_char( "Syntax:  deleffect [#effect]\n\r", ch );
        return FALSE;
    }

    value = atoi( effect );

    if ( value < 0 )
    {
        send_to_char( "Only non-negative effect-numbers allowed.\n\r", ch );
        return FALSE;
    }

    if ( !( pef = pObj->affected ) )
    {
        send_to_char( "OEdit:  Non-existant effect.\n\r", ch );
        return FALSE;
    }

    if( value == 0 )	/* First case: Remove first effect */
    {
        pef = pObj->affected;
        pObj->affected = pef->next;
        if(pef->where==TO_OBJECT2)
        {
            if(pef->bitvector&(ITEM_ANTI_CLASS|ITEM_ANTI_RACE|
                               ITEM_CLASS_ONLY|ITEM_RACE_ONLY)) {
                STR_REMOVE_BIT(pObj->strbit_extra_flags2,ITEM_ANTI_CLASS);
                STR_REMOVE_BIT(pObj->strbit_extra_flags2,ITEM_ANTI_RACE);
                STR_REMOVE_BIT(pObj->strbit_extra_flags2,ITEM_CLASS_ONLY);
                STR_REMOVE_BIT(pObj->strbit_extra_flags2,ITEM_RACE_ONLY);
                //		pObj->extra_flags2&=~pef->bitvector;
            }
        }
        free_effect( pef );
    }
    else		/* Effect to remove is not the first */
    {
        while ( ( pef_next = pef->next ) && ( ++cnt < value ) )
            pef = pef_next;

        if( pef_next )		/* See if it's the next effect */
        {
            pef->next = pef_next->next;
            if(pef->where==TO_OBJECT2)
            {
                if(pef->bitvector&(ITEM_ANTI_CLASS|ITEM_ANTI_RACE|
                                   ITEM_CLASS_ONLY|ITEM_RACE_ONLY)) {
                    STR_REMOVE_BIT(pObj->strbit_extra_flags2,ITEM_ANTI_CLASS);
                    STR_REMOVE_BIT(pObj->strbit_extra_flags2,ITEM_ANTI_RACE);
                    STR_REMOVE_BIT(pObj->strbit_extra_flags2,ITEM_CLASS_ONLY);
                    STR_REMOVE_BIT(pObj->strbit_extra_flags2,ITEM_RACE_ONLY);
                    //		    pObj->extra_flags2&=~pef->bitvector;
                }
            }
            free_effect( pef_next );
        }
        else                                 /* Doesn't exist */
        {
            send_to_char( "No such effect.\n\r", ch );
            return FALSE;
        }
    }

    send_to_char( "Effect removed.\n\r", ch);
    return TRUE;
}



bool oedit_name( CHAR_DATA *ch, char *argument )
{
    OBJ_INDEX_DATA *pObj;

    EDIT_OBJ(ch, pObj);

    if ( argument[0] == '\0' )
    {
        send_to_char( "Syntax:  name [string]\n\r", ch );
        return FALSE;
    }

    free_string( pObj->name );
    pObj->name = str_dup( argument );

    send_to_char( "Name set.\n\r", ch);
    return TRUE;
}



bool oedit_short( CHAR_DATA *ch, char *argument )
{
    OBJ_INDEX_DATA *pObj;

    EDIT_OBJ(ch, pObj);

    if ( argument[0] == '\0' )
    {
        send_to_char( "Syntax:  short [string]\n\r", ch );
        return FALSE;
    }

    free_string( pObj->short_descr );
    pObj->short_descr = str_dup( argument );
    // Short descriptions can start with a capital
    //    pObj->short_descr[0] = LOWER( pObj->short_descr[0] );

    send_to_char( "Short description set.\n\r", ch);
    return TRUE;
}



bool oedit_long( CHAR_DATA *ch, char *argument )
{
    OBJ_INDEX_DATA *pObj;

    EDIT_OBJ(ch, pObj);

    if ( argument[0] == '\0' )
    {
        send_to_char( "Syntax:  long [string]\n\r", ch );
        return FALSE;
    }

    free_string( pObj->description );
    pObj->description = str_dup( argument );
    pObj->description[0] = UPPER( pObj->description[0] );

    send_to_char( "Long description set.\n\r", ch);

    return TRUE;
}



bool set_value( CHAR_DATA *ch, OBJ_INDEX_DATA *pObj, char *argument, int value )
{
    if ( argument[0] == '\0' )
    {
        set_obj_values( ch, pObj, -1, argument );
        return FALSE;
    }

    if ( set_obj_values( ch, pObj, value, argument ) )
        return TRUE;

    return FALSE;
}



/*****************************************************************************
 Name:		oedit_values
 Purpose:	Finds the object and sets its value.
 Called by:	The four valueX functions below.
 ****************************************************************************/
bool oedit_values( CHAR_DATA *ch, char *argument, int value )
{
    OBJ_INDEX_DATA *pObj;

    EDIT_OBJ(ch, pObj);

    if ( set_value( ch, pObj, argument, value ) )
        return TRUE;

    return FALSE;
}


bool oedit_value0( CHAR_DATA *ch, char *argument )
{
    if ( oedit_values( ch, argument, 0 ) )
        return TRUE;

    return FALSE;
}



bool oedit_value1( CHAR_DATA *ch, char *argument )
{
    if ( oedit_values( ch, argument, 1 ) )
        return TRUE;

    return FALSE;
}



bool oedit_value2( CHAR_DATA *ch, char *argument )
{
    if ( oedit_values( ch, argument, 2 ) )
        return TRUE;

    return FALSE;
}



bool oedit_value3( CHAR_DATA *ch, char *argument )
{
    if ( oedit_values( ch, argument, 3 ) )
        return TRUE;

    return FALSE;
}

bool oedit_value4( CHAR_DATA *ch, char *argument )
{
    if ( oedit_values( ch, argument, 4 ) )
        return TRUE;

    return FALSE;
}

bool oedit_weight( CHAR_DATA *ch, char *argument )
{
    OBJ_INDEX_DATA *pObj;

    EDIT_OBJ(ch, pObj);

    if ( argument[0] == '\0' || !is_number( argument ) )
    {
        send_to_char( "Syntax:  weight [number]\n\r", ch );
        return FALSE;
    }

    pObj->weight = atoi( argument );

    send_to_char( "Weight set.\n\r", ch);
    return TRUE;
}



bool oedit_cost( CHAR_DATA *ch, char *argument )
{
    OBJ_INDEX_DATA *pObj;

    EDIT_OBJ(ch, pObj);

    if ( argument[0] == '\0' || !is_number( argument ) )
    {
        send_to_char( "Syntax:  cost [number]\n\r", ch );
        return FALSE;
    }

    pObj->cost = atoi( argument );

    send_to_char( "Cost set.\n\r", ch);
    return TRUE;
}



bool oedit_create( CHAR_DATA *ch, char *argument )
{
    OBJ_INDEX_DATA *pObj;
    AREA_DATA *pArea;
    int value;
    int  iHash;

    if (argument[0]==0) {
        for (value=ch->in_room->area->lvnum;
             value<=ch->in_room->area->uvnum;value++)
            if ( !get_obj_index( value ) )
                break;
        if (value>ch->in_room->area->uvnum) {
            send_to_char("No free vnums found in this area.\n\r",ch);
            return FALSE;
        }
    } else
        value = atoi( argument );

    /* OLC 1.1b */
    if ( value <= 0 || value >= MAX_VNUMS )
    {
        char output[MAX_STRING_LENGTH];

        sprintf( output, "Syntax:  create [0 < vnum < %d]\n\r", MAX_VNUMS );
        send_to_char( output, ch );
        return FALSE;
    }

    pArea = get_vnum_area( value );
    if ( !pArea )
    {
        send_to_char( "OEdit:  That vnum is not assigned to an area.\n\r", ch );
        return FALSE;
    }

    if ( !IS_BUILDER( ch, pArea ) )
    {
        send_to_char( "OEdit:  Vnum in an area you cannot build in.\n\r", ch );
        return FALSE;
    }

    if ( get_obj_index( value ) )
    {
        send_to_char( "OEdit:  Object vnum already exists.\n\r", ch );
        return FALSE;
    }

    pObj			= new_obj_index();
    pObj->vnum			= value;
    pObj->area			= pArea;
    pArea->vnum_objects_in_use++;

    if ( value > max_vnum_obj )
        max_vnum_obj = value;

    if (ch->desc->editor == ED_MOBPROG) {
        TCLPROG_CODE *pCode2;

        pCode2=(TCLPROG_CODE *)ch->desc->pEdit;
        pCode2->edit=FALSE;
    }

    iHash			= value % MAX_KEY_HASH;
    pObj->next			= obj_index_hash[iHash];
    obj_index_hash[iHash]	= pObj;
    ch->desc->pEdit		= (void *)pObj;

    send_to_char( "Object Created.\n\r", ch );
    return TRUE;
}

bool oedit_clone( CHAR_DATA *ch, char *argument )
{
    OBJ_INDEX_DATA *pObj;
    OBJ_INDEX_DATA *pSource;
    EFFECT_DATA *pef,*pefNew;
    EXTRA_DESCR_DATA *pEd,*pEdNew;
    PROPERTY *prop;
    int  value;
    int  i;

    EDIT_OBJ( ch, pObj );

    value = atoi( argument );

    /* OLC 1.1b */
    if ( argument[0] == '\0' || value <= 0 || value >= MAX_VNUMS )
    {
        char output[MAX_STRING_LENGTH];

        sprintf( output, "Syntax:  clone [0 < vnum < %d]\n\r", MAX_VNUMS );
        send_to_char( output, ch );
        return FALSE;
    }

    if(!(pSource = get_obj_index( value )))
    {
        send_to_char( "Obj vnum doesn't exist.\n\r", ch );
        return FALSE;
    }

    /* Clear old object */
    free_string( pObj->name );
    free_string( pObj->short_descr );
    free_string( pObj->description );
    free_string( pObj->material );

    while( pObj->extra_descr )
    {
        pEd=pObj->extra_descr->next;
        free_extra_descr(pObj->extra_descr);
        pObj->extra_descr=pEd;
    }

    while(pObj->affected)
    {
        pef=pObj->affected->next;
        free_effect(pObj->affected);
        pObj->affected=pef;
    }

    /* Clone Object */
    pObj->name		= str_dup( pSource->name );
    pObj->short_descr	= str_dup( pSource->short_descr );
    pObj->description   = str_dup( pSource->description );
    pObj->material	= str_dup( pSource->material );
    pObj->item_type	= pSource->item_type;
    STR_COPY_STR(pObj->strbit_extra_flags,pSource->strbit_extra_flags,MAX_FLAGS);
    STR_COPY_STR(pObj->strbit_extra_flags2,pSource->strbit_extra_flags2,MAX_FLAGS);
    pObj->wear_flags	= pSource->wear_flags;
    pObj->level		= pSource->level;
    pObj->condition	= pSource->condition;
    pObj->weight	= pSource->weight;
    pObj->cost		= pSource->cost;
    for(i=0;i<5;i++)
        pObj->value[i]	= pSource->value[i];

    for(pEd=pSource->extra_descr;pEd;pEd=pEd->next)
    {
        pEdNew=new_extra_descr();
        pEdNew->keyword=str_dup(pEd->keyword);
        pEdNew->description=str_dup(pEd->description);
        pEdNew->next=pObj->extra_descr;
        pObj->extra_descr=pEdNew;
    }

    /* Copy effects */
    for(pef=pSource->affected;pef;pef=pef->next)
    {
        pefNew		= new_effect();
        pefNew->where	= pef->where;
        pefNew->type	= pef->type;
        pefNew->level	= pef->level;
        pefNew->duration= pef->duration;
        pefNew->location= pef->location;
        pefNew->modifier= pef->modifier;
        pefNew->bitvector= pef->bitvector;
        pefNew->arg1	= pef->arg1;
        pefNew->next	= pObj->affected;
        pObj->affected	= pefNew;
    }

    /* and the properties */
    for (prop = pSource->property; prop ; prop = prop->next)
        if (prop->sValue==NULL||prop->sValue[0]==0)
            SetDObjectProperty(pObj,prop->propIndex->type,
                               prop->propIndex->key,&prop->iValue);
        else
            SetDObjectProperty(pObj,prop->propIndex->type,
                               prop->propIndex->key,prop->sValue);

    send_to_char("Object cloned.\n\r", ch );

    return TRUE;
}

bool oedit_ed( CHAR_DATA *ch, char *argument )
{
    OBJ_INDEX_DATA *pObj;
    EXTRA_DESCR_DATA *ed;
    char command[MAX_INPUT_LENGTH];
    char keyword[MAX_INPUT_LENGTH];

    EDIT_OBJ(ch, pObj);

    argument = one_argument( argument, command );
    one_argument( argument, keyword );
    smash_case(keyword);

    if ( command[0] == '\0' )
    {
        send_to_char( "Syntax:  ed add [keyword]\n\r", ch );
        send_to_char( "         ed delete [keyword]\n\r", ch );
        send_to_char( "         ed edit [keyword]\n\r", ch );
        send_to_char( "         ed format [keyword]\n\r", ch );
        send_to_char( "         ed show [keyword]\n\r", ch );
        return FALSE;
    }

    if ( !str_cmp( command, "add" ) )
    {
        if ( keyword[0] == '\0' )
        {
            send_to_char( "Syntax:  ed add [keyword]\n\r", ch );
            return FALSE;
        }

        ed                  =   new_extra_descr();
        ed->keyword         =   str_dup( keyword );
        ed->next            =   pObj->extra_descr;
        pObj->extra_descr   =   ed;

        editor_start( ch, &ed->description );

        return TRUE;
    }

    if ( !str_cmp( command, "edit" ) )
    {
        if ( keyword[0] == '\0' )
        {
            send_to_char( "Syntax:  ed edit [keyword]\n\r", ch );
            return FALSE;
        }

        for ( ed = pObj->extra_descr; ed; ed = ed->next )
        {
            if ( is_name( keyword, ed->keyword ) )
                break;
        }

        if ( !ed )
        {
            send_to_char( "OEdit:  Extra description keyword not found.\n\r", ch );
            return FALSE;
        }

        editor_start( ch, &ed->description );

        return TRUE;
    }

    if ( !str_cmp( command, "delete" ) )
    {
        EXTRA_DESCR_DATA *ped = NULL;

        if ( keyword[0] == '\0' )
        {
            send_to_char( "Syntax:  ed delete [keyword]\n\r", ch );
            return FALSE;
        }

        for ( ed = pObj->extra_descr; ed; ed = ed->next )
        {
            if ( is_name( keyword, ed->keyword ) )
                break;
            ped = ed;
        }

        if ( !ed )
        {
            send_to_char( "OEdit:  Extra description keyword not found.\n\r", ch );
            return FALSE;
        }

        if ( !ped )
            pObj->extra_descr = ed->next;
        else
            ped->next = ed->next;

        free_extra_descr( ed );

        send_to_char( "Extra description deleted.\n\r", ch );
        return TRUE;
    }


    if ( !str_cmp( command, "format" ) )
    {
        if ( keyword[0] == '\0' )
        {
            send_to_char( "Syntax:  ed format [keyword]\n\r", ch );
            return FALSE;
        }

        for ( ed = pObj->extra_descr; ed; ed = ed->next )
        {
            if ( is_name( keyword, ed->keyword ) )
                break;
        }

        if ( !ed )
        {
            send_to_char( "OEdit:  Extra description keyword not found.\n\r", ch );
            return FALSE;
        }

        /* OLC 1.1b */
        if ( strlen(ed->description) >= (MAX_STRING_LENGTH - 4) )
        {
            send_to_char( "String too long to be formatted.\n\r", ch );
            return FALSE;
        }

        ed->description = format_string( ed->description );

        send_to_char( "Extra description formatted.\n\r", ch );
        return TRUE;
    }

    if(!str_cmp( command, "show" ) )
    {
        if ( keyword[0] == '\0' )
        {
            send_to_char( "Syntax:  ed format [keyword]\n\r", ch );
            return FALSE;
        }

        for ( ed = pObj->extra_descr; ed; ed = ed->next )
        {
            if ( is_name( keyword, ed->keyword ) )
                break;
        }

        if ( !ed )
        {
            send_to_char( "OEdit:  Extra description keyword not found.\n\r", ch );
            return FALSE;
        }

        send_to_char( ed->description, ch );

        return TRUE;
    }

    oedit_ed( ch, "" );
    return FALSE;
}

bool oedit_material( CHAR_DATA *ch, char *argument )
{
    OBJ_INDEX_DATA *pObj;

    EDIT_OBJ(ch, pObj);

    if ( argument[0] == '\0')
    {
        send_to_char( "Syntax:  material [materialname]\n\r", ch );
        return FALSE;
    }

    pObj->material = str_dup( argument );

    send_to_char( "Material set.\n\r", ch);
    return TRUE;
}

bool oedit_level( CHAR_DATA *ch, char *argument )
{
    OBJ_INDEX_DATA *pObj;

    EDIT_OBJ(ch, pObj);

    if ( argument[0] == '\0')
    {
        send_to_char( "Syntax:  level [objlevel]\n\r", ch );
        return FALSE;
    }

    pObj->level=URANGE(0,atoi(argument),UMIN(LEVEL_COUNCIL,get_trust(ch)));

    send_to_char( "Level set.\n\r", ch);
    return TRUE;
}

bool oedit_condition( CHAR_DATA *ch, char *argument )
{
    OBJ_INDEX_DATA *pObj;

    EDIT_OBJ(ch, pObj);

    if ( argument[0] == '\0')
    {
        send_to_char( "Syntax:  condition [conditionvalue]\n\r", ch );
        return FALSE;
    }

    pObj->condition=URANGE(0,atoi(argument),100);
    if(pObj->condition<10) pObj->condition=0;
    else if(pObj->condition<25) pObj->condition=10;
    else if(pObj->condition<50) pObj->condition=25;
    else if(pObj->condition<75) pObj->condition=50;
    else if(pObj->condition<90) pObj->condition=75;
    else if(pObj->condition<100) pObj->condition=90;
    else pObj->condition=100;

    send_to_char( "Condition set.\n\r", ch);
    return TRUE;
}

/*
 * Need to issue warning if flag isn't valid.
 */
bool oedit_classonly( CHAR_DATA *ch, char *argument ) {
    OBJ_INDEX_DATA *pObj;
    EFFECT_DATA *pef,*prev_pef;
    char arg[MAX_INPUT_LENGTH];
    int bitvector=0,class;

    EDIT_OBJ(ch, pObj);

    if ( argument[0] == '\0' ) {
        send_to_char( "Syntax:  classonly <class>\n\r", ch );
        return FALSE;
    }

    /* Make bitmask of classes */
    argument = one_argument( argument, arg );
    while(arg[0] != '\0') {
        if((class=class_lookup(arg))==-1) {
            send_to_char("OEdit: Unknown class.\n\r",ch);
            return FALSE;
        }

        bitvector|=(1<<class);

        argument = one_argument( argument, arg );
    }

    /* Search if effect is already added. */
    for(prev_pef=NULL,pef=pObj->affected;pef;prev_pef=pef,pef=pef->next) {
        if(pef->where==TO_OBJECT2
                && pef->bitvector==ITEM_CLASS_ONLY) break;
    }

    /* If effect not already added, add it now */
    if(!pef) {
        pef             =   new_effect();
        pef->where	=   TO_OBJECT2;
        pef->location   =   0;
        pef->modifier   =   0;
        pef->type       =   -1;
        pef->duration   =   -1;
        pef->bitvector  =   ITEM_CLASS_ONLY;
        pef->arg1	=   bitvector;
        pef->next       =   pObj->affected;
        pObj->affected  =   pef;

        STR_SET_BIT(pObj->strbit_extra_flags2,ITEM_CLASS_ONLY);

        send_to_char( "Class only effect added.\n\r", ch);
    }
    else
    {
        pef->arg1^=bitvector;
        if (pef->arg1) {
            send_to_char( "Class only effect toggled.\n\r", ch);
        } else {
            send_to_char( "Class only effect removed.\n\r", ch);
            if (prev_pef) {
                prev_pef->next=pef->next;
            } else {
                pObj->affected=pef->next;
            }
            free_effect(pef);
        }
    }

    return TRUE;
}

/*
 * Need to issue warning if flag isn't valid.
 */
bool oedit_raceonly( CHAR_DATA *ch, char *argument ) {
    OBJ_INDEX_DATA *pObj;
    EFFECT_DATA *pef,*prev_pef;
    char arg[MAX_INPUT_LENGTH];
    int bitvector=0,race;

    EDIT_OBJ(ch, pObj);

    if ( argument[0] == '\0' ) {
        send_to_char( "Syntax:  raceonly <race>\n\r", ch );
        return FALSE;
    }

    /* Make bitmask of classes */
    argument = one_argument( argument, arg );
    while(arg[0] != '\0') {
        if((race=race_lookup(arg))==-1) {
            send_to_char("OEdit: Unknown race.\n\r",ch);
            return FALSE;
        }

        bitvector|=(1<<race);

        argument = one_argument( argument, arg );
    }

    /* Search if effect is already added. */
    for(prev_pef=NULL,pef=pObj->affected;pef;prev_pef=pef,pef=pef->next) {
        if(pef->where==TO_OBJECT2
                && pef->bitvector==ITEM_RACE_ONLY) break;
    }

    /* If effect not already added, add it now */
    if(!pef) {
        pef             =   new_effect();
        pef->where	=   TO_OBJECT2;
        pef->location   =   0;
        pef->modifier   =   0;
        pef->type       =   -1;
        pef->duration   =   -1;
        pef->bitvector  =   ITEM_RACE_ONLY;
        pef->arg1	=   bitvector;
        pef->next       =   pObj->affected;
        pObj->affected  =   pef;

        STR_SET_BIT(pObj->strbit_extra_flags2,ITEM_RACE_ONLY);

        send_to_char( "Race only effect added.\n\r", ch);
    } else {
        pef->arg1^=bitvector;
        if (pef->arg1) {
            send_to_char( "Race only effect toggled.\n\r", ch);
        } else {
            send_to_char( "Race only effect removed.\n\r", ch);
            if (prev_pef) {
                prev_pef->next=pef->next;
            } else {
                pObj->affected=pef->next;
            }
            free_effect(pef);
        }
    }

    return TRUE;
}

bool oedit_racepoison( CHAR_DATA *ch, char *argument ) {
    OBJ_INDEX_DATA *pObj;
    EFFECT_DATA *pef,*prev_pef;
    char arg[MAX_INPUT_LENGTH];
    int bitvector=0,race;
    int modifier=100;

    EDIT_OBJ(ch, pObj);

    if ( argument[0] == '\0' ) {
        send_to_char( "Syntax:  racepoison <race>\n\r", ch );
        return FALSE;
    }

    /* Make bitmask of classes */
    argument = one_argument( argument, arg );
    while(arg[0] != '\0') {
        if (is_number(arg)) {
            modifier=atoi(arg);
        } else {
            if((race=race_lookup(arg))==-1) {
                send_to_char("OEdit: Unknown race.\n\r",ch);
                return FALSE;
            }

            bitvector|=(1<<race);
        }

        argument = one_argument( argument, arg );
    }

    /* Search if effect is already added. */
    for(prev_pef=NULL,pef=pObj->affected;pef;prev_pef=pef,pef=pef->next) {
        if(pef->where==TO_OBJECT2
                && pef->bitvector==ITEM_RACE_POISON) break;
    }

    /* If effect not already added, add it now */
    if(!pef) {
        pef             =   new_effect();
        pef->where	=   TO_OBJECT2;
        pef->location   =   0;
        pef->modifier   =   modifier;
        pef->type       =   gsn_poison;
        pef->bitvector  =   ITEM_RACE_POISON;
        pef->arg1	=   bitvector;
        pef->next       =   pObj->affected;
        pObj->affected  =   pef;

        STR_SET_BIT(pObj->strbit_extra_flags2,ITEM_RACE_POISON);

        send_to_char( "Race poison effect added.\n\r", ch);
    } else {
        pef->arg1^=bitvector;
        pef->modifier=modifier;
        if (pef->arg1) {
            send_to_char( "Race poison effect toggled.\n\r", ch);
        } else {
            send_to_char( "Race poison effect removed.\n\r", ch);
            if (prev_pef) {
                prev_pef->next=pef->next;
            } else {
                pObj->affected=pef->next;
            }
            free_effect(pef);
        }
    }

    return TRUE;
}

/*
 * Need to issue warning if flag isn't valid.
 */
bool oedit_anticlass( CHAR_DATA *ch, char *argument )
{
    OBJ_INDEX_DATA *pObj;
    EFFECT_DATA *pef,*prev_pef;
    char arg[MAX_INPUT_LENGTH];
    int bitvector=0,class;

    EDIT_OBJ(ch, pObj);

    if ( argument[0] == '\0' )
    {
        send_to_char( "Syntax:  anticlass <classes>\n\r", ch );
        return FALSE;
    }

    /* Make bitmask of classes */
    argument = one_argument( argument, arg );
    while(arg[0] != '\0')
    {
        if((class=class_lookup(arg))==-1)
        {
            send_to_char("OEdit: Unknown class.\n\r",ch);
            return FALSE;
        }

        bitvector|=(1<<class);

        argument = one_argument( argument, arg );
    }

    /* Search if effect is already added. */
    for(prev_pef=NULL,pef=pObj->affected;pef;prev_pef=pef,pef=pef->next) {
        if(pef->where==TO_OBJECT2
                && pef->bitvector==ITEM_ANTI_CLASS) break;
    }

    /* If effect not already added, add it now */
    if(!pef)
    {
        pef             =   new_effect();
        pef->where	=   TO_OBJECT2;
        pef->location   =   0;
        pef->modifier   =   0;
        pef->type       =   -1;
        pef->duration   =   -1;
        pef->bitvector  =   ITEM_ANTI_CLASS;
        pef->arg1	=   bitvector;
        pef->next       =   pObj->affected;
        pObj->affected  =   pef;

        STR_SET_BIT(pObj->strbit_extra_flags2,ITEM_ANTI_CLASS);

        send_to_char( "Anti class effect added.\n\r", ch);
    }
    else
    {
        pef->arg1^=bitvector;
        if (pef->arg1) {
            send_to_char( "Anti class effect toggled.\n\r", ch);
        } else {
            send_to_char( "Anti class effect removed.\n\r", ch);
            if (prev_pef) {
                prev_pef->next=pef->next;
            } else {
                pObj->affected=pef->next;
            }
            free_effect(pef);
        }
    }

    return TRUE;
}

/*
 * Need to issue warning if flag isn't valid.
 */
bool oedit_antirace( CHAR_DATA *ch, char *argument )
{
    OBJ_INDEX_DATA *pObj;
    EFFECT_DATA *pef,*prev_pef;
    char arg[MAX_INPUT_LENGTH];
    int bitvector=0,race;

    EDIT_OBJ(ch, pObj);

    if ( argument[0] == '\0' )
    {
        send_to_char( "Syntax:  antirace <races>\n\r", ch );
        return FALSE;
    }

    /* Make bitmask of classes */
    argument = one_argument( argument, arg );
    while(arg[0] != '\0')
    {
        if((race=race_lookup(arg))==-1)
        {
            send_to_char("OEdit: Unknown race.\n\r",ch);
            return FALSE;
        }

        bitvector|=(1<<race);

        argument = one_argument( argument, arg );
    }

    /* Search if effect is already added. */
    for(prev_pef=NULL,pef=pObj->affected;pef;prev_pef=pef,pef=pef->next) {
        if(pef->where==TO_OBJECT2
                && pef->bitvector==ITEM_ANTI_RACE) break;
    }

    /* If effect not already added, add it now */
    if(!pef)
    {
        pef             =   new_effect();
        pef->where	=   TO_OBJECT2;
        pef->location   =   0;
        pef->modifier   =   0;
        pef->type       =   -1;
        pef->duration   =   -1;
        pef->bitvector  =   ITEM_ANTI_RACE;
        pef->arg1	=   bitvector;
        pef->next       =   pObj->affected;
        pObj->affected  =   pef;

        STR_SET_BIT(pObj->strbit_extra_flags2,ITEM_ANTI_RACE);

        send_to_char( "Anti race effect added.\n\r", ch);
    }
    else
    {
        pef->arg1^=bitvector;
        if (pef->arg1) {
            send_to_char( "anti race effect toggled.\n\r", ch);
        } else {
            send_to_char( "Anti race effect removed.\n\r", ch);
            if (prev_pef) {
                prev_pef->next=pef->next;
            } else {
                pObj->affected=pef->next;
            }
            free_effect(pef);
        }
    }

    return TRUE;
}

bool oedit_delete( CHAR_DATA *ch, char *argument) {
    OBJ_INDEX_DATA *pObj;

    EDIT_OBJ(ch, pObj);

    pObj->deleted=!pObj->deleted;

    sprintf_to_char(ch,"Object is %s deleted.\n\r",
                    pObj->deleted?"now":"not anymore");
    return TRUE;
}

bool oedit_picture(CHAR_DATA *ch, char *argument) {
    OBJ_INDEX_DATA *pObj;
    char s[MSL],*p;
    int i;

    EDIT_OBJ(ch, pObj);
    free_string(pObj->pueblo_picture);

    p=argument;
    i=0;
    while (*p) {
        switch (*p) {
        case ' ': s[i++]='%';s[i++]='2';s[i++]='0';break;
        case '~': s[i++]='%';s[i++]='7';s[i++]='e';break;
        default : s[i++]=*p;
        }
        p++;
    }
    s[i]=0;
    pObj->pueblo_picture=str_dup(s);
    return TRUE;
}

bool oedit_property( CHAR_DATA *ch, char *argument) {
    OBJ_INDEX_DATA *pObj;
    char stype[MAX_STRING_LENGTH];
    char key[MAX_STRING_LENGTH];
    char *svalue;
    long l;
    int  i;
    bool b,delete;
    char c;

    EDIT_OBJ(ch, pObj);

    argument=one_argument(argument,key);
    argument=one_argument(argument,stype);
    svalue=argument;

    delete=str_cmp(svalue,"delete")?FALSE:TRUE;

    if (svalue==NULL||svalue[0]==0) {
        send_to_char("Usage: property <key> <type> <value>\n\r",ch);
        send_to_char("  Where <type> is \"bool\", \"int\", \"long\", \"char\","
                     "\"string\n\r",ch);
        return FALSE;
    }

    if (!does_property_exist_s(key,stype)) {
        send_to_char("Unknown property, see `{Wpropertylist{x` for overview.\n\r",ch);
        return FALSE;
    }

    switch (which_keyword(stype,"bool","int","long","char","string",NULL)) {
    case 1:
        if (delete)
            DeleteDObjectProperty(pObj,PROPERTY_BOOL,key);
        else {
            switch (which_keyword(svalue,"true","false",NULL)) {
            case 1:
                b=TRUE;
                break;
            case 2:
                b=FALSE;
                break;
            default:
                oedit_property(ch,"");
                return FALSE;
            }
            SetDObjectProperty(pObj,PROPERTY_BOOL,key,&b);
        }
        break;
    case 2:
        if (delete)
            DeleteDObjectProperty(pObj,PROPERTY_INT,key);
        else {
            i=atoi(svalue);
            SetDObjectProperty(pObj,PROPERTY_INT,key,&i);
        }
        break;
    case 3:
        if (delete)
            DeleteDObjectProperty(pObj,PROPERTY_LONG,key);
        else {
            l=atol(svalue);
            SetDObjectProperty(pObj,PROPERTY_LONG,key,&l);
        }
        break;
    case 4:
        if (delete)
            DeleteDObjectProperty(pObj,PROPERTY_CHAR,key);
        else {
            c=svalue[0];
            SetDObjectProperty(pObj,PROPERTY_CHAR,key,&c);
        }
        break;
    case 5:
        if (delete)
            DeleteDObjectProperty(pObj,PROPERTY_STRING,key);
        else
            SetDObjectProperty(pObj,PROPERTY_STRING,key,svalue);
        break;
    default:
        oedit_property(ch,"");
        return FALSE;
    }

    send_to_char("Done\n\r",ch);

    return TRUE;
}
bool oedit_proginstance( CHAR_DATA *ch, char *argument )
{
    OBJ_INDEX_DATA *pObj;
    int vnum;

    EDIT_OBJ(ch, pObj);

    if ( argument[0] == '\0' || !is_number( argument ) )
    {
        send_to_char( "Syntax:  proginstance [vnum]\n\r", ch );
        return FALSE;
    }

    vnum = atoi( argument );

    if(vnum==0)
    {
        pObj->oprog_vnum=0;
        send_to_char("Objprog removed.\n\r", ch);
        return TRUE;
    }

    if(!get_oprog_index(vnum)) {
        sprintf_to_char(ch,"Obj program with vnum %d does not exist.\n\r",vnum);
        return FALSE;
    }

    pObj->oprog_vnum=vnum;

    send_to_char( "Proginstance set.\n\r", ch);
    return TRUE;
}
