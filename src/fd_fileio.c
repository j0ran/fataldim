//
// $Id: fd_fileio.c,v 1.3 2006/04/07 20:54:20 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"
#include "db.h"

static char fd_getc(FILE *fp) {
    char c;

    c=getc(fp);

    if (c=='\n' && fp==fpArea) mud_data.currentline++;
    return c;
}

static int fd_ungetc(int c, FILE *fp) {
    if (c=='\n' && fp==fpArea) mud_data.currentline--;
    return ungetc(c,fp);
}

//
// Read a string from a file and store it in a static buffer (hence the temp).
//
char *fread_string_temp( FILE *fp ) {
    char *	plast;
    char	c;
    static char stringread[10+2*MAX_VNUMS/8];
				// Make it big. very big. MSL isn't enough.
				// MSL is only 4608 bytes. The reading of
				// the beenthere data is 2*MAX_VNUMS/8,
				// which is 8192 bytes.
    plast=stringread;

    /*
     * Skip blanks.
     * Read first char.
     */
    do {
	c=fd_getc(fp);
    } while (isspace(c));

    if ((*plast++=c)=='~')
	return &str_empty[0];

    for ( ;; ) {
	switch ( *plast = fd_getc(fp) ) {
	default:
	    plast++;
	    break;

	case EOF:
	    bugf("fread_string(): EOF");
	    abort();
	    exit(1);

	case '\n':
	    plast++;
	    *plast++ = '\r';
	    break;

	case '\r':
	    break;

	case '~':
	    plast++;
	    plast[-1]=0;
	    return stringread;
	}
    }
}

//
// Read and allocate space for a string from a file.
// These strings are read-only and shared.
// Strings are hashed:
//   each string prepended with hash pointer to prev string,
//   hash code is simply the string length.
//   this function takes 40% to 50% of boot-up time.
//
char *fread_string( FILE *fp ) {
    char *	p;
    char *	q;
    int	l;

    p=fread_string_temp(fp);
    l=strlen(p);
    if (l>=MSL-1) bugf("Reading a string %d>>MSL",l);
    q=allocate_perm_string(p);
    return q;
}

//
// Read a line of a string from a file and store it in a static buffer.
//
char *fread_string_eol_temp( FILE *fp ) {
    static bool	char_special[256-EOF];
    char *	plast;
    char	c;
    static char	stringread[MSL];

    plast=stringread;

    if (char_special[EOF-EOF]!=TRUE) {
	char_special[EOF -  EOF]=TRUE;
	char_special['\n' - EOF]=TRUE;
	char_special['\r' - EOF]=TRUE;
    }

    /*
     * Skip blanks.
     * Read first char.
     */
    do {
	c=fd_getc(fp);
    } while (isspace(c));

    if ((*plast++=c)=='\n')
	return &str_empty[0];

    for ( ;; ) {
	if ( !char_special[ (*plast++=fd_getc(fp)) - EOF ] )
	    continue;

	switch ( plast[-1] ) {
	default:
	    break;

	case EOF:
	    bugf("fread_string_eol(): got EOF" );
	    abort();
	    break;

	case '\n':
	case '\r':
	    plast[-1]=0;
	    return stringread;
	}
    }
}
char *fread_string_eol( FILE *fp ) {
    return allocate_perm_string(fread_string_eol_temp(fp));
}

//
// Read to end of line (for comments).
//
void fread_to_eol( FILE *fp ) {
    char	c;

    do {
	c=fd_getc(fp);
    }
    while (c!='\n' && c!='\r');

    do {
	c=fd_getc( fp );
    }
    while (c=='\n' || c=='\r');

    fd_ungetc(c,fp);
    return;
}

//
// Read one word (into static buffer).
//
char *fread_word( FILE *fp ) {
    static char	word[MAX_INPUT_LENGTH];
    char *	pword;
    char	cEnd;

    do {
	cEnd=fd_getc(fp);
    } while (isspace(cEnd));

    if (cEnd=='\'' || cEnd=='"') {
	pword=word;
    } else {
	word[0]=cEnd;
	pword=word+1;
	cEnd=' ';
    }

    for ( ; pword<word+MAX_INPUT_LENGTH; pword++ ) {
	*pword=fd_getc(fp);
	if (cEnd==' ' ? isspace(*pword) : *pword==cEnd) {
	    if (cEnd==' ')
		fd_ungetc(*pword,fp);
	    *pword=0;
	    return word;
	}
    }

    bugf("fread_word(): word too long.");
    abort();
}

//
// Read a letter from a file.
//
char fread_letter(FILE *fp) {
    char	c;

    do {
	c=fd_getc(fp);
    } while (isspace(c));

    return c;
}

//
// Put a letter back into a file.
//
void funread_letter(FILE *fp,char c) {
    fd_ungetc(c,fp);
}

//
// Read a signed long from a file.
// - First is checked for a + or - sign
// - Then at least one digit is expected
// - Then is read until a non-digit is found
//
long fread_long( FILE *fp ) {
    long	number;
    bool	sign=FALSE;
    char	c;

    number = 0;

    c=fread_letter(fp);
    if (c=='+') {
	c=fd_getc(fp);
    } else if (c=='-') {
	sign=TRUE;
	c=fd_getc(fp);
    }

    if (!isdigit(c)) {
	bugf("fread_long(): bad format '%c'",c);
	abort();
    }

    while (isdigit(c)) {
	number = number*10 + c-'0';
	c      = fd_getc(fp);
    }

    if (sign)
	number=0-number;

    if (c=='|')
	number+=fread_long(fp);
    else if (c!=' ')
	funread_letter(fp,c);

    return number;
}

//
// Read an integer from a file.
//
int fread_number(FILE *fp) {
    return fread_long(fp);
}

//
// Read a long-bitfield from a file.
//
long flag_convert(char letter) {
    long	bitsum=0;

    if (isupper(letter)) {
	bitsum=1;
	bitsum<<=letter-'A';
    } else if (islower(letter)) {
	bitsum = 1<<26; /* 2^26 */
	bitsum<<=letter-'a';
    }

    return bitsum;
}

long fread_flag(FILE *fp) {
    int		number;
    char	c;
    bool	sign=FALSE;

    number=0;

    c=fread_letter(fp);
    if (c=='+') {
	c=fd_getc(fp);
    } else if (c=='-') {
	sign=TRUE;
	c=fd_getc(fp);
    }

    if (!isdigit(c)) {
	while (isalpha(c)) {
	    number+=flag_convert(c);
	    c=fd_getc(fp);
	}
    }

    while (isdigit(c)) {
	number=number*10 + c-'0';
	c=fd_getc(fp);
    }

    if (c=='|')
	number+=fread_flag(fp);

    else if (c!=' ')
	fd_ungetc(c,fp);

    if (sign)
	number=0-number;

    return number;
}

