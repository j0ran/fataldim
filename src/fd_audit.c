//
// $Id: fd_audit.c,v 1.7 2006/08/25 09:43:19 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"
#include "interp.h"


bool check_weapon(CHAR_DATA *ch,OBJ_INDEX_DATA *obj,FILE *fp) {
    static int dices_amount[]={0,
	1,1,1,1,1,2,1,2,3,2,5,3,4,2,5,4,4,6,4,5,5,7,4,6,5,5,3,9,5,
	6,6,4,8,5,7,7,4,9,5,10,10,10,8,4,5,9,9,8,8,10,10,10,6,9,11,
	7,8,4,6,10,10,12,12,12,12,13,13,6,6,10,10,8,9,12,12,12,7,
	7,11,10,9,9,6,7,12,14,14,14,11,10,8,12};
    static int dices_sizes[]={0,
	1,2,3,4,5,3,7,4,3,5,2,4,3,7,3,4,4,3,5,4,4,3,6,4,5,5,9,3,6,
	5,5,8,4,7,5,5,10,4,8,4,4,4,5,11,9,5,5,6,6,5,5,5,9,6,5,8,7,
	15,10,6,6,5,5,5,5,5,5,12,12,7,7,9,8,6,6,6,11,11,7,8,9,9,14,
	12,7,6,6,6,8,9,12,8};
    int hitbonus=0,dambonus=0;
    float dam_normal,dam_obj;
    EFFECT_DATA *af;

    if (obj->item_type!=ITEM_WEAPON)
	return FALSE;

    af=obj->affected;
    while (af) {
	if (af->location==APPLY_HITROLL)
	    hitbonus+=af->modifier;
	if (af->location==APPLY_DAMROLL)
	    dambonus+=af->modifier;
	af=af->next;
    }

    dam_obj=(1.0+obj->value[1])*obj->value[2]/2.0;
    dam_normal=(1.0+dices_sizes[obj->level])*dices_amount[obj->level]/2.0;

    fprintf(fp,"%d,%d,\"%s\",\"%s\",\"%d/%d\",%dd%d,%1.1f,%1.1f,%dd%d,%1.1f,%1.2f,\"%s\"",
	obj->vnum,
	obj->level,
	weapon_name(obj->value[0]),
	obj->short_descr,
	hitbonus,dambonus,
	obj->value[1],obj->value[2],
	dam_obj,
	dam_obj+dambonus,
	dices_amount[obj->level],dices_sizes[obj->level],
	dam_normal,
	dam_obj/dam_normal,
	option_string_from_long(obj->value[4],weaponflag_options)
	);
    fprintf(fp,"\n");
    return FALSE;
}


bool check_armour(CHAR_DATA *ch,OBJ_INDEX_DATA *obj,FILE *fp) {
    static int armor_pierce[]={
	1,1,1,1,1,2,2,3,3,3,3,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,8,8,8,
	9,9,9,10,10,10,10,11,11,11,11,12,12,12,13,13,13,13,14,14,
	14,14,14,15,15,15,15,16,16,16,16,16,17,17,17,17,17,18,18,
	18,18,19,19,19,20,20,20,20,21,21,21,22,22,22,22,23,23,23,
	23,23,24,24,24,24 };
    static int armor_bash[]={
	1,1,1,1,1,1,2,2,3,3,3,3,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,8,8,
	8,9,9,9,10,10,10,10,11,11,11,11,12,12,12,13,13,13,13,14,14,
	14,14,14,15,15,15,15,16,16,16,16,16,17,17,17,17,17,18,18,
	18,18,19,19,19,20,20,20,20,21,21,21,22,22,22,22,23,23,23,
	23,23,24,24,24 };
    static int armor_slash[]={
	1,1,1,1,1,1,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,8,
	8,8,9,9,9,10,10,10,10,11,11,11,11,12,12,12,13,13,13,13,14,
	14,14,14,14,15,15,15,15,16,16,16,16,16,17,17,17,17,17,18,
	18,18,18,19,19,19,20,20,20,20,21,21,21,22,22,22,22,23,23,
	23,23,23,24,24 };
    static int armor_exotic[]={
	0,0,0,0,0,0,0,0,0,0,1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,4,4,4,
	4,4,4,4,4,4,5,5,5,5,6,6,6,6,6,6,6,7,7,7,7,8,9,9,9,9,10,10,
	10,10,11,12,12,12,12,13,14,14,14,14,15,15,15,15,15,15,15,
	16,16,16,16,16,16,16,17,17,17,17,18,19,19,19,19,20 };
    int ac_obj,ac_normal;

    ac_obj=obj->value[0]+obj->value[1]+obj->value[2]+obj->value[3];
    ac_normal=armor_pierce[obj->level]+armor_bash[obj->level]+
		armor_slash[obj->level]+armor_exotic[obj->level];
    fprintf(fp,"%d,%d,\"%s\",%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%1.2f\n",
	obj->vnum,
	obj->level,
	obj->short_descr,
	obj->value[0],
	obj->value[1],
	obj->value[2],
	obj->value[3],
	ac_obj,
	armor_pierce[obj->level],
	armor_bash[obj->level],
	armor_slash[obj->level],
	armor_exotic[obj->level],
	ac_normal,
	1.0*ac_obj/ac_normal
	);
    return FALSE;
}


void check_objects(CHAR_DATA *ch,int lower,int upper) {
    OBJ_INDEX_DATA *obj;
    int i;
    FILE *fpweapon,*fparmour;

    send_to_char("checking objects.\n\r",ch);
    if ((fpweapon=fopen("weapons.csv","w"))!=NULL &&
	(fparmour=fopen("armour.csv","w"))!=NULL) {
	fprintf(fpweapon,"vnum,level,type,short,bonus,dam,avg,avg+bonus,dam,avg,ratio,flags\n");
	fprintf(fparmour,"vnum,level,short,pierce,bash,slash,exotic,total,pierce,bash,slash,exotic,total,ratio\n");
	for (i=lower;i<=upper;i++) {
	    if ((obj=get_obj_index(i))!=NULL) {
		switch (obj->item_type) {
		case ITEM_WEAPON:
		    check_weapon(ch,obj,fpweapon);
		    break;
		case ITEM_ARMOR:
		    check_armour(ch,obj,fparmour);
		    break;
		}
	    }
	}
	fclose(fparmour);
	fclose(fpweapon);
    }
}

void do_audit(CHAR_DATA *ch, char *argument) {
    char arg1[MSL],arg2[MSL],arg3[MSL];
    int lower,upper;

    if (argument[0]==0) {
	send_to_char("Syntax: audit <mobiles|objects|rooms> <lower vnum> <higher vnum>\n\r",ch);
	return;
    }

    argument=one_argument(argument,arg1);
    argument=one_argument(argument,arg2);
    argument=one_argument(argument,arg3);

    if (!is_number(arg2) || !is_number(arg3)) {
	do_function(ch,&do_audit,"");
	return;
    }

    lower=atoi(arg2);
    upper=atoi(arg3);

    switch(which_keyword(arg1,"objects","mobiles","rooms",NULL)) {
	default:
	    do_function(ch,&do_audit,"");
	    break;
	case 1:
	    check_objects(ch,lower,upper);
	    break;
	case 2:
	    break;
	case 3:
	    break;
    }
}
