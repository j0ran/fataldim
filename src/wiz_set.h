//
// $Id: wiz_set.h,v 1.3 2006/08/25 09:43:21 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef INCLUDED_WIZ_SET_H
#define INCLUDED_WIZ_SET_H

void	set_skill		(CHAR_DATA *ch, char *argument);
void	set_mob			(CHAR_DATA *ch, char *argument);
void	set_object		(CHAR_DATA *ch, char *argument);
void	set_room		(CHAR_DATA *ch, char *argument);
void	set_clan		(CHAR_DATA *ch, char *argument);
void	set_weapon		(CHAR_DATA *ch, char *argument);
void	set_effect		(CHAR_DATA *ch, char *argument);
void	set_object_property	(CHAR_DATA *ch,OBJ_DATA *obj,char *argument);
void	set_room_property	(CHAR_DATA *ch,ROOM_INDEX_DATA *room,
				 char *argument);
void	set_mob_property	(CHAR_DATA *ch,CHAR_DATA *victim,
				 char *argument);
void	set_object_extra	(CHAR_DATA *ch,OBJ_DATA *obj,char *argument);
void	set_object_wear		(CHAR_DATA *ch,OBJ_DATA *obj,char *argument);
void	set_object_owner	(CHAR_DATA *ch,OBJ_DATA *obj,char *newowner);
void	set_mud_logging		(CHAR_DATA *ch,char *argument);

#endif	// INCLUDED_WIZ_SET_H
