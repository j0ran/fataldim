//
// $Id: merc.h,v 1.431 2009/04/22 18:25:38 jodocus Exp $ */
//
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1996 Russ Taylor			   *
*	ROM has been brought to you by the ROM consortium		   *
*	    Russ Taylor (rtaylor@pacinfo.com)				   *
*	    Gabrielle Taylor (gtaylor@pacinfo.com)			   *
*	    Brian Moore (rom@rom.efn.org)				   *
*	By using this code, you have agreed to follow the terms of the	   *
*	ROM license, in the file Rom24/doc/rom.license			   *
***************************************************************************/

#ifndef INCLUDED_MERC_H
#define INCLUDED_MERC_H

//
// System includes. Just to make sure we have everything :-)
//
#include <sys/errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/uio.h>

#include <sys/resource.h>

#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <regex.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <arpa/telnet.h>

#ifdef NEEDS_TCL
#include <tcl.h>
extern Tcl_Interp *interp;
extern struct _progData *progData;
#endif

#ifdef THREADED_DNS
#include <pthread.h>
#endif

#ifdef NOTDEF
#include <malloc.h>
#include <io.h>
#include <dir.h>
#endif

#ifdef NEEDS_CRYPT_H
#include <crypt.h>
#endif

#ifdef HAS_MCCP
#include <zlib.h>
#define COMPRESS_BUF_SIZE	16384
#endif

#include "contrib/contrib.h"

#define DECLARE_DO_FUN( fun )		DO_FUN    fun
#define DECLARE_SPEC_FUN( fun )		SPEC_FUN  fun
#define DECLARE_SPELL_FUN( fun )	SPELL_FUN fun
#define DECLARE_WEAROFF_FUN( fun )	WEAROFF_FUN fun
#ifdef HAS_HTTP
#define DECLARE_HTML_FUN( fun )		HTML_FUN  fun
#endif
#ifdef HAS_POP3
#define DECLARE_POP3_FUN( fun )		POP3_FUN  fun
#endif
#define DECLARE_EVENT_FUN( fun )	EVENT_FUN fun

/*
 * Short scalar types.
 */
#if	!defined(FALSE)
#define FALSE	 0
#endif

#if	!defined(TRUE)
#define TRUE	 1
#endif

typedef unsigned char			bool;
typedef char				byte;
typedef unsigned char			ubyte;

#ifndef TEST
#define MUDHOST "FatalDim"		/* For POP3 and SMTP server */
#else
#define MUDHOST "FatalTest"		/* For POP3 and SMTP server */
#endif

/*
 * Structure types.
 */
#define ROOM_DATA ROOM_INDEX_DATA
typedef struct	mud_data		MUD_DATA;
typedef struct	effect_data		EFFECT_DATA;
typedef struct	area_data		AREA_DATA;
typedef struct	ban_data		BAN_DATA;
typedef struct	lockout_data		LOCKOUT_DATA;
typedef struct 	buf_type	 	BUFFER;
typedef struct	char_data		CHAR_DATA;
typedef struct	descriptor_data		DESCRIPTOR_DATA;
typedef struct	exit_data		EXIT_DATA;
typedef struct	extra_descr_data	EXTRA_DESCR_DATA;
typedef struct	help_data		HELP_DATA;
typedef struct	event_data		EVENT_DATA;
typedef struct	mem_data		MEM_DATA;
typedef struct	mob_index_data		MOB_INDEX_DATA;
typedef struct	note_data		NOTE_DATA;
typedef struct  notebook_data           NOTEBK_DATA;
typedef struct	obj_data		OBJ_DATA;
typedef struct	obj_index_data		OBJ_INDEX_DATA;
typedef struct	pc_data			PC_DATA;
typedef struct  gen_data		GEN_DATA;
typedef struct	reset_data		RESET_DATA;
typedef struct	room_index_data		ROOM_INDEX_DATA;
typedef struct  room_effect_data	ROOM_EFFECT_DATA;
typedef struct	shop_data		SHOP_DATA;
typedef struct	time_info_data		TIME_INFO_DATA;
typedef struct	weather_data		WEATHER_DATA;
typedef struct  clanmember_type		CLANMEMBER_TYPE;
typedef struct  clan_info		CLAN_INFO;
typedef struct  rank_info		RANK_INFO;
typedef struct  player_info		PLAYER_INFO;
#ifdef HAS_POP3
typedef struct  note_ref		NOTE_REF;
#endif
typedef struct  property_type           PROPERTY;
typedef struct  property_index_type     PROPERTY_INDEX_TYPE;
typedef struct  social_type		SOCIAL_TYPE;
typedef struct  alias_type		ALIAS_TYPE;
typedef struct  skilltable_type		SKILLTABLE_TYPE;
typedef struct  fs_entry		FS_ENTRY;
typedef struct  job_type		JOB_TYPE;
typedef struct	tclprog_list		TCLPROG_LIST;
typedef struct	tclprog_code		TCLPROG_CODE;
typedef struct	table_type		TABLE_TYPE;
typedef struct	option_type		OPTION_TYPE;
typedef struct	obj_weight_info		OBJ_WEIGHT_INFO;
typedef struct	lore_responses		LORE_RESPONSES;
typedef struct	channel_data		CHANNEL_DATA;
typedef struct	notes_data		NOTES_DATA;
typedef struct	badname_data		BADNAME_DATA;

/*
 * description types
 */
#define DESTYPE_MUD  1
#ifdef HAS_HTTP
#define DESTYPE_HTTP 2
#endif
#ifdef HAS_POP3
#define DESTYPE_POP3 3
#endif

/*
 * notebook flags
 */
#define NOTEBOOK_NONE		0
#define NOTEBOOK_NOGIVE		(A)


/*
 * Function types.
 */
typedef	void DO_FUN	( CHAR_DATA *ch, char *argument );
typedef bool SPEC_FUN	( CHAR_DATA *ch );
typedef void PHASE_FUN	( CHAR_DATA *ch );
typedef void SPELL_FUN	( int sn, int level, CHAR_DATA *ch, void *vo,
				int target );
typedef void WEAROFF_FUN ( CHAR_DATA *ch );
#ifdef HAS_HTTP
typedef void HTML_FUN   ( DESCRIPTOR_DATA *d,char *argument );
#endif
#ifdef HAS_POP3
typedef void POP3_FUN	( DESCRIPTOR_DATA *d,char *argument );
#endif
typedef void EVENT_FUN	( EVENT_DATA *event);


/*
 * String and memory management parameters.
 */
#define	MAX_SOCIAL_HASH		  256
#define	MAX_KEY_HASH		 1024
#ifndef MAX_STRING_LENGTH
#define MAX_STRING_LENGTH	 16384
#endif
#define MAX_INPUT_LENGTH	  256
#define PAGELEN			   22
#define MAX_HUNT_DEPTH		  150
#define MAX_HUNT_SIZE		 1000
#define MSL	MAX_STRING_LENGTH
#define MIL	MAX_INPUT_LENGTH


/*
 * Game parameters.
 * Increase the max'es if you add more of something.
 * Adjust the pulse numbers to suit yourself.
 */
#define MAX_VNUMS		65000
#define MAX_FLAGS		   20	// off/res/imm/vuln flags
#define MAX_SKILL		  256
#define MAX_GROUP		   28
#define MAX_IN_GROUP		   21
#define MAX_ALIAS		   20
#define MAX_CLASS		    4
#define MAX_EXT_CLASS     	   30
#define MAX_EXT_CLASS_SKILL  20
#define MAX_PC_RACE		    8
#define MAX_BONUS_SKILLS	    5 /* Do not change */
#define MAX_DAMAGE_MESSAGE	   44
#define MAX_SWEAR		   15
#define MAX_SWEARREPLY		    8
#define MAX_MOBKILL_SIZE	   50
#define MAX_TIMEZONES		    4
#define	MAX_JOBS		   30

#define PULSE_PER_SECOND	    4
#define PULSE_TAGSTART		  (30 * PULSE_PER_SECOND)
#define PULSE_REBOOT		  (30 * PULSE_PER_SECOND)
#define PULSE_VIOLENCE		  ( 3 * PULSE_PER_SECOND)
#define PULSE_MOBILE		  ( 4 * PULSE_PER_SECOND)
#define PULSE_MUSIC		  ( 6 * PULSE_PER_SECOND)
#ifdef TEST
#define PULSE_TICK		  (10 * PULSE_PER_SECOND)
#else
#define PULSE_TICK		  (45 * PULSE_PER_SECOND)
#endif
#define PULSE_AREA		  (60 * PULSE_PER_SECOND)
#define PULSE_ASAVE		  (1800 * PULSE_PER_SECOND)
#define PULSE_HOUR		  (3600 * PULSE_PER_SECOND)
#define PULSE_UNDERWATER	  (20 * PULSE_PER_SECOND)

#define MAX_LEVEL	100		/* implementor */
#define MAX_LEVEL1	(MAX_LEVEL - 1)	/* creator */
#define MAX_LEVEL2	(MAX_LEVEL - 2)	/* supreme being */
#define MAX_LEVEL3	(MAX_LEVEL - 3)	/* deity */
#define MAX_LEVEL4	(MAX_LEVEL - 4)	/* god */
#define MAX_LEVEL5	(MAX_LEVEL - 5)	/* immortal */
#define MAX_LEVEL6	(MAX_LEVEL - 6)	/* demigod */
#define MAX_LEVEL7	(MAX_LEVEL - 7)	/* angel */
#define MAX_LEVEL8	(MAX_LEVEL - 8)	/* avatar */
#define MAX_LEVEL9	(MAX_LEVEL - 9) /* hero */
#define LEVEL_HERO	MAX_LEVEL9
#define LEVEL_IMMORTAL	MAX_LEVEL8
#define LEVEL_COUNCIL	MAX_LEVEL1


//
// table-type
//
struct table_type {
    char	*name;
    int		value;
};

struct option_type {
    char	*name;
    int		value;
    bool	settable;
    char	*help;
};

/*
 * event-based stuff
 */
struct event_data {
    bool	valid;
    EVENT_DATA *next;
    int		delay;		// time left before event goes off
    CHAR_DATA *	ch;		// character which is in the event
    CHAR_DATA *	victim;		// possible victim, can be NULL
    OBJ_DATA *	object;		// possible object, can be NULL
    EVENT_FUN *	fun;		// which function to call
    void *	data;		// event-specific data
};

/*
 * Mud status structure.
 */

#define REBOOT_NONE	1
#define REBOOT_REBOOT	2
#define REBOOT_SHUTDOWN	3
#define REBOOT_COPYOVER	4


// channels
#define CHANNEL_GOSSIP		(0)
#define CHANNEL_MUSIC		(1)
#define CHANNEL_AUCTION		(2)
#define CHANNEL_QUESTION	(3)
#define CHANNEL_ANSWER		(4)
#define CHANNEL_QUEST		(5)
#define CHANNEL_QUOTE		(6)
#define CHANNEL_GRATS		(7)
#define CHANNEL_PRAY		(8)
#define CHANNEL_SHOUT		(9)
#define CHANNEL_YELL		(10)
#define CHANNEL_ANNOUNCE	(11)
#define CHANNEL_GTELL		(12)
#define CHANNEL_HEROTALK	(13)
#define CHANNEL_CLANTALK	(14)
#define CHANNEL_IMMTALK		(15)
#define MAX_CHANNELS		(16)	// update this if you add one


struct channel_data {
    char *	name;
    char *	prefixme;
    char *	prefixthem;
    char *	property;
    char *	colour;
    int		bit;
    bool	areaonly;
    bool	grouponly;
    bool	clanonly;
    bool	immonly;
    bool	heroonly;
    bool	awakeonly;
    int		minlevellisten;
};


MOB_INDEX_DATA * mobkill_table[MAX_MOBKILL_SIZE];


/*
 * PROPERTIES!
 * see property.h for the values of type
*/
struct	property_index_type {
    bool	valid;
    PROPERTY_INDEX_TYPE	*next;
    char	*key;
    int		type;
};

struct  property_type {
    PROPERTY_INDEX_TYPE	*propIndex;
    bool	valid;
    long        iValue;
    char        *sValue;
    PROPERTY    *next;  /* next property */
};

/*
 * Jobs
 */
struct job_type {
    bool	valid;
    char	*name;
    int		vnum;
    char	commands[MAX_FLAGS];
    char	channels[MAX_FLAGS];
    char	*note_targets;
    JOB_TYPE	*next;
};

struct buf_type
{
    BUFFER *    next;
    bool        valid;
    int      	state;  /* error state of the buffer */
    int      	size;   /* size in k */
    char *      string; /* buffer's string */
};



/*
 * Time and weather stuff.
 */
#define SUN_DARK		    0
#define SUN_RISE		    1
#define SUN_LIGHT		    2
#define SUN_SET			    3

#define SKY_CLOUDLESS		    0
#define SKY_CLOUDY		    1
#define SKY_RAINING		    2
#define SKY_LIGHTNING		    3

struct	time_info_data
{
    int		hour;
    int		day;
    int		month;
    int		year;
};

struct	weather_data
{
    int		mmhg;
    int		change;
    int		sky;
    int		sunlight;
};



/*
 * Connected state for a channel.
 */
#define CON_GET_NAME			 1
#define CON_GET_OLD_PASSWORD		 2
#define CON_CONFIRM_NEW_NAME		 3
#define CON_GET_NEW_PASSWORD		 4
#define CON_CONFIRM_NEW_PASSWORD	 5
#define CON_GET_NEW_RACE		 6
#define CON_GET_NEW_SEX			 7
#define CON_GET_NEW_CLASS		 8
#define CON_GET_ALIGNMENT		 9
#define CON_DEFAULT_CHOICE		10
#define CON_GEN_GROUPS			11
#define CON_PICK_WEAPON			12
#define CON_READ_IMOTD			13
#define CON_READ_MOTD			14
#define CON_BREAK_CONNECT		15
#define CON_GET_EMAIL			16
#define CON_GET_ANSI			17
#define CON_GET_SPEC_CLASS		18
#define CON_SPECIALIZE_CHOICE		19
#define CON_GEN_SPECGROUPS		20
#define CON_SCREEN_CREATION		21
#define CON_SCREEN_CUSTOMIZE		22
#define CON_SCREEN_SPECIALIZE		23
#define CON_READ_NEWBIEINFO		24
#define CON_COPYOVER_RECOVER		25
#define CON_PLAYING			26
#ifdef HAS_ALTS
#define CON_CHOOSE_ALT			27
#define CON_BEING_UBERALT		28
#endif

#ifdef HAS_HTTP
#define CON_HTTP			50
#define CON_HTTP_CLOSE			51
#endif
#ifdef HAS_POP3
#define CON_POP3_GET_USER		52
#define CON_POP3_GET_PASS		53
#define CON_POP3_CONNECTED		54
#endif
#define CON_LOGGING			55


#ifdef THREADED_DNS
#ifdef INET6
typedef char		HOST_DATA[16];
#else
typedef char		HOST_DATA[4];
#endif
#else
typedef char *		HOST_DATA;
#endif

/*
 * Descriptor (channel) structure.
 */
struct	descriptor_data
{
    DESCRIPTOR_DATA *	next;
    DESCRIPTOR_DATA *	snoop_by;
#ifdef HAS_ALTS
    CHAR_DATA *		uberalt;
#endif
    CHAR_DATA *		character;
    CHAR_DATA *		original;
#ifdef HAS_POP3
    CHAR_DATA *		pop3_char;
#endif
    bool		valid;
    bool		ipv6;
    HOST_DATA		Host;
    int			descriptor;
    int			connected;
    bool		fcommand;
    char		*pinbuf;
    #define INBUFSIZE	4*MAX_INPUT_LENGTH
    #define INCOMMSIZE	MAX_INPUT_LENGTH
    char		inbuf		[INBUFSIZE];
    char		incomm		[INCOMMSIZE];
    char		inlast		[INCOMMSIZE];
    int			repeat;
    char *		outbuf;
    int			outsize;
    int			outtop;
    int			lines;		// number of lines on a page, actually
    char *		showstr_head;
    char *		showstr_point;

    void *              pEdit;		/* OLC */
    char **		pStringBegin;
    char *		pStringEnd;
    int			currentLinenumber;
    int			editor;		/* OLC */
    time_t		idle;
    bool		pueblo:1;	// Pueblo
#ifdef HAS_MCCP
    bool		mccp:1;		// Mud Compression Control Protocol
    bool		mccp2:1;	// Mud Compression Control Protocol 2
    unsigned char	compressing;
    z_stream *		out_compress;
    unsigned char *	out_compress_buf;
#endif
    char *		terminaltype;
    char		telnet_options[256/8];	// all telnet-options
    bool		mxp:1;		// Mud eXtention Protocol
    bool		msp:1;		// Mud Sound Protocol
    bool		go_ahead:1;	// Send Go Ahead string

    long		bytessend;	// statistics: bytes send
    long		bytesreceived;	// statistics: bytes received
#ifdef HAS_MCCP
    long		uncompressedsend;// statistics: bytes send uncompressed
    long		compressedsend;	// statistics: bytes send compressed
#endif
};

/*
 * Attribute bonus structures.
 */
struct	str_app_type
{
    int	tohit;
    int	todam;
    int	carry;
    int	wield;
};

struct	int_app_type
{
    int	learn;
};

struct	wis_app_type
{
    int	practice;
};

struct	dex_app_type
{
    int	defensive;
};

struct	con_app_type
{
    int	hitp;
    int	shock;
};



/*
 * TO types for act.
 */
#define TO_ROOM		    0
#define TO_NOTVICT	    1
#define TO_VICT		    2
#define TO_CHAR		    3
#define TO_ALL		    4



/*
 * Help table types.
 */
struct	help_data
{
    bool	valid;
    HELP_DATA *	next;
    AREA_DATA *	area;		/* OLC 1.1b */
    int		level;
    char *	keyword;
    char *	text;
    bool	deleted;
};

/*
 * Shop types.
 */
#define MAX_TRADE	 5

#define SHOP_NORMAL	0
#define SHOP_QUEST	1

struct	shop_data
{
    bool	valid;
    SHOP_DATA *	next;			/* Next shop in list		*/
    int		keeper;			/* Vnum of shop keeper mob	*/
    int		buy_type [MAX_TRADE];	/* Item types shop will buy	*/
    int		profit_buy;		/* Cost multiplier for buying	*/
    int		profit_sell;		/* Cost multiplier for selling	*/
    int		open_hour;		/* First opening hour		*/
    int		close_hour;		/* First closing hour		*/
    bool	type;			/* No silver but quest points as payment */
};



/*
 * Per-class stuff.
 */

#define MAX_GUILD 	3
#define MAX_STATS 	5
#define STAT_STR 	0
#define STAT_INT	1
#define STAT_WIS	2
#define STAT_DEX	3
#define STAT_CON	4

/*
 * Creation points values
 */

struct	class_type
{
    char *	name;			/* the full name of the class */
    char 	who_name	[4];	/* Three-letter name for 'who'	*/
    int		points;			/* Number of cp for specialization */
    int		needed_points;		/* Number of points needed for customization */
    int		spec_off;		/* This class is a specialization of */
    int		spec_level;		/* Level on witch this clas can spec */
    int		attr_prime;		/* Prime attribute		*/
//  int		guild[MAX_GUILD];	/* Vnum of guild rooms		*/
    int		thac0_00;		/* Thac0 for level  0		*/
    int		thac0_32;		/* Thac0 for level 32		*/
    int		hp_min;			/* Min hp gained on leveling	*/
    int		hp_max;			/* Max hp gained on leveling	*/
    int		mana_min;		/* Min mana gained on leveling */
    int		mana_max;		/* Max mana gained on leveling */
    bool	fMana;			/* Class gains mana on level	*/
    char *	base_group;		/* base skills gained		*/
    char *	default_group;		/* default skills gained	*/
    char *	default_weapon;		/* default assigned weapon 	*/
};

struct item_type
{
    int		type;
    char *	name;
};

struct weapon_type
{
    char *	name;
    int		vnum;
    int		type;
    int		*gsn;
};

struct wiznet_type
{
    char *	name;
    long 	flag;
    int		level;
};

struct attack_type
{
    char *	name;			/* name */
    char *	noun;			/* message */
    int   	damage;			/* damage class */
};

struct race_type
{
    char *	name;			/* call name of the race */
    bool	pc_race;		/* can be chosen by pcs */
    char	strbit_act[MAX_FLAGS];	/* act bits for the race */
    char	strbit_eff[MAX_FLAGS];	/* eff bits for the race */
    char	strbit_eff2[MAX_FLAGS];	/* eff bits for the race */
    char	strbit_off[MAX_FLAGS];	/* off bits for the race */
    char	strbit_imm[MAX_FLAGS];	/* imm bits for the race */
    char        strbit_res[MAX_FLAGS];	/* res bits for the race */
    char	strbit_vuln[MAX_FLAGS];	/* vuln bits for the race */
    long	form;			/* default form flag for the race */
    long	parts;			/* default parts for the race */
};


struct pc_race_type  /* additional data for pc races */
{
    char *	name;			/* MUST be in race_type */
    char 	who_name[6];
    int		points;			/* cost in points of the race */
    int		class_mult[MAX_CLASS];	/* exp multiplier for class, * 100 */
    int		mana_mult;		/* Race mana/l multiplier */
    int		hp_mult;		/* Race hp/l multiplier */
    int		move_mult;		/* Race move/l multiplier */
    char *	skills[5];		/* bonus skills for the race */
    int 	stats[MAX_STATS];	/* starting stats */
    int		max_stats[MAX_STATS];	/* maximum stats */
    int		size;			/* eff bits for the race */
};


struct spec_type
{
    char * 	name;			/* special function name */
    SPEC_FUN *	function;		/* the function */
};



/*
 * Data structure for notes.
 */

#define NOTE_NOTE	0
#define NOTE_IDEA	1
#define NOTE_PENALTY	2
#define NOTE_NEWS	3
#define NOTE_CHANGES	4
#define NOTE_BUG	5
#define NOTE_TYPO	6
#define NOTE_MCNOTE	7
#define MAX_NOTES	8

struct notes_data {
    int		type;
    NOTE_DATA	*list;
    time_t	ttl;
    int		min_level;
    char *	posting_jobs;

    char *	name;
    char *	description;
    char *	single;
    char *	multiple;
};

struct	note_data
{
    NOTE_DATA *	next;
    bool 	valid;
    int		type;
    char *	sender;
    char *	date;
    char *	to_list;
    char *	subject;
    char *	text;
    time_t  	date_stamp;
};

struct notebook_data
{
    NOTEBK_DATA *next;
    bool	valid;
    char *      subject;
    char *      text;
    int		flags;
    time_t	created;
    time_t	modified;
};

/*
 * POP3 notelist structure
 */
#ifdef HAS_POP3
struct note_ref
{
    NOTE_REF    *next;	/* Next noteref */
    bool        valid;
    NOTE_DATA   *note;	/* Reference to the note (can be NULL) */
    int		size;   /* The size of the note */
    bool	deleted;/* Is note marked for deletion? */
};
#endif

/*
 * An effect.
 */
struct	effect_data
{
    EFFECT_DATA *	next;
    bool		valid;
    int			where;
    int			type;
    int			level;
    int			duration;
    int			location;
    int			modifier;
    int			bitvector;    // we only have one bit per effect
    long		casted_by;    /* will have id of char_data structure */
    int			arg1;
};

/* where definitions */
#define TO_EFFECTS	0
#define TO_OBJECT	1
#define TO_IMMUNE	2
#define TO_RESIST	3
#define TO_VULN		4
#define TO_WEAPON	5
#define TO_EFFECTS2	6	/* Effected2 mob flags and effects */
#define TO_OBJECT2	7	/* Extra2 object flags */
#define TO_ROOM1	8	/* Room effect on room, room_flags */
#define TO_ROOM2	9	/* Room effect on room, extra_flags */
#define TO_EXIT1	10	/* Room effect on exit */
#define TO_SKILLS	11	/* skill bonus */


/*
 * Clan member structure.
 */

#define UPPER_CLAN_LEVEL 35
#define LOWER_CLAN_LEVEL 5

#define MAX_CLAN_RANK	5	// 6 if clan gods are used

#define CLAN_NORMAL	0
#define CLAN_ELITE	1
#define CLAN_MASTER	2
#define CLAN_KNIGHT	3
#define CLAN_LEADER	4
//#define CLAN_IMMORTAL	5

struct clanmember_type {
    bool		valid;
    CLANMEMBER_TYPE *	next;
    CLANMEMBER_TYPE *	next_member;
    CLAN_INFO *		clan_info;
    RANK_INFO *		rank_info;
    char *		playername;
    CHAR_DATA *		player;
    time_t		lastonline;
};

struct rank_info {
    bool	valid;
    RANK_INFO *	next;
    int		rank;
    char *	rank_name;
    char *	who_name;
};

/*
 * Clan info structure.
 */
struct clan_info {
    bool	valid;
    CLAN_INFO * next;
    CLANMEMBER_TYPE *members;
    char *	name;
    char *	who_name;
    int 	hall;
    int		recall;		/* recall room for clan's */
    bool	independent;	/* true for loners */
    char *	description;
    char *	url;		// website
    long	coins;		/* Clan account */
    bool	deleted;	/* if clan has been removed from the game */
};


/*
 * Player info structure.
 */
struct player_info {
    int		level;
    time_t	lastlogin;
    time_t	idle;
    bool	online;
    char	email[MSL];
    char	icq[MSL];
    char	msn[MSL];
    char	aim[MSL];
    char	yahoo[MSL];
    char	xmpp[MSL];
    char	homepage[MSL];
    int		race;
    int		class;
    bool	publish;
};

//
// global configuration
//
struct mud_data {
    bool	wizlock;
    int		wizlock_violations;
    char *	wizlock_by;
    time_t	wizlock_since;

    bool	newlock;
    int		newlock_violations;
    char *	newlock_by;
    time_t	newlock_since;

    bool	dnslock;
    char *	dnslock_by;
    time_t	dnslock_since;

    time_t	clearcounter_since;
    char *	clearcounter_by;

#ifdef HAS_HTTP
    int		banhttp_violations;
#endif
#ifdef HAS_POP3
    int		banpop3_violations;
#endif
    int		banmud_violations;
    int		banplayer_violations;
    int		bansite_violations;
    int		bannew_violations;

    time_t	boottime;
    char *	boot_time;
    char *	reboot_by;
    bool	reboot_now;
    int		reboot_type;
    int		reboot_timer;
    int		reboot_pulse;

    int		connections;
#ifdef HAS_POP3
    int		pop3_connections;
#endif
#ifdef HAS_HTTP
    int		http_who_connections;
    int		http_help_connections;
    int		http_errors;
#endif
    int		players_total;
    int		players_new;
    int		players_deleted;

    int		poll_id;
    bool	poll_open;
    int		poll_teller[8];
    char	poll_question[MAX_STRING_LENGTH];
    char	poll_choice[8][MAX_STRING_LENGTH];
    bool	poll_choiceact[7];
    int		poll_totalvotes;

    int		time_offsets	[MAX_TIMEZONES];
    char *	time_zones	[MAX_TIMEZONES]; // name displayed to users
    char *	time_zonenames	[MAX_TIMEZONES]; // names to tzset(3)

    int		max_string;  // advisory amount of static string space for next reboot

    int		mud_port;
    int		http_port;
    int		pop3_port;
#ifndef NO_INET4
    int		control4_mud;
#endif
#ifdef INET6
    int		control6_mud;
#endif
#ifdef HAS_HTTP
#ifndef NO_INET4
    int		control4_http;
#endif
#ifdef INET6
    int		control6_http;
#endif
#endif
#ifdef HAS_POP3
#ifndef NO_INET4
    int		control4_pop3;
#endif
#ifdef INET6
    int		control6_pop3;
#endif
#endif
    bool	copyover;

    bool	socials_changed;

    bool	logAllPlayers;
    bool	logAllMobs;

    bool	no_timestamp_log;

    // level restrictions
    int		newbie_channel_level	[MAX_CHANNELS];
    char *	newbie_channel_by	[MAX_CHANNELS];
    time_t	newbie_channel_since	[MAX_CHANNELS];
    int		newbie_room_level;
    char *	newbie_room_by;
    time_t	newbie_room_since;
    int		newbie_notes_level	[MAX_NOTES];
    char *	newbie_notes_by		[MAX_NOTES];
    time_t	newbie_notes_since	[MAX_NOTES];
    int		poll_level;
    char *	poll_by;
    time_t	poll_since;

    // startup arguments
    bool	nogame;
    int		argc;
    char **	argv;
    char *	startup_dir;
    char *	work_dir;
    char *	exe_file;
    char *	exe_name;

    // directories
    char *	player_dir;
    char *	hardcore_dir;
    char *	area_dir;
    char *	room_dir;
    char *	log_dir;
    char *	area_import;
    char *	config_dir;
    char *	notesroot_dir;

    char *	currentareafile;
    int		currentline;

    char *	area_lst;
    char *	copyover_file;
    char *	shutdown_file;
    char *	newbie_file;
    char *	social_file;
    char *	ban_file;
    char *	badname_file;
    char *	clan_file;
    char *	job_file;
    char *	property_file;
    char *	swear_file;
    char *	music_file;
    char *	poll_file;
    char *	pinky_file;
    char *	statistics_file;
    char *	wholist_file;
    char *	i3_config;
    char *	mud_config;

    char *	notes_dir[MAX_NOTES];
};

/***************************************************************************
 *                                                                         *
 *                   VALUES OF INTEREST TO AREA BUILDERS                   *
 *                   (Start of section ... sstart here)                     *
 *                                                                         *
 ***************************************************************************/

/*
 * Well known mob virtual numbers.
 * Defined in #MOBILES.
 */
#define MOB_VNUM_FIDO		   3090
#define MOB_VNUM_CITYGUARD	   3060
#define MOB_VNUM_VAMPIRE	   3404
#define MOB_VNUM_ZOMBIE		     12
#ifdef I3
#define MOB_VNUM_I3_ZOMBIE	      3
#endif

#define MOB_VNUM_PATROLMAN	   2106
#define GROUP_VNUM_TROLLS	   2100
#define GROUP_VNUM_OGRES	   2101


/* RT ASCII conversions -- used so we can have letters in this file */

#define A		  	1
#define B			2
#define C			4
#define D			8
#define E			16
#define F			32
#define G			64
#define H			128

#define I			256
#define J			512
#define K		        1024
#define L		 	2048
#define M			4096
#define N		 	8192
#define O			16384
#define P			32768

#define Q			65536
#define R			131072
#define S			262144
#define T			524288
#define U			1048576
#define V			2097152
#define W			4194304
#define X			8388608

#define Y			16777216
#define Z			33554432
#define aa			67108864 	/* doubled due to conflicts */
#define bb			134217728
#define cc			268435456
#define dd			536870912
#define ee			1073741824

/*
 * ACT bits for mobs.
 * Used in #MOBILES.
 */
#define ACT_NONE		(0)
#define ACT_IS_NPC		(1)		/* Auto set for mobs	*/
#define ACT_SENTINEL	    	(2)		/* Stays in one room	*/
#define ACT_SCAVENGER	      	(3)		/* Picks up objects	*/
#define ACT_REMOVE_OBJ		(4)		/* buy's noremove objects */
#define ACT_MOBPROG_IGNOREPOS	(5)		/* ignore default position */
						/* for random triggers */
#define ACT_AGGRESSIVE		(6)    		/* Attacks PC's		*/
#define ACT_STAY_AREA		(7)		/* Won't leave area	*/
#define ACT_WIMPY		(8)
#define ACT_PET			(9)		/* Auto set for pets	*/
#define ACT_TRAIN		(10)		/* Can train PC's	*/
#define ACT_PRACTICE		(11)		/* Can practice PC's	*/
#define ACT_UNSEENSERVANT	(12)		/* Unseen servant	*/
#define ACT_IGNORENEGHEALING	(13)		// ignore negative healing
#define ACT_CORRUPT             (14)		// is a shopkeeper corrupt
#define ACT_UNDEAD		(15)
#define ACT_MUDWANDERER		(16)
#define ACT_CLERIC		(17)
#define ACT_MAGE		(18)
#define ACT_THIEF		(19)
#define ACT_WARRIOR		(20)
#define ACT_NOALIGN		(21)
#define ACT_NOPURGE		(22)
#define ACT_OUTDOORS		(23)
//efine ACT_                    (24)	// free!
#define ACT_INDOORS		(25)
#define ACT_NOQUEST             (26)
#define ACT_IS_HEALER		(27)
#define ACT_GAIN		(28)
#define ACT_UPDATE_ALWAYS	(29)
#define ACT_IS_CHANGER		(30)
#define ACT_PEACEFULL		(31)		/* this one never fights back */
#define ACT_PC_BLOCKS_RESPAWN	(32)
#define ACT_TCL_AGGRESSIVE	(33)
#define ACT_DOOR_WANDERER	(34)
#define ACT_LOCK_WANDERER	(35)


/* damage classes */
#define DAM_NONE                0
#define DAM_BASH                1
#define DAM_PIERCE              2
#define DAM_SLASH               3
#define DAM_FIRE                4
#define DAM_COLD                5
#define DAM_LIGHTNING           6
#define DAM_ACID                7
#define DAM_POISON              8
#define DAM_NEGATIVE            9
#define DAM_HOLY                10
#define DAM_ENERGY              11
#define DAM_MENTAL              12
#define DAM_DISEASE             13
#define DAM_DROWNING            14
#define DAM_LIGHT		15
#define DAM_OTHER               16
#define DAM_HARM		17
#define DAM_CHARM		18
#define DAM_SOUND		19

/* OFF bits for mobiles */
// keep in mind that there is currently room for 8*MAX_FLAGS flags.
// see mob_index_data -> strbit_off
#define OFF_NONE		(0)
#define OFF_AREA_ATTACK         (1)
#define OFF_BACKSTAB            (2)
#define OFF_BASH                (3)
#define OFF_BERSERK             (4)
#define OFF_DISARM              (5)
#define OFF_DODGE               (6)
#define OFF_FADE                (7)
#define OFF_FAST                (8)
#define OFF_KICK                (9)
#define OFF_KICK_DIRT           (10)
#define OFF_PARRY               (11)
#define OFF_RESCUE              (12)
#define OFF_TAIL                (13)
#define OFF_TRIP                (14)
#define OFF_CRUSH               (15)
#define ASSIST_ALL              (16)
#define ASSIST_ALIGN            (17)
#define ASSIST_RACE             (18)
#define ASSIST_PLAYERS          (19)
#define ASSIST_GUARD            (20)
#define ASSIST_VNUM             (21)
#define OFF_REMEMBER            (22)
#define OFF_HUNTER              (23)
#define OFF_HEADBUTT            (24)
#define OFF_EYEPOKE		(25)

/* return values for check_imm */
#define IS_NORMAL		0
#define IS_IMMUNE		1
#define IS_RESISTANT		2
#define IS_VULNERABLE		3

/* IMM bits for mobs */
// keep in mind that there is currently room for 8*MAXFLAGS flags.
// see mob_index_data -> strbit_imm
#define IMM_NONE		(0)
#define IMM_SUMMON              (1)
#define IMM_CHARM               (2)
#define IMM_MAGIC               (3)
#define IMM_WEAPON              (4)
#define IMM_BASH                (5)
#define IMM_PIERCE              (6)
#define IMM_SLASH               (7)
#define IMM_FIRE                (8)
#define IMM_COLD                (9)
#define IMM_LIGHTNING           (10)
#define IMM_ACID                (11)
#define IMM_POISON              (12)
#define IMM_NEGATIVE            (13)
#define IMM_HOLY                (14)
#define IMM_ENERGY              (15)
#define IMM_MENTAL              (16)
#define IMM_DISEASE             (17)
#define IMM_DROWNING            (18)
#define IMM_LIGHT               (19)
#define IMM_SOUND               (20)
//efine IMM_                    (21)    // free!!
//efine IMM_                    (22)    // free!!
//efine IMM_                    (23)    // free!!
#define IMM_WOOD                (24)
#define IMM_SILVER              (25)
#define IMM_IRON                (26)

/* RES bits for mobs */
// keep in mind that there is currently room for 8*MAXFLAGS flags.
// see mob_index_data -> strbit_res
#define RES_NONE                (0)
#define RES_SUMMON              (1)
#define RES_CHARM               (2)
#define RES_MAGIC               (3)
#define RES_WEAPON              (4)
#define RES_BASH                (5)
#define RES_PIERCE              (6)
#define RES_SLASH               (7)
#define RES_FIRE                (8)
#define RES_COLD                (9)
#define RES_LIGHTNING           (10)
#define RES_ACID                (11)
#define RES_POISON              (12)
#define RES_NEGATIVE            (13)
#define RES_HOLY                (14)
#define RES_ENERGY              (15)
#define RES_MENTAL              (16)
#define RES_DISEASE             (17)
#define RES_DROWNING            (18)
#define RES_LIGHT               (19)
#define RES_SOUND               (20)
//efine RES_                    (21)    // free!!
//efine RES_                    (22)    // free!!
//efine RES_                    (23)    // free!!
#define RES_WOOD                (24)
#define RES_SILVER              (25)
#define RES_IRON                (26)

/* VULN bits for mobs */
// keep in mind that there is currently room for 8*MAXFLAGS flags.
// see mob_index_data -> strbit_vuln
#define VULN_NONE               (0)
#define VULN_SUMMON             (1)
#define VULN_CHARM              (2)
#define VULN_MAGIC              (3)
#define VULN_WEAPON             (4)
#define VULN_BASH               (5)
#define VULN_PIERCE             (6)
#define VULN_SLASH              (7)
#define VULN_FIRE               (8)
#define VULN_COLD               (9)
#define VULN_LIGHTNING          (10)
#define VULN_ACID               (11)
#define VULN_POISON             (12)
#define VULN_NEGATIVE           (13)
#define VULN_HOLY               (14)
#define VULN_ENERGY             (15)
#define VULN_MENTAL             (16)
#define VULN_DISEASE            (17)
#define VULN_DROWNING           (18)
#define VULN_LIGHT              (19)
#define VULN_SOUND              (20)
//efine VULN_                   (21)    // free!!
//efine VULN_                   (22)    // free!!
//efine VULN_                   (23)    // free!!
#define VULN_WOOD               (24)
#define VULN_SILVER             (25)
#define VULN_IRON               (26)

/* body form */
#define FORM_EDIBLE             (A)
#define FORM_POISON             (B)
#define FORM_MAGICAL            (C)
#define FORM_INSTANT_DECAY      (D)
#define FORM_OTHER              (E)  /* defined by material bit */
//efine FORM_			(F)	// free!!

/* actual form */
#define FORM_ANIMAL             (G)
#define FORM_SENTIENT           (H)
#define FORM_UNDEAD             (I)
#define FORM_CONSTRUCT          (J)
#define FORM_MIST               (K)
#define FORM_INTANGIBLE         (L)

#define FORM_BIPED              (M)
#define FORM_CENTAUR            (N)
#define FORM_INSECT             (O)
#define FORM_SPIDER             (P)
#define FORM_CRUSTACEAN         (Q)
#define FORM_WORM               (R)
#define FORM_BLOB		(S)
//efine FORM_			(T)	// free!!
//efine FORM_			(U)	// free!!

#define FORM_MAMMAL             (V)
#define FORM_BIRD               (W)
#define FORM_REPTILE            (X)
#define FORM_SNAKE              (Y)
#define FORM_DRAGON             (Z)
#define FORM_AMPHIBIAN          (aa)
#define FORM_FISH               (bb)
#define FORM_COLD_BLOOD		(cc)
//efine FORM_			(dd)	// free!!
//efine FORM_			(ee)	// free!!

/* body parts */
#define PART_HEAD               (A)
#define PART_ARMS               (B)
#define PART_LEGS               (C)
#define PART_HEART              (D)
#define PART_BRAINS             (E)
#define PART_GUTS               (F)
#define PART_HANDS              (G)
#define PART_FEET               (H)
#define PART_FINGERS            (I)
#define PART_EAR                (J)
#define PART_EYE		(K)
#define PART_LONG_TONGUE        (L)
#define PART_EYESTALKS          (M)
#define PART_TENTACLES          (N)
#define PART_FINS               (O)
#define PART_WINGS              (P)
#define PART_TAIL               (Q)
//efine PART_			(R)	// free!!
//efine PART_			(S)	// free!!
//efine PART_			(T)	// free!!
/* for combat */
#define PART_CLAWS              (U)
#define PART_FANGS              (V)
#define PART_HORNS              (W)
#define PART_SCALES             (X)
#define PART_TUSKS		(Y)
//efine PART_			(Z)	// free!!
//efine PART_			(aa)	// free!!
//efine PART_			(bb)	// free!!
//efine PART_			(cc)	// free!!
//efine PART_			(dd)	// free!!
//efine PART_			(ee)	// free!!


/*
 * Bits for phasedskill_flags
 */
#define PHASED_NONE		(0)
#define PHASED_DOESOWNDAMAGE	(A)	// Does phased-function have its own
					// damage functions
#define PHASED_TAKEAFTERCARE	(B)	// Does phased-function have to clean
					// up some mess if target is destroyed

/*
 * Bits for 'affected_by'.
 * Used in #MOBILES.
 */
// keep in mind that there is currently room for 8*MAX_FLAGS flags.
// see mob_index_data -> strbit_affected_by
#define EFF_NONE		(0)
#define EFF_BLIND		(1)
#define EFF_INVISIBLE		(2)
#define EFF_DETECT_EVIL		(3)
#define EFF_DETECT_INVIS	(4)
#define EFF_DETECT_MAGIC	(5)
#define EFF_DETECT_HIDDEN	(6)
#define EFF_DETECT_GOOD		(7)
#define EFF_SANCTUARY		(8)
#define EFF_FAERIE_FIRE		(9)
#define EFF_INFRARED		(10)
#define EFF_CURSE		(11)
#define EFF_BLESS		(12)
#define EFF_POISON		(13)
#define EFF_PROTECT_EVIL	(14)
#define EFF_PROTECT_GOOD	(15)
#define EFF_SNEAK		(16)
#define EFF_HIDE		(17)
#define EFF_SLEEP		(18)
#define EFF_CHARM		(19)
#define EFF_FLYING		(20)
#define EFF_PASS_DOOR		(21)
#define EFF_HASTE		(22)
#define EFF_CALM		(23)
#define EFF_PLAGUE		(24)
#define EFF_WEAKEN		(25)
#define EFF_DARK_VISION		(26)
#define EFF_BERSERK		(27)
#define EFF_SWIM		(28)
#define EFF_REGENERATION        (29)
#define EFF_SLOW		(30)
#define EFF_WATERBREATHING	(31)
#define EFF_ARMOR		(32)
#define EFF_CHANGESEX		(33)
#define EFF_FRENZY		(34)
#define EFF_GIANTSTRENGTH	(35)
#define EFF_SHIELD		(36)
#define EFF_STONESKIN		(37)
#define EFF_CHILLTOUCH		(38)
#define EFF_CHAOS_SHIELD	(39)
#define EFF_WILD_SHIELD		(40)
#define EFF_WATERWALK		(41)
#define EFF_BARKSKIN		(42)
#define EFF_ENTANGLE		(43)
#define EFF_BATTLE_HYMN		(44)
#define EFF_WAR_DIRGE		(45)
#define EFF_DIVINE_RELIC	(46)
#define EFF_UNHOLY_RELIC	(47)
#define EFF_HEALING		(48)
#define EFF_INVITED		(49)
#define EFF_DETECT_CURSE	(50)
#define EFF_HALLUCINATION	(51)
#define EFF_HUNGER		(52)
#define EFF_THIRST		(53)

/*
 * Bits for 'affected_by2'.
 * Used in #MOBILES.
 */

#define EFF_NONE   		(0)
#define EFF_CANTRIP		(1)
#define EFF_IMPINVISIBLE	(2)

/* Ik wil een effect voor het verbergen van de allignment!!!
 */

/*
 * Sex.
 * Used in #MOBILES.
 */
#define SEX_NEUTRAL		      0
#define SEX_MALE		      1
#define SEX_FEMALE		      2
#define SEX_EITHER		      3

/* AC types */
#define AC_PIERCE			0
#define AC_BASH				1
#define AC_SLASH			2
#define AC_EXOTIC			3

/* dice */
#define DICE_NUMBER			0
#define DICE_TYPE			1
#define DICE_BONUS			2

/* size */
#define SIZE_TINY			0
#define SIZE_SMALL			1
#define SIZE_MEDIUM			2
#define SIZE_LARGE			3
#define SIZE_HUGE			4
#define SIZE_GIANT			5



/*
 * Well known object virtual numbers.
 * Defined in #OBJECTS.
 */
#define OBJ_VNUM_SILVER_ONE	      1
#define OBJ_VNUM_GOLD_ONE	      2
#define OBJ_VNUM_GOLD_SOME	      3
#define OBJ_VNUM_SILVER_SOME	      4
#define OBJ_VNUM_COINS		      5
#define OBJ_VNUM_TSHIRT		      6
#define OBJ_VNUM_BLOOD		      7
#define OBJ_VNUM_PUKE		      8
#define OBJ_VNUM_BODYBAG	      9

#define OBJ_VNUM_CORPSE_NPC	     10
#define OBJ_VNUM_CORPSE_PC	     11

#define OBJ_VNUM_MUSHROOM	     20
#define OBJ_VNUM_LIGHT_BALL	     21
#define OBJ_VNUM_SPRING		     22
#define OBJ_VNUM_DISC		     23
#define OBJ_VNUM_RELIC		     24
#define OBJ_VNUM_PORTAL		     25

#define OBJ_VNUM_CREATEFOOD1	     (OBJ_VNUM_MUSHROOM)
#define OBJ_VNUM_CREATEFOOD2         26
#define OBJ_VNUM_CREATEFOOD3         27
#define OBJ_VNUM_CREATEFOOD4         28
#define OBJ_VNUM_CREATEFOOD5         29

#define OBJ_VNUM_HUT1		     47
#define OBJ_VNUM_HUT2		     31
#define OBJ_VNUM_HUT3		     32
#define OBJ_VNUM_HUT4		     38

#define OBJ_VNUM_PORTALTREE          30
#define OBJ_VNUM_TENDRILS            53

#define OBJ_VNUM_HAND			54
#define OBJ_VNUM_FOOT			55
#define OBJ_VNUM_FINGER			56
#define OBJ_VNUM_EAR			57
#define OBJ_VNUM_EYE			58
#define OBJ_VNUM_LONG_TONGUE		59
#define OBJ_VNUM_EYESTALK		60
#define OBJ_VNUM_TENTACLE		61
#define OBJ_VNUM_FIN			62
#define OBJ_VNUM_WING			63
#define OBJ_VNUM_TAIL			64
#define OBJ_VNUM_CLAW			65
#define OBJ_VNUM_FANG			66
#define OBJ_VNUM_HORN			67
#define OBJ_VNUM_SCALES			68
#define OBJ_VNUM_TUSK			69
#define OBJ_VNUM_BRAINS			70
#define OBJ_VNUM_SEVERED_HEAD		71
#define OBJ_VNUM_TORN_HEART		72
#define OBJ_VNUM_SLICED_ARM		73
#define OBJ_VNUM_SLICED_LEG		74
#define OBJ_VNUM_GUTS			75

#define OBJ_VNUM_ROSE		   1001
#define OBJ_VNUM_EMPTYSCROLL	   3008
#define OBJ_VNUM_EMPTYVIAL	   3015

#define OBJ_VNUM_PIT		   3010

#define OBJ_VNUM_SCHOOL_MACE	   3700
#define OBJ_VNUM_SCHOOL_DAGGER	   3701
#define OBJ_VNUM_SCHOOL_SWORD	   3702
#define OBJ_VNUM_SCHOOL_SPEAR	   3717
#define OBJ_VNUM_SCHOOL_STAFF	   3718
#define OBJ_VNUM_SCHOOL_AXE	   3719
#define OBJ_VNUM_SCHOOL_FLAIL	   3720
#define OBJ_VNUM_SCHOOL_WHIP	   3721
#define OBJ_VNUM_SCHOOL_POLEARM    3722

#define OBJ_VNUM_SCHOOL_VEST	   3703
#define OBJ_VNUM_SCHOOL_SHIELD	   3704
#define OBJ_VNUM_SCHOOL_BANNER     3716
#define OBJ_VNUM_MAP		   3162
#define OBJ_VNUM_LOOMOFTIME	     90

#define OBJ_VNUM_WHISTLE	   2116



/*
 * Item types.
 * Used in #OBJECTS.
 */
#define ITEM_LIGHT		      1
#define ITEM_SCROLL		      2
#define ITEM_WAND		      3
#define ITEM_STAFF		      4
#define ITEM_WEAPON		      5
#define ITEM_TREASURE		      8
#define ITEM_ARMOR		      9
#define ITEM_POTION		     10
#define ITEM_CLOTHING		     11
#define ITEM_FURNITURE		     12
#define ITEM_TRASH		     13
#define ITEM_CONTAINER		     15
#define ITEM_DRINK_CON		     17
#define ITEM_KEY		     18
#define ITEM_FOOD		     19
#define ITEM_MONEY		     20
#define ITEM_BOAT		     22
#define ITEM_CORPSE_NPC		     23
#define ITEM_CORPSE_PC		     24
#define ITEM_FOUNTAIN		     25
#define ITEM_PILL		     26
#define ITEM_PROTECT		     27
#define ITEM_MAP		     28
#define ITEM_PORTAL		     29
#define ITEM_WARP_STONE		     30
#define ITEM_ROOM_KEY		     31
#define ITEM_GEM		     32
#define ITEM_JEWELRY		     33
#ifdef HAS_JUKEBOX
#define ITEM_JUKEBOX		     34
#endif
#define ITEM_QUEST		     35
#define ITEM_CLAN                    36

/* quest item consumption flags */
#define QUESTCONS_EAT		0
#define QUESTCONS_DRINK		1

/* quest item effect types flags */
#define QUESTEFF_LEVEL		0
#define QUESTEFF_PRACTICES	1
#define QUESTEFF_TRAINS		2
#define QUESTEFF_EXP		3
#define QUESTEFF_LONER		4
#define QUESTEFF_QUESTPOINTS	5

enum { QUEST_INFO, QUEST_REQUEST, QUEST_REJECT, QUEST_COMPLETE };

/*
 * Extra flags.
 * Used in #OBJECTS.
 */
#define ITEM_NONE		(0)
#define ITEM_GLOW		(1)
#define ITEM_HUM		(2)
#define ITEM_DARK		(3)
#define ITEM_LOCK		(4)
#define ITEM_EVIL		(5)
#define ITEM_INVIS		(6)
#define ITEM_MAGIC		(7)
#define ITEM_NODROP		(8)
#define ITEM_BLESS		(9)
#define ITEM_ANTI_GOOD		(10)
#define ITEM_ANTI_EVIL		(11)
#define ITEM_ANTI_NEUTRAL	(12)
#define ITEM_NOREMOVE		(13)
#define ITEM_INVENTORY		(14)
#define ITEM_NOPURGE		(15)
#define ITEM_ROT_DEATH		(16)
#define ITEM_VIS_DEATH		(17)
#define ITEM_NOSHOW		(18)
#define ITEM_NONMETAL		(19)
#define ITEM_NOLOCATE		(20)
#define ITEM_MELT_DROP		(21)
#define ITEM_HAD_TIMER		(22)
#define ITEM_SELL_EXTRACT	(23)
#define ITEM_FREEZE_PROOF	(24)
#define ITEM_BURN_PROOF		(25)
#define ITEM_NOUNCURSE		(26)
#define ITEM_UNIDENTIFABLE	(27)
#define ITEM_NOSTEAL		(28)
#define ITEM_NOSELL		(29)
#define ITEM_REFLECTING		(30)
#define ITEM_NOQUEST		(31)
#define ITEM_PC_BLOCKS_RESPAWN	(32)
#define ITEM_SAVEOBJ		(33)
#define ITEM_NO_LEFTOVERS	(34)
#define ITEM_INSURABLE		(35)
#define ITEM_INSURED		(36)
#define ITEM_QUESTITEM		(37)
#define ITEM_UNUSABLE		(38)
#define ITEM_INSTALLED		(39)
#define ITEM_NODESTROY		(40)

/*
 * Extra2 flags.
 * Used in #OBJECTS.
 */
#define ITEM_NONE		(0)
#define ITEM_ANTI_CLASS		(1)
#define ITEM_ANTI_RACE		(2)
#define ITEM_LEVEL		(3)	/* arg1 is new level */
#define ITEM_EXTENDED		(4)	/* More effects */
#define ITEM_CLASS_ONLY		(5)
#define ITEM_RACE_ONLY		(6)
#define ITEM_RACE_POISON	(7)

/*
 * Extended values
 */

#define EXTENDED_DUMMY		0

/*
 * Wear flags.
 * Used in #OBJECTS.
 */
#define ITEM_TAKE		(A)
#define ITEM_WEAR_FINGER	(B)
#define ITEM_WEAR_NECK		(C)
#define ITEM_WEAR_BODY		(D)
#define ITEM_WEAR_HEAD		(E)
#define ITEM_WEAR_LEGS		(F)
#define ITEM_WEAR_FEET		(G)
#define ITEM_WEAR_HANDS		(H)
#define ITEM_WEAR_ARMS		(I)
#define ITEM_WEAR_SHIELD	(J)
#define ITEM_WEAR_ABOUT		(K)
#define ITEM_WEAR_WAIST		(L)
#define ITEM_WEAR_WRIST		(M)
#define ITEM_WIELD		(N)
#define ITEM_HOLD		(O)
#define ITEM_NO_SAC		(P)
#define ITEM_WEAR_FLOAT		(Q)
/*
 * Mask for real wear positions
 */
#define ITEM_WEAR_MASK		(~(ITEM_TAKE | ITEM_NO_SAC))

/* weapon class */
#define WEAPON_EXOTIC		0
#define WEAPON_SWORD		1
#define WEAPON_DAGGER		2
#define WEAPON_SPEAR		3
#define WEAPON_MACE		4
#define WEAPON_AXE		5
#define WEAPON_FLAIL		6
#define WEAPON_WHIP		7
#define WEAPON_POLEARM		8

/* weapon types */
#define WEAPON_FLAMING		(A)
#define WEAPON_FROST		(B)
#define WEAPON_VAMPIRIC		(C)
#define WEAPON_SHARP		(D)
#define WEAPON_VORPAL		(E)
#define WEAPON_TWO_HANDS	(F)
#define WEAPON_SHOCKING		(G)
#define WEAPON_POISON		(H)

/* gate flags */
#define GATE_NORMAL_EXIT	(A)
#define GATE_NOCURSE		(B)
#define GATE_GOWITH		(C)
#define GATE_BUGGY		(D)
#define GATE_RANDOM		(E)
#define GATE_CLIMB		(F)
#define GATE_JUMP		(G)
#define GATE_STORE		(H)
#define GATE_RECALL		(I)
#define GATE_NOGETALL		(J)

/* furniture flags */
#define STAND_AT		(A)
#define STAND_ON		(B)
#define STAND_IN		(C)
#define SIT_AT			(D)
#define SIT_ON			(E)
#define SIT_IN			(F)
#define REST_AT			(G)
#define REST_ON			(H)
#define REST_IN			(I)
#define SLEEP_AT		(J)
#define SLEEP_ON		(K)
#define SLEEP_IN		(L)
#define PUT_AT			(M)
#define PUT_ON			(N)
#define PUT_IN			(O)
#define PUT_INSIDE		(P)




/*
 * Apply types (for effects).
 * Used in #OBJECTS.
 */
#define APPLY_NONE		      0
#define APPLY_STR		      1
#define APPLY_DEX		      2
#define APPLY_INT		      3
#define APPLY_WIS		      4
#define APPLY_CON		      5
#define APPLY_SEX		      6
#define APPLY_CLASS		      7
#define APPLY_LEVEL		      8
#define APPLY_AGE		      9
#define APPLY_HEIGHT		     10
#define APPLY_WEIGHT		     11
#define APPLY_MANA		     12
#define APPLY_HIT		     13
#define APPLY_MOVE		     14
#define APPLY_GOLD		     15
#define APPLY_EXP		     16
#define APPLY_AC		     17
#define APPLY_HITROLL		     18
#define APPLY_DAMROLL		     19
#define APPLY_SAVES		     20
#define APPLY_SAVING_PARA	     20
#define APPLY_SAVING_ROD	     21
#define APPLY_SAVING_PETRI	     22
#define APPLY_SAVING_BREATH	     23
#define APPLY_SAVING_SPELL	     24
#define APPLY_SPELL_EFFECT	     25
#define APPLY_ALIGNMENT		     26

/*
 * Values for containers (value[1]).
 * Used in #OBJECTS.
 */
#define CONT_CLOSEABLE		      (A)
#define CONT_PICKPROOF		      (B)
#define CONT_CLOSED		      (C)
#define CONT_LOCKED		      (D)
#define CONT_PUT_ON		      (E)
#define CONT_TRAP		      (F)	/* It's a trap */
#define CONT_TRAP_DARTS		      (G)	/* Damage to opener */
#define CONT_TRAP_POISON	      (H)	/* Poisons thief */
#define CONT_TRAP_EXPLODING	      (I)	/* Damage to room */
#define CONT_TRAP_EXTRACT	      (J)	/* container destroyed */
#define CONT_TRAP_SLEEPGAS	      (K)	/* Sleeping gass trough room */
#define CONT_TRAP_DEATH		      (L)	/* Death Trap */
#define CONT_NOGETALL		      (M)	// no get all


/*
 * Well known room virtual numbers.
 * Defined in #ROOMS.
 */
#define ROOM_VNUM_LIMBO		      2
#define ROOM_VNUM_MORGUE	      9
#define ROOM_VNUM_HELL		     13
#define ROOM_VNUM_INSURANCE          18
#define ROOM_VNUM_CHAT		   1200
#define ROOM_VNUM_DONATION	   3000
#define ROOM_VNUM_TEMPLE	   3001
#define ROOM_VNUM_ALTAR		   3054
#define ROOM_VNUM_SCHOOL	   3700
#define ROOM_VNUM_BALANCE	   4500
#define ROOM_VNUM_CIRCLE	   4400
#define ROOM_VNUM_DEMISE	   4201
#define ROOM_VNUM_HONOR		   4300
#define ROOM_VNUM_DONATION_VIAL	   3055
#define ROOM_VNUM_DONATION_PAPER   3055
#define OBJ_VNUM_DONATION_VIAL	   3141
#define OBJ_VNUM_DONATION_SCROLL   3142
#define OBJ_VNUM_DONATION_FOOD	   3143

/*
 * Lower and upper limits of areas not selectable for obj-quests (boundries included)
 */
#define VNUM_IMMORTZONE_LOWER 400
#define VNUM_IMMORTZONE_UPPER 499
#define VNUM_IMMORTAREA_LOWER 1200
#define VNUM_IMMORTAREA_UPPER 1299
#define VNUM_LIMBO_LOWER      1
#define VNUM_LIMBO_UPPER      99
#define VNUM_CLANAREA_LOWER   29000
#define VNUM_CLANAREA_UPPER   30299
#define VNUM_TAGGING_LOWER    1900
#define VNUM_TAGGING_UPPER    1949

/*
 * Room flags.
 * Used in #ROOMS.
 */
#define ROOM_NONE		(0)
#define ROOM_DARK		(1)
#define ROOM_CLANONLY		(2)
#define ROOM_NO_MOB		(3)
#define ROOM_INDOORS		(4)
#define ROOM_LIGHT		(5)
#define ROOM_PITCHBLACK		(6)
//efine ROOM_			(7)
//efine ROOM_			(8)
//efine ROOM_			(9)
#define ROOM_PRIVATE		(10)
#define ROOM_SAFE		(11)
#define ROOM_SOLITARY		(12)
#define ROOM_PET_SHOP		(13)
#define ROOM_NO_RECALL		(14)
#define ROOM_IMP_ONLY		(15)
#define ROOM_GODS_ONLY		(16)
#define ROOM_HEROES_ONLY	(17)
#define ROOM_NEWBIES_ONLY	(18)
#define ROOM_LAW		(19)
#define ROOM_NOWHERE		(20)
#define ROOM_DEATHTRAP		(21)
//efine ROOM_			(22)
#define ROOM_NOMAGIC		(23)
#define ROOM_SAVEOBJ		(24)
#define ROOM_NONEWBIE		(25)
#define ROOM_NOSACRIFICE	(26)
#define ROOM_NOGETALL		(27)
#define ROOM_NOQUEST		(28)


/*
 * Directions.
 * Used in #ROOMS.
 */
#define DIR_NORTH		      0
#define DIR_EAST		      1
#define DIR_SOUTH		      2
#define DIR_WEST		      3
#define DIR_UP			      4
#define DIR_DOWN		      5
#define DIR_MAX			      (6) // feel free to use in for-loops



/*
 * Exit flags.
 * Used in #ROOMS.
 */
#define EX_ISDOOR		      (A)
#define EX_CLOSED		      (B)
#define EX_LOCKED		      (C)
#define EX_NOWARN                     (D)
#define EX_PICKPROOF		      (F)
#define EX_NOPASS		      (G)
#define EX_EASY			      (H)
#define EX_HARD			      (I)
#define EX_INFURIATING		      (J)
#define EX_NOCLOSE		      (K)
#define EX_NOLOCK		      (L)
#define EX_TRAP			      (M)
#define EX_TRAP_DARTS		      (N)
#define EX_TRAP_POISON		      (O)
#define EX_TRAP_EXPLODING	      (P)
#define EX_TRAP_SLEEPGAS	      (Q)
#define EX_TRAP_DEATH		      (R)
#define EX_GATE_RECALL		      (S)
#define EX_SECRET		      (T)
#define	EX_TCLLOCK                    (U)



/*
 * Sector types.
 * Used in #ROOMS.
 */
#define SECT_INSIDE		      0
#define SECT_CITY		      1
#define SECT_FIELD		      2
#define SECT_FOREST		      3
#define SECT_HILLS		      4
#define SECT_MOUNTAIN		      5
#define SECT_WATER_SWIM		      6
#define SECT_WATER_NOSWIM	      7
#define SECT_WATER_BELOW	      8
#define SECT_AIR		      9
#define SECT_DESERT		     10
#define SECT_MAX		     11
// when updating also update movement_loss-table in act_move.c!!



/*
 * Equpiment wear locations.
 * Used in #RESETS.
 */
#define WEAR_NONE		     -1
#define WEAR_LIGHT		      0
#define WEAR_FINGER_L		      1
#define WEAR_FINGER_R		      2
#define WEAR_NECK_1		      3
#define WEAR_NECK_2		      4
#define WEAR_BODY		      5
#define WEAR_HEAD		      6
#define WEAR_LEGS		      7
#define WEAR_FEET		      8
#define WEAR_HANDS		      9
#define WEAR_ARMS		     10
#define WEAR_SHIELD		     11
#define WEAR_ABOUT		     12
#define WEAR_WAIST		     13
#define WEAR_WRIST_L		     14
#define WEAR_WRIST_R		     15
#define WEAR_WIELD		     16
#define WEAR_HOLD		     17
#define WEAR_FLOAT		     18
#define WEAR_SECONDARY		     19
#define MAX_WEAR		     20



/***************************************************************************
 *                                                                         *
 *                   VALUES OF INTEREST TO AREA BUILDERS                   *
 *                   (End of this section ... stop here)                   *
 *                                                                         *
 ***************************************************************************/

/*
 * Conditions.
 */
#define MAX_CONDITION		      5
#define COND_DRUNK		      0
#define COND_FULL		      1
#define COND_THIRST		      2
#define COND_HUNGER		      3
#define COND_ADRENALINE		      4



/*
 * Positions.
 */
#define POS_DEAD		      0
#define POS_MORTAL		      1
#define POS_INCAP		      2
#define POS_STUNNED		      3
#define POS_SLEEPING		      4
#define POS_RESTING		      5
#define POS_SITTING		      6
#define POS_FIGHTING		      7
#define POS_STANDING		      8



/*
 * ACT bits for players.
 */
#define PLR_NONE		(0)
#define PLR_IS_NPC		(1)		/* Don't EVER set.	*/
#define PLR_QUESTOR		(2)		/* Is player in a quest */

/* RT auto flags */
#define PLR_AUTOASSIST		(3)
#define PLR_AUTOEXIT		(4)
#define PLR_AUTOLOOT		(5)
#define PLR_AUTOSAC             (6)
#define PLR_AUTOGOLD		(7)
#define PLR_AUTOSPLIT		(8)
#define PLR_QUITTING		(9)
#define PLR_ORDERED		(10)
#define PLR_NOSAVE		(11)
#ifdef HAS_MCCP
#define PLR_MCCP		(12)	// Mud Compression Control Protocol
#define PLR_MCCP2		(13)	// Mud Compression Control Protocol 2
#endif

/* RT personal flags */
#define PLR_HOLYLIGHT		(14)
#define PLR_PKILLING		(15)
#define PLR_CANLOOT		(16)
#define PLR_NOSUMMON		(17)
#define PLR_NOFOLLOW		(18)
#define PLR_FREEZETITLE		(19)
#define PLR_COLOUR		(20)    /* Colour Flag By Lope */

/* penalty flags */
#define PLR_PERMIT		(21)
//efine PLR_			(22)	// free!
#define PLR_LOG			(23)
#define PLR_DENY		(24)
#define PLR_FREEZE		(25)
#define PLR_THIEF		(26)
#define PLR_KILLER		(27)

#define PLR_HAS_RAW_COLOUR	(28)	// show raw colour codes in editor
#define PLR_PUEBLO		(29)	// Pueblo enhancements for Pueblo
#define PLR_MXP			(30)	// mud extention protocol for ZMud
#define PLR_COMPASS		(31)	// show a compass in room description
#ifdef HAS_ALTS
#define PLR_IS_ALT		(32)	// set if player is an alt
#endif
#define PLR_SWITCHED		(33)	// set if player is switched
#define PLR_WANTS_RAW_COLOUR	(34)	// wants raw colours codes in editor
#define PLR_HOLYLIGHT_PLUSPLUS	(35)
#define PLR_HARDCORE		(36)


/* RT comm flags -- may be used on both mobs and chars */
#define COMM_NONE		(0)
#define COMM_QUIET              (1)
#define COMM_DEAF            	(2)
#define COMM_NOWIZ              (3)
#define COMM_NOAUCTION          (4)
#define COMM_NOGOSSIP           (5)
#define COMM_NOQUESTION         (6)
#define COMM_NOMUSIC            (7)
#define COMM_NOCLAN		(8)
#define COMM_NOQUOTE		(9)
#define COMM_SHOUTSOFF		(10)
#define COMM_ANNOUNCE		(11)
#define COMM_COMPACT		(12)
#define COMM_BRIEF		(13)
#ifdef I3
#define COMM_I3MUTE		(14)
#endif
#define COMM_COMBINE		(15)
#define COMM_SHOW_EXTRADAM	(16)
#define COMM_SHOW_EFFECTS	(17)
#define COMM_NOGRATS		(18)
#define COMM_NONEWBIE		(19)
#define COMM_NOEMOTE		(20)
#define COMM_NOSHOUT		(21)
#define COMM_NOTELL		(22)
#define COMM_NOCHANNELS		(23)
#define COMM_NOPRAY		(24)
#define COMM_SNOOP_PROOF	(25)
#define COMM_AFK		(26)
#define COMM_NOQUEST		(27)
#define COMM_SHOW_ARMOR		(28)
#define COMM_SHOW_EQUIPMENT	(29)
#define COMM_SHOW_WEIGHT	(30)
#define COMM_SHOW_PAIN		(31)
#define COMM_NOHEROTALK		(32)
#define COMM_SHOW_NOHUNGER	(33)
#define COMM_NOPAGE		(34)
#define COMM_NOCOLOR		(35)
#define COMM_NOANSWER		(36)
#define COMM_NOYELL		(37)
#define COMM_NOIMMTALK		(38)
#define COMM_NONOTE		(39)
#define COMM_AFK_BY_IDLE	(40)

#include  "fd_tcl_core.h"
#include  "fd_areaprog.h"
#include  "fd_mobprog.h"
#include  "fd_objprog.h"
#include  "fd_roomprog.h"

#include     "act_donate.h"
#include     "act_equipment.h"
#include     "act_food.h"
#include     "act_magic.h"
#include     "ban.h"
#include  "fd_clan.h"
#include  "fd_conf.h"
#include  "fd_copyover.h"
#include  "fd_dreams.h"
#include  "fd_editor.h"
#include     "fight.h"
#include  "fd_fileio.h"
#include     "healer.h"
#include     "help.h"
#include  "fd_http.h"
#include  "fd_job.h"
#include  "fd_log.h"
#include  "fd_lookup.h"
#include  "fd_math.h"
#include     "magic.h"
#include  "fd_mccp.h"
#include  "fd_memory.h"
#include  "fd_money.h"
#ifdef HAS_JUKEBOX
#include     "music.h"
#endif
#include  "fd_network.h"
#include  "fd_output.h"
#include  "fd_pop3.h"
#include  "fd_property.h"
#include     "recycle.h"
#include  "fd_shops.h"
#include  "fd_skills_gsn.h"
#include  "fd_spells.h"
#include "wiz_set.h"
#include "wiz_stat.h"
#include "wiz_where.h"
#include     "string.h"

/*
 * Prototype for a mob.
 * This is the in-memory version of #MOBILES.
 */
struct	mob_index_data
{
    bool		valid;
    MOB_INDEX_DATA *	next;
    SPEC_FUN *		spec_fun;
    SHOP_DATA *		pShop;
    int			vnum;
    int			group;
    int			count;
    int			killed;
    char *		player_name;
    char *		short_descr;
    char *		long_descr;
    char *		description;
    EXTRA_DESCR_DATA *	extra_descr;
    char		strbit_act[MAX_FLAGS];
    char		strbit_act_read[MAX_FLAGS];
    char		strbit_act_saved[MAX_FLAGS];
    char		strbit_affected_by[MAX_FLAGS];
    char		strbit_affected_by2[MAX_FLAGS];
    char		strbit_affected_read[MAX_FLAGS];
    char		strbit_affected_read2[MAX_FLAGS];
    char		strbit_affected_saved[MAX_FLAGS];
    char		strbit_affected_saved2[MAX_FLAGS];
    int			alignment;
    int			level;
    int			hitroll;
    int			hit[3];
    int			mana[3];
    int			damage[3];
    int			ac[4];
    int 		dam_type;
    char		strbit_off_flags[MAX_FLAGS];
    char		strbit_imm_flags[MAX_FLAGS];
    char		strbit_res_flags[MAX_FLAGS];
    char		strbit_vuln_flags[MAX_FLAGS];
			// formerly xxx_mem[0]
    char		strbit_off_read[MAX_FLAGS];	// some stuff of Joran
    char		strbit_imm_read[MAX_FLAGS];	// some stuff of Joran
    char		strbit_res_read[MAX_FLAGS];	// some stuff of Joran
    char		strbit_vuln_read[MAX_FLAGS];	// some stuff of Joran
			// formerly xxx_mem[1]
    char		strbit_off_saved[MAX_FLAGS];	// some stuff of Joran
    char		strbit_imm_saved[MAX_FLAGS];	// some stuff of Joran
    char		strbit_res_saved[MAX_FLAGS];	// some stuff of Joran
    char		strbit_vuln_saved[MAX_FLAGS];	// some stuff of Joran
    int			start_pos;
    int			default_pos;
    int			sex;
    int			race;
    long		wealth;
    long		form;
    long		form_mem[2];	/* Joran, for saving */
    long		parts;
    long		parts_mem[2];	/* Joran, for saving */
    int			size;
    char *		material;
    bool		deleted;
    char *		pueblo_picture;

    int			mprog_vnum;	/* instance mprog */

    AREA_DATA *		area;		/* OLC */
    PROPERTY		*property;
};

/* memory settings */
#define MEM_CUSTOMER	0
#define MEM_SELLER	1
#define MEM_HOSTILE	2
#define MEM_AFRAID	3
#define MEM_CUSTOM1	4
#define MEM_CUSTOM2	5
#define MEM_CUSTOM3	6
#define MEM_CUSTOM4	7

/* memory for mobs */
struct mem_data
{
    bool	valid;
    MEM_DATA *	next;
    long	type;
    long	id;
    int		times;
    time_t 	when;
};

/* Hunting flags */
#define HUNT_NONE	0
#define	HUNT_KILL	1
#define HUNT_ROOM	2
#define HUNT_FIND	3
#define HUNT_GFIND	4  /* same thing as FIND only forced-global */

#define HUNT_RESULT_NO_PATH   -1
#define HUNT_RESULT_NOT_FOUND -2

/*
 * One character (PC or NPC).
 */
struct	char_data
{
    bool		valid;
    CHAR_DATA *		next;
    CHAR_DATA *		next_in_room;
    CHAR_DATA *		next_player;
    CHAR_DATA *		master;
    CHAR_DATA *		leader;
    CHAR_DATA *		fighting;
    CHAR_DATA *		reply;
    CHAR_DATA *		pet;
    PHASE_FUN *		phasedskill_proc;
    int			phasedskill_step;
    void *		phasedskill_target;
    long		phasedskill_flags;
    int 		recall;
    MEM_DATA *		memory;
    SPEC_FUN *		spec_fun;
    MOB_INDEX_DATA *	pIndexData;
    DESCRIPTOR_DATA *	desc;
    EXTRA_DESCR_DATA *	extra_descr;
    EFFECT_DATA *	affected;
    NOTE_DATA *		pnote;
    OBJ_DATA *		carrying;
    OBJ_DATA *		on;
    ROOM_INDEX_DATA *	in_room;
    ROOM_INDEX_DATA *	was_in_room;
    AREA_DATA *		zone;
    PC_DATA *		pcdata;
    GEN_DATA *		gen_data;
    char *		name;
    long		id;
    int			version;
    char *		short_descr;
    char *		long_descr;
    char *		description;
    char *		prompt;
    char *		prefix;
    int			group;
    CLANMEMBER_TYPE *	clan;
    int			Sex;
    int			class;
    int			race;
    int			level;
    int			trust;
    int			played;
    time_t		logon;
    int			timer;
    int			wait;
    int			daze;
    int			hit;
    int			max_hit;
    int			mana;
    int			max_mana;
    int			move;
    int			max_move;
    long		gold;
    long		silver;
    int			exp;
    char		strbit_act[MAX_FLAGS];
    char		strbit_comm[MAX_FLAGS];	/* communication flags */
    char		strbit_wiznet[WIZNET_FLAGS]; /* wiz stuff, 20x8 bits */
    char		strbit_imm_flags[MAX_FLAGS];
    char		strbit_res_flags[MAX_FLAGS];
    char		strbit_vuln_flags[MAX_FLAGS];
    int			invis_level;
    int			incog_level;
    char		strbit_affected_by[MAX_FLAGS];
    char		strbit_affected_by2[MAX_FLAGS];
    int			position;
    int			practice;
    int			train;
    int			carry_weight;
    int			carry_number;
    int			saving_throw;
    int			alignment;
    int			hitroll;
    int			damroll;
    int			armor[4];
    int			wimpy;
    int			resetted_at;
    /* stats */
    int			perm_stat[MAX_STATS];
    int			mod_stat[MAX_STATS];
    /* parts stuff */
    long		form;
    long		parts;
    int			size;
    char*		material;
    /* mobile stuff */
    char		strbit_off_flags[MAX_FLAGS];
    int			damage[3];
    int			dam_type;
    int			start_pos;
    int			default_pos;
    /* mob program stuff */
    CHAR_DATA *		mprog_target;  /* Possible error */
    int			mprog_delay;
    long		mprog_timer;
    TCLPROG_LIST *	mprogs;		/* mob program stuff */
    char		strbit_mprog_triggers[MPROG_MAX_TRIGGERS];
    /* Hunting stuff */
    int			hunt_type;
    long		hunt_id;
    PROPERTY		*property;
};



/*
 * Data which only PC's have.
 */
struct	pc_data {
    PC_DATA *		next;
    CHAR_DATA *		switched_into;
    BUFFER * 		buffer;
    int			missedtells;
#ifdef HAS_POP3
    NOTE_REF		*noteref;
#endif
    bool		valid;
    char *		pwd;
    char *		bamfin;
    char *		bamfout;
    char *		title;
    char *		whoname;
#ifdef HAS_ALTS
    char *		alts;
    char *		uberalt;
#endif
    time_t              last_notes	[MAX_NOTES];
    int			perm_hit;
    int			perm_mana;
    int			perm_move;
    int			true_sex;
    time_t		idle;
    int			last_level;
    int			condition	[MAX_CONDITION];
    struct pc_skill 	{
			int	learned;
			int	forgotten; // can be negative for temorary skill bonusses
			int	counter;
			} skill[MAX_SKILL];
    bool		group_known	[MAX_GROUP];
    int			points;
    int			default_lines;	// number of lines on a page, configured
    bool              	confirm_delete;
    CLAN_INFO *		clan_recruit;
    char *		afk_text;	// not saved
    ALIAS_TYPE		*aliases;
    char *		pueblo_picture;
    NOTEBK_DATA *       notebook;
    int			killed;		// how many times has player been killed

    int 		security;	/* OLC - Builder security */

    int			questpoints;	/* For quests */
    int			swearing;	/* Times the character has sweared */
    long		bank;		/* Silver coins in the bank */
    char 		strbitgrand[30]; // 8*30=240 flags
    char		jobs[(MAX_JOBS+7)/8];

    bool		newhere;
    char		beeninroom[MAX_VNUMS/8];	// is bit is set, been in this room
    char		killedthatmob[MAX_VNUMS/8];	// is bit is set, mob has been killed
    char		seenthatmob[MAX_VNUMS/8];	// is bit is set, mob has been seen
    char		seenthatobject[MAX_VNUMS/8];	// is bit is set, objects has been seen
    char		usedthatobject[MAX_VNUMS/8];	// is bit is set, objects has been used

};

/* Data for generating characters -- only used during generation */
struct gen_data
{
    GEN_DATA	*next;
    bool	valid;
    bool	skill_chosen[MAX_SKILL];
    bool	group_chosen[MAX_GROUP];
    bool	auto_skill[MAX_SKILL];
    bool	auto_group[MAX_GROUP];
    int		points_chosen;
    int		weapon;
    int		page,pages;
    bool	customize;
};



/*
 * Liquids.
 */
#define LIQ_WATER        0

struct	liq_type
{
    char *	liq_name;
    char *	liq_color;
    int		liq_effect[5];
};


/*
 * Extra description data for a room or object.
 */
struct	extra_descr_data
{
    EXTRA_DESCR_DATA *next;	/* Next in list                     */
    bool	valid;
    char	*keyword;	/* Keyword in look/examine          */
    char	*description;	/* What to see                      */
};



/*
 * Prototype for an object.
 */
struct	obj_index_data
{
    bool		valid;
    OBJ_INDEX_DATA *	next;
    EXTRA_DESCR_DATA *	extra_descr;
    EFFECT_DATA *	affected;
    char *		name;
    char *		short_descr;
    char *		description;
    int			vnum;
    int			reset_num;
    char *		material;
    int			item_type;
    char		strbit_extra_flags[MAX_FLAGS];
    char		strbit_extra_flags2[MAX_FLAGS];
    long		wear_flags;
    int			level;
    int 		condition;
    int			count;
    int			weight;
    int			cost;
    int			value[5];
    long		deleted;
    char *		pueblo_picture;

    int			oprog_vnum;	// instance oprog

    PROPERTY		*property;

    AREA_DATA *		area;		/* OLC */
};



/*
 * One object.
 */
struct	obj_data
{
    bool		valid;
    OBJ_DATA *		next;
    OBJ_DATA *		next_content;
    OBJ_DATA *		prev_content;// !!! WARNING only valid during save.c:fwrite_obj
    OBJ_DATA *		contains;
    OBJ_DATA *		in_obj;
    CHAR_DATA *		carried_by;
    EXTRA_DESCR_DATA *	extra_descr;
    EFFECT_DATA *	affected;
    OBJ_INDEX_DATA *	pIndexData;
    ROOM_INDEX_DATA *	in_room;
    long		id;
    bool		enchanted;
    char *	        owner_name;
    long	        owner_id;
    char *		name;
    char *		short_descr;
    char *		description;
    int			item_type;
    char		strbit_extra_flags[MAX_FLAGS];
    char		strbit_extra_flags2[MAX_FLAGS];
    long		wear_flags;
    int			wear_loc;
    int			weight;
    int			cost;
    int			level;
    int 		condition;
    char *		material;
    int			timer;
    int			value	[5];
    int			resetted_at;
    PROPERTY		*property;

    TCLPROG_LIST *	oprogs;
    int			oprog_delay;
    int			oprog_timer;
    char		strbit_oprog_triggers[OPROG_MAX_TRIGGERS];
    CHAR_DATA *		oprog_target;
};



/*
 * Exit data.
 */
struct	exit_data
{
    bool		valid;
    ROOM_INDEX_DATA *	to_room;
    int			vnum;
    int			exit_info;
    int			key;
    char *		keyword;
    char *		description;

    EXIT_DATA *		next;		/* OLC */
    int			rs_flags;	/* OLC */
    int			orig_door;	/* OLC */
};



/*
 * Reset commands:
 *   '*': comment
 *   'M': read a mobile
 *   'O': read an object
 *   'P': put object in object
 *   'G': give object to mobile
 *   'E': equip object to mobile
 *   'D': set state of door
 *   'R': randomize room exits
 *   'S': stop (end of list)
 */

/*
 * Area-reset definition.
 */
struct	reset_data
{
    bool		valid;
    RESET_DATA *	next;
    char		command;
    int			arg1;
    int			arg2;
    int			arg3;
    int			arg4;
};



/*
 * Area definition.
 */
struct	area_data
{
    bool		valid;
    AREA_DATA *		next;
    CLAN_INFO *		conquered_by;	// clan-stuff, who conquered this area
    char *		name;
    int                 recall;
    int 		age;
    int 		nplayer;
    bool		empty;
    char *		filename;	/* OLC */
    char *		builders;	/* OLC - Listing of builders */
    char *		urlprefix;	// for pueblo
    char *		creator;	// Creators, not builders!
    char *		comment;	// comments for the area
    char *		description;	// comments for the area
    int			security;	/* OLC - Value 0-infinity  */
    int			lvnum;		/* OLC - Lower vnum */
    int			uvnum;		/* OLC - Upper vnum */
    int			vnum;		/* OLC - Area vnum  */
    long		area_flags;	/* OLC */
    int			version;	// area-version
    int			vnum_rooms_in_use;// amount of vnums in use for rooms
    int			vnum_mobs_in_use;// amount of vnums in use for mobs
    int			vnum_objects_in_use;// amount of vnums in use f objects
    int			llevel;		// level - lower
    int			ulevel;		// level - upper

    char *		areaprogram;
    bool		areaprogram_edit;
    char *		area_vars;

    TCLPROG_CODE *	aprog;
    bool		aprog_enabled;

    TCLPROG_LIST *	aprogs;
    int			aprog_delay;
    int			aprog_timer;
    bool		aprog_running;
    int			aprog_running_version;
    char		strbit_aprog_triggers[APROG_MAX_TRIGGERS];
    CHAR_DATA *		aprog_target;

    PROPERTY_INDEX_TYPE *properties;

    int			login_room;
};



/*
 * Room type.
 */
struct	room_index_data
{
    bool		valid;
    ROOM_INDEX_DATA *	next;
    CHAR_DATA *		people;
    OBJ_DATA *		contents;
    EXTRA_DESCR_DATA *	extra_descr;
    AREA_DATA *		area;
    EXIT_DATA *		exit	[6];
    EXIT_DATA * 	old_exit[6];
    EFFECT_DATA *	eff_perm;
    EFFECT_DATA *	eff_temp;
    long		extra_flags;
    char *		name;
    char *		description;
//  char *		owner;		/* now a property */
    int			vnum;
    char		strbit_room_flags[MAX_FLAGS];
    int			light;
    int			sector_type;
//  int			clan;		/* now a property */
    int			level;
    int			hunt;		/* Hunt path code, Added by Joran */
    bool		deleted;
    char *		pueblo_picture;

    RESET_DATA *	reset_first;	/* OLC */
    RESET_DATA *	reset_last;	/* OLC */

    TCLPROG_LIST *	rprogs;
    int			rprog_delay;
    int			rprog_timer;
    int			rprog_vnum;
    int			rprog_running_vnum;
    int			rprog_running_version;
    char		strbit_rprog_triggers[RPROG_MAX_TRIGGERS];
    CHAR_DATA *		rprog_target;

    PROPERTY		*property;
};

struct room_effect_data {
    bool		valid;
    ROOM_EFFECT_DATA	*next;
    ROOM_INDEX_DATA	*room;
    EFFECT_DATA		*affected;
};


/*
 * Types of attacks.
 * Must be non-overlapping with spell/skill types,
 * but may be arbitrary beyond that.
 */
#define TYPE_UNDEFINED               -1
#define TYPE_HIT                     10000



/*
 *  Target types.
 */
#define TAR_IGNORE		    0
#define TAR_CHAR_OFFENSIVE	    1
#define TAR_CHAR_DEFENSIVE	    2
#define TAR_CHAR_SELF		    3
#define TAR_OBJ_INV		    4
#define TAR_OBJ_CHAR_DEF	    5
#define TAR_OBJ_CHAR_OFF	    6
#define TAR_OBJ_ROOM		    8

#define TARGET_CHAR		    0
#define TARGET_OBJ		    1
#define TARGET_ROOM		    2
#define TARGET_NONE		    3



/*
 * Skills include spells as a particular case.
 */

struct	skilltable_type
{
    char *	name;			/* Name of skill		*/
    bool	implemented;		/* Is this skill implemented?   */
    ubyte	Skill_level[MAX_CLASS][MAX_PC_RACE];
    					/* Level needed by class	*/
    byte	Rating[MAX_CLASS][MAX_PC_RACE];
    					/* How hard it is to learn	*/
    ubyte	skill_adept[MAX_CLASS][MAX_PC_RACE];
    					/* To what % can it be learned? */
    SPELL_FUN *	spell_fun;		/* Spell pointer (for spells)	*/
    WEAROFF_FUN *wearoff_fun;		/* Wear off function (for effects) */
    int		target;			/* Legal targets		*/
    int		minimum_position;	/* Position for caster / user	*/
    int *	pgsn;			/* Pointer to associated gsn	*/
    ubyte	Min_mana[MAX_CLASS][MAX_PC_RACE];
    					/* Minimum mana used		*/
    int		beats;			/* Waiting time after use	*/
    char *	noun_damage;		/* Damage message		*/
    char *	msg_off;		/* Wear off message		*/
    char *	msg_obj;		/* Wear off message for obects	*/
    char *	msg_off_room;		/* Wear off message for rooom   */
};

struct  grouptable_type
{
    char *	name;
    int		Rating[MAX_CLASS][MAX_PC_RACE];
    char *	spells[MAX_IN_GROUP];
};

/*
 * Utility macros.
 */
#define ZEROVAR(var,type)	memset((void *)var,0,sizeof(type))
#define IS_VALID(data)		((data) != NULL && (data)->valid)
#define VALIDATE(data)		((data)->valid = TRUE)
#define INVALIDATE(data)	((data)->valid = FALSE)
#define UMIN(a, b)		((a) < (b) ? (a) : (b))
#define UMAX(a, b)		((a) > (b) ? (a) : (b))
#define URANGE(a, b, c)		((b) < (a) ? (a) : ((b) > (c) ? (c) : (b)))
#define LOWER(c)		((c) >= 'A' && (c) <= 'Z' ? (c)+'a'-'A' : (c))
#define UPPER(c)		((c) >= 'a' && (c) <= 'z' ? (c)+'A'-'a' : (c))
#define IS_SET(flag, bit)	((flag) & (bit))
#define SET_BIT(var, bit)	((var) |= (bit))
#define REMOVE_BIT(var, bit)	((var) &= ~(bit))
#define TOGGLE_BIT(var, bit)    ((var) ^= (bit))
#define ERROR			strerror(errno)

/*
 * String-bit macros
 */
#define STR_ZERO_BIT(var, size)  memset( (void *)(var), 0, (size))
#define STR_ONE_BIT(var, size)   memset( (void *)(var), 255, (size))
#define STR_IS_SET(var, bit)     ((((char *)(var))[((bit)/8)]) &   ((1<<((bit)%8))))
#define STR_SET_BIT(var, bit)    ((((char *)(var))[((bit)/8)]) |=  ((1<<((bit)%8))))
#define STR_REMOVE_BIT(var, bit) ((((char *)(var))[((bit)/8)]) &= ~((1<<((bit)%8))))
#define STR_TOGGLE_BIT(var, bit) ((((char *)(var))[((bit)/8)]) ^=  ((1<<((bit)%8))))
#define STR_COPY_BIT(var,dst,src)  if (STR_IS_SET((var),src)) 		\
					STR_SET_BIT((var),dst);		\
				   else STR_REMOVE_BIT((var),dst)
// handler.c
#define STR_SAME_STR(a,b,size)	(memcmp((a),(b),(size))==0)
#define STR_COPY_STR(a,b,size)	memcpy((a),(b),(size));
#define STR_AND_STR(a,b,size)	{ int i;for(i=0;i<(size);i++){(a)[i]&=(b)[i];}}
#define STR_OR_STR(a,b,size)	{ int i;for(i=0;i<(size);i++){(a)[i]|=(b)[i];}}
#define STR_XOR_STR(a,b,size)	{ int i;for(i=0;i<(size);i++){(a)[i]^=(b)[i];}}
#define STR_NOT_STR(a,size)	{ int i;for(i=0;i<(size);i++){(a)[i]=~(a)[i];}}

extern char ZERO_FLAG[MAX_FLAGS];
extern char ONE_FLAG[MAX_FLAGS];


/*
 * Character macros.
 */
#define IS_NPC(ch)		(STR_IS_SET((ch)->strbit_act, ACT_IS_NPC))
#define IS_PC(ch)		(!STR_IS_SET((ch)->strbit_act, ACT_IS_NPC))
#define IS_IMMORTAL(ch)		(get_trust(ch) >= LEVEL_IMMORTAL)
#define IS_HERO(ch)		(get_trust(ch) >= LEVEL_HERO)
#define IS_QUESTOR(ch)		(STR_IS_SET((ch)->strbit_act, PLR_QUESTOR))
#define IS_TRUSTED(ch,level)	(get_trust((ch)) >= (level))
#define IS_AFFECTED(ch, sn)	(STR_IS_SET((ch)->strbit_affected_by, (sn)))
#define IS_AFFECTED2(ch, sn)	(STR_IS_SET((ch)->strbit_affected_by2, (sn)))
#define PC_RACE(ch)		((ch)->race>=MAX_PC_RACE?0:(ch)->race)

#define GET_AGE(ch)		((int) (17 + ((ch)->played \
				    + current_time - (ch)->logon )/72000))

#define IS_GOOD(ch)		((ch->alignment) >= 350)
#define IS_EVIL(ch)		((ch->alignment) <= -350)
#define IS_NEUTRAL(ch)		(!IS_GOOD(ch) && !IS_EVIL(ch))

#define IS_AWAKE(ch)		(ch->position > POS_SLEEPING)
#define GET_AC(ch,type)		((ch)->armor[type]			    \
		        + ( IS_AWAKE(ch)			    \
			? dex_app[get_curr_stat(ch,STAT_DEX)].defensive : 0 ))
#define GET_HITROLL(ch)	\
		((ch)->hitroll+str_app[get_curr_stat(ch,STAT_STR)].tohit)
#define GET_DAMROLL(ch) \
		((ch)->damroll+str_app[get_curr_stat(ch,STAT_STR)].todam)

#define IS_OUTSIDE(ch)		(!STR_IS_SET(				    \
				    (ch)->in_room->strbit_room_flags,	    \
				    ROOM_INDOORS))

#define IS_NULL_STRING(s)	(((s)==NULL)||((s)[0]==0))
// note: PUEBLO and MXP are descriptor-related options.
#define HAS_PUEBLO(ch)		((HAS_DESCRIPTOR(ch))&&((ch)->desc->pueblo))
#define HAS_MXP(ch)		((HAS_DESCRIPTOR(ch))&&((ch)->desc->mxp))


#define WAIT_STATE(ch, npulse)	((ch)->wait = UMAX((ch)->wait, (npulse)))
#define DAZE_STATE(ch, npulse)  ((ch)->daze = UMAX((ch)->daze, (npulse)))
#define get_carry_weight(ch)	((ch)->carry_weight + (ch)->silver/10 +  \
						      (ch)->gold * 2 / 5)

#define IS_SWITCHED( ch )       ( ch->desc->original )
#define GET_DESCRIPTOR( ch )    ( (ch)->desc?ch->desc->descriptor:-1 )
#define HAS_DESCRIPTOR( ch )    ( (ch)->desc?TRUE:FALSE )
#define MOB_HAS_TRIGGER(ch,trig)  (STR_IS_SET((ch)->strbit_mprog_triggers,(trig)))
#define OBJ_HAS_TRIGGER(obj,trig) (STR_IS_SET((obj)->strbit_oprog_triggers,(trig)))
#define ROOM_HAS_TRIGGER(room,trig) (STR_IS_SET((room)->strbit_rprog_triggers,(trig)))
#define AREA_HAS_TRIGGER(area,trig) (STR_IS_SET((area)->strbit_aprog_triggers,(trig)))

/*
 * Object macros.
 */
#define CAN_WEAR(obj, part)	(IS_SET((obj)->wear_flags,  (part)))
#define IS_OBJ_STAT(obj, stat)	(STR_IS_SET((obj)->strbit_extra_flags, (stat)))
#define IS_OBJ_STAT2(obj, stat)	(STR_IS_SET((obj)->strbit_extra_flags2, (stat)))
#define IS_WEAPON_STAT(obj,stat)(IS_SET((obj)->value[4],(stat)))
#define WEIGHT_MULT(obj)	((obj)->item_type == ITEM_CONTAINER ? \
	(obj)->value[4] : 100)
#define IS_DIRECTION(name)	(!str_cmp( name, "n"     ) || \
				 !str_cmp( name, "north" ) || \
				 !str_cmp( name, "e"     ) || \
				 !str_cmp( name, "east"  ) || \
				 !str_cmp( name, "s"     ) || \
				 !str_cmp( name, "south" ) || \
				 !str_cmp( name, "w"     ) || \
				 !str_cmp( name, "west"  ) || \
				 !str_cmp( name, "u"     ) || \
				 !str_cmp( name, "up"    ) || \
				 !str_cmp( name, "d"     ) || \
				 !str_cmp( name, "down"  ) )




/*
 * Description macros.
 */
#define NAME(ch)		( IS_NPC(ch) ? (ch)->short_descr : (ch)->name)
#define PERS(ch, looker)	( can_see( (looker), (ch) ) ?		\
				( IS_NPC(ch) ? (ch)->short_descr	\
				: (ch)->name ) : "someone" )



/*
 * some details concerning
 * Lore
 */

struct lore_responses {   /* easily extendable, if necessary */
	char * message;
};


/* change or add to these but keep them in order of improving
   skill, so to speak */
/* erm, no effort to deal with 'traditional' compilers here */

typedef enum lore_ability_table {

	poor = 0, fair, good, excellent

} lore_ability_t;

#define LORE_POOR  40
#define LORE_FAIR  70
#define LORE_GOOD 100

/* LORE_EXCELLENT is effectively, anything over 100 */


/* weight info tables */

struct obj_weight_info {
	int	proportion;
	char *	message;
};



/*
 * end of details concerning
 * Lore
 */



/*
 * Structure for a social in the socials table.
 */
struct	social_type
{
    bool	valid;
    char *	name;
    char *	char_no_arg;
    char *	others_no_arg;
    char *	char_found;
    char *	others_found;
    char *	vict_found;
    char *	char_not_found;
    char *	char_auto;
    char *	others_auto;
    bool	deleted;
    int		type;
    SOCIAL_TYPE	*next;
};

#define SOCIAL_FRIENDLY		(0)
#define SOCIAL_AGGRESSIVE	(1)
#define SOCIAL_NEUTRAL		(2)

/*
 * Structure for an alias for the player
 */
struct	alias_type {
    bool	valid;
    ALIAS_TYPE *next;
    char *	alias;
    char *	command;
};

#include  "fd_data.h"

/*
 * Global variables.
 */
extern		HELP_DATA	  *	help_first;
extern		SHOP_DATA	  *	shop_first;
extern		CLAN_INFO	  *	clan_list;
extern		RANK_INFO	  *	rank_list;

extern		CHAR_DATA	  *	char_list;
extern		CHAR_DATA	  *	player_list;
extern		DESCRIPTOR_DATA   *	descriptor_list;
extern		OBJ_DATA	  *	object_list;
extern		FS_ENTRY	  *	fs_list;

extern		time_t			current_time;
extern		FILE *			fpReserve;
extern		TIME_INFO_DATA		time_info;
extern		WEATHER_DATA		weather_info;
extern		bool			MOBtrigger;
extern		MUD_DATA		mud_data;



/*
 * Data files used by the server.
 *
 * AREA_LIST contains a list of areas to boot.
 * All files are read in completely at bootup.
 * Most output files (bug, idea, typo, shutdown) are append-only.
 *
 * The NULL_FILE is held open so that we have a stream handle in reserve,
 * so players can go ahead and telnet to all the other descriptors.
 * Then we close it whenever we need to open a file (e.g. a save file).
 */

#define	NULL_FILE	"/dev/null"
#define PLAYER_DIR	"player"
#define HARDCORE_DIR	"hardcore"
#define AREA_DIR	"area"
#define ROOM_DIR	"area"
#define LOG_DIR		"log"
#define CONFIG_DIR	"area"
#define NOTESROOT_DIR	"notes"

#define TEMP_FILE	"romtmp"

#define AREA_LIST	"area.lst"	// List of areas
#define BUG_DIR		"bugs"		// Notes: bugs
#define TYPO_DIR	"typos"		// Notes: typos
#define NOTE_DIR	"notes"		// Notes: notes
#define IDEA_DIR	"ideas"		// Notes: ideas
#define PENALTY_DIR	"penal"		// Notes: penalties
#define NEWS_DIR	"news"		// Notes: news
#define CHANGES_DIR	"chang"		// Notes: changes
#define MCNOTE_DIR	"mcnote"	// Notes: changes

#define COPYOVER_FILE	"copyover.data"	// Copyover stuff
#define SHUTDOWN_FILE   "shutdown.txt"	// If this file exist, the mud
					// will not comeback
#define SOCIAL_FILE	"social.are"	// Socials
#define PINKY_FILE	"pinky.txt"	// Pinky quotes

#define CLAN_FILE	"fd_clan.tbl"	// Clan data
#define MUSIC_FILE	"music.txt"	// Music for jukeboxes
#define JOB_FILE	"jobs.txt"
#define PROPERTY_FILE	"property.txt"	// Properties
#define POLL_FILE	"poll.txt"	// Poll results
#define BADNAMES_FILE	"badnames.txt"	// Bad and banned names

#define NEWBIE_FILE	"fd_newbie.txt"	// Newbie settings
#define I3_FILE		"fd_i3.config"	// Intermud3 config
#define CONFIG_FILE	"fd_config.txt"	// Mud config
#define BAN_FILE	"ban.txt"	// Banned sites
#define SWEAR_FILE	"swear.txt"	// Bad words
#define STATS_FILE	"stats.txt"	// Statistics
#define WHOLIST_FILE	"wholist.txt"	// wholist
#define WATCHDOG_FILE	"watchdog"	// external watchdog

#ifdef HAS_MCCP
#define	TELOPT_MCCP		85
#define	TELOPT_MCCP2		86
#endif
#define	TELOPT_MSP		90
#define	TELOPT_MXP		91



/*
 * Our function prototypes.
 * One big lump ... this is every function in Merc.
 */
#define CD	CHAR_DATA
#define MID	MOB_INDEX_DATA
#define OD	OBJ_DATA
#define OID	OBJ_INDEX_DATA
#define RID	ROOM_INDEX_DATA
#define SF	SPEC_FUN
#define ED	EFFECT_DATA
#define TPC	TCLPROG_CODE
#define EDD	EXTRA_DESCR_DATA

/* act_comm.c */
void	add_follower	(CHAR_DATA *ch, CHAR_DATA *master );
void	stop_follower	(CHAR_DATA *ch );
void 	nuke_pets	(CHAR_DATA *ch );
void	die_follower	(CHAR_DATA *ch );
bool	is_same_group	(CHAR_DATA *ach, CHAR_DATA *bch );
bool	is_same_group_recursive	(CHAR_DATA *ach, CHAR_DATA *bch );
bool	has_same_master	(CHAR_DATA *ach, CHAR_DATA *bch );
bool	has_same_master_recursive	(CHAR_DATA *ach, CHAR_DATA *bch );
void	announce	(CHAR_DATA *ch,const char *string,...) ;
void    quit_char	(CHAR_DATA *ch );
char   *makedrunk	(CHAR_DATA *ch,char *argument,bool forced);
void	set_afk_state	(CHAR_DATA *ch, bool is_afk, bool manual);

/* act_enter.c */
RID	*get_portal_destination  (CHAR_DATA *ch,OBJ_DATA *portal);
RID	*get_random_room  (CHAR_DATA *ch);
void	enter             ( CHAR_DATA *ch, char *argument,long type, OBJ_DATA *aportal);

/* act_info.c */
void	set_title	( CHAR_DATA *ch, char *title );
char   *get_afk_title	( CHAR_DATA *ch );

void    lore            ( CHAR_DATA *, OBJ_DATA *, bool );
void    lore_verbosity  ( CHAR_DATA *, bool, lore_ability_t );
char *  obj_weight_message ( CHAR_DATA *, int, bool );
void	look_room	(CHAR_DATA *ch, ROOM_DATA *room, bool enablebrief);
void	look_through	(CHAR_DATA *ch, OBJ_DATA *obj);
void	look_in		(CHAR_DATA *ch, OBJ_DATA *obj, char *prefix);
void    look            ( CHAR_DATA *, char *, bool, bool );
bool	check_blind	( CHAR_DATA *ch, bool show_message );
int     get_direction   (const char *arg1);
int	do_count	( CHAR_DATA *ch, char *argument, bool html);
extern  char * const	where_name[];
int	mob_compare	( CHAR_DATA *ch, OBJ_DATA *obj);
void	show_exits	( CHAR_DATA *ch, bool fAuto);

/* act_move.c */
ROOM_INDEX_DATA *exit_target(CHAR_DATA *ch, EXIT_DATA *pexit);
bool	is_char_allowed_in_room(CHAR_DATA *ch, ROOM_INDEX_DATA *room, bool loud);
void	move_char	( CHAR_DATA *ch, int door, bool follow, bool ignorecharm  );
void    sit_obj         ( CHAR_DATA *ch, OBJ_DATA *obj, int quiet);
void    rest_obj        ( CHAR_DATA *ch, OBJ_DATA *obj, int quiet);
void	event_recall	( EVENT_DATA *event);

/* act_obj.c */
bool can_loot		(CHAR_DATA *ch, OBJ_DATA *obj );
bool obj_is_owned	(OBJ_DATA *obj);
bool obj_is_owned_by_another(CHAR_DATA *ch, OBJ_DATA *obj);
void    get_obj         ( CHAR_DATA *ch, OBJ_DATA *obj,
                            OBJ_DATA *container );
void get_from_container(CHAR_DATA *ch, OBJ_DATA *container, int amount, char *arg);
void	object_drop_air	( OBJ_DATA *obj );
void	object_drop_water ( OBJ_DATA *obj );
void	object_drop_fall ( OBJ_DATA *obj );

/* alias.c */
void 	substitute_alias (DESCRIPTOR_DATA *d, char *input);

/* comm.c */
void	nanny		( DESCRIPTOR_DATA *d, char *argument );
void	init_char_descriptor	(DESCRIPTOR_DATA *d,CHAR_DATA *ch);


/* db.c */
void	get_memory	( BUFFER *buf,char *argument );
void	init_races	( void );
char *	print_flags	( long flag );
void	boot_db		( void );
void	area_update	( void );
CD *	create_mobile	( MOB_INDEX_DATA *pMobIndex );
void	clone_mobile	( CHAR_DATA *parent, CHAR_DATA *clone);
OD *	create_object	( OBJ_INDEX_DATA *pObjIndex, int level );
OD *	create_object_data	( OBJ_INDEX_DATA *pObjIndex, int level );
void	create_object_tcl	( OBJ_DATA *obj );
void	clone_object	( OBJ_DATA *parent, OBJ_DATA *clone );
void	clear_char	( CHAR_DATA *ch );
EDD *	get_extra_descr	( const char *name, EXTRA_DESCR_DATA *ed );
MID *	get_mob_index	( int vnum );
OID *	get_obj_index	( int vnum );
RID *	get_room_index	( int vnum );
int	number_door	( void );
void	append_file	( CHAR_DATA *ch, char *file, char *str );
void	tail_chain	( void );
TPC *	get_mprog_index ( int vnum );
TPC *	get_oprog_index ( int vnum );
TPC *	get_rprog_index ( int vnum );
TPC *	get_aprog_index ( int vnum );

/* effect.c */
void	acid_effect	(void *vo, int level, int dam, int target);
void	cold_effect	(void *vo, int level, int dam, int target);
void	fire_effect	(void *vo, int level, int dam, int target);
void	poison_effect	(void *vo, int level, int dam, int target);
void	shock_effect	(void *vo, int level, int dam, int target);


/* handler.c */
ROOM_INDEX_DATA *	find_location( CHAR_DATA *ch, char *arg );
int  	show_sex	( CHAR_DATA *ch);
ED  	*effect_find (EFFECT_DATA *paf, int where, int sn);
void	effect_check	(CHAR_DATA *ch, int where, int vector);
void	effect_check_bit (CHAR_DATA *ch, int where, int vector);
int	count_users	(OBJ_DATA *obj);
void 	deduct_cost	(CHAR_DATA *ch,int costgold,int costsilver,bool canuseCreditCard);
void	effect_enchant	(OBJ_DATA *obj);
int 	check_immune	(CHAR_DATA *ch, int dam_type);
int	exact_command_to_vnum (const char arg[]);
int	command_to_vnum (const char arg[]);
const char *vnum_to_command (const int vnum);
bool	is_clan		(CHAR_DATA *ch);
bool	is_same_clan	(CHAR_DATA *ch, CHAR_DATA *victim);
bool	is_old_mob	(CHAR_DATA *ch);
int	get_weapon_sn	(CHAR_DATA *ch, bool secondary );
int	get_weapon_skill(CHAR_DATA *ch, int sn );
int     get_age         ( CHAR_DATA *ch );
void	reset_char	( CHAR_DATA *ch );
int	get_trust	( CHAR_DATA *ch );
int	get_curr_stat	( CHAR_DATA *ch, int stat );
int 	get_max_train	( CHAR_DATA *ch, int stat );
int	can_carry_n	( CHAR_DATA *ch );
int	can_carry_w	( CHAR_DATA *ch );
bool	player_exist	( char * );
bool	is_name		( char *str, char *namelist );
bool	is_exact_name	( char *str, char *namelist );
void	effect_to_char	( CHAR_DATA *ch, EFFECT_DATA *paf );
void	effect_to_obj	( OBJ_DATA *obj, EFFECT_DATA *paf );
void	effect_to_room  (ROOM_INDEX_DATA *room, EFFECT_DATA *paf);
void	effect_remove	( CHAR_DATA *ch, EFFECT_DATA *paf );
void	effect_remove_obj (OBJ_DATA *obj, EFFECT_DATA *paf );
void	effect_remove_room ( ROOM_INDEX_DATA *room, EFFECT_DATA *paf);
void	effect_strip	( CHAR_DATA *ch, int sn );
bool	is_affected	( CHAR_DATA *ch, int sn );
ED *	obj_has_effect	( OBJ_DATA *obj, int effect );
void	effect_join	( CHAR_DATA *ch, EFFECT_DATA *paf );
void	char_from_room	( CHAR_DATA *ch );
void	char_to_room	( CHAR_DATA *ch, ROOM_INDEX_DATA *pRoomIndex );
void	obj_to_char	( OBJ_DATA *obj, CHAR_DATA *ch );
void	obj_from_char	( OBJ_DATA *obj );
int	apply_ac	( OBJ_DATA *obj, int iWear, int type );
OD *	get_eq_char	( CHAR_DATA *ch, int iWear );
void	equip_char	( CHAR_DATA *ch, OBJ_DATA *obj, int iWear );
void	unequip_char	( CHAR_DATA *ch, OBJ_DATA *obj );
int	count_obj_list	( OBJ_INDEX_DATA *obj, OBJ_DATA *list );
void	obj_from_room	( OBJ_DATA *obj );
void	obj_to_room	( OBJ_DATA *obj, ROOM_INDEX_DATA *pRoomIndex );
void	obj_to_obj	( OBJ_DATA *obj, OBJ_DATA *obj_to );
void	obj_from_obj	( OBJ_DATA *obj );
void	extract_owned_obj(const char *owner, long except);
void	extract_invalid_owned_obj_from_char(CHAR_DATA *ch);
void	extract_obj	( OBJ_DATA *obj );
void	extract_char	( CHAR_DATA *ch, bool fPull );
CD *	get_char_room	( CHAR_DATA *ch, char *argument );
CD *	get_char_world	( CHAR_DATA *ch, char *argument );
CD *	get_char_area	( CHAR_DATA *ch, char *argument );
OD *	get_obj_type	( OBJ_INDEX_DATA *pObjIndexData );
OD *	get_obj_list	( CHAR_DATA *ch, char *argument, OBJ_DATA *list );
OD *	get_obj_list_numbered	( CHAR_DATA *ch, char *argument, OBJ_DATA *list, int *count );
OD *	get_obj_carry	( CHAR_DATA *ch, char *argument, CHAR_DATA *viewer );
OD *	get_obj_carry_numbered	( CHAR_DATA *ch, char *argument, CHAR_DATA *viewer, int *count );
bool	get_obj_carry_by_vnum ( CHAR_DATA *ch, int vnum);
OD *	get_obj_wear	( CHAR_DATA *ch, char *argument );
OD *	get_obj_wear_numbered	( CHAR_DATA *ch, char *argument, int *count );
OD *	get_obj_here	( CHAR_DATA *ch, char *argument );
OD *	get_obj_world	( CHAR_DATA *ch, char *argument );
OD *	get_obj_here_by_objtype (OBJ_INDEX_DATA *pObjIndex,OBJ_DATA *list);
OD *	create_money	( int gold, int silver );
int	get_obj_number	( OBJ_DATA *obj );
int	get_obj_weight	( OBJ_DATA *obj );
int	get_true_weight	( OBJ_DATA *obj );
bool	room_is_dark	( ROOM_INDEX_DATA *pRoomIndex );
bool	is_room_owned	( CHAR_DATA *ch, ROOM_INDEX_DATA *room);
bool	is_room_owner	( CHAR_DATA *ch, ROOM_INDEX_DATA *room);
bool	room_is_private	( ROOM_INDEX_DATA *pRoomIndex );
bool	is_allowed_in_room ( CHAR_DATA *ch, ROOM_INDEX_DATA *room);
bool	check_ordered	(CHAR_DATA *ch);
bool	can_see		( CHAR_DATA *ch, CHAR_DATA *victim );
bool	can_see_obj	( CHAR_DATA *ch, OBJ_DATA *obj );
bool	can_see_room	( CHAR_DATA *ch, ROOM_INDEX_DATA *pRoomIndex);
bool	can_drop_obj	( CHAR_DATA *ch, OBJ_DATA *obj );
bool	is_anti_race	( OBJ_DATA *obj, int race );
bool	is_anti_class	( OBJ_DATA *obj, int Class );
int	is_race_poison	( OBJ_DATA *obj, int race );

void	strbitset	(char *string,int bit);
void	strbitclr	(char *string,int bit);
bool	isstrbitset	(char *string,int bit);
void	strbittgl	(char *string,int bit);

CHAR_DATA	*hallucination_char(void);
OBJ_DATA	*hallucination_obj(void);

char *	stringtohex	(char *input,char *output,int length);
char *	hextostring	(char *input,char *output,int length);
char *	url		(char *url,char *file,AREA_DATA *area,bool show);
int	exp_to_level	(CHAR_DATA *ch);
void	set_phased_skill (CHAR_DATA *ch,PHASE_FUN *proc,void *target,int step,long flags);
void 	char_obj_alignment_check( CHAR_DATA *ch );

/* interp.c */
void	interpret	( CHAR_DATA *ch, char *argument );
int	number_argument	( char *argument, char *arg );
int	mult_argument	( char *argument, char *arg);
char *	one_argument	( char *argument, char *arg_first );
bool	check_social	( CHAR_DATA *ch, char *command, char *argument );
int	which_keyword       (char *keyword, ...);
int	which_exact_keyword (char *keyword, ...);


/* notes.c */
void load_poll (void);

/* save.c */
void	save_room_obj	( ROOM_INDEX_DATA *room);
void    save_all_room_obj (void);
void    new_save_all_room_obj (void);
void	save_char_obj	( CHAR_DATA *ch );
void    load_all_room_obj (void);
void	new_load_all_room_obj (void);
bool	load_char_obj	( DESCRIPTOR_DATA *d, char *name );
bool	getplayerinfo	(CHAR_DATA *ch,char *name,PLAYER_INFO *pi);

/* skills.c */
bool    skill_exists (int gsn);
char *  skill_name (int gsn,CHAR_DATA *);
bool    skill_implemented (int gsn,CHAR_DATA *);
int     skill_level (int gsn,CHAR_DATA *ch);
int     skill_rating (int gsn,CHAR_DATA *ch);
int     skill_adept (int gsn,CHAR_DATA *ch);
SPELL_FUN *skill_spell_fun (int gsn,CHAR_DATA *);
WEAROFF_FUN *skill_wearoff_fun (int gsn,CHAR_DATA *);
int     skill_target (int gsn,CHAR_DATA *);
int     skill_minimum_position (int gsn,CHAR_DATA *);
int *	skill_pgsn (int gsn,CHAR_DATA *);
int     skill_min_mana (int gsn,CHAR_DATA *ch);
int     skill_beats (int gsn,CHAR_DATA *);
char *  skill_noun_damage (int gsn,CHAR_DATA *);
char *  skill_msg_off (int gsn,CHAR_DATA *);
char *  skill_msg_obj (int gsn,CHAR_DATA *);
char *  skill_msg_off_room (int gsn,CHAR_DATA *);
SKILLTABLE_TYPE *skill_skilltype (int gsn,CHAR_DATA *ch);
bool	has_gained_skill (CHAR_DATA *ch, int sn );
bool	has_skill_available (CHAR_DATA *ch, int sn );
int	get_current_skill(CHAR_DATA *ch, int sn);
int	get_peak_skill(CHAR_DATA *ch, int sn);
int	get_skill (CHAR_DATA *ch, int sn );
bool	skillcheck(CHAR_DATA *ch, int gsn);
bool	skillcheck2(CHAR_DATA *ch, int gsn, int basechance);
void	update_skill_decay(CHAR_DATA *ch);

bool    group_exists (int gn);
bool    group_spell_exists (int gn,CHAR_DATA *ch,int sn);
char *  group_name (int gn,CHAR_DATA *ch);
int     group_rating (int gn,CHAR_DATA *ch);
char *  group_spell (int gn,CHAR_DATA *ch,int sn);

bool 	parse_gen_groups ( CHAR_DATA *ch,char *argument );
void	list_lost_skills ( CHAR_DATA *ch);
void 	list_group_costs ( CHAR_DATA *ch );
void    list_group_known ( CHAR_DATA *ch );
int 	exp_per_level	( CHAR_DATA *ch, int points );
void 	check_improve	( CHAR_DATA *ch, int sn, bool success,
				    int multiplier );
int 	group_lookup	(const char *name);
void	gn_add		( CHAR_DATA *ch, int gn);
void 	gn_remove	( CHAR_DATA *ch, int gn);
void 	group_add	( CHAR_DATA *ch, const char *name, bool deduct);
void	group_remove	( CHAR_DATA *ch, const char *name);

void	gendata_clear	( CHAR_DATA *ch);
void 	gendata_group_add (CHAR_DATA *ch,const char *name,bool deduct);
void	gendata_group_remove (CHAR_DATA *ch,const char *name);
void	gendata_to_pcdata (CHAR_DATA *ch);

bool	may_choose_group	(CHAR_DATA *ch,int gn);
bool	may_choose_skill	(CHAR_DATA *ch,int sn);

int	base_points	( CHAR_DATA *ch);

/* special.c */
SF *	spec_lookup	( const char *name );
char *	spec_string	( SPEC_FUN *function );

/* update.c */
void	advance_level	( CHAR_DATA *ch, bool hide );
void	gain_exp	( CHAR_DATA *ch, int gain, bool noisy );
void	gain_condition	( CHAR_DATA *ch, int iCond, int value );
void	update_handler	( void );

/* fd_swear.c */
char *	check_swearing	( char *sentence);
void	complain_swearing	( CHAR_DATA *ch, bool personal);

/* fd_hunt.c */
void	hunt_victim	( CHAR_DATA *ch );
int	find_path	(CHAR_DATA *ch,int from_room,int to_room,int max_depth,bool trough_doors,bool in_zone);


/* fd_penalty.c */
void	log_char	(CHAR_DATA *victim,char *reason,int timeout);
void	ool_penalize	(CHAR_DATA *ch);
int	get_ool_penalty_state(CHAR_DATA *ch);
void	show_ool_cantrip(CHAR_DATA *ch, CHAR_DATA *victim, int type);

/* quest.c */
void	quest		(CHAR_DATA *ch, int quest_action);

#undef	CD
#undef	MID
#undef	OD
#undef	OID
#undef	RID
#undef	SF
#undef AD
#undef MPC

/*****************************************************************************
 *                                    OLC                                    *
 *****************************************************************************/

/*
 * Object defined in limbo.are
 * Used in save.c to load objects that don't exist.
 */
#define OBJ_VNUM_DUMMY	1



/*
 * Area flags.
 */
#define         AREA_NONE       0
#define         AREA_CHANGED    (A)	/* Area has been modified. */
#define         AREA_ADDED      (B)	/* Area has been added to. */
#define         AREA_LOADING    (C)	/* Used for counting in db.c */
#define		AREA_VERBOSE	(D)	/* ???? */
#define		AREA_DELETE	(E)	/* These area's won't be saved in the list */
#define		AREA_UNFINISHED	(F)	/* Area is not finished yet. */
#define		AREA_PKZONE	(G)	/* Player killing is alowed in this zone */
#define		AREA_CONQUEST	(H)	/* Can this area be conqquered? */
#define		AREA_ARENA	(I)	/* Player kan pk eachother without losing anything */
#define		AREA_PC_BLOCKS_RESPAWN	(J)	/* mobs and obj don't reset if a pc is in the area */
#define		AREA_PROTECTED	(K)



#define MAX_DIR	6
#define NO_FLAG -99	/* Must not be used in flags or stats. */

/*
 * Global variables
 */
extern          AREA_DATA *             area_first;
extern          AREA_DATA *             area_last;
extern  	SHOP_DATA *             shop_last;

extern          char                    str_empty       [1];

extern  MOB_INDEX_DATA *        mob_index_hash  [MAX_KEY_HASH];
extern  OBJ_INDEX_DATA *        obj_index_hash  [MAX_KEY_HASH];
extern  ROOM_INDEX_DATA *       room_index_hash [MAX_KEY_HASH];
extern  TCLPROG_CODE *		rprog_index_hash [MAX_KEY_HASH];
extern  TCLPROG_CODE *		mprog_index_hash [MAX_KEY_HASH];
extern  TCLPROG_CODE *		oprog_index_hash [MAX_KEY_HASH];



/* db.c */
void	reset_area      ( AREA_DATA * pArea );
void	reset_room	( ROOM_INDEX_DATA *pRoom );

/* olc.c */
bool	run_olc_editor	( DESCRIPTOR_DATA *d );
char	*olc_ed_name	( CHAR_DATA *ch );
char	*olc_ed_vnum	( CHAR_DATA *ch );

/* olc_save.c */
char 	*fix_string	( const char *str );
void	save_socials	(void);

// pueblo needs this
#define URLBASE  "http://www.fataldimensions.nl"

#endif	// INCLUDED_MERC_H
