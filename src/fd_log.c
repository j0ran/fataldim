//
// $Id: fd_log.c,v 1.13 2005/12/08 12:18:53 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"
#include "db.h"


//
// Reports a bug to stderr. If we're loading the database, then also print
// the area-name
//
void bugf( const char *str, ...) {
    char buf[MAX_STRING_LENGTH];
    char s[2*MAX_INPUT_LENGTH],*ps;
    int i;
    va_list ap;
    static bool beenhere=FALSE;

    if (beenhere) {
	logf("[*] bugf(): Multiple calls to bug(), this is a bug.");
	return;
    }

    beenhere=TRUE;
    if (fBootDb)
	logf("[*] FILE: %s (line %d)",mud_data.currentareafile,mud_data.currentline);

    va_start(ap,str);
    vsprintf(buf,str,ap);
    va_end(ap);
    logf( "[*] BUG: %s",buf );

    // escape colour codes
    ps=s;
    for (i=0;buf[i];i++) {
	*ps++=buf[i];
	if (buf[i]=='$')
	    *ps++='$';
	if (buf[i]=='{')
	    *ps++='{';
    }
    *ps=0;
    wiznet(WIZ_BUGS,0,NULL,(void *)s,"[*****] BUG: $t");
    beenhere=FALSE;
    return;
}

//
// Log a message to stderr.
//
void log_stderr( const char *str,... ) {
    char	buf[MSL];
    char	strtime[MSL];
    va_list	ap;
    time_t	now=time(NULL);
    struct tm	lt;

    va_start(ap,str);
    vsprintf(buf,str,ap);
    va_end(ap);

    if (mud_data.no_timestamp_log) {
	fprintf(stderr,"%s\n",buf);
    } else {
	lt=*localtime(&now);
	strftime(strtime,sizeof(strtime),"%Y-%m-%d %T %a",&lt);
	fprintf(stderr,"%s %s\n",strtime,buf);
    }
    return;
}



//
// Log a message on wiznet.
//
char wiznetflags_visible[WIZNET_FLAGS];
void update_wiznet(bool add,char *flags) {
    if (add) {
	STR_OR_STR(wiznetflags_visible,flags,WIZNET_FLAGS);
    } else {
	CHAR_DATA *player=player_list;

	STR_ZERO_BIT(wiznetflags_visible,WIZNET_FLAGS);
	while (player!=NULL) {
	    STR_OR_STR(wiznetflags_visible,player->strbit_wiznet,WIZNET_FLAGS);
	    player=player->next_player;
	}
    }
}


void wiznet(int flag,int min_level,CHAR_DATA *ch,OBJ_DATA *obj,const char *str,...) {

    CHAR_DATA *	player;
    va_list     ap;
    char	buf[MSL];

    // don't waste time here.
    if (flag && !STR_IS_SET(wiznetflags_visible,flag))
	return;

    va_start(ap,str);
    vsprintf(buf,str,ap);
    va_end(ap);

    for ( player = player_list; player != NULL; player = player->next_player ) {
        if (IS_IMMORTAL(player)
	&&  STR_IS_SET(player->strbit_wiznet,WIZ_ON)
	&&  (!flag || STR_IS_SET(player->strbit_wiznet,flag))
	&&  get_trust(player) >= min_level
	&&  player != ch)
        {
	    bool had_raw=STR_IS_SET(player->strbit_act,PLR_HAS_RAW_COLOUR);
	    STR_SET_BIT(player->strbit_act,PLR_HAS_RAW_COLOUR);
	    if (STR_IS_SET(player->strbit_wiznet,WIZ_PREFIX))
	  	send_to_char("--> ",player);
	    if (!ch && !obj) {
		send_to_char(buf,player);
		send_to_char("\r\n",player);
	    } else
		act_new(buf,player,obj,ch,TO_CHAR,POS_DEAD);
	    if (!had_raw)
		STR_REMOVE_BIT(player->strbit_act,PLR_HAS_RAW_COLOUR);
        }
    }

    return;
}

void do_wiznet(CHAR_DATA *ch,char *argument) {
    int		flag;
    char	buf[MAX_STRING_LENGTH];
    char	target[MAX_STRING_LENGTH];
    char	command[MAX_STRING_LENGTH];
    CHAR_DATA *	victim;

    argument=one_argument(argument,command);

    if (!str_cmp(command,"on")) {
	if (STR_IS_SET(ch->strbit_wiznet,WIZ_ON))
	    send_to_char("You never left Wiznet!\n\r",ch);
	else
	    send_to_char("Welcome to Wiznet!\n\r",ch);
	STR_SET_BIT(ch->strbit_wiznet,WIZ_ON);
	return;
    }
    if (!str_cmp(command,"off")) {
	if (STR_IS_SET(ch->strbit_wiznet,WIZ_ON))
	    send_to_char("Signing off of Wiznet!\n\r",ch);
	else
	    send_to_char("You keep ignoring Wiznet!\n\r",ch);
	STR_REMOVE_BIT(ch->strbit_wiznet,WIZ_ON);
	return;
    }


    if (command[0]==0
    || !str_cmp(command,"status")
    || !str_cmp(command,"global")) {
	char *wiznetflags;

	victim=ch;
	one_argument(argument,target);

	if (buf[0]) {
	    if (get_trust(ch)>=LEVEL_IMMORTAL) {
		victim=get_char_world(ch,argument);
		if (victim) {
		    if (get_trust(ch)>=get_trust(victim))
			argument[0]=0;
		    if (IS_NPC(victim)) {
			send_to_char("Mobs don't do wiznet.\n\r",ch);
			return;
		    }
		} else
		    victim=ch;
	    }
	}

	if (command[0]==0) wiznetflags=victim->strbit_wiznet;
	if (!str_cmp(command,"status")) wiznetflags=victim->strbit_wiznet;
	if (!str_cmp(command,"global")) wiznetflags=wiznetflags_visible;

	buf[0] = '\0';

	// display ON and PREFIX
	for (flag = 0; flag<2; flag++) {
	    strcat(buf,
		(!is_channel_allowed(victim,flag))?"{w<n/a> ":
		STR_IS_SET(wiznetflags,wiznet_table[flag].flag)?
		    "  {y[X]{x ":"  {y[ ]{x ");
	    strcat(buf,wiznet_table[flag].name);
	    strcat(buf,spaces(20-strlen(wiznet_table[flag].name)));
	}
	strcat(buf,"\n\r");

	// display the rest of the flags
	for (flag = 2; wiznet_table[flag].name != NULL; flag++) {
	    strcat(buf,
		(!is_channel_allowed(victim,flag))?"{w<n/a> ":
		STR_IS_SET(wiznetflags,wiznet_table[flag].flag)?
		    "  {y[X]{x ":"  {y[ ]{x ");
	    strcat(buf,wiznet_table[flag].name);
	    strcat(buf,spaces(20-strlen(wiznet_table[flag].name)));
	    if ((flag-2)%3==2)
		strcat(buf,"\n\r");
	}
	if ((flag-2)%3!=0)
	    strcat(buf,"\n\r");

	if (command[0]==0
	|| !str_cmp(command,"status"))
	    sprintf_to_char(ch,"Wiznet status for %s:\n\r",victim->name);
	if (!str_cmp(command,"global"))
	    send_to_char("Global wiznet status\n\r",ch);

	send_to_char(buf,ch);
	return;
    }

    flag=wiznet_lookup(command);
    if (flag==-1) {
	send_to_char("No such option for wiznet\n\r",ch);
	return;
    }
    if (!is_channel_allowed(ch,flag)) {
	send_to_char("Option is not available for you.\n\r",ch);
	return;
    }
    STR_TOGGLE_BIT(ch->strbit_wiznet,wiznet_table[flag].flag);
    if (!STR_IS_SET(ch->strbit_wiznet,wiznet_table[flag].flag)) {
	update_wiznet(FALSE,NULL);
	sprintf_to_char(ch,"You will no longer see %s on wiznet.\n\r",
	    wiznet_table[flag].name);
    } else {
	 update_wiznet(TRUE,ch->strbit_wiznet);
	 sprintf_to_char(ch,"You will now see %s on wiznet.\n\r",
		    wiznet_table[flag].name);
    }

}

//
// Log a mob
//
void do_mud_logging(CHAR_DATA *ch,char *argument) {
    CHAR_DATA *	victim;
    char	arg1[MSL],arg2[MSL];

    if (ch->level<LEVEL_COUNCIL) {
	send_to_char("Only level 99 (council) and higher!\n\r",ch);
	return;
    }

    argument=one_argument(argument,arg1);
    argument=one_argument(argument,arg2);

    if ( arg1[0]==0 || ( victim = get_char_world( ch, arg1 ) ) == NULL ) {
	send_to_char( "Put log on who?\n\r", ch );
	return;
    }

    if( IS_PC(victim) ) {
	send_to_char("Only on NPC's.\n\r",ch);
	return;
    }

    if (victim->desc==NULL) {
	DESCRIPTOR_DATA *logging_descriptor;
	char filename[MSL];
	int desc;

	if (arg2[0]==0) {
	    sprintf(filename,"logging.%d",victim->pIndexData->vnum);
	} else {
	    if (strchr(arg2,'/')) {
		send_to_char("Invalid filename, should not contain a /\n\r",ch);
		return;
	    }
	    sprintf(filename,"logging.%s",arg2);
	}


	if ((desc=open(filename,O_RDWR|O_CREAT|O_TRUNC,0644))==-1) {
	    sprintf_to_char(ch,"Couldn't open logging to %s, reason %s\n\r",filename,ERROR);
	    return;
	}
	// create a logging descriptor
	logging_descriptor=new_descriptor(DESTYPE_MUD);
	logging_descriptor->descriptor=desc;
	lseek(logging_descriptor->descriptor,0,SEEK_END);
	logging_descriptor->Host[0]=0;
	logging_descriptor->Host[1]=0;
	logging_descriptor->Host[2]=0;
	logging_descriptor->Host[3]=0;
	logging_descriptor->connected=CON_LOGGING;
	logf( "[%d] Created logging character, logging to %s", logging_descriptor->descriptor,filename);
	logging_descriptor->next    = descriptor_list;
	descriptor_list             = logging_descriptor;

	logging_descriptor->character=victim;
	victim->desc=logging_descriptor;
	send_to_char("Logging enabled.\n\r",ch);
    } else if (victim->desc->connected==CON_LOGGING) {
	logf( "[%d] Removing logging character", victim->desc->descriptor);
	close(victim->desc->descriptor);
	if (victim->desc==descriptor_list) {
	    descriptor_list=descriptor_list->next;
	} else {
	    DESCRIPTOR_DATA *d;
	    for ( d=descriptor_list; d && d->next!=victim->desc; d=d->next ) ;
	    if ( d != NULL )
		d->next = victim->desc->next;
	}
	free_descriptor(victim->desc);
	victim->desc=NULL;
	send_to_char("Logging removed.\n\r",ch);
    } else {
	send_to_char("Cannot put a log on this one.\n\r",ch);
    }
}
