// $Id: olc_aedit.c,v 1.19 2006/08/19 19:11:44 jodocus Exp $
/***************************************************************************
 *  File: olc.c                                                            *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 *                                                                         *
 *  This code was freely distributed with the The Isles 1.1 source code,   *
 *  and has been used here for OLC - OLC would not be what it is without   *
 *  all the previous coders who released their source code.                *
 *                                                                         *
 ***************************************************************************/

#include "merc.h"
#include "olc.h"
#include "interp.h"
#include "db.h"

/*****************************************************************************
 Name:		check_range( lower vnum, upper vnum )
 Purpose:	Ensures the range spans only one area.
 Called by:	aedit_vnum(olc_act.c).
 ****************************************************************************/
bool check_range( int lower, int upper )
{
    AREA_DATA *pArea;
    int cnt = 0;

    for ( pArea = area_first; pArea; pArea = pArea->next )
    {
        /*
     * lower < area < upper
     */
        if ( ( lower <= pArea->lvnum && upper >= pArea->lvnum )
             ||   ( upper >= pArea->uvnum && lower <= pArea->uvnum ) )
            cnt++;

        if ( cnt > 1 )
            return FALSE;
    }
    return TRUE;
}



AREA_DATA *get_vnum_area( int vnum )
{
    AREA_DATA *pArea;

    for ( pArea = area_first; pArea; pArea = pArea->next )
    {
        if ( vnum >= pArea->lvnum
             && vnum <= pArea->uvnum )
            return pArea;
    }

    return 0;
}



/*
 * Area Editor Functions.
 */
bool aedit_show( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;
    PROPERTY_INDEX_TYPE *prop;

    EDIT_AREA(ch, pArea);

    sprintf_to_char(ch, "{yName:{x           %d [%s]\n\r", pArea->vnum, pArea->name);
    sprintf_to_char(ch, "{yDescription:{x\n\r%s",pArea->description);
    sprintf_to_char(ch, "{yComment:{x\n\r%s", pArea->comment);
    sprintf_to_char(ch, "{yCreator:{x        [%s]\n\r", pArea->creator);
    sprintf_to_char(ch, "{yRecall:{x         [%5d] %s\n\r", pArea->recall,
                    get_room_index( pArea->recall )
                    ? get_room_index( pArea->recall )->name : "none" );
    sprintf_to_char(ch, "{yLogin-room:{x     [%5d] %s\n\r", pArea->login_room,
                    get_room_index( pArea->login_room )
                    ? get_room_index( pArea->login_room )->name : "last-used" );
    sprintf_to_char(ch, "{yFile:{x           [%s]\n\r", pArea->filename );
    sprintf_to_char(ch, "{yVnums:{x          [%d-%d] %d in total\n\r",
                    pArea->lvnum, pArea->uvnum, pArea->uvnum-pArea->lvnum+1 );
    sprintf_to_char(ch, "{yIn use:{x         mobs: %d rooms: %d objects: %d\n\r",
                    pArea->vnum_mobs_in_use,
                    pArea->vnum_rooms_in_use,
                    pArea->vnum_objects_in_use);
    sprintf_to_char(ch, "{yLevel:{x          [%d-%d]\n\r",
                    pArea->llevel, pArea->ulevel );
    sprintf_to_char(ch, "{yAge:{x            %d\n\r", pArea->age );
    sprintf_to_char(ch, "{yPlayers:{x        %d\n\r", pArea->nplayer );
    sprintf_to_char(ch, "{yVersion:{x        %d\n\r", pArea->version );
    sprintf_to_char(ch, "{ySecurity:{x       [%d]\n\r", pArea->security );
    sprintf_to_char(ch, "{yBuilders:{x       [%s]\n\r", pArea->builders );
    sprintf_to_char(ch, "{yURL pref:{x       [%s]\n\r", pArea->urlprefix );
    sprintf_to_char(ch, "{yFlags:{x          [%s]\n\r", area_flags(pArea));
    sprintf_to_char(ch, "{yAreaprog::{x      %s (%s)\n\r",
                    pArea->aprog?(pArea->aprog_enabled?"Enabled":"Disabled"):"none",
                    pArea->aprog?pArea->aprog->title:"N/A");
    sprintf_to_char(ch,"{yProgram:{x        %s (use {Wprogram{x to edit it)\n\r",
                    pArea->areaprogram[0]==0?"none":"available");
    if (pArea->areaprogram_edit)
        send_to_char("                {Rwarning: is currently being editted!{x\n\r",ch);

    if (pArea->properties) {
        prop=pArea->properties;
        sprintf_to_char(ch,"{yProperties:{x     %s / %s\n\r",prop->key,table_find_value(prop->type,property_table));
        for (prop=prop->next;prop;prop=prop->next) {
            sprintf_to_char(ch,"                %s / %s\n\r",prop->key,table_find_value(prop->type,property_table));
        }
    } else
        send_to_char("{yProperties:{c     None\n\r",ch);

    return FALSE;
}



bool aedit_reset( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;

    EDIT_AREA(ch, pArea);

    reset_area( pArea );
    send_to_char( "Area reset.\n\r", ch );
    logf("[%d] %s resetted '%s'",GET_DESCRIPTOR(ch),ch->name,pArea->name);

    return FALSE;
}



bool aedit_create( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;

    if ( area_allocated >= MAX_VNUMS )	/* OLC 1.1b */
    {
        send_to_char( "We're out of vnums for new areas.\n\r", ch );
        return FALSE;
    }

    if (ch->pcdata->security<9) {
        send_to_char( "You are not allowed to create new areas.\n\r", ch );
        return FALSE;
    }

    if (ch->desc->editor == ED_MOBPROG) {
        TCLPROG_CODE *pCode2;

        pCode2=(TCLPROG_CODE *)ch->desc->pEdit;
        pCode2->edit=FALSE;
    }

    pArea               =   new_area();
    area_last->next     =   pArea;
    area_last		=   pArea;	/* Thanks, Walker. */
    ch->desc->pEdit     =   (void *)pArea;

    SET_BIT( pArea->area_flags, AREA_ADDED );
    SET_BIT( pArea->area_flags, AREA_UNFINISHED );
    send_to_char( "Area Created.\n\r", ch );
    return TRUE;	/* OLC 1.1b */
}



bool aedit_name( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;

    EDIT_AREA(ch, pArea);

    if (ch->pcdata->security<9) {
        send_to_char( "You are not allowed to change the name.\n\r", ch );
        return FALSE;
    }

    if ( argument[0] == '\0' )
    {
        send_to_char( "Syntax:   name [$name]\n\r", ch );
        return FALSE;
    }

    free_string( pArea->name );
    pArea->name = str_dup( argument );

    send_to_char( "Name set.\n\r", ch );
    return TRUE;
}



bool aedit_creator( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;

    EDIT_AREA(ch, pArea);

    if (ch->pcdata->security<9) {
        send_to_char( "You are not allowed to change the creator.\n\r", ch );
        return FALSE;
    }

    if ( argument[0] == '\0' )
    {
        send_to_char( "Syntax: creator <name>\n\r", ch );
        return FALSE;
    }

    free_string( pArea->creator );
    pArea->creator = str_dup( argument );

    send_to_char( "Creator set.\n\r", ch );
    return TRUE;
}


bool aedit_comment(CHAR_DATA *ch,char *argument) {
    AREA_DATA *pArea;

    EDIT_AREA(ch, pArea);

    if (argument[0] == '\0') {
        editor_start( ch, &pArea->comment );
        return TRUE;
    }

    send_to_char( "Syntax: comment\n\r", ch );
    return FALSE;
}

bool aedit_description(CHAR_DATA *ch,char *argument) {
    AREA_DATA *pArea;

    EDIT_AREA(ch, pArea);

    if (argument[0] == '\0') {
        editor_start( ch, &pArea->description );
        return TRUE;
    }

    send_to_char( "Syntax: desc\n\r", ch );
    return FALSE;
}

bool aedit_program(CHAR_DATA *ch,char *argument) {
    AREA_DATA *pArea;

    EDIT_AREA(ch, pArea);

    if (argument[0] == '\0') {
        editor_start( ch, &pArea->areaprogram );
        pArea->areaprogram_edit=TRUE;
        return TRUE;
    }

    send_to_char( "Syntax: program\n\r", ch );
    return FALSE;
}

bool aedit_file( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;
    char file[MAX_STRING_LENGTH];
    char full_old_file[MAX_STRING_LENGTH];
    char full_new_file[MAX_STRING_LENGTH];
    int i, length;
    FILE *fp;

    EDIT_AREA(ch, pArea);

    if (ch->pcdata->security<9) {
        send_to_char( "You are not allowed to change the area name.\n\r", ch );
        return FALSE;
    }

    one_argument( argument, file );	/* Forces Lowercase */

    if ( argument[0] == '\0' )
    {
        send_to_char( "Syntax:  filename [$file]\n\r", ch );
        return FALSE;
    }

    /*
     * Simple Syntax Check.
     */
    length = strlen( argument );
    if ( length > 8 )
    {
        send_to_char( "No more than eight characters allowed.\n\r", ch );
        return FALSE;
    }

    /*
     * Allow only letters and numbers.
     */
    for ( i = 0; i < length; i++ )
    {
        if ( !isalnum( file[i] ) )
        {
            send_to_char( "Only letters and numbers are valid.\n\r", ch );
            return FALSE;
        }
    }

    strcat( file, ".are" );

    snprintf(full_old_file,MSL,"%s/%s",mud_data.area_dir,pArea->filename);
    snprintf(full_new_file,MSL,"%s/%s",mud_data.area_dir,file);

    if ((fp=fopen(full_new_file,"r"))) {
        fclose(fp);
        sprintf_to_char(ch,"Warning: This file already exists.\n\r"
                           "If you want to import a new area, move it into '%s' and "
                           "reload the mud.\n\r",mud_data.area_import);
        return TRUE;
    }
    if (link(full_old_file,full_new_file)==0)
        unlink(full_old_file);

    strcpy(full_old_file+strlen(full_old_file)-4,".roo");
    strcpy(full_new_file+strlen(full_new_file)-4,".roo");
    if (link(full_old_file,full_new_file)==0)
        unlink(full_old_file);

    free_string( pArea->filename );
    pArea->filename = str_dup( file );

    send_to_char( "Filename set.\n\r", ch );
    return TRUE;
}



bool aedit_age( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;
    char age[MAX_STRING_LENGTH];

    EDIT_AREA(ch, pArea);

    one_argument( argument, age );

    if ( !is_number( age ) || age[0] == '\0' )
    {
        send_to_char( "Syntax:  age [#age]\n\r", ch );
        return FALSE;
    }

    pArea->age = atoi( age );

    send_to_char( "Age set.\n\r", ch );
    return TRUE;
}



bool aedit_recall( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;
    char room[MAX_STRING_LENGTH];
    int  value;

    EDIT_AREA(ch, pArea);

    one_argument( argument, room );

    if ( !is_number( argument ) || argument[0] == '\0' )
    {
        send_to_char( "Syntax:  recall [#rvnum]\n\r", ch );
        return FALSE;
    }

    value = atoi( room );

    if ( !get_room_index( value ) )
    {
        send_to_char( "AEdit:  Room vnum does not exist.\n\r", ch );
        return FALSE;
    }

    pArea->recall = value;

    send_to_char( "Recall set.\n\r", ch );
    return TRUE;
}

bool aedit_login_room( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;
    char room[MAX_STRING_LENGTH];
    int  value;

    EDIT_AREA(ch, pArea);

    one_argument( argument, room );

    if ( !is_number( argument ) || argument[0] == '\0' )
    {
        send_to_char( "Syntax:  login-room [#rvnum]\n\r", ch );
        return FALSE;
    }

    value = atoi( room );

    if ( (value!=0) && !get_room_index( value ) )
    {
        send_to_char( "AEdit:  Room vnum does not exist.\n\r", ch );
        return FALSE;
    }

    pArea->login_room = value;

    send_to_char( "Login-room set.\n\r", ch );
    return TRUE;
}



bool aedit_security( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;
    char sec[MAX_STRING_LENGTH];
    char buf[MAX_STRING_LENGTH];
    int  value;

    EDIT_AREA(ch, pArea);

    one_argument( argument, sec );

    if ( !is_number( sec ) || sec[0] == '\0' )
    {
        send_to_char( "Syntax:  security [#level]\n\r", ch );
        return FALSE;
    }

    value = atoi( sec );

    if ( value > ch->pcdata->security || value < 0 )
    {
        if ( ch->pcdata->security != 0 )
        {
            sprintf( buf, "Security is 0-%d.\n\r", ch->pcdata->security );
            send_to_char( buf, ch );
        }
        else
            send_to_char( "Security is 0 only.\n\r", ch );
        return FALSE;
    }

    pArea->security = value;

    send_to_char( "Security set.\n\r", ch );
    return TRUE;
}


bool aedit_urlprefix(CHAR_DATA *ch,char *argument) {
    AREA_DATA *pArea;
    char s[MSL],*p;
    int i;

    EDIT_AREA(ch, pArea);
    free_string(pArea->urlprefix);

    p=argument;
    i=0;
    while (*p) {
        switch (*p) {
        case ' ': s[i++]='%';s[i++]='2';s[i++]='0';break;
        case '~': s[i++]='%';s[i++]='7';s[i++]='e';break;
        default : s[i++]=*p;
        }
        p++;
    }
    s[i]=0;
    pArea->urlprefix=str_dup(s);
    return TRUE;
}


bool aedit_builder( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;
    char name[MAX_STRING_LENGTH];
    char buf[MAX_STRING_LENGTH];

    EDIT_AREA(ch, pArea);

    one_argument( argument, name );

    if ( name[0] == '\0' )
    {
        send_to_char( "Syntax:  builder [$name]  -toggles builder\n\r", ch );
        send_to_char( "Syntax:  builder All      -allows everyone\n\r", ch );
        return FALSE;
    }

    name[0] = UPPER( name[0] );

    if ( strstr( pArea->builders, name ) != NULL)
    {
        pArea->builders = string_replace( pArea->builders, name, "\0" );
        pArea->builders = string_unpad( pArea->builders );

        if ( pArea->builders[0] == '\0' )
        {
            free_string( pArea->builders );
            pArea->builders = str_dup( "None" );
        }
        send_to_char( "Builder removed.\n\r", ch );
        return TRUE;
    }
    else
    {
        buf[0] = '\0';
        if ( strstr( pArea->builders, "None" ) != NULL)
        {
            pArea->builders = string_replace( pArea->builders, "None", "\0" );
            pArea->builders = string_unpad( pArea->builders );
        }

        if (pArea->builders[0] != '\0' )
        {
            strcat( buf, pArea->builders );
            strcat( buf, " " );
        }
        strcat( buf, name );
        free_string( pArea->builders );
        pArea->builders=str_dup(buf);

        send_to_char( "Builder added.\n\r", ch );
        return TRUE;
    }

    return FALSE;
}



bool aedit_level( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;
    char lower[MAX_STRING_LENGTH];
    char upper[MAX_STRING_LENGTH];
    int  ilower;
    int  iupper;

    EDIT_AREA(ch, pArea);

    argument = one_argument( argument, lower );
    one_argument( argument, upper );

    if ( !is_number( lower ) || lower[0] == '\0'
         || !is_number( upper ) || upper[0] == '\0' )
    {
        send_to_char( "Syntax: level <#lower> <#upper>\n\r", ch );
        return FALSE;
    }

    if ( ( ilower = atoi( lower ) ) > ( iupper = atoi( upper ) ) )
    {
        send_to_char( "AEdit:  Upper must be larger then lower.\n\r", ch );
        return FALSE;
    }

    pArea->llevel=ilower;
    pArea->ulevel=iupper;

    send_to_char( "Levels set.\n\r", ch );
    return TRUE;
}

bool aedit_vnum( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;
    char lower[MAX_STRING_LENGTH];
    char upper[MAX_STRING_LENGTH];
    int  ilower;
    int  iupper;

    EDIT_AREA(ch, pArea);

    argument = one_argument( argument, lower );
    one_argument( argument, upper );

    if ( !is_number( lower ) || lower[0] == '\0'
         || !is_number( upper ) || upper[0] == '\0' )
    {
        if (!str_cmp(lower,"area")) {
            vnum_area(ch,pArea->vnum);
            return TRUE;
        }
        send_to_char( "Syntax:  vnum [#lower] [#upper]\n\r", ch );
        return FALSE;
    }

    if ( ( ilower = atoi( lower ) ) > ( iupper = atoi( upper ) ) )
    {
        send_to_char( "AEdit:  Upper must be larger then lower.\n\r", ch );
        return FALSE;
    }

    /* OLC 1.1b */
    if ( ilower <= 0 || ilower >= MAX_VNUMS || iupper <= 0 || iupper >= MAX_VNUMS )
    {
        char output[MAX_STRING_LENGTH];

        sprintf( output, "AEdit: vnum must be between 0 and %d.\n\r", MAX_VNUMS );
        send_to_char( output, ch );
        return FALSE;
    }

    if ( !check_range( ilower, iupper ) )
    {
        send_to_char( "AEdit:  Range must include only this area.\n\r", ch );
        return FALSE;
    }

    if ( get_vnum_area( ilower )
         && get_vnum_area( ilower ) != pArea )
    {
        send_to_char( "AEdit:  Lower vnum already assigned.\n\r", ch );
        return FALSE;
    }

    pArea->lvnum = ilower;
    send_to_char( "Lower vnum set.\n\r", ch );

    if ( get_vnum_area( iupper )
         && get_vnum_area( iupper ) != pArea )
    {
        send_to_char( "AEdit:  Upper vnum already assigned.\n\r", ch );
        return TRUE;	/* The lower value has been set. */
    }

    pArea->uvnum = iupper;
    send_to_char( "Upper vnum set.\n\r", ch );

    return TRUE;
}



bool aedit_lvnum( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;
    char lower[MAX_STRING_LENGTH];
    int  ilower;
    int  iupper;

    EDIT_AREA(ch, pArea);

    one_argument( argument, lower );

    if ( !is_number( lower ) || lower[0] == '\0' )
    {
        send_to_char( "Syntax:  lvnum [#lower]\n\r", ch );
        return FALSE;
    }

    if ( ( ilower = atoi( lower ) ) > ( iupper = pArea->uvnum ) )
    {
        send_to_char( "AEdit:  Value must be less than the uvnum.\n\r", ch );
        return FALSE;
    }

    /* OLC 1.1b */
    if ( ilower <= 0 || ilower >= MAX_VNUMS || iupper <= 0 || iupper >= MAX_VNUMS )
    {
        char output[MAX_STRING_LENGTH];

        sprintf( output, "AEdit: vnum must be between 0 and %d.\n\r", MAX_VNUMS );
        send_to_char( output, ch );
        return FALSE;
    }

    if ( !check_range( ilower, iupper ) )
    {
        send_to_char( "AEdit:  Range must include only this area.\n\r", ch );
        return FALSE;
    }

    if ( get_vnum_area( ilower )
         && get_vnum_area( ilower ) != pArea )
    {
        send_to_char( "AEdit:  Lower vnum already assigned.\n\r", ch );
        return FALSE;
    }

    pArea->lvnum = ilower;
    send_to_char( "Lower vnum set.\n\r", ch );
    return TRUE;
}



bool aedit_uvnum( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;
    char upper[MAX_STRING_LENGTH];
    int  ilower;
    int  iupper;

    EDIT_AREA(ch, pArea);

    one_argument( argument, upper );

    if ( !is_number( upper ) || upper[0] == '\0' )
    {
        send_to_char( "Syntax:  uvnum [#upper]\n\r", ch );
        return FALSE;
    }

    if ( ( ilower = pArea->lvnum ) > ( iupper = atoi( upper ) ) )
    {
        send_to_char( "AEdit:  Upper must be larger then lower.\n\r", ch );
        return FALSE;
    }

    /* OLC 1.1b */
    if ( ilower <= 0 || ilower >= MAX_VNUMS || iupper <= 0 || iupper >= MAX_VNUMS )
    {
        char output[MAX_STRING_LENGTH];

        sprintf( output, "AEdit: vnum must be between 0 and %d.\n\r", MAX_VNUMS );
        send_to_char( output, ch );
        return FALSE;
    }

    if ( !check_range( ilower, iupper ) )
    {
        send_to_char( "AEdit:  Range must include only this area.\n\r", ch );
        return FALSE;
    }

    if ( get_vnum_area( iupper )
         && get_vnum_area( iupper ) != pArea )
    {
        send_to_char( "AEdit:  Upper vnum already assigned.\n\r", ch );
        return FALSE;
    }

    pArea->uvnum = iupper;
    send_to_char( "Upper vnum set.\n\r", ch );

    return TRUE;
}

bool aedit_objlevel( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;
    OBJ_INDEX_DATA *pObjIndex;
    char arg1[MAX_STRING_LENGTH];
    int levelvec,vnum;

    EDIT_AREA(ch, pArea);

    argument = one_argument( argument, arg1 );

    if (!str_cmp("add",arg1) && isdigit(argument[0])) {
        levelvec=atoi(argument);
    }
    else if (!str_cmp("del",arg1) && isdigit(argument[0]))
    {
        levelvec=-1*atoi(argument);
    }
    else
    {
        send_to_char("Syntax: objlevel add|del <number>\n\r", ch );
        return FALSE;
    }

    /* Change level of all object from this area */
    for( vnum = pArea->lvnum; vnum <= pArea->uvnum; vnum++ )
    {
        if ( ( pObjIndex = get_obj_index(vnum) ) )
        {
            if ( pObjIndex->area == pArea && pObjIndex->level)
            {
                if (levelvec>0) pObjIndex->level+=levelvec;
                else if (levelvec<0)
                {
                    if (pObjIndex->level+levelvec<1) pObjIndex->level=1;
                    else pObjIndex->level+=levelvec;
                }
            }
        }
    }

    sprintf( arg1, "All object levels changed: %d levels %s.\n\r",
             levelvec>0?levelvec:-levelvec,
             levelvec<0?"decreased":"increased" );
    send_to_char( arg1, ch );

    return TRUE;
}

bool aedit_remove( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;

    EDIT_AREA(ch, pArea);

    if (argument[0]==0) {
        if (ch->pcdata->security<9) {
            send_to_char( "You are not allowed to delete areas.\n\r", ch );
            return FALSE;
        }

        TOGGLE_BIT(pArea->area_flags,AREA_DELETE);
        send_to_char( "Area delete flag toggled\n\r" ,ch);
    } else {
        char arg1[MAX_STRING_LENGTH];
        char slow[MAX_STRING_LENGTH],shigh[MAX_STRING_LENGTH];
        int  low,high,i;
        OBJ_INDEX_DATA *oid;
        MOB_INDEX_DATA *mid;
        ROOM_INDEX_DATA *rid;

        argument=one_argument(argument,arg1);
        argument=one_argument(argument,slow);
        argument=one_argument(argument,shigh);

        if ((!is_number(slow)) || (!is_number(shigh)) ||
                slow[0]==0 || shigh[0]==0 ) {
            send_to_char("Syntax: remove <obj|mob|room|mprog> <low> <high>\n\r",
                         ch);
            return TRUE;
        }
        low=atoi(slow);
        high=atoi(shigh);

        if (low<pArea->lvnum || high>pArea->uvnum || low>high ) {
            send_to_char("<low> should be lower than <high>. <low> should"
                         "be greater than the lowest vnum of this area. <high>"
                         "should be lower than the highest vnum of this area."
                         "Difficult, isn't it\n\r",ch);
            return TRUE;
        }

        switch (which_keyword(arg1,"mob","room","obj",NULL)) {
        case 0:
        case -1:
            send_to_char(
                        "Syntax: remove <obj|mob|room|mprog> <low> <high>\n\r",ch);
            break;

        case 1:
            for (i=low;i<=high;i++) {
                if ((mid=get_mob_index(i))) {
                    sprintf_to_char(ch,"Deleted %d: %s{x\n\r",
                                    i,mid->player_name);
                    mid->deleted=TRUE;
                }
            }
            sprintf_to_char(ch,"Deleted mobs from %d to %d.\n\r",
                            low,high);
            break;

        case 2:
            for (i=low;i<=high;i++) {
                if ((rid=get_room_index(i))) {
                    sprintf_to_char(ch,"Deleted %d: %s{x\n\r",
                                    i,rid->name);
                    rid->deleted=TRUE;
                }
            }
            sprintf_to_char(ch,"Deleted rooms from %d to %d.\n\r",
                            low,high);
            break;

        case 3:
            for (i=low;i<=high;i++) {
                if ((oid=get_obj_index(i))) {
                    sprintf_to_char(ch,"Deleted %d: %s{x\n\r",
                                    i,oid->name);
                    oid->deleted=TRUE;
                }
            }
            sprintf_to_char(ch,"Deleted objects from %d to %d.\n\r",
                            low,high);
            break;


        case 4:
            send_to_char("Not yet implemented, sorry!\n\r",ch);
            break;

        }

    }

    return TRUE;
}

bool aedit_unfinished( CHAR_DATA *ch, char *argument )
{
    AREA_DATA *pArea;

    EDIT_AREA(ch, pArea);

    if (ch->pcdata->security<9) {
        send_to_char("You are not allowed to toggle the unfinished flag.\n\r",ch);
        return FALSE;
    }

    TOGGLE_BIT(pArea->area_flags,AREA_UNFINISHED);
    send_to_char( "Area unfinished flag toggled\n\r" ,ch);

    return TRUE;
}

bool aedit_purge( CHAR_DATA *ch, char *argument ) {
    AREA_DATA *pArea;
    OBJ_DATA *obj,*obj_next;
    CHAR_DATA *vch,*vch_next;
    ROOM_INDEX_DATA *room;
    int i;

    if (str_cmp(argument,"area")) {
        send_to_char("If you REALLY want to purge the ENTIRE area, use 'purge area'.\n\r",ch);
        return FALSE;
    }

    EDIT_AREA(ch,pArea);

    for (i=pArea->lvnum;i<=pArea->uvnum;i++) {
        if ((room=get_room_index(i))!=NULL) {

            vch=room->people;
            while (vch!=NULL) {
                vch_next=vch->next_in_room;

                if (IS_NPC(vch))
                    extract_char(vch,TRUE);
                vch=vch_next;
            }

            obj=room->contents;
            while (obj!=NULL) {
                obj_next=obj->next_content;

                extract_obj(obj);
                obj=obj_next;
            }

        }
    }

    send_to_char("Area purged\n\r",ch);
    logf("[%d] %s purged '%s'",GET_DESCRIPTOR(ch),ch->name,pArea->name);
    return TRUE;
}

bool aedit_proginstance( CHAR_DATA *ch,char *argument) {
    send_to_char( "Please use aprog to enable and disable the areaprogram.\n\r", ch);
    return FALSE;
}

bool aedit_aprog( CHAR_DATA *ch,char *argument) {
    AREA_DATA *pArea;

    EDIT_AREA(ch, pArea);

    if (argument[0]) {
        if (!str_prefix(argument,"enable")) {
            pArea->aprog_enabled=TRUE;
            send_to_char("Aprog enabled",ch);
            return TRUE;
        }
        if (!str_prefix(argument,"disable")) {
            pArea->aprog_enabled=FALSE;
            send_to_char("Aprog diabled",ch);
            return TRUE;
        }
    }

    send_to_char("Syntax: aprog <enable|disable>\n\r",ch);
    sprintf_to_char(ch,"The areaprog is currently %s.\n\r",pArea->aprog_enabled?"enabled":"disabled");
    return FALSE;
}

bool aedit_property(CHAR_DATA *ch, char *argument) {
    AREA_DATA *pArea;
    char op[MAX_STRING_LENGTH];
    char stype[MAX_STRING_LENGTH];
    char key[MAX_STRING_LENGTH];

    int action,type;
    PROPERTY_INDEX_TYPE *pProp;


    EDIT_AREA(ch, pArea);

    argument=one_argument(argument,op);
    argument=one_argument(argument,key);
    argument=one_argument(argument,stype);

    action=which_keyword(op,"add","delete",NULL);
    type=table_find_name(stype,property_table);

    if (action<1 || action>2) {
        send_to_char("Usage: property add|delete <key> <type>\r\n",ch);
        return FALSE;
    }
    if (type==NO_FLAG) {
        sprintf_to_char(ch,"%s is not a valid property type.\r\n",stype);
        return FALSE;
    }

    switch (action) {
    case 1: // add
        if (does_property_exist_in_list(pArea->properties,key,type)) {
            send_to_char("Property already defined for this area\r\n",ch);
            return FALSE;
        }
        pProp=new_property_index();
        pProp->type=type;
        pProp->key=str_dup(key);
        pProp->next=pArea->properties;
        pArea->properties=pProp;
        if (!does_property_exist(key,type)) {
            pProp=new_property_index();
            pProp->type=type;
            pProp->key=str_dup(key);
            add_property_index(pProp);
        }

        break;
    case 2: // delete
        if (!does_property_exist_in_list(pArea->properties,key,type)) {
            send_to_char("Property not defined for this area\r\n",ch);
            return FALSE;
        }
        pProp=get_property_index_from_list(pArea->properties,key,type);
        remove_property_index_from_list(&pArea->properties,pProp);
        free_property_index(pProp);

        break;
    default:
        send_to_char("You shouldn't see this.",ch);
    }

    return TRUE;
}
