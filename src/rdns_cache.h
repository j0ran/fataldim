//
// cached reverse lookup
//
// for version 3.3 (release 4)
//
// $Id: rdns_cache.h,v 1.5 2008/05/11 20:50:47 jodocus Exp $
#ifndef RDNS_CACHE
#define RDNS_CACHE

void	rdns_cache_destroy(void);
int	rdns_cache_init(void);
void	rdns_cache_set_ttl(int new_ttl);

char * gethostname_cached(const char *addr, int len, int ttl_refresh);

#endif
