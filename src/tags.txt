Tags

fataldim-1	Stable version of Fatal Dimensions
fataldim-2	IMC2 has been added and is working correctly
fataldim-3	Before new tcl mobprogs.
fataldim-4	Before stringflags
fataldim-5	Before function based skills and groups

Branches

mobprog-tcl-1	The starting point for implementing tcl mob programs
