/* $Id: quest.c,v 1.79 2008/02/12 19:42:07 jodocus Exp $ */
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*       ROM 2.4 is copyright 1993-1995 Russ Taylor                         *
*       ROM has been brought to you by the ROM consortium                  *
*           Russ Taylor (rtaylor@pacinfo.com)                              *
*           Gabrielle Taylor (gtaylor@pacinfo.com)                         *
*           Brian Moore (rom@rom.efn.org)                                  *
*       By using this code, you have agreed to follow the terms of the     *
*       ROM license, in the file Rom24/doc/rom.license                     *
***************************************************************************/

/***************************************************************************
*  Automated Quest code written by Vassago of MOONGATE, moongate.ams.com   *
*  4000. Copyright (c) 1996 Ryan Addams, All Rights Reserved. Use of this  *
*  code is allowed provided you add a credit line to the effect of:        *
*  "Quest Code (c) 1996 Ryan Addams" to your logon screen with the rest    *
*  of the standard diku/rom credits. If you use this or a modified version *
*  of this code, let me know via email: moongate@moongate.ams.com. Further *
*  updates will be posted to the rom mailing list. If you'd like to get    *
*  the latest version of quest.c, please send a request to the above add-  *
*  ress. Quest Code v2.00.                                                 *
***************************************************************************/

#include "merc.h"
#include "interp.h"
#include "color.h"
#include "db.h"

/***************************************************************************
* rules:
* - if bit PLR_QUESTOR not set, the player isn't involved in any questing
*   things.
* - if quest_giver is NULL, the player is in 'waiting' mode.
*   time left for the next quest is in countdown-property
* - if quest_giver is !NULL, player is on a quest and in 'questing' mode.
*   object to retrieve is in quest_obj-property
*   mob to kill is in quest_mob-property (or -1 if already killed)
*
***************************************************************************/

/* Local functions */

bool generate_quest	( CHAR_DATA *ch, CHAR_DATA *questman,int *,int *);
void quest_update	( void );


void do_questinfo(CHAR_DATA *ch, char *argument) {
//    do_function(ch,&do_kwest,"info");
    quest(ch,QUEST_INFO);
}

/* The main quest function */
//void do_kwest(CHAR_DATA *ch, char *argument)
void quest(CHAR_DATA *ch, int quest_action)
{
    CHAR_DATA *questman;
    OBJ_DATA *obj=NULL, *obj_next;
    OBJ_INDEX_DATA *questinfoobj;
    MOB_INDEX_DATA *questinfo;
    char buf [MAX_STRING_LENGTH];
    int quest_mob,quest_countdown,quest_obj;
    long quest_giver;

    if (IS_NPC(ch)) {
	send_to_char("Go away, you NPC!\n\r",ch);
	return;
    }

    quest_mob=quest_giver=quest_countdown=quest_obj=0;
    GetCharProperty(ch,PROPERTY_INT,"quest_mob",&quest_mob);
    GetCharProperty(ch,PROPERTY_INT,"quest_obj",&quest_obj);
    GetCharProperty(ch,PROPERTY_LONG,"quest_giver",&quest_giver);
    GetCharProperty(ch,PROPERTY_INT,"quest_countdown",&quest_countdown);

//  sprintf_to_char(ch,"mob: %d   obj: %d    giver: %ld   countdown: %d\n\r",
//	quest_mob,quest_obj,quest_giver,quest_countdown);

    if (quest_action==QUEST_INFO) {
	if (!STR_IS_SET(ch->strbit_act,PLR_QUESTOR)) {
	    send_to_char("You aren't currently on a quest.\n\r",ch);
	    return;
	}

	/* not on a quest, in waiting mode */
	if (quest_giver==0) {
	    if (quest_countdown > 1) {
		sprintf_to_char(ch, "There are %d minutes remaining until "
		    "you can go on another quest.\n\r",quest_countdown);
	    } else {
		send_to_char("There is less than a minute remaining"
		    " until you can go on another quest.\n\r",ch);
	    }
	    return;
	}

	/* item retrieval quest */
	if (quest_obj>0) {
	    char buf[MSL];
	    char *short_descr;
	    questinfoobj=get_obj_index(quest_obj);
	    if (!GetCharProperty(ch,PROPERTY_STRING,"quest_desc",buf)) {
		short_descr=questinfoobj?questinfoobj->short_descr:NULL;
	    } else {
		short_descr=buf;
	    }
	    if (questinfoobj != NULL) {
		sprintf_to_char(ch,
		    "You are on a quest to get %s!\n\r",
		    short_descr);
		sprintf_to_char(ch,"Time left for this task is %d"
		    " minutes.\n\r",quest_countdown);
	    } else {
		bugf("Player %s is on questor-mode, but "
		    "object doesn't exist. Vnum is: %d",ch->name,quest_obj);
		send_to_char("You aren't currently on a quest."
		    " (error reported)\n\r",ch);
	    }
	    return;
	}

	/* mob killing - completed */
	if (quest_mob==0) {
	    sprintf_to_char(ch, "Your quest is ALMOST complete!\n\r"
		"You have %d minutes to get back to your questmaster before"
		" time runs out!\n\r",
		quest_countdown);
	    return;
	}

	/* mob killing - busy */
	if (quest_mob > 0) {
	    questinfo = get_mob_index(quest_mob);
	    if (questinfo != NULL) {
		sprintf_to_char(ch,
		    "You are on a quest to slay the dreaded %s!\n\r",
		    questinfo->short_descr);
		sprintf_to_char(ch,"Time left for this task is %d"
		    " minutes.\n\r",quest_countdown);
	    } else {
		bugf("Player %s is on questor-mode, but "
		    "mob doesn't exist. Vnum is: %d",ch->name,quest_mob);
		send_to_char("You aren't currently on a quest."
		    " (error reported)\n\r",ch);
	    }
	    return;
	}

	return;
    }

/* Checks for a character in the room with spec_questmaster set. This special
   procedure must be defined in special.c. You could instead use an
   ACT_QUESTMASTER flag instead of a special procedure. */

    for (questman = ch->in_room->people; questman != NULL;
	 questman = questman->next_in_room ) {
	if (!IS_NPC(questman)) continue;
        if (questman->spec_fun == spec_lookup( "spec_questmaster" ))
	    break;
    }

    if (questman == NULL ||
	questman->spec_fun != spec_lookup( "spec_questmaster" )) {
        send_to_char("You can't do that here.\n\r",ch);
        return;
    }

    if ( questman->fighting != NULL) {
	send_to_char("Wait until the fighting stops.\n\r",ch);
        return;
    }

    if (quest_action==QUEST_REJECT) {
	if (questman->pIndexData->vnum!=quest_giver) {
	    sprintf(buf, "I never sent you on a quest! "
		"Perhaps you're thinking of someone else.");
	    do_function(questman,&do_say,buf);
	    return;
	}

	if (!STR_IS_SET(ch->strbit_act, PLR_QUESTOR)) {
	    sprintf(buf, "But you're not on a quest!");
	    do_function(questman,&do_say,buf);
	    return;
	}

//	REMOVE_BIT(ch->strbit_act,PLR_QUESTOR);
	DeleteCharProperty(ch,PROPERTY_LONG,"quest_giver");
	DeleteCharProperty(ch,PROPERTY_INT,"quest_obj");
	DeleteCharProperty(ch,PROPERTY_INT,"quest_mob");
	DeleteCharProperty(ch,PROPERTY_STRING,"quest_desc");
	quest_countdown=30;
	SetCharProperty(ch,PROPERTY_INT,"quest_countdown",
	    &quest_countdown);

        act( "$n rejects $s quest.", ch, NULL, questman, TO_ROOM);
	act ("You reject your quest.",ch, NULL, questman, TO_CHAR);
	do_function(questman,&do_say,"A little too difficult for you? "
	    "Oh well, perhaps somebody else will help.");
	return;
    }

    if (quest_action==QUEST_REQUEST) {
	//act ("You ask $N for a quest.",ch, NULL, questman, TO_CHAR);
        act( "$n asks $N for a quest.", ch, NULL, questman, TO_ROOM);

	if (STR_IS_SET(ch->strbit_act, PLR_QUESTOR) && quest_giver) {
	    do_function(questman,&do_say,"But you're already on a quest!");
	    return;
	}
	if (quest_giver==0 && quest_countdown> 0) {
	    sprintf(buf,
		"You're very brave, %s, but let someone else have a chance.",
		ch->name);
	    do_function(questman,&do_say,buf);
	    do_function(questman,&do_say,"Come back later.");
	    return;
	}

	sprintf(buf, "Thank you, brave %s!",ch->name);
	do_function(questman,&do_say,buf);

	if (!generate_quest(ch,questman,&quest_mob,&quest_obj))
	    return;

        if (quest_mob > 0 || quest_obj > 0) {
            quest_countdown = number_range(15,10+ch->level/5);
	    quest_giver=questman->pIndexData->vnum;
	    STR_SET_BIT(ch->strbit_act, PLR_QUESTOR);
	    SetCharProperty(ch,PROPERTY_INT,"quest_countdown",&quest_countdown);
	    SetCharProperty(ch,PROPERTY_INT,"quest_obj",&quest_obj);
	    SetCharProperty(ch,PROPERTY_INT,"quest_mob",&quest_mob);
	    SetCharProperty(ch,PROPERTY_LONG,"quest_giver",&quest_giver);

	    sprintf(buf, "You have %d minutes to complete this quest.",
		quest_countdown);
	    do_function(questman,&do_say,buf);
	    do_function(questman,&do_say,"May the gods go with you!");
	}
	return;
    }

    if (quest_action==QUEST_COMPLETE) {

	//act("You inform $N you have completed your quest.",ch,NULL,questman,TO_CHAR);
        act("$n informs $N $e has completed $s quest.",ch,NULL,questman,TO_ROOM);

	if (quest_giver != questman->pIndexData->vnum) {
	    do_function(questman,&do_say,
		"I never sent you on a quest! Perhaps you're"
		" thinking of someone else.");
	    return;
	}

	if (STR_IS_SET(ch->strbit_act, PLR_QUESTOR)) {
	    if (quest_mob == 0 && quest_obj==0 ) {
		int pointreward, pracreward;

		pointreward = ch->level/10+number_range(1,5);

		do_function(questman,&do_say,
		    "Congratulations on completing your quest!");
		sprintf(buf,"As a reward, I am giving you %d quest point%s.",
		    pointreward,pointreward==1?"":"s");
		do_function(questman,&do_say,buf);
		if (number_percent()<15) {
		    pracreward = number_range(1,2);
		    sprintf(buf,"You also get %d practice%s sessions!",
			pracreward,pracreward==1?"":"s");
		    do_function(questman,&do_say,buf);
		    ch->practice += pracreward;
		    wiznet(WIZ_QUESTS,0,ch,NULL,
			"QUEST: $N got %d questpoints and %d practices.",
			pointreward,pracreward);
		} else
		    wiznet(WIZ_QUESTS,0,ch,NULL,
			"QUEST: $N got %d questpoints.",pointreward);

		DeleteCharProperty(ch,PROPERTY_LONG,"quest_giver");
		DeleteCharProperty(ch,PROPERTY_INT,"quest_obj");
		DeleteCharProperty(ch,PROPERTY_INT,"quest_mob");
		DeleteCharProperty(ch,PROPERTY_STRING,"quest_desc");
		quest_countdown=30;
		SetCharProperty(ch,PROPERTY_INT,"quest_countdown",
		    &quest_countdown);
		ch->pcdata->questpoints += pointreward;

	        return;
	    }
	    if (quest_obj != 0 ) {
		bool obj_found = FALSE;

    		for (obj = ch->carrying; obj != NULL; obj= obj_next) {
        	    obj_next = obj->next_content;

		    if (obj!=NULL &&
			obj->wear_loc==WEAR_NONE &&
			obj->pIndexData->vnum==quest_obj){
			obj_found = TRUE;
            	        break;
		    }
        	}
		if (obj_found == TRUE) {
		    int pointreward, pracreward;

		    pointreward = ch->level/10+number_range(1,5);

		    act("You hand $p to $N.",ch, obj, questman, TO_CHAR);
		    act("$n hands $p to $N.",ch, obj, questman, TO_ROOM);

		    do_function(questman,&do_say,
			"Congratulations on completing your quest!");
		    sprintf(buf,"As a reward, I am giving you %d quest point%s.",
			pointreward,pointreward==1?"":"s");
		    do_function(questman,&do_say,buf);
		    if (number_percent()<15) {
		        pracreward = number_range(1,2);
			sprintf(buf,"You also get %d practice session%s!",
			    pracreward,pracreward==1?"":"s");
			do_function(questman,&do_say,buf);
		        ch->practice += pracreward;
			wiznet(WIZ_QUESTS,0,ch,NULL,
			    "QUEST: $N got %d questpoints and %d practices.",
			    pointreward,pracreward);
		    } else
			wiznet(WIZ_QUESTS,0,ch,NULL,
			    "QUEST: $N got %d questpoints.",pointreward);

		    DeleteCharProperty(ch,PROPERTY_LONG,"quest_giver");
		    DeleteCharProperty(ch,PROPERTY_INT,"quest_obj");
		    DeleteCharProperty(ch,PROPERTY_INT,"quest_mob");
		    DeleteCharProperty(ch,PROPERTY_STRING,"quest_desc");
		    quest_countdown=30;
		    SetCharProperty(ch,PROPERTY_INT,
			"quest_countdown",&quest_countdown);
		    ch->pcdata->questpoints += pointreward;
		    extract_obj(obj);
		    return;
		}
		do_function(questman,&do_say,
		    "You haven't completed the quest yet,"
		    " but there is still time!");
		return;
	    }
	    if ((quest_mob>0 || quest_obj>0) && quest_countdown>0) {
		do_function(questman,&do_say,
		    "You haven't completed the quest yet,"
		    " but there is still time!");
		return;
	    }
	}
	if (quest_countdown<=0)
	    do_function(questman,&do_say,
		"But you didn't complete your quest in time!");
	else {
	    sprintf(buf, "You have to REQUEST a quest first, %s.",ch->name);
	    do_function(questman,&do_say,buf);
	}
	return;
    }

    send_to_char("QUEST commands {WINFO REQUEST COMPLETE REJECT{x.\n\r",ch);
    send_to_char("For more information, type {WHELP QUEST{x.\n\r",ch);
    return;
}


CHAR_DATA *FindEvilMobToKill(CHAR_DATA *ch,CHAR_DATA *questman) {
    long mcounter;
    MOB_INDEX_DATA *vsearch;
    CHAR_DATA *victim;
    int mob_vnum;
    int level_diff;
    int quest_countdown;
    bool localonly=FALSE;

    GetCharProperty(questman,PROPERTY_BOOL,"quest_localonly",&localonly);

    for (mcounter = 0; mcounter < 99999; mcounter ++) {
	mob_vnum = number_range(50, MAX_VNUMS);

	if ( (vsearch = get_mob_index(mob_vnum) ) != NULL ) {
	    level_diff = vsearch->level - ch->level;

	    if ((level_diff < 10 && level_diff > -5)
		&& !STR_IS_SET(vsearch->strbit_act,ACT_NOQUEST)
		&& IS_EVIL(vsearch)
		&& (!localonly || vsearch->area==questman->zone)
		&& vsearch->pShop == NULL
		&& !STR_IS_SET(vsearch->strbit_imm_flags, IMM_SUMMON)
    		&& !STR_IS_SET(vsearch->strbit_act,ACT_TRAIN)
    		&& !STR_IS_SET(vsearch->strbit_act,ACT_PRACTICE)
    		&& !STR_IS_SET(vsearch->strbit_act,ACT_IS_HEALER)
    		&& !STR_IS_SET(vsearch->strbit_act,ACT_IS_HEALER)
		&& !IS_SET(vsearch->area->area_flags,AREA_UNFINISHED)
		&& number_percent()<35) break;
		else vsearch = NULL;
	}
    }

    if ( vsearch == NULL ||
	( victim = get_char_world( ch, vsearch->player_name ) ) == NULL ) {
	do_function(questman,&do_say, "I'm sorry, but I don't have any quests for you at this time.");
	do_function(questman,&do_say,"Try again later.");
	quest_countdown=5;
	STR_SET_BIT(ch->strbit_act,PLR_QUESTOR);
	SetCharProperty(ch,PROPERTY_INT,"quest_countdown",&quest_countdown);
        return NULL;
    }

    return victim;
}


OBJ_DATA *FindObjectToFetch(CHAR_DATA *ch,CHAR_DATA *questman) {
    OBJ_DATA *obj;
    OBJ_INDEX_DATA *iobj;
    ROOM_DATA *room=NULL;
    CHAR_DATA *carrier;
    int obj_num, mcounter;
    bool localonly=FALSE;

    GetCharProperty(questman,PROPERTY_BOOL,"quest_localonly",&localonly);

    for (mcounter = 999; mcounter > 0; mcounter --) {
	obj_num = number_range(0, obj_inuse-1);

	for (obj=object_list;obj!=NULL;obj=obj->next)
	    if (obj_num--==0) break;
        if (obj==NULL)
	    continue;

	iobj=obj->pIndexData;

	if ((ch->level<iobj->area->llevel || ch->level>iobj->area->ulevel) ||
	    IS_SET(iobj->area->area_flags,AREA_UNFINISHED) ||
	    !IS_SET(iobj->wear_flags,ITEM_TAKE))
	    continue;

        if (STR_IS_SET(iobj->strbit_extra_flags,ITEM_NOQUEST))
	    continue;

        if (localonly && iobj->area!=questman->zone)
	    continue;

        if (iobj->count>100)
	    continue;

        carrier=NULL;
	if (obj->carried_by!=NULL) {
	    carrier=obj->carried_by;
	} else if (obj->in_obj!=NULL && obj->in_obj->carried_by!=NULL) {
	    carrier=obj->in_obj->carried_by;
	}

	if (carrier!=NULL &&
	   (IS_PC(carrier) ||
	    (carrier->level-10>ch->level) ||
	    (carrier->level+10<ch->level) ||
	    (carrier->pIndexData->pShop!=NULL) ||
	    IS_AFFECTED2(carrier, EFF_IMPINVISIBLE)))
	    continue;

	if (obj->in_room!=NULL) room=obj->in_room;
	else if ((obj->carried_by) && (obj->carried_by->in_room)) room=obj->carried_by->in_room;
	else if (obj->in_obj!=NULL) {
	    if (obj->in_obj->in_room!=NULL) room=obj->in_obj->in_room;
	    else if ((obj->in_obj->carried_by!=NULL) && (obj->in_obj->carried_by->in_room!=NULL)) 
		     room=obj->in_obj->carried_by->in_room;
	}

        if (room==NULL)
	    continue;

        // no ROOM_DEATHTRAP,... flag
	if (STR_IS_SET(room->strbit_room_flags,ROOM_DEATHTRAP) ||
	    STR_IS_SET(room->strbit_room_flags,ROOM_NOQUEST))
	    continue;

	// not decay
	if (obj->timer>0)
	    continue;

	// not of clanhall-area/immortal area
	if (((iobj->vnum>=VNUM_IMMORTZONE_LOWER) && (iobj->vnum<=VNUM_IMMORTZONE_UPPER)) ||
	    ((iobj->vnum>=VNUM_IMMORTAREA_LOWER) && (iobj->vnum<=VNUM_IMMORTAREA_UPPER)) ||
	    ((iobj->vnum>=VNUM_LIMBO_LOWER)      && (iobj->vnum<=VNUM_LIMBO_UPPER)) ||
	    ((iobj->vnum>=VNUM_CLANAREA_LOWER)   && (iobj->vnum<=VNUM_CLANAREA_UPPER)) ||
	    ((iobj->vnum>=VNUM_TAGGING_LOWER)    && (iobj->vnum<=VNUM_TAGGING_UPPER)))
	    continue;
	if (((room->vnum>=VNUM_IMMORTZONE_LOWER) && (room->vnum<=VNUM_IMMORTZONE_UPPER)) ||
	    ((room->vnum>=VNUM_IMMORTAREA_LOWER) && (room->vnum<=VNUM_IMMORTAREA_UPPER)) ||
	    ((room->vnum>=VNUM_LIMBO_LOWER)      && (room->vnum<=VNUM_LIMBO_UPPER)) ||
	    ((room->vnum>=VNUM_CLANAREA_LOWER)   && (room->vnum<=VNUM_CLANAREA_UPPER)) ||
	    ((room->vnum>=VNUM_TAGGING_LOWER)    && (room->vnum<=VNUM_TAGGING_UPPER)))
	    continue;

        if (iobj->item_type==ITEM_MONEY)
	    continue;

	if (obj_is_owned(obj))
	    continue;

	break;
    }

    if (mcounter==0) {
	wiznet(WIZ_QUESTS,0,ch,NULL,
	    "QUEST: couldn't find a good object fast enough. "
	    "-> no quest for $N");
	return NULL;
    }

    return obj;
}




bool generate_quest(CHAR_DATA *ch, CHAR_DATA *questman,int *quest_mob,int *quest_obj) {
    char buf[MAX_STRING_LENGTH];
    int chance;


    /*  30% chance it will send the player on a 'recover item' quest.
	30% chance it will send the player on a 'get this item for me' quest.
	40% chance it will send the player on a 'mob kill' quest
    */

    chance=number_percent();

    if (chance<30) {					// recover item
	int objvnum = 0;
	CHAR_DATA *vsearch;
	ROOM_INDEX_DATA *room;
	OBJ_DATA *questitem;
	int objects[]={80, 81, 82, 83, 84, 85, 86, 87, 88, 89 };

	if ((vsearch=FindEvilMobToKill(ch,questman))==NULL)
	    return FALSE;

	objvnum=objects[number_range(0,(sizeof(objects)/sizeof(objects[0]))-1)];

        questitem = create_object( get_obj_index(objvnum), ch->level );
	questitem->timer=60;
	room=vsearch->in_room;
	obj_to_room(questitem, room);
	op_load_trigger(questitem,NULL);
	*quest_obj = questitem->pIndexData->vnum;

	SetCharProperty(ch,PROPERTY_STRING,"quest_desc",questitem->short_descr);

	sprintf(buf, "Vile pilferers have stolen %s from Fatal's treasury!",
	    questitem->short_descr);
	do_function(questman,&do_say, buf);
	do_function(questman,&do_say,"My court wizardess, with her magic "
	    "mirror, has pinpointed its location.");

	/* I changed my area names so that they have just the name of the area
	   and none of the level stuff. You may want to comment these next two
	   lines. - Vassago */

	sprintf(buf, "Look in the general area of %s's %s for %s!",
	    room->area->creator, room->area->name, room->name);
	do_function(questman,&do_say,buf);
	return TRUE;
    }

    else if (chance<60) {			// Quest to get an object
	OBJ_INDEX_DATA *objid;
	OBJ_DATA *obj;

	if ((obj=FindObjectToFetch(ch,questman))==NULL) {
	    int quest_countdown;

	    do_function(questman,&do_say, "I'm sorry, but I don't have any quests for you at this time.");
	    do_function(questman,&do_say,"Try again later.");
	    quest_countdown=5;
	    STR_SET_BIT(ch->strbit_act,PLR_QUESTOR);
	    SetCharProperty(ch,PROPERTY_INT,"quest_countdown",&quest_countdown);
	    return FALSE;
	}

        objid=obj->pIndexData;
	*quest_obj=objid->vnum;

	sprintf(buf,"For a client, who wants to stay anonymous, I need %s.",objid->short_descr);
	do_function(questman,&do_say,buf);

	if (obj->carried_by) {
	    sprintf(buf,"I know that %s in %s has one.",obj->carried_by->short_descr,obj->carried_by->in_room->area->name);
	    do_function(questman,&do_say,buf);
	} else if (obj->in_obj && obj->in_obj->in_room) {
	    sprintf(buf,"It's in %s in %s.",obj->in_obj->short_descr,obj->in_obj->in_room->area->name);
	    do_function(questman,&do_say,buf);
	} else if (obj->in_obj && obj->in_obj->carried_by) {
	    sprintf(buf,"It's in %s carried by %s in %s.",obj->in_obj->short_descr,obj->in_obj->carried_by->short_descr,obj->in_obj->carried_by->in_room->area->name);
	    do_function(questman,&do_say,buf);
	} else if (obj->in_room) {
	    sprintf(buf,"It's in %s in %s.",obj->in_room->name,obj->in_room->area->name);
	    do_function(questman,&do_say,buf);
	}

	return TRUE;
    }

    else {					// Quest to kill a mob
	CHAR_DATA *vsearch;
	ROOM_INDEX_DATA *room;

	if ((vsearch=FindEvilMobToKill(ch,questman))==NULL)
	    return FALSE;

	room=vsearch->in_room;

	switch(number_range(0,1)) {
	case 0:
	    sprintf(buf,"An enemy of Fatal, %s, is making vile threats "
		"against the crown.",vsearch->short_descr);
	    do_function(questman,&do_say, buf);
	    do_function(questman,&do_say,"This threat must be eliminated!");
	    break;

	case 1:
	    sprintf(buf, "Midgaard's most heinous criminal, %s, has escaped "
		"from the dungeon!",vsearch->short_descr);
	    do_function(questman,&do_say,buf);
	    sprintf(buf, "Since the escape, %s has murdered %d civillians!",
		vsearch->short_descr, number_range(2,20));
	    do_function(questman,&do_say,buf);
	    do_function(questman,&do_say,"The penalty for this crime is death, "
		"and you are to deliver the sentence!");
	    break;
	}

	if (room->name != NULL) {
	    sprintf(buf, "Seek %s out somewhere in the vicinity of %s!",
		vsearch->short_descr,room->name);
	    do_function(questman,&do_say, buf);

	/* I changed my area names so that they have just the name of the area
	   and none of the level stuff. You may want to comment these next two
	   lines. - Vassago */

	    sprintf(buf, "That location is in the general area of %s's %s.",
		room->area->creator, room->area->name);
	    do_function(questman, &do_say,buf);
	}
	*quest_mob = vsearch->pIndexData->vnum;
    }
    return TRUE;
}

/* Called from update_handler() by pulse_area */

void quest_update(void) {
    CHAR_DATA *ch, *ch_next;
    int quest_mob,quest_obj,quest_countdown;
    long quest_giver;

    for ( ch = char_list; ch != NULL; ch = ch_next ) {
        ch_next = ch->next;

	if (IS_NPC(ch)) continue;

	if (!STR_IS_SET(ch->strbit_act, PLR_QUESTOR)) continue;

	quest_mob=quest_obj=quest_countdown=quest_giver=0;
	GetCharProperty(ch,PROPERTY_INT,"quest_mob",&quest_mob);
	GetCharProperty(ch,PROPERTY_INT,"quest_obj",&quest_obj);
	GetCharProperty(ch,PROPERTY_LONG,"quest_giver",&quest_giver);
	GetCharProperty(ch,PROPERTY_INT,"quest_countdown",
	   						 &quest_countdown);

	if (quest_giver==0) {
	    quest_countdown--;

	    if (quest_countdown>0) {
		SetCharProperty(ch,PROPERTY_INT,
		    "quest_countdown",&quest_countdown);
		continue;
	    }

	    send_to_char("You may now quest again.\n\r",ch);
	    STR_REMOVE_BIT(ch->strbit_act,PLR_QUESTOR);
	    DeleteCharProperty(ch,PROPERTY_LONG,"quest_giver");
	    DeleteCharProperty(ch,PROPERTY_INT,"quest_obj");
	    DeleteCharProperty(ch,PROPERTY_INT,"quest_mob");
	    DeleteCharProperty(ch,PROPERTY_INT,"quest_countdown");
	    DeleteCharProperty(ch,PROPERTY_STRING,"quest_desc");
	    continue;
	} else {
	    quest_countdown--;
	    if (quest_countdown>0) {
		if (quest_countdown<6)
		    send_to_char("Better hurry, you're almost out of time for "
			"your quest!\n\r",ch);

		SetCharProperty(ch,PROPERTY_INT,
		    "quest_countdown",&quest_countdown);
		continue;
	    }

	    quest_countdown=30;
	    sprintf_to_char(ch,"You have run out of time for your "
		"quest!\n\rYou may quest again in %d minute%s.\n\r",
		quest_countdown,quest_countdown==1?"":"s");
	    DeleteCharProperty(ch,PROPERTY_LONG,"quest_giver");
	    DeleteCharProperty(ch,PROPERTY_INT,"quest_obj");
	    DeleteCharProperty(ch,PROPERTY_INT,"quest_mob");
	    SetCharProperty(ch,PROPERTY_INT,
		"quest_countdown",&quest_countdown);
	    DeleteCharProperty(ch,PROPERTY_STRING,"quest_desc");
        }
    }
}


///////////////////////////////////////////////////////////////////////////////

int  TAG_timetotag = 0;
bool TAG_tagon     = FALSE;
int  TAG_timetag   = 0;
int  TAG_whotag    = 0;
int  TAG_tagging   = FALSE;
int  TAG_first     = 0;
int  TAG_pulse     = PULSE_TAGSTART;
int  TAG_maze	   = 1;
CHAR_DATA *tagmaster=NULL;

int TAG_MAZE[2][2]={{1900,1925},{1926,1949}};

#define	ROOM_LOWEST	1900
#define	ROOM_HIGHEST	1949

void do_tagging( CHAR_DATA *ch, char *argument) {
    char buf[MAX_STRING_LENGTH];
    char arg[MAX_STRING_LENGTH];
    CHAR_DATA *player;
    bool b;
    int i;

    argument=one_argument(argument,arg);

    switch (which_keyword(arg,"start","stop","maze",NULL)) {
	case -1:
	case 0:
	    send_to_char("Syntax: tagging <start/stop/maze number>\n\r",ch);
	    return;
	case 1:
	    if (TAG_tagon) {
		send_to_char("Tagging fun is already on!\n\r",ch);
		return;
	    }

	    tagmaster=get_char_world(ch,"tagmaster");
	    if (tagmaster==NULL) {
		send_to_char("Couldn't find the TagMaster!\n\r",ch);
		return;
	    }

	    STR_REMOVE_BIT(tagmaster->strbit_comm,COMM_NOCHANNELS);
	    STR_REMOVE_BIT(tagmaster->strbit_comm,COMM_NOTELL);

	    TAG_tagon=TRUE;
	    do_function(ch,&do_quest,"Okay everyone, tagging fun has started.");
	    do_function(ch,&do_quest,"type {Gtagjoin"C_QUEST" to join the fun!");
	    sprintf_to_char(ch,
		"Maze %d selected, use {Wtagging maze <num>{x "
		"to select another.\n\r",TAG_maze);
	    TAG_timetotag = 3;
	    for (player=player_list;player;player=player->next_player) {
		DeleteCharProperty(player,PROPERTY_BOOL,"tagjoined");
	    }

	    return;

	case 2:
	    if (!TAG_tagon) {
		send_to_char("There isn't any tagging going on!\n\r",ch);
		return;
	    }

	    TAG_tagging=FALSE;
	    TAG_tagon=FALSE;
	    TAG_whotag= 0;
	    TAG_timetotag=0;
	    TAG_timetag=0;
	    TAG_first=0;
	    do_function(ch,do_quest,"Okay everyone, tagging fun has stopped.");
	    {
		ROOM_INDEX_DATA *temple,*from_room;
		temple=get_room_index(ROOM_VNUM_TEMPLE);
		for (player=player_list; player!=NULL;player=player->next_player) {

		    b=FALSE;
		    GetCharProperty(player,PROPERTY_BOOL,"tagjoined",&b);
		    if (b) {
			if (player->in_room->vnum <TAG_MAZE[TAG_maze][0] ||
			    player->in_room->vnum >TAG_MAZE[TAG_maze][1]) {
			    act("You left and get nothing.",player,
				NULL,NULL,TO_CHAR);
			} else {
			    i=0;
			    GetCharProperty(player,PROPERTY_INT,"tagreward",&i);
			    sprintf(buf,"%s %d",player->name,UMIN(i,15));
			    do_function(ch,&do_questpoints,buf);
			}
			DeleteCharProperty(player,PROPERTY_INT,"tagreward");
			DeleteCharProperty(player,PROPERTY_BOOL,"tagit");
			DeleteCharProperty(player,PROPERTY_BOOL,"tagjoined");

			from_room=player->in_room;
			ap_leave_trigger(player,temple);
			char_from_room(player);
			char_to_room(player,temple);
			ap_enter_trigger(player,from_room);
		    }
		}
	    }
	    return;

	case 3:
	    i=atoi(buf)+1;
	    argument=one_argument(argument,buf);
	    if (!is_number(buf) || i<=0 || i>2 ) {
		send_to_char(
		    "Maze# should be an integer between 1 and 2.\n\r",ch);
		return;
	    }
	    sprintf_to_char(ch,"Maze %d selected.\n\r",i);
	    TAG_maze=i-1;
	    return;

    }
}

/* Deze moet in update.c onder aggr_update ofzo */

void tag_update(void) {
    char buf[MAX_STRING_LENGTH];
    CHAR_DATA *player;
    int random = 0;
    bool start = FALSE;
    bool decrease = FALSE;
    bool b;

    if (!IS_VALID(tagmaster))
	return;

    if (TAG_tagon == TRUE) {
	if (TAG_tagging == TRUE)
	    TAG_pulse -= 3;
	else
	    TAG_pulse -= 1;

	if(TAG_pulse <= 0) {
	    TAG_pulse = PULSE_TAGSTART;

	    if (TAG_timetotag > 0 && TAG_tagon == TRUE) {
		sprintf(buf, "%d tick%s to join the tagging fun.",
		    TAG_timetotag,
		    TAG_timetotag==1?"":"s");
		do_function(tagmaster,&do_quest,buf);
		decrease = TRUE;
	    }

	    if (TAG_timetotag == 0 && TAG_tagon && !TAG_tagging ) {
		if(TAG_whotag < 2) {
		    do_function(tagmaster,&do_quest,
			"Too few people to tag, stopping the fun.");
		    TAG_tagon = FALSE;

		    for (player=player_list;player!=NULL;player=player->next_player)
			DeleteCharProperty(player,PROPERTY_BOOL,"tagjoined");
		} else {
		    do_function(tagmaster,&do_quest,"Tagging has started!");
		    for (player=player_list;player!=NULL;player=player->next_player) {

			b=FALSE;
			GetCharProperty(player,PROPERTY_BOOL,"tagjoined",&b);
			if (b) {
			    ROOM_INDEX_DATA *to_room,*from_room;
			    random = number_range(TAG_MAZE[TAG_maze][0],TAG_MAZE[TAG_maze][1]);
			    from_room=player->in_room;
			    to_room=get_room_index(random);
			    ap_leave_trigger(player,to_room);
			    char_from_room(player);
			    char_to_room(player, to_room);
			    ap_enter_trigger(player,from_room);
			    look_room(player,player->in_room,TRUE);
			}
		    }

		    start = TRUE;
		}
	    }

	    if (TAG_tagging) {
		for (player = player_list; player!=NULL; player=player->next_player) {

		    b=FALSE;
		    GetCharProperty(player,PROPERTY_BOOL,"tagjoined",&b);
		    if ( b ) {
			b=FALSE;
			GetCharProperty(player,PROPERTY_BOOL,"tagit",&b);
			if (b) {
			    sprintf(buf,"%s You are in again!",player->name);
			    do_function(tagmaster,&do_tell,buf);
			    DeleteCharProperty(player,PROPERTY_BOOL,"tagit");
			}
		    }
		}
	    }

	    if(start == TRUE)
		TAG_tagging = TRUE;

	    if(decrease == TRUE)
		TAG_timetotag--;
	}
    }
    return;
}

void do_tagjoin(CHAR_DATA *ch, char *argument) {
    char buf[MAX_STRING_LENGTH];
    bool b;

    if (IS_NPC(ch))
	return;

    b=FALSE;
    GetCharProperty(ch,PROPERTY_BOOL,"tagjoined",&b);
    if (b) {
	send_to_char("You are already in dummy!\n\r", ch);
	return;
    }

    if( TAG_tagon != TRUE) {
	send_to_char("You can't join something that isn't there.\n\r",ch);
	return;
    }

    if( TAG_tagging == TRUE) {
	send_to_char("Already started, you are too late.\n\r",ch);
	return;
    }

    b=TRUE;
    SetCharProperty(ch,PROPERTY_BOOL,"tagjoined",&b);

    sprintf(buf,"%s has joined the fun!",ch->name);
    do_function(tagmaster,&do_quest,buf);
    TAG_whotag++;

    send_to_char("Please wait until the tagging fun starts.\n\r",ch);
    return;
}



void do_tag(CHAR_DATA *ch, char *argument) {

    CHAR_DATA *victim;
    char buf[MAX_STRING_LENGTH];
    int i;
    bool b;

    if( TAG_tagon != TRUE||TAG_tagging != TRUE) {
	check_social( ch, "tag", argument );
	return;
    }

    if(argument[0] == '\0') {
	send_to_char("Who do you want to tag?\n\r",ch);
	return;
    }

    if ( ( victim = get_char_room( ch, argument ) ) == NULL) {
	send_to_char("Nice try but that person isn't here!\n\r",ch);
	return;
    }

    if (IS_NPC(victim)) {
	act("What is $N doing here?",ch,NULL,victim,TO_CHAR);
	act("$n disappears in a puff of logic.",victim,NULL,NULL,TO_ROOM);
	extract_char(victim,TRUE);
	return;
    }

    if( victim == ch) {
	check_social( ch, "tag", "self" );
	return;
    }

    b=FALSE;
    GetCharProperty(victim,PROPERTY_BOOL,"tagit",&b);
    if (b) {
	send_to_char("You can't tag that one till the end of the tick!\n\r",ch);
	return;
    }

    b=FALSE;
    GetCharProperty(ch,PROPERTY_BOOL,"tagit",&b);
    if (b) {
	send_to_char("You can't play along till the next tick!\n\r",ch);
	return;
    }

    i=0;
    GetCharProperty(ch,PROPERTY_INT,"tagreward",&i);
    i++;
    SetCharProperty(ch,PROPERTY_INT,"tagreward",&i);

    b=TRUE;
    SetCharProperty(victim,PROPERTY_BOOL,"tagit",&b);

    send_to_char("You have tagged someone!!\n\r",ch);
    sprintf(buf,"%s has been tagged by %s!",victim->name,ch->name);
    do_function(tagmaster,&do_quest,buf);
    send_to_char("You have to wait till the next tick before you can continue.\n\r",victim);

    return;
}
