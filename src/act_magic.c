//
// $Id: act_magic.c,v 1.12 2004/06/06 09:12:43 jodocus Exp $
//
// Copyright (c) 2000
//      The staff of Fatal Dimensions.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY FATAL DIMENSIONS AND CONTRIBUTORS ``AS
// IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
// STAFF OF FATAL DIMENSIONS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "merc.h"

void drop_object(CHAR_DATA *ch,OBJ_DATA *obj) {  
  
    // huh?
    if (obj->carried_by==NULL)
	return;

    obj_from_char(obj);
    obj_to_room(obj,ch->in_room);

    act("Oh no, you've dropped $p",ch,obj,NULL,TO_CHAR);
    act("$n has dropped $p",ch,obj,NULL,TO_ROOM);
}


void do_quaff( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    OBJ_DATA *obj;

    one_argument( argument, arg );

    if ( arg[0] == '\0' ) {
	send_to_char( "Quaff what?\n\r", ch );
	return;
    }

    if ( ( obj = get_obj_carry( ch, arg, ch ) ) == NULL )
    {
	send_to_char( "You do not have that potion.\n\r", ch );
	return;
    }

    if ( obj->item_type != ITEM_POTION )
    {
	send_to_char( "You can quaff only potions.\n\r", ch );
	return;
    }

    if (ch->level < obj->level)
    {
	send_to_char("This liquid is too powerful for you to drink.\n\r",ch);
	return;
    }

    //
    // oh oh, let's see if $e doesn't drop it!
    //
    if (IS_PC(ch) && ch->position==POS_FIGHTING) {
	int chance;

	// too tired in the arms
	if (ch->move<=0) {
	    send_to_char(
		"You're too tired in your arms to grab that potion.\n\r",ch);
	    return;
	}

	// if hp<50%, up to 75% chance of dropping ...
	if (ch->hit<ch->max_hit/2) {
	    chance=100-75*(100*ch->hit/ch->max_hit)/100;
	    if (number_percent()<chance) {
		drop_object(ch,obj);
		return;
	    }
	}

	// adrenaline effect, up to 48% chance of dropping
	if (ch->pcdata->condition[COND_ADRENALINE]) {
	    chance=100-ch->pcdata->condition[COND_ADRENALINE]; // 100 to 52
	    if (number_percent()>chance) {
		drop_object(ch,obj);
		return;
	    }
	}

	// strength-check
	// if (ch->perm_stat[stat],ch->mod_stat[stat]

    }
    if ( OBJ_HAS_TRIGGER( obj, OTRIG_PREQUAFF ) )
	if (op_prequaff_trigger( obj, ch )==FALSE)
		return;

    act( "$n quaffs $p.", ch, obj, NULL, TO_ROOM );
    act( "You quaff $p.", ch, obj, NULL ,TO_CHAR );
    if (IS_PC(ch))
	STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);

    obj_cast_spell( obj->value[1], obj->value[0], ch, ch, NULL, "" );
    obj_cast_spell( obj->value[2], obj->value[0], ch, ch, NULL, "" );
    obj_cast_spell( obj->value[3], obj->value[0], ch, ch, NULL, "" );
    obj_cast_spell( obj->value[4], obj->value[0], ch, ch, NULL, "" );

    if ( OBJ_HAS_TRIGGER( obj, OTRIG_QUAFF ) )
	op_quaff_trigger( obj, ch );

    if (!STR_IS_SET(obj->strbit_extra_flags, ITEM_NO_LEFTOVERS)) {
	OBJ_DATA *nobj;
	nobj=create_object(get_obj_index(OBJ_VNUM_EMPTYVIAL),0);
	obj_to_char(nobj,ch);
	op_load_trigger(nobj,NULL);
    }
    extract_obj( obj );
    return;
}



void do_recite( CHAR_DATA *ch, char *argument ) {
    char arg1[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    char s[MIL];
    CHAR_DATA *victim;
    OBJ_DATA *scroll;
    OBJ_DATA *obj;

    argument = one_argument( argument, arg1 );
    strcpy(s,argument);		// safe for in obj_cast_spell()
    argument = one_argument( argument, arg2 );

    if ((scroll=get_eq_char(ch,WEAR_HOLD))!=NULL) {
	if (scroll->item_type!=ITEM_SCROLL)
	    scroll=NULL;
	else if (!is_name(arg1,scroll->name))
	    scroll=NULL;
    }

    if (scroll==NULL && (scroll=get_obj_carry(ch,arg1,ch))==NULL) {
	send_to_char( "You do not have that scroll.\n\r", ch );
	return;
    }

    if ( scroll->item_type != ITEM_SCROLL ) {
	send_to_char( "You can recite only scrolls.\n\r", ch );
	return;
    }

    if ( ch->level < scroll->level) {
	send_to_char(
	    "This scroll is too complex for you to comprehend.\n\r",ch);
	return;
    }

    obj = NULL;
    if ( arg2[0] == '\0' ) {
	victim = ch;
    } else {
	if ( ( victim = get_char_room ( ch, arg2 ) ) == NULL
	&&   ( obj    = get_obj_here  ( ch, arg2 ) ) == NULL ) {
	    send_to_char( "You can't find it.\n\r", ch );
	    return;
	}
    }

    if (OBJ_HAS_TRIGGER(scroll,OTRIG_PRERECITE))
	if (!op_prerecite_trigger(scroll,ch,obj,victim))
	    return;

    act( "$n recites $p.", ch, scroll, NULL, TO_ROOM );
    act( "You recite $p.", ch, scroll, NULL, TO_CHAR );
    if (IS_PC(ch)) {
	STR_SET_BIT(ch->pcdata->usedthatobject,scroll->pIndexData->vnum);
	if (obj!=NULL)
	    STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);
    }

    if (!skillcheck2(ch,gsn_scrolls,80)) {
	send_to_char("You mispronounce a syllable.\n\r",ch);
	check_improve(ch,gsn_scrolls,FALSE,2);
    } else {
    	obj_cast_spell( scroll->value[1], scroll->value[0], ch, victim, obj, s);
    	obj_cast_spell( scroll->value[2], scroll->value[0], ch, victim, obj, s);
    	obj_cast_spell( scroll->value[3], scroll->value[0], ch, victim, obj, s);
    	obj_cast_spell( scroll->value[4], scroll->value[0], ch, victim, obj, s);
	check_improve(ch,gsn_scrolls,TRUE,2);
	if (OBJ_HAS_TRIGGER(scroll, OTRIG_RECITE))
	  op_recite_trigger(scroll,ch,obj,victim);
    }

    if (!STR_IS_SET(scroll->strbit_extra_flags, ITEM_NO_LEFTOVERS)) {
	OBJ_DATA *nscroll;
	nscroll=create_object(get_obj_index(OBJ_VNUM_EMPTYSCROLL),0);
	obj_to_char(nscroll,ch);
	op_load_trigger(nscroll,NULL);
    }
    extract_obj( scroll );
    return;
}



void do_brandish( CHAR_DATA *ch, char *argument )
{
    CHAR_DATA *vch;
    CHAR_DATA *vch_next;
    OBJ_DATA *staff;
    int sn;

    if ( ( staff = get_eq_char( ch, WEAR_HOLD ) ) == NULL )
    {
	send_to_char( "You hold nothing in your hand.\n\r", ch );
	return;
    }

    if ( staff->item_type != ITEM_STAFF )
    {
	send_to_char( "You can brandish only with a staff.\n\r", ch );
	return;
    }

    if ( ( sn = staff->value[3] ) < 0
    ||   sn >= MAX_SKILL
    ||   skill_spell_fun(sn,ch) == 0
    ||   !skill_implemented(sn,ch))
    {
	bugf( "Do_brandish: bad sn %d for %s.", sn,NAME(ch) );
	return;
    }

    WAIT_STATE( ch, 2 * PULSE_VIOLENCE );
    if (IS_PC(ch))
	STR_SET_BIT(ch->pcdata->usedthatobject,staff->pIndexData->vnum);

    if ( staff->value[2] > 0 )
    {
	act( "$n brandishes $p.", ch, staff, NULL, TO_ROOM );
	act( "You brandish $p.",  ch, staff, NULL, TO_CHAR );
	if ( ch->level < staff->level
	||   !skillcheck2(ch,gsn_staves,80)) {
	    act ("You fail to invoke $p.",ch,staff,NULL,TO_CHAR);
	    act ("...and nothing happens.",ch,NULL,NULL,TO_ROOM);
	    check_improve(ch,gsn_staves,FALSE,2);
	}

	else for ( vch = ch->in_room->people; vch; vch = vch_next )
	{
	    vch_next	= vch->next_in_room;

	    switch ( skill_target(sn,ch) )
	    {
	    default:
		bugf("Do_brandish: bad target for sn %d for %s.",sn,NAME(ch));
		return;

	    case TAR_IGNORE:
		if ( vch != ch )
		    continue;
		break;

	    case TAR_CHAR_OFFENSIVE:
		if ( IS_NPC(ch) ? IS_NPC(vch) : !IS_NPC(vch) )
		    continue;
		break;

	    case TAR_CHAR_DEFENSIVE:
		if ( IS_NPC(ch) ? !IS_NPC(vch) : IS_NPC(vch) )
		    continue;
		break;

	    case TAR_CHAR_SELF:
		if ( vch != ch )
		    continue;
		break;
	    }

	    obj_cast_spell( staff->value[3], staff->value[0], ch, vch, NULL, argument );
	    check_improve(ch,gsn_staves,TRUE,2);
	}
    }

    if ( --staff->value[2] <= 0 )
    {
	act( "$n's $p blazes bright and is gone.", ch, staff, NULL, TO_ROOM );
	act( "Your $p blazes bright and is gone.", ch, staff, NULL, TO_CHAR );
	extract_obj( staff );
    }

    return;
}



void do_zap( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    OBJ_DATA *wand;
    OBJ_DATA *obj;

    one_argument( argument, arg );
    if ( arg[0] == '\0' && ch->fighting == NULL )
    {
	send_to_char( "Zap whom or what?\n\r", ch );
	return;
    }

    if ( ( wand = get_eq_char( ch, WEAR_HOLD ) ) == NULL )
    {
	send_to_char( "You hold nothing in your hand.\n\r", ch );
	return;
    }

    if ( wand->item_type != ITEM_WAND )
    {
	send_to_char( "You can zap only with a wand.\n\r", ch );
	return;
    }

    obj = NULL;
    if ( arg[0] == '\0' )
    {
	if ( ch->fighting != NULL )
	{
	    victim = ch->fighting;
	}
	else
	{
	    send_to_char( "Zap whom or what?\n\r", ch );
	    return;
	}
    }
    else
    {
	if ( ( victim = get_char_room ( ch, arg ) ) == NULL
	&&   ( obj    = get_obj_here  ( ch, arg ) ) == NULL )
	{
	    send_to_char( "You can't find it.\n\r", ch );
	    return;
	}
    }
    // PREZAP here
    if (OBJ_HAS_TRIGGER(wand,OTRIG_PREZAP) &&
	!op_prezap_trigger(wand,ch,victim,obj))
	return;

    WAIT_STATE( ch, 2 * PULSE_VIOLENCE );
    if (IS_PC(ch))
	STR_SET_BIT(ch->pcdata->usedthatobject,wand->pIndexData->vnum);

    if ( wand->value[2] > 0 )
    {
	if ( victim == ch ) {
	    act( "$n zaps $mself with $p.", ch, wand, victim, TO_NOTVICT );
	    act( "You zap yourself with $p.", ch, wand, victim, TO_CHAR );
	} else if ( victim != NULL ) {
	    act( "$n zaps $N with $p.", ch, wand, victim, TO_NOTVICT );
	    act( "You zap $N with $p.", ch, wand, victim, TO_CHAR );
	    act( "$n zaps you with $p.",ch, wand, victim, TO_VICT );
	} else {
	    act( "$n zaps $P with $p.", ch, wand, obj, TO_ROOM );
	    act( "You zap $P with $p.", ch, wand, obj, TO_CHAR );
	    if (IS_PC(ch))
		STR_SET_BIT(ch->pcdata->usedthatobject,obj->pIndexData->vnum);
	}

 	if (ch->level < wand->level
	||  !skillcheck2(ch,gsn_wands,80)) {
	    act( "Your efforts with $p produce only smoke and sparks.",
		 ch,wand,NULL,TO_CHAR);
	    act( "$n's efforts with $p produce only smoke and sparks.",
		 ch,wand,NULL,TO_ROOM);
	    check_improve(ch,gsn_wands,FALSE,2);
	}
	else
	{
	    obj_cast_spell( wand->value[3], wand->value[0], ch, victim, obj, argument );
	    check_improve(ch,gsn_wands,TRUE,2);
	}
    }

    if (OBJ_HAS_TRIGGER( wand, OTRIG_ZAP))
      op_zap_trigger(wand,ch,victim,obj);

    if ( --wand->value[2] <= 0 )
    {
	act( "$n's $p explodes into fragments.", ch, wand, NULL, TO_ROOM );
	act( "Your $p explodes into fragments.", ch, wand, NULL, TO_CHAR );
	extract_obj( wand );
    }

    return;
}

/* Original Code by Todd Lair.                                        */
/* Improvements and Modification by Jason Huang (huangjac@netcom.com).*/
/* Permission to use this code is granted provided this header is     */
/* retained and unaltered.                                            */

void event_brew(EVENT_DATA *event) {
    // ch is the person who brews
    // object is the object which is brewed in.

    CHAR_DATA *ch=event->ch;
    OBJ_DATA *obj=event->object;
    int *sn=event->data;

    if (!IS_VALID(obj))
	return;
    if (!IS_VALID(ch))
	return;

    WAIT_STATE( ch, skill_beats(gsn_brew,ch) );

    /* Check the skill percentage, fcn(wis,int,skill) */
    if ( !IS_NPC(ch)
         && ( !skillcheck(ch,gsn_brew) ||
              number_percent( ) > ((get_curr_stat(ch,STAT_WIS)-13)*5 +
                                   ((get_curr_stat(ch,STAT_INT)-13)*3) )))
    {
	act( "$p explodes violently!", ch, obj, NULL, TO_ALL );
        spell_acid_blast(gsn_acid_blast, UMIN(ch->level+10,LEVEL_HERO - 1), ch, ch, TARGET_CHAR);
	extract_obj( obj );
	check_improve(ch,gsn_brew,FALSE,2);
	return;
    }

    /* took this outside of imprint codes, so I can make do_brew differs from
       do_scribe; basically, setting potion level and spell level --- JH */

    obj->level = ch->level/2;
    obj->value[0] = ch->level/4;
    check_improve(ch,gsn_brew,TRUE,2);

    spell_imprint(*sn, ch->level, ch, obj, TARGET_OBJ);
    act("$n has finished brewing.",ch,NULL,NULL,TO_ROOM);
}

void do_brew ( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    OBJ_DATA *obj;
    int sn,*psn;

    if (ch->position==POS_FIGHTING) {
	send_to_char("Not now! You're busy!\n\r",ch);
	return;
    }

    if ( !IS_NPC( ch )
	&& ch->level < skill_level(gsn_brew,ch) )
    {
	send_to_char( "You do not know how to brew potions.\n\r", ch );
	return;
    }

    argument = one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Brew what spell?\n\r", ch );
	return;
    }

    /* Do we have a vial to brew potions? */
    for ( obj = ch->carrying; obj; obj = obj->next_content )
    {
	if ( obj->item_type == ITEM_POTION && obj->wear_loc == WEAR_HOLD )
	    break;
    }

    /* Interesting ... Most scrolls/potions in the mud have no hold
       flag; so, the problem with players running around making scrolls
       with 3 heals or 3 gas breath from pre-existing scrolls has been
       severely reduced. Still, I like the idea of 80% success rate for
       first spell imprint, 25% for 2nd, and 10% for 3rd. I don't like the
       idea of a scroll with 3 ultrablast spells; although, I have limited
       its applicability when I reduced the spell->level to 1/3 and 1/4 of
       ch->level for scrolls and potions respectively. --- JH */


    /* I will just then make two items, an empty vial and a parchment available
       in midgaard shops with holdable flags and -1 for each of the 3 spell
       slots. Need to update the midgaard.are files --- JH */

    if ( !obj )
    {
	send_to_char( "You are not holding a vial.\n\r", ch );
	return;
    }

    if ( ( sn = skill_lookup(arg) )  < 0)
    {
	send_to_char( "You don't know any spells by that name.\n\r", ch );
	return;
    }

    /* preventing potions of gas breath, acid blast, etc.; doesn't make sense
       when you quaff a gas breath potion, and then the mobs in the room are
       hurt. Those TAR_IGNORE spells are a mixed blessing. - JH */

    if ( (skill_target(sn,ch) != TAR_CHAR_DEFENSIVE) &&
         (skill_target(sn,ch) != TAR_CHAR_SELF)              )
    {
	send_to_char( "You cannot brew that spell.\n\r", ch );
	return;
    }

    act( "$n begins preparing a potion.", ch, NULL, NULL, TO_ROOM );
    act( "You begin to prepare a potion.", ch, NULL, NULL, TO_CHAR );

    psn=(int *)calloc(1,sizeof(int));
    *psn=sn;
    create_event(2,ch,NULL,obj,event_brew,psn);
    WAIT_STATE( ch, skill_beats(gsn_brew,ch) );
}

void event_scribe(EVENT_DATA *event) {
    CHAR_DATA *ch=event->ch;
    OBJ_DATA *obj=event->object;
    int *psn=event->data;

    if (!IS_VALID(obj))
	return;
    if (!IS_VALID(ch))
	return;

    /* Check the skill percentage, fcn(int,wis,skill) */
    if ( !IS_NPC(ch)
         && ( !skillcheck(ch,gsn_scribe) ||
              number_percent( ) > (((get_curr_stat(ch,STAT_WIS)-13)*5 +
                                   ((get_curr_stat(ch,STAT_INT)-13)*3) ))))
    {
	act( "$p bursts in flames!", ch, obj, NULL, TO_CHAR );
	act( "$p bursts in flames!", ch, obj, NULL, TO_ROOM );
        spell_fireball(gsn_fireball, UMIN(ch->level+10,LEVEL_HERO - 1), ch, ch, TARGET_CHAR);
	check_improve(ch,gsn_scribe,FALSE,2);
	extract_obj( obj );
	return;
    }

    /* basically, making scrolls more potent than potions; also, scrolls
       are not limited in the choice of spells, i.e. scroll of enchant weapon
       has no analogs in potion forms --- JH */

    obj->level = ch->level*2/3;
    obj->value[0] = ch->level/3;
    check_improve(ch,gsn_scribe,TRUE,2);

    spell_imprint(*psn, ch->level, ch, obj, TARGET_OBJ);
    act("$n has finished scribing.",ch,NULL,NULL,TO_ROOM);
}

void do_scribe ( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    OBJ_DATA *obj;
    int sn,*psn;

    if (ch->position==POS_FIGHTING) {
	send_to_char("Not now! You're busy!\n\r",ch);
	return;
    }

    if ( !IS_NPC( ch )
	&& ch->level < skill_level(gsn_scribe,ch)) {
	send_to_char( "You do not know how to scribe scrolls.\n\r", ch );
	return;
    }

    argument = one_argument( argument, arg );

    if (arg[0]==0) {
	send_to_char( "Scribe what spell?\n\r", ch );
	return;
    }

    /* Do we have a parchment to scribe spells? */
    for ( obj = ch->carrying; obj!=NULL ; obj = obj->next_content ) {
	if ( obj->item_type == ITEM_SCROLL && obj->wear_loc == WEAR_HOLD )
	    break;
    }
    if (obj==NULL) {
	send_to_char( "You are not holding a parchment.\n\r", ch );
	return;
    }


    if ((sn=skill_lookup(arg))<0) {
	send_to_char( "You don't know any spells by that name.\n\r", ch );
	return;
    }

    act( "$n begins writing a scroll.", ch, NULL, NULL, TO_ROOM );
    act( "You begin to write a scroll.", ch, NULL, NULL, TO_CHAR );
    WAIT_STATE(ch,skill_beats(gsn_scribe,ch));

    psn=(int *)calloc(1,sizeof(int));
    *psn=sn;
    create_event(2,ch,NULL,obj,event_scribe,psn);
}
