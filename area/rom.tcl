namespace eval mud::memory {
  namespace export remember forget memory

  #Remeber given identifier, each time the function is called this function
  #will return a higher number. This memory is kept in the namespace of
  #the caller
  proc remember {id} {
    namespace eval [uplevel {namespace current}] {
      variable __thememory
      set __id [uplevel {set id}]

      set __i [expr [::mud::memory::memory $__id]+1]
      array set __thememory "$__id $__i"

      return $__i
    }
  }
  
  #Forget the given identifier. This function doesn't work completely
  #correct yet, since it is only set to 0 and not really removed from
  #the array, so no memory is freed by using this command.
  proc forget {id} {
    namespace eval [uplevel {namespace current}] {
      variable __thememory
      set __id [uplevel {set id}]
      
      if {[array get __thememory $__id]!=""} {
        set __thememory($__id) ""
      }

      return 0
    }
  }
 
  #Returns the total times the id is remembered.
  proc memory {id} {
    namespace eval [uplevel {namespace current}] {
      variable __thememory
      set __id [uplevel {set id}]

      if {[array get __thememory $__id]==""} {
        return 0
      } else {
        return $__thememory($__id)
      }
    }
  }

}

proc uses {lib} {
  namespace eval [uplevel {namespace current}] {
    switch [uplevel {set lib}] {
      memory {namespace import ::mud::memory::*}
      default {return -code error "uses: unknown library [uplevel {set lib}]"}
    } 
  }
}

proc color {args} {
  if {[llength $args]==0} {
    return "\{x"
  } else {
    switch [lindex $args 0] {
      red {return "\{r"}
      green {return "\{g"}
      blue {return "\{b"}
      cyan {return "\{c"}
      magenta {return "\{m"}
      yellow {return "\{y"}
      gray {return "\{w"}
      grey {return "\{w"}
      white {return "\{w"}
      underline {return "\{_"}
      blink {return "\{*"}
      inverse {return "\{#"}
      strike {return "\{-"}
      black {return "\{D"}
      random {return "\{>"}
      RED {return "\{R"}
      GREEN {return "\{G"}
      BLUE {return "\{B"}
      CYAN {return "\{C"}
      MAGENTA {return "\{M"}
      YELLOW {return "\{Y"}
      GRAY {return "\{D"}
      GREY {return "\{D"}
      WHITE {return "\{W"}
    }
  }
}
