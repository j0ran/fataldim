---
title: "The Fatal Dimensions Mud Server"
date: 2019-11-10T13:39:22+01:00
hidden: true
menu:
    main:
      name: server
      parent: info
---
Fatal Dimensions is currently running in a [Docker](http://www.docker.com) container on a [Digital Ocean](https://www.digitalocean.com/) droplet. This website is a static website generated with [Hugo](https://gohugo.io/) running in another Docker container on the same droplet. The source code of all of this can be found here: https://bitbucket.org/j0ran/fataldim.

Before this - in the beginning - the mud server looked like this:

![Fatal Dimensions Mud Server](mudserver.jpg)

Fatal Dimensions was running on a P5/90MHz based PC with 64Mb of RAM and a standard NE2000 ethernet-card, capable of pushing about 10 million bits per seconds into your general direction. The system is powered by FreeBSD 4.4. It is like an wigwam: no windows, no gates and apache inside. 