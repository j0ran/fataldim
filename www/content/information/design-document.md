---
title: "Design Document"
date: 2019-11-06T09:08:06+01:00
hidden: true
menu:
    main:
      parent: info
---
This document describes the structure and rules of Fatal Dimensions. Please be aware that this is a draft and subject to changes. Comment are welcome and can be send to the Fatal Dimensions Council.

Council
=======

The council is a group of immortals that together runs Fatal Dimensions. The concil consists of the following members:

* The Head Council Member, this will be Fatal.
* The Head Questor
* The Head Builder
* The Head Coder
* Additional members.

What does the council do?
-------------------------

The Council discusses and votes on the hiring/firing of the immortal staff. Monthly they decide on advances/demotions for the staff, from the reports submitted to Fatal prior to the first day of the month. They design and approve all documents for offline and online that are presented. Heads usually will submit these to Fatal. They decide the major running of the mud through teamwork and experience. When the council thinks a disision of an immotal was wrong, it may be turned back and the immortal will get a penelty acordingly.
How to contact the council

You can contact the council by sending a note to: council
or sending email to: council@fataldimensions.nl.

Jobs/Tasks
==========

* Immortals have to have a job description. every month a report has to be made (builders give a description of their new) area(s), questors about their quests etc). The council will evaluate the reports and rise/lower the levels. Add and remove jobs. The report has to be send to the Head Council member (Fatal) before the first day of every new month.
* Add new jobs: head-questor, head-builder etc who help the normal questor/builders and make information ailable on how to quest/build/etc.
* Hiring imms: Level 91 does not mean automaticly becoming immortal, but are great candidates to become immortal.

Builder
-------

### WhoName 
Builder

### Description
* A potential builder will be give a one month "probation" period.
* During the month the builder will be given a small project on which his/her work will be judged.
* There is a second possible way to determine if a candidate is "builder material". That would be is send a sample of the area, the more detail, the better.
* A builder must show the ability to follow directions/instructions.
* A builder must show that he/she followed the tips/guidelines/rules that are posted on the web
* The builder must show they are productive withing that month.
* At the end of the month the "Head Builder" will submit a report and an opinion, and the other Council Members will vote and if they have their own personal opinions or their own observations then of course they are welcome.
* A builder, if accepted after probation, must continue to show that his work is following the guidelines/rules/tips/etc.
* A builder must continue to show that he is "productive" meaning that he does work when he/she logs on.
* The standards for the most part is a "sight" judgement...it is almost impossible to judge a builder's work otherwise.

### Tasks
* design and development of new areas according to building-specifications. (TODO: link to builder specification)
* report of area development (e.g. story, rooms)

### Rules
Before a builder may start a new area he must send a review of the area they want to make to the Head Builder. The Head Builder must approve of the area. When he does the Head Builder will give the Builder some vnum's and permission to make the area.

Offline Builder
---------------

### WhoName 
Builder

### Description
The offline buider also builds area's, but uses primarily the offline builder. This can be found on the Fatal Dimensions homepage in the builders section. The offline builder is not an immortal on the mud, but he has all the tasks a normal builder has like sending in reports. When he finished an area, or wants to put an area in the mud, he will ave to send the area file to the Head Builder.

### Tasks
* All tasks that a normal builder has

Head Builder
------------

### WhoName 
Head Builder

### Description
The Head Builder must have a good knowledge of the M.U.D. (i.e. know the different areas, know how to "walk" to a good deal of the areas not just use "goto")

* The Head Builder must have an excellent knowledge of OLC.
* The Head Builder must know the rules/regulation/tips/guidelines for building.
* The Head Builder must know how to catch things like unbalanced mobs/objects or flags (lack of flags) that may or may not be correct (this goes to a knowledge of OLC)
* The Head Builder must be flexible and must be someone the rest of the Council think they can work with.
* This is an experience job, most of the above standards come with experience.
* The Head Builder must have a creative imagination and it would help it fell within the theme of Fatal Dimensions.

### Tasks
* specification and documentation for building
* education for newbie-builders
* area-management (vnums, progress, location, ideas)
* report of progress related to his tasks.
* Make sure typos in areas, rooms, objects and mobs are removed. If it is in the area of another builder he will instruct that builder to remove it. Typos in stock or other areas are corrected by the head builder himself.

Questor
-------

### WhoName 
Questor

### Tasks
* running of quests and rewarding according to quest-specification
* report of amount and type of quests

Head Questor
------------

### WhoName 
Head Questor

### Tasks
* specification of quests and rewards
* report of progress related to his tasks.

PR
--

### Tasks
* keeping up to date of references to the mud on the public internet (e.g. mudconnecter)
* gaining new players and naamsbekendheid for the mud and the new oppurtunities/possibilities via usenet or something. (rec.games.mud.announce for example)
* report of progress related to his tasks.

Coder
-----

### WhoName 
Coder

### Tasks
* implemention of delegated programming requests
* reporting of progress and new ideas

Head Coder
----------

### WhoName 
Head Coder

### Tasks
* delegation of programming requests
* design and specification of new developments (is also rom, olc-updates)
* struggling through idea-notes (circle is not an option and scan will not be implemented)
* reporting of progress related to his tasks

Council-member
--------------

### Tasks
* post-approval of specifications, documents and designs
* post-approval for website
* members are main-builder, main-coder, main-questor.
* post-approval of penalties and executing the heavy penalties (e.g. perm-ban, delete).
* report of given heavy penelties for post approval of the other members.

Head Council-member
-------------------

### WhoName 
Implementor

### Description
This job is Fatal's job. He makes dissions on the jobs of the council member. Other jobs are keeping track of the jobs of the immortals and the job changes. Also he keeps track of players who want to become immortal. Immortal wannabies should always send there wishes to the Head Council member. When a Head job is not forfilled, he will take the task upon him or delegate it to another immortal. The Head Council member has a veto on all desisions.

### Tasks
* recruiting of new counsil-members
* removing members from the council
* keeping track of the jobs of all immortals
* report of the status of the immortals in the mud
* taking responsibilities for unforfilled head-jobs
* has a veto on all desisions.

Newbie-helper
-------------

### Description
No description.

### Tasks
* helping newbies where and when needed.
* just help, don't cheat. (<-- plain english)
* report helping on who and when and how. Also report when you use the restore command on players.

Guests
------

### WhoName 
Guest

### Tasks
* chatting about life in muds in the most general way you can think of.
* no reporting

Help-editors
------------

### Tasks
* correcting/creating the help where necessary
* reporting if changes has been done

Website
-------

### Tasks
* design, implementation of the website
* maintain the public documents (specifications etc)
* reporting about changes

Clan Overseer
-------------

### WhoName 
Clan Overseer

### Tasks
* keeping track of clans, and there members
* decide if new clans should be made, or if old clans are dead.
* communication between clan leaders and the builders
* reporting of progress related to his tasks.

ClanImmortal
------------

**At the moment there are no clan immortals anymore recourse it gave too much trouble. Immortals spend more time in running the clan (what they shouldn't do) is stead of doing there real job.**

### Description
~~They are there for Role Playing purposes. Having a god to donate, sac to and to worship. That Imm is the intermeadiary between that clan and the council, builder, overseer. They should know their clan members and have general knowledge of clan workings (ie. gold in bank, number of active members, inactive members). They are not to be a recruiting/advancing member of the clan. They can talk to people get them interested in the clan .. but they themselves can not recruit them. Imms are also responsible for the general conduct of the clan .. if there is a problem usually the clan Imm will be one of the first notified. Try to get the problem resolved, before it become serious and the council needs to step in and handle it. The rules of the clan and mud should be known by the Imm so that they can properly administer the clan. Imms also should be advisor to the leaders.. support and counsler also.~~

### Tasks

* ~~this is a roleplaying task, no real responsibilities~~
* ~~communication with the Clan Overseer~~
* ~~no reports needed, real work is done by Clan Overseer~~
* ~~is not allowed to handle any clan tasks like recruiting or determining rules~~

Sistermud Member
----------------

### WhoName 
SisterMud

### Description
Sistermud members are just like normal guests, but can be reconized by there whoname that it is a sistermud member. They have the same tasks a guest has.

Stringer
--------

### WhoName 
Stringer

### Description
* Trusted immortals could get the job stringer.
* With this job you get the commands so you can handle all restring AND enchant tokens.
* With this job you are also allowed to do restrings and enchants (using the set and string command) in exchange for the quest tokens.
* You are not allowed to change objects on your own for other mortals or immortals.
* The tokens must be purged when done. (So it is visible in the logfile)

### Tasks
* being able to handle restring and enchant tokens
* report of changed objects and for who the objects where changed.

Levels of immortals
===================

Need to be filled in yet.

Building specifications
=======================

The Head Builder will specify the building specifications. These will be published on the website and maybe in the help system of the mud. These specifications involve:

* Kind of areas
* Kind of mobs
* Limitations on objects
* Colors and color-leaking
* Balance of area
* Type of story (must fit in the mud).

Quest specifications
====================

The head questor will specify the quest specifications. These will be published on the website and maybe in the help system of the mud. The quests itself must be described in quest documents. All questors may run quests from these documents according to the Quest Specifications. The Quest Specifications and the Quest documents can all be found in the Quest Document.

Quest Specification
-------------------

* General specifications of how to run quests.
* Witch rules must be followed while running quests.
* What type of quests and rewards are not allowed.
* Timespan of quests.

Quest Document
--------------

* Will describe the quest and how it should be run.
* Describes the rewards that may be given for those quests.

Penalty system
==============

Available penalties and penalty commands
----------------------------------------

This section contains the penelties that can be given. If a penalty is not in the list, please notify the council. The penalties typedset in a fixed font are penalty commands. Not all penalty commands are real penalties, but it are commands that should appear in the penalty report. When an immortal has no log command, he or she may not give penalties unless stated otherwise. If you can't log someone, you may not give an penalty to that player. The council always checs afterwards if the penalty was correctly given. If not the immortal giving the penelty can get a penalty.

Recap:

* All the following commands need to be added to the penalty report when used. Also when not used as a penalty!
* You may NOT give any panalties if you don't have the log command.

Penalty commands:

Who may give these penalties | Type | List if penalty commands
-----------------------------|------|--------------------------
All Immortals | | smite, notell, nochannel, noemote, freeze, ban <site> newbie, newlock, force mortals to drop stuff, putting a mortal in jail, removing objects from mortals.
Council members | | ban <site> all, grant, revoke, trust (on level 92-98 immortals), pardon, wizlock, deny, giving a mortal damage.
Head Council Member | | ban <site> permit, permban <site> permit, permban <site> all, permban <site> newbie, allow <site>, advance, trust, purge on players, force delete

When should you use what penalty?
---------------------------------

This part is under construction, currently the penalty rules are as follows:

* The guideline for breaking the rules are:
  * Smite, for small misteps
  * NoTell, NoChannel, NoShout, NoEmote, for abusive language
  * Freeze, for very annoying persons.
  * ban newbie, for characters that keep creating new characters. You don't have to log yourself before doing this, but you must have a good reason that is visible in the logs.
* Try to use penalties wisely, you can get a penalty from the council when it thinks you gave ginven a too heavy penalty.
* Don't use penalty command's you may not use.

Below is a list of given and approved penalties, this list will grow in time and in the end the defenitive penalty list will be derived from this list:

Offence | Offence |  Who | Penalty
--------|---------|------|---------
Breaking OOL Rules (R1:6)    | 1  | Immortals     | 2 days in jail (Room 13)
Breaking OOL Rules (R1:6)    | 2  | Immortals     | 4 days in jail (Room 13)
Breaking OOL Rules (R1:6)    | 3  | Immortals >95 | Frozen 1 week
Breaking OOL Rules (R1:6)    | 4  | Council       | Deletion
Harassing Immortals (R1:10)  | 1  | Immortals     | 1 day in jail (Room 13)
Harassing Immortals (R1:10)  | >1 | Immortals     | Frozen for 2 days
Sexual oriented names (R3:6) |    |               | Very low level chars may be deleted. Players may be banned if they continue to create new names of this kind. And higher level players may be frozen.

Giving penelties and removing penelties.
----------------------------------------

All commands that have to do with giving penelties, demoting and removing jobs hould be added to the penelty note list. This way all immortals know what is happening. These commands should also be seperetly be reported to the council. Also commands like removing penalties, assigning jobs and promoting should be reported to the council.

R1: Players Rules, Laws and Rights
==================================

These are the rules of Fatal Dimensions. For other matters, use common sense.

* REMEMBER playing here is a privilege not a right. Obey any orders from imms, and you will be fine.
* No kill stealing. Do not attack somebody else's mob, unless they ask you to. Also avoid attacking fleeing mobs.
* [Multiplaying](#what-is-multiplaying) is allowed up to 3 characters, not one more.
* [Out of level interaction](#what-is-out-of-level-interaction) is limited.
* If you find a bug, please report it! Not reporting bugs will be considered cheating.
* No swearing over any channel (not even while telling or saying)! This includes the harrassment or abuse of others. Be very careful with the pray channel, insulting or harassing immortals is not allowed.
* Dueling is allowed only in the specified areas. Anything else is PKill and won't be tolerated.See [PK Rules](#pk-rules).
* Mud Sex is not allowed. Mud sex is activly roleplaying having sex.
* IMMs are managers - they do not exist to bestow divine favour upon mortals. If an immortal helps you level or gives you powerfull items please report it to the immortal staff.
* Harassing or abusing immortals is not allowed.
* You are not allowed to spam. This means you are not allowed to flood any channel with messages. This includes any channel you can think of and even more (the immortal spam detection channel). We call something spamming when it starts to bother players (mortal or immortal).
* Changes to these rules are at the discretion of the Council, and may be done without warning, notice, or even evidence of sanity. Walk softly, and carry a big stick.

What is multiplaying
--------------------

Multiplaying is playing with multiple characters online at once. You may not have more than three characters online at once, this includes link dead characters. Sometimes it is hard to check is people are multiplaying or using a gateway. If you are using a gateway, and are accused of multiplaying please state so directly to the immortal who is asking the questions. If the immortal asks the question multiple times, please answer multiple times.

What is Out of Level interaction
--------------------------------

Out of Level (OOL) means grouping, batteling with or helping characters that are about 10 levels lower than you. OOL is limited on Fatal Dimensions.

The rules are:

* Grouping will be limited by the code. You may (and con) not group 10 levels below or above your level.
* Powerleveling is not allowed. Powerleveling is helping a character of your own or of somebody else with the intension of leveling them as quick as possible. So doing more then just helping. When someone thinks a player is powerleveling he should notify the council, an immortal should log both the powerleveler and the one who is helping with the powerleveling. The council will decide if it is powerleveling according to them. Both the powerleveler and the player that is helping will get a penalty.

Use COMMON SENSE when coming close to crossing the line. Look at [when should you give what penaltie](#when-should-you-use-what-penalty) for information about the penalties for breaking of this rule.

This was instituted in the name of fairness because not everyone has a high level charactter that they can rely on.

Accepted at any level are, kindness, gathering of equipment, corpse retrievals and advise.

PK Rules
--------

Player killing is allowed only in specific areas only. Like the real all worlds, some places are dangerous. And you must be careful where you go. But, all Pk zones must be posted carefuly so that no player may wander into them accidentally. the Pk rules, briefly:

1. PK in Pk areas only.
2. NO summoning players into PK areas.
3. No looting. Corpses willl be donated.

* No xp loss or gain.
* No KILLER flag wil be given in designated areas.

PK Rules are in the rules section number 6. So summoning players into a PK areay is breaking rule 6.2.

R2: Immortal Rules, Laws and Rights
===================================

These are the immortal rules, laws and rights of Fatal Dimensions.

1. Do not kill mortals, do not kill mortals, do not kill mortals. This includes transferring/summoning them to aggressive mobs or switching to agressive mobs.
2. Do not cheat for ANY mortal in ANY way, especially your own. Unless of course you LIKE deletion. Yes this includes tanking, healing/restoring, casting protection spells <armor, sanct>, and softening up or altering mobs.
3. Do not transfer or summon mobs to mortals or mortals to mobs. They have legs, if they want to kill a mob, they can easilly go there themselves.
4. DO NOT kill the shopkeepers for any reason at ALL, unless you are a builder with permission to alter the shopkeeper and need to reload him.
5. DO NOT use reboot. <--- Note, simple English, only council members may use reboot.
6. Do not give out free equipment. You don't need to win brownie points with the mortals. Quest are fine but handing out items isn't.
7. Do not undermine the authority of a higher level god. If you see that someone has been frozen or nochannelled do not restore their priveleges. They are being punished for a reason.
8. Do not try to overrule each other, the final disision is done by the council.
9. Trusted mortals: Do not use your god powers to help your mortal character. If you do, your trust will be taken away.
10. Do only things that you are allowed to do according to your job. So if you are not a questor, don't give quests. If you don't have the job of newbie-helper, don't help any newbies, whatever happens. If you don't have a job assigned, you may only chat.
11. See [Rewarding players](#rewarding-players) for the rules how to reward mortal players who help immortals with testing code, mobs or areas.
12. Immortals may have mortal players that are in a clan as long those members are not clan leaders.
13. When making big changes that could have negative effects on players, the involved immortals must try to notify the players as early as possible.
14. You may not break any of the above rules. NEVER!

Rewarding Players
-----------------

When mortal players help an immortal with testing code, an mob or an area, etc. They may get one of the following rewards:

1. When testing a new area, giving an item from the area. A normal item, not an unbalanced one.
2. Giving the player a t-shirt (with no armor bonuses at all).
3. Giving 100gp
4. Giving 5qp

If you give another reward please notify the council, they will then decide if that reward is allowed.

R3: Naming Rules
================

1. Confusing whonames like `[ 1 Human War]` are not allowed. Players, specially newbies, won't understand why such a player can do all those things.
2. Whonames may not contain more than 14 positions.
3. The whoname should reflect the job of the immortal (if immortal). Most whonames are given in the job descriptions. Hero's may choose a whoname.
4. Only council members may change the whonames.
5. Only players with level 91 or higher may have whonames. This includes Hero's.
6. Sexual oriented names, whonames, titles or afk titles are not allowed. This includes names like Sexlover or VerlagSux.

Being immortalized and deimmortalized
=====================================

Becomming an immortal
---------------------

* When you are immortalized a backup of your mortal char will be made.
* There should be more time spend in assigning new immortals. The basic commands and rules shouls be explained right after the player is immortalized.

Becomming deimmortalized
------------------------

When you don't want to be an immortal anymore, or when the council decides you shouldn't be an immortal anymore or when you havn't send in your repport for two months, you will be deimmortalized. This is done in the following way:

* If we have a backup file of your old mortal char it will be put back.
* If for some reason no backup should exist, your immortal char will be deleted.

What to do if you see someone breaking the rules or cheating
============================================================

Mortals
-------

* If you see someone cheat, you should report it to an immortal by telling, noting or emailing him or her.
* When an immortal cheats, report it to the council.
* When a council member cheats, report it to the Head Council Member (Fatal).
* When the Head Council Member cheats, report it to the council.

Immortals
---------

* If you see someone cheat, mortal or immortal put a log on him or her and note or email the council about it. You must enter the time and date of when it happened and what rule is broken.
* If the cheater becomes really agressive and disturbs the rest of the mud you must do the following:
    * Log yourself
    * While logged give the person at least two warnings.
    * After 5 minutes you may take actions, by giving penelties.
    * Don't remove the logs, this must be done by the Head Council Member. Removing the log in not allowed.
    * Note or email the council about it.
* If you can't log the person, you are not allowed to act.
* See [penalties system](#penalty-system) for what penelties you are allowed to give.

R4: Rules about creating a clan
===============================

Some question has been raised as to how to start a new clan, or take control of an existing one.

To start your own clan you must have the following:

1. A leader who is above level 50.
2. Active interest in starting the clan.
3. The 1250 diamond startup fee. (For 1 Clan Room, 110/110 healing, and clan recall), To reinstate a clan that has been disbanded by rule R5.3, it costs only 1000gp.
4. Immortal approval from the Clan Overseers or Implementors.
5 .At least 4 clan members. (All played by different players)

To take over a clan you must do the same. The existing clan hall will be striped until it is a standard clan hall. If you want to take over the clan as it is you need to ask the Clan Overseer how much it costs. The clan overseer will determine the price of the complete clan hall. It is only possible to take over a clan who's Leaders haven't been online for 2 months.

R5: General clan rules
======================

1. A clan may have no more then 2 clan leaders. If there are more then 2 clan leaders, the clan overseer has the right to demote clan leaders to masters until there are only 2 clan leaders.
2. A clan has to have at least a minimum of 4 active players (not characters). With the new system to remove inactive players that Leaders have they need to regularly police their clans and make sure that they have enough active members.
3. If a current clan goes under the 4 person requirement, they will get a 30 day notice that they need to bring the active total up or be disbanded, from the Clan Overseer.  If any clan that is active falls under the 4 active members for more then the 30 day grace period the clan is then disbanded and a new charge be required to reactivate the clan.

R6: Sistermuds
==============

The council has set the following guidelines for sistermuds:

1. A sister mud should have a decent player base.
2. The runners of the sistermuds must have good knowledge about coding and running muds so ideas can be exchanged.
3. The mud should have a good quality.
4. The mud should be willing add intermud chat to their mud and linking that way to Fatal Dimensions.

Lost Equipment Policy
=====================

When a player has lost equipment because of an crash he can ask the Head Council Member (Fatal) to restore the last backup of the character from before the crash. (Email him if it needs to be done quick). The player should give the date and time of the crash. The Head Council Member will check if there was a crash and if the player was online at that time. Furthermore the player should have been killed within 30 minutes before the crash. This means that it is not possible to get a disarmed weapon back after a crash. The player could have lost it anytime. (ie. not because of the crash)
