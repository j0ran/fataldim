---
title: "General information about Fatal Dimensions"
date: 2019-10-29T21:51:21+01:00
menu:
    main:
      name: general
      parent: info
      weight: 10
---
* [The Fatal Dimensions Mud Server]({{<ref server>}}) information and picture.
* Read here how the mud is run in the [Design Document]({{<ref design-document>}}). If you want to become an immortal on Fatal Dimensions you need to read this document. It is also a good way for players to find out how things work on Fatal Dimensions.
* Take a look the mudconnector entry of Fatal Dimensions.
* [Mud FAQ](https://www.mudconnect.com/mudfaq/), All the answers on the questions you could ask about mud's in general.

How to become an immortal
-------------------------

To become an immortal you must do the following things..

1. Become familiair with Fatal Dimensions as an mud. So even if you are a great builder on another mud, we still want you to play a bit in Fatal Dimensions, learn her features and some of the new non-stock areas.
2. Read the [Design Document]({{<ref design-document>}}). This documents tells you about how the mud is run and what jobs you could apply for. You may always give comments about this document when you think it is incorrect or not complete.
3. If you want to become a builder, you should have a talk with the Head Builder first.
4. Then you can apply for the job by sending an email to the [Fatal Dimensions Council](mailto:council@fataldimensions.nl).
5. Every month the council has a council meeting, during this meeting the council members decide if they are going to hire you.

Information Services
--------------------

* See who is online by looking at the [Fatal Dimensions Who List]({{<ref who-is-online>}}).
* Read the [Fatal Dimensions Online Help]({{<ref help>}}) here on the website.
