---
title: "Fatal Dimensions Wizlist"
date: 2019-11-10T14:26:21+01:00
menu:
    main:
      name: wizlist
      parent: info
      weight: 50
---
The wizlist gives a list of all active immortals on Fatal Dimensions.

{{<help "wizlist">}}
