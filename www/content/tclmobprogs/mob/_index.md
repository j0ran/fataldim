---
title: Mob commands
hidden: true
weight: 60
menu:
    main:
      parent: building
---
Default the mob command will use the mob that is running the
mobprog. Directly after the mob command you can use switches to
search for another mob. It duplicates some of the char commands to
make writing mobprogs easier.

```tcl
mob ?-mob? ?-target? ?-second? ?-last? ?-find <charid>? ?-set <slot>? ?-get <slot>? <option> ?<arg> <arg> ...?
```

Switches: 

| Switch           | Description
| ---------------- | -----------
| `-mob`           | Execute command on the mob that is running the mobprog (default)
| `-target`        | The remembered mob. (Doesn't work if it is a char)
| `-last`          | Command will be done on the mob that did a command last time
| `-second`        | If secondary char is a mob, this switch can be used to access this char.
| `-find <charid>` | Execute the command on the mob with the given &lt;charid&gt;
| `-set <slot>`    | Store the resulting char/mob in a memory slot. (0<=slot<=4) Slots are reset when the trigger ends
| `-get <slot>`    | Recall a char/mob from a memory slot.
