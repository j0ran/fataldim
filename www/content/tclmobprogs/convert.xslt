<?xml version="1.0"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes" omit-xml-declaration="yes" />

    <xsl:template match="/tclmobprogs">
        <xsl:apply-templates select="command"/>
    </xsl:template>

    <xsl:template match="command">
        <h1 class="command">
          <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
          <xsl:value-of select="@name"/> Command Reference
        </h1>
        <ul class="reftoc">
            <xsl:for-each select="option">
                <xsl:sort select="@name"/>
                <li>
                    <a>
                        <xsl:attribute name="href">#<xsl:value-of select="ancestor::command/@name"/>_<xsl:value-of select="@name"/></xsl:attribute>
                        <xsl:value-of select="@name"/>
                    </a>
                </li>
            </xsl:for-each>
        </ul>
        <xsl:for-each select="option">
            <xsl:sort select="@name"/>
            <xsl:apply-templates/>
            <xsl:text disable-output-escaping="yes"><![CDATA[{{<usernotes key="tclmobprogs/]]></xsl:text><xsl:value-of select="ancestor::command/@name"/>/<xsl:value-of select="@name"/><xsl:text disable-output-escaping="yes"><![CDATA[">}}]]></xsl:text>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="proto | trigger">
        <h2 class="proto">
            <xsl:attribute name="id"><xsl:value-of select="ancestor::command/@name"/>_<xsl:value-of select="parent::option/@name"/></xsl:attribute>
            <xsl:apply-templates/>
        </h2>
    </xsl:template>

    <xsl:template match="param">
        <span>
            <xsl:attribute name="class">param<xsl:if test="@optional"> opt</xsl:if><xsl:if test="@literal"> lit</xsl:if></xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="description">
        <div class="desc">
            <p><xsl:apply-templates/></p>
        </div>
    </xsl:template>

    <xsl:template match="p | P">
        <xsl:text disable-output-escaping="yes"><![CDATA[</p><p>]]></xsl:text>
    </xsl:template>

    <xsl:template match="ref">
      <a>
        <xsl:attribute name="href">#<xsl:value-of select="@name"/>_<xsl:value-of select="@option"/></xsl:attribute>
        <xsl:apply-templates/>
      </a>
    </xsl:template>

    <xsl:template match="list">
        <dl>
            <xsl:apply-templates/>
        </dl>
    </xsl:template>

    <xsl:template match="item">
        <dt><xsl:value-of select="@name"/></dt>
        <dd><xsl:apply-templates/></dd>
    </xsl:template>

    <xsl:template match="example">
        <div class="example">            
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="code">
        <xsl:text disable-output-escaping="yes"><![CDATA[{{<highlight tcl>}}]]></xsl:text>
        <xsl:apply-templates/>
        <xsl:text disable-output-escaping="yes"><![CDATA[{{</highlight>}}]]></xsl:text>
    </xsl:template>

    <xsl:template match="value">
        <div class="trig-value">
            <span class="field">Value: </span>
            <span class="value"><xsl:apply-templates/></span>
        </div>
    </xsl:template>

    <xsl:template match="defaultchar">
        <div class="trig-defaultchar">
            <span class="field">Default Char: </span>
            <span class="value"><xsl:apply-templates/></span>
        </div>
    </xsl:template>

    <xsl:template match="*">
        [Missing]
    </xsl:template>
</xsl:stylesheet>
