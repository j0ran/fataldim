---
title: Object Commands
hidden: true
weight: 80
menu:
    main:
      parent: building
---
This command can be used to get information about an object. Default the commands will work on the object that
was used in the trigger.

```tcl
obj ?-find <objid>? ?-last? ?-obj? ?-second? ?-set <slot>? ?-get <slot>? <option> ?<arg> <arg> ...?
```

Switches:

<TABLE BORDER="1" WIDTH="100%">
	<TR>
		<TD WIDTH="19%"><TT>-find &lt;objid&gt;</TT></TD>
		<TD WIDTH="81%">Search for the object with this object id. </TD>
	</TR>
	<TR>
		<TD WIDTH="19%"><TT>-obj</TT></TD>
		<TD WIDTH="81%">Unused </TD>
	</TR>
	<TR>
		<TD WIDTH="19%"><TT>-second</TT></TD>
		<TD WIDTH="81%">Secondary object of the trigger</TD>
	</TR>
	<TR>
		<TD WIDTH="19%"><TT>-last</TT></TD>
		<TD WIDTH="81%">Last object referenced.</TD>
	</TR>
	<TR>
		<TD WIDTH="19%"><TT>-set &lt;slot&gt;</TT></TD>
		<TD WIDTH="81%">Store the resulting obj in a memory slot. (0<=slot<=4) Slots are reset when the trigger ends</TD>
	</TR>
	<TR>
		<TD WIDTH="19%"><TT>-get &lt;slot&gt;</TT></TD>
		<TD WIDTH="81%">Recall an obj from a memory slot.</TD>
	</TR>
</TABLE>
<!-- $Id: tclmobprogs_obj.htm,v 1.2 2004/02/08 18:08:21 joost Exp $ -->
