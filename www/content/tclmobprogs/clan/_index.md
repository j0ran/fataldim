---
title: Clan Commands
hidden: true
weight: 130
menu:
    main:
      parent: building
---
This command can be used to get information about a clan. Default the commands will work on the character that was used in the trigger.

```tcl
clan ?-mob? ?-char? ?-target? ?-last? ?-second? ?-find <charid>? <option> ?<arg> <arg> ...?
```

Switches: 

<TABLE BORDER="1">
    <TR>
        <TD WIDTH="19%"><TT>-target</TT></TD>
        <TD WIDTH="81%">The remembered mob.</TD>
    </TR>
    <TR>
        <TD WIDTH="19%"><TT>-last</TT></TD>
        <TD WIDTH="81%">Command will be done on the mob that did a command last time</TD>
    </TR>
    <TR>
        <TD WIDTH="19%"><TT>-second</TT></TD>
        <TD WIDTH="81%">Use secondary char if available.</TD>
    </TR>
    <TR>
        <TD WIDTH="19%"><TT>-find &lt;charid&gt;</TT></TD>
        <TD WIDTH="81%">Execute the command on the mob with the given &lt;charid&gt;</TD>
    </TR>
    <TR>
            <TD WIDTH="19%"><TT>-char</TT></TD>
            <TD WIDTH="81%">Not used</TD>
    </TR>
</TABLE>

{{<usernotes key="tclmobprogs/trigger/clan/general" title="General remarks">}}
<!-- $Id: tclmobprogs_clan.htm,v 1.1 2001/12/10 09:42:53 mud Exp $ -->
