---
title: Special Functions
hidden: true
weight: 160
menu:
    main:
      parent: building
---
These are functions which can be used inside mobprograms, but aren't
related to a special piece of the mud. They are defined in the file
rom.tcl in the area-directory.
<!-- $Id: tclmobprogs_rom.htm,v 1.1 2001/12/10 09:42:54 mud Exp $ -->
