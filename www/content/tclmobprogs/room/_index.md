---
title: Room Commands
hidden: true
weight: 110
menu:
    main:
      parent: building
---
The room will always be viewed in perspective of a char. Default
this is the default mob. With the switches you can view the room
in the perspective of another char. The room chosen will be the
room the char is in. The only way of selecting a room the char is
not in is using the <B>-vnum</B> option. Here the chosen char will
be used the view the given room. The <B>-last</B> switch will return
the last char used for the room commando and the last room.

```tcl
room ?-mob? ?-char? ?-target? ?-last? ?-second? ?-find <charid>? ?-vnum <vnum>? ?-object <objectid>? <option> ?<arg> <arg> ...?
```
<!-- $Id: tclmobprogs_room.htm,v 1.1 2001/12/10 09:42:54 mud Exp $ -->
