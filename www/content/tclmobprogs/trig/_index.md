---
title: Trigger Commands
hidden: true
weight: 120
menu:
    main:
      parent: building
---
Triggers are functions which select if a trigger is triggered. For
example, a trigger with "trig true" is always triggered, a trigger
with "trig chance 25" has a 25% chance of being triggered.

<!-- $Id: tclmobprogs_trig.htm,v 1.1 2001/12/10 09:42:54 mud Exp $ -->
