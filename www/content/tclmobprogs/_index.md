---
title: Introduction
hidden: true
weight: 10
menu:
    main:
      parent: building
---
Written by [Joran Jessurun](mailto:fatal@fataldimensions.nl) (Fatal)
for [Fatal Dimensions](http://www.fataldimensions.nl).

A mob can be associated with one mobprog. This is done in
the mob editor with the procinstance command. When a mobile is
created, this program will be executed. When a mob is extracted
(purged / removed from game) all variables and procedures created
by the mobprog will be deleted.

A mobprog will normally contain some definitions for procedures
(using the TCL proc command) and at least one call to the addtrigger
command. The addtrigger command will let the mob react on certain
events by calling a mobprog.

A small example for a mobprog:

```tcl
proc greetplayer {} {
    ! say "Hello [char short], how are you today?"
    ! tip [char]
}

addtrigger 1 greet {greetplayer} {trig true}
```

This small mobprogam will be executed when the mob associated with
this mobprogam is loaded (with load mob command or a reset). It will
define a procedure that greets the default character and tipping
its hat to him/her. Finally it will add a greet trigger. The name
of that trigger is 1. This can be used to delete the trigger
dynamically (not often used). A greet trigger is a trigger that
will go off when a player enters the room. The last argument of a
trigger is an extra condition that should be valid before the
trigger executes. Here `{trig true}` is used, meaning that
this trigger will always occur. If it had been `{trig chance
40}` there only is a 40% chance the trigger will occur. If the
trigger fires it executes the `{greetplayer}` command,
defined earlier.


{{<usernotes key="intro/quick" title="Quick Intro">}}

Default Command Target
======================

When a trigger executes the mob, char, room and obj command will
refer to the default.

* **mob** - The mob command will normally always refer to the player
            mob that is executing the mobprogam.
* **char** - When the trigger involves a character, char will
    refer to that character. The greet trigger is an example of a
    trigger that involves a character.
* **!** - The ! command is for executing mud commands. As an
    default the ! command will always use the mob that is executing
    the mobprog.
* **addtrigger** - The addtrigger should only be used outside
    procedure definitions and will always be executed on the mob that
    is running the mobprog.
* **room** - default the room will be the room that the mob
    that is running the mobprog is standing in.

The **mud** and **random** commands do not have any special
mud structures associated with them.

{{<usernotes key="intro/defaulttar" title="Default Command Target">}}

Search mechanisms
=================

charid's, objid's and locations can be used as parameters for
the commands. The following paragraphs will describe the format of
these identifiers.

The charid
----------

A charid is a way to identify characters in the mud. This can
be one player or one or more mobiles. The charid has the following
form:

    [options:]<argument>

With the options you can specify how the search must be done.
The options are a list of characters. Allowed characters are:

| Char  | Desciption
|-------|-----------
| **g** | Search globally. Default all search actions are only done in the current room. This option makes the system search the whole mud.
| **m** | Only find mobs.
| **p** | Only fins PCs.
| **v** | Use vnum to find a mob


The `<argument>` can be a name or an id (number). Every player
and mob have an unique id. This can be used to find that character.

A name can haver a prefix like this:
    
    2.janitor

This will search for the second janitor in the room.

Additionally you can access the memory slots by using `#<number>`, eg:

    #1

will retrieve the char/mod currenty stored in slot 1. See the -set and -get options
on the `char commands page`. You can't combine the 
`[options:]<argument>` syntax with the `#` syntax.

### Examples:

| Charid      | Description
|-------------|------------
| m:10        | Mob with id 10 in this room.
| gm:10       | The same mob, but search globally.
| p:123252424 | The player with the given id in current room
| v:3001      | The mob with vnum 3001 in current room
| Test        | The mob or player with name Test in current room
| mg:Test     | The mob with name test searched globally
| p:Fatal     | The player with name Fatal in current room
| mg:3.janitor| The third janitor in the mud
| #2          | The char/mob currently stored in slot 2

The objid
---------

The objid is used to find an instance of an object. It has the following form:

    [options:]<argument>

Option letters: 

| Char | Description
|-------|------------
| **v** | Use vnum to find the object.
| **t** | Use string as object type.

The argument can be a name or a number. A number indicates the
objects unique id. object id's will not be preserved after reboot.

Just as with chars/mobs you can use `#<number>` to access the memory slots.

### Examples:

| Objid      | Description
|------------|------------
| 100        | Search for object with id 100
| v:3001     | Search for object with vnum 3001
| t:weapon   | Search for a weapon
| t:3.weapon | Search for the third weapon
| test       | Search for the object named test
| #4         | Current obj in slot 4

The location
------------

The location is used to find a room in the mud. You can search
for room vnum or a mob or object that is in a room.

* `[options:]<name>` - Search for char or object.
* `[options:]<number>` - Search for char or object or vnum. (default vnum)

Option letters: 

| Char  | Description
|-------|------------
| **m** | Find only mobs
| **p** | Find only PC's
| **c** | Find mobs or PC's.
| **o** | Don't search for chars, but for objects

Normally when using a number it will be interpreted as a room vnum.
Using the options **m**, **p**, **c** or **o** it will
be interpreted as an id.

### Examples: 

| Location | Description
|----------|------------
| 3001     | Room with vnum 3001
| m:100    | Room with mob with id 100
| p:Fatal  | The room where Fatal is standing
| o:122    | The room which contains object with id 122. It will only find objects that are lying in the room.
