---
title: Room Triggers
hidden: true
weight: 40
menu:
    main:
      parent: building
---
RoomTrigger Command
-------------------

The roomtrigger command is defined as a separate command. 

```tcl
roomtrigger <name> <type> <call> [<trigger>]
```

Adds a trigger to the room. Use &lt;name&gt; to give the trigger
an unique name. Non-unique names will be rejected.  Also numbers
are allowed names. This name can be used to delete the trigger
later. Type is the trigger type, like "look", "enter" and so on.
Trigger types are described later. &lt;call&gt; is an TCL-script
that should be executed. Normally this will be the name of a
procedure to execute. The &lt;trigger&gt; gives the trigger condition.
This should be a TCL command (or script) that returns a boolean.
If the boolean is true, the &lt;call&gt; script will be executed.
If no trigger condition is given, the call script will always be executed.

The &lt;trigger&gt;-script may generate variables that then can
be used in the &lt;call&gt; script. The &lt;trigger&gt; script has
also access to all other functionality you would normally have in
a TCL-mobprog.

Trigger Types
-------------

Triggers are fairly easy to add, but this basic list should hold
for most needs. Their names, argument list syntaxes, and translation
into more articulate English are given below. Some triggers generate
a trigger value.  This value can be accessed with the <B>[trig]</B>
command or trough the <B>$::t</B> variable. The <B>trig </B>command
has a lot of routines for easy access to these trigger values.

{{<usernotes key="tclmobprogs/room_triggers/general" key="room triggers">}}

<!-- $Id: triggers_room.htm,v 1.2 2003/08/09 10:53:45 joost Exp $ -->
