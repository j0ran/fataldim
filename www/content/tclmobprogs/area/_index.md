---
title: Area Command
hidden: true
weight: 90
menu:
    main:
      parent: building
---
This command can be used to get information about the area.
Default the area where the (mobprog executing) mob is standing will
be used.

```tcl
area ?-vnum <vnum>? ?-last? ?-area? <option> ?<arg> <arg> ...?
```

Switches:

<TABLE BORDER="1" WIDTH="100%">
	<TR>
		<TD WIDTH="30%"><TT>-vnum &lt;vnum&gt;</TT></TD>
		<TD WIDTH="70%">The vnum of the area to use the command on </TD>
	</TR>
	<TR>
		<TD WIDTH="30%"><TT>-rvnum &lt;vnum&gt;</TT></TD>
		<TD WIDTH="70%" rowspan="3">Selects the area vnum falls into</TD>
	</TR>
	<TR>
		<TD WIDTH="30%"><TT>-ovnum &lt;vnum&gt;</TT></TD>
	</TR>
	<TR>
		<TD WIDTH="30%"><TT>-mvnum &lt;vnum&gt;</TT></TD>
	</TR>
	<TR>
		<TD WIDTH="30%"><TT>-last</TT></TD>
		<TD WIDTH="70%">Use the last area referenced. </TD>
	</TR>
	<TR>
		<TD WIDTH="30%"><TT>-area</TT></TD>
		<TD WIDTH="70%">Unused. </TD>
	</TR>
	<TR>
		<TD WIDTH="30%"><TT>-find &lt;charid&gt;</TT></TD>
		<TD WIDTH="70%">Use the area the mob or player is standing in. </TD>
	</TR>
	<TR>
		<TD WIDTH="30%"><TT>-char </TT></TD>
		<TD WIDTH="70%">&nbsp;</TD>
	</TR>
	<TR>
		<TD WIDTH="30%"><TT>-second </TT></TD>
		<TD WIDTH="70%">&nbsp;</TD>
	</TR>
	<TR>
		<TD WIDTH="30%"><TT>-mob </TT></TD>
		<TD WIDTH="70%">&nbsp;</TD>
	</TR>
	<TR>
		<TD WIDTH="30%"><TT>-target</TT></TD>
		<TD WIDTH="70%">&nbsp;</TD>
	</TR>
</TABLE>
<!-- $Id: tclmobprogs_area.htm,v 1.2 2004/05/31 17:14:25 joost Exp $ -->
