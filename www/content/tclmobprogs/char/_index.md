---
title: Char Commands
hidden: true
weight: 70
menu:
    main:
      parent: building
---
Commands that work on mobs characters and player characters. The default character is the character that caused the trigger.

```tcl
char ?-mob? ?-char? ?-target? ?-last? ?-second? ?-find <charid>? ?-set <slot>? ?-get <slot>? <option> ?<arg> <arg> ...?
```

Switches: 

<TABLE BORDER="1">
    <TR>
	<TD WIDTH="19%"><TT>-mob</TT></TD>
	<TD WIDTH="81%">Execute command on the mob that is running the mobprog (default)</TD>
    </TR>
    <TR>
	<TD WIDTH="19%"><TT>-target</TT></TD>
	<TD WIDTH="81%">The remembered mob. (Doesn't work if it is a char)</TD>
    </TR>
    <TR>
	<TD WIDTH="19%"><TT>-last</TT></TD>
	<TD WIDTH="81%">Command will be done on the mob that did a command last time</TD>
    </TR>
    <TR>
	<TD WIDTH="19%"><TT>-second</TT></TD>
	<TD WIDTH="81%">Use secondary char if available.</TD>
    </TR>
    <TR>
	<TD WIDTH="19%"><TT>-find &lt;charid&gt;</TT></TD>
	<TD WIDTH="81%">Execute the command on the mob with the given &lt;charid&gt;</TD>
    </TR>
    <TR>
	    <TD WIDTH="19%"><TT>-set &lt;slot&gt;</TT></TD>
	    <TD WIDTH="81%">Store the resulting char/mob in a memory slot. (0<=slot<=4) Slots are reset when the trigger ends</TD>
    </TR>
    <TR>
	    <TD WIDTH="19%"><TT>-get &lt;slot&gt;</TT></TD>
	    <TD WIDTH="81%">Recall a char/mob from a memory slot.</TD>
    </TR>
    <TR>
	    <TD WIDTH="19%"><TT>-char</TT></TD>
	    <TD WIDTH="81%">Not used</TD>
    </TR>
</TABLE>
<!-- $Id: tclmobprogs_char.htm,v 1.2 2004/02/08 18:08:21 joost Exp $ -->
