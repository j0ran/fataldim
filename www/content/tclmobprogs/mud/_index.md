---
title: Mod Commands
hidden: true
weight: 100
menu:
    main:
      parent: building
---
This command can be used to get information about the mud.

```tcl
mud <option> ?<arg> <arg> ...?
```

<!-- $Id: tclmobprogs_mud.htm,v 1.1 2001/12/10 09:42:53 mud Exp $ -->
