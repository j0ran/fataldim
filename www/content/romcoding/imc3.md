---
title: Fatal Dimensions - MavEtJu's rom coding part
subtitle: "Intermud-3"
date: 2001/12/12Y00:31:47
menu:
    main:
        name: intermud
        weight: 1
        parent: romcoding
---
After the second time I ran into problems with IMC2 because of
overwriting of pointers in memory (people don't like it when their
player-files get screwed up) and the fact that the IMC2 protocol
was not properly described, it was old and could use a looot of
enhancements to scale up back to a usefull protocol (sorry if I
hurt peoples feeling with this, but standing still is not good
these days) I decided it was time to step up to a new protocol.

Via via I had heard about Intermud-3 used on LPmuds and also found
the protocol description on the website of the Centre of Imaginary
Environments and also was able to compile and start an LPmud to I
could start reverse engineering. Oh well, at least I knew what was
going over the line, but that wasn't enough because they also send
some other data via the TCP connection, that is the length of the
packet. And I spend a full evening finding out what the 4 bytes in
front of it meant, mostly because I miscounted the amount of bytes
in the packet I was debugging...

Here it is, my own creatino for Intermud-3 support for ROM based
muds. Although the first release (2000/02/17) was marked as "tryout",
this version is working fine. How fine? We run it on Fatal Dimensions,
so we trust it enough :-)

That's all right now.

To honour the Open Source principle of "release early, release
often", you can find the first release of the Intermud3 source-code
here. It's tested on the FastRom code and it works. There is also
documentation on it, on how to install it and it's designed.

And if you're online, you can connect to a channel via:

    prompt> i3 setup imud_gossip igossip
    imud_gossip @ Nightmare is now locally known as igossip
    --> I3 - setup_channel: setting up to imud_gossip @ Nightmare as igossip

    prompt> i3 listen igossip
    You are now subscribed to imud_gossip (igossip @ Nightmare)
    --> I3 - check_channel: subscribing to igossip (imud_gossip @ Nightmare)

    prompt> igossip hello guys, another rom-based mud.
    [igossip] Blaat@Ploeps: hello guys, another rom-based mud.


* Read the [LICENSE](LICENSE)
* Read the [INSTALL](INSTALL)
* Read the [docs](fd_imc3.txt)
* Download the [code of 2000/02/17](fd_imc3-20000217.tar.gz)

If you have questions, feel free to ask them via email or on the
ROM mailing-list. Or via the *Add Note* button on bottom of this
paragraph.

{{<usernotes key="romcoding/IMC3comments" title="IMC3 comments">}}

<!-- Start of NedStat Basic code -->

<a href="http://nl.nedstatbasic.net/cgi-bin/viewstat?name=mavromimc3"><img
src="http://nl.nedstatbasic.net/cgi-bin/nedstat.gif?name=mavromimc3"      
border=0 alt="" nosave width=22 height=22></a>                            

<!-- End of NedStat Basic code -->
<!-- Start of ReferStat -->

<script type="text/javascript" language="JavaScript">
<!--                                                 
d=document;                                          
d.write("<img src=\"http://nl.nedstatbasic.net");                               
d.write("/cgi-bin/referstat.gif?");                               
d.write("name=mavromimc3&refer=");                               
d.write(escape(top.document.referrer));                               
d.write("\" width=1 height=1 align=\"right\">");                               
// -->                                                                         
</script>                                                                       

<!-- End of ReferStat -->
