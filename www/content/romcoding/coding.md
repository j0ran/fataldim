---
title: Fatal Dimensions - MavEtJu's rom coding part
subtitle: "Coding issues"
date: 2001/12/12Y00:31:47
menu:
    main:
        name: coding
        weight: 1
        parent: romcoding
---
make clean
----------

Rule number one of compiling C programs is *if you've changed something,
compile everything which might have been affected.*
Rule number two however states *compile only what has been affected.*
(You will find this out of you're working with large projects on
relative slow computers).

My complete source-code now (decmber 1998) exists of about 85K
lines, a clean compile on my P6/300 MHz takes about 2 minutes. Way
too long to wait for, much too short to do something else in the
mean time. So I don't use clean makes every time, I make sure my
Makefile knows when to compile what by using *makedepend*.
The syntax for makedepend is `makedepend *.c` and it will
add this to your Makefile:

```makefile
# DO NOT DELETE

act_comm.o: /usr/include/sys/types.h /usr/include/sys/cdefs.h
act_comm.o: /usr/include/machine/ansi.h /usr/include/machine/types.h
act_comm.o: /usr/include/machine/endian.h /usr/include/sys/time.h
act_comm.o: /usr/include/time.h /usr/include/stdio.h /usr/include/string.h
act_comm.o: /usr/include/stdlib.h /usr/include/ctype.h
act_comm.o: /usr/include/runetype.h merc.h recycle.h tables.h lookup.h
act_comm.o: color.h interp.h</pre>
```

Next time you run makedepend it will remove everything below the
`# DO NOT DELETE` and re-create the lines. Next time you run
*make* it will only compile act_comm.c if one of the files in
/usr/include or merc.h, recycle.h, tables.h, lookup.h, color.h or
interp.h is touched.

makedepend is part of the X11-windowing-system, often located in
/usr/X11R6/bin/ or similair places. I'm not sure if there is a
package for it.

If you don't have makedepend
----------------------------

If you don't have *makedepend*, you can use *gcc* for it:

    Actually, if you have the latest gcc, -E and -MM option can do the same
    job, for example...

    depend:
        $(CC) -E -MM $(C_FILES) -I. > .depend

    -James Seng


CVS
---

CVS is a version control system, it allows you to ...

* Keep track on what changes are made and in case of problems backout to an earlier revision.
* Develop on multiple computers or with multiple people and still being sure to work with the latest versions.

Okay, this will probably not convince you of it. But have a look
at http://www.freebsd.org/cgi/cvsweb.cgi.
It's from the FreeBSD project and they keep track of all the changes
in their sources. If it is good enough for them, it should be good
enough for you too :-)

The CVS tree for this mud is also available on http://www.fataldimensions.org/cgi-edwin/cvsweb.
It's a little bit modified to not to display the source-code, only
the comments.

CVS is supported right out of the box for Linux and FreeBSD
environments, others should download and compile rcs (rcs.5.7.tar.gz)
and cvs (cvs.1.9.tar.gz).
