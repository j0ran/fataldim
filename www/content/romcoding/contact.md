---
title: Fatal Dimensions - MavEtJu's rom coding part
subtitle: Contact
date: 2001/12/12T00:31:47
menu:
    main:
        name: contact
        weight: 1000
        parent: romcoding
---
Email
=====

I have three email addresses:

* mud-related mail: [mavetju@fataldimensions.org](mailto:mavetju@fataldimensions.org)
* private mail: [edwin@mavetju.org](mailto:edwin@mavetju.org)

Homepage
========

My homepage is at http://www.mavetju.org.

Mud
===

There is only one mud I can surely be found on is Fatal Dimension
(http://www.fataldimensions.nl)
which can be found on [telnet://mud.fataldimensions.org:4000](telnet://mud.fataldimensions.org:4000).
