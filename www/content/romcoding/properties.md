---
title: Fatal Dimensions - MavEtJu's rom coding part
subtitle: "Property System of Fatal Dimensions or 'keeping your mud scalable'"
date: 2001/12/12T00:31:47
menu:
    main:
        name: properties
        weight: 1
        parent: romcoding
---
This document describes the idea and setup of the Fatal Dimensions
Property System.

Introduction
------------

Development of new features to muds is often delayed due to
implementation of administrative tasks, for example:

* How to load the data from files into the memory
* How to store the data in the structures of the mud
* How to view them online
* How to modify them online
* How to save the data back to disk

With loading and saving there is often the problem of versionizing,
to keep track of not-yet-updated files.

The Property System of Fatal Dimensions is designed to overcome
these immediatly problems. It allows builders to add newly defined
features to the mud, even when the code for it is not implemented
yet in the mud. It saves and loads the properties from a standard
format. It displays the properties in a standard way. And it allows
properties based in the following formats: characters, integer,
longint, boolean and strings.

A property is a combination of a *key* and a *variable-type*,
for example "name" and "string", "level" and "integer", "holylight"
and "boolean".

Internal setup of the Property System of Fatal Dimenions
--------------------------------------------------------

Every item in the mud, i.e. room, chars and objects, and every item
in the mud-database (the database the mobs and objects are created
from) has the ability to have properties.

A property is an element of a linked list. If you want know the
value of a property for a room, the room-property-list is walked
through. If you want to know the value of property for a character
or an object, it first looks through the list in the actual object,
then through the list of the object the database.

```c
typedef struct  property_type           PROPERTY;
/*
 * PROPERTIES!
 * see property.h for the values of type
 */
struct  property_type {
    PROPERTY    *next;  /* next property */
    bool        valid;  /* for recycle */
    int         type;   
    char        *key;
    long        iValue;
    char        *sValue;
};

struct  char_data {
	:
	PROPERTY            *property;
	:

struct  mob_index_data {
	:
	PROPERTY            *property;
	:

struct  obj_index_data {
	:
	PROPERTY            *property;
	:

struct  obj_data {
	:
	PROPERTY            *property;
	:

struct  room_index_data {
	:
	PROPERTY            *property;
	:
```

When to use and when not to use
-------------------------------

Properties can be used best, because of the for non-frequent things.

For the coder
-------------

As interface for the coder, the following functions are available:

* <b>Set<i>XXXX</i>Property</b>: Set the value of a property.
* <b>Get<i>XXXX</i>Property</b>: Get the value of a property.
* <b>Del<i>XXXX</i>Property</b>: Delete a property.

*XXXX* has to be replaced with *Room*, *Object* or *Char*.

```c
int     SetRoomProperty(ROOM_INDEX_DATA *room,int type,char *key,void *value);
int     SetObjectProperty(OBJ_DATA *obj,int type,char *key,void *value);
int     SetCharProperty(CHAR_DATA *ch,int type,char *key,void *value);
int     SetDObjectProperty(OBJ_INDEX_DATA *obj,int type,char *key,void *value);
int     SetDCharProperty(MOB_INDEX_DATA *ch,int type,char *key,void *value);

int     GetRoomProperty(ROOM_INDEX_DATA *room,int object_type,char *key,void *value);
int     GetObjectProperty(OBJ_DATA *obj,int object_type,char *key,void *value);
int     GetCharProperty(CHAR_DATA *ch,int object_type,char *key,void *value);

int     DelRoomProperty(ROOM_INDEX_DATA *room,int object_type,char *key);
int     DelObjectProperty(OBJ_DATA *obj,int object_type,char *key);
int     DelCharProperty(CHAR_DATA *ch,int object_type,char *key);
int     DelDObjectProperty(OBJ_INDEX_DATA *obj,int object_type,char *key);
int     DelDCharProperty(MOB_INDEX_DATA *ch,int object_type,char *key);
```

### SetXXXXProperty
SetXXXXProperty() replaces or adds a new property
