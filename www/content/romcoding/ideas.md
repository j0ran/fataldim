---
title: Fatal Dimensions - MavEtJu's rom coding part
subtitle: "Ideas"
date: 2001/12/12Y00:31:47
menu:
    main:
        name: ideas
        weight: 1
        parent: romcoding
---
This section describes ideas for new spells, skills, commands and
other things which are implemented on Fatal Dimensions but not
(yet) available as patches.

Fighting
--------

### Choose a different target
During a fight it should be possible to concentrate your attention
on another victim (for example, a guard who just popped by while
you were fighting the big bad boss and who is now also poking in
you.

---

Groups
------

### Leader
To re-arrange a group-hierarchy, a new leader could be choosen by
the current leader.

### Leaving / entering rooms
When the leader of a group leaves the room, the rest should follow
him. When the leader of a group enters the room, the rest follows him:

```
NSU > 
Zaphod leaves south.
Krakeling follows Zaphod.

NSU > 
Zaphod has arrived.
Krakeling has arrived, following Zaphod.
```

---

Mobs
----

### Pets and their names
It should possible to give your pet a name when you buy it and this
name should be used for commands (the *order* command) and
instead of the normal pet-name (for example: *Bonky has arrived,
following Verlag* instead of *A puppy has arrived, following
Verlag*).

### Healer
Implement a **diagnose** which tells you how much bad you're
hurt and how much a complete heal would cost and a **heal all**
to get fixed completly.

### Shopkeepers
It is not a nice view if you see <i>You sell the blabla for **0**
gold and 1 silver coin**s**</i>

---

Skills
------

### Sharpen
If you implement a sharpen, keep in mind that you need a whet-stone
for this and that it isn't that sharp after a long time.

Spells
------

### Abort spell
If you're the caster of a spell on yourself, it should be possible
to abort that spell. Of course that spell could save.

### Healer
The healer should be able to remove *weaken*, *faerie fire*
and *chill touch*.

---

Immortal-commands
-----------------

### mwhere/owhere
mwhere/owhere should be able to be used with vnums instead of names:

```
none > mwhere 3001
  1) [ 3001] the baker                    [ 3009] The Bakery
```

---

Communication
-------------

### Notes
The recipient should be checked! If the player/group does not exist,
the note should not be able to be posted.
