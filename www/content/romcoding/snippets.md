---
title: Fatal Dimensions - MavEtJu's rom coding part
subtitle: "Snippets"
date: 2001/12/12T00:31:47
menu:
    main:
        name: snippits
        weight: 1
        parent: romcoding
---
Threaded DNS
------------

### Who needs this?
Everybody who is tired of the delay in the gethostbyname() function.

### What is it exactly?
When using a custom library for DNS lookups, the mud will no longer
lock up when it's trying to get the hostname by a certain IP address.
The hostname will be cached also, so next time a player logs in
from that address it will not do the lookup again.

### Files

* [LICENSE](/~edwin/rdns/LICENSE)
* [CHANGES](/~edwin/rdns/CHANGES)
* [INSTALL](/~edwin/rdns/INSTALL)
* [rdns2.tar.gz](/~edwin/rdns/rdns2.tar.gz)

Thanks to [Jodocus](mailto:bps@dse.nl) for the code!
{{<usernotes key="romcoding/RDNSComments" title="RDNS-cache comments">}}

---

Apache authentication module
----------------------------

### Who needs this?
If you want to use your player-database as authication source for
the Apache webserver.

### What is it exactly?
If you browse through the src-directory of Apache you see a directory
modules, with in it directories named as standard, proxy, extra
etc. There are the modules which allow you to authenticate yourself
with an standard userid/password, to use Apache as a proxy-server,
to show directories as if you're ftp-ing etc.

This module adds extra functionality to the authentication-part.
Normal authentication uses only userid/password and DBM authentication.
On http://modules.apache.org 
there is a huge repository of other authentication modules (for
example, SMB authentication, Oracle database authentication etc).
Just another database-authentication :-)

### Disclaimer

* This module is build for Apache 1.3.4 and might or might not work for other versions.
* This module might or might not contain buffer-overflows, use at your own risk.

### Where to find it?

* Download it from this site: [mod_auth_rom.c](mod_auth_rom.c) 
* Download it from http://modules.apache.org, search for *rom*.
