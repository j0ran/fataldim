//
// addaffect.c as implemented on Fatal Dimensions
// Please read the manual on http://fataldimensions.nl.eu.org/~edwin/rom
// If you find an error, please contact us, via private email or via the
// rom-mailinglist.
//
// Please note that if you choose to use this snippet and it gives
// problems during compiling you should first try to find out yourself
// why, how, what and fix it yourself before mailing other people about
// it. thanks in advance :-)
//
// Edwin Groothuis aka MavEtJu
// http://fataldimensions.nl.eu.org/
// mavetju@chello.nl
//

void do_affect_set(CHAR_DATA *ch, char *argument) {
    char obj[MAX_INPUT_LENGTH];
    char loc[MAX_INPUT_LENGTH];
    char mod[MAX_INPUT_LENGTH];
    char cmd[MAX_INPUT_LENGTH];
    char action[MAX_INPUT_LENGTH];
    OBJ_DATA *pobj;
    int type=TO_OBJECT;
    AFFECT_DATA *pAf,*pAf_next;
    int bitvector=0;
    int loc_flag,value;

    smash_tilde( argument );
    argument = one_argument( argument, obj );
    argument = one_argument( argument, action );
    argument = one_argument( argument, loc );
    argument = one_argument( argument, mod );
    argument = one_argument( argument, cmd );

    if (!str_cmp(action,"add")) {
        if (loc[0]==0 || mod[0]==0 || !is_number(mod)) {
            send_to_char("Syntax: set aff <obj> add <loc> <#mod> [<obj|aff|imm|res|vuln> <flags>]\n\r",ch);
            send_to_char("Syntax: set aff <obj> del <aff#>\n\r",ch);
            send_to_char("Where:\n\r",ch);
            olc_help_flag_string(ch,"loc",apply_flags);

            send_to_char("{ymod{x: a number\n\r",ch);
            olc_help_flag_string(ch,"flags for aff",affect_flags);
            olc_help_flag_string(ch,"flags for imm",imm_flags);
            olc_help_flag_string(ch,"flags for res",res_flags);
            olc_help_flag_string(ch,"flags for vuln",vuln_flags);

            return;
        }

        if ((loc_flag=flag_value(apply_flags,loc))==NO_FLAG) {
            olc_help_flag_string(ch,"flags for apply",apply_flags);
            return;
        }

        if (cmd[0]) {
            if (!str_prefix(cmd,"affect")) {
                type=TO_AFFECTS;
                if ((bitvector=flag_value(affect_flags,argument))==NO_FLAG) {
                    olc_help_flag_string(ch,"flags for aff",affect_flags);
                    return;
                }
            } else if (!str_prefix(cmd,"object")) {
                type=TO_OBJECT;
                bitvector=0;
            } else if (!str_prefix(cmd,"immune")) {
                type=TO_IMMUNE;
                if ((bitvector=flag_value(imm_flags,argument))==NO_FLAG) {
                    olc_help_flag_string(ch,"flags for imm",imm_flags);
                    return;
                }
            } else if (!str_prefix(cmd,"resist")) {
                type=TO_RESIST;
                if ((bitvector=flag_value(res_flags,argument))==NO_FLAG) {
                    olc_help_flag_string(ch,"flags for res",res_flags);
                    return;
                }
            } else if (!str_prefix(cmd,"vulnerability")) {
                type=TO_VULN;
                if ((bitvector=flag_value(vuln_flags,argument))==NO_FLAG) {
                    olc_help_flag_string(ch,"flags for vuln",vuln_flags);
                    return;
                }
            } else {
                send_to_char("Unknown flag-type, try obj, aff, imm, res or vuln.\n\r",ch);
                return;
            }
        }

        if ( ( pobj = get_obj_world( ch, obj ) ) == NULL ) {
            send_to_char( "Nothing like that in heaven or earth.\n\r", ch );
            return;
        }

        pAf             =   new_affect();
        pAf->where      =   type;
        pAf->location   =   loc_flag;
        pAf->modifier   =   atoi( mod );
        pAf->level      =   pobj->level;
        pAf->type       =   0;
        pAf->duration   =   -1;
        pAf->bitvector  =   bitvector;
        pAf->next       =   pobj->affected;
        pobj->affected  =   pAf;

        send_to_char("Affect added.\n\r",ch);
        return;
    }

    if (!str_cmp(action,"del")) {
        if ( ( pobj = get_obj_world( ch, obj ) ) == NULL ) {
            send_to_char( "Nothing like that in heaven or earth.\n\r", ch );
            return;
        }

        if (loc[0]==0 || !is_number(loc) || (value=atoi(loc))<0 ) {
            do_affect_set(ch,"x add");
            return;
        }

        pAf=pobj->affected;
        if (!pAf) {
            send_to_char("Affect not found, sorry.\n\r",ch);
            return;
        }
        if (value==0) {
            pobj->affected=pAf->next;
            if (pAf->where==TO_OBJECT2 &&
                pAf->bitvector&(ITEM_ANTI_CLASS|ITEM_ANTI_RACE|
                                ITEM_CLASS_ONLY|ITEM_RACE_ONLY))
                pobj->extra_flags2&=~pAf->bitvector;
            sprintf_to_char(ch,"Removing %s.\n\r",
                affect_loc_name(pAf->location));
            free_affect(pAf);
        } else {
            while ((pAf_next=pAf->next)&&--value)
                pAf=pAf_next;
            if (pAf_next) {
                pAf->next = pAf_next->next;
                if (pAf->where==TO_OBJECT2 &&
                    pAf->bitvector&(ITEM_ANTI_CLASS|ITEM_ANTI_RACE|
                                    ITEM_CLASS_ONLY|ITEM_RACE_ONLY))
                    pobj->extra_flags2&=~pAf->bitvector;
                sprintf_to_char(ch,"Removing %s.\n\r",
                    affect_loc_name(pAf->location));
                free_affect(pAf_next);
            } else {
                send_to_char("Affect not found, sorry.\n\r",ch);
                return;
            }
        }

        return;
    }

    do_affect_set(ch,"x add");
}

