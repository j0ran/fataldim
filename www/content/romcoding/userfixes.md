---
title: Fatal Dimensions - MavEtJu's rom coding part
subtitle: "Fixes in player level"
date: 2001/12/12T00:31:47
menu:
    main:
        name: user fixes
        weight: 1
        parent: romcoding
---
Alignment changes
-----------------

In Rom2.4b6's fight.c/group_gain() function there is checked for
alignment changes, but only for the one who kills the mob, not for
the rest of the group:

*ch* and *_ch* is the one making the kill.<br/>
*gch* are the group members.

```c
for ( gch = ch->in_room->people; gch != NULL; gch = gch->next_in_room )
{
    OBJ_DATA *obj;
    OBJ_DATA *obj_next;

    if ( !is_same_group( gch, ch ) || IS_NPC(gch))
	continue;
    xp = xp_compute( gch, victim, group_levels );  
    sprintf( buf, "You receive %d experience points.\n\r", xp );
    send_to_char( buf, gch );
    gain_exp( gch, xp );

    for ( obj = _ch->carrying; obj != NULL; obj = obj_next )
    {
	obj_next = obj->next_content;
	if ( obj->wear_loc == WEAR_NONE )
	    continue;

	if ( ( IS_OBJ_STAT(obj, ITEM_ANTI_EVIL)    && IS_EVIL(_ch)    )
	||   ( IS_OBJ_STAT(obj, ITEM_ANTI_GOOD)    && IS_GOOD(_ch)    )
	||   ( IS_OBJ_STAT(obj, ITEM_ANTI_NEUTRAL) && IS_NEUTRAL(_ch) ) )
	{
	    act( "You are zapped by $p.", _ch, obj, NULL, TO_CHAR );
	    act( "$n is zapped by $p.",   _ch, obj, NULL, TO_ROOM );
	    obj_from_char( obj );
	    obj_to_room( obj, _ch->in_room );
	}
    }
}
```

### Fix
Replace all the _ch's with gch.

---

Blindness, sleeping and exits
-----------------------------

### Idea
When you're sleeping (or are blinded), you shouldn't get the exits
in your prompt but an other string.

### Patch
In comm.c/bust_a_prompt(), replace:

```c
doors[0] = '\0';
for (door = 0; door < 6; door++)
{
    if ((pexit = ch->in_room->exit[door]) != NULL
    &&  pexit ->u1.to_room != NULL
    &&  (can_see_room(ch,pexit->u1.to_room)
    ||   (IS_AFFECTED(ch,AFF_INFRARED) 
    &&    !IS_AFFECTED(ch,AFF_BLIND)))
    &&  !IS_SET(pexit->exit_info,EX_CLOSED))
    {
        found = TRUE;
        strcat(doors,dir_name[door]);
    }
}
if (!found)
    strcat(buf,"none");
sprintf(buf2,"%s",doors);
```

with

```c
doors[0] = '\0';
if (ch->position>POS_SLEEPING) {
    if (IS_AFFECTED(ch,AFF_BLIND) &&
	!IS_SET(ch->act,PLR_HOLYLIGHT)) {
	found=TRUE;
	strcpy(doors,"blinded");
    } else {
	for (door = 0; door < 6; door++) {
	    if ((pexit = ch->in_room->exit[door]) != NULL
	    &&  pexit ->to_room != NULL
	    &&  (can_see_room(ch,pexit->to_room)
	    ||   IS_AFFECTED(ch,AFF_INFRARED))
	    &&  !IS_AFFECTED(ch,AFF_BLIND)
	    &&  !IS_SET(pexit->exit_info,EX_CLOSED))
	    {
		found = TRUE;
		strcat(doors,dir_name[door]);
	    }
	}
    }
} else {
    strcpy(doors,"sleeping");
    found=TRUE;
}
if (!found)
    strcpy(doors,"none");
```

---

Blindness and assist
--------------------

### Problem
When you're invisible or mobs in a room are blinded, they still
"scream and attack" when trying to assist even while they can't
see you.

### Fix
In fights.c/check_assist(), replace

```c
if (IS_AWAKE(rch) && rch->fighting == NULL)
```

with

```c
if (IS_AWAKE(rch) && rch->fighting == NULL && can_see(rch,victim))
```

---

Rings and AC
------------

### Problem
When wearing a ring, the AC-value of it isn't calculated into the
AC of the player.

### Fix
In handler.c/apply_ac(), add

```patch
    switch ( iWear )
    {
    case WEAR_BODY:     return 3 * obj->value[type];
    case WEAR_HEAD:     return 2 * obj->value[type];
    case WEAR_LEGS:     return 2 * obj->value[type];
+   case WEAR_FINGER_L: return     obj->value[type];
+   case WEAR_FINGER_R: return     obj->value[type];
    case WEAR_FEET:     return     obj->value[type];
    case WEAR_HANDS:    return     obj->value[type];
```

### Note
This is a fix under discussion. You might, or might not, agree with
it. Some people say "*a ring doesn't do anything to protect*",
other people say that "*a ring might do a little of protection*".
Anyway, before you apply this fix, try to find out if there are
rings with AC bonus and try to keep them rare.

---

Rescueing of pets
-----------------

### Problem
When trying to rescue your pet, you get the message "*Doesn't need your help!*"

### Fix
In fight.c/do_rescue, replace this:

```patch
-   if ( !IS_NPC(ch) && IS_NPC(victim) )
+   // is_same_group is for checking that you can rescue your pet(s)
+   if ( !IS_NPC(ch) && IS_NPC(victim) && !is_same_group(ch,victim))
    {
        send_to_char( "Doesn't need your help!\n\r", ch );
        return;
    }
```
