---
title: Fatal Dimensions - MavEtJu's rom coding part
subtitle: "Troubleshooting"
date: 2001/12/12T00:31:47
menu:
    main:
        name: troubleshooting
        weight: 1
        parent: romcoding
---
Networking
----------

### Problem
Help! My mud won't start and it says: *bind: Address already in use*.

### Cause
An other process is/was already listening on the port your mud
wants to use.

### Tracking
First, check if your port is already in use. This can be done with
the *netstat* command:

```
[~] edwin@bw2> netstat -an | grep port
tcp        0      0  *.port                 *.*                    LISTEN
tcp        0      0  131.155.132.36.port    192.68.44.34.3559      ESTABLISHED
tcp        0      0  131.155.132.36.port    12.55.3.19.1025        TIME_WAIT
tcp        0      0  131.155.132.36.port    131.155.132.109.3597   FIN_WAIT2
```

The first line tells you there is a process listening on your
mud-port. The second line tells you that there is somebody connected
to that process. The third line is from a connection which was just
disconnected. The last line is from a connection which is just
terminated but not yet disconnected.

#### The LISTEN-state
If you see LISTEN, that means there is a process running on your
system which is accepting incoming TCP-connections. Check if you
can find it the easy way: look through the process-table of the
system to see anything which looks familair:


```
[~] edwin@bw2> ps auxw | less
USER       PID %CPU %MEM   VSZ  RSS  TT  STAT STARTED       TIME COMMAND
edwin     2812  0.0  2.2   252  652  p2  S+    1:48PM    0:00.08 less
root         1  0.0  0.1   484   12  ??  Is   16Dec98    0:00.72 /sbin/init --
root         2  0.0  0.1     0   12  ??  DL   16Dec98    5:01.48  (pagedaemon)
root         3  0.0  0.1     0   12  ??  DL   16Dec98    0:00.64  (vmdaemon)
root         4  0.0  0.1     0   12  ??  DL   16Dec98   43:48.37  (update)
somebody   200  0.0  0.1   444   12  p0- I    16Dec98    0:09.43 /bin/csh ./startup
somebody   205  0.0  0.0   444    0  p0- IW   -          0:00.00 (csh)
somebody 24916  0.7 23.1  8872 7040  ??  S    Sun10PM   64:33.14 ../src/rom port
edwin     2811  0.0  0.9   656  272  p2  R+    1:48PM    0:00.01 ps -auxw
```

On SunOS and AIX the command is *ps -ef* instead of *ps
auxw*. The *w* means: display the whole command, otherwise
it will stop at 80 characters. And on systems without *less*
you should use *more* or considering installing *less*.

We see our own processes (*ps* and *less*), some
system-processes (init, a handfull of daemons) and two with a
familair name: *startup* and *rom* and they're owned by
the user *somebody*. Next step is to find the person who owns
the userid *somebody* and ask him to stop fiddling around on
that port.

If the process doesn't show up with the *ps* command, then
you need bigger ammunition. *lsof* displays all the open files
on a system. Only problem is that you need root-privilege to get
this information, but you might be lucky.

First, check if you can start *lsof* from your path. If not,
try */sbin*, */usr/sbin*, */usr/local/bin* and
*/usr/local/sbin*. If not, check if you can read the man-page
of it. If still not, you should ask your sysadmin to install it
(ftp://vic.cc.purdue.edu/pub/tools/unix/lsof). If he won't, you're
totally out of luck. For the people who found it:

```
[~] edwin@bw2> /usr/local/sbin/lsof | less
COMMAND  PID  USER   FD   TYPE     DEVICE   SIZE/OFF  INODE NAME
bash    2769 edwin  txt   VREG   0,131077     389120 345642 /usr/local/bin/bash
bash    2769 edwin  txt   VREG   0,131077      69632 284166 /usr/libexec/ld.so
:
lsof    2878 edwin    3u  PIPE 0xf0692700      16384        ->0xf072a400
lsof    2878 edwin    6u  PIPE 0xf0783800      16384        ->0xf0781a00</pre>
Ouch... only processes of myself... again, but now as root:
<pre>[~] root@bw2> /usr/local/sbin/lsof | less
COMMAND     PID     USER   FD   TYPE     DEVICE   SIZE/OFF  INODE NAME
swapper       0     root  cwd   VDIR   0,131072        512      2 /
init          1     root  cwd   VDIR   0,131072        512      2 /
:
mymud      1763 somebody    4u  inet 0xf05e6700        0t0    TCP *:port (LISTEN)
```

If your version of *lsof* supports it, you can also do

```
[~] root@bw2> /usr/local/sbin/lsof -i :port | less
COMMAND   PID     USER   FD   TYPE     DEVICE SIZE/OFF INODE NAME
mymud   24916 somebody   4u  inet 0xf06dd900      0t0   TCP *:port (LISTEN)
```

Okay, and again you can contact that user *somebody* and ask
him to stop fiddling on that port.

#### Killing processes
If you find the ids of the processes fiddling on your port, you
can kill them friendly with *kill id* or use some pressure
with *kill -9 id*. Note that you need to be owner of the
process before you can do it.

#### FIN_WAIT2
If you want to know the technical background of the FIN_WAIT2
problem, checkout the documentation of Apache
(http://apache.org/docs/misc/fin_wait_2.html). If you have an
connection in FIN_WAIT2-state, you can't bind to that port again
until it is timed-out or closed. For some systems this is just 1
second (AIX) up to minutes (FreeBSD).
