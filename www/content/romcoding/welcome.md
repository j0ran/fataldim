---
title: Fatal Dimensions - MavEtJu's rom coding part
subtitle: Welcome to MavEtJu's RoM programming kingdom
date: 2001/12/12T00:31:47
menu:
    main:
        name: welcome
        weight: -10
        parent: romcoding
---
Disclaimer
----------

All the information on these pages is delivered as-is. If you are
not sure if you should read it or use it, then please leave now.
If you however have the guts to go on and read the stuff what is
here and decide to use the information, please note that you're
doing to completly voluntarely and that nobody from the Fatal
Dimension-crew can be hold responsible for everything which doesn't
work after you made the suggested changes.

General
-------

Fatal Dimensions is currently running on a P5/90MHz based PC with
64Mb of RAM and a standard NE2000 ethernet-card, capable of pushing
about 10 million bits per seconds into your general direction. The
system is powered by FreeBSD 3.4.

It is like an wigwam: no windows, no gates and apache inside :-)

Mud
---

* http://www.fataldimensions.nl
* [telnet://mud.fataldimensions.nl:4000](telnet://mud.fataldimensions.nl:4000)

Member of the
-------------

<center>
&quot;MERC/Derivative Programmers&quot; webring<br>
<a href="HTTP://www.webring.com/cgi-bin/webring?ring=merc;id=38;next" target="_top">[Next]</A>
<a href="HTTP://www.webring.com/cgi-bin/webring?ring=merc;list" target="_top">[Index]</A>
<a href="HTTP://www.webring.com/cgi-bin/webring?ring=merc;id=38;prev" target="_top">[Prev]</A>
<a href="HTTP://www.webring.com/cgi-bin/webring?ring=merc;home" target="_top">[Joining]</A>
</center>

<a HREF="http://www.nedstat.nl/cgi-bin/viewstat?name=mavetjurom" target="main"><img SRC="http://www.nedstat.nl/cgi-bin/nedstat.gif?name=mavetjurom" ALIGN="BOTTOM" BORDER="0"></a>
