---
title: Fatal Dimensions - MavEtJu's rom coding part
subtitle: "Memory-leaks"
date: 2001/12/12Y00:31:47
menu:
    main:
        name: memleak
        weight: 1
        parent: romcoding
---
This are my reported memory-leaks in the Rom24b6 distribution.

prompt
------

### Problem
When loading the field *Prom* from the player-file, the old
prompt is not freed.

### Patch
In save.c/fread_char(), replace

```c
KEYS( "Prompt",      ch->prompt,             fread_string( fp ) );
KEY ( "Prom",        ch->prompt,             fread_string( fp ) );
```

with

```c
KEYS( "Prompt",      ch->prompt,             fread_string( fp ) );
KEYS( "Prom",        ch->prompt,             fread_string( fp ) );
```

---

affects
-------

### Problem
When assigning an affect to a character/object, the memory allocated
for the affect isn't validated and thus never freed when the affect
is removed.

### Patch
In handler.c/affect_to_char() and affect_to_obj(), replace

```c
*paf_new            = *paf;
VALIDATE(paf);      /* in case we missed it when we set up paf */
```

with

```c
*paf_new            = *paf;
VALIDATE(paf<b>_new</b>);  /* in case we missed it when we set up paf */
```

---

IMC
---

### Problem
The strings for IMC-related tools are only allocated, never freed.

### Patch
In recycle.c/free_pcdata(), add the following statements:

```c
free_string(pcdata->rreply);
free_string(pcdata->rreply_name);
free_string(pcdata->ice_listen);
```

---

Clan/Race
---------

This one is reported by <a href="mailto:lordmage@erols.com">Chris Litchfield</a>

### Problem
When reading the Clan/Race of a player, memory is lost.

### Patch
In save.c/fread_char(), replace:

```c
KEY( "Clan", ch->clan, clan_lookup(fread_string(fp)));
```

and

```c
KEY( "Race", ch->race, race_lookup(fread_string(fp)));
```

with

```c
if ( !str_cmp( word, "Clan" ) ) {
     char *tmp = fread_string(fp);
     ch->clan = clan_lookup(tmp);
     free_string(tmp);
     fMatch = TRUE;
     break;
}
```

and

```c
if ( !str_cmp( word, "Race" ) ) {
     char *tmp = fread_string(fp);
     ch->race = race_lookup(tmp);
     free_string(tmp);
     fMatch = TRUE;
     break;
}
```
