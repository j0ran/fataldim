---
title: Fatal Dimensions - MavEtJu's rom coding part
subtitle: User snippets
date: 2001/12/12T00:31:47
menu:
    main:
        name: user snippits
        weight: 1
        parent: romcoding
---
Addaffect
---------

### Who needs this?
Everybody who wants to put spell-affects on objects which are permanent.

### Usage

<div style="border:1px solid black;padding:0.5em;background:#eee;font-family:monospace;">
none &gt; <b>set affect</b><br>
Syntax: set aff &lt;obj&gt; add &lt;loc&gt; &lt;#mod&gt; [&lt;obj|aff|imm|res|vuln&gt; &lt;flags>]<br>
Syntax: set aff &lt;obj&gt; del &lt;aff#><br>
Where:<br>
loc none str dex int wis con sex class level age height weight mana hp move gold exp ac hitroll damroll saving-para saving-rod saving-petri saving-breath saving-spell spell <br>
mod: a number<br>
flags for aff blind invisible detect_evil detect_invis detect_magic detect_hidden detect_good sanctuary faerie_fire infrared curse poison protect_evil protect_good sneak hide sleep charm flying pass_door haste calm plague weaken dark_vision berserk swim regeneration slow <br>
flags for imm summon charm magic weapon bash pierce slash fire cold lightning acid poison negative holy energy mental disease drowning light sound wood silver iron <br>
flags for res summon charm magic weapon bash pierce slash fire cold lightning acid poison negative holy energy mental disease drowning light sound wood silver iron <br>
flags for vuln summon charm magic weapon bash pierce slash fire cold lightning acid poison negative holy energy mental disease drowning light sound wood silver iron<br>
<br>
none &gt; <b>set affect flag add none 0 aff flying</b><br>
none &gt; <b>set affect flag add str 2</b><br>
none &gt; <b>stat obj flag</b><br>
Name(s): orange flag<br>
[...]<br>
Values: 0 0 0 0 0<br>
Extra description keywords: 'orange flag'<br>
Removable affect str by 2, level 0. <br>
Removable affect none by 0, level 0. Adds flying affect.<br>
<br>
none &gt; <b>set affect flag del 1</b><br>
none &gt; <b>stat obj flag</b><br>
Name(s): orange flag<br>
[...]<br>    
Values: 0 0 0 0 0<br>
Extra description keywords: 'orange flag'<br>
Removable affect str by 2, level 0. <br>
</div>


<h3>Warning</h3>
When removing affects, make sure they're removable (i.e. not from
the object itself but from enchantments, this function and so on).
I've solved it by modifying the <i>stat obj</i> command so that it
displays the difference between <i>static affects</i>, from the
object itself versus <i>removable affects</i>, from enchantments.

<pre style="border:1px solid black;padding:0.5em;background:#eee;font-family:monospace;">
none > <b>stat obj sword</b>
Name(s): sword Peppin
[...]
Damage noun is slash.
Weapons flags: frost sharp vorpal
Removable affect str by 2, level 50. 
Static affect none by 0, level 50. Adds extra anti_class object flag.
Static affect damroll by 3, level 50. 
Static affect hitroll by 3, level 50. 
</pre>

So the first affect (strengh) is removable, the rest isn't.

### Where to find it?

* Download it from this site: [addaffect.c](addaffect.c)
