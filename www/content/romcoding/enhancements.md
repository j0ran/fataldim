---
title: Fatal Dimensions - MavEtJu's rom coding part
subtitle: "Enhancements"
date: 2001/12/12Y00:31:47
menu:
    main:
        name: enhancements
        weight: 1
        parent: romcoding
---
sockets
-------

### What
The *socket* command doesn't display sockets for people who
didn't have entered their name yet.

### Patch
In act_wiz.c/do_socket(), replace

```c
if ( d->character != NULL && can_see( ch, d->character ) 
&& (arg[0] == '\0' || is_name(arg,d->character->name)
		   || (d->original && is_name(arg,d->original->name))))
{
```

with

```c
if (d->character==NULL) {
    sprintf( buf+strlen(buf) ,"[%3d %2d] nobody yet@%s\n\r",
	d->descriptor,
	d->connected,
	d->host
	);
} else if ( d->character != NULL && can_see( ch, d->character ) 
	&& (arg[0] == '\0' || is_name(arg,d->character->name)
                           || (d->original && is_name(arg,d->original->name))))
{
```

### New output

    none > sock
    [  8  1] nobody yet@gw-nl2.philips.com
    [  7  0] MavEtJu@gw-nl2.philips.com
    1 user

---

logging
-------

Log much much more regarding networking. Log when somebody makes
a tcp-connection, log when somebody enters an invalid name, an
invalid password, log when somebody tries to login while he's
online, log when somebody disconnects. Use wiznet to display all
information.

---

dnslock
-------

### NOTE!
This is one solution, a better solution can be found on the snippet
page.

### What
If your DNS server is acting wierd (broken, slow...) and a new user
logs in, the mud will freeze for about 45 seconds. In the mean
time, the user has logged off and on again, causing another freeze
and if this is repeated multiple times nobody will like it anymore.

### Solution
Implement a function like *dnslock* which prevents reverse
DNS lookups.

### Patch
In comm.c/init_descriptor(), replace:

```c
from = gethostbyaddr( (char *) &sock.sin_addr,
        sizeof(sock.sin_addr), AF_INET );
```

with

```c
if (!mud_status.dnslock)
    from = gethostbyaddr( (char *) &sock.sin_addr,
            sizeof(sock.sin_addr), AF_INET );
else
    from=NULL;
dnew->host = str_dup( from ? from->h_name : buf );
```
