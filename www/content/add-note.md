---
title: "It is not possible to Add Notes"
date: 2019-11-11T21:10:32+01:00
---
The original Fatal Dimensions website had the posibility for users to add notes to different sections of the website. This was in a time when features like this were not yet spammed by bots. This website still contains the original notes, but has disabled the posibility to add new notes. If you really want to add a note somewhere you can send an email to [council@fataldimensions.nl](mailto:council@fataldimensions.nl).