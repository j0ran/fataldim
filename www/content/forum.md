---
title: "Forum"
date: 2019-10-31T21:13:20+01:00
linktitle: forum
menu: main
weight: 40
---
The forum is not available anymore, but there is a [Facebook page](https://www.facebook.com/Fatal-Dimensions-MUD-188327687853154/) for Fatal Dimensions.
