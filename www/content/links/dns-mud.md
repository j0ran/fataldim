---
title: "DNS-mudding"
date: 2019-11-16T10:33:45+01:00
menu:
    main:
      name: dns mudding
      parent: links
      weight: 20
---
```console
[edwin@bw2:~] nslookup - hastur.rlyeh.net
Default Server:  hastur.rlyeh.net
Address:  193.215.252.2

> set querytype=txt
> set domain=adventure
> 1
Server:  hastur.rlyeh.net
Address:  193.215.252.2

1.adventure     text = "You are walking along a country road, you    \
                        spot a pizza lying in the middle of the road. \
                        You are ravenous with hunger. Do you (2) walk \
                        past, or (3) attack the poor helpless pizza?"
adventure       nameserver = hastur.rlyeh.net
hastur.rlyeh.net        internet address = 193.215.252.2
> 2
Server:  hastur.rlyeh.net
Address:  193.215.252.2

2.adventure     text = "You continue along the country road...       \
                        You come to a place you recognize, a pizza is \
                        lying in the middle of the road.              \
                        You are ravenous with hunger. Do you (2) walk \
                        past, or (3) attack the poor helpless pizza?"
adventure       nameserver = hastur.rlyeh.net
hastur.rlyeh.net        internet address = 193.215.252.2
> 3
Server:  hastur.rlyeh.net
Address:  193.215.252.2

3.adventure     text = "You attack the pizza, there is an epic battle. \
                        As you're an experienced adventurer, you see \
                        that this is a 1 HD pizza.  Should be easy to \
                        kill.  Type (4)."
adventure       nameserver = hastur.rlyeh.net
hastur.rlyeh.net        internet address = 193.215.252.2
> 4
Server:  hastur.rlyeh.net
Address:  193.215.252.2

4.adventure     text = "You miss the pizza. \
                        The pizza missed. \
                        You miss the pizza.  \
                        The pizza unbalanced you. \
                        You slash the pizza across the face.   \
                        The pizza spun you around.                  \
                        You slash the pizza lightly. \
                        Something happens. Type (5)."
adventure       nameserver = hastur.rlyeh.net
hastur.rlyeh.net        internet address = 193.215.252.2
> 5
Server:  hastur.rlyeh.net
Address:  193.215.252.2

5.adventure     text = "Something strange happens.  As you hit \
                        the pizza, it grows, absorbing energy \
                        from your hits. Type (7)"
adventure       nameserver = hastur.rlyeh.net
hastur.rlyeh.net        internet address = 193.215.252.2
> 7
Server:  hastur.rlyeh.net
Address:  193.215.252.2

7.adventure     text = "The pizza grows, and is now becoming \
                        very large.  Do you want to: (8) continue \
                        to harass the pizza, or (9) run away?"
adventure       nameserver = hastur.rlyeh.net
hastur.rlyeh.net        internet address = 193.215.252.2

Okay, now the story splits in two parts (hugely interactive!)
First the flee part:

> 9
Server:  hastur.rlyeh.net
Address:  193.215.252.2

9.adventure     text = "You run away, and live to run another day."
adventure       nameserver = hastur.rlyeh.net
hastur.rlyeh.net        internet address = 193.215.252.2

Wow, you've survived it... But what would have happened when you didn't flee and were stupid enough to continue fighting?:

> 8
Server:  hastur.rlyeh.net
Address:  193.215.252.2

8.adventure     text = "You missed the pizza. \
                        The pizza blasted you. \
                        You missed the pizza. \
                        The pizza blasts you, pulverizing a great deal of assorted bones. \
                        You feel dizzy. \
                        The pizza leaves up.  Type (11)"
adventure       nameserver = hastur.rlyeh.net
hastur.rlyeh.net        internet address = 193.215.252.2
> 11
Server:  hastur.rlyeh.net
Address:  193.215.252.2

11.adventure    text = "A shadow appears around your legs. \
                        As you bend down to study the strange phenomenon, \
                        you hear a curious whistling sound from above.  \
                        You look up, just in time to see the pizza, falling \
                        from the sky, crushing you to pieces.  Type (12)"
adventure       nameserver = hastur.rlyeh.net
hastur.rlyeh.net        internet address = 193.215.252.2
> 12
Server:  hastur.rlyeh.net
Address:  193.215.252.2

12.adventure    text = "You lie flat on the road, You are steaming in the \
                        sun, and your cheese is just starting to melt. \
                        You see an adventurer coming towards you on the road."
adventure       nameserver = hastur.rlyeh.net
hastur.rlyeh.net        internet address = 193.215.252.2

Real adventurers (if anybody still reading) have seen we missed number 6. But that was on purpose!

> 6
Server:  hastur.rlyeh.net
Address:  193.215.252.2

6.adventure     text = "Did I tell you to type 6? Huh? Did I?"
adventure       nameserver = hastur.rlyeh.net
hastur.rlyeh.net        internet address = 193.215.252.2
```

Enough for today, enjoy a real mud :-)

MavEtJu 
