---
title: "Usefull Links"
date: 2019-11-16T10:33:24+01:00
menu:
    main:
      identifier: the-links
      name: links
      parent: links
      weight: 10
---
* [The Mud Connector](http://www.mudconnect.com/), for all your Mud resources.
* [Mud FAQ](http://www.mudconnector.com/mudfaq/), All the answers on the questions you could ask about mud's in general.
* [![Kyndig's Online](kynbanner.gif)](http://www.kyndig.com) Kyndig's Online Text Game Resource Site at http://www.kyndig.com.
* [The Game Grotto](http://www.gamegrotto.com/), this link works again, but is it still about muds?
* [Mudlinks](http://mudlist.eorbit.net/~mudlist/) (was Doran's Mudlist), a very nice mudlist.
* [![Pueblo](pueblo-now.gif)](http://pueblo.sourceforge.net/) [Pueblo Mud Client](http://pueblo.sourceforge.net/), a very nice mud client.
* [KECs Home-Page](http://members.magnet.at/kec/english/index.htm), a mud tool for telnet users and a Rom24 graphical area designer.
* [![Get zMUD Now!](zmudnow_ani.gif)](http://www.zuggsoft.com/) <span style="color:red">Zugg's MUD Client</span>, my favourite mud client.

Award(s)
========

[![Award404](award404.gif)](http://www.plinko.net/404/)

[![ArcadeAward](thearcadeaward1.gif)](http://www.thearcades.com/)

Fatal Dimensions
----------------

* Runs on [Linode](https://www.linode.com/).
* Using [Docker](https://www.docker.com/).
* The website is generated with [Hugo](https://gohugo.io/) and is hosted using [Nginx](https://www.nginx.com/).
* The source code is hosted [here](https://bitbucket.org/j0ran/fataldim/src) on [Bitbucket](https://bitbucket.org).

Until 31st of May 2012 Fatal Dimensions
---------------------------------------

**was:**

[![FreeBSD](powerlogo.gif)](http://www.freebsd.org/) [![Apache](apache_pb.gif)](http://www.apache.org/)

**and used:**

[![PHP](php4zend-small.gif)](http://www.php.net/)

Other Links
===========

* [The 'homepage' of Frank Schaap (Fragment.nl)](http://fragment.nl/), he graduated on the performance of gender in online role playing games (MUD's).

Useless Links
=============

* A short article about [DNS-Mudding]({{<ref dns-mud>}}).
* DC Heroes in Fatal Dimensions? [Read on!](http://www.mlists.com/archives/dc-heroes/old/January/0034.html)
* [THE ASIAN - All the news. All the time.](http://www.the-asian.com/film/wa-suicide.htm)
