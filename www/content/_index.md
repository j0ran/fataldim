---
title: "Hello mudders, welcome to the Fatal Dimensions homepage"
linktitle: "home"
weight: 10
menu: "main"
---
Fatal Dimensions is a fantasy-oriented [MUD](https://en.wikipedia.org/wiki/MUD) where players of different classes can explore their ways through varied areas of wonder and imagination. Fatal Dimensions has a few stock areas so any newbie can feel quite at home in the beginning. We have a dedicated group of builders who regularly add new areas to the world. In Fatal Dimensions you can hack-and-slack your career but it is also fun and more rewarding to investigate and interact with the various mobs and environments that you may encounter. 

Explore for example, the following areas built by Fatal Dimensions building staff:

* Thieves Academy by Verlag: ever wanted to see how thieves get trained? Better not get backstabbed while you are there!
* Wander through the dangers of the Abyss by Honey and avoid Rutterskins, T-Rex and more exotic creatures...
* Join or fight Robin Hood in Moulder's Sherwood Forest.
* Engage with the wild and savage gods in Nicademus' Realm of the Ancients.
* Meet the wondrous people of Jonathan Swift's Gullivers Travels in Benedict's Balnibardi, Laputa or Glubdubdrib.
* And even more areas!

{{<usernotes key="main/intro">}}

Connect to Fatal Dimensions
----------------------------

The addres of the mud is: mud.fataldimensions.nl port 4000

Connect to Fatal Dimensions using telnet:
```sh
telnet mud.fataldimension.nl 4000
```

Or use use a client like [Pueblo](https://sourceforge.net/projects/pueblo/).

If you want to contact the staff of Fatal Dimensions, either login or mail us at council@fataldimensions.nl.


Vote for us at Mudconnector!
----------------------------

[Vote for Our Mud on TMC!](http://www.mudconnect.com/cgi-bin/vote_rank.cgi?mud=Fatal+Dimensions)

MudWorld Award of Excellence - 2nd Place
----------------------------------------

<center>![trohpy](/images/trophy-2.gif "MudWorld Award of Excellence Trophy - 2nd Place")</center>

The award is of the MudWorld Awards 2001 area plot story contest. Juries were Illuvatar of PhoenixMUD, Keolah of Rogue Winds, Michelle Thompson of the Art of Building and Molly O'Hara of 4 Dimensions. We have won 2nd place in the Category Gothic with the plot story of The Black Castle. So, don't hesitate do visit this area when you are around (beware of the dog, however).

Benedict

{{<usernotes key="main/mudworldaward">}}

Features
--------

* Started in August, 1996
* Based on Rom2.4b6
* Heavily modified code
* TCL based mob programs
* Pueblo enhanced
* A lot of new and original skills like summon corpse, cantrip, scare, water walk, eyepoke etc.
* Automated quests
* Regulary quests done by the immortals
* Extended clan system with ranks
* The posibility to conquer area's
* Easy character creation
* A new classes system is in the making
* Limited PK in arena's
* A lot of custom area's and enhanced stock area's
* Includes its own POP3 server to read the mud notes
* You own email address @fataldimensions.org, see help email.
* Includes its own HTTP server for up to date information about the mud
* Intermud-3 Communications towards other muds (IMC)
* The Loony Tunes Pinky and the Brain are available as mobs :)

{{<latestnews count="4">}}

{{<usernotes key="main/latestnews">}}

