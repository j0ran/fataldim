---
author: jodocus@fataldimensions.org
date: 2002-01-15 16:36:00
---
proc go_south {} {<br>
  ! Bye, I am going south (or east)<br>
  return true<br>
}<br>
roomtrigger 1 leave {go_south} {trig exits south east }<br>
