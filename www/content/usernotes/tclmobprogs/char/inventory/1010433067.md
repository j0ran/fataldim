---
author: Verlag@fataldimensions.org
date: 2002-01-07 20:51:00
---
<pre>proc spinnen {} {
  set inv [char carries -id 32033]
  if {[llength $inv] > 0} then {
    room echo The spinningwheel starts to rumble.
    char echo You throw some [color yellow]straw[color] on the spinningwheel, not knowing what to do with it otherwise.
    obj -find [ lindex $inv 0] extract
    char echoaround [char name] throws some [color yellow]straw[color] on the spinningwheel, not knowing what to do with it otherwise.
    room oload 32037
  } else {
    room echo The spinningwheel starts to rumble.
    room echo And stops after a little while.
  }
}
objtrigger 1 speech {spinnen} { trig words rumpelstiltskin }

(from FairyWorld)
</pre>