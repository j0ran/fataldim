---
author: mavetju@chello.nl
date: 2000-11-07 10:33:00
---
A small program to test all the options:
<pre>set descriptor 0

proc open {} {
    variable descriptor
    ! say opening
    set descriptor [data open smote]
    ! say $descriptor
}
proc close {} {
    variable descriptor
    ! say closing $descriptor
    data close $descriptor
    set descriptor 0
}
proc read {} {
    variable descriptor
    ! say reading $descriptor
    ! say [data read $descriptor]
}
proc info {} {
    variable descriptor
    ! say descriptor: $descriptor [data tell $descriptor] [data eof $descriptor]
}
proc forward {} {
    variable descriptor
    ! say forward
    data seek $descriptor 0 end
    info
}
proc rewind {} {
    variable descriptor
    ! say rewind
    data seek $descriptor 0 set
    info
}
addtrigger aa speech {open} {trig words open}
addtrigger ab speech {read} {trig words read}
addtrigger ac speech {info} {trig words info}
addtrigger ad speech {rewind} {trig words rewind}
addtrigger ae speech {forward} {trig words forward}
addtrigger af speech {close} {trig words close}
</pre>

Use "stat files" to see the status of the files.
