---
author: mavetju@chello.nl
date: 2000-10-29 14:32:00
---
A small example of how object-progs with the pre-triggers work.
This is for a container which can be (un)locked, opened, closed, worn and removed:

<pre>proc maybe {s} {
    if {[random 1 2]==1} {
        ! -char say $s: failed
        return false
    } else {
        ! -char say $s: ok
        return true
    }
}

proc pre_open {} { return [ maybe "pre_open" ] }
proc open {} { ! -char say open }

proc pre_close {} { return [ maybe "pre_close" ] }
proc close {} { ! -char say close }

proc pre_lock {} { return [ maybe "pre_lock" ] }
proc lock {} { ! -char say lock }

proc pre_unlock {} { return [ maybe "pre_unlock" ] }
proc unlock {} { ! -char say unlock }

proc pre_remove {} { return [ maybe "pre_remove" ] }
proc remove {} { ! -char say remove }

proc pre_wear {} { return [ maybe "pre_wear" ] }
proc wear {} { ! -char say wear }

objtrigger 1 pre_open {pre_open} {trig true}
objtrigger 2 open {open} {trig true}
objtrigger 3 pre_close {pre_close} {trig true}
objtrigger 4 close {close} {trig true}
objtrigger 5 pre_lock {pre_lock} {trig true}
objtrigger 6 lock {lock} {trig true}
objtrigger 7 pre_unlock {pre_unlock} {trig true}
objtrigger 8 unlock {unlock} {trig true}
objtrigger 9 pre_wear {pre_wear} {trig true}
objtrigger a wear {wear} {trig true}
objtrigger b pre_remove {pre_remove} {trig true}
objtrigger c remove {remove} {trig true}
</pre>

Load the object, load a key and do the things
you normall do with containers: unlock, open, wear, remove, close and lock :-)
