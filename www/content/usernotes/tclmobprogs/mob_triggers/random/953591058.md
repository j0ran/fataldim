---
author: mavetju@chello.nl
date: 2000-03-20 23:24:00
---
This trigger is only checked when the mob is in its default position or when the mob has the "ignore_position" act-flag.
