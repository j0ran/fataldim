---
author: Smokey@fataldimensions.org
date: 2003-02-15 00:58:00
---
This is one of the most simple programs:
The mob says hello -with charname- back.
<pre>
proc  greethello {} {
   ! say Hello [char] !
}

addtrigger 1 speech { greethello } {trig words hello }
</pre>