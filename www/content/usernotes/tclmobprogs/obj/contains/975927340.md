---
author: mavetju@chello.nl
date: 2000-12-04 11:55:00
---
To use this parameter:
<pre>
proc test {} {
    set inhoud [obj contains]
    for {set i 0} {$i<[llength $inhoud]} {incr i} {
	set objid [ lindex $inhoud $i]
        ! say $i $objid [ obj -find $objid name ]
    }
}

objtrigger 1 lookat {test} {trig true}
</pre>
