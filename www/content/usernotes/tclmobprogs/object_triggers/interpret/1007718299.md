---
author: aggreviate@fataldimensions.org
date: 2001-12-07 10:44:00
---
Note: For both pre_interpret and post_interpret triggers. If it returns false, the valid command is blocked. If it returns true, the valid command is executed. In both cases of returns, the procedure called up by the trigger will always be run.