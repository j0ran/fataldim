---
author: aggreviate@fataldimensions.org
date: 2001-10-05 10:19:00
---
Yeah it may be a bit of a disappointment when the mud crashes. Would it be possible to do a more frequent saving for hero quest status only in this case? Aggressive thief. I agree, it may kill newbies but how about setting it to be aggressive only to suitable level(s) eg 91. As for killing chars, we can have the heart warning whoever that holds it that s/he is now PK-able, and to avoid that, just drop the heart.