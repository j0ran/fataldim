import glob

for file in glob.glob("*.txt"):
    print(file)

    with open(file) as f:
        content = "".join(f.readlines())
    
    with open(file.replace(".txt", ".md"), 'w') as f:
        f.write("---\n")
        f.write("---\n")
        f.write(content)
