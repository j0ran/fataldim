---
title: "How to design an area for JoranMUD"
date: 2019-11-01T23:17:25+01:00
hidden: true
showtoc: true
menu:
    main:
      parent: building
---
**By Benedict**

*Noveber 19 1998*

This document describes how to go about making a design for JoranMUD, from the very first conception to the final document that needs to be delivered for final approval. It is not a manual on the actual building itself, although many aspects in this document obviously prepare for that work. 

CREDENTIALS
===========

This document has been written by Benedict (aka Henri Achten irl): Last known url that worked: http://www.ds.arch.tue.nl/General/staff/henri/.

The document draws from experience on JoranMUD (http://www.fataldimensions.nl), also known as Fatal Dimensions.

Feel free to use this document any way you like, as long as you keep it intact and make due references to the sources mentioned in the credentials. 

I. FIRST IDEA: WHAT TO DO
=========================

Every once in a while a MUD player can't help thinking: "Wouldn't it be nice if there were such and such an area?"

When this emotion catches you, and if it persists, then prepare for a few weeks of planning and designing your area! 

In this manual, I'll use my own design process for the area "Laputa" as illustration material, and quote regularly from JoranMUD so you can see for yourself how some things work out. 

I.1. What is an area?
---------------------

An area is a collection of rooms that together defines what might be called a territory within JoranMUD. Midgaard for example is an area, Old Thalos, New Thalos, and Forestol are also areas. Areas do not need to be cities. The Plains of the North is a wilderness north of Midgaard, as is the Holy Grove, and the Swamp. An area need not have a uniform size: there are small areas and large areas.

Usually, an area is centered on a particular theme or idea. An area therefore, is consistent throughout its design, which is why you can usually identify areas by their name: they are distinct from each other. 

* **An area has a distinct idea** that is executed throughout the whole design. If you have many more ideas, make more areas. If you have never done an area design before, start with a simple idea, and see if you like the work.

I.2. What is a room?
--------------------

A room is a space in an area. Contrary to its name, a room can be a field, open to every direction. It can even be completely air! (Just fly over Midgaard to see it). A room is where you get when you do a move commando: when you go east, you come into the 'room' east.

Rooms do not have set dimensions. A room can be quite small -as small as a toilet, for example - or quite large - a piece of the ocean. Also, going from one room to the next need not cover the same distance: it just depends on the level of detail you want to convey in the rooms. 

Example:

    --[1]--[2]--[3]--
            |
      [4]--[5]--[6]

Rooms `[1]` `[2]` `[3]` are a road. Room `[2]` connects to a house that has rooms `[4]` `[5]` `[6]`. The size of the house is much smaller than the road. Where the distance travelled from `[1]` > `[2]` could be half a mile (if that were your choice), the distance between `[4]` > `[5]` can be three yards (three meters). So it does not necessarily follow that when you could leave `[4]` in the north, you would end up in room `[1]`. Rather, you would have to make an additional room between `[1]` and `[2]`. 

* **Rooms can differ in size.** There is no uniform distance between rooms.

I.3. What are mobs?
-------------------

Anything you can kill, anything you can sell something to, buy something from, etc. are mobs. Mobs are the living creatures &ndash; except for players &ndash; provided for by the MUD through which you gain experience points. Examples are beastly fido, the healer, Grum, mudschool monster, cityguard, the major, etc. Mobs need to be designed when you want to have new mobs (the fun part of course is to add new mobs to the MUD rather than reloading existing ones) in your area.

* Mobs are the flavor of an area. They need to be consistent with the area and add to the experience.

I.4. What are objects?
----------------------

Anything you can hold, wear, wield, eat, drink, burn, use, etc. is an object in JoranMUD. Examples are standard issue dagger, city guard signet ring, fountain, Grum's scimitar, sleeping bag, etc. If you want to have new objects in your area (and what other point is there to make an area for?) then you have to design these objects as well.

* The Laputa area design has 47 rooms, 29 mobs, and over 78 objects.

I.5. The very start
-------------------

The design for Laputa started with the idea that it would be very nice if the floating island Laputa from Gulliver's Travels by Jonathan Swift would be incorporated in the MUD. Laputa fits very well in the overall theme of JoranMUD which focuses on fantasy, magic, wizardry, medieval and ancient times.

* **Does your idea fit in JoranMUD?** Just try to imagine if a player entering your area would notice a discrepancy or not.
* **Where do ideas come from?** It is no shame to try to work out a theme from a book, movie, comic, or whatever into an area design. It is actually a great way to get to know these original works, and a lot of creativity is required to build a good area design out of some text or whatever. Any book, environment and persons need to be adapted in some way to fit in the world of JoranMUD, but it is great fun to do. Original ideas also are very welcome, and just try to be as comprehensive and consistent as a novelist in the theme and idea for your area.

Getting started on an area design is a two step process.

*STEP 1*: The very first germ of the idea for an area should be written down in a couple of sentences, giving the gist of it.

*STEP 2*. When you have written down this idea, send it to the Head Builder immortal who's job it is to coordinate building. This is to note the Head Builder you have an idea, and to get a first reaction: good idea, bad idea, fit or no fit, yes-but, etc. When you are not sure how well an idea will be received, wait until you have the reaction! Do not waste time on an idea that might be rejected: just think up of a new one.

Here is my description of the very first idea. I simply started with a quote from the book.

> "The island Laputa is exactly circular; its diameter 7837 yards. It is 300 yards thick. The bottom, seen from below is one even regular plate of adamant, shooting up to the height of 200 yards. Above it lie the several minerals in their usual order, and over all is a coat of rich mould ten or twelve feet deep."

Then, I followed with a few ideas on the area:

"In Laputa, you should be able to get some non-standard equipment: therefore it will be low on swords." ... "The Laputeans are very good mathematicians, geometers and astronomers. This make them ideal to be clerics and mages rather than warriors." ... "In general, Laputeans are not aggressive, given to such introspection that they would not notice a fight taking place. However, when you touch their interests, they become very aware of you." ...

I.6. They like it!
------------------

The head builder likes the idea and has given an OK to proceed with the work. Congratulations. Now the real work starts, as well as section II.

II. MAKE A MAP AND DESCRIBE THE AREA
====================================

The most important part of the area is the collection of rooms that make it up. This is where a player will always pass through when he, she, or it is in your area. The area must be interesting without mobs. How to go about this?

Simple: Draw a map.

II.1. What is a map?
--------------------

A map is a set of numbered rooms connected with each other by lines. It can be drawn by hand or done in ASCII on your computer. It is easiest to start by hand, as in the beginning you'll be adding, removing, and shifting rooms regularly before you get it right. When you enter JoranMUD you get a map of Midgaard. Look at it to see how it is made.

Example: My map for the first level of Laputa

         ----[18]-[8]-[19]----
         |        /|         |
       [17]        |/       [20]
      ___|        [7]        |___
     |             |             |
    [16]           |            [21]
     |     [13]---[6]---[12]     |
     |             |             |
     |       /     |      /      |
    [1]----[2]----[3]----[4]----[5]
    /|            /|            /|
     |             |             |
     |     [14]---[9]---[15]     |
    [25]           |            [22]
     |____         |/         ___|
          |       [10]       |
         [24]      |        [23]
          |        |         |
          |_______[11]_______|
                   /

Each `[x]` is a room, the x standing for the number. The `---` and `|` between the `[x]` are the connections between the rooms. Up is north, down is south, left is west, and right is east. When there is a `/` under a `[x]`, this means down, and a `/` above a `[x],` this means you can go up from that room. In particular the up and down are tricky to draw in a plan. The rest is quite obvious I suppose.

So in this map, you can go down from rooms `[1]` `[5]` `[8]` `[11]` and you can go up from rooms `[2]` `[4]` `[7]` `[10]`. Also this map illustrates the text from anecdote 3 that Laputa is circular. The map is also circular (more or less).

II.2. Room descriptions
-----------------------

Obviously, the area map is intricately linked with the descriptions of the rooms. The room descriptions are the pieces of text you always see when you enter a room, whether there are monsters, objects, or other players or not. The room description not only shows what is there, it also is intended to convey atmosphere. Also, from the text it must become obvious which directions you can go to.

Let's give an example:

    [1] Entry to Laputa.
    You enter Laputa, the flying city, by a seat hanging from a steel cable. The cable disappears in an ingenious looking structure overhead. The entrance point locks on to a broad boulevard leading along the edge of the island from north to south. People can be seen taking a stroll along the edge of the wonderful island north and south. To the east another magnificent road stretches ahead.
    [north east south]
    =========================
    room: outside. sector: city

OK, here is how it works. The `[1]` corresponds with the map shown above. Each room has a name, which is shown in the MUD (just look at any room you are in). The next text, from "You enter Laputa..." to "To the east another magnificent road stretches ahead." is the room description.

* Never tell how a person should feel about a room unless it is painfully obvious. So, don't tell something like "The masses really cheer you up" when the player could have a really bad humor and think all this fuss is quite annoying.

The [north east south] I have included as a note from which directions you can go from this room. It helps to check whether the text of the description is complete, and also the other descriptions in section II.4.

To conclude, the "room: outside. sector: city" is used in the building to determine whether for example you can see the weather or not (outside, therefore yes), and if the terrain is easy to travel through (city, therefore yes).

* A player immediately notices when descriptions are copied in a sequence of rooms, as some builders like to do because it saves typing. Such sequences are tediously boring! Even when they are intended to convey the sense of repetition, as in a maze, I feel there should be some variation, however small. Reward the player who carefully reads the room descriptions.

II.3. Extra descriptions
------------------------

Nothing is so annoying than when you read in the room description "You enter Laputa, the flying city, by a seat hanging from a steel cable. The cable disappears in an ingenious looking structure overhead..." and you type "look seat", the system responds with "You do not see a seat". This is the point of extra descriptions, and they really enhance the experience of an area. Take care to note the keywords that curious players will examine in your descriptions, even the most unimportant ones. In the room of the example, the following extra descriptions are given:

    look seat:
    The seat is made of iron. The holes through which the cable goes are very nicely decorated with geometric figures. The seat is quite strong.
    look cable:
    Strong links joined to each other lead all the way up to the floating island in the air. The cable looks strong enough to lift an Elephant.
    look structure:
    Graceful arches support a slender roof. Someone has given a lot of thought of how a journey upward must make a fresh visitor feel.

* Extra descriptions really add to the area you have designed. It offers additional information to color your design. It also makes players feel rewarded when they discover all these details.

II.4. Descriptions between rooms
--------------------------------

Rooms often are open to each other (for example in a road), yet you cannot see beyond the room you are in. Is it not amazing that when you type "look east" you usually get the response by the system "Nothing special there."??? Why bother to go in that direction anyway then?

This is where the list [north east south] comes in handy. Systematically work through all possible directions and try to imagine what you would see when you were in that room looking in those directions. Take care to keep the description well balanced with the main description of the room: you don't want players to have to read in every direction when they are in your area. Just make sure there is something when they do want to however. When there is a secret door leading to another room, obviously you cannot directly state that there is an exit in that direction. Try to work in a hint in the text that might give players the idea to further search for something like that.

Here are the direction descriptions for room [1]:

    look north:
    The curve of the island stretches a long way to the north.
    look east:
    A stair leading upward strikes your view to the east. It lies alongside the road that leads all the way through the islands central axis.
    look south:
    The curve of the island stretches a long way to the south.
    look down:
    As much as you would like, you cannot go back from this point of the island. It only takes people up.

* Players can just have arrived in the room, or be on the way back. So never assume one direction for your description, such as "To the east you see the room you came from" when a player could have never been there. Also, avoid such famous lines as "In your front you see the cathedral" when in fact it is in the players back because he, she or it came from the opposite direction.

III. WHAT ARE THE MOBS ABOUT?
=============================

Well, not only the rooms make up an area, but also the opponents you'll find there, the so-called mobs. Often a mob is linked with a room, such as for example Grum in the temple, the shop owners in their shops, and the beholder in Old Thalos. Other mobs are mobile such as the cityguards, vagabonds, and fido's in Midgaard.

In the majority of cases, mobs are the ones you have to defeat in order to gain experience points (xp's) and equipment you need. However, they can also be of help, such as the healers and shop owners, or they just are informative or add color to the area. Try to think up at least the following when you design the mobs:

* Number the mobs. Just as you number the rooms, number the mobs!
* Give a good name. A good name is very important. Try to make the name as distinct as possible: there are enough anonymous guards and warriors around. The name is also shown when you are in a room.
* Description. The description is what the player gets to see when he, she, or it types "look mob". Keep in mind that when the mob is something that occurs quite a lot in the MUD, the description can be more vague then when it occurs only once in the MUD. Don't make the description excessively long, and never never never write down such lines as "The fearsome warrior yells out and strikes at you" when it is a level 5 warrior that simply doesn't attack any player over level 7 and couldn't scare most players out of their wits if he wanted to. It also makes no sense for higher level mobs because the player could be invisible or sneaking around. Only include things that are always true. All the optional bits should become obvious from the mobs behavior.
* Establish whether the mob stays in the room or not. When it is not mobile, this is called "sentinel" for some reason I don't know.
* Include a list of the room numbers where the mob appears. Also include the mob numbers in the room descriptions when you have distributed them. This is very useful to check whether distribution is really satisfactory. 

Here are a few examples from the Laputa design:

    (5) The King's son.
    A very young man looks up from his books and blushes: he should not have noticed you entering the room. He has never left the island and never will according to the rule of Balnibardi. You have disturbed the young prince in the process of sharpening his feather pen.
    Sentinel
    [39]

Notice that in this example, there is a "You" sentence. This sentence is true, whether the prince will attack you or not, and it also holds when a player is fighting the prince. Furthermore, the sentence gives a clue about the equipment of the prince.

    (6) Dauphin.
    The Dauphin is a tall fellow, thin and yet sturdy. He is quite willing to take the throne from his father - living or not. He has never left the island and never will according to the rule of Balnibardi. As though anticipating your unbidden visit, he is wielding a spear.
    Sentinel
    [40]

    (7) (White aura) Queen of Balnibardi.
    The Queen is quite unlike her husband very aware of the world and of your presence. She is a handsome woman and radiates authority. According to Laputa law, she is confined to the island until after child-bearing.
    Sentinel
    [38]

The (White aura) bit indicates that the Queen has a sanctuary spell on her. Sanctuary means that she'll take 50% of the normal damage the attack of a player could deal. Use this aspect only when such a feature can be adequately explained from the mob. In this case the Queen can be safely assumed to know this art.

    (8) Person of Quality.
    A person of lower noblehood, who enjoys the regular visit to the Courts of the King. He is very well at ease, and enjoys the respect of his peers.
    Not sentinel
    [2] [4] [34] [35] [42]

The previous examples all were sentinel, which means they will stay in their rooms. This mob occurs on several places in the Laputa design (rooms `[2]` `[4]` `[34]` `[35]` `[42]`) and can also move about. Notice that the description does not give much personal features since it would be strange to see the same particular features of a person everywhere around.

The mobs have a particular role in your area and should fit into it. Make sure that mobs that belong to a room make sense there.

Here is an example of the room of the Queen:

    [38] Laputa Queen's quarters.
    Highly realistic murals of landscapes give this room an atmosphere that would deny its existence in the air far above the ground. It is clear that the inhabitant yearns for other surroundings than the floating empire.
    look east:
    A grove filled with a magnitude of strange animals meets your eye.
    look grove/animal:
    It is a painting on the wall!
    look north:
    Between the trees, there is a vision of a waterfall from an endless cliff.
    look trees/waterfall/cliff:
    You bump your nose on the painting on the wall.
    look west:
    A rich treasury, large enough to put to shame the collections of the greatest dragons!
    look treasury/collection:
    It isn't all gold that shines - paint does the job too.
    look up:
    A great line of birds of prey and paradise look down upon you.
    look birds/paradise:
    Some skilled master of illusion has painted this scene for sure.
    look south:
    A view into the Chamber of Presence.
    [south]
    =========================
    room: indoors. sector: city

Why is the room described the way it is? The men of Laputa are so much engaged in deep thought that they hardly give any attention to their wives and daughters. These on the other hand, are bored to death on the floating island in the air and yearn for the main land. The Queen is no exception, and therefore she has ordered to have her room painted with the best murals there are. That is also the reason why the description of the directions contain other extra descriptions such as the treasury/collection in the west wall: it looks so real that you look with more detail at it.
Don't overdo this method and do not put vital information in such a second layer of information. If an important clue must be given, hint to that already in the main description of either the mob or the room.

What fun is there in a mob that just stands, sits, or sleeps around waiting for you to come by and kill it? Some basic actions by mobs are defined quite easily such as the mobility question, but also such things as aggressiveness, scavenger (does it pick up trash), defend other mobs, and their races, skills and classes pretty much define what mobs do and are. If you are comfortable with some low level programming, or know a person who is willing to do so, you can come up with special behavior of the mobs: random sentence speaking, or triggering on special words by players, or checking for items in players, etc. Those last things are called mob programs. Read the file "mobprog.doc" which you can find at the JoranMUD site for more detailed information of mob programs.

Here is an example from the Laputa design:

    (3) (White aura) The King of Balnibardi.
    This is the Lord of Laputa and of the island of Balnibardi to which Laputa belongs. The King is engaged in deep contemplation, troubled by a problem of some magnitude. He is a powerful Laputean, and sometimes has to take harsh decisions against cities not willing to pay taxes.
    Sentinel
    [37]
    The King of Balnibardi exclaims:
    "I have solved the problem of the Square Circle!"
    "I have determined the issue of Longitude!"
    "I have conceived the Mandelbrot set!"
    "What on heaven and earth is the c in E=mc2?!"
    "Where did I leave my toys?!"
    "Surely the Earth must be round - like a donut!"

The King of Balnibardi is a Laputean, which means he is engaged in deep thought. The random sentences noted above are chosen from once in a while so that the King appears to be thinking and solving problems. Since he is a Laputean, he is also unable to distinguish true from foolish problems, which explains the selection of phrases.

IV. WHAT EQUIPMENT CAN YOU GET?
===============================

Equipment is anything you can wear or use as player that helps you in your travels or battles in JoranMUD. The fun part of the design is to think of these pieces of equipment. Equipment can belong to a mob (such as the standard issue stuff of the Midgaard cityguards), can lie around in the MUD (such as the herbs in the Planes of the North), or can be for sale (such as in any of the shops). Make sure a mob carries equipment that belongs to the mob (don't give a good priest an evil vampiric sword), and that the levels of the equipment compare fairly well with the mob (a level 90 mob with level 10 equipment is very frustrating for a player, and vice versa does not make much sense).

Before you go around designing equipment, take a good look around in JoranMUD at the equipment there to see what is possible and what the stats on the equipment are. Try to find what kinds of equipment suit the area design and if you can identify items that are difficult to get. JoranMUD has a great collection of swords, for example, but the other kinds of weapons are more difficult to get.

In the Laputa design, special care has been taken to supply non-standard equipment: therefore it is low on swords (in fact only 1!), but there are plenty of different levels daggers (6), flails (3), maces (2), clubs (2), axes (2), polearms (2), spears (2), whips (2), wands (1), and staves (2). There are also magical items such as rings and equipment that are scarce in high levels (gloves (2), boots (1), belts (2), rings (2), staffs (2+), wands (1), leggings (3), robes (6), cloaks (6), gauntlets (1), helms (5), shields (2), belts (2), and potions (4)). There is also one key, and a number of locks to pick.

Here are some examples from the King's personal collection. Notice that since they belong to the same mob, there are many commonalties between the descriptions and stats:

    [bc] Laputa Farewell
    The massive head of this flail seems to move of its own volition, seeking out a target of its liking. The handle is nicely decorated, and a small plate on it says "Point this side towards you". The head is of solid metal, somehow looking lighter than its material would naturally be.
    level 65. bless glow anti-evil. value is 0. 10d7 (average 38). hit roll by +6. damage roll by +4. Adds anti-class affect. Adds anti-race affect.

The stats on level and such are not really necessary in the first phase of designing your area. A basic level indication can be quite adequate. Just remember that the level of equipment and mob are close.

    [bd] Laputa Sidesweap
    Well balanced, with the maximum force of blow located at the sharpest point, this mace makes a formidable weapon for one skilled in its art. A small plate on it says "Not for domestic purposes".
    level 65. bless hum. value is 0. 10d7 (average 38). hit roll +4. damage roll +7. Sharp.

    [be] Laputa Basher
    Not your everyday blunt hit-and-run club, but covered with sharp jagged ridges of very hard steel. It even has a hand protection for parrying blows. A small plate on it says "Hard hat area only".
    level 65. noremove. value is 0. Sharp shocking. hit roll by 7. damage roll by 8. [only a giant can use this as a second weapon] anti-dwarf. anti-hobbit.

Not everything in the design needs to be equipment. You can think of objects that are just there for fun or for giving your design extra depth. The most notable non-equipment item in JoranMUD is jewelry: objects that have some value. There is no limit to what you can imagine on jewelry, and there are plenty of examples in JoranMUD.

Another kind of thing that is worthwhile to think about is stuff like potions, staves, wands, and other magic items. You can also think of items that increase interaction with mobs or that will prevent them from attacking you - the sky is the limit here!

V. ANYTHING ELSE IN THE DESIGN
==============================

V.1. Level range
----------------

An area has a level range, and although opinion differs in this respect, the range can be quite large (for example 30-90) AS LONG AS it is clear where the low - medium - high level mobs are so that a level 30 player does not tumble into an aggressive level 80 mob that makes meatloaf of him. An often used trick is the neutral 'guard-mob' that blocks the rooms with the higher level mobs. Defeating the guard-mob proves you are worthy to enter the rooms with the more difficult mobs (no guarantees of course). Of course it does not always need to be a guard mob. Another trick is that the further you enter the area, the more difficult the mobs become. Plus, the room descriptions before the really tough mobs can give hints about the nearing danger (it could also be bluff though). Make logical groups of mobs whose levels are close to each other, so that when you can basically take them on, you can roam that part of the area until you have leveled enough to visit another part later. By the way, adding a mobile higher level aggressive mob amidst lower level mobs can add some spice to a level: will it show up when a player is fighting something, or will it, she, or he bump into it when taking the all-too-familiar route to the easy to defeat mob?

V.2. Alignment
--------------

One particular flavor that is really nice in JoranMUD is the balancing and use of alignment in mobs. Alignment is a number that shows whether a mob (or player) tends to the satanic (-1000) or angelic (+1000). The -200 <-> +200 range is termed neutral. If a good player kills an evil mob, her, its, his alignment is reinforced. However, when a good mob is killed by a good player, the alignment will drop (and vice versa of course for evil players killing good and evil mobs). Why is this something of influence?
Equipment can have a flag that states whether they can be only be used by evil, good, or neutral players. It is quite inconceivable for example, that the equipment of a thoroughly evil mob can be used by a player who is good or angelic at heart. The equipment then, gets the anti_good flag. Now, the moment a good player kills a number of good mobs, and the alignment has dropped to neutral, then the anti_evil and anti_neutral equipment the player is wearing will refuse to be used by him, her, or it. In this way, players will not randomly kill, but really have to chose their opponents.

*Be clear what general alignment the mobs and their equipment should have.* Do not overdo the extreme good or extreme bad in an area unless you have a very good reason (all mobs in hell are satanic, and all mobs in the Holy Grove are angelic). Rather use more neutral mobs than any of the plainly extreme sides. With equipment however, you can be more differentiated than with mobs. Also notice that at the moment, JoranMUD caters well for good players (much anti_evil equipment around) but rather poorly for evil players (not that much anti_good equipment).

*Animals seldom have an alignment* as they cannot consciously chose for evil or good. Only animals that are associated with good and evil could have alignment other than neutral, but this is rarely the case.

V.3. Aggressive and/or peaceful
-------------------------------

Just as a player will not hack and slack in every room it, she, or he travels through, a mob will not automatically attack a player. The tendency to attack a player or not is called is called aggressiveness. When a mob is aggressive, and a player enters its room of a lower or same level, then the mob will attack. Figure out in your design of the area what mobs should have a tendency to be aggressive and which do not.

What is also possible, is to determine what mobs would help other mobs when they are attacked. This can be helpful for bodyguards that will attack when their boss is attacked.
It is not always the case that a mob is plain in sight when you enter a room. A mob with evil intentions, or one that is very shy, can be hiding or invisible. A mob can also be triggered into attack when a player makes a specific remark or steal something.
V.4. Locks, traps and deathtraps

JoranMUD does not boast many locks. Try to think of your everyday life and count the number of locks you encounter on a daily basis... the number will astound you. So, try to think whether places will be locked, and who carry the locks, and whether the locks are easy or hard to pick if you have no key.
Thieves also have detect trap and remove trap skill, but there are hardly any traps in JoranMUD! Lets make these thieves a valued member of any group of players and introduce those neat nifty evil little traps that poison, sleepgas, explode, or whatever do to players.

The world is a dangerous place, and JoranMUD would be no exception since there are deathtraps at several places. A deathtrap is a room in which a player dies because of some cause (poisonous gas from some vulcanic source, a collapsing building, a 100 yard deep fall, etc.) At the moment of writing this design guide, these deathtraps are not functioning but the day of their revival and undoing of many players will return! In all cases, figure out whether there are some really dangerous places in your area design. Not every area should have a death trap, but if one is unavoidable, include it. Also make sure to include hints so that the weary and careful player can avoid them. Don't make them too obvious - stupid, lazy or illiterate players should have a higher death rate anyway.

VI. FINAL STATS AND DOCUMENTS
=============================

Well, when you have done all this (I told you it would take some weeks) you compile your material in one document. A good set up can be the following:

1. TITLE
2. Brief description of the area.
3. Level range.
4. Maps of the area with numbered rooms.
5. Room descriptions (as shown above) with numbers of the mobs in the rooms.
6. Mob descriptions (as shown above) with the numbers of the rooms they are in and the numbers of the equipment they are carrying.
7. Equipment description (as shown above) including all the objects that are not equipment.

Send it to the head builder and ALWAYS keep a copy (and a backup of your copy) for your self.

The document you now have really helps you to quickly start building once you have received permission to build and got the vnums for building. Even with such a preparation as this one you'll find there are still a lot of things to be decided during building, and you'll be glad to have taken care of the most basic issues right now in the design document.

* **FINAL TIP**: The room numbers, mob numbers, and equipment numbers you have used in your document typically start with (1) and such, whereas the vnums (numbers for your rooms, mobs, and equipment) can start at any value. When you have been assigned a vnum-range (for example 12100-12200), search and replace in your text editor the existing numbers with the new vnums. That greatly simplifies finding your way in the design document when you are actually building in JoranMUD.


Enjoy designing and building!<br>
*Benedict*