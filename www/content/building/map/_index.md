---
title: "Map"
date: 2019-10-31T20:44:12+01:00
menu:
    main:
      name: map
      parent: building
      weight: 40
---
{{<figure src="masterplan.jpg" link="masterplan.jpg" title="Fatal Dimensions Map" width="640">}}

Click on the map to zoom.

Here is a first complete draft of the location of all the areas on Fatal Dimensions. It is still rather sketchy, and in particular New Thalos is drawn much too big, but that was the only way to get it to fit with the river.

Also added in light grey are my ideas about expanding the areas:

* Get a coast line along the south to connect with the east of New Thalos.
* An archipeleago in the south sea.
* The island of Glubdubdrib is in process of building. Added to Balnibardi will also be Lagado city, Lagado Opera, and the Spike.
 
If you feel like doing any of the below ideas, give me a call:

* "Camelot" in the south west coast opposed to the fortress that right now ends the Old Marsh area.
* A big new city in the west and a road to Midgaard to complete it.
* Extending the dark river that runs through Midgaard to the Sands of Sorrow.
* Change the course of Ishtar river to make it fit with the rest of the map.
* Open up the north and add some oriental cities such as "Ur" and "Babylon" (I'd like to have more oriental stuff in the east and western stuff in the west. You know it makes sense).
* The volcano along Ishtar river can be very nice to start a "Journey to the Centre of the Earth" like area.
* Ishtar river now ends suddenly in mountains, but it could also lead to the "Lost World".
* A clue in the text north of Moria mentions a city behind the planes, so that could be the start for something there.
* Whatever more comes up... 

*Benedict, january 2000.*
