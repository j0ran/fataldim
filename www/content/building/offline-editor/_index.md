---
title: "The Fatal Dimensions Offline Editor "
date: 2019-11-10T19:32:42+01:00
menu:
    main:
      name: "offline editor"
      parent: building
      weight: 30
---
These days the offline editor is obsolete. We have a buildersmud which is a maintained cvs copy of the real thing. In particular, when you are working with mobprogs we strongly advise you to use this mud.

Only immortals can download the offline editor.

If you are a fledgling builder you can send [Council](council@fataldimensions.nl) and talk to the Head Builder for more details.

* [JoranMud offline editor](joranmud.zip) (Last updated: October 19th 2019, 20:47)

