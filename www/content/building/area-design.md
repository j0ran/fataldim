---
title: "Area Design"
date: 2019-10-31T21:18:13+01:00
menu:
    main:
      name: "area design"
      parent: building
      weight: 10
---
* [How do I design an area]({{<ref how-do-i-design-an-area>}}): An overview of everything you need to know and do when making a new area in Fatal Dimensions.
* [Glosdraug's Building Rules, Tips & Writing Techniques]({{<ref glosdraugs-building-rules>}}), with additional very good information. Read it!
* [Rom24b4.txt]({{<ref rom2.4>}}), the standard Rom area documentation.
* [Properties Document]({{<ref properties>}}), describes all properties used in Fatal Dimensions.
* [Zone Basics & Hints](http://www.goodnet.com/~esnible/zonebase.html), you can look at this one if you have some spare time.
* [Fatal-Objects]({{<ref fatal-objects>}}), use this page to determine object levels and prices.
* [Builder manual 101]({{<ref builder>}}), this is the document Verlag wants you to look at and try on the testmud.

How to use these documents
--------------------------

If you want to know how to get started about designing an area, read the first two. They show all that goes into making room descriptions, mobs, equipment, levels, and balancing. The first contains the mandatory area proposals that you need to submit to the Head Builder. The second document also has information about balancing. The last document (Zone Basics & Hints) basically repeats everything stated in the two above, but rather abbreviated.

When you are in the process of building, you'll frequently consult the Rom24b4 document. It contains all the basic information of the MUD. The same goes for the Properties document. Use these for reference, and only then ask questions to the staff. During building, you are required to submit report monthly. Glosdraug's documents shows what format you should submit them in.
