---
title: "Glosdraug's Building Rules, Tips & Writing Techniques"
date: 2019-11-02T20:18:26+01:00
hidden: true
menu:
    main:
      parent: building
---
Builder Rules
=============

1. All new area concepts must be submitted & approved by the Head Builder.
2. Submit area ideas to the Head Builder.
3. Areas should try to reflect medival/fantasy theme; Will be flexible on this point if idea/concept is really good & it meets council approval.
4. Monthly Progress Reports From Each Builder Are To Be Submitted To the Head Builder So He Can Submit An Overall Report To The Council
5. These reports are MANDATORY for they will help determine how each individual staff member is performing, which in turn can lead to promotion/demotion & it also gives a sense on how the staff as a whole is doing & what changes, if any, need to be made.
6. Reports look like this:<br>
    Date:<br>
    Rooms Built: vnums - to -<br>
    Objects Built: vnums - to -<br>
    Mobs Built: vnums - to -<br>
    MobProgs Built: vnums - to -<br>
    &lt;short summary of progress and estimated finishing time&gt;
7. DO NOT TRY TO FALSIFY INFO IN A REPORT FOR THEY WILL BE CHECKED!
8. All areas will be checked by the Head Builder before they are connected/open.
9. Any questions on Building should be directed to the Head Builder either by e-mail, note, or directly. 

General Tips
============

* **Do It On Paper First.** Having a map to work from helps A LOT when doing exits & provides a visual impetus to getting all those tedious bits done.
* **An area will take twice as long as you think it will to build.** Unfortunately this is a sad fact of life.
* **Be Fair With Items.** Good items should be hard to get. Lousy items should require much less effort. Utilize the different flags (rot_death, melt_delt, etc.). Utilize alingment flags (anti-good, anti-evil, anti-neutral).<br>
**WARNING**: Use anti-neutral with caution. Utilize the anti-race & anti-class flags ----> Remeber a piece of eq on a halfling or kender SHOULDN'T fit a Giant & vice-versa.
* **DO NOT GO CRAZY WITH ADDING AFFECTS.** REMEMBER: BALANCE!
* **Do not just make items that help**, but also try & mix in some that harm. Variety is the spice of life.
* **Weapon Damage**: The dice roll for a weapon should be the level of that weapon. (I.E. A level 15 sword dice roll would be 5d3, 3d5, etc.). You can average 6-8 levels above or below. (I.E. A level 15 sword could also have a dice roll of 4d5, 3d3, 2d7, etc.)
* **Try to think of pieces of eq which are hard to find** & put them in your area. Some of these include belts, gloves, boots, whips, etc.
* Have Fun with Cool affects on objects, Good & Bad; also have fun with cool skills on mobs. Just make sure to make sense (i.e. No firebreathing halfling mobs). 

Writing Techniques
==================

* When you write a room, the player is already in the room. "SHOW, DO NOT TELL"! DO NOT use the pronoun "YOU" in a room description.
* Do not tell a player what he is "feeling" when entering a room. Try something like "A presence of evil fills this room...." or "A wave of fear seems to develop all who enter this dark place."
* Focus on what is IN the room, NOT what can been seen FROM the room. Think: What is in it? What is the lighting/coloring? Furniture? Trees or Flowers and what kind? Any animal life? Weather? Structures?
* Use language effectively! Avoid plainess like "There is Ivy on the walls." This is nice, but boring. "Ivy springs from cracks in the floor, smothering all other plant life." This is much better.
* Added descriptions. These add character to the game and make players work. If players are forced to solve a puzzle to move on they will need these.
* Descriptions can be used to hide/convey clues/information....doors/exits...objects.
* These added descriptions are NOT mandatory, but they do provide character and flare to an area.
* A room description should average 4-8 lines in length. Please remember to format the room descriptions or any descriptions for that matter.
* A mob description should be around 3 lines in length, but this is flexible, just try not to make them dull.
* Extended object descriptions are optional, but again, add flare.
* Exit descriptions are optional; Make use of the exit flags & have fun with them. 

MobProgs
========

These are not mandatory, but they are definately cool little gadgets. 

Object Balancing
================

Armor

Armor Ratings For Eq

```
Lvl. 1-9 = 2-3
Lvl. 10 = 4-5
Lvl. 15 = 5-6
Lvl. 20 = 6-7
Lvl. 25 = 7-8
Lvl. 30 = 10-11
Lvl. 35 = 11-12
Lvl. 40 = 12-13
Lvl. 45 = 13-14
Lvl. 50 = 14-15
Lvl. 55 = 15-16
Lvl. 60 = 16-17
Lvl. 65 = 18-19
Lvl. 70 = 19-20
Lvl. 71 = 22-25
Lvl. 75 = 25-28
Lvl. 80 = 28-31
Lvl. 85 = 31-34
Lvl. 90 = 35-40
```

As with weapons you may have a 5 level cushion when assigning values for armor. This means you can use either a level 10 rating or a level 20 rating for a level 15 piece of armor. **WARNING: DO NOT GO CRAZY WITH THIS.** I will double check everything.Do not overuse this cushion and don't use it for each type of rating (pierce, bash, slice, exotic). Mix up the values. Exotic armor ratings should be very low, if any rating at all, for armor below level 20.

Miscellaneous
=============

* Remember: BALANCE! BALANCE! BALANCE!
* USE THE HELP FILES!
* Heal/Mana Rates ARE NOT to exceed 300%
* FINISH WHAT YOU BEGIN!
* Work diligently.
* If stuck, ask for help.
* Some things are written in an older form of ROM so they require " " for them to work. Most of the liquids are like this...."dragon blood"
* Don't create area for "lazy" players. Try and Keep things fair, but challenging.
* "Quest Items" must be submitted to the Head Builder (Verlag), who will then submit them to the High Council.
* Building can sometimes get boring, but that is the life of a builder. If you need to take a break, that is fine, but remember there is a lot that needs done, so try to work as much as sanity will allow.
* HAVE FUN IN YOUR CREATIVITY & TAKE PRIDE IN YOUR WORK!
* Download & print builders.doc from web-site; It is helpful in a lot of ways.
* Remember we are a team! Help each other out when you can! 

Verlags additions
=================

A few things/rules:

* All mobs who can become aggresive, or summon/create other aggresive mobs -must- be no_summon
* (more to come) 