---
title: "Mob Programs"
date: 2019-11-02T21:17:20+01:00
menu:
    main:
      name: "mobprogs"
      parent: building
      weight: 20
---
Fatal Dimensions has switched from standard Merc mobprogs to [TCL](http://www.scriptics.com/products/tcltk/) based mobprogs. This makes the mob programs much more flexible. If you want to see an example, go to Grum (recall) and say "Count to 5", or any other number.

* [TCL Mob Progs documentation]({{<ref tclmobprogs>}}), your basic reference for all you want to know about mobprograms in Fatal Dimensions.
* [Benedict's Mob Prog Patterns](../benedicts-mob-program-patterns), Tips & Tricks for writing your very own mob programs for non-programmers. (Work in progress)
* [TCL Command Reference](http://www.scriptics.com/man/tcl8.2/TclCmd/contents.htm) from the [Scriptics](http://www.scriptics.com/) site.
* [TclTutor](http://www.msen.com/~clif/TclTutor.html) is a program to learn the TCL script language. This language is used to program the mobs on Fatal Dimensions.

How to use these documents
--------------------------

Before you start to learn making mobprogs, use the TclTutor. This is an interactive tutorial for learning what TCL is about. Window 95/98/NT users should download "A TCL-Pro wrapped executable for Windows 95 and Windows NT" for a no installation version. Make sure we don't answer questions if you have not done this.

If you do not want to make a career in programming, read Benedict's Mob Prog Patterns document.

The general TCL commands can be found in the TCL Command Reference. Because the mud TCL version is secured, filesystem and network commands do not work. For mud specific commands read the TCL Mob Progs documentation. It is important to understand the part about charid, objid and location.Take a look at examples of mobprogs to understand these completely.

Experiment with the mobprogs using the [Offline Editor](../offline-editor).

{{<usernotes key="tclmobprogs/generalcomments" title="TCL mobprogs comments">}}