---
date: 2022-02-18T22:57:09+01:00
---
Fatal Dimensions has been moved from [Digital Ocean](https://www.digitalocean.com/) to [Linode](https://www.linode.com/).
