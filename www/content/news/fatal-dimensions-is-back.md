---
date: 2019-10-20
---
Fatal Dimensions is back. Except for the website, everything should be exactly the same as the moment when the mud was
shutdown. I brought the mud back for nostalgic reasons, and I plan to keep it running for as long as possible.
