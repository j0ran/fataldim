---
date: 2001-01-23
---
When you have configured your email address in your profile (<a href="/info/help.php?hidx=E&help=EMAIL">help
email</a>), your name will be added to the addressbook on Fatal Dimensions (at 00:00CET) with that address.

