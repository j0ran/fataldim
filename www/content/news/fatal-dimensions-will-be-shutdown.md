---
date: 2012-05-03
---
The Fatal Dimensions domain registration will expire on 31st of May, 2012.
We're not intending to extend this any more. As a side effect, this server
will be shut down afterwards.

Clifton bravely took over the MUD administration. The domain is already his, and he has a running version of the MUD.

If you want to have your playerfile transfered, please send a request by email to me from your email address posted in your finger information. (otherwise the request will not be honoured) 


Thank you all for playing.<br>
Verlag

