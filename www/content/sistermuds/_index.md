---
title: "Sistermuds"
date: 2019-11-16T10:16:22+01:00
linktitle: sister muds
menu: main
weight: 80
---
This is a list of Muds with whom Fatal Dimensions has special working relationships, primarily the sharing of ideas regarding code, building, etc. We are added a live communication between the Muds, but it is only available for immortals at the moment. 

* Currently we have no active sister muds.

Other Muds interested in participating in a sisterhood with Fatal Dimensions should email [the council](mailto:council@fataldimensions.org?$subject=Requesting%20sistermud%20information) for more information.

Old Sistermuds
==============
[![Yerth Banner](yrthbanner.jpg)](http://www.yerth.mudservices.com/)

* [Yerth](http://www.yerth.mudservices.com/) is a ROM 2.4 based MUD, which doesn't seem to be running anymore.  
* [Evil Intentions](http://www.evil.linex.com/). It is not open for public yet. And Aerik said it was okay to add it's link to this homepage. I can't connect to this one anymore. (snif)
* [Realms of Frustration](http://www.cs.utwente.nl/~ia_remko/rof). This mud runned on: [zaphod.cs.utwente.nl 4000](telnet://zaphod.cs.utwente.nl:4000), but isn't alive anymore. But I will keep it here for a while.